#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_flIndLocal.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_flIndLocal.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_flInd.h"
#include "FxString.h"
#include "io_intfile.h"

namespace glb {
	const char* FileName(const char *pcszPath, const int cPathLen);// common/global.cpp
	const CFxString& GetAppPath();// common/global.cpp
	double Bytes(uint64_t n, const char **pps);
}
namespace fl {
namespace i {

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIndILcl class файловый массив файловых отступов. Предполагается, что массив
/// существует долго, на протяжении всей жизни приложения.
/// \param O: объект массива
/// \param ITP: тип индекса: файловый отступ int64_t или индекс int32_t
///
template <class O, int nInds>
class CIndILcl : public CIndBase<O>
{	// отдельный индекс читается в файле при открытии и записывается в файл при закрытии
	template <class ITP>
	class CIndex : public CIntFile<ITP> {
		typename CIndBase<O>::fcmp m_fcmp;// ф-ция сравнения двух объектов
		glb::CFxArray<ITP, true> m_ai;		// массив отступов или индексов для каждого объекта 'О'
	public:
		CIndex();
		CIndex(typename CIndBase<O>::fcmp f);
	public:// файловые наследуют CIntFile<ITP>
		virtual int64_t open(const char *pcszPN) override;
		virtual void close() override;
	public:// массив...
		int32_t Insert(int32_t iAt, ITP v);
		int32_t Append(ITP v);
		void Rem(const int iAt, const int cnt);
		void Rem(const int64_t no1, const int64_t no2, const int32_t nrem);
		ITP RemNC(int32_t iAt);
		void Move(int iTo, int iFrom);
		void Empty();
		void Set(typename CIndBase<O>::fcmp f);
	public:// атрибуты
		bool IsValid() const;
		int32_t count() const;
		int cmp(const O &a, const O &b) const;
		const ITP* Index(int32_t *pcnt) const;
	};
	CIndex<int32_t> m_iDel,		// ид-ры удалённых (не действительных) эл-тов
					m_ai[nInds];// произвольные индексы
public:
	typedef CIndex<int32_t> INDEX;
	typedef CIndex<int64_t> LNDEX;
	CIndILcl(typename CIndBase<O>::fcmp f)
		: CIndBase<O>(f) {}
public:// файловые операцыи
	virtual int64_t open(const char *pcszPN) override;
	virtual void close() override;
public:// отступы
	virtual void Set(typename CIndBase<O>::fcmp *f);
	virtual const CIndex<int32_t>* GetIndex(int32_t *pcnt = NULL) const;
protected:
	virtual CIndex<int32_t>* GetIndex(int32_t *pcnt = NULL);
public:// операцыи с индексом
	virtual int SetOffset(int64_t noffset) override;// добавит только отступ
	virtual int SetInd(int iInd, int iAt, int iBas) override;// вставит базовый индекс в указанный (в удалённых iAt игнорируется)
	virtual void Rem(const int iInd, const int iAt, const int cnt) override;
	virtual void Rem(const int64_t no1, const int64_t no2, const int nrem) override;
	virtual int32_t RemNC(int32_t iInd, int32_t iAt) override;
	virtual void Move(int iInd, int iTo, int iFrom) override;
	virtual void Empty() override;
public:// извлечения обеъкта, экстрацыи...
	virtual int Bas(int iInd, int iObj) override;	// базовый индекс по индексу
	virtual int cmp(int iInd, const O &a, const O &b) const override;	// сравнит объекты
	virtual int32_t count() const override;
	virtual int32_t count(int iInd) const override;
	virtual bool CheckCount() const override;
	virtual int64_t ObjSize(int iInd, int32_t iObj) override;
	virtual const void* Index(int32_t iInd, int32_t *pcnt = NULL) const override;// void* потому что может быть int* и long*
};

template <class O, int nInds>
template <class ITP>
CIndILcl<O,nInds>::CIndex<ITP>::CIndex()
{
	m_fcmp = NULL;
}

template <class O, int nInds>
template <class ITP>
CIndILcl<O,nInds>::CIndex<ITP>::CIndex(typename CIndBase<O>::fcmp f)
{
	m_fcmp = f;
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::open открываю файл pcszPN, читаю индексы и закрываю файл.
/// \param pcszPN: путь и имя файла с индексами.
/// \return к-во индексов
///
template <class O, int nInds>
template <class ITP>
int64_t CIndILcl<O,nInds>::CIndex<ITP>::open(const char *pcszPN)
{
	int64_t cnt = CIntFile<ITP>::open(pcszPN);
	if (cnt > 0) {
		m_ai.SetCountND((int32_t) cnt);
		CIntFile<ITP>::getcp(0, m_ai, cnt * sizeof(ITP));
	}
	CIntFile<ITP>::close();
	return cnt;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::close как часть задачи закрытия индексированного файлового массива.
/// Откроет файл с индексом, запишет индекс и закроет файл.
///
template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::close()
{
	ASSERT(!CBinFile::IsOpen());
	ASSERT(CBinFile::m_sPath.GetCount());
	int64_t nf = CIntFile<ITP>::open(), // однажды открытый файл содержит путь в CBinFile::m_sPath
		na = m_ai.GetCount();
	if (na < nf) // в файле эл-тов больше, нужно сократить к-во
		CIntFile<int64_t>::SetCount((int) na);
	// редактирую весь индекс
	CBinFile::putcp(0, m_ai, na * sizeof(ITP));
	CIntFile<ITP>::close();
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief func. fl::CIndex::Add добавляет отступ объекта 'o' в индекс в соответствии с
/// указанной ф-цией сравнения m_fcmp.
/// \param o: добавляемый в 'Array' объект.
/// \return индекс нового эл-та в этом объекте-индексе.
///
template <class O, int nInds>
template <class ITP>
int32_t CIndILcl<O,nInds>::CIndex<ITP>::Insert(int32_t iAt, ITP v)
{
	ASSERT(iAt >= 0 && iAt < count());
	if (iAt < m_ai.GetCount())
		m_ai.Insert(iAt, v);
	else
		m_ai.Add(v);
	return iAt;
}


/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Append когда точно известно, что 'o' больше всех в индексе...
/// \param o: добавляемый в конец индекса эл-т.
/// \return индекс эл-та
///
template <class O, int nInds>
template <class ITP>
int32_t CIndILcl<O,nInds>::CIndex<ITP>::Append(ITP v)
{
	ASSERT(v >= m_ai.Last());
	return m_ai.Add(v);
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Rem удаляет указанное к-во подряд стоящих (в этом индексе) индексов.
/// Не для отступов! Для удаления отступов в базовом индексе используй Rem(int64_t,int64_t,int)
/// К примеру, нужно удалить 2 эл-та с индексами 4 (iAt) и 5. Которые соотвественно равны 7
/// и 213. Нужно откорректировать оставшиеся так, чтобы каждый из них уменьшился на к-во
/// больших его значения удаляемых индексов.
/// Если рассматриваемый в цикле индекс 8, он уменьшается на 1 и становится равным первому
/// удаляемому. Он уменьшается только однажды. Если рассматриваемый индекс 212, он так же умень-
/// шается только однажды и равен теперь 211. Если рассматриваемый индекс 214, он уменьшается
/// дважды и равен теперь 212. А место удаляемого занимет индекс со значением 215, так же
/// уменьшившись на 2. Если индексы или отступы упорядочены по возрастанию, используй
/// Rem(const int64_t, const int64_t, const int)
/// \param iAt: индекс первого удаляемого эл-та.
/// \param cnt: к-во удаляемых эл-тов.
/// \return
///
template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::Rem(const int32_t iAt, const int32_t cnt)
{
	ASSERT(iAt >= 0 && cnt > 0 && iAt + cnt <= m_ai.GetCount());
	int64_t *const ps = m_ai, *pc, *prc;// 's' start; 'c' current; 'm' maximum
	int64_t *const pm = ps + m_ai.GetCount(), *const prs = ps + iAt, *const prm = ps + iAt + cnt;
	// сначала коррекция остающихся эл-тов
	for (pc = ps; pc < pm; pc++)
		for (prc = prs; prc < prm; prc++)
			if ((pc - ps < iAt || pc - ps >= iAt + cnt) && *pc >= *prc)
				*pc--;
	// затем удаление от iAt до iAt + cnt
	m_ai.RemAt(iAt, cnt);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief fl::CIndex::Rem удаляю из индекса все отступы или индексы от 'no1' включительно,
/// до 'no2' исключительно. Их количество должно быть равно nrem.
/// В отличие от предыдущей ф-ции, где на вводе индексы эл-тов этого индекса, здесь эл-ты
/// удаляются по их значениям в индексе, а не по индексам.
/// \param no1: первый отступ или индекс (удаляется)
/// \param no2: последний отступ или индекс (не удаляется)
/// \param nrem: к-во удаляемых элементов
/// \return к-во удалённых эл-тов.
///
template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::Rem(const int64_t no1, const int64_t no2, const int nrem)
{
	ASSERT(nrem > 0 && nrem <= m_ai.GetCount());
	ASSERT(nrem == (no2 - no1) / sizeof *m_ai);
	ASSERT(no1 >= 0 && no2 > 0 && no2 <= m_ai.Last());
	const int cnt = m_ai.GetCount();
	int nrn = 0, nra = 0; // rem.now, rem.all - к-во эл-тов
	int64_t *pns = m_ai, *pno, *pnm,
		szrem = no2 - no1;	// к-во удаляемых байт или индексов
	for (pno = pns, pnm = pns + cnt; pno < pnm && nra < nrem; pno++)
		if (*pno >= no1 && *pno < no2)
			nrn++;
		else {
			if (*pno >= no2)
				*pno -= szrem;
			if (nrn > 0) {
				pno -= nrn;
				pnm -= nrn;
				/* nrn = 2, nra = 0;
				 * 0123456789
				 *		o
				 * 10 - 5 - 2 = 3
				 */
				if (pno + nrn < pnm)
					memcpy(pno, pno + nrn, (cnt - (int) (pno - pns) - nrn - nra) * sizeof *pno);
				nra += nrn;
				nrn = 0;
			}
		}
	// здесь удалятся последние эл-ты, если (pno == pnm && nrn > 0)
	ASSERT(nrem == nrn + nra);
	m_ai.SetCountND(cnt - nrn - nra);
}

template <class O, int nInds>
template <class ITP>
ITP CIndILcl<O,nInds>::CIndex<ITP>::RemNC(int32_t iObj)
{
	ITP n = m_ai[iObj];
	m_ai.RemAt(iObj);
	return n;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Move перемещает эл-т во время переиндексации (см. CIndex::Reset)
/// \param i: индекс перемещаемого эл-та
/// \param iTo: индекс эл-та, на место которого ставится эл-т из 'i'.
///
template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::Move(int iTo, int iFrom)
{
	m_ai.Move(iTo, iFrom);
}

template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::Empty()
{
	m_ai.SetCountZeroND();
}

template <class O, int nInds>
template <class ITP>
void CIndILcl<O,nInds>::CIndex<ITP>::Set(typename CIndBase<O>::fcmp f)
{
	m_fcmp = f;
}

template <class O, int nInds>
template <class ITP>
int32_t CIndILcl<O,nInds>::CIndex<ITP>::count() const
{
	ASSERT(m_ai.GetCount() == CIntFile<int64_t>::count());
	return m_ai.GetCount();
}

template <class O, int nInds>
template <class ITP>
int CIndILcl<O,nInds>::CIndex<ITP>::cmp(const O &a, const O &b) const
{
	ASSERT(this->m_fcmp);
	return this->m_fcmp(a, b);
}

template<class O, int nInds>
template<class ITP>
const ITP* CIndILcl<O, nInds>::CIndex<ITP>::Index(int32_t *pcnt) const
{
	*pcnt = m_ai.GetCount();
	return m_ai;
}

template <class O, int nInds>
int64_t CIndILcl<O,nInds>::open(const char *pcszPN)
{
	ASSERT(nInds > 0 && nInds < 100);
	char *pszPN = NULL;
	int i, n, x, npath = (int) strlen(pcszPN);
	glb::CFxString sPath(pcszPN, npath);
	sPath.SetCount(npath + 3); // +3: ".01" - ".99"
	pszPN = sPath;
	strncpy(pszPN + npath, ".02", 4);// .02 потому что отсутствуют отступы. см. CIndVLcl::open
	m_iDel.open(pcszPN);
	for (i = 0; i < nInds; i++) {
		snprintf(pszPN + npath, 4, ".%02d", i + 3);
		n = m_ai[i].open(pszPN); // откроет, прочтёт и закроет файл
		if (i == 0)
			x = n;
		else if (x != n)
			throw err::CLocalException(SI_INT_DISPARITY, time(NULL), x, n, pcszPN);
	}
	return m_ai[0].count();
}

template <class O, int nInds>
void CIndILcl<O,nInds>::close()
{
	m_iDel.close();
	for (int i = 0; i < nInds; i++)
		m_ai[i].close();
}

template<class O, int nInds>
void CIndILcl<O, nInds>::Set(typename CIndBase<O>::fcmp *f)
{
	ASSERT(nInds > 0 && nInds < 100);
	for (int i = 0; i < nInds; i++)
		m_ai[i].Set(f + i);
}

///////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Index protected для CIndex
/// \return указатель на массив отступов или индексов, упорядоченных в соответствии с m_fcmp
///
template <class O, int nInds>
typename CIndILcl<O,nInds>::INDEX* CIndILcl<O,nInds>::GetIndex(int32_t *pcnt) // pcnt = NULL
{
	return m_ai;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Index возвращает весь индекс
/// \param pcnt: к-во эл-тов индекса
/// \return
///
template <class O, int nInds>
const typename CIndILcl<O,nInds>::INDEX* CIndILcl<O,nInds>::GetIndex(int32_t *pcnt) const // pcnt = NULL
{
	if (pcnt)
		*pcnt = m_ai.GetCount();
	return m_ai;
}

template<class O, int nInds>
int CIndILcl<O, nInds>::SetOffset(int64_t noffset)
{
	ASSERT(false);
	return -1;// ф-ция только для индекса с файловыми отступами, как CvShrd
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::SetInd вставляет базовый индекс, возвращённый ф-цией SetOffset в указанную позицию
/// определённого индекса.
/// \param iInd: индекс, в который вставляется базовый. Не может быть ioffset
/// \param iAt: позиция в индексе, в которую вставляется базовый индекс
/// \param iBas: базовый индекс, возвращённый ф-цией SetOffset
/// \return iAt
///
template <class O, int nInds>
int CIndILcl<O,nInds>::SetInd(int iInd, int iAt, int iBas)
{
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Insert(iAt, iBas) : m_ai[iInd].Insert(iAt, iBas);
}

template <class O, int nInds>
void CIndILcl<O,nInds>::Rem(const int iInd, const int iAt, const int cnt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	if (iInd == idel)
		m_iDel.RemNC(iAt);
	else
		m_ai[iInd].Rem(iAt, cnt);
}

template <class O, int nInds>
void CIndILcl<O,nInds>::Rem(const int64_t no1, const int64_t no2, const int nrem)
{
	ASSERT(false);
}

template<class O, int nInds>
int32_t CIndILcl<O, nInds>::RemNC(int32_t iInd, int32_t iAt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.RemNC(iAt) : m_ai[iInd].RemNC(iAt);
}

template <class O, int nInds>
void CIndILcl<O,nInds>::Move(int iInd, int iTo, int iFrom)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	m_ai[iInd].Move(iTo, iFrom);
}

template <class O, int nInds>
void CIndILcl<O,nInds>::Empty()
{
	m_iDel.Empty();
	for (int i = 0; i < nInds; i++)
		m_ai[i].Empty();
}

template <class O, int nInds>
int CIndILcl<O,nInds>::Bas(int iInd, int iObj)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Value(iObj) : m_ai[iInd].Value(iObj);
}

template<class O, int nInds>
int CIndILcl<O,nInds>::cmp(int iInd, const O &a, const O &b) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return m_ai[iInd].cmp(a, b);
}

template <class O, int nInds>
int32_t CIndILcl<O,nInds>::count() const
{
	ASSERT(nInds > 0 && nInds < 100);
	int32_t x, i, n;
	for (x = m_ai[0].count(), i = 1, n; i < nInds; i++)
		if (x > (n = m_ai[i].count()))
			x = n;
	return x;
}

template<class O, int nInds>
int32_t CIndILcl<O, nInds>::count(int iInd) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	m_ai[iInd].count();
}

template<class O, int nInds>
bool CIndILcl<O, nInds>::CheckCount() const
{
	ASSERT(nInds > 0 && nInds < 100);
	for (int32_t x = m_ai[0].count(), i = 1; i < nInds; i++)
		if (x != m_ai[i].count())
			return false;
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndILcl::Index значения индекса. Применяется пока что для поиска в m_iDel. Возвращеет
/// указатель на void потому что в других производных от CIndBase индекс может быть файловым
/// отступом т.к. длинным целым.
/// \param iInd: idel или пользовательский
/// \param pcnt: к-во эл-тов в индексе
/// \return указатель на первый эл-т индекса
///
template<class O, int nInds>
const void* CIndILcl<O, nInds>::Index(int32_t iInd, int32_t *pcnt) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Index(pcnt) : m_ai[iInd].Index(pcnt);
}

template <class O, int nInds>
class CIndVLcl : public CIndILcl<O, nInds>
{
//	typename CIndILcl<O, nInds>::template CIndex<int64_t> m_iOfsts; // файловые отступы (базовый индекс)
	typename CIndILcl<O, nInds>::LNDEX m_iOfsts; // файловые отступы (базовый индекс)
public:
	CIndVLcl();	// для массива. Обязательно присвоить зн-е m_fcmp всем эл-там массива
	CIndVLcl(typename CIndBase<O>::fcmp *f);
public:// открытие/закрытие
	virtual long int open(const char *pcszPN = NULL) override;
	virtual void close() override;
public:// запись/удаление
	virtual int SetOffset(int64_t noffset);// добавит только отступ
	virtual void Rem(int iInd, int iAt, int cnt) override;
	virtual void Rem(int iInd, int64_t no1, int64_t no2, int32_t nrem) override;
	virtual int32_t RemNC(int32_t iInd, int32_t iAt) override;
	virtual void Move(int iInd, int iTo, int iFrom) override;
	virtual void Empty() override;
public:// извлечение
	virtual int64_t Offset(int iInd, int iObj);// файловый отступ объекта по индексу
	virtual int64_t Offset(int iObj);// файловый отступ объекта по базовому индексу
	virtual int32_t Bas(int iInd, int iObj) override;	// базовый индекс по индексу
	virtual const void* Index(int32_t iInd, int32_t *pcnt = NULL) const override;// void* потому что может быть int* и long*
public:// сравнение и прочее
	virtual int32_t count() const override;
	virtual int32_t count(int iInd) const override;
	virtual bool CheckCount() const override;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVLcl::CIndVLcl конструктор для массивов. Перед использованием требуется иницыацыя.
///
template<class O, int nInds>
CIndVLcl<O,nInds>::CIndVLcl()
{
	// Требуется иницыацыя ф-цией CIndBase<O>::Set...
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVLcl::CIndVLcl
/// \param *f: указатель на первую ф-цию сравнения в пос-ти ф-ций, количество которых равно nInds для
/// каждого индекса m_ai.
///
template<class O, int nInds>
CIndVLcl<O, nInds>::CIndVLcl(typename CIndBase<O>::fcmp *f)
	: CIndILcl<O,nInds>::CIndILcl(f)
{

}

template<class O, int nInds>
long int CIndVLcl<O, nInds>::open(const char *pcszPN)
{
	ASSERT(nInds >= 0 && nInds < 100);
	char *pszPN = NULL;
	int i, n, x, npath = (int) strlen(pcszPN);
	glb::CFxString sPath(pcszPN, npath);
	sPath.SetCount(npath + 3); // +3: ".01" - ".99"
	pszPN = sPath;
	strncpy(pszPN + npath, ".01", 4);
	x = m_iOfsts.open(pcszPN);
	n = CIndILcl<O,nInds>::open(pcszPN);
	if (x != n)
		throw err::CLocalException(SI_INT_DISPARITY, time(NULL), x, n, glb::FileName(pcszPN, npath + 3));
	return n;
}

template<class O, int nInds>
void CIndVLcl<O,nInds>::close()
{
	m_iOfsts.close();
	CIndVLcl<O,nInds>::close();
}

template<class O, int nInds>
int CIndVLcl<O, nInds>::SetOffset(int64_t noffset)
{
	return m_iOfsts.Append(noffset);
}

template<class O, int nInds>
void CIndVLcl<O, nInds>::Rem(int iInd, int iAt, int cnt)
{
	ASSERT(nInds >= 0 && nInds < 100);
	ASSERT(iInd >= ioffset && iInd < nInds);
	if (iInd == ioffset)
		m_iOfsts.Rem(iAt, cnt);
	else
		CIndILcl<O,nInds>::Rem(iAt, cnt);
}

template<class O, int nInds>
void CIndVLcl<O, nInds>::Rem(int/*iInd*/, int64_t no1, int64_t no2, int32_t nrem)
{
	m_iOfsts.Rem(no1, no2, nrem);
}

template<class O, int nInds>
int32_t CIndVLcl<O, nInds>::RemNC(int32_t iInd, int32_t iAt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.RemNC(iAt) : CIndILcl<O,nInds>::RemNC(iInd, iAt);
}

template<class O, int nInds>
void CIndVLcl<O, nInds>::Move(int iInd, int iTo, int iFrom)
{
	ASSERT(iInd >= idel && iInd < nInds);
	if (iInd == ioffset)
		m_iOfsts.Move(iTo, iFrom);
	else
		CIndILcl<O,nInds>::Move(iTo, iFrom);
}

template<class O, int nInds>
void CIndVLcl<O, nInds>::Empty()
{
	m_iOfsts.Empty();
	CIndILcl<O,nInds>::Empty();
}

template<class O, int nInds>
int64_t CIndVLcl<O, nInds>::Offset(int iInd, int iObj)
{
	ASSERT(iInd >= idel && iInd < nInds);
	return m_iOfsts.Value(CIndILcl<O,nInds>::Bas(iInd, iObj));
}

template<class O, int nInds>
int64_t CIndVLcl<O, nInds>::Offset(int iObj)
{
	return m_iOfsts.Value(iObj);
}

template<class O, int nInds>
int32_t CIndVLcl<O, nInds>::Bas(int iInd, int iObj)
{
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? iObj : CIndILcl<O,nInds>::Bas(iInd, iObj);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVLcl::Index переопределяет ф-цию базового класса. Возвращает указатель на первый эл-т индекса,
/// здесь тип любого индекса int32_t, а в производном классе может быть CIndVLcl int64_t.
/// \param iInd: индекс, обл.значений от idel до nInds - 1
/// \param pcnt: к-во эл-тов в индексе.
/// \return указатель на первый эл-т индекса. Если запрашивается ioffset из производного класса, тип указателя
/// будет int64_t, иначе int32_t
///
template<class O, int nInds>
const void *CIndVLcl<O, nInds>::Index(int32_t iInd, int32_t *pcnt) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.Index(pcnt) : CIndILcl<O,nInds>::Index(iInd, pcnt);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVLcl::count вычисляет наименьшее к-во эл-тов во всех индексах. В процессе добавления
/// нового эл-та к-во индексом может отличаться. По завершении CheckCount.
/// \return
///
template<class O, int nInds>
int32_t CIndVLcl<O, nInds>::count() const
{
	return glb::Mn(m_iOfsts.count(), CIndILcl<O, nInds>::count());
}

template<class O, int nInds>
int32_t CIndVLcl<O, nInds>::count(int iInd) const
{
	ASSERT(nInds >= ioffset && nInds < 100);
	ASSERT(iInd >= 0 && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.count() : CIndILcl<O, nInds>::count(iInd);
}

template<class O, int nInds>
bool CIndVLcl<O, nInds>::CheckCount() const
{
	return (CIndILcl<O, nInds>::CheckCount() && m_iOfsts == CIndILcl<O, nInds>::count(0));
}

}// namespace i
}// namespace fl
