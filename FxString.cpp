/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FxString.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FxString.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxString.h"
#include <cfloat>
//#include <cmath>
#include <ctime>
#include <stdarg.h>
#include "defs.h"
//#include <locale>

namespace glb {
int32_t FormatString(char *pszTxt, const int32_t cMx, const char *pszFmt, va_list &varl);

CFxString::CFxString(CBYTES &ac)
	: CFxArray<char, true>(8)
{
	if (ac.GetCount()) {
		BAS::SetCount(ac.GetCount() + 1);// m_iCount включает завершающий нуль, как и s.m_pData
		ASSERT(m_pData);
		memcpy(m_pData, (const char*) (CBYTE*) ac, m_iCount - 1);
		m_pData[m_iCount - 1] = '\0';
	}
}

CFxString::CFxString(char c)
{
	BAS::SetCount(2);
	m_pData[0] = c;
	m_pData[1] = '\0';
}

CFxString::CFxString(BYTE c)
{
	BAS::SetCount(2);
	m_pData[0] = c;
	m_pData[1] = '\0';
}

const CFxString& CFxString::operator =(const CFxString &s)
{
	ASSERT(s.m_iCount >= 0);
	BAS::SetCount(s.m_iCount);// m_iCount включает завершающий нуль, как и s.m_pData
	if (m_iCount > 0) {
		ASSERT(m_pData);
		memcpy(m_pData, s.m_pData, s.m_iCount);
	}
	return *this;
}

CFxString& CFxString::operator =(CFxString &&s)
{
	Attach(s);
	return *this;
}

const CFxString& CFxString::operator =(CBYTES &ab)
{
	ASSERT(ab.GetCount() >= 0);
	SetCountND(ab.GetCount());// GetCount() включает завершающий нуль если он есть
	if (m_iCount > 0) {
		memcpy(m_pData, (CCHAR*) (CBYTE*) ab, m_iCount - 1);
		m_pData[m_iCount - 1] = '\0';
		while (m_iCount > 1 && m_pData[m_iCount - 2] == '\0')
			m_iCount--;
		if (m_iCount == 1)
			m_iCount = 0;
	}
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::operator = присваивает строку
/// \param pcsz: строка символов, завершённая нулём
/// \return *this
///
const CFxString& CFxString::operator =(CCHAR *pcsz)
{//	m_iCount - к-во символов, включает завершающий нуль
	int32_t cnt = pcsz ? (int32_t) strlen(pcsz) + 1 : 0;
	if (m_iCount > cnt)
		m_iCount = cnt;	// set count no delete
	else if (m_iCount < cnt)
		if (m_iMem >= cnt)
			m_iCount = cnt;
		else
			CFxArray::SetCount(cnt);
	ASSERT(m_pData && pcsz);// для cland, чтобы не п-дел
	memcpy(m_pData, pcsz, cnt);
	return *this;
}

const CFxString& CFxString::operator +=(const CFxString &s)
{
	ASSERT(m_iCount >= 0 && s.m_iCount >= 0);
	int32_t cnt = GetCount();			// без завершающего нуля
	BAS::SetCountND(cnt + s.m_iCount);	// с завершающим нулём, если 's' не пустая строка
	if (m_iCount > 0)
		memcpy(m_pData + cnt, s.m_pData, s.m_iCount);
	return *this;
}

CFxString CFxString::operator +(const CFxString &s) const
{
	CFxString str(*this);
	return str += s;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::operator += добавляет в строку массив байт
/// \param s: массив байт - строка не завершённая нулём
/// \return
///
const CFxString& CFxString::operator +=(CBYTES &ab)
{
	ASSERT(m_iCount >= 0 && ab.GetCount() > 0 && ab.Last() != 0);
	int32_t cnt = GetCount(), c2 = ab.GetCount(); // без завершающего нуля
	BAS::SetCountND(cnt + c2 + 1);	// с завершающим нулём
	if (m_iCount > 0) {
		ASSERT(m_pData);			// для настырного clang, убеждённого, что m_pData тут может быть нуль
		memcpy(m_pData + cnt, ab, c2);
		m_pData[m_iCount - 1] = '\0';
	}
	return *this;
}

const CFxString& CFxString::operator +=(BYTE c)
{
	ASSERT(m_iCount >= 0);
	int32_t cnt = GetCount();	// без завершающего нуля
	BAS::SetCountND(cnt + 2);	// с завершающим нулём
	if (m_iCount > 0) {
		ASSERT(m_pData);		// для настырного clang, убеждённого, что m_pData тут может быть нуль
		m_pData[m_iCount - 2] = c;
		m_pData[m_iCount - 1] = '\0';
	}
	return *this;
}

const CFxString& CFxString::operator +=(char c)
{
	int32_t cnt = GetCount();	// без завершающего нуля
	SetCount(cnt + 1);			// теперь к-во символов больше 1 (включая заверш.нуль, и он завершает строку)
	ASSERT(m_iCount > 1 && m_pData);
	m_pData[m_iCount - 2] = c;
	return *this;
}

int32_t CFxString::Add(int32_t n)
{
	const int32_t cnt = GetCount(), digits = 12;
	if (m_iMem - cnt < digits + 1) {// +1 учитывает завершающий нуль, GetCount возвращает без него
		SetCount(cnt + digits);		// SetCount добавит 1 для учёта завершающего нуля
		m_iCount = cnt;				// чистое к-во без заверш.нуля
	} else if (m_iCount > 0)
		m_iCount--;					// чистое к-во без заверш.нуля
	m_iCount += 1 + glb::i2a(n, m_pData + m_iCount, m_iMem - m_iCount, 10);
	return cnt;						// индекс первого добавленного символа
}

/* Add добавит в строку 'f' указанной точности.
 * Вернёт индекс первого добалвенного символа.
 */
int32_t CFxString::Add(double f, int32_t nPrecision)
{
	const int32_t sz = DBL_DIG + DBL_MANT_DIG + 2, // 2 - точка и знак
			cnt = GetCount();
	if (m_iMem - cnt < sz + 1) {// +1 учитывает завершающий нуль
		SetCount(cnt + sz);		// SetCount добавит завреш.нуль
		m_iCount = cnt;			// к-во без учёта завершающего нуля
	} else if (m_iCount > 0)
		m_iCount--;				// к-во без учёта завершающего нуля
	// как можно это сделать без использования 'sprintf'? П-ц сложно, не стоит ввязываться.
	m_iCount += 1 + snprintf(m_pData + m_iCount, (size_t)(m_iMem - m_iCount), "%.*f", nPrecision, f);
	return cnt;	// индекс первого добавленного символа
}

// добавляет время в указанном формате не более 32-х символов...
int32_t CFxString::Add(time_t tm, const char *pcszFmt) // pcszFmt = "%d.%m.%Y %H:%M"
{
	const int32_t cnt = GetCount(), dt = 32;
	if (m_iMem - cnt < dt + 1) {
		SetCount(cnt + dt);
		m_iCount = cnt;
	} else if (m_iCount > 0)
		m_iCount--;
	m_iCount += 1 + strftime(m_pData + m_iCount, (size_t)(m_iMem - m_iCount), pcszFmt, gmtime(&tm));
	return cnt;
}

// Add добавит строку в указанном формате (формат glb::FormatString) не более 'sz' байт без завешающего нуля.
int32_t CFxString::SetFrmt(cint32_t sz, CCHAR *pcszFmt, ...)
{
	SetCountND(sz);
	va_list vl;
	va_start(vl, pcszFmt);
	int32_t n = FormatString(m_pData, sz, pcszFmt, vl);
	va_end(vl);
	SetCountND(n);
	return n;
}

int32_t CFxString::AddFrmt(cint32_t sz, CCHAR *pcszFmt, ...)
{
	int32_t cnt = m_iCount > 0 ? m_iCount - 1 : 0;
	SetCountND(cnt + sz);
	va_list vl;
	va_start(vl, pcszFmt);
	int32_t n = FormatString(m_pData + cnt, sz, pcszFmt, vl);
	va_end(vl);
	SetCountND(cnt + n);
	return n;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::Insert переопределяет ф-цию базового класса, чтобы добавить нуль в конец
/// \param iAt, c передаю в ф-цию базового класса
///
void CFxString::Insert(int32_t iAt, const char &c)
{
	BAS::Insert(iAt, c);
	m_pData[m_iCount - 1] = '\0';
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::Insert переопределяю только чтобы добавить нуль в конец строки т.к. это не
/// всегда происходит в CFxArray
/// \param iAt, pc, cnt передаю в ф-цию базового класса
///
void CFxString::Insert(int32_t iAt, const char *pc, int32_t cnt)
{
	BAS::Insert(iAt, pc, cnt);
	m_pData[m_iCount - 1] = '\0';
}

/*
 * конвертирую символы строки в wcar_t-символы, используя стандартную С-библиотеку
 * https://cplusplus.com/reference/cwchar/mbrtowc/
 */
int32_t CFxString::Utf8Towc(CFxArray<wchar_t, true> &aw)
{
	mbstate_t mbs;
	size_t sz, n = 0;
	wchar_t wc;
	memset(&mbs, 0, sizeof mbs);	// иницыацыя 'mbs'-состояния
	while (n < m_iCount) {
		sz = mbrtowc(&wc, m_pData + n, m_iCount - n, &mbs);
		if (!sz)
			break;
		aw.AddND(wc);
		n += sz;
	}
	return aw.GetCount();
}

/* pcs - строка символов
 * nCount - к-во символов в pcs без учёта завершающего нуля
 * Add приклеит строку к имеющейся, перераспределит память только если её недостатоно
 * Вернёт индекс первого добавленного эл-та.
 */
int32_t CFxString::Add(const char *pcs, int32_t nCount) // nCount = 1
{
	ASSERT(nCount >= 0 && m_iCount >= 0);
	// если строка this пустая (!m_pData), к-во символов нуль. Если нет, то на 1 больеше (для '\0').
	int32_t i = !m_iCount ? 0 : m_iCount - 1;
	int32_t nNewCnt = i + nCount + 1;
	if (m_iMem < nNewCnt)
		CFxArray<char, true>::SetCount(nNewCnt);
	else
		m_iCount = nNewCnt;
	if (pcs && nCount && m_pData) {
		memcpy(m_pData + i, pcs, nCount);
		m_pData[i + nCount] = '\0';
	}// возр.значение аналогично ф-ции базового класса
	// индексирует первый символ добавленной строки
	return (m_iCount - nCount - 1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::Add добавлю символ в конец строки
/// \param c: новый символ
/// \return индекс добавленного символа
///
int32_t CFxString::Add(const char &c)
{
	ASSERT(m_iCount >= 0 && (!m_iCount || m_pData[m_iCount - 1] == '\0'));
	const int32_t nNewCnt = !m_iCount ? 2 : m_iCount + 1;
	BAS::SetCountND(nNewCnt);
	m_pData[m_iCount - 2] = c;
	m_pData[m_iCount - 1] = '\0';
	return (m_iCount - 2);
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::GetBinData записываю в 'a' содержимое объекта без завершающего нуля
/// \param a
/// \return к-во записанных байт
///
/*int32_t CFxString::GetBinData(BYTES &a) const
{
	cint32_t nBytes = a.GetCount(), cnt = (m_iCount <= 0) ? 0 : m_iCount - 1;
	a.AddND((CBYTE*) &cnt, sizeof cnt);
	if (cnt)
		a.AddND((CBYTE*) m_pData, cnt);
	return (a.GetCount() - nBytes);
}*/

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::SetBinData возвращает в объект то что ранее было взято ф-цией GetBinData.
/// \param p
/// \param n: к-во байт в 'p'
/// \return к-во прочитанных байт
///
/*int32_t CFxString::SetBinData(CBYTE *cpb, int32_t cnt)
{	// GetBinData не записывает заверш.нуль
	int32_t n, i = read_tp(n, cpb, cnt);
	ASSERT(n >= 0);
	if (m_iMem < n)
		SetCount(n);
	ASSERT(!m_iCount || m_iCount > 1);// не может быть нуль
	return (m_iCount ? i + read_raw((BYTE*) m_pData, m_iCount - 1, cpb + i, cnt - i) : sizeof(int32_t));
}*/

/*const CFxStringI& CFxStringI::operator =(const CFxStringI &s)
{
	CFxArray<char, true>::SetCount(s.m_iCount);// m_iCount включает завершающий нуль, как и сама строка m_pData
	if (m_pData)
		memcpy(m_pData, s.m_pData, (size_t) s.m_iCount);
	m_id = s.m_id;
	return *this;
}

const CFxStringI& CFxStringI::operator =(const CFxString &s)
{
	m_iCount = 0;
	Add(s, s.GetCount());// довольно уебищно со стороны страуструппа не давать доступ к защищщ.эл-там 's'
	m_id = INV32_ID;
	return *this;
}

const CFxStringI &CFxStringI::operator =(const CFxArray<char, true> &s)
{
	m_iCount = 0;
	Add(s, s.GetCount());
	m_id = INV32_ID;
	return *this;
}*/

} // namespace glb