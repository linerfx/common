/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл MeasureUnit.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * MeasureUnit.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "MeasureUnit.h"
namespace store {

CMu::CMu()
{
	m_iNm = m_iShrtNm = INV32_BIND;
	m_fGrams = .0;
//	m_flags = 0;
}

CMu::CMu(const CMu &o)
{
	m_iNm = o.m_iNm;
	m_iShrtNm = o.m_iShrtNm;
	m_fGrams = o.m_fGrams;
//	m_flags = o.m_flags;
}

CMu::CMu(double fgr, int32_t inm, int32_t isnm)
{
	m_iNm = inm;
	m_iShrtNm = isnm;
	m_fGrams = fgr;
//	m_flags = 0;
}

/*int32_t CMu::SetBinData(CBYTE *cpb, int32_t cnt)
{
//	ASSERT((uint64_t) this == (uint64_t) &m_fGrams);
	return glb::read_raw((BYTE*) &m_fGrams, SZ_MU, cpb, cnt);
}

int32_t CMu::GetBinData(glb::CFxArray<BYTE, true> &ab) const
{
	ab.Add((CBYTE*) this, SZ_MU);
	return SZ_MU;
}*/

const CMu& CMu::operator =(const CMu &o)
{
	m_fGrams = o.m_fGrams;	// к-во грамм этой е.и.
	m_iNm = o.m_iNm;		// ид-р строки с наименованием е.и.
	m_iShrtNm = o.m_iShrtNm;// ид-р строки с коротким наименованием е.и.
//	m_flags = o.m_flags;	// флаги для массива ITM_* и ITM_MUP если товар штучый
	return *this;
}

};// namespace store