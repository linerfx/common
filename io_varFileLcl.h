#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_varFileLcl.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_varFileLcl.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "defs.h"
#include "io_varFileBase.h"
#include "io_flIndLocal.h"

namespace fl {
namespace a {

template <class Obj, int nInds>
class CvLcl : public CvBase<Obj, nInds>
{
public:
	typedef CvBase<Obj, nInds> BAS;
	typedef i::CIndVLcl<Obj, nInds> INDEX;
protected:
	INDEX m_ind;
public:
	CvLcl(typename INDEX::fcmp *ppf);
	~CvLcl() override {
		if (CvBase<Obj, nInds>::IsOpen())
			CvBase<Obj, nInds>::close();
	}
protected:
	virtual INDEX* GetIndex() override;
public:
	virtual const INDEX* GetIndex() const override;
	virtual const Obj& GetItem(int32_t iIndex, int32_t iItem) const;
	virtual Obj& GetItem(int32_t iIndex, int32_t iItem);
};

////////////////////////////////////////////////////////////////////////////////////
/// \brief CvLcl
/// \param ppf - ук-ль на первый эл-т массива ф-ций сравнения для каждого индекса в к-ве nInds
///
template<class Obj, int nInds>
CvLcl<Obj, nInds>::CvLcl(typename INDEX::fcmp *ppf)
	: m_ind(ppf)
{
	ASSERT(nInds > 0 && nInds < 100 && ppf);
	// теперь открой ф-цией CvBase<Obj,nInds>::open()
}

template<class Obj, int nInds>
i::CIndVLcl<Obj,nInds>* CvLcl<Obj, nInds>::GetIndex()
{
	return &m_ind;
}

template<class Obj, int nInds>
const i::CIndVLcl<Obj,nInds>* CvLcl<Obj, nInds>::GetIndex() const
{
	return &m_ind;
}

template<class Obj, int nInds>
const Obj& CvLcl<Obj, nInds>::GetItem(int32_t iInd, int32_t i) const
{
	ASSERT(iInd >= ioffset && iInd < nInds && i >= 0 && i < GetIndex().count());
	CvLcl<Obj,nInds> *po = const_cast<CvLcl<Obj,nInds>*>(this);
	ASSERT(po != NULL);
	return po->GetItem(iInd, i);
}

template<class Obj, int nInds>
Obj& CvLcl<Obj, nInds>::GetItem(int32_t iInd, int32_t iObj)
{
	ASSERT(iInd >= ioffset && iInd < nInds && iObj >= 0 && iObj < GetIndex().count());
	bool bCloseFile = false;
	int64_t nofst, nosz;
	Obj &obj = this->m_obj;
	const int32_t cnt = GetIndex().count();
	// пользовательские индексы хранят базовые индексы, а базовый индекс - файловые отступы
	int iBas = m_ind.Bas(iInd, iObj);
	ASSERT(iBas >= 0 && iBas < cnt);
	if (true == (bCloseFile = !CvBase<Obj, nInds>::IsOpen()))
		CvBase<Obj,nInds>::open();
	// файловый отступ и размер объекта
	nofst = m_ind.Offset(iBas);
	nosz = (cnt - iBas > 1) ? m_ind.Offset(iBas + 1) - nofst : this->m_nsz - nofst;
	ASSERT(nosz > 0);
	// читаю объект в файле
	BAS::ab.SetCountND((int32_t) nosz);
	this->get(nofst, BAS::ab, nosz);
	if (bCloseFile)
		CvBase<Obj,nInds>::close();
	obj.SetBinData(BAS::ab, BAS::ab.GetCount());
	return obj;
}

} // namespace a
} // namespace fl