/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл ErrLog.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде,
 * в каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * ErrLog.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <cstdarg>
#include "ErrLog.h"
#if defined __ITOG | defined __STRFLA
#include "strdef.h"
#include "io_strings.h"
namespace fl {
	extern CStrings strs;
}// namespace fl
#endif// defined __ITOG | difined __STRFLA

namespace glb {
	int FormatString(char *pszTxt, const int cnt, const char *pszFmt, va_list &varl);
	const char* FileName(const char *pcszPath, int *pnLen);
	int SafeCopyString(char *pszDest, int iDestLen, const char *pcszSource, int iSrcLen = -1);
}
namespace err {

void CLog::Add(const char *pcszFrmt, ...)
{
	const int STRLEN = 512;
	glb::CFxString s;
	s.SetCount(STRLEN - 1);
	va_list vlist;
	va_start(vlist, pcszFrmt);
	s.SetCountND(glb::FormatString(s, STRLEN, pcszFrmt, vlist));
	va_end(vlist);
	putl(s);
}

void CLog::Add(const char *pcszFrmt, va_list &vl)
{
	ASSERT(pcszFrmt);
	const int STRLEN = 512;
	glb::CFxString s;
	s.SetCount(STRLEN - 1);
	s.SetCountND(glb::FormatString(s, STRLEN, pcszFrmt, vl));
	putl(s);
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLog::put для клиента записывает последнюю строку из m_as в файл без меж-процессорной
/// синхронизации файла т.к. клиент запускается в операционке в единственном числе.
/// \return к-во записанных байт. В сл.ошибки швырнет исключение.
///
CLog::~CLog()
{
#if defined __ITGCLNT || defined __LTTS
	m_sDir.SetCount(0);
#endif
}

#if defined __ITGCLNT || defined __LTTS

int CLog::putl(glb::CFxString &s)
{
	ASSERT(s.GetCount());
	cerror = 0;
	FILE *pf;
	glb::CFxString sPath;
	std::lock_guard<std::recursive_mutex> l(m_mtx);
	const char *pcszPath = PathName(sPath);
	if (!pcszPath)
		return 0;// замечена активность после работы деструктора glb::log
	if ((pf = fopen(pcszPath, "a")) != NULL) {
		if (s.Last() != '\n')
			s.Add('\n');
		int n = fwrite((const char*) s, 1, s.GetCount(), pf);
		fclose(pf);
		if (n != s.GetCount())
			throw err::CCException(cerror, __FILE__, __LINE__);
		emit NewRecord(s);
		return n;
	}
	throw err::CCException(cerror, __FILE__, __LINE__);
}

#else
////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLog::put для сервера (приложения в операционке, в которой работает QSharedMemory,
/// такой как Linux, Wind'а, Mac... все кроме Android'а) записывает последнюю строку из m_as
/// в файл с меж-процессорной/меж-потоковой синхронизацией.
/// \return к-во записанных байт. В сл.ошибки швырнет исключение.
///
int CLog::putl(glb::CFxString &s)
{
	cerror = 0;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs);
	fl::CString &sf = os.obj();
	FILE *pf;
	const int cfn = 32;
	char pszfn[cfn];
	glb::CFxString sPath, sKey;
	int n = -1, e;
	CCHAR *pcszfn = glb::FileName(PathName(sPath), &n);
	glb::SafeCopyString(pszfn, cfn, pcszfn, n);
	sKey.SetCount(127);
	sKey.SetCountND(fl::CFileMan::CFile::MkKey(sKey, cfn, 2, SZ(SI_LOGD, sf), pszfn));
	fl::CFileMan::CLocker l(sKey);
	if ((pf = fopen(sPath, "a")) != NULL) {
		s.Add('\n');
		n = fwrite((CCHAR*) s, 1, s.GetCount(), pf);
		e = cerror;
		fclose(pf);
		if (n != s.GetCount())
			throw err::CCException(e, SZ(SI_CE, sf), time(nullptr), strerror(e), e,
								   glb::FileName(__FILE__, nullptr), __LINE__);
		emit NewRecord(s);
		return n;
	}
	e = cerror;
	throw err::CCException(e, SZ(SI_CE, sf), time(nullptr), strerror(e), e, glb::FileName(__FILE__, nullptr), __LINE__);
}
#endif// #if defined __ITGCLNT || defined __LTTS, #else
} // namespace err