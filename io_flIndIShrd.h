#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_flIndIShrd.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_flIndIShrd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_flInd.h"
#include <QSharedMemory>
#include "FxString.h"
#include "io_intfile.h"
#include "FileMan.h"
#include "io_path.h"

namespace glb {
	const char* FileName(const char *pcszPath, const int cPathLen);// common/global.cpp
	const CFxString& GetAppPath();// common/global.cpp
	double Bytes(uint64_t n, const char **pps);
}
namespace fl {
namespace i {// index

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIndIShrd class. Производные: CIndVShrd
///
template <class O, int nInds>
class CIndIShrd : public CIndBase<O>
{
public:
	template <class TP>
	class CMem {
	public:
		int64_t	m_nAll,	// к-во эл-тов в выделенном сегменте разделяемой памяти
				m_nUsed;// использовано эл-тов в разделяемой памяти. Если это к-во больше m_nAll, значит сегмент не действительный
		int32_t m_nUsers,// к-во пользователей этого сегмента. При обнулении содержимое записывается на диск.
				m_res;
		TP		m_no;	// первый эл-т массива в к-ве m_nAll/Used может быть 32 бит для базового индекса, или 64 для отступа
		/////////////////////////
		void Init(int64_t na, int64_t nu);
		int Add(TP val);
		void Move(int64_t iTo, int64_t iFrom);
		int Insert(int64_t iAt, TP val);
		bool IsValid() const;
		static size_t StaticSize();
		static size_t DynamicSize(int64_t nElements);
		static size_t size(int64_t ni, int64_t *pn);
	};
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/// \brief The CIndex class индекс файлового массива, размещнный в разделяемой памяти
	///
	template<class TP>
	class CIndex : public CIntFile<TP> {
	private:
		QSharedMemory *m_psm;					// разделяемая между потоками и процессами память
		CMem<TP> *m_pMem;						// структура памяти
		typename CIndBase<O>::fcmp m_fcmp;		// ф-ция сравнения двух объектов
	public:
		CIndex();
		CIndex(typename CIndBase<O>::fcmp f);
		~CIndex();
	public:// файловые операцыи
		virtual int64_t open(const char *pcszPN) override;
		virtual void close() override;
		virtual void drop();
	protected:// управление выделенной памятью
		int64_t Realloc();
	public:// открытая добыча/старания
		void Set(typename CIndBase<O>::fcmp f);
		TP Value(int i) const;
		virtual const TP* Index(int32_t *pcnt = NULL) const;
	public:// операцыи с разделяемой памятью
		int32_t Insert(int32_t iAt, TP v);
		int32_t Append(TP v);
		void Rem(cint32_t iAt, cint32_t cnt);
//		void Rem(const int64_t no1, const int64_t no2, const int32_t nrem);
		TP RemNC(int32_t iAt);
		void Move(int iTo, int iFrom);
		void Empty();
	public:// атрибуты
		bool IsValid() const;
		int32_t count() const override;
		int cmp(const O &a, const O &b) const;
		static const char* MkKey(char *pszKey, const int cnKey, const char *pcszPN, int n);
	};
	typedef CIndex<int32_t> INDEX;
	typedef CIndex<int64_t> LNDEX;
private:
	INDEX m_iDel, m_ai[nInds];// базовые индексы удалённых эл-тов и произвольных индексов
public:
	CIndIShrd();	// для массива. Обязательно присвоить зн-е m_fcmp во всех массивах
	CIndIShrd(typename CIndBase<O>::fcmp *f);
public:// открытие/закрытие/определение ф-ций сравнения для индексов
	virtual long int open(const char *pcszPN = NULL) override;
	virtual void close() override;
	void drop();
	virtual void Set(typename CIndBase<O>::fcmp *f) override;
public:// запись/удаление
	virtual int SetOffset(int64_t noffset) override;// добавит только отступ
	virtual int SetInd(int iInd, int iAt, int iBas) override;// вставит базовый индекс в указанный (в удалённых iAt игнорируется)
	virtual void Edit(int32_t iInd, int32_t iStart, int32_t nAdd) override;// изменит все значения, начиная от iStart
	virtual void Rem(int iInd, int iAt) override;
	virtual int RemNC(int iInd, int iAt) override;
	virtual void Move(int iInd, int iTo, int iFrom) override;
	virtual void Empty() override;
public:// извлечение
	virtual int32_t Bas(int iInd, int iObj) const override;	// базовый индекс по индексу
	virtual int64_t Ofst(int iInd, int iObj) const override;// файловый отступ
	virtual const void* Index(int32_t iInd, int32_t *pcnt = NULL) const override;
public:// сравнение и прочее
	virtual int cmp(int iInd, const O &a, const O &b) const override;	// сравнит объекты
	virtual int32_t count() const override;
	virtual int32_t count(int iInd) const override;
	virtual bool CheckCount() const override;
	virtual bool Del() const;
	virtual bool IsValid() const;
};

template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CMem<TP>::Init(int64_t na, int64_t nu)
{
	ASSERT(na >= nu);
	m_nAll = na;
	m_nUsed = nu;
	m_nUsers = 1;
	m_no = -1; // первый отступ изначально недействительный
}

//////////////////////////////////////////////////////////////////////////////////
/// CIndIShrd<O,nInds>::CMem<TP>::Add добавляет значение в массив, возвращает индекс
/// добавленного эл-та или -1, если вся выделенная память использована.
///
template<class O, int nInds>
template<class TP>
int CIndIShrd<O,nInds>::CMem<TP>::Add(TP val)
{
	ASSERT(m_nUsed <= m_nAll);
	if (m_nUsed == m_nAll)
		return -1;// требуется перераспределение памяти
	TP *pi = &m_no;
	pi[m_nUsed] = val;
	return m_nUsed++;
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief CIndexShared::CMem::Move перемещает объект с одной позиции на другую
/// аналогично CFxArray::Move.
/// \param iTo: новый индекс для эл-та
/// \param iFrom: прежний индекс эл-та
///
template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CMem<TP>::Move(int64_t iTo, int64_t iFrom)
{
	ASSERT(iTo != iFrom && iTo >= 0 && iFrom >= 0 && iTo < m_nUsed && iFrom < m_nUsed);
	TP *pi = &m_no, n = pi[iFrom];
	if (iTo < iFrom)
		memmove(pi + iTo + 1, pi + iTo, (iFrom - iTo) * sizeof *pi);
	else
		memcpy(pi + iFrom, pi + iFrom + 1, (iTo - iFrom) * sizeof *pi);
	pi[iTo] = n;
}

template<class O, int nInds>
template<class TP>
int CIndIShrd<O,nInds>::CMem<TP>::Insert(int64_t iAt, TP val)
{
	ASSERT(m_nUsed <= m_nAll);
	if (m_nUsed == m_nAll)
		return -1;// требуется перераспределение памяти
	TP *pi = &m_no;
	memmove(pi + iAt + 1, pi + iAt, (m_nUsed - iAt) * sizeof *pi);
	pi[iAt] = val;
	return m_nUsed++;
}

template<class O, int nInds>
template<class TP>
bool CIndIShrd<O,nInds>::CMem<TP>::IsValid() const
{
	return (m_nUsed >= 0 && m_nAll >= m_nUsed);
}

template<class O, int nInds>
template<class TP>
size_t CIndIShrd<O,nInds>::CMem<TP>::StaticSize()	// static
{
	return (sizeof m_nAll + sizeof m_nUsed + sizeof m_nUsers + sizeof m_res);
}

template<class O, int nInds>
template<class TP>
size_t CIndIShrd<O,nInds>::CMem<TP>::DynamicSize(int64_t nElements) // static
{
	return (sizeof(TP) * nElements);
}

////////////////////////////////////////////////////////////////
/// \brief size считает размер запрашиваемой памяти в байтах
/// \param ni (В): имющееся/нужное к-во индексов (элементов в основном файле)
/// \param pn (Из): следует запрашивать это к-во эл-тов
/// \return требуемое к-во байт
///
template<class O, int nInds>
template<class TP>
size_t CIndIShrd<O,nInds>::CMem<TP>::size(int64_t ni, int64_t *pn) // static
{	// меньшее из 1024 и 128 | меньшее из 1024 и х + 25% | т.е. не более 1024
	*pn = ni + MIN(1024, MAX(128, (int) (ni * .25)));// запрашиваю на четверть больше
	if (*pn % 8)
		*pn += 8 - *pn % 8;
	return (CMem::StaticSize() + CMem::DynamicSize(*pn));
}

template<class O, int nInds>
template<class TP>
CIndIShrd<O,nInds>::CIndex<TP>::CIndex()
{
	m_psm = NULL;
	m_pMem = NULL;
	m_fcmp = NULL;
}

template<class O, int nInds>
template<class TP>
CIndIShrd<O,nInds>::CIndex<TP>::CIndex(typename CIndBase<O>::fcmp f)
{
	m_psm = NULL;
	m_pMem = NULL;
	m_fcmp = f;
}

template<class O, int nInds>
template<class TP>
inline CIndIShrd<O, nInds>::CIndex<TP>::~CIndex()
{
	if (m_psm) {
		if (m_psm->isAttached())
			m_psm->detach();
		delete m_psm;
		m_psm = nullptr;
	}
	m_pMem = nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd<O,nInds>::CIndex<TP>::open открываю файл, извлекаю содержимое в разделяемую
/// память и закрываю файл.
/// Здесь доступ к файлу не синхронизируется, это происходит при открытии основного файла
/// CIntFileI или CVarFile.
/// \param pcszPN: полный путь и имя файла индекса
/// \return к-во эл-тов индекса (к-во эл-тов основного файла)
///
template<class O, int nInds>
template<class TP>
int64_t CIndIShrd<O,nInds>::CIndex<TP>::open(const char *pcszPN)
{
	if (m_psm && m_psm->isAttached()) {
		m_pMem = (CMem<TP>*) m_psm->data();
		// ... сравнение указателей. Нахуя оно тут? Определяет повторное открытие. Это сравнение тут никчему.
		if (m_pMem && m_pMem->IsValid() && pcszPN != CBinFile::m_sPath)
			return m_pMem->m_nUsed;
		m_pMem->m_nUsers--;
		m_psm->detach();// сегмент не вместил нужное к-во, должен быть новый (см. Add)
	}
	const int cnKey = 64;
	int64_t nMem = -1;
	QSharedMemory::SharedMemoryError e = QSharedMemory::NoError;
	char pszKey[cnKey] = "";
	int64_t nUsed = CIntFile<TP>::open(pcszPN);
	try {
		MkKey(pszKey, cnKey, pcszPN, nUsed);
		if (m_psm)
			m_psm->setKey(pszKey);
		else
			m_psm = new QSharedMemory(pszKey);
		size_t sz = CMem<TP>::size(nUsed, &nMem);// не менее 128 индексов или отступов
		if (m_psm->create(sz, QSharedMemory::ReadWrite)) {
			m_pMem = (CMem<TP>*) m_psm->data();
			m_pMem->Init(nMem, nUsed);// 1 user
			if (nUsed)
				CIntFile<TP>::getcp(0, &m_pMem->m_no, nUsed);
		} else if ((e = m_psm->error()) == QSharedMemory::AlreadyExists && m_psm->attach()) {
			m_pMem = (CMem<TP>*) m_psm->data();
			m_pMem->m_nUsers++;
			nUsed = m_pMem->m_nUsed;
		} else {
			const char *pcsz;					// %t \'%s\' при запросе %*f %s в файле '%s':%d.
			throw err::CLocalException(SI_SHMEM, time(NULL), qPrintable(m_psm->errorString()),
									   2, glb::Bytes(sz, &pcsz), pcsz, __FILE__, __LINE__);
		}
	} catch (...) {
		CIntFile<TP>::close();
		throw;
	}
	CIntFile<TP>::close();
	return nUsed;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd<O,nInds>::CIndex<TP>::close - часть ф-ции закрытия индексированного, синхро-
/// низированного файлового массива. Основной файл открывается и закрывается всякий раз, блокируя
/// и отпуская доступ других потоков/процессов к файлу и попутно, к индексам этого типа, одним из
/// которых является этот объект.
/// Т.к. индекс используется множеством процессов и потоков, здесь общая память не освобождается.
/// Пока нет понятия долговременного использования файла. Основной открывается с эксклюзивным доступом,
/// в том числе к находящимся в разделяемой памяти индексам. Закрывает файл, открывая доступ другим
/// приложениям/потокам, но индексы должны оставаться открытыми. До каких пор? До завершения работы
/// приложения. Значит, до деструктора основного файла? Действо синхронизируется основным файлом.
/// Пока что close ничего тут не делает, закрытие вынесу в drop.
///
template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::close()
{
	ASSERT(IsValid());
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndexShared::drop запишет индекс в файл. Ф-цию следует вызывать по завершении
/// работы с индексированным массивом.
///
template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::drop()
{	// последний пользователь запишет индексы.
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->IsValid());// см. CMem::Add и CMem::Insert
	ASSERT(!CBinFile::IsOpen() && CBinFile::m_sPath.GetCount());
	if (--m_pMem->m_nUsers == 0) {
		int64_t n = CIntFile<TP>::open();// однажды открытый файл содержит путь в CBinFile
		if (n > m_pMem->m_nUsed)	// укорачиваю файл
			CIntFile<TP>::SetCount(m_pMem->m_nUsed);
		if (m_pMem->m_nUsed > 0)	// перезаписываю весь индекс
			CIntFile<TP>::putcp(0, &m_pMem->m_no, m_pMem->m_nUsed);
		CIntFile<TP>::close();		// перезапишет m_nsz и закроет файл (fclose calls flush)
	}
	m_pMem = NULL;
	m_psm->detach();// деструктор удалит память m_psm. Однако при аварийном завершении деструктор не вызывается.
	delete m_psm;
	m_psm = nullptr;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndexShared::Realloc перераспределяет разделяемую память когда запас исчерпан.
/// См. CMem::Add/Insert
/// Первый поток/процесс, обнаружевший переполнение, сбрасывает в файл (изменённое и в другом
/// к-ве) содержимое индекса, отмечает этот сегмент как недействительный, бросает этот сегмент,
/// запрашивает новый с запасом и читает в него из файла.
/// Другой поток, при открытии файла, обрануживший недействительный сегмент, читает из файла
/// к-во, делает с ним ключ и подключается к новому сегменту this->open.
/// \return к-во элементов
///
template<class O, int nInds>
template<class TP>
int64_t CIndIShrd<O,nInds>::CIndex<TP>::Realloc()
{
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->m_nUsed == m_pMem->m_nAll);// см. CMem::Add/Insert
	const int cKey = 64;
	char szKey[cKey] = "";
	// последовательность действий: 1) сбросить содержимое m_pShrd в файл, 2) отключиться от разделяемой
	// памяти, 3) сделать ключь с новым к-вом, 4) запросить большее к-во с другим ключом, 5) прочесть файл
	// в новый сегмент.
	int64_t nUsed = CIntFile<TP>::open(), nMem = -1;// nUsed - прежнее к-во
	CIntFile<TP>::edit(0, &m_pMem->m_no, m_pMem->m_nUsed);
	// новый ключ с новым к-вом
	MkKey(szKey, cKey, CBinFile::m_sPath, (int) m_pMem->m_nUsed);
	// nUsed далее - известное к-во, позже CMem::size назначит требуемое.
	nUsed = m_pMem->m_nUsed++;	// (++) метка для других пользователей этого сегмента: он не действительный
	ASSERT(!m_pMem->IsValid());
	m_pMem->m_nUsers--;		// один выбыл
	m_psm->setKey(szKey);	// задействует detach()
	m_pMem = NULL;
	size_t sz = CMem<TP>::size(nUsed, &nMem);
	if (m_psm->create(sz, QSharedMemory::ReadWrite)) {
		m_pMem = (CMem<TP>*) m_psm->data();
		m_pMem->Init(nMem, nUsed);
		// возвращаю индекс в новый сегмент
		CIntFile<TP>::get(0, &m_pMem->m_no, nUsed);
	} else {
		CIntFile<TP>::close();
		const char *pcsz;
		throw err::CLocalException(SI_SHMEM, time(NULL), qPrintable(m_psm->errorString()),
		                           2, glb::Bytes(sz, &pcsz), pcsz, glb::FileName(__FILE__, nullptr), __LINE__);
	}
	CIntFile<TP>::close();
	return nUsed;
}

template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::Set(typename CIndBase<O>::fcmp f)
{
	ASSERT(f);
	m_fcmp = f;
}

template<class O, int nInds>
template<class TP>
TP CIndIShrd<O,nInds>::CIndex<TP>::Value(int i) const
{
	ASSERT(IsValid());
	ASSERT(i >= 0 && i < m_pMem->m_nUsed);
	TP *pi = &m_pMem->m_no;
	return pi[i];
}

template<class O, int nInds>
template<class TP>
const TP* CIndIShrd<O,nInds>::CIndex<TP>::Index(int32_t *pcnt) const
{
	ASSERT(IsValid());
	if (pcnt)
		*pcnt = m_pMem->m_nUsed;
	return &m_pMem->m_no;
}

template<class O, int nInds>
template<class TP>
int32_t CIndIShrd<O,nInds>::CIndex<TP>::Insert(int32_t iAt, TP v)
{
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->IsValid());
	ASSERT(iAt >= 0 && iAt <= m_pMem->m_nUsed);
	if (iAt < m_pMem->m_nUsed) {
		if (m_pMem->Insert(iAt, v) < 0) {
			Realloc();
			m_pMem->Insert(iAt, v);
		}
	} else if (m_pMem->Add(v) < 0) {
		Realloc();
		m_pMem->Add(v);
	}
	ASSERT(m_pMem->IsValid());
	ASSERT(iAt < m_pMem->m_nUsed);
	return iAt;
}

template<class O, int nInds>
template<class TP>
int32_t CIndIShrd<O,nInds>::CIndex<TP>::Append(TP v)
{
	int32_t iv = m_pMem->Add(v);
	if (iv < 0) {
		Realloc();
		iv = m_pMem->Add(v);
	}
	ASSERT(m_pMem->IsValid());
	return iv;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndexLocal::Rem удаляет указанное к-во подряд стоящих индексов (не отступов!).
/// Для удаления отсупов используй Rem(const int64_t, const int64_t, const int32_t).
/// \param iAt: индекс первого удаляемого эл-та.
/// \param cnt: к-во удаляемых эл-тов.
/// \return
///
template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::Rem(cint32_t iAt, cint32_t cnt)
{
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->IsValid());
	ASSERT(iAt >= 0 && cnt > 0 && iAt + cnt <= m_pMem->m_nUsed);
	TP *const ps = &m_pMem->m_no, *pc, *prc;// 's' start; 'c' current; 'm' maximum
	TP *const pm = ps + m_pMem->m_nUsed, *const prs = ps + iAt, *const prm = ps + iAt + cnt;
	// коррекция остающихся индексов
	for (pc = ps; pc < pm; pc++)
		for (prc = prs; prc < prm; prc++)
			if ((pc - ps < iAt || pc - ps >= iAt + cnt) && *pc >= *prc)
				(*pc)--;
	// затем удаление от iAt до iAt + cnt
	if (iAt + cnt < m_pMem->m_nUsed)
		memcpy(prs, prm, (m_pMem->m_nUsed - iAt - cnt) * sizeof(TP));
	m_pMem->m_nUsed -= cnt;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd<O,nInds>::CIndex<TP>::Rem удаляю из индекса файловые отступы от 'no1'
/// включительно, до 'no2' исключительно из индекса, где отступы не упорядочены по
/// возрастанию. Для упорядоченных отступов есть ф-ция EditOfsts(int,int)..
/// \param no1: первый отступ от начала файла (удаляется)
/// \param no2: последний отступ от начала файла (не удаляется)
/// \param nrem: к-во удаляемых отступов
/// \return к-во удалённых эл-тов.
/// ф-ция рабочая, удалена за ненадобностью после того, как отступы в индексах заменены базовыми индексами.
/*template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::Rem(const int64_t no1, const int64_t no2, const int32_t nrem)
{
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->IsValid());
	ASSERT(nrem > 0 && nrem <= m_pMem->m_nUsed);
	ASSERT(nrem == (no2 - no1) / sizeof m_pMem->m_no);
	ASSERT(no1 >= 0 && no2 > 0 && no2 <= (&m_pMem->m_no)[m_pMem->m_nUsed - 1]);
	const int64_t cnt = m_pMem->m_nUsed;
	int32_t nrn = 0, nra = 0; // rem.now, rem.all - к-во эл-тов
	TP *pns = &m_pMem->m_no, *pno, *pnm,
		szrem = no2 - no1;	// к-во байт удаляемых из основного файла
	for (pno = pns, pnm = pns + cnt; pno < pnm && nra < nrem; pno++)
		if (*pno >= no1 && *pno < no2)
			nrn++;
		else {
			if (*pno >= no2)
				*pno -= szrem;
			if (nrn > 0) {
				pno -= nrn;
				pnm -= nrn;
*/				/* nrn = 2, nra = 0;
				 *		||
				 * 0123456789
				 *		o
				 * 10 - 5 - 2 = 3
				 */
/*				if (pno + nrn < pnm)
					memcpy(pno, pno + nrn, (cnt - (pno - pns) - nrn - nra) * sizeof *pno);
				nra += nrn;
				nrn = 0;
			}
		}
	// здесь удалятся последние эл-ты, если (pno == pnm && nrn > 0)
	ASSERT(nrn + nra == nrem);
	m_pMem->m_nUsed -= nrem;
}*/

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndexShared::RemNC protected удаляет из индекса эл-т без коррекции других эл-тов
/// Используется для отката последнего изменения в сл.возникновения файловой ошибки или для
/// удаления из индекса недействительных эл-тов.
/// \param iAt: индекс удаляемого эл-та
/// \return удалённый эл-т (удалённое значение)
///
template<class O, int nInds>
template<class TP>
TP CIndIShrd<O,nInds>::CIndex<TP>::RemNC(int32_t iAt)
{
	ASSERT(IsValid());
	ASSERT(iAt >= 0 && iAt < m_pMem->m_nUsed);
	TP *const ps = &m_pMem->m_no, n;
	n = ps[iAt];
	if (m_pMem->m_nUsed - iAt > 1)
		memcpy(ps + iAt, ps + iAt + 1, (m_pMem->m_nUsed - iAt - 1) * sizeof(TP));
	m_pMem->m_nUsed--;
	return n;
}

template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::Move(int iTo, int iFrom)
{
	m_pMem->Move(iTo, iFrom);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd<O,nInds>::CIndex<TP>::Empty обнулит к-во эл-тов в разделяемой памяти.
///
template<class O, int nInds>
template<class TP>
void CIndIShrd<O,nInds>::CIndex<TP>::Empty()
{
	ASSERT(m_psm && m_psm->isAttached());
	ASSERT(m_pMem && m_pMem->IsValid());// зови 'open' сначала
	m_pMem->m_nUsed = 0;
}

template<class O, int nInds>
template<class TP>
bool CIndIShrd<O,nInds>::CIndex<TP>::IsValid() const
{
	return (m_pMem && m_pMem->IsValid() && m_psm && m_psm->isAttached() && m_psm->size() >= sizeof(CMem<TP>));
}

template<class O, int nInds>
template<class TP>
int32_t CIndIShrd<O,nInds>::CIndex<TP>::count() const
{
	return ((m_pMem && m_pMem->IsValid()) ? (int32_t) m_pMem->m_nUsed : CIntFile<TP>::count());
}

template<class O, int nInds>
template<class TP>
int CIndIShrd<O,nInds>::CIndex<TP>::cmp(const O &a, const O &b) const
{
	ASSERT(m_fcmp);
	return m_fcmp(a, b);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::CIndex::MkKey собирает уникальный ключ для файла, который состоит из числа и имён
/// папки и файла.
/// \param pszKey: результат ф-ции
/// \param cnKey: к-во символов в pszKey
/// \param pcszPN: указывает на первый символ строки с именами папки и файла, разделёнными стандартным разде-
/// лителем, как '/' или '\' для windows.
/// \param n: число помещается в начало ключа. Использую размер использованной памяти для унификации.
/// \return ключ pszKey
///
template<class O, int nInds>
template<class TP>
const char* CIndIShrd<O,nInds>::CIndex<TP>::MkKey(char *pszKey, const int cnKey, const char *pcszPN, int n)// static
{
	int32_t nk = glb::i2a<int>(n, pszKey, cnKey, 10),
	        i = fl::CPath::LastNames(3, pcszPN);// третьим именем с конца вероятно будет имя приложения
	ASSERT(i >= 0);
//	if (i > 0)
//		i--;// на разделитель
	CFileMan::CFile::MkKey(pszKey + nk, cnKey - nk, 1, pcszPN + i);
	return pszKey;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::CIndIShrd конструктор для массивов. Перед использованием требуется иницыацыя.
///
template<class O, int nInds>
CIndIShrd<O,nInds>::CIndIShrd()
{
	// Требуется иницыацыя ф-цией CIndBase<O>::Set...
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::CIndIShrd
/// \param *f: указатель на первую ф-цию сравнения в пос-ти ф-ций, количество которых равно nInds для
/// каждого индекса m_ai.
///
template<class O, int nInds>
CIndIShrd<O,nInds>::CIndIShrd(typename CIndBase<O>::fcmp *f)
{
	for (int i = 0; i < nInds; i++)
		m_ai[i].Set(f[i]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::open откроет все файлы индексов, прочтёт их содержимое и закроет каждый файл.
/// \param pcszPN: путь и имя к основному файлу, чьим индексом является этот объект.
/// \return к-во элементов массива
///
template<class O, int nInds>
long int CIndIShrd<O,nInds>::open(const char *pcszPN)
{
	ASSERT(nInds > 0 && nInds < 100);
	char *pszPN = NULL;
	int i, n, x, npath = (int) strlen(pcszPN);
	glb::CFxString sPath(pcszPN, npath);
	sPath.SetCount(npath + 3); // +3: ".01" - ".99"
	pszPN = sPath;
	strncpy(pszPN + npath, ".02", 4);// .01 для отступов CIndVShrd
	m_iDel.open(pszPN);
	for (i = 0; i < nInds; i++) {
		snprintf(pszPN + npath, 4, ".%02d", i + 3);
		n = m_ai[i].open(pszPN); // откроет, прочтёт и закроет файл
		if (i == 0)
			x = n;
		else if (x != n)
			throw err::CLocalException(SI_INT_DISPARITY, time(NULL), 0, i, glb::FileName(pcszPN, npath + 3));
	}
	return m_ai[0].count();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::close только для совместимости. Для индекса в разделяемой памяти такое закрытие
/// не требуется т.к. файл, в котором размещаются индексы закрывается в ф-ции open сразу после чте-
/// ния. А индексы продолжают существовать в разделяемой памяти. Для завершения работы с индексами
/// нужно вызывать drop.
///
template<class O, int nInds>
void CIndIShrd<O,nInds>::close()
{
	m_iDel.close();
	for (int i = 0; i < nInds; i++)
		m_ai[i].close();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::drop отсоединяет объект от разделяемой памяти. Если этот индекс последний в
/// числе пользователей разделяемой памяти, то запишет индекс в файл.
///
template<class O, int nInds>
void CIndIShrd<O,nInds>::drop()
{
	m_iDel.drop();
	for (int i = 0; i < nInds; i++)
		m_ai[i].drop();
}

template<class O, int nInds>
void CIndIShrd<O, nInds>::Set(typename CIndBase<O>::fcmp *f)
{
	ASSERT(nInds > 0 && nInds < 100);
	for (int i = 0; i < nInds; i++)
		m_ai[i].Set(f[i]);
}

template<class O, int nInds>
int CIndIShrd<O, nInds>::SetOffset(int64_t/*noffset*/)
{
	ASSERT(false);// для производного...
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::SetInd вставляет базовый индекс, возвращённый ф-цией SetOffset в указанную позицию
/// определённого индекса.
/// \param iInd: индекс, в который вставляется базовый. Не может быть ioffset
/// \param iAt: позиция в индексе, в которую вставляется базовый индекс
/// \param iBas: базовый индекс, возвращённый ф-цией SetOffset
/// \return iAt
///
template<class O, int nInds>
int CIndIShrd<O,nInds>::SetInd(int iInd, int iAt, int iBas)
{
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Insert(iAt, iBas) : m_ai[iInd].Insert(iAt, iBas);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::Edit изменяет подряд стоящие индексы, упорядоченные по возрастанию. Здесь это
/// возможно только для индекса idel.
/// \param iInd: возможно только одно значение: idel
/// \param iStart: индекс первого изменяемого эл-та
/// \param nAdd: добавляемое/отнимаемое значение
///
template<class O, int nInds>
void CIndIShrd<O, nInds>::Edit(int32_t iInd, int32_t iStart, int32_t nAdd)
{
	ASSERT(iInd == idel);
	int32_t cnt, *pi = (int32_t*) Index(iInd, &cnt);
	ASSERT(pi != NULL);
	for (pi += iStart; iStart < cnt; iStart++, pi++)
		*pi += nAdd;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::Rem удаляет указанное к-во эл-тов из индекса, правит оставшиеся эл-ты.
/// см. CIndex<TP>::Rem
///
template<class O, int nInds>
void CIndIShrd<O,nInds>::Rem(int iInd, int iAt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	if (iInd == idel)
		m_iDel.Rem(iAt, 1);
	else
		m_ai[iInd].Rem(iAt, 1);
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::RemNC удаляет эл-т из индекса без изменения других эл-тов. Может ис-
/// пользоваться для отката начатых изменений при добавлении эл-та основного массива, или
/// извлечения индекса не действительного эл-та из m_iDel. Напомню, m_iDel упорядочен по
/// значению базового индекса, который он хранит.
/// \param iInd: idel или произвольный индекс
/// \param iAt: индекс эл-та. Игнорируется, если iInd равен idel
/// \return удалённый индекс
///
template<class O, int nInds>
int CIndIShrd<O, nInds>::RemNC(int iInd, int iAt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? (m_iDel.count() ? m_iDel.RemNC(iAt) : -1) : m_ai[iInd].RemNC(iAt);
}

template<class O, int nInds>
void CIndIShrd<O,nInds>::Move(int iInd, int iTo, int iFrom)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	m_ai[iInd].Move(iTo, iFrom);
}

template<class O, int nInds>
void CIndIShrd<O,nInds>::Empty()
{
	m_iDel.Empty();
	for (int i = 0; i < nInds; i++)
		m_ai[i].Empty();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::Bas вернёт базовый индекс объекта
/// \param iInd: индекс, по которому извлекается базовый. Не может быть ioffset
/// \param iObj: индекс объекта в iInd
/// \return базовый индекс объекта
///
template<class O, int nInds>
int32_t CIndIShrd<O,nInds>::Bas(int iInd, int iObj) const
{
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Value(iObj) : m_ai[iInd].Value(iObj);
}

template<class O, int nInds>
int64_t CIndIShrd<O, nInds>::Ofst(int iInd, int iObj) const
{
	ASSERT(false);// здесь нет размера объекта. Бери "szo * Bas(iInd, iObj)"
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::Index возвращает указатель на первый эл-т указанного индекса. Указатель на void потому
/// что в шаблоне базового класса нет TP (хотя, можно было бы и дать), а производный содержит файловый отступы.
/// Поэтому ф-ция CIndBase<O>::Index(int32_t, int32_t) может вернуть int* или long*
/// \param iInd: индекс. Не может быть ioffset
/// \param pcnt: (ИЗ) к-во эл-тов индекса. Для удалённых m_iDel будет отличаться.
/// \return указатель на первый эл-т индекса. Здесь всегда int32_t*
///
template<class O, int nInds>
const void* CIndIShrd<O,nInds>::Index(int32_t iInd, int32_t *pcnt) const
{
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return (iInd == idel) ? m_iDel.Index(pcnt) : m_ai[iInd].Index(pcnt);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::cmp сравнивает 2 объекта ф-цией сравнения указанного индекса
/// \param iInd: индекс, чьей ф-цией сравниваются объекты
/// \param a, b: сравниваемые объекты
/// \return -1, 0 или 1.
///
template<class O, int nInds>
int CIndIShrd<O,nInds>::cmp(int iInd, const O &a, const O &b) const
{
	ASSERT(iInd >= 0 && iInd < nInds);
	return m_ai[iInd].cmp(a, b);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndIShrd::count возвращает наименьшее к-во эл-тов в индексе. Наименьшее, потому что эта ф-ция
/// отвечает за сведения о к-ве эл-тов файлового массива с эл-тами динамич.размера. Во время добавления нового
/// эл-та в массив к-во в индексах может быть разным. Поэтому достоверное к-во в этом случае - наименьшее.
/// \return к-во эл-тов в индексе
///
template<class O, int nInds>
int32_t CIndIShrd<O,nInds>::count() const
{
	ASSERT(nInds > 0 && nInds < 100);
	int32_t cnt = INT32_MAX;
	for (int i = 0; i < nInds; i++)
		cnt = glb::Mn(cnt, m_ai[i].count());
	return cnt;
}

template<class O, int nInds>
int32_t CIndIShrd<O, nInds>::count(int iInd) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd == idel || iInd >= 0 && iInd < nInds);
	return ((iInd == idel) ? m_iDel.count() : m_ai[iInd].count());
}

template<class O, int nInds>
bool CIndIShrd<O, nInds>::CheckCount() const
{
	ASSERT(nInds > 0 && nInds < 100);
	for (int x = m_ai[0].count(), i = 1; i < nInds; i++)
		if (x != m_ai[i].count())
			return false;
	return true;
}

template<class O, int nInds>
bool CIndIShrd<O, nInds>::Del() const
{
	return (m_iDel.count() > 0);
}

template<class O, int nInds>
bool CIndIShrd<O, nInds>::IsValid() const
{
	if (!m_iDel.IsValid())
		return false;
	for (int i = 0; i < nInds; i++)
		if (!m_ai[i].IsValid())
			return false;
	return true;
}

}// namespace i
}// namespae fl