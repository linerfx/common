#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_strings.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_strings.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_varFileShrd.h"
#include "io_string.h"

namespace fl {

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CStrings class
/// Все строки, влючая сообщения об ошибках, предупреждения, вопросы к п-лю, а так же списки продуктов и товаров
/// приложения iTog хранятся в файле 'strings', который представляет собой индексированный файловый массив с
/// объектами динамического размера, каждый из которых состоит из последовательности UTF-8 символов.
/// Есть вспомогательная программа StrFileArray, с помощю которой возможно создать и редактировать файловый
/// массив строк.
///
class CStrings : public a::CvShrd<CString, CString::iStrsInds>
{
	const int32_t m_aiu[2]{ CString::iStrsNms, CString::iStrsLeti };
public:
	typedef a::CvShrd<CString, CString::iStrsInds> BAS; // base class
public:
	explicit CStrings();// объявляю CStrings глобально
	virtual ~CStrings();
	// переопределения
	virtual CCHAR* PathName() override;
	virtual CCHAR* Key(glb::CFxString &s) override;
	virtual cint32_t* UnicIndices(int32_t *pcnt) const override;
public:
//	int cmp(int32_t a, int32_t z);
};

///////////////////////////////////////////////////////////////////////////////////
/// \brief CStrings::UnicIndex уникальный индекс
/// \return
///
/*inline int32_t CStrings::UnicIndex() const
{
	return m_aiu[0];// первый уникальный индекс
}*/

/*inline int CStrings::cmp(int32_t a, int32_t z)
{
	CFileItemObtainer<CStrings, CString> oa(this), oz(this);
	return oa.GetItem(ioffset, a).cmp(oz.GetItem(ioffset, z));
}*/

}// namespace fl