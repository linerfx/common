/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FileMan.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FileMan.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FileMan.h"
#include <QString>
#include "exception.h"
//#include "io_path.h"
#include <ctime>
#include <cstdarg>
#include "ErrLog.h"
#include <ctime> // clock

namespace err {
	extern CLog log;			// журнал (ошибок в том числе) для каждого потока
}

namespace glb {
	double Bytes(uint64_t n, const char **pps);
}

namespace fl {
	extern CFileMan man;

/////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::cmp_ids статическая ф-ция сравнивает 2 файла
/// \param f1
/// \param f2
/// \return <0; 0; >0...
///
int CFileMan::cmp_keys(const CFileMan::CFile &f1, const CFileMan::CFile &f2)
{
    ASSERT(f1.m_pcszKey && f2.m_pcszKey);
    return strcmp(f1.m_pcszKey, f2.m_pcszKey);
}

////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFileMan
///
CFileMan::CFileMan() :
	m_afc{ cmp_keys },
	m_af(16, m_afc)
{

}

CFileMan::~CFileMan()
{

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::Lock блокирует указанный файл указанного инструмента и периода
/// \param pcszKey: уникальный идентификатор файла. Копируется в разделяемую память, тот указатель возвращается.
/// \return возвращает базовый ид-р "файла" в массиве 'm_af', или -1 в сл.ошибки. Этот индекс постоянный для
/// объекта 'pcszKey' на протяжении всей жизни приложения.
///
int32_t CFileMan::Lock(const char *pcszKey)
{
    CFileMan::CFile f, *pf;
	int i = -1;
	f.m_pcszKey = pcszKey;
    m_mtx.lock();
	if ((i = m_af.FindEqual(f, INV32_IND, fl::ibanal)) >= 0 && i < m_af.GetCount()) {
		pf = &m_af.GetItem(fl::ibanal, i);
		i = m_af.BaseInd(fl::ibanal, i);
	} else {
		m_mtx.unlock();
		f.Create(pcszKey);
		m_mtx.lock();
		i = m_af.Add(&f); // assert 'f' is valid; добавит копию, вернёт базовый ид-р, не индекс fl::ibanal
		f.Invalidate();
		pf = &m_af[i];
	}
	if (!pf->Locked())
		pf->Lock();
	m_mtx.unlock();
	return i;
}

///////////////////////////////////////////////////////////////////////
/// \brief CFileMan::Lock блокирует файл по прямому индексу, однажды возвращённому
/// ф-цией Lock(const char*).
/// \param i: базовый ид-р fl::ioffset, не fl::ibanal
///
void CFileMan::Lock(int32_t iFile)
{
	ASSERT(iFile >= 0 && iFile < m_af.GetCount());
	m_mtx.lock();// доступ к массиву "файлов"
	try {
		CFile &f = m_af[iFile];
		f.Lock();
		m_mtx.unlock();
	} catch (...) {
		m_mtx.unlock();
	}
}

/////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::Unlock
/// \param i: базовый ид-р "файла", не индекс fl::ibanal
///
void CFileMan::Unlock(int32_t i)
{
	m_mtx.lock();
	try {
		ASSERT(i >= 0 && i < m_af.GetCount() && m_af[i].IsValid());
		m_af[i].Unlock();
		m_mtx.unlock();
	} catch (...) {
		m_mtx.unlock();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::Destroy ставит точку в работе с "файлом" - удаляет объект "разделяемая память"
/// \param i: базовый ид-р "файла" в m_af, не индекс
///
void CFileMan::Destroy(int32_t i)
{
	m_mtx.lock();
	try {
		ASSERT(m_af[i].IsValid());
		CFile &f = m_af[i];
		if (f.Locked())
			f.Unlock();
		f.Destroy();
		m_mtx.unlock();
	} catch (...) {
		m_mtx.unlock();
	}
}

void CFileMan::Destroy()
{
	CFile *pf = nullptr;
	m_mtx.lock();
	try {
		for (int i = 0, n = m_af.GetCount(); i < n; i++) {
			pf = &m_af.GetItem(0, i);
			if (pf->Locked())
				pf->Unlock();
			pf->Destroy();
		}
		m_mtx.unlock();
	} catch (...) {
		m_mtx.unlock();
	}
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::Locked
/// \param _if: базовый ид-р файла fl::ioffset, не индекс, вроде fl::ibanal
/// \return
///
bool CFileMan::Locked(int32_t _if)
{
	m_mtx.lock();
	try {
		bool blocked = m_af[_if].Locked();
		m_mtx.unlock();
		return blocked;
	} catch (...) {
		m_mtx.unlock();
		throw;
	}
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::GetFile находит или создаёт "файл" по ключу
/// \param pcszKey: ключ файла, собранный ф-цией MkKey
/// \return найденный или созданный по ключу файл
///
const CFileMan::CFile* CFileMan::GetFile(const char *pcszKey)
{
	ASSERT(pcszKey && *pcszKey);
	CFileMan::CFile f, *pf = nullptr;
	int i = -1;
	f.m_pcszKey = pcszKey;
	m_mtx.lock();// меж-потоковая синхронизация доступа к массиву "файлов"
	if ((i = m_af.FindEqual(f, INV32_IND, 0)) >= 0 && i < m_af.GetCount())
		pf = &m_af.GetItem(0, i);
	else {
		try {
			f.Create(pcszKey);
		} catch(...) {
            m_mtx.unlock();
			throw;
        }
        pf = &m_af[m_af.Add(&f)];
    }
    m_mtx.unlock();
    return pf;
}

/////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::CFile
///
CFileMan::CFile::CFile()
	: m_blocked(false)
	, m_psm(nullptr)
	, m_pcszKey(nullptr)
{

}

CFileMan::CFile::CFile(const CFileMan::CFile &f)
	: m_blocked(false)
	, m_psm(nullptr)
	, m_pcszKey(nullptr)
{
	*this = f;
}

///////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::CFile: move constructor перемещает объект f в этот.
/// \param f
///
CFileMan::CFile::CFile(CFileMan::CFile &f)
    : glb::CFxArrayIndItem(f)
	, m_psm(f.m_psm)
	, m_pcszKey(f.m_pcszKey)
	, m_blocked(f.m_blocked)
{
	f.m_psm = nullptr;
    f.m_pcszKey = nullptr;
	f.m_blocked = false;
//	f.m_id.Init();
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::SetKey: статическая ф-ция собирает уникальный ключ для файла на диске,
/// предположительно по именам папок от корневой папки, в которой лежат все сведения приложения.
/// Таким как iTog/strings/. Такой путь может быть разделённым на отдельные строки или одной строкой.
/// \param pszKey: ИЗ - результат, ключ - строка, завершённая нулём.
/// \param nSzKey: В - размер pszKey в байтах
/// \param n: к-во строк, из которых собирается ключ
/// \param ... только завершённые нулём строки символов char
/// \return: длина строки pszKey без завершающего нуля
///
int CFileMan::CFile::MkKey(char *pszKey, const int nSzKey, int n, ...)
{	// копирую символы, заменяя разделитель и пробел на подчёркивание
    va_list vlist;
	int len = 0, i;
	auto cpys =[&len] (char *szd, int nld, const char *szs) {
		int i = 0, // szs
		    j = 0; // szd
		while (j < nld - 1 && szs[i])
			if (!j && len > 0)
				szd[j++] = '_';
			else if (szs[i] == CPSEP || isspace(szs[i])) {
				szd[j++] = '_';
				i++;
			} else
				szd[j++] = szs[i++];
		szd[j] = '\0';
		return j;// strlen szd
	};
	len = cpys(pszKey, nSzKey, "FlManKey");
    va_start(vlist, n);
	for (i = 0; i < n; i++)
		len += cpys(pszKey + len, nSzKey - len, va_arg(vlist, const char*));
    va_end(vlist);
	return len;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::operator=() оператор копирования для индексированного массива CFxArrayInd
/// теперь 23.09.24 оператор перемещения, сдирает с аргумента всё что есть, оставляя аргумент пустым.
/// Объясняю ниже.
/// \param o: копируемый объект
/// \return этот объект
///
const CFileMan::CFile& CFileMan::CFile::operator =(const CFile &co)
{
	m_blocked = co.m_blocked;
	m_pcszKey = co.m_pcszKey;
	m_psm = co.m_psm;
	CFile &o = const_cast<CFile&>(co);
	o.m_blocked = false;
	o.m_pcszKey = nullptr;
	o.m_psm = nullptr;
	// если 'o' подключен к разделяемой памяти нужно подключить к этой же памяти и этот объект
/*	if (o.m_psm && o.m_psm->isAttached()) {
		if (!m_psm)
			m_psm = new QSharedMemory;
		else if (m_psm->isAttached() && m_psm->key() != o.m_psm->key())
			m_psm->detach();
		if (!m_psm->isAttached()) {
			m_psm->setKey(o.m_psm->key());
			m_psm->attach();
			if (!m_blocked) {
				m_blocked = m_psm->lock();
				ASSERT(m_blocked == true);
			}// 23.09.24 debug здесь возникает такой вопрос: если o.IsLocked(), нельзя перенести это состояние в
			// этот объект без утраты тем. Если я копирую ключ, значит 'o' подлежит утилизации? Не может
			// же быть два объекта с одним ключом? Копирование происходит при изменении размера массива "файлов"
			// fl::CFileMan::m_af. Значит, 'o' нужно не копировать, а перемещать m_psm = o.m_psm.
			CShared *ps = (CShared*) m_psm->data();
			m_pcszKey = &ps->m_cKey;
			if (m_blocked) {
				m_blocked = m_psm->unlock() != true;
				ASSERT(!m_blocked);
			}
		}
	} else if (m_psm && m_psm->isAttached())
		m_psm->detach();
	glb::CFxArrayIndItem::operator =(o);*/
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::Create создаёт объект разделяемой (на уровне операционки) памяти (shared mem)
/// Увы, не фацает в ипаном андройде. Только для Linux, Windows, Mac-хуяк.
/// \param pcszKey: идентификатор файла, собранный ф-цией MkKey
/// \param nKeyLen: длина pcszKey без завершающего нуля
/// \param parent: см. QSharedMemory
/// \return результат создания сегмента памяти ф-цией QSharedMemory::create
///
bool CFileMan::CFile::Create(const char *pcszKey, QObject *parent) // parent = Q_NULLPTR
{
    if (!m_psm)
        m_psm = new QSharedMemory(pcszKey, parent);
	else if (m_psm && m_psm->isAttached() && m_psm->key().compare(pcszKey) != 0)
        m_psm->detach();
	size_t sz = CShared::size(pcszKey);
	CShared *ps = NULL;
	m_pcszKey = NULL;
	// андройд уебан почему то не хочет create (LockError (6))
	if (m_psm->create(sz, QSharedMemory::ReadWrite)) {
		m_psm->lock();
		ps = (CShared*) m_psm->data();
		ps->init(pcszKey);
		m_pcszKey = &ps->m_cKey;
		m_psm->unlock();
	} else if (m_psm->error() == QSharedMemory::AlreadyExists && m_psm->attach()) {
		ps = (CShared*) m_psm->data();
		m_pcszKey = &ps->m_cKey;
	} else {
		CCHAR *pcsz = nullptr;
		double f = glb::Bytes(sz, &pcsz);
		QString se = m_psm->errorString();	// %t \'%s\' при запросе %*f %s в файле '%s':%d.
		throw err::CLocalException(SI_SHMEM, time(NULL), qPrintable(se), 2, f, pcsz, __FILE__, __LINE__);
    }
	return true;
}

void CFileMan::CFile::Destroy()
{
	if (m_psm) {
		if (m_psm->isAttached())
			m_psm->detach();
		delete m_psm;
		m_psm = nullptr;
	}
}

///////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::Changed определяет, был ли файл изменён после
/// предыдущего доступа к нему этим объектом.
/// \return не нуль, если файл был изменён после его предыдущего открытия
///
bool CFileMan::CFile::Changed(clock_t since) const
{
	ASSERT(m_blocked && m_psm && m_psm->isAttached());
	CShared *ps = (CShared*) m_psm->data();
	return(!ps->m_tmUpdated || since < ps->m_tmUpdated);
}

void CFileMan::CFile::Invalidate()
{   // отсоединяю объект от системного, если подсоединён
	if (m_psm && m_psm->isAttached())
		m_psm->detach();
	m_pcszKey = nullptr;
	m_blocked = false;
}

CFileMan::CFile& CFileMan::CFile::operator =(CFileMan::CFile &&f)
{
	m_psm = f.m_psm;
	m_pcszKey = f.m_pcszKey;
	m_blocked = f.m_blocked;
	f.m_psm = nullptr;
	f.m_pcszKey = nullptr;
	f.m_blocked = false;
	return *this;
}

////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CLocker::CLocker создаст объект и заблокирует ключ.
/// \param pcszKey: уникальный идентификатор файла. Если не нуль, объект
/// синхронизации будет создан заблокирован. Иначе останется не активным.
/// Задействовать объект можно будет ф-ций Lock()
///
CFileMan::CLocker::CLocker(const char *pcszKey)
	: m_if(-1)
{
	if (pcszKey)
		m_if = man.Lock(pcszKey);
}

CFileMan::CLocker::~CLocker()
{
	if (m_if >= 0 && man.Locked(m_if))
		man.Unlock(m_if);
	m_if = -1;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CLocker::Lock - для повторного использования объекта c тем же ключом,
/// который был использован в ф-ции Lock(const char*).
///
void CFileMan::CLocker::Lock()
{
	ASSERT (m_if >= 0 && !man.Locked(m_if));
	man.Lock(m_if);
}

void CFileMan::CLocker::Unlock()
{
	ASSERT (m_if >= 0 && man.Locked(m_if));
	man.Unlock(m_if);
}

void CFileMan::CLocker::Destroy()
{
	man.Destroy(m_if);
}

bool CFileMan::CLocker::Locked() const
{
	return (m_if >= 0 && man.Locked(m_if));
}

clock_t CFileMan::CLocker::TmUpdated() const
{
	ASSERT(IsValid());
	return man.GetFile(m_if)->GetTmUpdate();
}

/////////////////////////////////////////////////////////////////////////////
/// \brief CFileMan::CLocker::Lock если конструктор этого объекта был без параметров
/// \param pcszKey: ключ к "файлу" синхронизации CFileMan::CFile
/// \return индекс "файла" в массиве CFileMan::m_af
///
int32_t CFileMan::CLocker::Lock(const char *pcszKey)
{
	return (m_if = man.Lock(pcszKey));
}

}// namespace fl