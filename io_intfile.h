#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_intFile.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_intFile.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_file.h"

#define SZTP ((int32_t) sizeof(TP))
#define I2B (iAt * SZTP)
#define CNT (cnt * SZTP)
#define FLCNT (m_nsz / SZTP)
namespace fl {
/*************************************************************************
 * CIntFile: файловый массив эл-тов одинакового размера 'TP' в файле CBinFile
 * осторойжней с ним, если объекты содержат виртуальные ф-ции... наследуй файловые объекты от CFileItem.
 */
template <class TP>
class CIntFile : public CBinFile
{
public:	// открытие/закрытие
	virtual int64_t open(const char *pcszPN = NULL) override;
public:// грубое вмешательство - добавляю, изменяю, удаляю...
	virtual int64_t put(const TP *pd, const int64_t cnt);
	virtual int64_t putcp(const int64_t iAt, const TP *pd, const int64_t cnt);
	virtual int64_t insert(const int64_t iAt, const TP *po, const int64_t cnt);
	virtual int64_t edit(const int64_t iAt, const TP *po, const int64_t cnt);
	virtual int64_t remove(const int64_t iAt, const int64_t cnt) override;
	void SetCount(int nNewCount);
	int64_t UpdateIndexed(const TP *a, const int na, int (*)(const TP&, const TP&));
public:// извлечение
	virtual int64_t get(const int64_t iAt, TP *po, const int64_t cnt = 1) const;
	virtual int64_t getcp(const int64_t iAt, TP *po, const int64_t cnt = 1) const;
	int64_t getlast(int64_t cnt, glb::CFxArray<TP, true> &a) const;
public:// атрибуты
	int64_t Find(const TP &o, int (*)(const TP&, const TP&)) const;
public: // атрибуты
	virtual int32_t count() const;
};

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile::open откроет файл
/// \param pcszPN: путь и имя файла. Может быть нулём, если файл открывается повторно.
/// \return к-во элементов 'TP' в файле.
///
template<class TP>
int64_t CIntFile<TP>::open(const char *pcszPN) // pcszPN = NULL
{
   int64_t n = CBinFile::open(pcszPN);
   ASSERT(!(n % sizeof(TP)));
   return (n / sizeof(TP));
}

template<class TP>
int64_t CIntFile<TP>::put(const TP *pd, const int64_t cnt)
{
	return (CBinFile::put(pd, CNT) / SZTP);
}

template<class TP>
int64_t CIntFile<TP>::putcp(const int64_t iAt, const TP *pd, const int64_t cnt)
{
	ASSERT(iAt >= 0 && iAt <= FLCNT);
	return (CBinFile::putcp(I2B, pd, CNT) / SZTP);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile<TP>::insert вставляет 'cnt' эл-тов 'TP', на которые указывает 'po'
/// в файле 'm_pf' по индексу в файле 'iAt'. Индекс может быть равен к-ву эл-тов в файле,
/// тогда ф-ция добавит новые эл-ты в конец файла как 'put'.
/// \return Вернёт к-во эл-тов в файле, или нуль, если вставка превысит наибольший
/// допустимый размер файла.
///
template <class TP>
int64_t CIntFile<TP>::insert(const int64_t iAt, const TP *po, const int64_t cnt)
{
	ASSERT(!(m_nsz % SZTP) && iAt >= 0 && iAt <= FLCNT);
	return (CBinFile::insert(I2B, po, CNT) / SZTP);
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile::edit редактирует 'cnt' эл-тов 'TP' на которые указывает 'po' в
/// файле 'm_pf' по индексу эл-та в файле 'iAt'. Редактирование может увеличить к-во
/// эл-тов в файле.
/// \param iAt: индекс первого эл-та в файле подлежащий редактированию.
/// \param po: массив эл-тов, подлежащих записи в файл.
/// \param cnt: к-во эл-тов, подлежащих записи в файл.
/// \return к-во записанных в файл эл-тов.
///
template <class TP>
int64_t CIntFile<TP>::edit(const int64_t iAt, const TP *po, const int64_t cnt)
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP) && iAt >= 0 && iAt <= FLCNT);
	return (CBinFile::edit(I2B, po, CNT) / SZTP);
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile::remove удалит указанное к-во эл-тов из файла.
/// \param iAt: индекс первого эл-та в файле, подлежащего удалению.
/// \param cnt: к-во эл-тов в файле, подлежащих удалению.
/// \return к-во удалённых эл-тов.
///
template <class TP>
int64_t CIntFile<TP>::remove(const int64_t iAt, const int64_t cnt)
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP) && iAt >= 0 && iAt <= FLCNT);
	return (CBinFile::remove(I2B, CNT) / SZTP);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile<TP>::get прочтёт указанное к-во эл-тов.
/// \param iAt: индекс эл-та в файле
/// \param po (ИЗ): указатель на хранилище для прочитанных эл-тов достаточное для
/// запрашиваемых эл-тов
/// \param cnt: к-во запрашиваемых эл-тов не должно превышать имеющееся в файле
/// \return к-во прочитанных эл-тов или нуль при попытке читать за пределами файла.
/// Швырнет исключение CCException, если возникла ошибка ввода/вывода.
///
template <class TP>
int64_t CIntFile<TP>::get(const int64_t iAt, TP *po, const int64_t cnt) const // cnt = 1
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP) && iAt >= 0 && iAt + cnt <= FLCNT);
	return (CBinFile::get(I2B, po, CNT) / SZTP);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile::putcp прочтёт указанное к-во байт из файла в текущей позиции.
/// \param iAt: позиция в файле только для контроля правильности вызова.
/// \param po: указатель на массив элементов в который читаю из файла.
/// \param cnt: к-во эл-тов, подлежащих чтению из файла.
/// \return к-во эл-тов прочитанных и записанных в 'po'.
///
template<class TP>
int64_t CIntFile<TP>::getcp(const int64_t iAt, TP *po, const int64_t cnt) const // cnt = 1
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP) && iAt >= 0 && iAt + cnt <= FLCNT);
	return (CBinFile::get(I2B, po, CNT) / SZTP);
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile<TP>::last читает последние 'cnt' эл-тов из файла. Если запрашива-
/// емое к-во больше имеющегося в файле, читаю все эл-ты из файла.
/// \param cnt: к-во эл-тов, подлежащих чтению из файла в массив.
/// \param a: массив, в который помещаю указанное к-во эл-тов из файла.
/// \return к-во прочитанных элементов может быть меньше запрашиваемого.
///
template<class TP>
int64_t CIntFile<TP>::getlast(int64_t cnt, glb::CFxArray<TP, true> &a) const
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP));
	int64_t iAt = FLCNT - cnt;
	if (iAt < 0) {
		cnt += iAt;
		iAt = 0;
	}
	a.SetCountND(cnt);
	return CBinFile::get(I2B, a, CNT);
}

template<class TP>
void CIntFile<TP>::SetCount(int nNewCount)
{
	ASSERT(!(m_nsz % SZTP));
	ASSERT(nNewCount <= FLCNT); // не более. Иначе Edit.
	if (nNewCount < FLCNT)
		remove(nNewCount, FLCNT - nNewCount);
}

/* Find находит эл-т 'o' в файле, эл-ты которого упорядочены по условию, по которому
 * сравнивает ф-ция 'cmp'. Например, вершины упорядочены по времени и ф-ция 'cmp'
 * сравнивает вершины по времени.
 * - o: искомый объект
 * - cmp: ф-ция сравнения возвращает знач-е меньше нуля, если искомый объект меньше
 * объекта в файле, больше нуля, если искомый об-т больше и 0, если объекты равны.
 */
template<class TP>
int64_t fl::CIntFile<TP>::Find(const TP &o, int (*cmp)(const TP&, const TP&)) const
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP));
	int64_t a, i, x, z = count();
	TP o2;
	for (a = 0, i = --z / 2; a <= z; i = (a + z) / 2) {
		Read(i, o2, 1);
		if ((x = cmp(o, o2)) < 0)
			z = i - 1;
		else if(x > 0)
			a = i + 1;
		else
			break;
	}
	return((a > z) ? -1 : i);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIntFile<TP>::UpdateIndexed записываю массив 'a' в файл, синхронизирую индексы эл-тов в
/// файле с индексами эл-тов из массива эл-ты в файле и массиве должны быть упорядочены в соответствии
/// с ф-цией сравнения 'cmp'. Эл-т 'TP' должен иметь переменную CObjID::m_id.
/// \param pa: указатель на записываемый массив.
/// \param na: к-во эл-тов в pa
/// \param cmp: ф-ция сравнения.
///
template<class TP>
int64_t CIntFile<TP>::UpdateIndexed(const TP *pa, const int na, int (*cmp)(const TP&, const TP&))
{
	int64_t i, j;
	if ((i = Find(pa[0], cmp)) < 0 || i >= count()) {
		// если объект не найден в файле, переписываю файл с начала
		SetCount(0);
		i = na;
		if (i > MX_TOPS)
			insert(0, pa + (i - MX_TOPS), MX_TOPS);
		else
			insert(0, pa, na);
	} else {
		int64_t cnt = i + na; // всего - будущий размер
		/*		 t
		 * f. 0123456789
		 * a.    0123456789
		 * x = 0 - 3 = -3;
		 * x1= (3 + 10) / 2 = 6;
		 * x1= 9 - 6 = 3;
		 * x = -3 - 3 = -6
		 *	  ------ +
		 * a:	 3210123456
		 * f: 654
		 */
		TP o;
		Read(i, &o, 1);
		int x = pa[0].m_id.m_iObj - o.m_id.m_iObj,
			x1 = (int) (cnt / 2);
		x1 = pa[na - 1].m_id.m_iObj - x1;
		j = i;
		for (int i = 0, cnt = na; i < cnt; i++, j++) {
			o = pa[i];
			o.m_id.m_iObj -= x1;
			Edit(j, &o, 1);
		}
		for (x -= x1, i -= 1; i >= 0; i--) {
			Read(i, &o, 1);
			o.m_id.m_iObj += x;
			Edit(i, &o, 1);
		}
	}
	return count();
}

template<class TP>
int32_t CIntFile<TP>::count() const
{
	ASSERT(m_nsz >= 0 && !(m_nsz % SZTP));
	return (int32_t) FLCNT;
}

// определения только для эл-тов одинакового размера
#undef SZTP
#undef I2B
#undef CNT

}// namespace fl