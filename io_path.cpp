/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_path.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_path.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_path.h"
#if defined __ITOG || defined __STRFLA
#include "io_strings.h"
#endif
#include <QStandardPaths> // пути, зависимые от операционки
#include "dlgchoosepath.h"
#include "exception.h"
#include <sys/stat.h>	// mkdir
#include "strdef.h"
#include "defs.h"
#include <QRegularExpression>
/*#ifdef Q_OS_ANDROID
#include <QDir>
#endif*/
namespace glb {

extern thread_local CFxString sAppPath;// путь к файлам приложения определяется при запуске
bool DelFile(const char *pcszPName);

}// namespace glb

namespace fl {
extern int nBlockSz; // global.cpp - размер блока для чтения/записи
////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::CreateAppPath создаёт путь к хранилищу приложения: OSDependentDataPath/Линейщикъ/
/// Следует вызывать эту ф-цию однажды, во время запуска приложения до создания первого потока-инструмента
/// т.к. эта ф-ция влияет на глобальную переменную glb::sAppPath, используемую во всех потоках приложения
/// которая не синхронизируется.
/// \param pcszApp: имя приложения
/// \return	-1: первый запуск приложения (создана дирректория с файлами приложения)
///			 0: дирректория с файлам приложения уже имеется, может пустая. Это может быть очередной или первый запуск
///			 1: код ошибки +1 на тот случай, если 'cerror' не знает. Qt-сука умалчивает.
///
int CPath::CreateAppPath(const char *pcszApp)
{
	glb::CvArray<glb::CFxString> as(1);
	const int cnPaths = OSDataPaths(as);
	struct stat st; // POSIX file stat
	glb::CFxString *ps;
	int i = -1, n = -1, iret = 0;
	glb::sAppPath.SetCountZeroND();
	if (cnPaths <= 0)// CreateAppPath вызывается на старте, когда файловый массив strings ещё не открыт
		throw err::CLocalException(S_IE, time(nullptr), "нет системных путей", -1, __FILE__, __LINE__);
#ifdef Q_OS_ANDROID
	// похоже, что в андройде не нужно делать папку для приложения, она делается операционкой по
	// запросу AndroidManifest.xml. Не ожидал от андройда такой любезности... Посмотрим теперь как туда пишется
	for (i = 0; i < cnPaths; i++)
		if (strncmp(as[i], "/storage", 8) == 0)
			break;
	ps = (i < cnPaths) ? &as[i] : &as[0];
	memset(&st, 0, sizeof st);
	stat(*ps, &st);
	ASSERT(st.st_blksize);
	iret = 0;// тут всегда "очередной запуск", папку делать не приходится, буду пользоваться тем что есть.
	goto return_path;
#endif// defined Q_OS_ANDROID
	// __STRFLA определена в CMakeLists.txt
#ifdef __STRFLA
	// с версии 6.5 qt возвращает пути с именем приложения. Заменяю 'StrFileArray' на pcszApp
	auto chldir =[pcszApp](glb::CFxString &s) {
		char *pszPath = s;// null terminated
		if (!pszPath)
			return;
		int i = CPath::LastNames(1, pszPath);
		if (i >= 0 && CPath::CmpNames(pcszApp, pszPath + i)) {
			int32_t n = strlen(pcszApp);
			if (s.GetCount() - i < n)
				s.SetCount(i + n);
			s.Edit(i, pcszApp, n);
			s.SetCountND(i + n);
		}
	};
#endif// defined __STRFLA
	for (i = 0; i < cnPaths; i++) {
		ps = &as[i];
		// не знаю откуда здесь теперь взялось имя приложения в пути. Появилось после запуска отладки android'а.
		// тем не менее, больше имя приложения в путь не добавляю... это из-за обновления qt 6.4 до 6.5
#ifdef __STRFLA
		chldir(*ps);
#endif// defined __STRFLA
//		AddName(*ps, pcszApp);
        memset(&st, 0, sizeof st);
        if (stat(*ps, &st) < 0) {
            n = cerror;
            switch (n) {
			case ENOENT:
				break; // нет папки APP_NAME по пути 's'
            default:
                throw err::CCException(n, S_IE, strerror(n), n, __FILE__, __LINE__);
            }
        } else if (S_ISDIR(st.st_mode))
			goto return_path;
	} // for... нет дирректории APP_NAME по известным путям. Создам такую.
    ps = &as[(cnPaths == 1) ? 0 : UserSelectPath(as)];
    if (mkdir(*ps, S_IFDIR|S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH) < 0) {
		n = cerror;
		throw err::CCException(n, S_IE, time(nullptr), strerror(n), n, __FILE__, __LINE__);
    }
	iret = -1; // дирректория создана впервые
return_path:
    glb::sAppPath.Attach(*ps);
	glb::sAppPath.AddND(SPSEP);
	if (n == ENOENT)
		stat(glb::sAppPath, &st);
	nBlockSz = st.st_blksize;
	return iret;
}

/* CheckMakeDir создам дирректорию dir если не существует.
 */
void CPath::CheckMakeDir(const char *pd)
{
	struct stat st; // POSIX file stat
	memset(&st, 0, sizeof st);
	if (stat(pd, &st) < 0)
		switch (cerror) {
		case ENOENT:
			break; // нет папки 'dir'
		default:
			throw err::CCException(cerror, __FILE__, __LINE__);
		}
	else if (S_ISDIR(st.st_mode))
		return; // есть такая папка
	if (mkdir(pd, S_IFDIR|S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH) < 0)
		throw err::CCException(cerror, __FILE__, __LINE__);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::MakeFileName делаю имя файла из наименования или любого слова
/// \param sNm: какое-то слово или слова для наименования файла
/// \param pcszExtn: завершённый нулём массив символов с расширением, включа точку, как ".txt"
/// \return к-во записанных байт в sNm
///
int CPath::MakeFileName(glb::CFxString &sNm, CCHAR *pcszExtn, int nStrLen)
{
	if (CheckFileName(sNm).GetCount() > 0)
		sNm.AddND(pcszExtn, nStrLen);
	return sNm.GetCount();
}

int CPath::MakeFileName(BYTES &sNm, CBYTE *pcszExtn, int nBytes)
{
	if (CheckFileName(sNm).GetCount() > 0)
		sNm.AddND(pcszExtn, nBytes);
	return sNm.GetCount();
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::FileExist определяет наличие файла в папке хранилища приложения.
/// \param pcszDir: имя дирректории в папке хранилища glb::sAppPath
/// \param pcszFile: имя файла
/// \return размер файла в байтах, если такой файл есть, или нуль. Если файл пустой,
/// всё равно, что его нет.
///
size_t CPath::FileExist(const char *pcszDir, const char *pcszFile)
{
	ASSERT(glb::sAppPath.GetCount());
	glb::CFxString s(glb::sAppPath);
	AddName(AddName(s, pcszDir), pcszFile);
	struct stat st; // POSIX file stat
	memset(&st, 0, sizeof st);
	if (stat(s, &st) < 0)
		switch (cerror) {
		case ENOENT:
			break; // нет папки 'dir'
		default:
			throw err::CCException(cerror, __FILE__, __LINE__);
		}
	else if (S_ISDIR(st.st_mode))
		return 0; // есть такая папка
	return st.st_size;
}

size_t CPath::FileExist(const char *pcszPathName)
{
	struct stat st; // POSIX file stat
	memset(&st, 0, sizeof st);
	if (stat(pcszPathName, &st) < 0)
		switch (cerror) {
		case ENOENT:
			break; // нет папки 'dir'
		default:
			throw err::CCException(cerror, __FILE__, __LINE__);
		}
	else if (S_ISDIR(st.st_mode))
		return 0; // есть такая папка
	return st.st_size;
}

void CPath::FileSetNull(const char *pcszPathName)
{
	cerror = 0;
	FILE *pf = fopen(pcszPathName, "w");
	if (!pf)
		throw err::CCException(cerror, __FILE__, __LINE__);
	fclose(pf);
}

/* GetOSDataPaths получает массив строк - путей к хранилищам для приложений,
 * определённых в операционке.
 */
int CPath::OSDataPaths(glb::CvArray<glb::CFxString> &a)
{
	QStringList paths { QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation) };//QStandardPaths::GenericDataLocation) };
	glb::CFxString *ps = nullptr;
	QString *pqs = nullptr;
	const int cnt = paths.count();
	a.SetCount(cnt);
	for (int i = 0; i < cnt; i++) {
		ps = &a[i];
		if (ps->GetCount())
			ps->SetCountZeroND();
		pqs = &paths[i];
        ps->Add(qPrintable(*pqs), pqs->length());
	}
	return cnt;
}

/* UserSelectPath: выводит диалог со списком путей для выбора одного из них,
 * где будут храниться все сведения приложения.
 * Возвращает индекс пути в 'as' независимо от того, как завершился диалог.
 */
int fl::CPath::UserSelectPath(const glb::CvArray<glb::CFxString> &as)
{
	ui::CDlgChoosePath dlg(nullptr, as);
	dlg.exec();
	return dlg.m_iSelected;
}

/*
 * AddName добавляет в путь 's' имя 'pcszName', отделяя его разделителем если нужно.
 * - ac: результат ф-ции this->GetAppPath
 */
glb::CFxString& CPath::AddName(glb::CFxString &spath, const char *pcszName, int nNmLen)
{
	ASSERT((const char*) spath != nullptr);
	while (*pcszName == CPSEP || isspace(*pcszName))
		pcszName++;
	if (spath.Last() != CPSEP)
		spath.AddND(SPSEP);
	spath.AddND(pcszName, nNmLen);
	return spath;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::ArrayPath собирает путь к файлу объектов, вложенному в период.
/// предпочтительный вариант вызова: glb::CFxString s(ArrayPath(s, i, p, a)); // move
/// \param pcszServer: сервер - real или pactice
/// \param pcszInstr: имя инструмента
/// \param pcszPeriod: имя периода
/// \param pcszNm: имя файла, которому через разделитель может предшествовать имя папки.
/// \return возвращаю путь к файлу
///
glb::CFxString CPath::ObjsFlPath(const char *pcszServer, const char *pcszInstr, const char *pcszPeriod, const char *pcszNm)
{
    ASSERT(glb::sAppPath.GetCount());
    glb::CFxString s(glb::sAppPath);
	return AddName(AddName(AddName(AddName(s, pcszServer), pcszInstr), pcszPeriod), pcszNm);
}

glb::CFxString CPath::PeriodPath(const char *pcszServer, const char *pcszInstr, const char *pcszPeriod)
{
    ASSERT(glb::sAppPath.GetCount());
    glb::CFxString s(glb::sAppPath);
	return AddName(AddName(AddName(s, pcszServer), pcszInstr), pcszPeriod);
}

glb::CFxString CPath::InstrPath(const char *pcszServer, const char *pcszInstr)
{
    ASSERT(glb::sAppPath.GetCount());
    glb::CFxString s(glb::sAppPath);
	return AddName(AddName(s, pcszServer), pcszInstr);
}

glb::CFxString CPath::ServerPath(const char *pcszServer)
{
    ASSERT(glb::sAppPath.GetCount());
    glb::CFxString s(glb::sAppPath);
	return AddName(s, pcszServer);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::LastNames находит последние nNames имени в пути
/// \param nNames: к-во имён (файла/папок)
/// \param pcszPath: путь
/// \param nPathLen: длина пути
/// \return индекс первого символа имени
///
int32_t CPath::LastNames(const int32_t nNames, const char *pcszPath, int32_t nPathLen) // nPathLen = -1
{
	ASSERT(strlen(pcszPath));
	int32_t i, j, k;
	if (nPathLen < 0)
		nPathLen = strlen(pcszPath);
	for (i = nPathLen - 1; isspace(pcszPath[i]) || pcszPath[i] == CPSEP; i--)
		;
	for (j = -1, k = 0; i >= 0 && k < nNames; i--)
		if (pcszPath[i] == CPSEP) {
			k++;
			j = i + 1;
		}
	return j;
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::CmpNames сравнивает два наименования до первого разделителя
/// \param ps1: указывает на первый символ первой строки завершённой нулём
/// \param ps2: указывает на первый символ второй строки завершённой нулём
/// \return <0 0 или >0
///
int32_t CPath::CmpNames(const char *ps1, const char *ps2)
{
	int32_t i = 0;
	while (ps1[i] && ps2[i] && ps1[i] != CPSEP && ps2[i] != CPSEP && ps1[i] == ps2[i])
		i++;
	if (!ps1[i] && ps2[i] == CPSEP ||
		!ps2[i] && ps1[i] == CPSEP)
		return 0;
	return (ps1[i] - ps2[i]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::CheckFileName
/// \param s: имя без расширения
/// \return
///
QString& CPath::CheckFileName(QString &s)
{// модельеры не пропускают в набор символов эти 2: [] и вероятно эти: +)( хоть escape их, хоть убейся...
	QRegularExpression re;
	re.setPattern("[{}/\\|]");
	s.replace(re, "-");
	re.setPattern("[=&^%$#@!?:\"\';,.\r\b\t\f\n]«»");
	return s.remove(re);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::CheckFileName удалит некоторые символы из наименования. Вызывается перед отправкой
/// на сервер. Удаляемые символы: ()+=&^%$#@!?:\"\';,.\r\b\t\f\n
/// \param s: имя без расширения
/// \return
///
glb::CFxString& CPath::CheckFileName(glb::CFxString &s)
{
	ASSERT(!s.GetCount() || s.Last() == '\0');
	int32_t c = s.GetCount();
	char *pcsz = s, *psz = pcsz;
	if (!c)
		return s;
	while ((psz = strpbrk(psz, " []{}/\\|")) != nullptr)
		*psz = '-';
	psz = pcsz;
	// только 1-байтовые символы
	while (c-- > 0 && (psz = strpbrk(psz, "()+=&^%$#@!?:\"\';,.\r\b\t\f\n")) != nullptr)
		s.RemND(psz - pcsz, 1);
	return s;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPath::CheckFileName: удалит некоторые символы из строки-наименования файла.
/// Удаляемые символы: ()+=&^%$#@!?:\"\';,.\r\b\t\f\n
/// \param s: наименование без расширения
/// \return
///
BYTES& CPath::CheckFileName(BYTES &s)
{
	int32_t c = s.GetCount();
	char *pcsz, *psz;
	if (!c)
		return s;
	s.AddND('\0');
	pcsz = (char*) (BYTE*) s;
	psz = pcsz;
	while ((psz = strpbrk(psz, " []{}/\\|")) != nullptr)
		*psz = '-';
	psz = pcsz;
	// только 1-байтовые символы
	while (c-- > 0 && (psz = strpbrk(psz, "[]{}()+=&^%$#@!?:\"\';,.\r\b\t\f\n")) != nullptr)
		s.RemND(psz - pcsz, 1);
	s.SetCountND(s.GetCount() - 1); // '\0'
	return s;
}

#if defined __ITOG || defined __STRFLA

extern CStrings strs;

void CPath::RmFile(int32_t iDrNmID, int32_t iFlNmID)
{
	CFileItemObtainer<CStrings, CString> os(fl::strs);
	fl::CString &s = os.GetItem(fl::ioffset, iFlNmID);
	if (s.m_nref <= 1) {
		CFileItemObtainer<CStrings, CString> os1(fl::strs);
		fl::CString &s1 = os1.GetItem(fl::ioffset, iDrNmID);
		glb::CFxString sPath(glb::sAppPath);
		fl::CPath::AddName(fl::CPath::AddName(sPath, s1.str()), s.str());
		glb::DelFile(sPath);
	}
}

#endif

} // namespace fl