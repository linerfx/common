#pragma once
#ifdef __llvm__
#pragma clang diagnostic ignored "-Wdangling-else"
#pragma clang diagnostic ignored "-Wlogical-op-parentheses"
#endif
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FxVArray.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FxVArray.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxArray.h"

namespace glb {

////////////////////////////////////////////////////////////////////////////////////
/// \brief The CvArray class массив объектов динамического размера наследует
/// CFxArray<TP, false>, чтобы переопределить ф-ции чтения/записи объекта в/из массива
/// байт для файловых операций.
///
template <class TP>
class CvArray : public CFxArray<TP, false>
{
public:
	typedef CFxArray<TP, false> BAS;
	typedef CvArray<TP> THIS;
public:
	CvArray();
	CvArray(const CvArray &a);	// копирования - тоже д.массивов
	CvArray(CvArray &&a);		// перемещение (move constructor) C++11
	CvArray(int iGrow);			// дежурный, индивидуальнй подход
	CvArray(const TP *pt, int cnt = 1);
	virtual ~CvArray();
public:
	const THIS& operator =(const THIS &o);
	THIS& operator =(THIS &&o);
	virtual int GetBinData(BYTES &a) const override;
	virtual int32_t SetBinData(CBYTE *p, int n) override;
};

template<class TP>
CvArray<TP>::CvArray()
{

}

template<class TP>
CvArray<TP>::CvArray(const CvArray &a)
	: BAS(a)
{

}

/*template<class TP>
CvArray<TP>::CvArray(CvArray &a)
	: BAS(a)
{

}*/

template<class TP>
CvArray<TP>::CvArray(CvArray &&a)
	: BAS(a)
{

}

template<class TP>
CvArray<TP>::CvArray(int iGrow)
	: BAS(iGrow)
{

}

template<class TP>
CvArray<TP>::CvArray(const TP *pt, int cnt)
	: BAS(pt, cnt)
{

}

template<class TP>
CvArray<TP>::~CvArray()
{

}

template <class TP>
int CvArray<TP>::GetBinData(BYTES &a) const
{
	int32_t i, cnt = sizeof BAS::m_iCount;
	a.AddND((CBYTE*) &(BAS::m_iCount), cnt);
	for (i = 0; i < BAS::m_iCount; i++)
		cnt += BAS::m_pData[i].GetBinData(a);
	return cnt;
}

template <class TP>
int32_t CvArray<TP>::SetBinData(CBYTE *cpb, int cnt)
{
	int32_t i, nCount;
	int32_t n = read_tp(nCount, cpb, cnt);
	if (nCount < 0)
		throw err::CLocalException(SI_NED, time(NULL), FileName(__FILE__, nullptr), __LINE__);
	BAS::SetCountND(nCount);
	for (i = 0; i < nCount; i++)
		n += BAS::m_pData[i].SetBinData(cpb + n, cnt - n);
	return n;
}

template<class TP>
const CvArray<TP>& CvArray<TP>::operator =(const CvArray<TP> &o)
{
	BAS::SetCount(o.m_iCount);
	for (int i = 0; i < BAS::m_iCount; i++)
		BAS::m_pData[i] = o.m_pData[i];
	return *this;
}

template<class TP>
CvArray<TP>& CvArray<TP>::operator =(CvArray<TP> &&o)
{
	BAS::Attach(o);
	return *this;
}

}// namespace glb