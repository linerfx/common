#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл gcolour.h — часть программы iTogClient. iTogClient свободная
 * программа, вы можете перераспространять её и/или изменять на условиях
 * Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient распространяется в надежде, что она будут полезной, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * gcolour.h is part of iTogClient. iTogClient programm is free software: you
 * can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * iTogClient is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxArray.h"
#include <stdint.h>
#include <float.h>

namespace glb {
namespace c {

class CColour;
const CColour TxtBG(bool bActive);
const CColour Border(bool bActive);

struct CClr {
	enum Clr {
		nn = 0,				// значения всех 3-х цветов равны
		be = 0x000000ff,	// наибольший цвет синий
		gn = 0x0000ff00,	// наибольший цвет зелёный
		rd = 0x00ff0000		// наибольший цвет красный
	} clr;
	uint32_t c;				// значение цвета в нижнем байте
	CClr() {
		clr = nn;
		c = 0;
	}
};

struct CColour : public CFxArrayIndItem
{
	enum {
		Dif, // сравнение по разнице (меньше разница, меньше цветность)
		Clr, // сравнение по цветам
		Cnt  // к-во индексов
	};
	uint32_t c1, c2;
	// обязательное для массива
	CColour();
	CColour(uint32_t a, uint32_t b);
	bool IsValid() const override;
	void Invalidate() override;
	uint32_t Lightest() const;
	static CClr Mx(uint32_t &c, bool rem);
	static CClr* Srt(CClr ac[3], uint32_t c);
	static float Light(uint32_t c);
	static float Red(uint32_t c);
	static float Grn(uint32_t c);
	static float Ble(uint32_t c);
	static float dif(uint32_t c);
	static uint32_t Invert(uint32_t c);
	static int cmp(const CColour &a, const CColour &b);
	static int cmpd(const CColour &a, const CColour &b);
	typedef int (*fcmp)(const CColour&, const CColour&);
	static const CColour& Nearest(uint32_t colour);
};

inline CColour::CColour()
{
	c1 = c2 = 0;
}

inline CColour::CColour(uint32_t a, uint32_t b)
{
	c1 = a;
	c2 = b;
}

inline bool CColour::IsValid() const
{
	return (c1 != 0 || c2 != 0);
}

inline void CColour::Invalidate()
{
	c1 = c2 = 0;
}

////////////////////////////////////////////////////////////////////////
/// \brief CColour::Lightest
/// \return наиболее светлый из двух
///
inline uint32_t CColour::Lightest() const
{
	uint32_t r1 = (c1 & 0x00ff0000) >> 16 & 0x000000ff,
	        g1 = (c1 & 0x0000ff00) >> 8 & 0x000000ff,
	        b1 = c1 & 0x000000ff,
	        r2 = (c2 & 0x00ff0000) >> 16 & 0x000000ff,
	        g2 = (c2 & 0x0000ff00) >> 8 & 0x000000ff,
	        b2 = c2 & 0x000000ff;
	float f1 = (r1 + g1 + b1) / 3.f,
	        f2 = (r2 + g2 + b2) / 3.f;
	return (DCMP_MORE(f1, f2, FLT_EPSILON) ? c1 : c2);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief CColour::Light яркость в диапазоне 0.f - 255.f
/// \param c
/// \return
///
inline float CColour::Light(uint32_t c)
{
	uint32_t r = (c & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (c & 0x0000ff00) >> 8 & 0x000000ff,
	        b = c & 0x000000ff;
	return ((r + g + b) / 3.f);
}

///////////////////////////////////////////////////////////////////////////
/// \brief CColour::Red сколько красного относительно зелёного и синего?
/// \param c: цвет
/// \return к-во красного относительно двух других в процентах
///
inline float CColour::Red(uint32_t c)
{
	uint32_t r = (c & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (c & 0x0000ff00) >> 8 & 0x000000ff,
	        b = c & 0x000000ff;
	return ((float) r / (g + b) * 100.f);
}

///////////////////////////////////////////////////////////////////////////
/// \brief CColour::Grn сколько зелёного в цвете?
/// \param c: цвет
/// \return к-во зелёного относительно двух других в процентах
///
inline float CColour::Grn(uint32_t c)
{
	uint32_t r = (c & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (c & 0x0000ff00) >> 8 & 0x000000ff,
	        b = c & 0x000000ff;
	return ((float) g / (r + b) * 100.f);
}

//////////////////////////////////////////////////////////////////////////////
/// \brief CColour::Ble сколько синего в цвете?
/// \param c: цвет
/// \return к-во синего в цвете относительно двух других в процентах
///
inline float CColour::Ble(uint32_t c)
{
	uint32_t r = (c & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (c & 0x0000ff00) >> 8 & 0x000000ff,
	        b = c & 0x000000ff;
	return ((float) b / (r + g) * 100.f);
}

// чем более цвета разнятся, тем более ярко выраженный цвет
// чем меньше цвета разнятся, тем более нейтральный (серый) цвет
inline float CColour::dif(uint32_t c)
{
	uint32_t r = (c & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (c & 0x0000ff00) >> 8 & 0x000000ff,
	        b = c & 0x000000ff;
	return (((float) ABS(r - g) + ABS(r - b) + ABS(g - b)) / 3.f);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CGrayColours class наиболее нейтральные цвета с наименьшей цветностью из имеющихся
/// предполагается для выбора тёмного или светлого фона
///
class CGrayColours
{
public:
	CGrayColours();
	static const CColour& Nearest(uint32_t c);
};

// https://webgradients.com/
#define GCOLORS { \
    { 0xff9a9e, 0xfad0c4 }, \
    { 0xa18cd1, 0xfbc2eb }, \
    { 0xfad0c4, 0xffd1ff }, \
    { 0xffecd2, 0xfcb69f }, \
    { 0xff9a9e, 0xfecfef }, \
    { 0xf6d365, 0xfda085 }, \
    { 0xfbc2eb, 0xa6c1ee }, \
    { 0xfdcbf1, 0xe6dee9 }, \
    { 0xa1c4fd, 0xc2e9fb }, \
    { 0xd4fc79, 0x96e6a1 }, \
    { 0x84fab0, 0x8fd3f4 }, \
    { 0xcfd9df, 0xe2ebf0 }, \
    { 0xa6c0fe, 0xf68084 }, \
    { 0xfccb90, 0xd57eeb }, \
    { 0xe0c3fc, 0x8ec5fc }, \
    { 0xf093fb, 0xf5576c }, \
    { 0xfdfbfb, 0xebedee }, \
    { 0x4facfe, 0x00f2fe }, \
    { 0x43e97b, 0x38f9d7 }, \
    { 0xfa709a, 0xfee140 }, \
    { 0x30cfd0, 0x330867 }, \
    { 0xa8edea, 0xfed6e3 }, \
    { 0x5ee7df, 0xb490ca }, \
    { 0xd299c2, 0xfef9d7 }, \
    { 0xf5f7fa, 0xc3cfe2 }, \
    { 0x667eea, 0x764ba2 }, \
    { 0xfdfcfb, 0xe2d1c3 }, \
    { 0x89f7fe, 0x66a6ff }, \
    { 0xfddb92, 0xd1fdff }, \
    { 0x9890e3, 0xb1f4cf }, \
    { 0xebc0fd, 0xd9ded8 }, \
    { 0x96fbc4, 0xf9f586 }, \
    { 0x2af598, 0x009efd }, \
    { 0xcd9cf2, 0xf6f3ff }, \
    { 0x6a11cb, 0x2575fc }, \
    { 0x37ecba, 0x72afd3 }, \
    { 0xebbba7, 0xcfc7f8 }, \
    { 0xfff1eb, 0xace0f9 }, \
    { 0xc471f5, 0xfa71cd }, \
    { 0x48c6ef, 0x6f86d6 }, \
    { 0xfeada6, 0xf5efef }, \
    { 0xe6e9f0, 0xeef1f5 }, \
    { 0xaccbee, 0xe7f0fd }, \
    { 0xe9defa, 0xfbfcdb }, \
    { 0xc1dfc4, 0xdeecdd }, \
    { 0x0ba360, 0x3cba92 }, \
    { 0x00c6fb, 0x005bea }, \
    { 0x74ebd5, 0x9face6 }, \
    { 0x6a85b6, 0xbac8e0 }, \
    { 0xa3bded, 0x6991c7 }, \
    { 0x9795f0, 0xfbc8d4 }, \
    { 0xa7a6cb, 0x8989ba }, \
    { 0xf43b47, 0x453a94 }, \
    { 0x0250c5, 0xd43f8d }, \
    { 0x88d3ce, 0x6e45e2 }, \
    { 0xd9afd9, 0x97d9e1 }, \
    { 0x7028e4, 0xe5b2ca }, \
    { 0x13547a, 0x80d0c7 }, \
    { 0xff0844, 0xffb199 }, \
    { 0x93a5cf, 0xe4efe9 }, \
    { 0x434343, 0x000000 }, \
    { 0x93a5cf, 0xe4efe9 }, \
    { 0x92fe9d, 0x00c9ff }, \
    { 0xff758c, 0xff7eb3 }, \
    { 0x868f96, 0x596164 }, \
    { 0xc79081, 0xdfa579 }, \
    { 0x8baaaa, 0xae8b9c }, \
    { 0xf83600, 0xf9d423 }, \
    { 0xb721ff, 0x21d4fd }, \
    { 0x6e45e2, 0x88d3ce }, \
    { 0xd558c8, 0x24d292 }, \
    { 0xabecd6, 0xfbed96 }, \
    { 0x5f72bd, 0x9b23ea }, \
    { 0x09203f, 0x537895 }, \
    { 0xddd6f3, 0xfaaca8 }, \
    { 0xdcb0ed, 0x99c99c }, \
    { 0xf3e7e9, 0xe3eeff }, \
    { 0xc71d6f, 0xd09693 }, \
    { 0x96deda, 0x50c9c3 }, \
    { 0xf77062, 0xfe5196 }, \
    { 0xa8caba, 0x5d4157 }, \
    { 0x29323c, 0x485563 }, \
    { 0x16a085, 0xf4d03f }, \
    { 0xff5858, 0xf09819 }, \
    { 0x2b5876, 0x4e4376 }, \
    { 0x00cdac, 0x8ddad5 }, \
    { 0x4481eb, 0x04befe }, \
    { 0xdad4ec, 0xf3e7e9 }, \
    { 0x874da2, 0xc43a30 }, \
    { 0x4481eb, 0x04befe }, \
    { 0xe8198b, 0xc7eafd }, \
    { 0xf794a4, 0xfdd6bd }, \
    { 0x64b3f4, 0xc2e59c }, \
    { 0x0fd850, 0xf9f047 }, \
    { 0xee9ca7, 0xffdde1 }, \
    { 0x209cff, 0x68e0cf }, \
    { 0xbdc2e8, 0xe6dee9 }, \
    { 0xe6b980, 0xeacda3 }, \
    { 0x1e3c72, 0x2a5298 }, \
    { 0x9be15d, 0x00e3ae }, \
    { 0xed6ea0, 0xec8c69 }, \
    { 0xffc3a0, 0xffafbd }, \
    { 0xcc208e, 0x6713d2 }, \
    { 0xb3ffab, 0x12fff7 }, \
    { 0x243949, 0x517fa4 }, \
    { 0xfc6076, 0xff9a44 }, \
    { 0xdfe9f3, 0xffffff }, \
    { 0x00dbde, 0xfc00ff }, \
    { 0xf9d423, 0xff4e50 }, \
    { 0x50cc7f, 0xf5d100 }, \
    { 0x0acffe, 0x495aff }, \
    { 0x616161, 0x9bc5c3 }, \
    { 0xdf89b5, 0xbfd9fe }, \
    { 0xed6ea0, 0xec8c69 }, \
    { 0xd7d2cc, 0x304352 }, \
    { 0xe14fad, 0xf9d423 }, \
    { 0xb224ef, 0x7579ff }, \
    { 0xc1c161, 0xd4d4b1 }, \
    { 0xec77ab, 0x7873f5 }, \
    { 0x007adf, 0x00ecbc }, \
    { 0x20E2D7, 0xF9FEA5 }, \
    { 0xA8BFFF, 0x884D80 }, \
    { 0xB6CEE8, 0xF578DC }, \
    { 0xFFFEFF, 0xD7FFFE }, \
    { 0xE3FDF5, 0xFFE6FA }, \
    { 0x7DE2FC, 0xB9B6E5 }, \
    { 0xCBBACC, 0x2580B3 }, \
    { 0xB7F8DB, 0x50A7C2 }, \
    { 0x007adf, 0x00ecbc } }
    
}// namespace c
}// namespace glb
