﻿/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл TimeDiff.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * TimeDiff.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "TimeDiff.h"
//#include <math.h>	// pow
//#include "defs.h"

/*#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*CTimeDiff::CTimeDiff()
{
//	ASSERT(sizeof(FILETIME) == sizeof(unsigned __int64));
//	GetSystemTimeAsFileTime((FILETIME*) &m_tmStart);
}*/

namespace glb {

CTimeDiff::~CTimeDiff()
{

}

}// namespace glb
/*	часть секунды:
	0,000000001	наносекунда (pow(10.0, -9.0))
	0,0000001	интервал значения Win.FileTime (100 наносекунд)
	0,001		миллисекунда (pow(10.0, -3.0)) - интервал, возвращаемый GetElapsed в других версиях
	1,0			секунда - результат этой ф-ции
*/
/*double CTimeDiff::GetElapsed() const
{
	ASSERT(sizeof(FILETIME) == sizeof(unsigned __int64));
	unsigned __int64 nTime;
	GetSystemTimeAsFileTime((FILETIME*) &nTime);
	return ((double) (nTime - m_tmStart) / pow(10.0, 7));
}*/

/* Now вернёт "точку во времени", используя дикий (стандартный) С++.
 */
/*std::chrono::system_clock::time_point CTimeDiff::Now() const
{
	using namespace std::chrono;
	time_point<system_clock, milliseconds> ms;
	system_clock::time_point tpms(ms);
	tpms = system_clock::now();
	return tpms;
}*/

/*void CTimeDiff::Initialize()
{
	ASSERT(sizeof(FILETIME) == sizeof(unsigned __int64));
	GetSystemTimeAsFileTime((FILETIME*) &m_tmStart);
}*/
