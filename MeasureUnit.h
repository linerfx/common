#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл MeasureUnit.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * MeasureUnit.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <cfloat>// DBL_EPSILON
#include "defs.h"
#include "strdef.h"
//#include <cstring>
//#include "FxArray.h"

namespace store { // единица измерения

// размер полезных сведений объекта для чтения/записи из/в файл
#define SZ_MU (sizeof(double) + sizeof(int32_t) * 4)
///////////////////////////////////////////////////////////////////
/// \brief The CMu struct е.и. для расхода товара содержит к-во грамм этой е.и.
/// Например, унция равна 28,349523125 гр.
/// Объект годится для массива, не содержит вирт.ф-цый
///
struct CMu {
	enum EIndices {
		iInm,	// сравниваются индексы строк, содержащих наименования
		iISnm,	// сравниваются индексы строк, содержащих сокращённые наименования
		iVal,	// сравниваются значения
		nMuInds
	};
	double m_fGrams;	// к-во грамм этой е.и.
	int32_t m_iNm,		// ид-р строки с наименованием е.и.
		m_iShrtNm;		// ид-р строки с коротким наименованием е.и.
	CMu();
	CMu(const CMu&);
	CMu(double fgr, int32_t inm, int32_t isnm);
	void Set(double fgr, int32_t inm, int32_t isnm, uint32_t flags = 0);
	CMu& Init();
	double ConvWeight(double fcnt, const CMu &to) const;
	double ConvPrice(double fcnt, const CMu &to) const;
	bool ByPiece() const;
	// ф-ции для массива объектов переменной длины или с вирт.ф-циями glb::CvArray тут не требуются
	const CMu& operator =(const CMu &o);
	bool IsValid() const;	// эл-т не действительный, подлежит замене
	int Invalidate();		// делает эл-т не действительным, подлежащим замене
	bool IsEmpty() const;	// эл-т пустой, никогда не был действительным
	bool operator ==(const CMu &mu) const;
	bool operator !=(const CMu &mu) const;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief CMu::convert привожу указанное к-во этого товара к др.единице измерения.
/// \param this_cnt: к-во товара в этой е.и.
/// \param to: е.и. к которой приводится указанное к-во в этой е.и.
/// \return к-во в е.и. 'to'.
///
inline void CMu::Set(double fgr, int32_t inm, int32_t isnm, uint32_t flags) // flags = 0
{
	m_iNm = inm;
	m_iShrtNm = isnm;
	m_fGrams = fgr;
}

inline CMu& CMu::Init()
{
	m_iNm = m_iShrtNm = INV32_BIND;
	m_fGrams = .0;
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CMu::convWeiht привожу вес в этой е.и. к другой
/// \param fcnt: вес в этой е.и.
/// \param to: е.и. к которой приводится значение fcnt
/// \return вес в е.и. 'to'
/// вес 0,8 кг в граммы (800)
/// (1000 * 0,8 / 1) = 800
/// вес 800 гр в кг (0,8)
/// 1 * 800 / 1000 = ,8
///
inline double CMu::ConvWeight(double fcnt, const CMu &to) const
{	// штучный товар не конвертируется
	ASSERT(!DCMP_EQUAL(m_fGrams, .0, DBL_EPSILON) && !DCMP_EQUAL(to.m_fGrams, .0, DBL_EPSILON));
	ASSERT(IsValid() && to.IsValid() && !ByPiece() && !to.ByPiece());
	return (m_fGrams * fcnt / to.m_fGrams);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMu::ConvPrice привожу цену в одной е.и. к другой. Например, цену за кг. к цене за грамм
/// \param fPrc: цена за эту е.и.
/// \param to: е.и. к которой приводится цена
/// \return цена за е.и. 'to'
/// цена 500 за л. - в цену за 1 мл.
/// 1 * 500 / 1000 = 0,5
/// цена ,5 за мл. - в цену за 1 л.
/// 1000 * ,5 / 1 = 500
///
inline double CMu::ConvPrice(double fPrc, const CMu &to) const
{	// штучный товар не конвертируется
	ASSERT(!DCMP_EQUAL(m_fGrams, .0, DBL_EPSILON) && !DCMP_EQUAL(to.m_fGrams, .0, DBL_EPSILON));
	ASSERT(IsValid() && to.IsValid() && !ByPiece() && !to.ByPiece());
	return (to.m_fGrams * fPrc / m_fGrams);
}

inline bool CMu::ByPiece() const
{
	return (m_iNm == SI_PGF && m_iShrtNm == SI_PG);
}

inline bool CMu::IsValid() const
{
	return (/*!(m_flags & ITM_DEL) && m_iNm != INV32_BIND && m_iShrtNm != INV32_BIND &&*/
			(m_iNm == SI_PGF && m_iShrtNm == SI_PG || !DCMP_EQUAL(m_fGrams, .0, DBL_EPSILON)));
}

/*inline int CMu::Invalidate()
{
	m_flags |= ITM_DEL;
	return 0;
}*/

inline bool CMu::IsEmpty() const
{
	return (m_iNm == INV32_BIND && m_iShrtNm == INV32_BIND && /*!m_flags && */DCMP_EQUAL(m_fGrams, .0, DBL_EPSILON));
}

inline bool CMu::operator ==(const CMu &mu) const
{
	return (m_iNm == mu.m_iNm && m_iShrtNm == mu.m_iShrtNm && DCMP_EQUAL(m_fGrams, mu.m_fGrams, .001));
}

inline bool CMu::operator !=(const CMu &mu) const
{
	return (m_iNm != mu.m_iNm || m_iShrtNm != mu.m_iShrtNm || !DCMP_EQUAL(m_fGrams, mu.m_fGrams, .001));
}

};// namespace mu

