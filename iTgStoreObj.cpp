/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgStoreObj.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgStoreObj.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "iTgStoreObj.h"
#include "iTgDefs.h"
#ifdef __ITOG
#include "io_strings.h"
#include "product.h"

namespace fl {
extern CStrings strs;
}// namespace fl
namespace store {
extern CProducts prdcts;
}// namespace store
#endif // defined __ITOG
namespace  glb {
	const char* FileName(const char *pcszPath, int *pnLen);
}
namespace o {
#if defined(__ITOG)
extern CiTgOpts opt;
#endif
}
namespace store {

int CStaple::GetBinData(BYTES &a) const
{
	int32_t n = BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_fTara, SZ_STPL);
	return (n + SZ_STPL);
}

int CStaple::SetBinData(CBYTE *cpb, int cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_fTara, SZ_STPL, cpb + i, cnt - i));
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::cmp_cat сравниваю по 4-м категориям товаров (первая основная и подкатегории)
/// То же o::CiTgOpts::cmp_cat(int,int)
/// \param a, z: сравниваемые товары
/// \return -1 | 0 | 1
///
int CStaple::cmp_cat(const CStaple &a, const CStaple &z)
{	// четыре категории товаров (основная и 3 подкатегории)
	for (int i = 0; i < 32; i += 8)
		if ((a.m_nc & SC_1 << i) < (z.m_nc & SC_1 << i))
			return -1;
		else if ((a.m_nc & SC_1 << i) > (z.m_nc & SC_1 << i))
			return 1;
	return 0; // товары одной категории
}

int CResidue::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	int32_t n = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.AddND((CBYTE*) &m_fRes, SZ_RESIDUE);
	return (a.GetCount() - n);
}

int CResidue::SetBinData(CBYTE *cpb, const int32_t cnt)
{
	int32_t i = fl::CFileItem::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_fRes, SZ_RESIDUE, cpb + i, cnt - 1));
}

int CIngredient::GetBinData(BYTES &a) const
{
	ASSERT((m_flags & ITM_MNMS) == ITM_STPL || (m_flags & ITM_MNMS) == ITM_PRDCT);
	ASSERT(m_iSource >= 0);
	int32_t n = a.GetCount();
	BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_fLoad, SZ_STPLEX);
	return (a.GetCount() - n);
}

int CIngredient::SetBinData(CBYTE *cpb, int cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_fLoad, SZ_STPLEX, cpb + i, cnt - i);
	ASSERT((m_flags & ITM_MNMS) == ITM_STPL || (m_flags & ITM_MNMS) == ITM_PRDCT);
	ASSERT(m_iSource >= 0);
	return i;
}

const CResidue& CResidue::operator =(const CResidue &o)
{
	fl::CFileItem::operator =(o);
	memcpy((void*) &m_fRes, &o, SZ_RESIDUE);
	return *this;
}

#ifdef __ITOG
int32_t CIngredient::Invalidate(const int32_t iObj, bool bRmFrmPrnt)
{
	return BAS::Invalidate(iObj, bRmFrmPrnt);
}
#endif

CProduct::CProduct()
	: BAS(product)
	, m_fLoad(.0)
	, m_fRnd(1.f)
	, m_iMu(INV32_BIND)
{
#if defined(__ITOG) || defined(__STRFLA)
	m_fRnd = o::opt.m_fRndTo;
#endif
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::GetBinData записывает содержимое объекта в массив для хранения
/// в файле.
/// \param a: массив, в который добавляется содержимое этого объекта.
/// \return к-во записанных в 'a' байт.
/// Здесь важно, чтобы m_fLoad был первым эл-том объекта.
///
int32_t CProduct::GetBinData(BYTES &a) const
{
	ASSERT(IsValid());
	int n = BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_fLoad, SZ_PRODUCT);
	return (n + SZ_PRODUCT);// + m_aAddAlt.GetBinData(a));
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::SetBinData копирует из pb в объект ранее сохранённые ф-цией
/// GetBinData сведения.
/// \param cpb: массив, из которого восстанавливается этот объект
/// \param cnt: размер массива 'pb' в байтах.
/// \return к-во байт прочитаннных в 'pb'.
/// Швырнет исключение, если размер 'pb' не соответствует ожиданиям.
/// Здесь важно, чтобы m_fLoad был первым эл-том объекта.
///
int32_t CProduct::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_fLoad, SZ_PRODUCT, cpb + i, cnt - i));
//	return (i + m_aAddAlt.SetBinData(cpb + i, cnt - i));
}

int CIncome::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	int32_t n = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.AddND((CBYTE*) &m_fQtt, SZ_INCOME);
	return (a.GetCount() - n);
}

int CIncome::SetBinData(CBYTE *cpb, int cnt)
{
	int i = fl::CFileItem::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_fQtt, SZ_INCOME, cpb + i, cnt - i));
}

int CExpend::GetBinData(BYTES &a) const
{
	int32_t n = a.GetCount();
	BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_fQtt, SZ_EXPEND);
	return (a.GetCount() - n);
}

int CExpend::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_fQtt, SZ_EXPEND, cpb + i, cnt - i));
}

const CExpend& CExpend::operator =(const CExpend &o)
{
	BAS::operator =(o);
	m_iStpl = o.m_iStpl;
	m_iSplr = o.m_iSplr;
	m_iRcvr = o.m_iRcvr;
	m_fQtt = o.m_fQtt;
	return *this;
}

#ifdef __ITOG

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::cmp_nmtara уник.индекс - сравнение по наименованиям без учёта регистра и
/// по ёмкости тары
/// \return <0|0|>0
///
int CStaple::cmp_nmtarai(const CStaple &a, const CStaple &z)
{
	if (int n = cmp_nmi(a, z))
		return n;
	return DCMP_MORE(a.m_fTara, z.m_fTara, .0001) ? 1 : DCMP_LESS(a.m_fTara, z.m_fTara, .0001) ? -1 : 0;
}

// долгое посимвольное сравнение наименований товаров
int CStaple::cmp_nm(const CStaple &a, const CStaple &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iNm == z.m_iNm)
		return 0;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iNm).cmp(oz.GetItem(fl::ioffset, z.m_iNm));
}

int CStaple::cmp_nmi(const CStaple &a, const CStaple &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iNm == z.m_iNm)
		return 0;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iNm).cmpi(oz.GetItem(fl::ioffset, z.m_iNm));
}

int CStaple::cmp_art(const CStaple &a, const CStaple &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iArtl == z.m_iArtl)
		return 0;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iArtl).cmp(oz.GetItem(fl::ioffset, z.m_iArtl));
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::cmp_catnm сравниваю по категории и наименованию без учёта регистра
/// \return <0|0|>0
///
int CStaple::cmp_catnmi(const CStaple &a, const CStaple &z)
{
	if (int n = cmp_cat(a, z))
		return n;
	if (a.m_iNm == z.m_iNm)
		return 0;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iNm).cmpi(oz.GetItem(fl::ioffset, z.m_iNm));
}

#endif

}// namespace store