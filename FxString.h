#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FxString.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FxString.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxArray.h"

namespace glb {
int32_t cmp_iUtf8s(CCHAR *pcsza, cint32_t n1, CCHAR *pcszb, cint32_t n2);
int cmp_iUtf8sn(CCHAR *pcsz1, cint32_t c1, CCHAR *pcsz2, cint32_t c2, int32_t nmx);
int32_t cmp_utf8s(CCHAR *pcsza, cint32_t n1, CCHAR *pcszb, cint32_t n2);

class CFxString;
int str_utf8Case(CFxString &s, CharCase cs);

/* Соглашение: строка всегда завершена нулём, к-во символов (m_iCount) всегда включает
 * завершающий нуль (больше на 1, чем вернёт strlen())
 */
class CFxString : public CFxArray<char, true>
{
	typedef CFxArray<char, true> BAS;
public:// конструкторы и присваивание
	CFxString();
	CFxString(const CFxString &s);
//	CFxString(CFxString &s);
	CFxString(CFxString &&s); // констр-р перемещения CFxArray<char, true>
	CFxString(int32_t iGrow);
	CFxString(CBYTES &ac);
	CFxString(char c);
	CFxString(BYTE c);
	CFxString(CCHAR *pcsz, const int32_t cnt);
//	CFxString(CCHAR *pcsz); этот конструктор конфликтует с оператором char*, его сука нельзя объявлять, это однозначно
public:// изменения
	virtual const CFxString& operator =(const CFxString &s);
	virtual CFxString& operator =(CFxString &&s);
	virtual const CFxString& operator =(CCHAR *pcsz);
	virtual const CFxString& operator =(CBYTES &ab);
	virtual const CFxString& operator +=(const CFxString &s);
	virtual CFxString operator +(const CFxString &s) const;
	virtual const CFxString& operator +=(CBYTES &ab);
	virtual const CFxString& operator +=(BYTE c);
	virtual const CFxString& operator +=(char c);
	int32_t Add(const char &c) override;
	int32_t Add(int32_t n);
	int32_t Add(double f, int32_t nPrecision);
	int32_t Add(time_t tm, const char *pcszFmt = "%d.%m.%Y %H:%M");
	int32_t SetFrmt(cint32_t cnt, CCHAR *pcszFmt, ...);
	int32_t AddFrmt(cint32_t cnt, CCHAR *pcszFmt, ...);
	virtual void Insert(int32_t i, const char &c) override;
	virtual void Insert(int32_t i, const char *pc, int32_t cnt) override;
	int32_t Utf8Towc(CFxArray<wchar_t, true> &aw);
	virtual int32_t Add(const char *pcs, int32_t nCount = 1) override;
	virtual int32_t AddND(const char *pcs, int32_t nCount = 1) override;
	virtual void RemAt(const int32_t iInd, const int32_t iCnt = 1) override;
	virtual void RemND(const int32_t iInd, const int32_t iCnt = 1) override;
	virtual void Move(int32_t iTo, int32_t iFr) override;
	virtual void SetCountZeroND() override;
	virtual void SetCountND(int32_t nCount) override;
	virtual void SetCount(int32_t iNewCount) override;
//	virtual int32_t GetBinData(BYTES &a) const override;
//	virtual int32_t SetBinData(CBYTE *p, int32_t n) override;
public:// атрибуты
	virtual bool operator ==(const char *pcsz) const;
	virtual bool operator !=(const char *pcsz) const;
	int32_t cmp(const CFxString &s) const;
	int32_t cmp(CBYTES &s) const;
	int32_t cmpi(const CFxString &s) const;
	int32_t cmpi(const CFxString &s, cint32_t nfrst) const;
	CFxString& SetUpper();// inplace
	CFxString& SetLower();// inplace
	virtual operator const char* () const override;
	virtual const char& Last() const override;
	virtual char& Last() override;
	virtual int32_t GetCount() const override;
};

// стандартный конструктор для массивов
inline CFxString::CFxString() :
    CFxArray<char, true>(8)
{

}

// конструктор копирования для массивов и не только
inline CFxString::CFxString(const CFxString &s)
	: CFxArray<char, true>(s)
{

}

/*inline CFxString::CFxString(CFxString &s)
	: CFxArray<char, true>(s)
{

}*/

inline CFxString::CFxString(CFxString &&s)
	: CFxArray<char, true>::CFxArray(s)
{

}

inline CFxString::CFxString(int32_t iGrow) :
	CFxArray<char, true>(iGrow)
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::CFxString иницыацыя строки
/// \param pcsz: указатель на первый символ завершённой нулём строки
/// \param cnt: к-во символов строки без учёта завершающего нуля
///
inline CFxString::CFxString(CCHAR *pcsz, const int32_t cnt)
	: CFxArray<char, true>(pcsz, cnt + 1)
{
	BAS::m_pData[cnt] = '\0';
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::CFxString конструкцыя из строки, завершённой нулём.
/// \param pcsz: строка, завершённая нулём. Если нуль или не завершена нулём, результа не определён.
///
/*inline CFxString::CFxString(CCHAR *pcsz)
	: CFxArray<char, true>::CFxArray(pcsz, strlen(pcsz) + 1)
{	// в конструкторе CFxArray не вызываются переопределённые вирт.ф-ции
	BAS::Last() = '\0';
}*/

//////////////////////////////////////////////////////////////////////////
/// \brief CFxString::Add переопределяет ф-цию CFxArray<char, true>::Add(char)
/// \param c: добавляю символ
/// \return индекс добавленного эл-та
///
inline int32_t CFxString::AddND(const char *pcs, int32_t nCount) // nCount = 1
{
	return Add(pcs, nCount);
}

inline CFxString::operator const char*() const
{
	return m_pData;	// null terminated
}

inline const char& CFxString::Last() const
{
	ASSERT(m_iCount > 0 && (m_iCount > 1 || m_pData[0] == '\0'));
	return ((m_iCount == 1) ? m_pData[0] : m_pData[m_iCount - 2]);
}

inline char& CFxString::Last()
{	// Напомню, здесь m_iCount включает завершающий нуль. Last подразумевает не пустой массив (m_pData != nullptr)
	ASSERT(m_iCount > 0 && (m_iCount > 1 || m_pData[0] == '\0'));
	return ((m_iCount == 1) ? m_pData[0] : m_pData[m_iCount - 2]);
}

inline int32_t CFxString::cmp(const CFxString &s) const
{
	return glb::cmp_utf8s(m_pData, GetCount(), s.m_pData, s.GetCount());
}

inline int32_t CFxString::cmp(CBYTES &s) const
{
	int32_t cnt;
	CBYTE *cpb = s.data(cnt);
	return glb::cmp_utf8s(m_pData, GetCount(), (CCHAR*) cpb, cnt);
}

/*
 * сравниваю строки без учёта регистра, используя стандартную С-библиотеку
 * https://cplusplus.com/reference/cwchar/mbrtowc/
 */
inline int32_t CFxString::cmpi(const CFxString &s) const
{
	return glb::cmp_iUtf8s(m_pData, GetCount(), s.m_pData, s.GetCount());
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::cmpi сравнивает без учёта регистра только первые 'nfrst' символов
/// обеих строк
/// \param s: сравниваемая с этой строка
/// \param nfrst: наибольшее к-во символов для сравнения
/// \return <0|0|>0
///
inline int32_t CFxString::cmpi(const CFxString &s, cint32_t nfrst) const
{
	cint32_t c1 = GetCount(), c2 = s.GetCount();
	return glb::cmp_iUtf8sn(m_pData, c1, s.m_pData, c2, nfrst);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::SetUpper меняю регистр символов всей строки на верхний
/// \return this
///
inline CFxString& CFxString::SetUpper()
{
	str_utf8Case(*this, upper);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxString::SetLower меняю регистр всех символов строки на нижний
/// \return this
///
inline CFxString& CFxString::SetLower()
{
	str_utf8Case(*this, lower);
	return *this;
}

inline void CFxString::RemAt(const int32_t iInd, const int32_t iCnt)
{
	ASSERT(iInd < m_iCount - 1);
	BAS::RemAt(iInd, iCnt);
}

inline void CFxString::RemND(const int32_t iInd, const int32_t iCnt)
{
	ASSERT(iInd < m_iCount - 1);
	BAS::RemND(iInd, iCnt);
}

inline void CFxString::Move(int32_t iTo, int32_t iFr)
{
	ASSERT(iTo < m_iCount - 1 && iFr < m_iCount - 1 && iTo != iFr);
	BAS::Move(iTo, iFr);
}

inline void CFxString::SetCountZeroND()
{
	if (m_iCount > 0) {
		BAS::SetCountND(1);
		*m_pData = '\0';
	}
}

inline void CFxString::SetCountND(int32_t nCount)
{
	BAS::SetCountND(nCount + 1);
	m_pData[nCount] = '\0';
	ASSERT(m_iCount - 1 == nCount);
}

inline void CFxString::SetCount(int32_t iNewCount)
{
	ASSERT(iNewCount >= 0);
	if (!iNewCount)
		BAS::SetCount(0);
	else {
		BAS::SetCount(iNewCount + 1);
		m_pData[iNewCount] = '\0';
	}
}

inline bool CFxString::operator ==(const char *pcsz) const
{
	if (!m_pData || !pcsz)
		return ((m_pData == pcsz) ? true : false);
	return (strcmp(m_pData, pcsz) == 0);
}

inline bool CFxString::operator !=(const char *pcsz) const
{
	if (!m_pData || !pcsz)
		return ((m_pData != pcsz) ? true : false);
	return (strcmp(m_pData, pcsz) != 0);
}

inline int32_t CFxString::GetCount() const
{
	return ((m_iCount > 0) ? m_iCount - 1 : 0);
}

} // namespace glb
