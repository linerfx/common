#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_varFileShrd.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_varFileShrd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_varFileBase.h"
#include "io_flIndVShrd.h"
//#include "io_opener.h"
//#include "strdef.h"
#include "FileMan.h"

namespace fl {
	extern CFileMan man;

namespace a {

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief class CvShrd индексированный файловый массив с эл-тами динамич. размера, с синхронизи-
/// рованным доступом между потоками и процессами с индексами, размещёнными в общей памяти.
/// Предполагается использовать этот массив в объекте, используемом всё время существования процесса.
/// Пример (на коем тестировался класс): fl::CStrings.
/// Меж-процессорная и меж-потоковая синхронизация выполнена объектом CFileMan::CLocker m_lock, который
/// блокирует доступ к файлу по стандартному ключу для всех аналогичных объектов в разных процессах или
/// потоках в начале всякой ф-ции редактирования массива.
/// \param Obj: объект файла, производный от CFileItem.
/// \param nInds: к-во индексов. См. CIndex и его производные CIndexLocal и CIndexShared.
/// CvShrd наследуют: fl::CStrings, cafe::CBills, cafe::CContractors, cafe::CCoworkers, cafe::CDepts,
/// cafe::CMenues, store::CProducts
///
template <class Obj, int nInds>
class CvShrd : public CvBase<Obj, nInds>
{
public:
	typedef CvShrd<Obj, nInds> THIS;
	typedef i::CIndVShrd<Obj, nInds> INDEX;
	typedef CvBase<Obj, nInds> BAS;
	typedef glb::CFxArray<BYTE, true> IOBUF;
private:
	CFileMan::CLocker m_lock;
	INDEX m_ind;
public:
	explicit CvShrd(); // для массивов... требуется CvBase::Set(typename i::CIndBase<Obj>::fcmp *f) для каждого
	explicit CvShrd(const char *pcszKey, const char *pcszPN, typename INDEX::fcmp *ppf, bool bopen = false);
	virtual ~CvShrd();
protected:
	virtual INDEX* GetIndex() override;
public:// открытия/закрытия
	virtual int64_t open(const char *pcszPN) override;
	virtual int64_t open(const char *pcszKey, const char *pcszPN);
	virtual void close() override;
	virtual void drop();
	// расширенные возможности для открытия файла без параметров. Производный класс должен собрать путь, ключ...
	virtual int64_t open() override;
	virtual const char* PathName() override;	// путь и имя файла (производный класс обязан предъявить)
	virtual const char* Key(glb::CFxString& s) override;// ключ к файлу для QSharedMemory (производный класс обязан предъявить)
public:// извлечение из массива
    virtual const INDEX* GetIndex() const override;
//    virtual int32_t UnicIndex() const override;
    virtual const int32_t* UnicIndices(int32_t *pcnt) const override;
public:// атрибуты
	virtual bool IsOpen() const override;
};

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvShrd конструктор CvShrd откроет индексированный синхронизированный
/// файловый массив, если bopen не нуль. Если нуль, файл не будет открыт, будет создан пустой,
/// безействующий объект. После его можно будет задействовать ф-цией Open.
/// \param pcszKey: уникальный идентификатор файла. Может быть нулём, если !bopen
/// \param pcszPN: путь и имя открываемого файла. Может быть нулём, если !bopen
/// \param ppf: массив ф-цйи сравнения индексированного файлового массива. Должен быть предоставлен в к-ве nInds.
/// \param bopen: открывать ли файл? Если нуль, попытка открыть файл не будет предпринята.
/// соответственно, уникальный идентификатор файла и путь могут быть недействительными.
///
template<class Obj, int nInds>
CvShrd<Obj, nInds>::CvShrd()
{
	ASSERT(nInds > 0 && nInds < 100);
}

template<class Obj, int nInds>
CvShrd<Obj, nInds>::CvShrd(const char *pcszKey,
                           const char *pcszPN,
                           typename INDEX::fcmp *ppf,
                           bool bopen) // bopen = true
	: m_lock(pcszKey)
	, m_ind(ppf)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(!bopen || pcszKey && pcszPN);
	if (bopen)
		open(pcszPN);
}

template<class Obj, int nInds>
CvShrd<Obj, nInds>::~CvShrd()
{
	drop();
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvShrd::GetIndex возвращает указанный индекс. Здесь тонкое место, GCC справ-
/// ляется, на других компиляторах пока не проверялось. Тонкое потому что dynamic_cast возвра-
/// щает 0 т.к. не приводит CIndShared<CvShrd>* к CIndex<CvBase>*, только "грубое"
/// С-приведение действует, я так полагаю, благодаря GCC, вопреки правилам С++. Чтобы выйти из
/// создавшегося положения, нужно индекс здесь использовать шаблоном, т.е. добавить Indx к Obj
/// и nInds и описать отдельно каждый индекс отдельно (без наследования) с идентичным interface'ом.
/// \param i: индекс объета от ioffset до nInds - 1.
/// \return указатель на индекс
///
template<class Obj, int nInds>
typename i::CIndVShrd<Obj, nInds>* CvShrd<Obj, nInds>::GetIndex()
{
	return &m_ind;
}

template<class Obj, int nInds>
const typename i::CIndVShrd<Obj, nInds>* CvShrd<Obj, nInds>::GetIndex() const
{
    return &m_ind;
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Open откроет индексированный файл с синхронизацией между потоками и процессами на
/// уровне операционки. Эту ф-цию следует вызывать если в конструктор был отправлен ключ. На-
/// пример, при повторном отркрытии файлового массива.
/// За 'open' следует звать 'close'.
/// \param pcszPN путь и имя файла
/// \return к-во элементов в файле
///
template<class Obj, int nInds>
int64_t CvShrd<Obj, nInds>::open(const char *pcszPN)
{
	if (!m_lock.Locked())
		m_lock.Lock();
	return BAS::open(pcszPN);// откроет файл, прочтёт индексы
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief fl::CvShrd::open откроет индексированный файл с синхронизацией между потоками
/// и приложениями на уровне операционки. За 'open' следует звать 'close'.
/// \param pcszKey: ключ файла см. CFileMan::CFile::MkKey
/// \param pcszPN: путь к файлу
/// \return к-во эл-тов в файле
///
template<class Obj, int nInds>
int64_t CvShrd<Obj, nInds>::open(const char *pcszKey, const char *pcszPN)
{
	ASSERT(!m_lock.Locked() && !BAS::IsOpen());
	m_lock.Lock(pcszKey);
	return BAS::open(pcszPN);
}

template<class Obj, int nInds>
void CvShrd<Obj, nInds>::close()
{
	ASSERT(m_lock.Locked() && BAS::IsOpen());
	BAS::close();
	m_lock.Unlock();// индексы остаются в разделяемой памяти
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvShrd::drop закрывает файл, сбрасывает все индексы в их файлы, закрывает их,
/// удаляет объект разделяемой памяти
///
template<class Obj, int nInds>
void CvShrd<Obj, nInds>::drop()
{
	if (BAS::IsOpen())
		close();
	// если объект не использовался, можно не перезаписывать индексы
	if (!m_lock.IsValid())
		return;
	m_lock.Lock();// ограничение доступа и к индексам тоже
	if (m_ind.IsValid())
		m_ind.drop();
	m_lock.Destroy();// снимает блокировку, удаляет разделяемую память
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStrings::get извлекаю множество строк из индексированного массива по
/// указанным идентификаторам/индексам.
/// \param as: ИЗ - результат ф-ции
/// \param pIDs: В - индексы извлекаемых строк
/// \param nIDs: В - к-во извлекаемых строк
/// \return к-во полученых строк
///
template<class Obj, int nInds>
int64_t CvShrd<Obj, nInds>::open()
{
	ASSERT(!m_lock.Locked() && !BAS::IsOpen());
	if (!BAS::m_sPath.GetCount()) {
		glb::CFxString sKey;
		return open(Key(sKey), PathName());
	}
	return open(BAS::m_sPath);
}

template<class Obj, int nInds>
const char* CvShrd<Obj, nInds>::PathName()
{
	ASSERT(false);// для производного класса
	return NULL;
}

template<class Obj, int nInds>
const char* CvShrd<Obj, nInds>::Key(glb::CFxString& s)
{
	ASSERT(false);// для производного класса
	return NULL;
}

/*template<class Obj, int nInds>
int32_t CvShrd<Obj, nInds>::UnicIndex() const
{
	return CvBase<Obj, nInds>::UnicIndex();
}*/

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvShrd::UnicIndices возвращает массив индексов уникальных индексов или нуль.
/// \param pcnt: [ИЗ] ук-ль на к-во уник.индексов. Может быть нуль.
/// \return расположенные в возраст.порядке индексы уник.индесов, как iStrsNms, iStrStplNmi
/// См. fl::a::CheckExistence()
///
template<class Obj, int nInds>
const int32_t* CvShrd<Obj, nInds>::UnicIndices(int32_t *pcnt) const // override
{
	return CvBase<Obj, nInds>::UnicIndices(pcnt);
}

template<class Obj, int nInds>
bool CvShrd<Obj, nInds>::IsOpen() const
{
	return (m_lock.Locked() && BAS::IsOpen());
}

}// namespace a
}// namespace fl
