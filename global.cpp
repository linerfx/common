/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл global.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * global.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <cstdarg>
#include <ctime>
#include <stdio.h>		// remove
//#include <unistd.h>	// ftruncate() нужная ф-ция
#include "defs.h"
#include "global.h"
#include "ErrLog.h"
//#include <locale>
#if defined __ITOG | defined __STRFLA
#include "io_strings.h"
#endif// defined __ITOG | defined __STRFLA
#if defined OACRL_LIBRARY || defined LINER_PROJ
#include "oa_defs.h"
#endif// defined OACRL_LIBRARY || defined LINER_PROJ

// выключаю предупреждения о фиг.скобках в и/или и скобках в логических: '&&' within '||'
#ifdef __llvm__
#pragma clang diagnostic ignored "-Wdangling-else"
#pragma clang diagnostic ignored "-Wlogical-op-parentheses"
#endif

namespace fl {
    int nBlockSz;	// блок диска для ф-цый чтения/записи в CBinFile см. CPath::CreateAppPath
#if defined __ITOG | defined __STRFLA
	extern CStrings strs;
#endif
}
namespace err {
	extern CLog log;
}// namespace err
namespace glb {
	// эти определения must exist in your app... take care of it. Now!
	extern thread_local const CFxString sAppPath;

/////////////////////////////////////////////////////////////////////////////////////
/// \brief GetAppPath
/// \return
///
const CFxString& GetAppPath()
{
	return sAppPath;
}

/*static err::CLog log;
thread_local static double g_eps;
thread_local static double g_pnt;
const int g_aiTopLevels[] = TLA_INITIALIZER;*/

/*
 * FormatString - собираю строку согласно с форматом и аргументами.
 * формат pszFmt может содержать:
 * 1. символ (записывается как есть),
 * 2. '%' - знак формата,
 * 3. '*' если стоит за знаком формата, то означает аргумент:
 *		- целое беззнаковое число больше нуля - точность для double
 *		- строка формата для времени (по умолчанию: "%d.%m.%Y %H:%M:%S)",
 *		- длина строки, не завершённой нулём
 *		- для других игнорируется.
 * 4. символ формата, следующий за "%[*]":
 *  i|d - целое 32 бит | l - целое 64 бит | u|x - беззнаковое целое 32 бит | f|g - float/double |
 *  s - строка, нуль в конце | t - time_t время от ф-ции time(), здесь приводится ф-цией localtime.
 * Строка обязательно завершается нулём.
 * Возвращаю к-во записанных байт без учёта завершающего нуля.
 */
int FormatString(char *pszTxt, const int cMx, const char *pszFmt, va_list &varl)
{
	ASSERT(cMx > 0 && pszTxt != nullptr);
    bool bFmt = false;
    int iCnt, iTmp;
	union {
		int32_t i;
		uint32_t iu;
        double d;
		time_t time;
        char *ps;
		int64_t n;
    } data;
	union {
		uint32_t iPrec;
        const char *pszTmFmt;
    } prec_fmt;
	struct tm *pstm = nullptr;
	prec_fmt.pszTmFmt = (const char*) -1;
	for (iCnt = 0, *pszTxt = '\0'; *pszFmt && iCnt < cMx; pszFmt++) {
		if (bFmt) {
			switch (*pszFmt) {
            case '*':
				prec_fmt.iPrec = va_arg(varl, uint32_t);
				continue; // for
            case 'i':
            case 'd':
				data.i = va_arg(varl, int32_t);
				iTmp = i2a(data.i, pszTxt + iCnt, cMx - iCnt, 10);
                break;
			case 'l':
				data.n = va_arg(varl, int64_t);
				iTmp = i2a(data.n, pszTxt + iCnt, cMx - iCnt, 10);
				break;
			case 'u':
			case 'x':
				data.iu = va_arg(varl, uint32_t);
				iTmp = i2a(data.iu, pszTxt + iCnt, cMx - iCnt, (*pszFmt == 'u') ? 10 : 16);
                break;
			case 'f':
                data.d = va_arg(varl, double);
				if (prec_fmt.iPrec != -1U)
					iTmp = snprintf(pszTxt + iCnt, (size_t) (cMx - iCnt), "%.*f", prec_fmt.iPrec, data.d);
                else
					iTmp = snprintf(pszTxt + iCnt, (size_t) (cMx - iCnt), "%f", data.d);
                break;
			case 'g':
                data.d = va_arg(varl, double);
				if (prec_fmt.iPrec != -1U)
					iTmp = snprintf(pszTxt + iCnt, (size_t) (cMx - iCnt), "%.*g", prec_fmt.iPrec, data.d);
                else
					iTmp = snprintf(pszTxt + iCnt, (size_t) (cMx - iCnt), "%g", data.d);
                break;
			case 's':
				data.ps = va_arg(varl, char*);
                ASSERT(data.ps);
				// здесь prec_fmt.iPrec либо 0xffffffff, или равна длине строки, если указана точность
				for (iTmp = 0; data.ps[iTmp] && iCnt + iTmp < cMx && (uint32_t) iTmp < prec_fmt.iPrec; iTmp++)
                    *(pszTxt + iCnt + iTmp) = *(data.ps + iTmp);
                break;
			case 't':
				data.time = va_arg(varl, time_t);
				if (prec_fmt.pszTmFmt == (char*) -1)
					prec_fmt.pszTmFmt = "%H:%M:%S %d.%m.%Y";
				pstm = localtime(&data.time);
				ASSERT(pstm->tm_hour >= 0 && pstm->tm_hour < 24 && pstm->tm_yday > 0 && pstm->tm_wday < 32);
				iTmp = (int) strftime(pszTxt + iCnt, (size_t) (cMx - iCnt), prec_fmt.pszTmFmt, pstm);
                break;
            default:
                ASSERT(false);
                // no break
			case '%':
                iTmp = 1;
                pszTxt[iCnt] = *pszFmt;
                break;
            }
			prec_fmt.iPrec = 0xffffffff;
			if (iTmp < 0)
                break; // leave 'for'
            iCnt += iTmp;
            bFmt = false;
		} // if (bFmt)
		else if (*pszFmt == '%')
			bFmt = true;
        else
            pszTxt[iCnt++] = *pszFmt;
    }
	ASSERT(iCnt <= cMx);
	if (iCnt == cMx)
		iCnt -= 1;
	pszTxt[iCnt] = '\0';
	return iCnt;
}

/*
 * ф-ция сравнения строк. Возвращает <0, если ps1 меньше, >0, если ps1 больше и 0, если строки равны
 * Если длина одной из строк -1, убедись, что строка завершена нулём.
 */
int cmp_str(CCHAR *pa, CCHAR *pz, int na, int nz)
{
	if (na < 0)
		na = strlen(pa);
	if (nz < 0)
		nz = strlen(pz);
	for (int i = 0; i < na && i < nz; i++)
		if (pa[i] != pz[i])
			return (pa[i] - pz[i]);
	return (na - nz);
}

/*
 * ф-ция сравнения ANSI-строк. Возвращает <0, если ps1 меньше, >0, если ps1 больше и 0, если строки равны
 * Если длина одной из строк -1, убедись, что строка завершена нулём.
 */
int cmp_str(CBYTE *pa, CBYTE *pz, int na, int nz)
{
	if (na < 0)
		na = strlen((CCHAR*) pa);
	if (nz < 0)
		nz = strlen((CCHAR*) pz);
	for (int i = 0; i < na && i < nz; i++)
		if (pa[i] != pz[i])
			return (pa[i] - pz[i]);
	return (na - nz);
}

int cmp_bytes(CBYTES &a, CBYTES &z)
{
	int32_t na, nz;
	CBYTE *pa = a.data(na), *pz = z.data(nz);
	return cmp_str(pa, pz, na, nz);
}

/*
 * ф-ция стравнения Utf-8 строк без учёта регистра. Возвращает <0|0|>0
 * https://cplusplus.com/reference/cwchar/mbrtowc/
 */
int cmp_iUtf8s(CCHAR *pcsz1, cint32_t c1, CCHAR *pcsz2, cint32_t c2)
{
	mbstate_t mbs, mbss;
	size_t sz1 = 0, sz2 = 0, n1 = 0, n2 = 0;
	wchar_t wc1 = 0, wc2 = 0;
	memset(&mbs, 0, sizeof mbs);
	memset(&mbss, 0, sizeof mbss);
	for (n1 = n2 = 0; n1 < c1 && n2 < c2; n1 += sz1, n2 += sz2) {
		sz1 = mbrtowc(&wc1, pcsz1 + n1, c1 - n1, &mbs);
		sz2 = mbrtowc(&wc2, pcsz2 + n2, c2 - n2, &mbss);
		if (sz1 == (size_t) -1 || sz1 == (size_t) -2 || sz2 == (size_t) -1 || sz2 == (size_t) -2)
			throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__, nullptr), __LINE__);
		if (!sz1 || !sz2) {
			ASSERT(false);	// завершённая нулём строка, завершающий нуль в числе символов строки
			break;			// (не верное к-во символов строки)
		}
		wc1 = towlower(wc1);
		wc2 = towlower(wc2);
		if (wc1 != wc2)
			return (wc1 - wc2);
	}// одна из строк или обе пройдены до конца или одна или обе строки пустые
	return (c1 - c2);
}

/*
 * ф-ция стравнения Utf-8 строк без учёта регистра не более указанного к-ва utf-8 символов.
 * Возвращает <0|0|>0
 * https://cplusplus.com/reference/cwchar/mbrtowc/
 */
int cmp_iUtf8sn(CCHAR *pcsz1, cint32_t c1, CCHAR *pcsz2, cint32_t c2, int32_t nmx)
{
	ASSERT(nmx > 0);
	mbstate_t mbs, mbss;
	size_t sz1 = 0, sz2 = 0, n1 = 0, n2 = 0;
	wchar_t wc1 = 0, wc2 = 0;
	memset(&mbs, 0, sizeof mbs);
	memset(&mbss, 0, sizeof mbss);
	for (n1 = n2 = 0; nmx > 0 && n1 < c1 && n2 < c2; n1 += sz1, n2 += sz2, nmx--) {
		sz1 = mbrtowc(&wc1, pcsz1 + n1, c1 - n1, &mbs);
		sz2 = mbrtowc(&wc2, pcsz2 + n2, c2 - n2, &mbss);
		if (sz1 == (size_t) -1 || sz1 == (size_t) -2 || sz2 == (size_t) -1 || sz2 == (size_t) -2)
			throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__, nullptr), __LINE__);
		if (!sz1 || !sz2) {
			ASSERT(false);
			break; // конец строки или обеих строк. Ошибка: неверно указано к-во символов
		}
		wc1 = towlower(wc1);
		wc2 = towlower(wc2);
		if (wc1 != wc2)
			return (wc1 - wc2);
	}// лимит к-ва сравниваемых символов или одна из строк или обе пройдены до конца или одна или обе строки пустые
	return (nmx > 0) ? (c1 - c2) : 0;
}

/*
 * ф-ция сравнения Utf-8 строк с учётом регистра. Возвращает <0|0|>0
 * https://cplusplus.com/reference/cwchar/mbrtowc/
 */
int cmp_utf8s(CCHAR *pcsz1, cint32_t c1, CCHAR *pcsz2, cint32_t c2)
{
	mbstate_t mbs, mbss;
	size_t sz1 = 0, sz2 = 0, n1 = 0, n2 = 0;
	wchar_t wc1 = 0, wc2 = 0;
	memset(&mbs, 0, sizeof mbs);
	memset(&mbss, 0, sizeof mbss);
	for (n1 = n2 = 0; n1 < c1 && n2 < c2; n1 += sz1, n2 += sz2) {
		sz1 = mbrtowc(&wc1, pcsz1 + n1, c1 - n1, &mbs);
		sz2 = mbrtowc(&wc2, pcsz2 + n2, c2 - n2, &mbss);
		if (sz1 == (size_t) -1 || sz1 == (size_t) -2 || sz2 == (size_t) -1 || sz2 == (size_t) -2)
			throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__, nullptr), __LINE__);
		if (!sz1 || !sz2) {
			ASSERT(false);
			break; // конец строки или обеих строк. Ошибка: неверная длина строки
		}
		if (wc1 != wc2)
			return (wc1 - wc2);
	}// одна из строк или обе пройдены до конца
	return (c1 - c2);
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief str_utf8Case меняю регистр всех символов в Utf-8 строке
/// \param s: строка символов
/// \param cs: перечислени6 lower или upper
/// \return длина строки (к-во символов строки)
///
int str_utf8Case(CFxString &s, CharCase cs)
{
	mbstate_t mbs, mbs1;
	size_t sz = 0, sz1 = 0, i = 0;
	wchar_t wc;
	int32_t nc = 0, c = s.GetCount();
	char ac[MB_CUR_MAX], *pcsz = s;
	memset(&mbs, 0, sizeof mbs);
	memset(&mbs1, 0, sizeof mbs1);
	for (i = 0; i < c; i += sz, nc++) {
		sz = mbrtowc(&wc, pcsz + i, c - i, &mbs);
		if (sz == (size_t) -1 || sz == (size_t) -2)
			throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__, nullptr), __LINE__);
		if (!sz) {
			ASSERT(false);
			break; // конец строки или обеих строк. Ошибка: неверная длина строки
		}
		if (!iswalpha(wc))
			continue;
		if (cs == lower)
			if (iswlower(wc))
				continue;
			else
				wc = towlower(wc);
		else if (iswupper(wc)) // cs == upper
			continue;
		else
			wc = towupper(wc);
		sz1 = wcrtomb(ac, wc, &mbs1);
		if (sz1 == (size_t) -1 || sz1 == (size_t) -2)
			throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__, nullptr), __LINE__);
		if (!sz1) {
			ASSERT(false);
			break; // конец строки или обеих строк. Ошибка: неверная длина строки
		}
		if (sz != sz1) {
			if (sz > sz1) {
				s.RemAt(i, sz - sz1);
				sz = sz1;
			} else
				while (sz < sz1) {
					s.Insert(i, 0x20); // ansii-пробел
					sz++;
				}
			c = s.GetCount();
			pcsz = s;
		}
		s.Edit(i, ac, sz);
	}
	return nc;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief SafeCopyString копирует строку символов
/// \param pszDest - буфер для копирования.
/// \param iDestLen - размер буфера в байтах.
/// \param pcszSource - строка - источник - может не завершаться нулём, тогда её длина
/// должна быть указана в iSrcLen.
/// \param iSrcLen - длина строки без учёта завершающего нуля - может быть меньше, или
/// равняться нулю если источник завершается нулём.
/// \return к-во записанных символов без учёта заверш.нуля
///
int SafeCopyString(char *pszDest, int iDestLen, const char *pcszSource, int iSrcLen)
{
	if (iSrcLen <= 0)
		iSrcLen = (int) strlen(pcszSource);
	if (iDestLen <= iSrcLen)
		iSrcLen = iDestLen - 1;
	if (iSrcLen >= 0) {
		if (iSrcLen)
			memcpy(pszDest, pcszSource, iSrcLen);
		pszDest[iSrcLen] = '\0';
		return iSrcLen;
	}
	return 0;
}

///////////////////////////////////////////////////////////
/// \brief Bytes сокращает целое до действительного до сотни, указывая в строке е.и.
/// \param n - В: сокращаемое число
/// \param pps - ИЗ: е.и.
/// \return сокращённое действительное
///
double Bytes(uint64_t n, const char **pps)
{
	if (!(n / 1024)) {
		*pps = S_BYTES;
		return n;
	}
	double f = n / 1024.0;
	*pps = S_KB;
	if (f < 1000.0)
		return f;
	*pps = S_MB;
	if((f /= 1024.0) < 1000.0)
		return f;
	*pps = S_GB;
	if ((f / 1024.0) < 1000.0)
		return f;
	*pps = S_TB;
	return (f / 1024.0);
}

// SplitString для разделения числа пробелами через определённый интервал, например, через
// три символа: 1 234 567
const char* SplitString(char *pszNum, const int cStrMax, const int cPeriod)
{
	ASSERT(cPeriod >= 2);
	if (!cStrMax || !pszNum)
		return nullptr;
	const int cCnt = (int) strlen(pszNum);
	int i, x;
	if (cCnt <= 0)
		return nullptr;
	for (i = cCnt - 1, x = 1; i > 0; i--)
		if((cCnt - i) % cPeriod == 0 && cCnt - i < cStrMax) {
			memmove(pszNum + i + 1, pszNum + i, (size_t) (cCnt - i + x) * sizeof(char));
			pszNum[i] = ' ';
			x++;
		}
	pszNum[MIN(cStrMax - 1, cCnt + x)] = '\0';
	return pszNum;
}
// 0123456789
// 12 3456 7890
////////////////////////////////////////////////////////////////////////////////////////////
/// \brief xa2ui читаю шестнадцатиричное целое из строки в беззнаковое целое. Сначала пропускаю
/// пробелы, затем читаю число префикс '0x' или '0X', затем число до первого символа вне диапазона 0-9 a-f A-F
/// \param sz: строка - ввод
/// \param x: результат - вывод
/// \return к-во прочитанных байт
///
int32_t xa2ui(const char *sz, uint32_t &x)
{
	int i;
	x = 0U;
	for (i = 0; isspace(sz[i]); i++)
		;
	if (sz[i] == '0' && (sz[i + 1] == 'x' || sz[i + 1] == 'X'))
		i += 2;
	while (i++ >= 0) {// infinite
		if (sz[i] >= '0' && sz[i] <= '9')
			x = x * 16 + (sz[i] - '0');
		else if (sz[i] >= 'a' && sz[i] <= 'f')
			x = x * 16 + 10 + (sz[i] - 'a');
		else if (sz[i] >= 'A' && sz[i] <= 'F')
			x = x * 16 + 10 + (sz[i] - 'A');
		else
			break;
	}
	return i;
}


// формат строки со временем: h(h):m(m):s(s)
int ParseTime(const char *pcszTm)
{
	int i = 0, j;
	int nh = 0, nm = 0, ns = 0;
	// fucking white spaces in fucking user string
	while (IsWS(*pcszTm))
		pcszTm++;
	// hours (optional)
	j = glb::a2i(nh, pcszTm);
	// разделителем во времени может быть двоеточие и нуль - окончание строки
	if (j > 2 || j && (!ValidTmDel(pcszTm[i + j]) || !ValidHour(nh)))
		return -1;
	// если в строке нет минут и секунд, верну время
	if (IsZero(pcszTm[i + j]))
		goto _ret;
	i += j + 1; // +1 -- разделитель
	// minutes (optional)
	j = a2i(nm, pcszTm + i);
	if (j > 2 || j && (!ValidTmDel(pcszTm[i + j]) || !ValidMinSec(nm)))
		return -1;
	// если в строке нет секунд, верну дату и время
	if (IsZero(pcszTm[i + j]))
		goto _ret;
	i += j + 1; // +1 -- разделитель
	// seconds (optional)
	j = a2i(ns, pcszTm + i);
	if (j > 2 || !ValidMinSec(ns))
		return -1;
	// fucking time
_ret:
	return(nh * 3600 + nm * 60 + ns);
}

// формат строки с датой и временем: d(d).m(m).yyyy h(h):m(m):s(s)
time_t ParseDateTime(const char *pcszTm)
{
	int i, j;
	struct tm tmSrc;
	// fucking initialization
	memset(&tmSrc, 0, sizeof tmSrc);
	// fucking white spaces in fucking user string
	while (IsWS(*pcszTm))
		pcszTm++;
	// time format: d(d).m(m).yyyy h(h):m(m):s(s)
	i = a2i(tmSrc.tm_mday, pcszTm);
	if (i > 2 || !ValidDtDel(pcszTm[i++]) || !ValidMonthDay(tmSrc.tm_mday))
		return -1L;
	// month
	j = a2i(tmSrc.tm_mon, pcszTm + i);
	tmSrc.tm_mon--; // jan = 0
	if (j > 2 || !ValidDtDel(pcszTm[i++ + j]) || !ValidMonth(tmSrc.tm_mon))
		return -1L;
	i += j;
	// year
	j = a2i(tmSrc.tm_year, pcszTm + i);
	if (tmSrc.tm_year >= 1900)
		tmSrc.tm_year -= 1900;
	// разделителем времени и даты может быть пробел или нуль - окончание строки
	if (j != 4 || !ValidDtTmDel(pcszTm[i + j]) || !ValidYear(tmSrc.tm_year))
		return -1L;
	// если в строке нет времени, верну дату.
	if (IsZero(pcszTm[i + j]))
		goto _ret;
	i += j + 1;	// +1 -- разделитель
				// hours (optional)
	j = a2i(tmSrc.tm_hour, pcszTm + i);
	// разделителем во времени может быть двоеточие и нуль - окончание строки
	if (j > 2 || j && (!ValidTmDel(pcszTm[i + j]) || !ValidHour(tmSrc.tm_hour)))
		return -1L;
	// если в строке нет минут и секунд, верну дату и время
	if (IsZero(pcszTm[i + j]))
		goto _ret;
	i += j + 1; // +1 -- разделитель
				// minutes (optional)
	j = a2i(tmSrc.tm_min, pcszTm + i);
	if (j > 2 || j && (!ValidTmDel(pcszTm[i + j]) || !ValidMinSec(tmSrc.tm_min)))
		return -1L;
	// если в строке нет секунд, верну дату и время
	if (IsZero(pcszTm[i + j]))
		goto _ret;
	i += j + 1; // +1 -- разделитель
				// seconds (optional)
	j = a2i(tmSrc.tm_sec, pcszTm + i);
	if (j > 2 || !ValidMinSec(tmSrc.tm_sec))
		return -1L;
	// fucking time
_ret:
	tmSrc.tm_isdst = tmSrc.tm_yday = tmSrc.tm_wday = -1;
	time_t tmres = mktime(&tmSrc);
	// добавить g_tmFickingAdd чтобы вернуть грёбаный gmt
	return(tmres);// + g_tmFuckingAdd);
}

/*double Round(double val, int iPrecision)
{
	ASSERT(iPrecision >= 0);
	double point = pow(10.0, iPrecision);
	double res = floor(val * point) / point;
	if (val < 0.0)
		return((val - res <= -5.0 / point / 10.0) ? res - 1.0 / point : res);
	return((val - res >= 5.0 / point / 10.0) ? res + 1.0 / point : res);
}*/

/* cм. global.h template<TP> TP Round(TP...)
double Round(const double &val, int iPrecision)
{
	ASSERT(iPrecision >= 0);
	double pnt = pow(10.0, -iPrecision);
	double res = floor(val / pnt) * pnt;
	if (val < 0.0)
		return ((val - res <= -5.0 * (pnt * .1)) ? res - pnt : res);
	return ((val - res >= 5.0 * (pnt * .1)) ? res + pnt : res);
}

float Round(float val, int iPrecision)
{
	ASSERT(iPrecision >= 0);
	float pnt = pow(10.f, -iPrecision);
	float res = floor(val / pnt) * pnt;
	if (val < 0.f)
		return ((val - res <= -5.f * (pnt * .1f)) ? res - pnt : res);
	return ((val - res >= 5.f * (pnt * .1f)) ? res + pnt : res);
}

double Round(const double &f, const double &pnt)
{
	double tmp = floor(f / pnt) * pnt;
	return(((f - tmp) >= 5.0 * (pnt * .1)) ? tmp + pnt : tmp);
}

float Round(float f, float pnt)
{
	double tmp = floor(f / pnt) * pnt;
	return(((f - tmp) >= 5.f * (pnt * .1f)) ? tmp + pnt : tmp);
}*/

////////////////////////////////////////////////////////////////////////////////
/// \brief FileName вычисляет только имя файла без расширения
/// \param pcszPath: имя файла. Может быть полный путь
/// \param pnLen: может быть нулём, может содержать длину строки, или указыать на 0/-1,
/// на выводе будет содержать к-во символов имени файла (без расширения)
/// \return указатель на первый символ имени файла
///
CCHAR* FileName(CCHAR *pcszPath, int *pnLen)
{
    ASSERT(pcszPath);
	int n = 0,
	    i = (pnLen && *pnLen > 0) ? *pnLen : (int) strlen(pcszPath) - 1;
    while (i > 0 && (pcszPath[i] == CPSEP || isspace(pcszPath[i])))
        i--;
    while (i > 0 && pcszPath[i] != CPSEP) {
		i--;
		n++;
    }
	if (pnLen)
		*pnLen = n;
	return ((i > 0) ? pcszPath + i + 1 : pcszPath);
}

/////////////////////////////////////////////////////////////////////////////////
/// \brief FileName находит первый символ имени файла.
/// \param pcszPath: путь и имя файла
/// \param cPathLen: длина строки 'pcszPath'
/// \return указатель на первый символ файла
///
const char* FileName(const char *pcszPath, const int cPathLen)
{
	ASSERT(pcszPath && cPathLen > 0);
	int i = cPathLen - 1;
	while (i > 0 && (pcszPath[i] == CPSEP || isspace(pcszPath[i])))
		i--;
	while(i > 0 && pcszPath[i] != CPSEP)
		i--;
	return ((i > 0) ? pcszPath + i + 1 : pcszPath);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FileTitle возвращает указатель pcszFlName, вычисляет длину файла без расширения.
/// \param pcszFlName: указатель на наименование и расширение файла
/// \param nLen: длина наименования на вводе, на выводе равно длине имени файла без расширения.
/// \return указатель на наименование файла.
///
const char* FileTitle(const char *pcszFlName, int *nLen)
{
	int i = *nLen - 1;
	while (i > 0 && pcszFlName[i] != '.')
		i--;
	if (i > 0)
		*nLen = i;
	return pcszFlName;
}

// площадь треугольника по формуле Герона (вычесляет площадь треугольника по его сторонам)
double TriangleSquare(double a, double b, double c)
{
	double hp = (a + b + c) / 2.0;	// полупериметр
	return sqrt(hp * (hp - a) * (hp - b) * (hp - c));
}

//////////////////////////////////////////////////////////////////////
/// \brief CharLower переводит строку 'psz' в нижний регистр
/// \param psz - переводимая строка, должна быть завершена нулём
/// \return возвращает psz == CharLower(psz);
///
const char* ToLower(char *psz)
{
	char *pc = psz;
	for (; *pc; pc++)
		*pc = (char) tolower(*pc);
	return psz;
}

const char* ToUpper(char *psz)
{
	char *pc = psz;
	for (; *pc; pc++)
		*pc = (char) toupper(*pc);
	return psz;
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief ChangeColour меняю все цвета в svg-рисунке на указанный, записываю замененные цвета
/// в массив (если такой имеется) в той же пос-ти, что были найдены в рисунке.
/// \param pcszSvg: svg-рисунок
/// \param nlen: длина строки pcszSvg
/// \param nClrWith: grb-цвет, на который меняются все цвета в файле
/// \param ac: массив, где сохраню заменённые цвета из рисунка
///
void ChangeColour(BYTES &sSvg, uint32_t nClrWith, CFxArray<uint32_t, true> *ac)
{
    BYTE *psSvg = sSvg, *psEnd = psSvg + sSvg.GetCount(), sNewClr[8];
    i2a(nClrWith & 0x00ffffff, (char*) sNewClr, 8, 16);
	auto hook =[psSvg, &psEnd, sNewClr, &sSvg, &ac](BYTE *psQut) {
		// psQut == '#'
		BYTE *ps = psQut;
		if (ac) {
			uint32_t n;
			ps += xa2ui((const char*) ++ps, n);
			ac->Add(n);
		} else
			while (isxdigit(*++ps))
				;
		// #e2ebf0
		// 01234567
		//        7
		if (ps - psQut < 7)
            return psQut + 1;
		if (ps - psQut > 7) {
			int n = ps - psQut - 7;
            sSvg.RemND(ps - psSvg - n, n);
            psEnd = psSvg + sSvg.GetCount();
        }
		memcpy(psQut + 1, sNewClr, 6);
		return ps;
    };
    BYTE *psz = psSvg;
    while (psz < psEnd)
		if (*psz == '#')
            psz = hook(psz);
        else
            psz++;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief ChangeColour меняю все цвета в svg-рисунке на заданные в аргументе
/// \param sSvg: рисунок
/// \param aClrs: массив цветов, которые последовательно заменяют все цвета в sSvg.
/// Делается так: сначала замени все цвета в рисунке предыдущй одноименной ф-цией, которая запомнит
/// замененные цвета. Затем верни сохранённые цвета этого рисунка обратно этой ф-цией.
///
void ChangeColour(CFxArray<BYTE, true> &sSvg, const CFxArray<uint32_t, true> &aClrs)
{
	ASSERT(aClrs.GetCount());
	BYTE *psSvg = sSvg, *psEnd = psSvg + sSvg.GetCount(), sNewClr[8];
	int32_t iClr = 0;
	auto sclr =[&sNewClr](uint32_t clr) {
		i2a(clr & 0x00ffffff, (char*) sNewClr, 8, 16);
	};
	auto hook =[&aClrs, psSvg, &iClr, &psEnd, sNewClr, &sSvg, sclr](BYTE *psQut) {
		ASSERT(*psQut == '\"');
		BYTE *ps = psQut + 1;
		if (*ps != '#')
			return ps;
		while (isxdigit(*++ps))
			;
		// "#e2ebf0"
		// 012345678
		if (ps - psQut < 8)
			return psQut + 1;
		if (ps - psQut > 8) {
			int n = ps - psQut - 8;
			sSvg.RemND(ps - psSvg - n, n);
			psEnd = psSvg + sSvg.GetCount();
		}
		sclr(aClrs[iClr]);
		if (aClrs.GetCount() - iClr > 1)
			iClr++;
		memcpy(psQut + 2, sNewClr, 6);
		return (ps + 1);
	};
	BYTE *psz = psSvg;
	while (psz < psEnd)
		if (*psz == '\"')
			psz = hook(psz);
		else
			psz++;
}

#if defined __ITOG || defined __STRFLA
/////////////////////////////////////////////////////////////////////////////////////
/// \brief SetFile записываю файл, возвращаю к-во записанных байт. GetFile теперь шаблон в GetFile.h
/// \param pcszPthNm (в): путь и имя файла.
/// \param cpb (из): массив, откуда перемещаю текст в файл.
/// \param cnt (в): к-во байт в cpb
/// \return к-во записанных байт (размер файла)
///
size_t SetFile(CCHAR *pcszPthNm, CBYTE *cpb, const size_t cnt)
{
	ASSERT(pcszPthNm && fl::nBlockSz);
	FILE *pf;
	size_t nWritten = 0, szfl = 0, n;
	cerror = 0;
	// создаю новый файл, а если такой есть, его содержимое удаляется
	if (!(pf = fopen(pcszPthNm, "wb"))) {
		fl::CFileItemObtainer<fl::CStrings, fl::CString> osf(fl::strs), osd(fl::strs);
		n = cerror;
		throw err::CCException(n, SZ(SI_FLE, osf.obj()), time(NULL), SZ(SI_WRITE, osd.obj()), pcszPthNm,
		                       strerror(n), FileName(__FILE__, nullptr), __LINE__);
	}
	while (cnt > szfl && (nWritten = fwrite(cpb + szfl, 1, MIN(cnt - nWritten, fl::nBlockSz), pf)) > 0)
		szfl += nWritten;
	if ((n = cerror) != 0) {
		fl::CFileItemObtainer<fl::CStrings, fl::CString> osf(fl::strs), osd(fl::strs);
		fclose(pf);
		throw err::CCException(n, SZ(SI_FLE, osf.obj()), time(NULL), SZ(SI_WRITE, osd.obj()), pcszPthNm,
	                           strerror(n), FileName(__FILE__, nullptr), __LINE__);
	}
	fclose(pf);
	return szfl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief DelFile удаляю файл с диска
/// \param pcszPName: путь и имя удаляемого файла
/// \return трю/хрю
///
bool DelFile(const char *pcszPName)
{
	if (0 != remove(pcszPName)) { // zero - success
		int n = cerror;
		fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs), os2(fl::strs);
		fl::CString &s1 = os.GetItem(fl::ioffset, SI_FLE), &s2 = os2.GetItem(fl::ioffset, SI_WDEL);
		throw err::CCException(n, s1.str(), time(NULL), s2.str(), pcszPName, strerror(n),
							   FileName(__FILE__, nullptr), __LINE__);
	}
	return true;
}

#elif defined( __LINER) // defined __ITOG | defined __STRFLA

bool ValidPeriod(int iPeriod)
{
/*	switch(iPeriod)
	{
	case 1:
	case 5:
	case 15:
	case 30:
	case 60:
	case 240:
	case 1440:
	case 10080:
	case 43200:
		return true;
	default:
		return false;
	}*/
	// период может быть месячным, недельным, дневным, часовым и минутным. Но не промежуточным.
	return(iPeriod >= 43200 && iPeriod % 43200 == 0 ||
	    iPeriod >= 60 && iPeriod < 43200 && (iPeriod % 60 == 0 || iPeriod % 1440 == 0 || iPeriod % 10080 == 0) ||
	    iPeriod > 0 && iPeriod < 60);
}
#else
/////////////////////////////////////////////////////////////////////////////////////
/// \brief GetFile читаю файл, добавляю содержимое в массив, возвращаю к-во записанных байт.
/// \param pcszPthNm (в): путь и имя файла.
/// \param ab (из): массив, куда добавляю прочитанное из файла.
/// \return к-во записанных в 'ab' байт
///
int32_t GetFile(const char *pcszPthNm, BYTES &ab)
{
	ASSERT(pcszPthNm);
	ASSERT(fl::nBlockSz);
	FILE *pf;
	const int32_t ca = ab.GetCount();
	int32_t e, na = ca, nRead;
	cerror = 0;
	if (!(pf = fopen(pcszPthNm, "rb"))) {
		e = cerror;
		throw err::CCException(e, "%t пиздец нахуй '%s' не открывается т.к. '%s' (%d). См. '%s':%d.", time(NULL),
							   FileName(pcszPthNm, nullptr), strerror(e), e, FileName(__FILE__, nullptr), __LINE__);
	}
	ab.SetCountND(na + fl::nBlockSz);
	while ((nRead = (int32_t) fread(ab + na, 1, fl::nBlockSz, pf)) > 0) {
		na += nRead;
		if (nRead != fl::nBlockSz)
			break;
		ab.SetCountND(na + fl::nBlockSz);
	}
	if (feof(pf)) {
		fclose(pf);
		ab.SetCountND(na);
		return (na - ca); // к-во байт записанных в 'ab'
	}
	e = cerror;
	fclose(pf);
	ab.SetCountND(ca);
	throw err::CCException(e, "%t пиздец нахуй '%s' не читается т.к. '%s' (%d). См. '%s':%d.", time(NULL),
						   FileName(pcszPthNm, nullptr), strerror(e), e, FileName(__FILE__, nullptr), __LINE__);
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief SetFile записываю файл, возвращаю к-во записанных байт
/// \param pcszPthNm (в): путь и имя файла.
/// \param cpb (в): массив, откуда перемещаю текст в файл.
/// \param len (в): длина массива cpb
/// \return к-во записанных байт (размер файла)
///
size_t SetFile(const char *pcszPthNm, CBYTE *cpb, const size_t len)
{
	ASSERT(pcszPthNm && cpb && len > 0);
	ASSERT(fl::nBlockSz);
	FILE *pf;
	size_t nWritten = 0, szfl = 0, e;
	cerror = 0;
	// создаю новый файл, а если такой есть, его содержимое удаляется
	if (!(pf = fopen(pcszPthNm, "wb"))) {
		e = cerror;
		throw err::CCException(e, "%t пиздец нахуй '%s' не открывается т.к. '%s' (%d). См. '%s':%d.", time(NULL),
							   FileName(pcszPthNm, nullptr), strerror(e), e, FileName(__FILE__, nullptr), __LINE__);
	}
	while (len > szfl && (nWritten = fwrite(cpb + szfl, 1, MIN(len - nWritten, fl::nBlockSz), pf)) > 0)
		szfl += nWritten;
	if ((e = cerror) != 0) {
		fclose(pf);
		throw err::CCException(e, "%t пиздец нахуй '%s' не записывается т.к. '%s' (%d). См. '%s':%d.", time(NULL),
						   FileName(pcszPthNm, nullptr), strerror(e), e, FileName(__FILE__, nullptr), __LINE__);
	}
	fclose(pf);
	return szfl;
}

#endif// defined __ITOG | defined __STRFLA

#if defined OACRL_LIBRARY || defined LINER_PROJ
/* наименование периода
 * prdID - идентификатор периода из ../common/oa_defs.h
 */
const char* PeriodStr(oatp::CandlestickGranularity prdID)
{
	switch (prdID) {
	case S5: // 5 second candlesticks, minute alignment,
		return "S5";
	case S10: // 10 second candlesticks, minute alignment,
		return "S10";
	case S15: // 15 second candlesticks, minute alignment,
		return "S15";
	case S30: // 30 second candlesticks, minute alignment,
		return "S30";
	case M1: // 1 minute candlesticks, minute alignment,
		return "M1";
	case M2: // 2 minute candlesticks, hour alignment,
		return "M2";
	case M4: // 4 minute candlesticks, hour alignment,
		return "M4";
	case M5: // 5 minute candlesticks, hour alignment,
		return "M5";
	case M10: // 10 minute candlesticks, hour alignment,
		return "M10";
	case M15: // 15 minute candlesticks, hour alignment,
		return "M15";
	case M30: // 30 minute candlesticks, hour alignment,
		return "M30";
	case H1: // 1 hour candlesticks, hour alignment,
		return "H1";
	case H2: // 2 hour candlesticks, day alignment,
		return "H2";
	case H3: // 3 hour candlesticks, day alignment,
		return "H3";
	case H4: // 4 hour candlesticks, day alignment,
		return "H4";
	case H6: // 6 hour candlesticks, day alignment,
		return "H6";
	case H8: // 8 hour candlesticks, day alignment,
		return "H8";
	case H12: // 12 hour candlesticks, day alignment,
		return "H12";
	case D: // 1 day candlesticks, day alignment,
		return "D";
	case W: // 1 week candlesticks, aligned to start of week,
		return "W";
	case M: // 1 month candlesticks, aligned to first day of the month */
		return "M";
	}
	ASSERT(false);
	return nullptr;
}
/*	thread_local static char pszPrd[BUFLEN32];
	pszPrd[0] = '\0';
	if (nPrdMin <= 0)
		return pszPrd;
	// месяц
	if(nPrdMin >= 43200)	// / 43200 > 0 && nPrdMin % 43200 == 0)
		snprintf(pszPrd, BUFLEN32, "MN%d", nPrdMin / 43200);
	// неделя
	else if(nPrdMin < 43200 && nPrdMin / 10080 > 0 && nPrdMin % 10080 == 0)
		snprintf(pszPrd, BUFLEN32, "W%d", nPrdMin / 10080);
	// день
	else if(nPrdMin < 10080 && nPrdMin / 1440 > 0 && nPrdMin % 1440 == 0)
		snprintf(pszPrd, BUFLEN32, "D%d", nPrdMin / 1440);
	// час
	else if(nPrdMin < 1440 && nPrdMin / 60 > 0 && nPrdMin % 60 == 0)
		snprintf(pszPrd, BUFLEN32, "H%d", nPrdMin / 60);
	// минута
	else if(nPrdMin < 60)	// && (nPrdMin == 1 || nPrdMin / 5 > 0 && nPrdMin % 5 == 0))
		snprintf(pszPrd, BUFLEN32, "M%d", nPrdMin);
	return pszPrd;*/


const char* PeriodSecStr(int nPrdSec)
{
	switch (nPrdSec) {
	case PS5: // 5 second candlesticks, minute alignment,
		return "S5";
	case PS10: // 10 second candlesticks, minute alignment,
		return "S10";
	case PS15: // 15 second candlesticks, minute alignment,
		return "S15";
	case PS30: // 30 second candlesticks, minute alignment,
		return "S30";
	case PM1: // 1 minute candlesticks, minute alignment,
		return "M1";
	case PM2: // 2 minute candlesticks, hour alignment,
		return "M2";
	case PM4: // 4 minute candlesticks, hour alignment,
		return "M4";
	case PM5: // 5 minute candlesticks, hour alignment,
		return "M5";
	case PM10: // 10 minute candlesticks, hour alignment,
		return "M10";
	case PM15: // 15 minute candlesticks, hour alignment,
		return "M15";
	case PM30: // 30 minute candlesticks, hour alignment,
		return "M30";
	case PH1: // 1 hour candlesticks, hour alignment,
		return "H1";
	case PH2: // 2 hour candlesticks, day alignment,
		return "H2";
	case PH3: // 3 hour candlesticks, day alignment,
		return "H3";
	case PH4: // 4 hour candlesticks, day alignment,
		return "H4";
	case PH6: // 6 hour candlesticks, day alignment,
		return "H6";
	case PH8: // 8 hour candlesticks, day alignment,
		return "H8";
	case PH12: // 12 hour candlesticks, day alignment,
		return "H12";
	case PD1: // 1 day candlesticks, day alignment,
		return "D";
	case PW1: // 1 week candlesticks, aligned to start of week,
		return "W";
	case PMN1: // 1 month candlesticks, aligned to first day of the month */
		return "M";
	}
	ASSERT(false);
	return nullptr;
}

/* Period возвращает период в секундах, или нуль, если строка не содержит
 * один из известных периодов.
 */
int PeriodSec(const char* pszPeriod)
{
	int iPeriod = 0;
	while (!isalpha(*pszPeriod))
		pszPeriod++;
	switch (*pszPeriod++) {
	case 'S':
		if (isdigit(*pszPeriod))
			iPeriod = atoi(pszPeriod);
		break;
	case 'M':
		if (isdigit(*pszPeriod))
			iPeriod = 60 * atoi(pszPeriod);
		else if (!isalpha(*pszPeriod))
			iPeriod = 24 * 3600 * 30;
		break;
	case 'H':
		if (isdigit(*pszPeriod))
			iPeriod = 3600 * atoi(pszPeriod);
		break;
	case 'D':
		if (!isalnum(*pszPeriod))
			iPeriod = 24 * 3600;
		break;
	case 'W':
		if (!isalnum(*pszPeriod))
			iPeriod = 24 * 3600 * 7;
		break;
	}
	return iPeriod;
}

// возвращаю период инструмента в минутах
// pszInstrPrd - инструмент и период в формате EURUSD,M15 - имя инструмента, запятая, имя периода
// pszInstr - инструмент
// nInstrLen - длина pszInstr
int ParseInstrPrd(const char *pszInstrPrd, char *pszInstr, const int nInstrLen)
{
	ASSERT(nInstrLen > 0);
	int i;
	for (i = 0; pszInstrPrd[i] != ',' && pszInstrPrd[i] != '\0' && i < nInstrLen; i++)
		pszInstr[i] = pszInstrPrd[i];
	pszInstr[i] = '\0';
	if (i >= nInstrLen)
		return 0;
	return PeriodSec(pszInstrPrd + i + 1);
}

int ParseInstrPrdInplace(char *pszInstrPrd, const int nStrLen)
{
	ASSERT(nStrLen > 0);
	int i = 0;
	while (pszInstrPrd[i] != ',' && pszInstrPrd[i] != '\0' && i < nStrLen)
		i++;
	pszInstrPrd[i] = '\0';
	if (i >= nStrLen)
		return 0;
	return PeriodSec(pszInstrPrd + i + 1);
}

float PrevFibo(float fLevel);
float NextFibo(float fLevel)
{
	float fInt;
	fLevel = modff(fLevel, &fInt);
	if (fLevel < 0.f || fInt < 0.f)				// -1,382		-0,382
		return(fInt - PrevFibo(fabsf(fLevel)));	// -1 - 0,236	 0 - 0,236
	if (DCMP_LESS(fLevel, 0.236f, 0.0001f))
		fLevel = 0.236f;
	else
	if (DCMP_LESS(fLevel, 0.382f, 0.0001f))
		fLevel = 0.382f;
	else
	if (DCMP_LESS(fLevel, 0.5f, 0.01f))
		fLevel = 0.5f;
	else
	if (DCMP_LESS(fLevel, 0.618f, 0.0001f))
		fLevel = 0.618f;
	else
	if (DCMP_LESS(fLevel, 0.764f, 0.0001f))
		fLevel = 0.764f;
	else
		fLevel = 1.f;
	return(fInt + fLevel);
}

float PrevFibo(float fLevel)
{
	float fInt = 0.f;
	fLevel = modff(fLevel, &fInt);
	if (fLevel < 0.f || fInt < 0.f)
		return(fInt - NextFibo(fabsf(fLevel)));
	if (DCMP_EQUAL(fLevel, 0.f, .0001f)) {
		fLevel = 0.764f;
		fInt -= 1.f;
	} else
	if (DCMP_MORE(fLevel, 0.764f, 0.0001f))
		fLevel = 0.764f;
	else
	if (DCMP_MORE(fLevel, 0.618f, 0.0001f))
		fLevel = 0.618f;
	else
	if (DCMP_MORE(fLevel, 0.5f, 0.01f))
		fLevel = 0.5f;
	else
	if (DCMP_MORE(fLevel, 0.382f, 0.0001f))
		fLevel = 0.382f;
	else
	if (DCMP_MORE(fLevel, 0.236f, 0.0001f))
		fLevel = 0.236f;
	else
		fLevel = 0.f;
	return(fInt + fLevel);
}

// идентификатор периода из oa_defs.h по периоду в секундах
oatp::CandlestickGranularity PeriodID(int nPrdSec)
{
	switch (nPrdSec) {
	case PS5:		// 5 second candlesticks, minute alignment,
		return S5;
	case PS10:		// 10 second candlesticks, minute alignment,
		return S10;
	case PS15:		// 15 second candlesticks, minute alignment,
		return S15;
	case PS30:		// 30 second candlesticks, minute alignment,
		return S30;
	case PM1:		// 1 minute candlesticks, minute alignment,
		return M1;
	case PM2:		// 2 minute candlesticks, hour alignment,
		return M2;
	case PM4:		// 4 minute candlesticks, hour alignment,
		return M4;
	case PM5:		// 5 minute candlesticks, hour alignment,
		return M5;
	case PM10:		// 10 minute candlesticks, hour alignment,
		return M10;
	case PM15:		// 15 minute candlesticks, hour alignment,
		return M15;
	case PM30:		// 30 minute candlesticks, hour alignment,
		return M30;
	case PH1:		// 1 hour candlesticks, hour alignment,
		return H1;
	case PH2:		// 2 hour candlesticks, day alignment,
		return H2;
	case PH3:		// 3 hour candlesticks, day alignment,
		return H3;
	case PH4:		// 4 hour candlesticks, day alignment,
		return H4;
	case PH6:		// 6 hour candlesticks, day alignment,
		return H6;
	case PH8:		// 8 hour candlesticks, day alignment,
		return H8;
	case PH12:		// 12 hour candlesticks, day alignment,
		return H12;
	case PD1:		// 1 day candlesticks, day alignment,
		return D;
	case PW1:		// 1 week candlesticks, aligned to start of week,
		return W;
	case PMN1:		// 1 month candlesticks, aligned to first day of the month */
		return M;
	}
	ASSERT(false);
	return nullptr;
}

/* PeriodSec период в секундах по идентификатору
 * nPrdID - идентификатор из oa_defs.h
 */
int PeriodSec(oatp::CandlestickGranularity nPrdID)
{
	switch (nPrdID) {
	case S5: // 5 second candlesticks, minute alignment,
		return PS5;
	case S10: // 10 second candlesticks, minute alignment,
		return PS10;
	case S15: // 15 second candlesticks, minute alignment,
		return PS15;
	case S30: // 30 second candlesticks, minute alignment,
		return PS30;
	case M1: // 1 minute candlesticks, minute alignment,
		return PM1;
	case M2: // 2 minute candlesticks, hour alignment,
		return PM2;
	case M4: // 4 minute candlesticks, hour alignment,
		return PM4;
	case M5: // 5 minute candlesticks, hour alignment,
		return PM5;
	case M10: // 10 minute candlesticks, hour alignment,
		return PM10;
	case M15: // 15 minute candlesticks, hour alignment,
		return PM15;
	case M30: // 30 minute candlesticks, hour alignment,
		return PM30;
	case H1: // 1 hour candlesticks, hour alignment,
		return PH1;
	case H2: // 2 hour candlesticks, day alignment,
		return PH2;
	case H3: // 3 hour candlesticks, day alignment,
		return PH3;
	case H4: // 4 hour candlesticks, day alignment,
		return PH4;
	case H6: // 6 hour candlesticks, day alignment,
		return PH6;
	case H8: // 8 hour candlesticks, day alignment,
		return PH8;
	case H12: // 12 hour candlesticks, day alignment,
		return PH12;
	case D: // 1 day candlesticks, day alignment,
		return PD1;
	case W: // 1 week candlesticks, aligned to start of week,
		return PW1;
	case M: // 1 month candlesticks, aligned to first day of the month */
		return PM1;
	}
}
#endif //defined OACRL_LIBRARY || defined LINER_PROJ
}// namespace glb
