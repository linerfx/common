#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_flInd — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_flInd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "defs.h"
namespace fl::a {

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief SetItem осуществляет логику целостности индексированного массива и не пустого эл-та
/// массива.
/// Если эл-т "пустой" (fl::CFileItem::IsEmpty() == true) он станет не действительным в массиве 'A'.
/// Если эл-т не "пустой", и в массиве 'A' есть уинк.индексы, их целостность не будет нарушена:
/// если объект 'o' есть в файле, он будет изменён, если его нет в файле, будет добавлен новый.
/// Если в массиве 'A' нет уник.индексов, объект 'o' будет добавлен или изменён в файле 'a' в
/// зависимости от наличия его ид-ра на вводе: если ид-р 'iItm' действительный, объект в файле
/// будет заменен на 'o', если ид-р не действительный, будет добавлен новый обект.
/// \param a: файловый массив от fl::a::CvBase или fl::a::CiShrd
/// \param o: файловый объект от fl::CFileItem. В зависимости от значения ид-ра объека, объект 'o'
/// будет добавлен в массив 'a' или изменён в массиве 'a', не нарушая целостности уник.индексов.
/// Если объект пустой o.IsEmpty(), и его ид-р действительный, то файловый объект будет отмечен как
/// не действительный (удалён).
/// \param iind: индекс, как fl::ioffset или специфический для массива 'a'
/// \param iItm: индекс в индексе 'iind' объекта 'o' в файловом массиве 'a'. Если ид-р действи-
/// тельный, файловый объект будет заменен в массиве 'a' на новый объект 'o', если ид-р не дей-
/// ствительный, не пустой объект 'o' будет добавлен в массив 'a'.
/// \return базовый ид-р объекта 'o' в файловом массиве 'a'. Может быть не действительным.
///
template<class A, class O>
int32_t SetItem(A &a, O &o, int16_t iind, int32_t iItm)
{
	ASSERT(&a != nullptr && &o != nullptr);
	if (o.IsEmpty()) { // пустой эл-т однозначно становится не действительным (если есть его ид-р)
		if (iItm >= 0)
			a.Invalidate(iind, iItm);
		iItm = INV32_BIND;
	} // если есть уник.индексы, сохраняется их целостность
	else if (a.UnicIndices(nullptr))
		iItm = (iItm < 0) ? a.AddUnic(o) : a.EditUnic(o, iind, iItm);
	else // если нет уник.индексов, не пустой эл-т добавляется или редактируется в зависимости от наличия ид-ра
		iItm = (iItm < 0) ? a.Add(o) : a.Edit(o, iind, iItm);
	return iItm;
}

}// namespace fl::a