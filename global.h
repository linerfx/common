#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл global.cpр — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * global.cpр is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <cctype>
#include <math.h>
#include <cctype>
#include <cstring>
#include "defs.h" // 'liner' definisions
#include <cstdio>
#include <clocale>
#include "./exception.h"
#include "./strdef.h"
namespace glb {
const char* FileName(const char *pcszPath, int *pnLen);

////////////////////////////////////////////////////
/// \brief The CharCase enum регистр для ф-ции str_utf8Case()
///
enum CharCase {
	lower,
	upper
};

/* читаю действительное число из pcsz (завершённая нулём строка), возвращаю
 * к-во прочитанных символов, или 0, если на вводе нет числа
 * cnt - к-во прочитанных символов
 */
template <class Real>// Real может быть любым действительным
int a2f(Real &fResult, const char *pcsz, const int cnt)
{
	const struct lconv *pl = localeconv();
	int i = 0;
	int16_t nInt = 0,// к-во символов десятичной части числа
		nDec = -1;	// к-во символов дробной части числа
	bool bSign = false;
	while (isspace(*pcsz))
		pcsz++;
	if (*pcsz == '-') {
		bSign = true;
		i++;
	}
	fResult = 0.0;
	for (; i < cnt && (pcsz[i] >= '0' && pcsz[i] <= '9' || nDec < 0 && pcsz[i] == *pl->decimal_point); i++) {
		if (pcsz[i] == *pl->decimal_point) {
			nDec = 1; // точность включает разделитель
			continue;
		}
		if (nDec >= 0) {
			fResult += (pcsz[i] - '0') / pow(10.0, nDec);
			nDec++;
		} else {
			fResult = fResult * 10.0 + (pcsz[i] - '0');
			nInt++;
		}
	}
	if (bSign)
		fResult = -fResult;
	if (nDec < 0)
		nDec = 0;
	ASSERT(i == nInt + nDec + bSign);
	return ((i == bSign) ? 0 : nInt + nDec + bSign);
}

/* получаю целое число из строки, возвращаю к-во прочитанных символов или 0,
 * если на вводе нет числа
 */
template <class Int>// Int может быт любым целым
int a2i(Int &result, const char *pcsz)
{
	const char *pcsz0 = pcsz, // start
			*pcszS = nullptr; // sign
	int iSign = 1;
	while (isspace(*pcsz))
		pcsz++;
	if(*pcsz == '-') {
		iSign = -1;
		pcszS = pcsz++;
	} else if(*pcsz == '+')
		pcszS = pcsz++;
	result = 0;
	for ( ; *pcsz >= '0' && *pcsz <= '9'; pcsz++) {
		result *= 10;
		result += *pcsz - '0';
	}
	result *= iSign;
	return ((pcszS && pcsz - pcszS == 1) ? 0 : (int) (pcsz - pcsz0));
}

/* получаю целое число из строки, возвращаю к-во прочитанных символов или 0,
 * если на вводе нет числа
 */
template <class Int>// Int может быт любым целым
int a2i(Int &result, const char *pcsz, const int cnt)
{
	const char *pcsz0 = pcsz, // start
			*pcszS = nullptr; // sign
	int iSign = 1;
	while (isspace(*pcsz))
		pcsz++;
	if(*pcsz == '-') {
		iSign = -1;
		pcszS = pcsz++;
	} else if(*pcsz == '+')
		pcszS = pcsz++;
	result = 0;
	for ( ; pcsz - pcsz0 < cnt && *pcsz >= '0' && *pcsz <= '9'; pcsz++) {
		result *= 10;
		result += *pcsz - '0';
	}
	result *= iSign;
	return ((pcszS && pcsz - pcszS == 1) ? 0 : (int) (pcsz - pcsz0));
}

// приведение целого беззнакового, или знакового числа в строку, завешённую нулём
// возвращаю к-во символов в строке без учёта завершающего нуля
template<class Type>
int i2a(Type num, char *pszTxt, const int nTxtLen, const int iRadix)
{
	ASSERT(iRadix == 10 || iRadix == 16 || iRadix == 2);
	int iSign = 1, i = nTxtLen;
	char c;
	if (num < 0) {
		iSign = -1;
		num *= (Type) iSign;
	}
	if (i > 0)
		pszTxt[--i] = '\0';
	for ( ; i > 0 && num > 0; num /= (Type) iRadix) {
		c = (num % (Type) iRadix);
		pszTxt[--i] = (c < 10) ? c + '0' : c - 10 + 'A';
	}
	if (iSign < 0 && i > 0)
		pszTxt[--i] = '-';
	if (i > 0)
		memmove(pszTxt, pszTxt + i, (unsigned int) (nTxtLen - i) * sizeof(char));
	return (nTxtLen - i - 1);
}

template<class TP>
TP Mn(TP v1, TP v2)
{
	return((v1 < v2) ? v1 : v2);
}

template<class TP>
TP Mx(TP v1, TP v2)
{
	return((v1 > v2) ? v1 : v2);
}

template <class Real>
Real fmn(Real f1, Real f2, Real fe)
{
	return(DCMP_LESS(f1, f2, fe) ? f1 : f2);
}

template <class Real>
Real fmx(Real f1, Real f2, Real fe)
{
	return(DCMP_MORE(f1, f2, fe) ? f1 : f2);
}

template <class Real>
int fcmp(Real f1, Real f2, Real eps)
{
	return (DCMP_MORE(f1, f2, eps) ? 1 : DCMP_LESS(f1, f2, eps) ? -1 : 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FindEqual находит идентичный объект базового типа, либо объекта, для которого переопре-
/// делены операторы сравнения. Для объектов лучше использовать следующую ф-цию.
/// \param pa: просматриваемый массив, эл-ты которого упорядочены по возрастанию.
/// \param cnt: к-во эл-тов массива 'pa'.
/// \param v: искомый объект.
/// \return индекс искомого эл-та в 'pa'.
///
template<class TP>
int FindEqual(const TP *pa, const int cnt, const TP &v)
{
	int a = 0, i, z = cnt - 1;
	for (i = z / 2; a <= z; i = a + (z - a) / 2)
		if (v > pa[i])
			a = i + 1;
		else if (v < pa[i])
			z = i - 1;
		else {// assert массив упорядочен по возрастанию (не уникальный индекс)
			ASSERT(!i || v >= pa[i - 1]);
			ASSERT(i == cnt - 1 || v <= pa[i + 1]);
			return i;
		}
	return -1; // эл-т не найден
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FindNearest находит идентичный 'v' объект в массиве 'pa'.
/// \param pa: массив эл-тов, упорядоченный по возрастанию по тому значению, которое сравнивается в cmp
/// \param cnt: к-во эл-тов массива 'pa'
/// \param v: искомый объект.
/// \param cmp: ф-ция сравнения объектов. Возвращает 1 если первый аргумент больше второго, -1, если меньше
/// 0 если равны.
/// \return индекс равного, или первого большего элемента в упорядоченном по возрастанию массиве 'pa'.
/// Возвращаемое значение может быть равным 'cnt', если 'v' больше последнего эл-та в 'pa'
///
template<class TP>
int FindEqual(const TP *pa, const int cnt, const TP &v, int (*cmp)(const TP &o, const TP &o2))
{
	int a = 0, i, c, z = cnt - 1;
	for (i = z / 2; a <= z; i = a + (z - a) / 2) {
		c = cmp(v, pa[i]);
		if (c > 0)
			a = i + 1;
		else if (c < 0)
			z = i - 1;
		else {// массив упорядочен по возрастанию (не уникальный индекс)
			ASSERT(!i || cmp(v, pa[i - 1]) >= 0);
			ASSERT(i == cnt - 1 || cmp(v, pa[i + 1]) <= 0);
			return i;
		}
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FindNearest находит идентичный, или первый больший эл-т в массиве 'pa'.
/// \param pa: просматриваемый массив, эл-ты которого упорядочены по возрастанию.
/// \param z: к-во эл-тов массива 'pa'.
/// \param v: искомый объект.
/// \return Возвращает индекс равного, или первого большего элемента в 'pa'.
/// Возвращаемое значение может быть равным к-ву элементов, если 'v' больше последнего эл-та в 'pa'
///
template<class TP>
int FindNearest(const TP *pa, const int cnt, const TP &v)
{
	ASSERT(cnt > 0);
	int a = 0, i, z = cnt - 1;
	for (i = z / 2; a < z; i = a + (z - a) / 2)
		if (v > pa[i])
			a = MIN(z, i + 1);
		else if (v < pa[i])
			z = MAX(a, i - 1);
		else
			break;
	if (a == z) {
		ASSERT(i == a);
		if (v > pa[i])
			i++;
	}
	ASSERT(i <= cnt && (i == cnt || v <= pa[i]));
	return i;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FindNearest находит ближайший или идентичный 'v' объект в массиве 'pa'.
/// \param pa: массив эл-тов, упорядоченный по возрастанию по тому значению, которое сравнивается в cmp
/// \param cnt: к-во эл-тов массива 'pa'
/// \param v: искомый объект.
/// \param cmp: ф-ция сравнения объектов. Возвращает 1 если первый аргумент больше второго, -1, если меньше
/// 0 если равны.
/// \return индекс равного, или первого большего элемента в упорядоченном по возрастанию массиве 'pa'.
/// Возвращаемое значение может быть равным 'cnt', если 'v' больше последнего эл-та в 'pa'
///
template<class TP>
int FindNearest(const TP *pa, const int cnt, const TP &v, int (*cmp)(const TP &o, const TP &o2))
{
	ASSERT(cnt > 0);
	int a = 0, i, z = cnt - 1, c;
	for (i = z / 2; a < z; i = a + (z - a) / 2) {
		c = cmp(v, pa[i]);
		if (c > 0)
			a = MIN(z, i + 1);
		else if (c < 0)
			z = MAX(a, i - 1);
		else
			break;
	}
	if (a == z) {
		ASSERT(i == a);
		if (cmp(v, pa[i]) > 0)
			i++;
	}
	ASSERT(i <= cnt && (i == cnt || cmp(v, pa[i]) <= 0));
	return i;
}


//////////////////////////////////////////////////////////////////////////////////
/// \brief Round округлит 'f' до определённой точности
/// \param FLOAT: действительное float или double
/// \param f: округляемое число
/// \param iPrecision: точность - к-во знаков после запятой
/// \return округлённое действительное
///
template<class FLOAT>
FLOAT Round(FLOAT f, int iPrecision)
{	// в С++11 pow и modf перегружены для всех действительных типов
	ASSERT(iPrecision >= 0);					// f: 1.234567
	FLOAT fi, pnt = pow(10, -iPrecision);		// pnt: 0,00001
	f = modf(f / pnt, &fi);						// f: .7; i: 123456
	if (fi >= .0 && DCMP_MOE(f, .5, .01))
		fi += 1.0;								// i: 123457
	else if (fi < .0 && DCMP_LOE(f, -.5, .01))
		fi -= 1.0;
	return (fi * pnt);							// 1.23457
}

// pnt: .00001

//////////////////////////////////////////////////////////////////////////////////
/// \brief Round округлит 'f'
/// \param FLOAT: действительное float или double
/// \param f: округляемое число
/// \param pnt: точность, не наименьшее. Если 1.0, округляю до целого. Кроме дробного,
/// как 0,001, может быть целым 10, 100, 1000 и т.д.
/// \return округлённое действительное
///
template<class FLOAT>
FLOAT Round(FLOAT f, FLOAT pnt)
{	// в С++11 modf перегружена для всех действительных типов
	FLOAT fi;
	f = modf(f / pnt, &fi);						// f: .7; i: 123456
	if (fi >= 0 && DCMP_MOE(f, .5, .01))
		fi += 1.0;								// i: 123457
	else if (fi < 0 && DCMP_LOE(f, -.5, .01))
		fi -= 1.0;
	return (fi * pnt);							// 1.23457
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief read_tp читаю любое целое или действительное число из ввода.
/// \param TP: любой базовый тип, или структура статич.размера
/// \param sz: размер объекта, или -1. В таком случае размер берётся sizeof(TP)
/// \param pb: ввод
/// \param npb: к-во байт на вводе
/// \return полученное число, или швырну исключение, если на вводе недостаточно байт.
///
template<class TP, int sz = -1>
inline TP read_tp(CBYTE *pb, int32_t npb)
{
	TP n;
	if (npb < sizeof n)
		throw err::CLocalException(SI_NED, time(NULL), FileName(__FILE__, nullptr), __LINE__);
	memcpy((void*) &n, pb, (sz < 0) ? sizeof n : sz);
	return n;
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief read_tp читаю любое целое или действительное число из ввода.
/// \param TP: любой базовый тип, или структура статич.размера
/// \param pb: ввод
/// \param npb: к-во байт на вводе
/// \return к-во прочитанных байт, или швырну исключение, если на вводе недостаточно байт.
///
template<class TP>
inline int32_t read_tp(TP &o, CBYTE *pb, int32_t npb)
{
	if (npb < sizeof o)
		throw err::CLocalException(SI_NED, time(NULL), FileName(__FILE__, nullptr), __LINE__);
	memcpy(&o, pb, sizeof o);
	return sizeof o;
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief read_raw читаю ввод в массив байт, швырну исключение, если запрашиваемое
/// к-во байт больше имеющегося.
/// \param pbDest: помещаю сюда пос-ть байт в запрашиваемом к-ве
/// \param nDestLen: запрашиваемое к-во байт
/// \param pbSrc: ввод
/// \param nSrcLen: к-во байт на вводе
/// \return к-во прочитанных байт (равное запрашиваемому), или швырну исключение,
/// если на вводе их недостаточно.
///
inline int32_t read_raw(BYTE *pbDest, int32_t nDestLen, CBYTE *pbSrc, int32_t nSrcLen)
{
	if (nSrcLen < nDestLen)
		throw err::CLocalException(SI_NED, time(NULL), __FILE__, __LINE__);
	memcpy(pbDest, pbSrc, nDestLen);
	return nDestLen;
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief pass_raw читаю длину пос-ти эл-тов указанного типа из ввода, проверяю их
/// наличие на вводе, возвращаю к-во прочитанных байт.
/// Ввод в формате:
/// 1) к-во эл-тов TP
/// 2) последовательность байт, указанного размера
/// Допускается пустая последовательность, но к-во (0) должно быть указано.
/// \param TP: тип числа на вводе, которое содержит длину последующей пос-ти байт.
/// \param cpb: ввод с сервера или из файла
/// \param cnt: к-во байт на вводе
/// \return к-во прочитанных байт, или швырну исключение, если на вводе недостаточно байт.
///
template<class TP>
inline int32_t pass_raw(CBYTE *cpb, cint32_t cnt)
{
	int32_t n = read_tp<int32_t>(cpb, cnt);
	if (n * sizeof(TP) > cnt - sizeof n)
		throw err::CLocalException(SI_NED, time(NULL), FileName(__FILE__, nullptr), __LINE__);
	return (sizeof n + n * sizeof(TP));
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief cmpi сравниваю 2 int'а. Ф-цыя предназначена для индексированного целочисленного массива
/// \param i1
/// \param i2
/// \return -1 | 0 | 1
///
template <class INT>
inline int cmpi(INT i1, INT i2)
{
	return ((i1 < i2) ? -1 : (i1 > i2) ? 1 : 0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// динамич.приведение типа - ссылки и указателя - макрокоманда
///
#define CAST(t, to, obj) (glb::Cast<t,to>(obj, __FILE__, __LINE__))

template<class T, class To>
inline To& Cast(T &o, CCHAR *pcszFl, int32_t iLn)
{
	ASSERT(&o != nullptr);
	try {
		return dynamic_cast<To&>(o);
	} catch (std::bad_cast &e) {
		throw err::CLocalException(SI_BADCAST, typeid(o).name(), typeid(To).name(), e.what(), pcszFl, iLn);
	}
}

template<class T, class To>
inline To* Cast(T *po, CCHAR *pcszFl, int32_t iLn)
{
	ASSERT(po != nullptr);
	try {
		return dynamic_cast<To*>(po);
	} catch (std::bad_cast &e) {
		throw err::CLocalException(SI_BADCAST, typeid(*po).name(), typeid(To).name(), e.what(), pcszFl, iLn);
	}
}

///////////////////////////////////////////////////////
}// namespace glb

namespace fl {
namespace a {
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief FindObject находит объект в файловом массиве по значению и базовому идентификатору (индекс файло-
/// вого отступа) в указанном не уникальном индексе и в указанном диапазоне.
/// Ф-ция сделана для поиска в не уникальном индексе. Нет смысла в вызове ф-ции, если iInd уникальный.
/// \param o: искомый объект из файла. Базовый ид-р должен быть известен.
/// \param iBas: базовый индекс (ид-р) объекта 'o'
/// \param iInd: не уникальный иднекс, в котором ищу объект. Если индекс уникальный, не ошибка, но глупость.
/// \param iStart: первый индекс (в iInd) в диапазоне включительно, или -1, если не задействован
/// \param iEnd: последний индекс (в iInd) в диапазоне включительно, или -1, если не задействован
/// \return идекс в iInd
///
/*template<class A, class I, class O, int nInds>
int32_t FindObject(const A &a, const O &o, int32_t iInd, int32_t iBas, int32_t iStart = 0, int32_t iEnd = -1)
{	// A::FindEqual использует второй объект, возвращаемый A::static_obj()
	O *po = NULL;
	a.static_obj(&po);
	ASSERT(&o != po);
	int32_t i = a.FindEqual(o, iInd, iBas, iStart, iEnd), iMid;
	const I *pi = a.GetIndex();
	if (i < 0 || iBas == pi->Bas(iInd, i))
		return i;// не найден, или найден идентичный эл-т
	// 'o' найден, нужно найти базовый инд-р среди равных в не уникальном индексе
	const int32_t cnt = pi->count();
	if (iStart < 0)
		iStart = 0;
	if (iEnd < 0 || iEnd >= cnt)
		iEnd = cnt - 1;
	iMid = i;
	while (++i <= iEnd) {
		a.GetItem(iInd, i, *po);
		if (pi->cmp(iInd, o, *po) != 0)
			break;
		if (iBas == pi->Bas(iInd, i))
			return i;// найден искомый эл-т в индексе iInd
	}
	for (i = iMid - 1; i >= iStart; i--) {
		a.GetItem(iInd, i, *po);
		if (pi->cmp(iInd, o, *po) != 0)
			return -1;
		if (iBas == pi->Bas(iInd, i))
			return i;// найден искомый эл-т в индексе iInd
	}
	return ((i < iStart) ? -1 : i);// m_obj содержит эл-т из файла, индексируемый i
}*/

#if defined __ITOG || defined __STRFLA

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CheckExistence проверит наличие объекта в индексированном файловом массиве fl::a::CvBase
/// или fl::a::CiShrd по всем индексам. Ф-ция имеет смысл только с массивами, в которых есть уникальные индексы
/// и если файловый массив не пустой. В таком случае она надёжно защищает файловый массив от нарушения целостнос-
/// ти уник.индексов.
/// Если в массиве уник.индексов нет, ф-ция вернёт -1 (объект не найден, он уникальный в этом массиве).
/// \param a: индексированный файловый массив fl::a::CvBase или fl::a::CiShrd.
/// \param o: искомый эл-т
/// \return:
/// -1 если объект не найден ни в одном уникальном индексе, значит аргумент - уникальный объект не нарушает
/// целостность массива с уникальными индексами, такой объект можно добавлять в массив или менять имеющийся объект;
/// INT_MIN если объект есть во всех уник.индексах, но не найден в одном из не уникальных индексов;
/// <(-1) если объект нарушает целостность одного из уникальных индексов своим отсутствием в одном из них или
/// наоборот, присутствием в одном из них. Этот индекс вычисляется (i * -1 - 1), где i - результат ф-ции;
/// >=0 базовый ид-р объекта 'o', если найден идентичный по всем индексам объект. В этом случае аргумент и
/// имеющийся в файле объект могут отличаться только не индексированными значениями. Это значит, что замена
/// имеющегося в файле объекта не приведёт к нарушению целостности индексированного массива, но добавление
/// такого объекта в файл не допускается.
///
/*template<class A, class I, class O, int nInds>
int32_t CheckExistence(const A &a, const O &o)
{
	ASSERT(a.IsOpen());
	int32_t i, j, nui, iObj, cnt = a.Count(), iBas;
	cint32_t *pi = a.UnicIndices(&nui);
*/	/* Невозможно найти идентичный объект в массиве без уник.индекса. Даже если по всем не
	 * уник.индексам объект будет похожим, нет гарантии, что он будет тем самым, идентичным */
/*	if (!cnt)
		return -1; // пустой массив
	// убедиться в уникальности объекта по всем уникальным индексам
	for (i = 0; i < nui; i++)
		if ((iObj = a.FindEqual(o, pi[i])) >= 0 && iObj < cnt) {
			if (i > 0)						// в таком случае 'o' нарушает целостность уник.индекса присутствием
				return ((pi[i] + 1) * -1);	// Т.е. ind[iret * -1 - 1]
			iBas = a.BaseIndex(pi[i], iObj);
			break;							// 'o' найден в первом уник.индексе
		}
	ASSERT(!i || i == nui);
	if (i == nui)
		return -1;	// уник.индексов нет, или 'o' не найден ни в одном уник.индексе
	ASSERT(!i && pi && nui);
	// первый индекс должен быть уникальным, другие могут располагаться не последовательно
	ASSERT(*pi == 0);*/
	/* 'o' найден в первом уник.индексе. Если и во всех других индексах (включая не уникальные) 'o'
	 * так же имеется, то можно вернуть базовый ид-р эл-та, как абсолютно идентичного заданному...
	 * Вперёд, по всем индексам, кроме pi[0]... */
/*	for (j = 0; j < nInds; j++) {
		if (!i && j == pi[i]) {
			i++;	// уже проверенный уник.индекс см. выше
			continue;
		}
		if (a.FindEqual(o, j, iBas) < 0) {
			if (i < nui && j == pi[i])
				return (j + 1) * -1;// 'o' не найден в уник.индексе (нарушает целостность отсутствием)
//			ASSERT(false);			// 'o' не найден в не уникальном индексе. Пока что х.з. что это может быть.
//			break;					// Когда (если) выяснится, нужно смотреть что возвращать
			return INT_MIN;
		}
		if (i < nui && j == pi[i])
			i++;
	}
	return ((j == nInds) ? iBas : -1);
}*/
#endif //__ITOG, __STRFLA

}// namespace a
}// namespace fl
