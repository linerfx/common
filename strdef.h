#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл strdef.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * strdef.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
// имя дирректории для хранения массива строк (folder)
#define S_STRAF				"Строки"
// имя индексированного файла со строками (str... aray)
#define S_STRA				S_STRAF
// ошибка: нет путей к хранилищу приложения (ошибка на старте).
#define S_NPATH				"в системе нет путей к хранилищу приложения"
// ошибка: нет путей к хранилищу приложения (ошибка на старте). 1) время; 2) имя .h/cpp-файла; 3) №/индекс строки
#define S_NPATHF			"%t" S_NPATH ". Файл \"%s\"; стр.%d."
// уникальный во всей системе индентификатор файлового массива строк (см. CFileMan)
#define S_FLID				"iTogStrings"
// CException init error
#define S_IE				"%t ошибка '%s' (%d) во время инициализации в \'%s\':%d."
#define S_IEU				"%t непредусмотернная ошибка во время инициализации в \'%s\':%d."
// байты/мегабайты
#define S_BYTES				"байт"
#define S_KB				"КБ"
#define S_MB				"МБ"
#define S_GB				"ГБ"
#define S_TB				"ТБ"

//#if defined __ITOG || defined __ITGCLNT
/////////////////////////////////////////////////////////////////////////////////////////////
// идентификаторы предопределённых строк. Значение каждого определения соответствует индексу
// строки в файле lang-ru.txt и соответственно индексу в файловом массиве glb::strs сервера iTog.
//
// %t функция завершилась без ошибок.
#define SI_NOE				0
//#endif// defined(__ITOG) || defined(__ITGCLNT)
// %t недостаточно сведений '%s':%d.
#define SI_NED				1
//#if defined __ITOG || defined __ITGCLNT || defined __STRFLA
// %t \'%s\' при запросе %*f %s в файле '%s':%d.
#define SI_SHMEM			2
// %t попытка превысить наибольший допустимый размер файла '%s' %*f %s на %*f %s в файле '%s':%d.
#define SI_FLSZ				3
// %t попытка читать за пределами файла '%s'. Размер файла %*f %s; запрашиваемое число %d в файл. позиции %d. %s:%d.
#define SI_OFL				4
// %t несоответствие к-ва элементов (%d и %d) в файловом массиве '%s'.
#define SI_INT_DISPARITY	5
// "%t исключение %d в файле '%s':%d." (err::CLocalException)
#define SI_LE				6
//#endif
// %t С-исключение '%s' (%d) в файле \'%s\':%d.
#define SI_CE				7
//#if defined __ITOG || defined __ITGCLNT || defined __STRFLA
// %t стандартное исключение \'%s\' в файле \'%s\':%d.
#define SI_SE				8
// %t неизвестное исключение в файле \'%s\':%d.
#define SI_UE				9
// %t файловый массив '%s' повреждён. Размер файла %*f %s. Исключение в файле '%s':%d
#define SI_FLDMGD			10
// %t слишком большой объект '%s' для файлового массива '%s'. Ошибка в файле '%s':%d.
#define SI_BGOBJ			11
// ид-р имени дирректории файлов журнала
#define SI_LOGD				12
// ид-р имени дирректории с рисунками
#define SI_PICTD			13
// исключение Too Many Items: "%t превышено к-во элементов '%s' в массиве '%s'. Файл '%s':%d."
#define SI_TMITMS			14
// Ошибка в iTgClnt: "%t в соединении отказано из-за ошибки '%s' (%d)."
#define SI_SKTE				15
// "Товар" - эл-т массива store::CGoods
#define SI_STPL				16
// "Товары" - подпись, мн.число для store::CGoods. Есть ещё "Товаров" IS_OSTPLS
#define SI_STPLS			17
// дирректория для файлового массива "Ингредиенты". См. "Ингредиент" SI_STPLEX
#define SI_STPLSEX			18
// "Приход" для store::CIncomes
#define SI_INCM				19
// "Расход" для store::CExpends
#define SI_EXPEND			20
// "Продукты" для store::CProducts
#define SI_PRDCTS			21
// имя дирректории файлового массива остатков товаров CResidues
#define SI_RSDS				22
// имя дирректории "отделы" с массивом отделов CDepts
#define SI_DEPTS			23
// ид-р стоки со словом "Меню" (имя папки/файла, так же используется в представлениях)
#define SI_MENUE			24
// "Сотрудники" для cafe::CCoworkers
#define SI_CWRKRS			25
// имя файлового массива "Контрагенты" массива cafe::CContractors
#define SI_CNTRCRS			26
// имя файлового массива "Счета" cafe::CBills
#define SI_BILLS			27
// %t ошибка сервера %d '%s' в файле '%s':%d.
#define SI_SRV_ERROR		28
// %t ошибка розетки %d '%s' в файле %s:%d.
#define SI_SKT_ERROR		29
// %t ошибка %d в потоке. Завершаю поток.
#define SI_SOCKET_ERROR		30
// SSL: %t сервер %sвстревожен тем что '%s' (%d).
#define SI_SRVALRT			31
// SSL: "охуенно " (встревожен сервер)
#define SI_SRVPZ			32
// SSL: %t сервер сообщает клиенту, что %sвстревожен тем что '%s' (%d).
#define SI_SRVSALRT			33
// "Подменю" для cafe::CSubmenues
#define SI_SUBMENUE			34
// "%t исключение в ф-ции потока сервера." Исключение, повлекшее завершение потока
#define SI_THREAD_EXIT		35
// добавление/редактирование эл-тов базы: "%t исключение %d при попытке удалённо добавить или редактировать объект. Файл '%s':%d"
#define	SI_OBJEDITE			36
// добавление/редактирование эл-тов базы: "%t Си-исключение '%s' при попытке удалённо добавить или редактировать объект. Файл '%s':%d."
#define	SI_COBJEDITE		37
// добавление/редактирование эл-тов базы: "%t неизвестное исключение при попытке удалённо добавить или редактировать объект. Файл '%s':%d"
#define	SI_UOBJEDITE		38
// ошибка записи в сеть: %t ошибка записи заказа '%s'. Ошибка возникла в файле '%s':%d.
#define SI_WRITEERROR		39
// %t клиент отлючен. Local: %s:%d, peer: '%s', %s:%d;
#define SI_SKTD				40
// %t недостаточно сведений в ответе на запрос '%s'. Файл '%s':%d.
#define SI_NEDN				41
// имя файла фонового изображения по-умолчанию "фон-по-умолчанию.jpg"
#define	SI_BGI				42
// ид-р строки с текстом "Представьтесь пожалуйста"
#define	SI_WLCMUN			43
// ид-р строки с текстом "Введите ваш пароль"
#define	SI_WLCMPW			44
// ид-р строки с текстом кнопки "Авторизация" для представления приветствия и прочих
#define SI_ENTER			45
// ид-р строки с текстом кнопки "Регистрация" для представления приветствия и прочих
#define SI_RGNEW			46
// %t соединение установлено
#define SI_SSL_CONNECTED	47
// %t тип соединения изменился на '%s'
#define SI_MDCH				48
// не зашифрованное
#define SI_MDCH_UE			49
// %t новый клиент: '%s', '%s'.
#define SI_CLNTC			50
// сервер зашифрован
#define SI_MDCH_ES			51
// %t состояние сервера изменилось на '%s'.
#define SI_SKTSTCH			52
// не соединён
#define SI_SKTUC			53
// поиск сервера
#define SI_SKTLU			54
// соединяюсь
#define SI_SKTCNG			55
// соединён
#define SI_SKTCD			56
// связан
#define SI_SKTBD			57
// слушаю
#define SI_SKTLN			58
// закрываю
#define SI_SKTCLS			59
// %t сервер зашифрован
#define SI_SRVENCD			60
// %t сервер соединён
#define SI_SRVCNCD			61
// имя папки с сертификатами
#define SI_SSLCERTD			62
// имя папки с сертификатом подписанта (cert. authority)
#define SI_SSLCAD			63
// шаблон поиска файла сертификата: "*.arm"
#define SI_SSLCERTFNPAT		64
// имя файла сертификата. У меня "cs.crt"
#define SI_SSLCERTFN		65
// имя файла ключа. У меня "cs.key"
#define SI_SSLKEYFN			66
// %t начал прислушиваться...
#define SI_SRVLSTN			67
// %t ошибка! Нет сертификата авторитетного подписанта.
#define SI_SSLCAAE			68
// %t соединение было одобрено, но возникла ошибка '%s' (%d).
#define SI_SRVACCEPTE		69
// SSL: "%t кто-то %sошибся с шифрованием соединения: '%s' (%d)." первая строка может быть SI_SRVPZ
#define SI_SSLE				70
// %t '%s' запрашивает новое соединение.
#define SI_NEWCNCT			71
// %t задействован обмен ключами... Кое что нужно.
#define SI_PSKA				72
// %t %sошибка %d на рассмотрение от сервера '%s'.
#define SI_SSLEV			73
// "критическая" (ошибка)
#define SI_SSLEC			74
// %t сервер остановлен
#define SI_SRVTCLS			75
// имя сервера. У меня 'Coffea-Stimulus'
#define SI_SRVNM			76
// %t %d байт записано
#define SI_BTS_WRTN			77
// ид-р строки "Подчтовый адрес"
#define SI_PSTADRS			78
// "Сотрудник" cafe::CCoworker
#define SI_CWRKR			79
// ид-р строки с подписью "Имя"
#define SI_SIGN_NM			80
// ид-р строки с подписью "Отчество"
#define SI_SIGN_MDNM		81
// ид-р строки с подписью "Фамилия"
#define SI_SIGN_SNM			82
// ид-р строки с подписью "Индивидуальный идентификационный номер"
#define SI_SIGN_IIN			83
// ид-р строки с подписью "Пароль"
#define SI_SIGN_PW			84
// ид-р строки с подписью "Фотография"
#define SI_SIGN_PH			85
// ид-р строки с подписью "Получатель"
#define SI_RCVR				86
// ид-р строки с подписью "Страна"
#define SI_SIGN_CY			87
// ид-р строки с подписью "Область"
#define SI_SIGN_RGN			88
// ид-р строки с подписью "Город"
#define SI_SIGN_CTY			89
// ид-р строки с подписью "Район"
#define SI_SIGN_DT			90
// ид-р строки с подписью "Индекс"
#define SI_SIGN_INDX		91
// ид-р строки с подписью "Улица"
#define SI_SIGN_STRT		92
// ид-р строки с подписью "Строение №"
#define SI_SIGN_BUI			93
// ид-р строки с подписью "Название строения"
#define SI_SIGN_BN			94
// ид-р строки с подписью "Квартира/Комната"
#define SI_SIGN_FT			95
// ид-р строки с подписью "Заметки"
#define SI_SIGN_NTS			96
// ид-р строки с подписью "Причина увольнения"
#define SI_SIGN_FO			97
// ид-р строки с подписью "Номера телефонов"
#define SI_SIGN_TN			98
// ид-р строки с подписью "Соц.сети"
#define SI_SIGN_SMR			99
// ид-р строки с подписью "Поставщик"... См. "Поставщики"
#define SI_SPLR				100
// ид-р строки "Должности" для файлового массива cafe::CJobs
#define SI_JOBS				101
// ид-р строки с подписью "Дата рождения"
#define SI_SIGN_DB			102
// ид-р строки "Текст" - имя папки с текстовыми файлами
#define SI_TXT				103
// %t ошибка %s файла '%s': %s. Файл: %s:%d.
#define SI_FLE				104
// "при открытии" while opening
#define	SI_WOPEN			105
// чтения
#define SI_WREAD			106
// записи
#define SI_WRITE			107
// "удаления" while deletion
#define SI_WDEL				108
// ид-р строки "Изображение"
#define SI_SIMG				109
// ид-р строки "Наименование" как 'entry name'
#define	SI_SNM				110
// ид-р строки "Описание" description
#define SI_SDN				111
// ид-р строки "Должность"
#define SI_JOB				112
// ид-р строки "Файл изображения" (заголовок формы или подпись поля ввода)
#define SI_SELFL_TTL		113
// ид-р строки "Выбрать файл" (ид-р строки с подписью на кнопке)
#define SI_SELFL_BTN		114
// ид-р строки с именем файла рисунка "меню.svg"
#define SI_SVGMENUE			115
// алфавит для выбора символа для подчёркивания/назначения клавиши в формах/диалогах
#define SI_ALPHBT			116
// "Слишком большой файл"
#define SI_FL_TOOBIG		117
// %t попытка несанкционированного доступа п-ля %d к объекту '%s' id %d. Файл %s:%d.
#define SI_UA				118
// %t попытка п-ля %d изменить пароль '%s' на '%s'. Файл %s:%d.
#define SI_PWCH				119
// ид-р строки "Изменить" (пункт меню)
#define SI_EDIT				120
/* пос-ть печатных символов для создания пароля:
 * `1234567890-=\~!@#$%^&*()_+|qwertyuiop[]asdfghjkl;'zxcvbnm, ... cм. strings.txt стр. 129 */
#define SI_PWRD_RAW			121
// имя файла рисунка для кнопки "кнопка-вход.svg"
#define SI_SVGLOGIN			122
// имя файла рисунка для кнопки "кнопка-выход.svg"
#define SI_SVGLOGOUT		123
// имя файла svg-рисунка "кнопка-добавить.svg"
#define SI_SVGADD			124
// имя файла svg-рисунка "кнопка-регистрация.svg"
#define SI_SVGREG			125
// имя файла svg-рисунка "стрелка-влево.svg" кнопка гл.меню с рисунком SI_SVGLA
#define SI_SVGLA            126
// имя файла svg-рисунка "стрелка-вправо.svg" кнопка гл.меню с рисунком SI_SVGRA
#define SI_SVGRA            127
// имя файла svg-рисунка "кнопка-есть-ещё.svg"
#define SI_SVGMORE          128
// имя файла svg-рисунка "кнопка-применить.svg"
#define SI_SVGAPPLY			129
// имя файла рисунка "кнопка-настройки.svg"
#define SI_SVGOPTS			130
// имя файла рисунка "кнопка-удалить.svg"
#define SI_SVGDEL			131
// имя файла рисунка "кнопка-изменить.svg" (карандаш)
#define SI_SVGEDIT			132
// имя файла рисунка "кнопка-удалить-из-списка.svg"
#define SI_SVGDFL			133
// слово "организации" для связки с "Наименование" в основных настройках
#define SI_ENTPS			134
// слово "Настройки"
#define SI_OPTS				135
// "Адрес сервера"
#define SI_SRVA				136
// слово "Сохранить" для кнопки
#define SI_SAVE				137
// имя файла с настройками
#define SI_OPTFN			138
// имя администратора
#define SI_SUDO				139
// слово администратора
#define SI_SUDOPW			140
// t% попытка нарушить уникальный индекс в массиве '%s'. Объект '%s' уже есть в массиве. Файл '%s':%d
#define SI_UIBRK			141
// е.и. "кг"
#define SI_MUKG				142
// е.и. "килограмм"
#define SI_MUKGF			143
// е.и. "гр"
#define SI_MUGR				144
// е.и. "грамм"
#define SI_MUGRF			145
// е.и. "л"
#define SI_MUL				146
// е.и. "литр"
#define SI_MULF				147
// е.и. "мл"
#define SI_MUML				148
// е.и. "миллилитр"
#define SI_MUMLF			149
// е.и. "унц"
#define SI_MUOZ				150
// е.и. "унция"
#define SI_MUOZF			151
// е.и. "фнт"
#define SI_MUP				152
// е.и. "фунт"
#define SI_MUPF				153
// "Новый элемент меню"
#define SI_NEWMENUE			154
// ид-р строки с подписью "Единица измерения"
#define SI_SMU				155
// имя файла для е.и. mu::Out "расходные"
#define SI_MUOT				156
// ид-р строки с наименованием основной категории товаров по-умолчанию "Разное"
#define SI_SCINV			157
// ид-р строки с именем файла рисунка 'структура.svg'
#define SI_SVGSTRUCTURE		158
// ид-р строки с наименованием подкатегории "Оборудование" (категории "Разное")
#define SI_SCEQT			159
// ид-р строки с наименованием подкатегории "Мебель" (категории "Разное")
#define SI_SCFUR			160
// ид-р строки с наименованием подкатегории "Канцтовары" (категории "Разное")
#define SI_SCSTTR			161
// ид-р строки с наименованием подкатегории "Пищевые" (категории "Продукты")
#define SI_SCFD				162
// ид-р строки с наименованием подкатегории "Напитки" (категории "Продукты")
#define SI_SCDR				163
// ид-р строки с подписью поля ввода "Создан"
#define SI_CRTD				164
// ид-р строки с подписью поля ввода "Изменён"
#define SI_EDTD				165
// ид-р строки с подписью поля ввода "Стоимость"
#define SI_COST				166
// ид-р строки с подписью поля ввода "Выход"
#define SI_OUT				167
// ид-р строки с подписью поля ввода "Наценка"
#define SI_LD				168
// ид-р строки с подписью поля ввода "Цена"
#define SI_PRC				169
// ид-р строки с подписью "К-во"
#define SI_QTTS				170
// ид-р строки с подписью "Количество"
#define SI_QTT				171
// ид-р строки с подписью "е.и." - сокращение "Единица измерения" см. SI_SMU
#define SI_MUS				172
// ид-р строки с подписью "Потери"
#define SI_LOSS				173
// ид-р строки с подписью "Калорий"
#define SI_CAL				174
// ид-р строки с подписью "К" (сокращение для "калорий")
#define SI_SCAL				175
// ид-р строки с подписью "кК" (сокращение для "Кило-калорий")
#define SI_SKCAL			176
// ид-р строки с наименованием основной денежной единицы
#define SI_CRNC				177
// ид-р строки с сокращённым наименованием денежной единицы
#define SI_SCRNC			178
// ид-р строки с префиксным обозначением денежной единицы: Y от currency s̱ymbol
#define SI_YCRNC			179
// ид-р сроки с подписью "Округлить до"
#define SI_RNDTO			180
// ид-р строки с подписью "порядковый номер", у меня '№'
#define SI_SIGNN			181
// ид-р строки с подписью "Артикул"
#define SI_SARTL			182
// "Подотдел"
#define SI_SUBDEPT			183
// "Выбранные"
#define SI_SEL				184
// "%t попытка п-ля %d добавить пароль '%s'. Файл %s:%d". Эта ошибка аналогичная SI_PWCH
#define SI_PWA				185
// Ид-р строки с подписью "Тара"
#define SI_TARA				186
// Ид-р строки с подписью "Категория". Есть ещё "Категории" SI_CATS
#define SI_CAT				187
// "Поставщики" - категория/тип контрактника
#define SI_SPLRS			188
// "клиенты" - категория/тип контрактника
#define SI_CLNT				189
// "БИН/ИИН" - подпись
#define SI_PID				190
// "тел.№" - подпись
#define SI_TN				191
// "Ссылки" - подпись
#define SI_REFS				192
// "БИК" - подпись (bank id)
#define SI_BID				193
// "ИИК" - подпись (bank account)
#define SI_BA				194
// "Банк" - подпись (bank name)
#define SI_BN				195
// "Договор" - подпись
#define SI_AGRMNT			196
// "Подотделы" cм. SI_DEPTS
#define SI_SBDEPTS			197
// "Ингредиент" См. так же SI_STPLSEX
#define SI_STPLEX			198
// "Контрагент"
#define SI_CNTRCR			199
// "Отдел" есть ещё "Подотдел" SI_SUBDEPT
#define SI_DEPT				200
// "Продукт"
#define SI_PRDCT			201
// "Счёт"
#define SI_BILL				202
// "Новый"
#define SI_NEWM				203
// "Новая"
#define SI_NEWF				204
// "Новое"
#define SI_NEWN				205
// "Брутто"
#define SI_BRT				206
// "Нетто"
#define SI_NTT				207
// е.и. "шт"
#define SI_PG				208
// е.и. "штука"
#define SI_PGF				209
// ид-р строки с именем директории с договорами "Договора"
#define SI_AGRMNTS			210
// ид-р строки "Категории". Есть ещё "Категория" SI_CAT
#define SI_CATS				211
// ид-р строки "Товаров". Есть ещё "Товар" SI_STPL и "Товары" SI_STPLS
#define SI_OSTPLS			212
// ид-р строки ошибки/исключения std::bad_cast "Неправильное приведение типа %s к %s: %s; файл %s; строка %s."
#define SI_BADCAST			213
// кнопка-выбрать.svg
#define SI_SVGSELECT		214
// кнопка-отменить-выбор.svg
#define SI_SVGCALCELSEL		215
// кнопка '+' есть ещё SI_SVGADD
#define SI_SVGPLUS			216
// кнопка '-'
#define SI_SVGMINUS			217
// е.и. "г" - гектограмм/100 гр, сокращённая запись
#define SI_MUHGR			218
// е.и. "гектограмм" (100 гр)
#define SI_MUHGRF			219
// "Тип"
#define SI_TP				220
// "кнопка-доп-ингредиент.svg"
#define SI_SVGINGADD		221
// "кнопка-альт-ингредиент.svg"
#define SI_SVGINGALT		222
// подпись "Альтернатива для" - заголовок альт.ингр-ты для списка ингр-тов продукта
#define SI_INGTALT			223
// подпись "Дополнительно:" - заголовок доп.ингр-тов списка ингр-тов продукта
#define SI_INGTADD			224
// не задейстованные строки
#define SI_RES10			225
#define SI_RES11			226
#define SI_RES12			227
#define SI_RES13			228
#define SI_RES14			229
#define SI_RES15			230
#define SI_RES16			231
#define SI_RES17			232
#define SI_RES18			233
#define SI_RES19			234
#define SI_RES20			235
// программные
//...

namespace fl {

// обязательные индексы индексированных файловых массивов с объектами переменного размера
enum EIndices {
	inone = -3,	// нет индекса
	idel,		// массив индексов недействительных эл-тов в индексированных массивах
	ioffset,	// файловый отступ для файловых массивов с объектами переменной длины
	ibanal		/* первый индекс обязательный для индексированных массивов. Если массив содержит
				 * уник.индексы, ibanal должен быть уникальным */
};

}// namespace fl
//#endif// defined __ITOG || defined __ITGCLNT || defined __STRFLA

#if defined __ITOG || defined __STRFLA

// завешрённая нулём строка из файла const char* (Utf-8) по идентификатору строки SI_...
#define SZ(i,o) fl::strs.GetItem(fl::ioffset, i, o).str()
//#define SZO(i) fl::strs.GetItem(fl::ioffset, i, *fl::strs.static_obj(NULL)).str()
// массив байт glb::CFxArray<BYTE, true> из файла:
#define AB(i,o) fl::strs.GetItem(fl::ioffset, i, o).m_str

#elif defined (__ITGCLNT) // // defined __ITOG | defined __STRFLA

// если на сервере строки формата хранятся в индексированом файловом массиве, то клиент
// получает их на старте (см. tcp::CiTgClnt::CiTgClnt) в первом заказе

// завершённая нулём строка с сервера const char* (Utf-8) по идентификатору строки SI_...
#define SZ(i) fl::String(i)
//err::CLocalException::FrmtStr(i)

#endif// defined __ITGCLNT
