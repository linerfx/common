#pragma once
/*
 * Copyright 2023 Николай Вамбольдт
 *
 * Файл io_FileItem.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2023 Nikolay Vamboldt
 *
 * io_FileItem.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "defs.h"
#include "FxArray.h"
#include "iTgDefs.h"
#include "iTgOpts.h"

// размер базового эл-та CFileItem для чтения/записи в/из файла
#define SZ_VFI (sizeof(int64_t) * 2 + sizeof(int32_t) + sizeof(int16_t) * 2)

namespace fl {
template<class A, class O>
class CFileItemObtainer;
////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CFileItem class - базовый элемент индексированного файлового массива
/// с объектами как динамического так и статического размера т.к. последние наследуются и
/// имеют виртуальные ф-ции, поэтому их эл-ты не могут выстраиваться как в массиве ввиду
/// того, что С++ стандарт не гарантирует качества.
/// https://ru.stackoverflow.com/questions/1079060/Расположение-полей-в-памяти-при-наследовании
/// К объекту индексированного файлового массива, наследующего этот, предъявляются
/// следующие требования:
/// 1) наличие конструктора без параметров.
/// 2) желательно наличие конструктора перемещения 'move' С++11
/// 3) оператор присваивания '=' (копирования) и 'move'-оператор (перемещения)
/// 4) ф-ции копирования/присваивания:
/// 	unsigned int Obj::GetBinData(glb::CFxArray<BYTE, true>&) и
///		Obj& Obj::SetBinData(const glb::CFxArray<BYTE, true>&).
/// 5) ф-ция 'bool IsEmpty()' вернёт "да", если объект пустой
/// 6) ф-ция 'bool IsValid()' вернёт "да", если объект не действительный (может быть не пустой).
///		Не действительный объект подлежит замене в файле (окончательной утилизации).
/// 7) ф-ция 'void Invalidate()', которая быстро обнуляет объект, не освобождая память.
/// Об индексах важно: уникальность/похожесть объекта определяется сравнением ф-циями индексов, поэтому значимые
/// переменные класса следует индексировать. К примеру, ид-р объекта 1, в который вложен объект 2, такой как
/// fl::CBaseObj с индексом патриарха m_iPrnt. Эту переменную в объекте 2 следует индексировать т.к. при изменении
/// объекта 2 в ф-ции fl::CvBase::EditUnic(), если на вводе будет другой объект 2 равный имеющемуся объекту в файле,
/// файловый объект может быть изменён, а изменяемый удалён. А если не индексированные ид-ры m_iPrnt будут отличаться
/// у похожих объектов, один из них будет изменён с другим значением ид-ра m_iPrnt, а второй объект будет утерян.
/// Т.е. индексация нужна не только для быстрого доступа к объекту в массиве, но и для идентификации объекта, которая
/// влияет на его сохранность в файловом массиве.
/// CFileItem наследуют:
/// fl::CString, cafe::CBill, fl::CBaseObj, cafe::CContractor, cafe::CCoworker, store::CProduct, store::CStaple,
/// store::CResidue, store::CIncome, store::CExpend, cafe::CPostAdrs, cafe::CJob, store::CCat
///
class CFileItem {
public:
	template<class A, class O>
	friend class CFileItemObtainer;// имеет доступ к закрытой тут m_bUsed
	int64_t m_tmc,		// дата/время создания
		m_tme;			// дата/время изменения
	uint32_t m_flags;	// флаги ITM_*, такие как ITM_DEL или ITM_INNER...
	uint16_t m_nref;	// к-во ссылок на этот объект других объектов
	uint16_t m_nusr;	// пользовательские, как ид-р эл-та управления представления
private:// не учитываются в SZ_VFI:
	BYTE m_bUsed,		// существует только в памяти, означает что объект использутеся см. CFileItemObtainer
		m_abRes[7];		// выравниваю в памяти до 64 бит
public:// конструцыя/деструкцыя:
	CFileItem();
	CFileItem(const CFileItem &o);
	virtual ~CFileItem();
public:
	virtual void Init();
public:// функцыонал:
	virtual int GetBinData(BYTES &a) const;
	virtual int SetBinData(CBYTE *cpb, cint32_t cnt);
	static size_t sz();
	virtual const CFileItem& operator =(const CFileItem &o);
	virtual bool IsValid() const;
	virtual bool IsEmpty() const;
	virtual bool IsNull() const;
	virtual int32_t Invalidate(const int32_t iObj = -1, bool bRmFrmPrnt = false);
	bool Used() const;
};

inline CFileItem::CFileItem()
	: m_tmc(0)		// дата/время создания
	, m_tme(0)		// дата/время изменения
	, m_flags(0)	// флаги ITM_*, такие как ITM_DEL или ITM_INNER...
	, m_nref(0)		// к-во ссылок на этот объект других объектов
	, m_nusr(0)		// пользовательские сведения, как ид-р эл-та управления представления
	, m_bUsed(false)// в файл не пишется, существует только в памяти, означает что объект занят
{

}

inline CFileItem::CFileItem(const CFileItem &o)
	: m_tmc(o.m_tmc)	// дата/время создания
	, m_tme(o.m_tme)	// дата/время изменения
	, m_flags(o.m_flags)// флаги ITM_*, такие как ITM_DEL или ITM_INNER...
	, m_nref(o.m_nref)	// к-во ссылок на этот объект других объектов
	, m_nusr(o.m_nusr)	// пользовательские сведения, как ид-р эл-та управления представления
	, m_bUsed(o.m_bUsed)// в файл не пишется, существует только в памяти, означает что объект занят
{

}

inline CFileItem::~CFileItem()
{

}

inline void CFileItem::Init()
{
	m_flags = 0;
	m_nref = m_nusr = 0;
	m_tmc = m_tme = 0;
}

inline size_t CFileItem::sz()
{
	return SZ_VFI;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileItem::operator = можно использовать только с объектами статич.размера
/// предвариетльно убедившысь, что к-ва байт достаточно
/// \param cpb
/// \return ук-ль на следующий объект в 'cpb'
///
/*inline CBYTE* CFileItem::operator =(CBYTE *cpb)
{
	memcpy((void*) &m_tmc, cpb, SZ_VFI);
	return (cpb + SZ_VFI);
}*/

inline const CFileItem& CFileItem::operator =(const CFileItem &o)
{
	m_flags = o.m_flags;
	m_nref = o.m_nref;
	m_tmc = o.m_tmc;
	m_tme = o.m_tme;
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileItem::IsValid не действительный объект занчит удалённый в файле. Он не может
/// быть добавлен в файл, но действительный эл-т в файле может быть заменен на не действительный.
/// \return трю/хрю
///
inline bool CFileItem::IsValid() const
{
	ASSERT(!(m_flags & ITM_DEL) || !m_nref);
	return !(m_flags & ITM_DEL);
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileItem::IsEmpty правило пустого объекта: пустой объект не может быть записан
/// в файловый массив. Если имеющийся в файле не пустой объект редактируется пустым, файловый
/// объект отмечается как не действительный (- удаляется).
/// \return трю/хрю (пустой/непустой)
///
inline bool CFileItem::IsEmpty() const
{
	return (!m_flags && !m_nref && !m_tmc && !m_tme);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileItem::IsNull отсутствующий объект совершенно новый
/// \return трю/хрю
///
inline bool CFileItem::IsNull() const
{
	return (!m_flags && !m_nref && !m_tmc && !m_tme);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFileItem::Invalidate если к-во ссылок на объект обнуляется, отмечает объект как не
/// удалённый/действительный, подлежащий замене новым при поступлении оного в массив.
/// Эта ф-ция вызывается из ф-ции удаления файлового объекта fl::a::CvBase::Invalidate() или
/// fl::a::CiShrd::Invalidate() во время удаления объекта из файлового массива.
/// \param iObj: базовый ид-р этого, удаляемого объекта, чтобы удалить его из патриарха (для производного)
/// \param bRmFrmPrnt: удалить ли ид-р этого эл-та из объекта-патриарха? (для производного)
/// \return к-во ссылок на объект. Если нуль, то массив помещает ид-р этого объекта во внутренний
/// массив удалённых объектов.
///
inline int32_t CFileItem::Invalidate(const int32_t/*iObj = -1*/, bool /*bRmFrmPrnt = false*/)
{
	ASSERT(m_nref > 0);
	if (--m_nref == 0)
		m_flags |= ITM_DEL;
	return m_nref;
}

inline bool CFileItem::Used() const
{
	return m_bUsed;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CFileItemObtainer class - искуственный, ускоряет взаимодействие с массивом, предотвращает
/// использование одного и того же объекта в рекурсиях. Его следует использовать для получения файлового
/// объекта для чтения/записи эл-та индексированного массива.
///
template<class A, class O>
class CFileItemObtainer
{
	A &m_a;
	O &m_o;
public:
	CFileItemObtainer(A &a);
	CFileItemObtainer(A *pa);
	virtual ~CFileItemObtainer();
	O& obj();
	O& GetItem(int32_t iInd, int32_t iObj);
};

template<class A, class O>
inline CFileItemObtainer<A,O>::CFileItemObtainer(A &a)
	: m_o(*A::static_obj())
	, m_a(a)
{
	m_o.m_bUsed = true;
}

template<class A, class O>
inline CFileItemObtainer<A,O>::CFileItemObtainer(A *pa)
	: m_o(*A::static_obj())
	, m_a(*pa)
{
	m_o.m_bUsed = true;
}

template<class A, class O>
inline CFileItemObtainer<A,O>::~CFileItemObtainer()
{
	m_o.m_bUsed = false;
}

template<class A, class O>
inline O& CFileItemObtainer<A, O>::obj()
{
	return m_o;
}

template<class A, class O>
inline O& CFileItemObtainer<A, O>::GetItem(int32_t iInd, int32_t iObj)
{
	return m_a.GetItem(iInd, iObj, m_o);
}

//////////////////////////////////////////////////////////////////////////////////////
/// размер объектов статического размера классa CBaseObj для применения внутри ф-ций класса
///
class CBaseObj;
#define SZ_BSOBJ (sizeof (fl::CBaseObj::Type) + sizeof(int32_t) * 4)

///////////////////////////////////////////////////////////////////////////////////////
/// \brief The CBaseObj class - объект/эл-т с подэлементами, как продукт, отдел/подотдел
/// или меню/подменю
/// Наслудуют: store::CProduct, cafe::CDept. Используется напрямую как меню и подменю
///
class CBaseObj : public CFileItem
{
public:
	typedef CFileItem BAS;
	enum EIndices {
		iINames,	// уник.индекс по ид-рам строк-наименованиям
		iNames,		// уник.индекс по наименованиям
		iPrntNms,	// по индексам патриархов и наименованиям
		iFlags,		// по флагам
		iInds
	};
	// логично было бы перенести это перечисление в isc::CBaseItem
	enum Type {		// что из себя представляет этот объект:
		ndef,		// не определено
		dept,		// это отдел CDept, в m_ai ид-ры подотделов или меню
		subdept,	// это подотдел CDept, в m_ai ид-ры подотделов или меню
		menue,		// это меню, в m_ai ид-ры подменю
		submenue,	// это подменю, в m_ai ид-ры продуктов
		product,	// это продукт, в m_ai ид-ры ингредиентов
		// следующие ид-ры используются главным образом в isc::CBaseItem::tp
		staple,		// это товар, m_ai пустой
		ingrdt,		// ингредиент store::CStlEx, m_ai пустой
		cntrctr,	// контрактник, m_ai пустой
		cwrkr,		// сотрудник
		splr,		// поставщик (наследует контрактника)
		bill,		// счёт
		income,		// приход
		expend,		// расход
		// если будешь добавлять ид-р типа, не забудь о ф-ции Invalidate()
		types
	} m_tp;
	struct ID {
		Type tp;	// тип объекта-подэлемента
		int32_t i;	// базовый ид-р объекта
		ID() { tp = ndef; i = INV32_BIND; }
		ID(Type t, int32_t id) : tp(t), i(id) { }
	};
	int32_t m_iNm,	// ид-р строки с наименованием
		m_iPrnt,	// ид-р отдела для подотдела, ид-р подотдела для меню, ид-р меню для подменю
		m_iDn,		// ид-р строки - имени файла с описанием
		m_iImg;		// ид-р строки - имени файла с изображением
protected:
	// базовые ид-ры вложенных объектов упорядочены по возрастанию, добавлять ф-цией AddSubitem()
	glb::CFxArray<ID, true> m_ai;
public:
	CBaseObj();
	CBaseObj(Type tp);
	CBaseObj(const CBaseObj&);
	CBaseObj(CBaseObj&&);
	virtual ~CBaseObj() {}
public:// обязательные переопределения для эл-та индексированного файлового массива
	const CBaseObj& operator =(const CBaseObj &o);
	CBaseObj& operator =(CBaseObj &&o);
	virtual int GetBinData(BYTES &a) const override;
	virtual int SetBinData(const BYTE *pb, int cnt) override;
	virtual void Init() override;
	static size_t sz();
#ifdef __ITOG
	virtual int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt) override;
#endif// defined __ITOG
//	virtual bool IsValid() const override;
	virtual bool IsEmpty() const override;
	virtual bool IsNull() const override;
public:
	void AddSubitem(Type tp, int32_t id);
	const glb::CFxArray<ID, true>& Subitems() const;
	glb::CFxArray<ID, true>& Subitems();
	const ID& operator [](int i) const;
	ID& operator [](int i);
public:// индексы
	typedef int(*fcmp)(const CBaseObj&, const CBaseObj&);
#ifdef __ITOG
	static int cmp_names(const CBaseObj &a, const CBaseObj &z);
	static int cmp_prnt_nms(const CBaseObj &a, const CBaseObj &z);
#endif// defined __ITOG
	static int cmp_inames(const CBaseObj &a, const CBaseObj &z);
	static int cmp_flags(const CBaseObj &a, const CBaseObj &z);
};

inline const CBaseObj& CBaseObj::operator =(const CBaseObj &o)
{
	BAS::operator =(o);
	memcpy(&m_tp, &o.m_iNm, SZ_BSOBJ);
	m_ai = o.m_ai;
	return *this;
}

/////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::sz
/// \return размер объектов статич.размера, без учёта массивов
///
inline size_t CBaseObj::sz()
{
	return (BAS::sz() + SZ_BSOBJ);
}

inline CBaseObj& CBaseObj::operator =(CBaseObj &&o)
{
	BAS::operator =(o);
	memcpy(&m_tp, &o.m_iNm, SZ_BSOBJ);
	m_ai.Attach(o.m_ai);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::IsEmpty единственный обязательный параметр - наименование объекта. Если нет
/// наименования, объект подлежит удалению в файле даже если есть всё остальное.
/// \return трю/хрю
///
inline bool CBaseObj::IsEmpty() const
{	// наименование объекта - обязательный параметр. Если его нет, нет и объекта
	return (m_iNm == INV32_BIND);
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::IsNull отсутствующий объект совершенно новый
/// \return трю/хрю
///
inline bool CBaseObj::IsNull() const
{
	return (m_iNm == INV32_BIND && m_iDn == INV32_BIND && m_iImg == INV32_BIND &&
			!m_ai.GetCount() && fl::CFileItem::IsNull());
}

inline const glb::CFxArray<CBaseObj::ID, true>& CBaseObj::Subitems() const
{
	return m_ai;
}

inline glb::CFxArray<CBaseObj::ID, true>& CBaseObj::Subitems()
{
	return m_ai;
}

inline const CBaseObj::ID& CBaseObj::operator [](int i) const
{
	return m_ai[i];
}

inline CBaseObj::ID& CBaseObj::operator [](int i)
{
	return m_ai[i];
}

inline int CBaseObj::cmp_inames(const CBaseObj &a, const CBaseObj &z)
{
	return ((a.m_iNm < z.m_iNm) ? -1 : (a.m_iNm > z.m_iNm) ? 1 : 0);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::cmp_flags сравниваю объект по преоритету и флагам как ITM_DEL и прочим. В
/// нижнем байте флагов может находиться преоритет. Сначала сравниваю по преоритету, затем по
/// прочим флагам.
/// \param a, z: сравниваемые эл-ты
/// \return -1, 0 или 1
///
inline int CBaseObj::cmp_flags(const CBaseObj &a, const CBaseObj &z)
{
	BYTE na = (BYTE) (a.m_flags & 0x000000ff), nz = (BYTE) (z.m_flags & 0x000000ff);
	uint32_t fa = ITM_MNOIND(z.m_flags) >> 8 & 0x00ffffff,
		fz = ITM_MNOIND(z.m_flags) >> 8 & 0x00ffffff;
	return ((na > nz) ? 1 : (na < nz) ? -1 : (fa > fz) ? 1 : (fa < fz) ? -1 : 0);
}

}// namespace fl

#if defined __ITOG || defined __ITGCLNT

namespace cafe {
/////////////////////////////////////////////////////////////////////////////////////////////
/// class CDept
///
class CDept : public fl::CBaseObj
{
public:
	enum EIndices {
		iDptINm = fl::CBaseObj::iINames,
		iDptNm = fl::CBaseObj::iNames,
		iDptPrntNm = fl::CBaseObj::iPrntNms,
		iFlgs = fl::CBaseObj::iFlags,
		iDptInds
	};
private:
	/* массив категорий товаров для отдела в отличие от аналогичного массива категорий в настройках
	 * содержит только "концевые" категории без индексов строк с наименованиями */
	o::CiTgOpts::fCmpUCat m_afcc[1] = { o::CiTgOpts::CCat::cmpc };
public:
	o::CiTgOpts::AUCATS m_aCat; // допустимые для этого отдела концевые категории товаров в приходе/расходе
public:
	CDept();
	virtual ~CDept() {}
	virtual int GetBinData(BYTES &a) const override;
	virtual int SetBinData(CBYTE *cpb, int cnt) override;
	void DifCat(o::CiTgOpts::AUCATS &aiuc,
				glb::CFxArray<uint32_t, true> &aucAdd,
				glb::CFxArray<uint32_t, true> &aucRem) const;
#ifdef __ITOG
	// индексы...
	static int cmp_names(const CDept &a, const CDept &z);
	static int cmp_prnt_nms(const CDept &a, const CDept &z);
#endif// defined __ITOG
	static int cmp_inames(const CDept &a, const CDept &z);
	static int cmp_flags(const CDept &a, const CDept &z);
};

inline CDept::CDept()
	: CBaseObj(dept), m_aCat(16, m_afcc)
{

}

/*inline fl::CBaseObj::Type CDept::Type() const
{
	return (m_iPrnt >= 0) ? subdept : dept;
}*/

inline int CDept::GetBinData(BYTES &a) const
{
	int n = fl::CBaseObj::GetBinData(a);
	return n + m_aCat.GetBinData(a);
}

inline int CDept::cmp_inames(const CDept &a, const CDept &z)
{
	return fl::CBaseObj::cmp_inames(a, z);
}

inline int CDept::cmp_flags(const CDept &a, const CDept &z)
{
	return fl::CBaseObj::cmp_flags(a, z);
}

}// namespace cafe

#endif // defined __ITOG || __ITGCLNT
#ifdef __ITOG

namespace cafe {

inline int CDept::cmp_names(const CDept &a, const CDept &z)
{
	return fl::CBaseObj::cmp_names(a, z);
}

inline int CDept::cmp_prnt_nms(const CDept &a, const CDept &z)
{
	return fl::CBaseObj::cmp_prnt_nms(a, z);
}

}// namespace cafe

#endif // defined __ITOG