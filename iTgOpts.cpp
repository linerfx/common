/*
 * Copyright 2023 Николай Вамбольдт
 *
 * Файл iTgOps.cpp — часть программы iTogClient. iTogClient свободная
 * программа, вы можете перераспространять её и/или изменять на условиях
 * Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient распространяется в надежде, что она будут полезной, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2023 Nikolay Vamboldt
 *
 * iTgOps.cpp is part of iTogClient. iTogClient programm is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * iTogClient is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "iTgOpts.h"
#include "FxString.h"
#include "iTgDefs.h"
#ifdef __ITOG
#include "strdef.h"
#include "io_strings.h"
#include "../iTog/iTog/server.h"
namespace fl {
	extern CStrings strs;
}
namespace o {
	extern CiTgOpts opt;
}
#endif// defined __ITOG

namespace glb {
	extern thread_local const CFxString sAppPath;	// путь к хранилищу приложения
}
namespace o {// options

CiTgOpts::CiTgOpts()
	: m_nItemsToFetch(56)
	, m_cPrcn(2)// точность цены по-умолчанию 2
	, m_cr{ 0, 0, 0 }
	, m_aCat(8, m_afCmpCat)
	, m_iDept(INV32_BIND) // присваивается в CServer::onBinMessageReceived()::ITG_EDEPT(V); см. glb::SetOptDept()
	, m_iPrdctMU(o::iMuHg)
	, m_fRndTo(1.f)
{
	ASSERT((uint64_t) &m_aCat - (uint64_t) &m_nItemsToFetch == SZ_OPTITG);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::SetBinData читаю настройки п-ля с сервера, включая строки с наименованиями
/// \param cpb, cnt: ввод с сервера
/// \return к-во прочитанных байт
///
int32_t CiTgOpts::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	ASSERT(sizeof(CCat) == sizeof CCat::uf + sizeof CCat::is);
	int32_t i, n, ns;
	CCat c;
	// 1. сначала переменные статич.размера
	i = glb::read_raw((BYTE*) &m_nItemsToFetch, SZ_OPTITG, cpb, cnt);
	// 2а. к-во категорий товаров
	i += glb::read_tp(n, cpb + i, cnt - i);
	while (n-- > 0) {
		// 2б. категории товаров
		i += glb::read_tp(c, cpb + i, cnt - i);
		ASSERT(c.IsValid()); // в массиве только действительные категории
		m_aCat.Add(c);
	}// 3. периодические издержки
	i += m_aRecCosts.SetBinData(cpb + i, cnt - i);
	// 4. операционные издержки
	i += m_aTrnsCosts.SetBinData(cpb + i, cnt - i);
	// 5. единицы измерения
	i += m_amu.SetBinData(cpb + i, cnt - i);
	// индексы ид-ры наименований типов/категорий контрактников
	return (i + m_aiCnts.SetBinData(cpb + i, cnt - i));
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::GetBinData добавляю в массив сведения из настроек п-ля для отправки их
/// на сервер, расширяя прочими сведениями, как строки из fl::strs
/// \param a: ИЗ - куда записываю настройки п-ля
/// \return к-во байт, записанных в 'a'
///
int32_t CiTgOpts::GetBinData(BYTES &a) const
{
	ASSERT(sizeof(CCat) == sizeof CCat::uf + sizeof CCat::is);
	cint32_t cnt = a.GetCount();
	CCat c;
	// 1. сначала статич.сведения
	a.Add((CBYTE*) &m_nItemsToFetch, SZ_OPTITG);
	// 2. категории товаров CFxArray<CCat,true>::GetBinData(): 2а к-во категорий, 2б категории
	m_aCat.GetBinData(a);
	// 3. периодические издержки
	m_aRecCosts.GetBinData(a);
	// 4. операционные издержки
	m_aTrnsCosts.GetBinData(a);
	// 5. единицы измерения
	m_amu.GetBinData(a);
	// 6. ид-ры наименований типов/категорий контрактников
	m_aiCnts.GetBinData(a);
	// возвращаю к-во прочитанных (в этом объекте) байт
	return (a.GetCount() - cnt);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::cmp_cat сравнение категорий товаров.
/// То же store::CStaple::cmp_cat(const store::CStaple&, const store::CStaple&)
/// \param i, j: индексы сравниваемых эл-тов m_aCat
/// \return -1|0|1
///
int CiTgOpts::cmp_cat(int i, int j) const
{
	if (i == j)
		return 0;
	return CCat::cmpc(m_aCat.GetItem(0, i), m_aCat.GetItem(0, j));
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::cmp_cat сравниваю указанные уровни категорий
/// \param i, j: индексы сравниваемых категорий в индексе 0
/// \param iLvl: один из четырёх уровней SC_1,2,3,4
/// \return см. CiTgOpts::LvlsCmp
///
CiTgOpts::LvlsCmp CiTgOpts::cmp_cat(int i, int j, uint32_t uLvl) const
{
	ASSERT(uLvl == SC_1 || uLvl == SC_2 || uLvl == SC_3 || uLvl == SC_4);
	uint32_t a = m_aCat.GetItem(0, i).uf & uLvl,
		z = m_aCat.GetItem(0, j).uf & uLvl;
	return ((!a && !z) ? lceBth : !a ? lceFrst : !z ? lceScnd : (a == z) ? lcSame : lcDif);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::TopCats извлекаю из массива категорий только "конечные"
/// \param ac: сюда помещаю результат
/// \return к-во категорий в 'ac'
///
int CiTgOpts::TopCats(glb::CFxArray<uint32_t, true> &ac) const
{
	ASSERT(m_aCat.GetCount());
	CCat a, b;
	for (int i = 0, n = m_aCat.GetCount(); i < n - 1; i++) {
		a = m_aCat.GetItem(0, i);
		b = m_aCat.GetItem(0, i + 1);
		ASSERT(a.IsValid() && b.IsValid());// в массиве только действительные категории
		// если уровень категории 'a' больше или равен уровню следующей, значит категория 'a' конечная
		if (a.Sbcats() >= b.Sbcats())
			ac.Add(a.uf);
	}// последняя категория всегда конечная
	ASSERT(b.IsValid());
	ac.Add(b.uf);
	return ac.GetCount();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::TopCats извлекаю из массива категорий только "конечные"
/// \param ac: массив индексов конечных категорий
/// \return к-во конечных категорий
///
int CiTgOpts::TopCats(glb::CFxArray<int32_t, true> &ac) const
{
	ASSERT(m_aCat.GetCount());
	int i, n;
	for (i = 0, n = m_aCat.GetCount(); i < n - 1; i++)
		// если уровень категории 'i' больше или равен уровню следующей, значит категория 'i' конечная
		if (m_aCat.GetItem(0, i).Sbcats() >= m_aCat.GetItem(0, i + 1).Sbcats())
			ac.Add(i);
	// последняя категория всегда конечная
	ac.Add(i);
	return ac.GetCount();
}

#ifdef __ITOG
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CatStr собираю строку категории как Товары/Калькулируемые/Напитки
/// \param ufc: категория товара
/// \param sCat: ИЗ, результат
///
void CiTgOpts::CatStr(uint32_t ufc, glb::CFxString &sCat)
{	// наименование категории товара - от низкого до высокого уровня
	o::CiTgOpts::CCat c(ufc, INV32_IND);
	fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs);
	fl::CString &sf = os.obj();
	sCat.SetCountZeroND();
	for (int i = 3 * 8; i >= 0; i -= 8) // 4 категории: i = 24, 16, 8, 0.
		if (SC_1 << i & ufc) {			// 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff
			uint32_t u = 0;				// маска над-категорий
			for (int j = i + 8; j < 32; j += 8)
				u |= SC_1 << j;
			c.uf = ufc & ~u;			// исключаю над-категории из категории этого уровня
			c.is = o::opt.m_aCat.FindEqual(c, INV32_IND, 0);
			ASSERT(c.is >= 0 && c.is < o::opt.m_aCat.GetCount());
			c = o::opt.m_aCat.GetItem(0, c.is);
			fl::strs.GetItem(fl::ioffset, c.is, sf);
			if (i > 0)
				sf.m_str.Insert(0, '/');
			sCat.Insert(0, (CCHAR*) (CBYTE*) sf.m_str, sf.m_str.GetCount());
		}
}

////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::ReadFile читаю файл настроек п-ля без всякой синхронизации
/// Использует буфер обмена tcp::CServer::IOBUF
///
void CiTgOpts::ReadFile()
{
	ASSERT(glb::sAppPath.GetCount() > 0);
	int32_t i = 0, n;
	fl::CBinFile f;
	try {
		fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs);
		glb::CFxString sPath(glb::sAppPath);
		fl::CPath::AddName(sPath, SZ(SI_OPTFN, os.obj()));
		n = (int32_t) f.open(sPath);
		if (!n) {// нет файла настроек, категории товаров по-умолчанию:
			CCat ac[] = { { SC_INV, SI_SCINV }, { SC_FAD, SI_PRDCTS }
						 , { SC_EQUPT, SI_SCEQT }, { SC_FUR, SI_SCFUR }, { SC_STTR, SI_SCSTTR }
						 , { SC_FD, SI_SCFD }, { SC_DR, SI_SCDR } };
			for (int i = 0; i < sizeof ac / sizeof *ac; i++)
				ASSERT(ac[i].IsValid());
			m_aCat.Add(ac, sizeof ac / sizeof *ac);
			// единицы измерения по-умолчанию
			ASSERT(!m_amu.GetCount());
			// грамм - iMuGr
			m_amu.Add({ 1.0, SI_MUGRF, SI_MUGR });
			// килограмм - iMuKg
			m_amu.Add({ 1000.0, SI_MUKGF, SI_MUKG });
			// миллилитр - iMuMl
			m_amu.Add({ 1.0, SI_MUMLF, SI_MUML });
			// литр - iMuL
			m_amu.Add({ 1000.0, SI_MULF, SI_MUL });
			// эвердьюпойсная унция - iMuOz
			m_amu.Add({ 28.349523125, SI_MUOZF, SI_MUOZ });
			// такой жэ фунт - iMuFnt
			m_amu.Add({ 28.349523125 * 16.0, SI_MUPF, SI_MUP });
			// штучный товар без е.и. - iMuPg
			m_amu.Add({ .0, SI_PGF, SI_PG });
			// гектограмм - iMuHg (100 гр): "гектограмм", "г"
			m_amu.Add({ 100.0, SI_MUHGRF, SI_MUHGR });
			// категории контрактников по-умолчанию
			int32_t aic[] = { SI_SPLRS, SI_CLNT };
			m_aiCnts.SetCountND(0);
			m_aiCnts.Add(aic, sizeof aic / sizeof *aic);
			return;
		}// читаю файл
		BYTES &ab = *tcp::CServer::io_buf();
		ab.SetCountND(n);
		f.getcp(0, ab, n);
		f.close();
		SetBinData(ab, ab.GetCount());
	} catch (...) {
		if (f.IsOpen())
			f.close();
		throw;
	}
}

//////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::WriteFile записываю файл настроек п-ля без всякой синхронизации.
/// Использует буфер обмена tcp::CServer::IOBUF
///
void CiTgOpts::WriteFile() const
{
	ASSERT(glb::sAppPath.GetCount() > 0);
	fl::CBinFile f;
	try {
		fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs);
		fl::CString &sf = os.obj();
		glb::CFxString sPath(glb::sAppPath);
		fl::CPath::AddName(sPath, SZ(SI_OPTFN, sf));
		glb::CFxArray<BYTE, true> &ab = *tcp::CServer::io_buf();
		ab.SetCountZeroND();
		GetBinData(ab);
		f.open(sPath);
		f.putcp(0, ab, ab.GetCount());
		f.close();
	} catch (...) {
		if (f.IsOpen())
			f.close();
		throw;
	}
}
#endif// defined __ITOG

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::PrntMask нахожу маску патриарха. Например, есть категория SC_FUR, её
/// патриарх SC_INV (маска SC_1), а маска пиздёныша SC_3
/// \param uc: категория, патриарха которой нужно найти
/// \return SC_1|SC_2|SC_3|SC_4
///
uint32_t CiTgOpts::CCat::PrntMask(uint32_t uc)
{
	ASSERT(IsValid(uc));
	if (uc & SC_4)
		return SC_3|SC_2|SC_1;
	if (uc & SC_3)
		return SC_2|SC_1;
	if (uc & SC_2)
		return SC_1;
	return 0U;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::ChildMask маска пиздёныша указанной категории
/// \param uc
/// \return
///
uint32_t CiTgOpts::CCat::ChildMask(uint32_t uc)
{
	ASSERT(IsValid(uc));
	if (uc & SC_4)
		return 0U;
	if (uc & SC_3)
		return SC_4|SC_3|SC_2|SC_1;
	if (uc & SC_2)
		return SC_3|SC_2|SC_1;
	return SC_2|SC_1;
}

int CiTgOpts::CCat::Sbcats(uint32_t uc)
{
	ASSERT(IsValid(uc));
	if (uc & SC_4)
		return 4;
	if (uc & SC_3)
		return 3;
	if (uc & SC_2)
		return 2;
	return 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::Sbcats: к-во (под-)категорий
/// \return 1 - 4 включительно
///
int CiTgOpts::CCat::Sbcats() const
{
	ASSERT(IsValid());
	if (uf & SC_4)
		return 4;
	if (uf & SC_3)
		return 3;
	if (uf & SC_2)
		return 2;
	return 1;
}

int CiTgOpts::CCat::Bits(BYTE b)
{
	int i, cnt = 0;
	for (i = 0; i < 8; i++)
		if (b & 1 << i)
			cnt++;
	return cnt;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::IsValid действительность категории: это не должна быть маска или мусор
/// 1) Основная категория обязательная
/// 2) В каждой из четырёх (под-)категорий (в каждом байте) один бит
/// 3) Последовательность категорий нарушена, если подкатегория следует за отсутствующей (под-)категорией
/// \param cat: проверяемая категория, не маска
/// \return "трю" | "хрю"
///
bool CiTgOpts::CCat::IsValid(uint32_t cat)
{	// 1 бит в каждой из четырёх под-категорий
	int i, nPrev, n, cnt = 0;
	for (i = 0, nPrev = 1; i < 32; i += 8, nPrev = n)
		if ((n = Bits((cat & SC_1 << i) >> i & SC_1)) > 1)
			return false;	// в каждой под-категории должен быть только один бит
		else if (n) {
			if (!nPrev)
				return false;// нарушена последовательность
			cnt++;
		}
	return (cnt > 0);	// должна быть по меньшей мере одна категория
}

/////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::IsValid действительность категории
/// \return "трю" или "хрю"
///
bool CiTgOpts::CCat::IsValid() const
{
	return (IsValid(uf) && is != INV32_BIND);
}

///////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::Lvl
/// \return уровень категории 0 - 3
/// Например, SC_FAD и SC_INV уровень 0, SC_FD и SC_FUR уровень 1
///
/*int CiTgOpts::CCat::Lvl() const
{
	ASSERT(IsValid(uf));
	int i = 0;
	while (i < 32) {
		if (!(uf & SC_1 << i))
			break;
		i += 8;
	}
	return (i / 8);
}*/

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::cmpc сравниваю категории по каждому байту. Для индекса категорий CiTgOpts::m_aCat
/// \param a, z: сравниваемые категории
/// \return -1|0|1
///
int CiTgOpts::CCat::cmpc(const CCat &a, const CCat &z)
{
//	ASSERT(a.IsValid(a.uf) && z.IsValid(z.uf)); для поиска допускается 0-категория или любая другая. Проверяй на вводе
	// первая категория
	if ((a.uf & SC_1) < (z.uf & SC_1))
		return -1;
	if ((a.uf & SC_1) > (z.uf & SC_1))
		return 1;
	// вторая категория
	if ((a.uf & SC_2) < (z.uf & SC_2))
		return -1;
	if ((a.uf & SC_2) > (z.uf & SC_2))
		return 1;
	// третья категория
	if ((a.uf & SC_3) < (z.uf & SC_3))
		return -1;
	if ((a.uf & SC_3) > (z.uf & SC_3))
		return 1;
	// четвёртая категория
	if ((a.uf & SC_4) < (z.uf & SC_4))
		return -1;
	if ((a.uf & SC_4) > (z.uf & SC_4))
		return 1;
	// все категории равны
	return 0;
}

int CiTgOpts::CCat::cmpc(const uint32_t &a, const uint32_t &z)
{
//	ASSERT(IsValid(a) && IsValid(z)); для поиска допускается 0-категория или любая другая. Проверяй на вводе
	// первая категория
	if ((a & SC_1) < (z & SC_1))
		return -1;
	if ((a & SC_1) > (z & SC_1))
		return 1;
	// вторая категория
	if ((a & SC_2) < (z & SC_2))
		return -1;
	if ((a & SC_2) > (z & SC_2))
		return 1;
	// третья категория
	if ((a & SC_3) < (z & SC_3))
		return -1;
	if ((a & SC_3) > (z & SC_3))
		return 1;
	// четвёртая категория
	if ((a & SC_4) < (z & SC_4))
		return -1;
	if ((a & SC_4) > (z & SC_4))
		return 1;
	// все категории равны
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiTgOpts::CCat::cmpm сравнение масок категорий как битовых полей слеванаправо.
/// Если сравниваю 7 и 5 (бин 111 и 101), результат будет 1.
/// Если сравниваю 1 и 6 (бин 001 и 110), результат будет -1.
/// Ф-ция используется для индекса по каждой подкатегории товара. Например, чтобы выбрать
/// из таблицы объекты по категории "вся еда", нужно найти ближайший эл-т по второй подка-
/// тегории и линейно выбрать все с первым байтом 0x02
/// \param a, z: сравниваемые байты
/// \return -1|0|1
///
/*int CiTgOpts::CCat::cmpm(const uint32_t &a, const uint32_t &z)
{
	int i, n;
	uint32_t b, y;
	for (i = 0; i < 32; i++) {
		b = a & 0x80000000 >> i;
		y = z & 0x80000000 >> i;
		if (b < y)
			return -1;
		else if (b > y)
			return 1;
	}
	return 0;
}*/

}// namespace o