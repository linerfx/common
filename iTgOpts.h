#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл Options.h — часть программы iTogClient. iTogClient свободная
 * программа, вы можете перераспространять её и/или изменять на условиях
 * Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient распространяется в надежде, что она будут полезной, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * Options.h is part of iTogClient. iTogClient programm is free software:
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * iTogClient is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include "defs.h"
#include "FxArray.h"
#include "MeasureUnit.h"
#ifdef __ITOG
#include "FxString.h"
#endif
// размер статич.эл-тов CiTgOpts: int32_t m_nItemsToFetch, BYTE m_cPrcn и BYTE m_cr[3]
#define SZ_OPTITG	(18 * sizeof(int32_t))
namespace o {

// индексы е.и. в CiTgOpts::m_amu
enum EMu {
	iMuGr,	// грамм
	iMuKg,	// килограмм
	iMuMl,	// милилитр
	iMuL,	// литр
	iMuOz,	// унция
	iMuFnt,	// фунт
	iMuPg,	// штучный товар
	iMuHg,	// гектограмм (hectogramm) 100 гр
	iMuCnt
};

/////////////////////////////////////////////////////////////////////////////////
/// \brief The CiTgOpts class содержит настройки п-ля.
///
class CiTgOpts
{
public:
	// периодические издержки
	struct CRecCost {	// recurring cost - периодические издержки
		enum EPeriod {
			hmonth,		// пол-месяца
			month,		// месяц
			quarter,	// квартал
			hyear,		// полугодие
			year		// год
		};
		int64_t nday;	// дата (первого) списания
		double fCost;	// сумма издержки
		int32_t iNm;	// ид-р строки с наименованием
		EPeriod period;
		CRecCost() : period(month), nday(0), fCost(.0), iNm(INV32_BIND) { }
	};
	struct CTransCost { // transaction cost - операционные издержки
		double fProCost;// процент расхода
		int32_t iNm;	// ид-р строки с наименованием
		CTransCost() : fProCost(.0), iNm(INV32_BIND) { }
	};
	class CCat {		// категория товара
	public:
		uint32_t uf;	// флаги категории SC_* как SC_FD
		int32_t is;		// ид-р строки с наименованием категории
	public:
		CCat() : uf(0), is(INV32_BIND) { }
		CCat(const CCat &c) : uf(c.uf), is(c.is) { }
		CCat(uint32_t nf, int32_t i) : uf(nf), is(i) { }
		const CCat& operator =(const CCat &o) { uf = o.uf; is = o.is; return *this; }
	public:
		static uint32_t PrntMask(uint32_t uc);
		static uint32_t ChildMask(uint32_t uc);
		static int Sbcats(uint32_t uc);
		uint32_t PrntMask() const;
		uint32_t ChildMask() const;
		int Sbcats() const;
		static int Bits(BYTE b);
	public:
		static bool IsValid(uint32_t cat);
		bool IsValid() const;
		// индексы индексированного массива:
		static int cmpc(const CCat &a, const CCat &z);
		static int cmpc(const uint32_t &a, const uint32_t &z);
	};
public:
	/* категории товаров тут все, включая повторяющиеся в основных категориях, как SC_FAD в SC_FD
	 * потому что каждая категория содержит ид-р строки-наименования. В других массивах содержатся
	 * конечные категории, без повторений. См. cafe::CDept::m_aCat */
	typedef int(*fCmpCat)(const CCat&, const CCat&);
	typedef int(*fCmpUCat)(const uint32_t&, const uint32_t&);
	typedef glb::CFxArrayInd<CCat, 1, true> ACATS;		// массив категорий товаров
	typedef glb::CFxArrayInd<uint32_t, 1, true> AUCATS;	// массив категорий товаров
	fCmpCat m_afCmpCat[1] = { CCat::cmpc };
	int32_t m_nItemsToFetch,	// стандартное к-во эл-тов запроса
			m_iDept,			// ид-р отдела, чьи меню выводятся на старте клиента
			m_iPrdctMU,			// индекс е.и. продукта по-умолчанию (см. store::CProduct::m_iMu)
			m_aiRes[13];		// другие настройки
	float m_fRndTo;				// округлить цену до... 0,001; 0,1; 1; 5; 10; 100 и т.д.
	BYTE m_cPrcn;				// точность цены - к-во после запятой (0, 2 или как угодно)
	BYTE m_cr[3];				// выравниваю до 64 б.
	ACATS m_aCat;				// все категории товаров, как SC_FD
	glb::CFxArray<CRecCost, true>
		m_aRecCosts;			// периодические издержки
	glb::CFxArray<CTransCost, true>
		m_aTrnsCosts;			// операционные расходы в той пос-ти, что накладываются на цену
	glb::CFxArray<store::CMu, true>
		m_amu;					// все единицы измерения по-умолчанию индексы EMu
	glb::CFxArray<int32_t, true>
		m_aiCnts;				// ид-ры строк - наименований типов/категорий контрактников
public:
	CiTgOpts();
	int32_t SetBinData(CBYTE *cpb, cint32_t cnt);
	int32_t GetBinData(BYTES&) const;
	int cmp_cat(int i, int j) const;
	enum LvlsCmp {
		lcSame,		// равны на указанном урове
		lcDif,		// отличаются на указанном урове
		lceFrst,	// первый отсутствует второй присутствует
		lceScnd,	// второй отсутствует первый присутствует
		lceBth		// оба пустые на указанном уровне
	};
	LvlsCmp cmp_cat(int i, int j, uint32_t uLvl) const;
	int TopCats(glb::CFxArray<uint32_t,true> &ac) const;
	int TopCats(glb::CFxArray<int32_t,true> &ac) const;
//	const store::CMu& ProductMU() const;
#ifdef __ITOG
	void CatStr(uint32_t ufc, glb::CFxString &sCat);
	void ReadFile();
	void WriteFile() const;
#endif
};

/*inline const store::CMu& CiTgOpts::ProductMU() const
{
	return m_amu[m_iProductMu];
}*/

inline uint32_t CiTgOpts::CCat::PrntMask() const
{
	return PrntMask(uf);
}

inline uint32_t CiTgOpts::CCat::ChildMask() const
{
	return ChildMask(uf);
}

} // namespace o