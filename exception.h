#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл exception.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * exception.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
//#include "defs.h"
#include <cstdint>	// типы (C) int32_t и т.д.
#include <cstring> // memcpy, strerror
//#include "incl_fxarray.h" // правильно включает CFxString и CFxArray
#ifdef OACRL_LIBRARY
#include "../curl/include/curl/curl.h" // CURLcode
#endif // defined OACRL_LIBRARY

namespace err {

/* CLocalException содержит ошибку
 */
class CLocalException {
public:
	// m_iStrID содержит базовый индекс строки с ошибкой из файлового массива строк (см. strdef.h)
	// в клиенте этот индекс транслируется статич. ф-цией FrmtStr
	int32_t m_iStrID;
	CLocalException(int iStrErrorID, ...);
	CLocalException(const char *pcsz, ...);
#if defined __ITGCLNT || defined __LTTS
//	static const char* FrmtStr(int32_t iStr);
#endif // defined __ITGCLNT || defined __LTTS
	virtual ~CLocalException() {}
};

#ifdef OACRL_LIBRARY
class CCrlException {
public:
	CURLcode m_code;
	CCrlException(CURLcode code, const char *pcszFile, const int nLine);
};
#endif // defined OACRL_LIBRARY

/* CCException содержит errno-ошибку с описанием от операционки.
 */
class CCException {
public:
	int m_errno;
	CCException(int nCerrno, const char *pcszFormat, ...);
	const char *text() const {
		return strerror(m_errno);
	}
};

/*
 * CInputException содержит ошибку ввода с сервера Oanda. Если на вводе
 * ожидается не то, что там содержится, или не содержится.
 */
class CInputException {
public:
	int m_id, // идентификатор на вводе
		m_idExpected, // ожидаемый идентификатор
		m_ind;	// индекс события в тексте
	CInputException(int id, int idExpected, int i, const char *pcszFormat, ...);
};

#ifdef OACRL_LIBRARY
/*
 * COaException - только oacrl может создать объект и швырнуть кому угодно
 * размещённая в "куче" строка не дублируется при копировании
 */
class COaException {	// 128 бит
public:
	int m_nhttp,		// http error: 400, 401, 403, 404, 405
		m_nRejReason;	// причина отказа - идентификатор из oa_defs.h
	const char *m_pcszDescr; // указатель 64 бит.
	COaException(/*const */COaException &&ce) {
		memcpy(this, &ce, sizeof *this);
		ce.m_pcszDescr = nullptr;// только конечный получатель удаляет строку
	}// конструктор читает текст-ответ сервера с описанием ошибки
	COaException(int http, const char *pcsz, const int nlen, const char *pcszFile, const int cnLine);
	~COaException() {
		if (m_pcszDescr)
			delete [] m_pcszDescr;
	}
};
#endif // defined OACRL_LIBRARY

} // namespace err

//#endif // CEXCEPTION_H
