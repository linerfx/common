#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_intFilei.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_intFilei.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_flIndLocal.h"

namespace fl {
namespace a {

/////////////////////////////////////////////////////////////////////////////////////
/// \brief The CiLcl class - индексированный файловый массив эл-тов статического
/// размера
///
template <class Obj, int nInds>
class CiLcl : public CIntFile<Obj>
{
	typedef CIntFile<Obj> BAS;
	Obj m_obj; // GetItem() возвращает m_obj

protected:// индексы в разделяемой памяти
	i::CIndILcl<Obj, nInds> m_ind;

public:	// конструкцыи
	CiLcl(typename i::CIndBase<Obj>::fcmp *ppf);

public: // операцыи
	virtual int64_t open(const char *pcszPN) override;
	virtual void close(bool bChanged);
	void close() = delete;

public: // извлечение по индексу, как: int_fileI[ii][i]
	const Obj& GetItem(int32_t iInd, int32_t iObj) const;
	Obj& GetItem(int32_t iInd, int32_t iObj);
	const i::CIndILcl<Obj, nInds>* GetIndex() const;
	i::CIndILcl<Obj, nInds>* GetIndex();
};

////////////////////////////////////////////////////////////////////////////////////
/// \brief CiLcl<Obj, nInds>::CiLcl
/// \param ppf - массив ф-ций сравнения для каждого индекса (в к-ве nInds)
///
template<class Obj, int nInds>
CiLcl<Obj, nInds>::CiLcl(typename i::CIndBase<Obj>::fcmp *ppf)
	: i::CIndBase<Obj>::CIndBase(ppf)
{
	ASSERT(nInds > 0 && nInds < 100);
}

template<class Obj, int nInds>
long CiLcl<Obj, nInds>::open(const char *pcszPN)
{
	ASSERT(nInds > 0 && nInds < 99);
	if (m_ind.open(pcszPN) != BAS::open((const char*) pcszPN)) // швырнет исключение, если что не так...
		throw err::CLocalException(SI_INT_DISPARITY, time(NULL), m_ind.count(), BAS::count(),
	                               glb::FileName(pcszPN, strlen(pcszPN)));
	return this->count();
}

template<class Obj, int nInds>
void CiLcl<Obj, nInds>::close(bool bChanged)
{
	ASSERT(nInds > 0 && nInds < 99);
	ASSERT(BAS::IsOpen());
	// если файл небыл изменён, индексы остаются прежними...
	if (!bChanged) {
		BAS::close();
		return;
	}
	m_ind.close();
	BAS::close();
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief CiLcl::operator[] читает эл-т по отступу, вызывается в CIndex::operator[](int).
/// \param nOffset: отступ в файле хранится в индексах m_ai
/// \return объект из файлового массива
///
template<class Obj, int nInds>
const Obj& CiLcl<Obj, nInds>::GetItem(int32_t iInd, int32_t iObj) const
{
	const int iBas = m_ind.Bas(iInd, iObj);
	BAS::get(iBas, &m_obj);
	return m_obj;
}

template<class Obj, int nInds>
const i::CIndILcl<Obj, nInds>* CiLcl<Obj, nInds>::GetIndex() const
{
	return &m_ind;
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CiLcl::operator[] предоставляет возможность использовать оператор двухмерного
/// массива: o[i][offset], где 'i' - индекс для этого оператора, а 'offset' - отступ для
/// оператора 'const Object& CIndex<Array, Object, nInds>::operator[](int i)'
/// \param i: индекс индекса 'm_ai'
/// \return указанный индекс типа CIndex
///
template<class Obj, int nInds>
i::CIndILcl<Obj, nInds>* CiLcl<Obj, nInds>::GetIndex()
{
	return &m_ind;
}

}// namespace a
}// namespace fl