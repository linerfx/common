#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_path.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_path.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxString.h"
#include "FxVArray.h"

namespace fl {
/*************************************************************************
 * 27.06.2021 CPath: пути к файлам, с которыми имеет дело "Линейщикъ".
 * Пути берутся из QT-функций, включаемых в io_path.cpp. Оказывается, в стандартной библиотеке нет
 * ф-ций работы с папками. Поэтому есть варианты пользоваться QT, или POSIX. Попробую пока второй
 * чисто из спортивного интереса. К QT перейти будет никогда не поздно. Тем более, подозреваю, что
 * она использует ф-ции POSIX, как универсальный вариант, вряд ли ф-ции операционки. Скомпилирую QT,
 * посмотрю в отладке.
 * Структура папок в "Линейщикъ"е такая:
 *	- Линейщикъ
 *		- Сервер							real, practice - термины Oanda
 *			- Инструмент					как eur_usd
 *				- Сведения об инструменте	класс CinstrInfo : public oatp::Instrument
 *				- Период					m1, s10 и т.д.
 *					- вершины				tops
 *					- пос-ти вершин			tseq, sseq
 *					- линии					lines (индексированный массив)
 *						- линии				lines
 *						- индексы			h1, d2...
 *					- эл-ты стат-ки			sunits (индексированый массив)
 *						- эл-ты				sunits
 *						- индексы			i1, i2...
 *					- Настаройки			popt
 *				- Общие настройки			iopt
 *		- Настройки взаимодействия			lopt
 * В oanda есть ещё уч.запись, которая содержит свои заказы, поэтому их нужно отличать таким образом.
 * Либо заказы записывать в периоде, отмечая уч.записью, либо в инструменте, отмечая периодом и уч.зап.
 * Период в заказе имеет значение т.к. изначально заказ у меня сопровождался в периоде, который его от-
 * крыл. Так и нужно оставить. Значит, заказ записывается в периоде.
 * Вопрос о линиях открытый: линии создаются и отслеживаются в периоде, но должны легко перемещаться из
 * периода в период от минимального в максимально возможный. Проблема в перемещении в меньший период:
 * обязательно ли получать все свечи с сервера, чтобы счесть их к-во? От к-ва зависит значение линии.
 */
class CPath
{
protected:
	static int OSDataPaths(glb::CvArray<glb::CFxString> &as);
	static int UserSelectPath(const glb::CvArray<glb::CFxString> &as);
public:
	static int CreateAppPath(const char *pcszApp);
	static void CheckMakeDir(const char *pd);
	static int MakeFileName(glb::CFxString &sNm, CCHAR *pcszExtn, int nStrLen);
	static int MakeFileName(BYTES &sNm, CBYTE *pcszExtn, int nBytes);
	static size_t FileExist(const char *pcszDir, const char *pcszFile);
	static size_t FileExist(const char *pcszPathName);
	static void FileSetNull(const char *pcszPathName);
	static glb::CFxString& AddName(glb::CFxString &ac, const char *pcszName);
	static glb::CFxString& AddName(glb::CFxString &ac, const char *pcszName, int nNmLen);
	static glb::CFxString ObjsFlPath(const char *pcszServer, const char *pcszInstr, const char *pcszPeriod, const char *pcszNm);
	static glb::CFxString PeriodPath(const char *pcszServer, const char *pcszInstr, const char *pcszPeriod);
	static glb::CFxString InstrPath(const char *pcszServer, const char *pcszInstr);
	static glb::CFxString ServerPath(const char *pcszServer);
	static int32_t LastNames(const int32_t nNames, const char *pcszPath, int32_t nPathLen = -1);
	static int32_t CmpNames(const char *ps1, const char *ps2);
	static QString& CheckFileName(QString &s);
	static glb::CFxString& CheckFileName(glb::CFxString &s);
	static BYTES& CheckFileName(BYTES &s);
	static void RmFile(int32_t iDrNmID, int32_t iFlNmID);
};

inline glb::CFxString& CPath::AddName(glb::CFxString &spath, const char *pcszName)
{
	return AddName(spath, pcszName, strlen(pcszName));
}

}// namespace fl