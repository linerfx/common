#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_varFileBase.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_varFileBase.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_file.h"
#include "global.h"
#include "io_flInd.h"
#include "exception.h"
#include "strdef.h"
#include <typeinfo>
#include "io_opener.h"
#include "io_indexIntegrity.h"
#include "iTgDefs.h"
#include "io_FileItem.h"

namespace glb {
	const char* FileName(const char *pcszPath, int *pnLen = NULL);
	double Bytes(uint64_t n, const char **pps);
}

namespace fl {
namespace a {
extern CIndexIntegrity ii;
//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief class CvBase базовый класс для CvLcl и CvShrd - это индексированный файловый массив.
/// По новому соглашению от 19.09.2022 базовый индекс содержит файловые отступы int64_t, а поль-
/// зовательские индексы содержат базовые индексы int32_t.
/// По новому соглашению от 23.08.2024 задействуется логика не пустого эл-та и, как и прежде, -
/// логика целостности уникальных индексов. См. fl::a::SetItem()
/// CvBase наследуют CvLcl и CvShrd
/// \param Obj: объект массива динамического размера, производный от CFileItem. Для эффективности
/// желательно, чтобы ф-ция объекта SetBinData не перераспределяла память, если её достаточно.
/// \param nInds: к-во индексов массива, исключая базовый индекс ioffset
/// CvBase наследует CvShrd
///
template <class Obj, int nInds>
class CvBase : public CBinFile
{
	typedef CBinFile BAS;
	typedef i::CIndBase<Obj> INDEX;
	typedef CvBase<Obj, nInds> THIS;
	typedef glb::CFxArray<BYTE, true> IOBUF;
public: // конструкцыя/деструкцыя
	explicit CvBase();
	virtual ~CvBase();
protected: // это для наследующего класса, где есть индексы CIndexLocal или CIndexShared
	virtual INDEX* GetIndex();
public: // открыть/закрыть файл/определить ф-ции сравнения для индексов
	virtual int64_t open(const char *pcszPathName) override;
	virtual void close() override;
	virtual void Set(typename i::CIndBase<Obj>::fcmp *f);
	// расширенные возможности для открытия файла без параметров. Производный класс должен собрать путь, ключ...
	virtual int64_t open();
	virtual const char* PathName();	// путь и имя файла (производный класс обязан предъявить)
	virtual const char* Key(glb::CFxString &s);// ключ к файлу для QSharedMemory (производный класс обязан предъявить)
public: // вмешательства и насилия...
	virtual int32_t Add(const Obj &o);
    virtual int32_t AddUnic(const Obj &o);
	virtual int32_t Edit(const Obj &o, cint32_t iInd, cint32_t i, bool bIncsRef = false);
	virtual int32_t EditUnic(const Obj &o, int32_t iInd, int32_t iObj, bool bSafe = true);
	virtual int32_t Rem(cint32_t iInd, cint32_t iAt); // удаляю 1 эл-т, от 'iAt' по индексу 'iInd'
	virtual void Empty();
	virtual int32_t Invalidate(const int32_t iInd, const int32_t iObj, bool bRmFrmPrnt = false);
	virtual int32_t FindEqual(const Obj &o, int32_t iInd, int32_t iBas = INV32_BIND,
							  int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t FindNearest(const Obj &o, int32_t iInd, int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t FindValid(const Obj &o, int32_t iInd, int32_t iBas = -1, int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t Reindex(const Obj &o, int32_t iInd, int32_t iCur);
	virtual int32_t ObjSz(int32_t iInd, int32_t iObj, int64_t &nOfst) const;
public: // извлечения - экстракцыи
	virtual const INDEX* GetIndex() const;
    virtual Obj& GetItem(int32_t iIndex, int32_t iItem, Obj &o);
    virtual const Obj& GetItem(int32_t iIndex, int32_t iItem, Obj &o) const;
public:
//	virtual int32_t UnicIndex() const;
	virtual const int32_t* UnicIndices(int32_t *pcnt) const;
	virtual int32_t Count() const;
	virtual int32_t Count(int32_t iInd) const;
	virtual int32_t BaseIndex(int32_t iInd, int32_t i) const;
public:
	static BYTES* io_buf();
	static Obj* static_obj();
//	static bool IsStatic(const Obj*);
};

template<class Obj, int nInds>
CvBase<Obj, nInds>::CvBase()
{

}

template<class Obj, int nInds>
CvBase<Obj, nInds>::~CvBase()
{

}

template<class Obj, int nInds>
i::CIndBase<Obj>* CvBase<Obj, nInds>::GetIndex()
{
	ASSERT(false); // только для наследования
	return NULL;
}

template<class Obj, int nInds>
const i::CIndBase<Obj>* CvBase<Obj, nInds>::GetIndex() const
{
    ASSERT(false); // только для производных классов
    return NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::GetItem извлекаю 1 эл-т из массива
/// \param iInd: может быть ioffset, idel или пользовательским
/// \param iObj: индекс эл-та в индексе iInd
/// \param o:
/// \return ссылку на 'o'
///
template<class Obj, int nInds>
Obj& CvBase<Obj, nInds>::GetItem(int32_t iInd, int32_t iObj, Obj &o)
{	// здесь к-во элементов в индесах может отличаться (+/-1), если объект добавляется или удаляется
	COpener<THIS> opnr(*this);
    int64_t nofst, nosz;
    const INDEX *pi = GetIndex();
	ASSERT(iInd >= fl::idel && iInd < nInds && iObj >= 0 && iObj < pi->count(iInd));
    // пользовательские индексы хранят базовые индексы, а базовый индекс - файловые отступы
    int32_t iBas = pi->Bas(iInd, iObj);
	ASSERT(iBas >= 0 && iBas < pi->count(iInd));
    // файловый отступ и размер объекта
    nosz = ObjSz(fl::ioffset, iBas, nofst);
    ASSERT(nosz > 0);
    // читаю объект в файле
    IOBUF *pab = io_buf();
    pab->SetCountND((int32_t) nosz);
    BAS::get(nofst, *pab, nosz);
    o.SetBinData(*pab, pab->GetCount());
    return o;
}

template<class Obj, int nInds>
const Obj &CvBase<Obj, nInds>::GetItem(int32_t iInd, int32_t iObj, Obj &o) const
{
	ASSERT(iInd >= fl::ioffset && iInd < nInds && iObj >= 0 && iObj < GetIndex()->count(iInd));
    CvBase<Obj, nInds> *pa = const_cast<CvBase<Obj, nInds>*>(this);
	ASSERT(pa != NULL);
	return pa->GetItem(iInd, iObj, o);
}

/*template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::UnicIndex() const
{
	return fl::inone; // нет уник.индекса
}*/

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::UnicIndices возвращает ук-ль на массив индексов уникальных индексов или нуль.
/// Индексы должны располагаться в возрастающем порядке, первым в массиве обязательно должен быть
/// уникальный индекс, если они есть.
/// \param pcnt: [ИЗ] ук-ль на к-во уник.индексов. Может быть нуль.
/// \return расположенные в возраст.порядке индексы уник.индесов, как iStrsNms, iStrStplNmi
///
template<class Obj, int nInds>
const int32_t* CvBase<Obj, nInds>::UnicIndices(int32_t *pcnt) const
{
	if (pcnt)
		*pcnt = 0;
	return nullptr; // нет уник.индексов
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase<Obj, nInds>::open откроет индексированный файл. Файл может быть с рашрирением или
/// без, но каждый файл индекса находится в этой же дирректории, что сам файл и заканчивается порядковым
/// номером индекса, таким как '.01', '.02' и т.д. Наибольшее к-во индексов 99.
/// \param pcszPN - путь и имя файла. Может быть нулём, если файловый массив уже открывался этим объектом.
/// \return к-во эл-тов в файле. Швырнет исключение, если возникла файловая ошибка.
///
template<class Obj, int nInds>
int64_t CvBase<Obj, nInds>::open(const char *pcszPN)
{
	ASSERT(pcszPN || m_sPath.GetCount());
	ASSERT(nInds >= 0 && nInds < 128);
	CBinFile::open(pcszPN); // швырнет исключение, если что не так... вернёт к-во байт в файле
	return GetIndex()->open(pcszPN); // к-во эл-тов в файле
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::close закроет файл, индексы останутся в памяти. Чтобы перезаписать индексы на
/// диске, нужно вызывать INDEX::drop()
///
template<class Obj, int nInds>
void CvBase<Obj, nInds>::close()
{
	ASSERT(nInds >= 0 && nInds < 128);
	ASSERT(CBinFile::IsOpen());
	GetIndex()->close();
	CBinFile::close();
}

template<class Obj, int nInds>
void CvBase<Obj, nInds>::Set(typename i::CIndBase<Obj>::fcmp *f)
{
	GetIndex()->Set(f);
}

template<class Obj, int nInds>
int64_t CvBase<Obj, nInds>::open()
{
	ASSERT(false);// для производного
	return -1;
}

template<class Obj, int nInds>
const char* CvBase<Obj, nInds>::PathName()
{
	ASSERT(false);// для производного
	return nullptr;
}

template<class Obj, int nInds>
const char* CvBase<Obj, nInds>::Key(glb::CFxString &s)
{
	ASSERT(false);// для производного
	return nullptr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase<Obj, nInds>::Add добавляет в индексированный файловый массив копию 'o' записывает
/// объект в конец файла и во все индексы. Используй AddUnic() в массиве с уник.индексами
/// \param o: записываемый объект, производный от CFileItem.
/// \return базовый ид-р нового эл-та
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Add(const Obj &co)
{
	ASSERT(nInds >= 0 && nInds < 128);
	fl::a::COpener<THIS> opnr(*this);//(IsOpen());
	// здесь в 'o' меняется счётчик к-ва ссылок и время
	if (Obj *po = const_cast<Obj*>(&co)) {
		po->m_nref = 1;
		po->m_tmc = po->m_tme = time(nullptr);
	}// декларацыи, иницыацыи... в конец файла, если нет удалённых
	INDEX *pi = GetIndex();
	// базовый индекс - отступы, упорядоченные по возрастанию
	int32_t i, ai[nInds], iBas = pi->count(idel) ? *((int32_t*) pi->Index(idel)) : -1;
	// если есть недействительные эл-ты, редактирую первый недействительный
	if (iBas >= 0 && iBas < Count())
		return Edit(co, fl::ioffset, iBas);
	// добавляю в конец файла
	iBas = pi->SetOffset(m_nsz);
	// в пользовательские индексы отправляется базовый
	for (i = ibanal; i < nInds; i++)
		ai[i] = pi->SetInd(i, FindNearest(co, i), iBas);
	ASSERT(iBas < pi->count(fl::ioffset));
	try { // объект к записи, его размер
		IOBUF *pab = io_buf();
		pab->SetCountZeroND();
		if (co.GetBinData(*pab) <= 0)
			throw err::CLocalException(SI_BGOBJ, time(NULL), typeid(co).name(), glb::FileName(m_sPath),
									   glb::FileName(__FILE__), __LINE__);
		// объект в конец основного файла
		CBinFile::put(*pab, pab->GetCount());
	} catch (...) {
		pi->RemNC(fl::ioffset, Count() - 1);
		for (i = ibanal; i < nInds; i++)
			pi->RemNC(i, ai[i]);
		throw;
	}
	return iBas; // базовый индекс нового эл-та
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::AddUnic добавляет эл-т только в том случае, если во всех уникальных индексах
/// нет такого объекта. Если есть, добавлять такой эл-т нельзя. Но если добавляемый объект
/// идентичен одному из имеющихся, верну индекс идентичного эл-та.
/// \param o: добавляемый эл-т
/// \return базовый индекс нового эл-та (в сл. добавления нового эл-та равен Count() - 1); или
/// индекс полностью идентичного эл-та; или <0, если 'o' добавить нельзя т.к. нарушает целостность
/// одного из уникальных индексов.
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::AddUnic(const Obj &o)
{
	COpener<THIS> opnr(*this);
	int32_t i = -1;
	// если объект не нарушает целостность уник.индексов в массиве и объект уникальный, добавлю новый
	if (ii.IsUnic<THIS, Obj>(*this, o))
		return Add(o);
	// если объект идентичный по всем уник.индексам имеющемуся
	if ((i = ii.IsEqual()) >= 0)
		return Edit(o, fl::ioffset, i, true);
	// 'o' нельзя добавить в массив, он нарушет целостность уник.индексов
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// CvBase<Obj, nInds>::Edit - редактирование объекта в индексированном файловом массиве.
/// \param o: новый эл-т, вместо указанного индексами, производный от fl::CFileItem. Если это один из static_obj(),
/// будет размещён новый в куче.
/// Ссылка на объект константная для совместимости. Объект тут меняется: к-во ссылок, время.
/// Ссылки на объект... Если объект не действительный, к-во ссылок на него может быть хуй. Если объект действительный,
/// а к-во ссылок у него хуй, значит редактор (вызывающий Edit()) нихуя не знает о к-ве ссылок, ему похую. В этом
/// случае новый объект наследует к-во ссылок изменяемого. Если же объект 'o' действительный и редактор указал к-во
/// ссылок, Edit() уважает к-во ссылок редактора.
/// \param iInd: индекс, по которому редактируется объект iObj
/// \param iObj: индекс объекта в индексе iInd
/// \param bInrsRef: увеличить к-во ссылок на изменяемый объект. По-умолчанию false
/// \return базовый индекс изменённого объекта или -1 в сл.ошибки, такой как: попытка несанкцыонированной замены
/// защищённого объекта не соответствующим по статусу (маска ITM_MSEC). Швырнет исключения err::CCException или
/// err::CLocalException в сл. ошибки.
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Edit(const Obj &co, cint32_t iInd, cint32_t iObj, bool bIncrsRef)  // bIncrsRef = 0
{
	ASSERT(nInds >= 0 && nInds < 100 && iInd >= fl::ioffset && iInd < nInds);
	COpener<THIS> opnr(*this);
    INDEX *pi = GetIndex();
	ASSERT(iObj >= 0 && iObj < pi->count(iInd));
    IOBUF *pab = io_buf();
	CFileItemObtainer<THIS, Obj> oold(this);
	// Базовый индекс хранит файл.отступы, остальные индексы содержат базовые индексы
	int32_t iBas = pi->Bas(iInd, iObj), ai[nInds], i, nAdd, nDel;
	// редактируемый объект нужно извлечь из файла, чтобы найти во всех индексах для переиндексации
	Obj &old = oold.GetItem(fl::ioffset, iBas);
	ASSERT(old.m_flags & ITM_DEL || old.m_nref > 0);
	// в базе есть защищённые объекты (secured), их можно заменять только аналогичными объектами
	if ((old.m_flags ^ co.m_flags) & ITM_MSEC)
		return -1;
	// файловый отступ и размер редактируемого эл-та.
    int64_t nOfst;
	const int64_t nOldSz = ObjSz(fl::ioffset, iBas, nOfst);
    ASSERT(nOldSz > 0);
    if (nOldSz > INT_MAX)
		throw err::CLocalException(SI_BGOBJ, time(NULL), typeid(co).name(), glb::FileName(BAS::m_sPath),
								   glb::FileName(__FILE__), __LINE__);
    // индексы объекта для последующей переиндексации
	for (i = ibanal; i < nInds; i++) {
		ai[i] = (i == iInd) ? iObj : FindEqual(old, i, iBas);
		ASSERT(ai[i] >= 0 && ai[i] < pi->count(i));
    } // если редактируется не действительный объект, удалю его ид-р из индекса удалённых...
	if (!old.IsValid()) {
		ASSERT(co.IsValid()); // хуй нахуй? Сомневаюсь...
		cint32_t *pir = (cint32_t*) pi->Index(idel, &nDel);
        i = glb::FindEqual(pir, nDel, iBas);
        ASSERT(i >= 0 && i < nDel);
        pi->RemNC(idel, i);
	}
#ifdef __DEBUG
	else if (co.IsValid()) {
		cint32_t *const pir = (cint32_t*) pi->Index(idel, &nDel);
		ASSERT(glb::FindEqual(pir, nDel, iBas) < 0);
	}
#endif
	// к-во ссылок нового объекта может быть хуй, если объект удаляется (оформляется как не действительный), иначе
	// скорее всего не указано, тогда заимствую его у старого если оно есть (если старый действит.) Иначе 1.
	Obj &o = const_cast<Obj&>(o);
	if (!co.m_nref && co.IsValid())
		o.m_nref = old.m_nref ? old.m_nref + bIncrsRef : 1;
	// время изменения и время создания
	o.m_tme = time(nullptr);
	o.m_tmc = (old.m_flags & ITM_DEL) ? o.m_tme : old.m_tmc;
	// новый объект - в файл
    pab->SetCountZeroND();
	cint64_t nNewSz = o.GetBinData(*pab); // размер нового эл-та в файле (здесь должен быть не больше INT_MAX)
    if (nNewSz <= 0 || nNewSz > INT_MAX)
		throw err::CLocalException(SI_BGOBJ, time(NULL), typeid(co).name(), glb::FileName(BAS::m_sPath),
								   glb::FileName(__FILE__), __LINE__);
    // если объекты одного размера, двигать в основном файле ничего не нужно, но может быть переиндексация
    if ((nAdd = (int32_t) (nNewSz - nOldSz)) < 0)
        BAS::remove(nOfst, glb::Mn((int32_t) nOldSz, nAdd * -1));
	else if (nAdd > 0 && nOfst + nOldSz < BAS::m_nsz) {
		ASSERT(pab->GetObjectsAlloc() >= nAdd);
        BAS::insert(nOfst, *pab, nAdd);// мусор
	} // перезапись основного файла
    BAS::edit(nOfst, *pab, nNewSz);
    // правка отступов в базовом индексе, всех после iBas
    if (nAdd)
		pi->Edit(fl::ioffset, iBas + 1, nAdd);
    // правка пользовательских индексов... основной эл-т в файле изменён, отступы отредактированы
    for (i = 0; i < nInds; i++)
		Reindex(co, i, ai[i]);
	return iBas;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::EditUnic заменит указанный индексами объект на новый, сохранив целостность уни-
/// кальных индексов. Ф-ция добавит новый объект в файл, если указано 'bSafe' и изменяемый объект
/// отличается от имеющегося и на него ссылаются больше одного раза.
/// Если объект 'o' уже есть в файле наряду с указанным индексами (например, изменяю строку "водка"
/// на строку "вискарь", но "вискарь" уже есть в файле), уменьшу к-во ссылок изменяемого объекта и
/// если оно обнуляется, то удалю его, а к-во ссылок найденного объекта увеличу и заменю его на но-
/// вый.
/// \param o: новый объект, вместо указанного индексами. Не может быть одним из статических. Если
/// этот объект уже есть в файле и он отличается от указанного индексами, уменьшу к-во ссылок на
/// объект в файле и увеличу к-во ссылок на найденный объект заменив его на 'o'.
/// \param iInd: индекс массива, по которому меняю объект (не может быть idel)
/// \param iObj: индекс объекта в 'iInd'
/// \param bSafe: если 'o' - уникальный для этого массива объект и bSafe "трю" и на изменяемый в
/// файле объект ссылаются более одного раза, он останется без изменений за исключением уменьшения
/// счётчика на 1. А новый объект 'o' будет добавлен в файл. Если !bSafe (значит force), то объект
/// в файле будет изменён несмотря на к-во ссылок.
/// \return базовый индекс 'fl::ioffset' эл-та 'o'
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::EditUnic(const Obj &oNew, int32_t iInd, int32_t iObj, bool bSafe) // bSafe = true
{
	ASSERT(nInds >= 0 && nInds < 100 && iInd >= fl::ioffset && iInd < nInds);
	COpener<THIS> opnr(*this);
	int32_t iBas = ii.CanEdit<THIS, Obj>(*this, oNew);
	// если 'o' - это изменнёный или идентичный указанному в файле...
	if (iBas == BaseIndex(iInd, iObj))
		return Edit(oNew, iInd, iObj);
	// если 'o' уникальный объект, им можно заменить имеющийся или добавить новый
	if (ii.IsUnic()) {
		if (bSafe) {// только если на изменяемый объект не ссылаются другие
			CFileItemObtainer<THIS, Obj> oo(this);
			Obj &old = oo.GetItem(iInd, iObj);
			if (old.m_nref > 1) {
				iBas = Add(oNew); // o.m_nref = 1; возвращаю i - базовый ид-р 'o'
				old.m_nref--;
				Edit(old, iInd, iObj);
			} else // 1 или 0 если удалёный
				iBas = Edit(oNew, iInd, iObj);
		} else { // иначе меняю несмотря на к-во ссылок (force edit)
			const_cast<Obj&>(oNew).m_nref = 0; // к-во ссылок будет как в файле см. Edit()
			iBas = Edit(oNew, iInd, iObj);
		}
		return iBas;
	}// <0? объект нельзя ни добавить в файл ни изменить имеющийся т.к. он нарушает целостность уник.индексов
	if (iBas < 0)
		return iBas;
	// единственный объект в файле, который возможно заменить на 'o' индексируется 'i', но этот объект не
	// указан аргументом iObj. Почему? Вероятно, объект 'o' сильно изенился и теперь похож на другой. Значит,
	// к-во ссылок указанного индексами объекта должно быть уменьшено. Если обнуляется, объект становится не
	// действительным.
	Invalidate(iInd, iObj);
	// К-во ссылок найденного объекта увеличивается на 1 и объект в файле заменяется новым. Это спорный момент
	// если индексов много. Если в массиве уник.индекс единственный, то возможно, а если нет? Спорно.
	return Edit(oNew, fl::ioffset, iBas, true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::Rem удаляет 1 эл-т из файла и из всех индексов по указанному индексу с правкой
/// отступов и индексов. По новому соглашению от 19.09.2022 базовый индекс содержит отступы,
/// пользовательские индексы содержат базовые индексы...
/// \param iInd: индекс, по которому извлекается объект.
/// \param iAt: позиция удаляемого объекта в индексе.
/// \return базовый индекс удалённого эл-та
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Rem(cint32_t iInd, cint32_t iAt)
{
    COpener<THIS> opnr(*this);
	ASSERT(iInd >= fl::ioffset && iInd < nInds);
    // декларацыи, иницыацыи...
    INDEX *pi = GetIndex();
    // базовый индекс содержит файловые отступы
    int32_t i, cnt, iBas = pi->Bas(iInd, iAt);
	CFileItemObtainer<THIS, Obj> oo(this);
	Obj &o = oo.GetItem(ioffset, iBas);
	int64_t nOfst, nosz = ObjSz(fl::ioffset, iBas, nOfst);
    // сначала удаляю из всех индексов. FindEqual() использует базовый индекс (файловые отступы) в GetItem()
    for (i = ibanal; i < nInds; i++)
		pi->Rem(i, (i == iInd) ? iAt : FindEqual(o, i, iBas));
	if (o.IsValid()) {// из индекса не действительных эл-тов эл-т удаляется тоже с коррекцией оставшихся
		cint32_t *pn = (const int32_t*) pi->Index(idel, &cnt);// упорядоченные по возрастанию индексы ioffset
        pi->Rem(idel, (iInd == idel) ? iAt : glb::FindEqual(pn, cnt, iBas));
    }// теперь можно удалить файловый отступ и сам объект из основного файла...
	pi->Rem(fl::ioffset, iBas);
    if (pi->count() - i > 1)
        BAS::remove(nOfst, nosz); // физически, из основного файла байт по отступу
    else // последний эл-т...
        m_nsz = nOfst;
    return iBas; // базовый индекс
}

template<class Obj, int nInds>
void CvBase<Obj, nInds>::Empty()
{
    COpener<THIS> opnr(*this);
    GetIndex()->Empty();
	remove(0, m_nsz); // all
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::Invalidate вызывает Edit() - изменяет эл-т в файле. Если к-во ссылок на эл-т
/// обнуляется, помещает базовый индекс эл-та в массив индексов недействительных эл-тов, отмечает
/// объект как недействительный (удаляет).
/// \param iInd: индекс (не может быть idel)
/// \param iObj: индекс эл-та
/// \param bRmFrmPrnt: передаётся удаляемому объекту
/// \return базовый индекс удаляемого эл-та
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Invalidate(const int32_t iInd, const int32_t iObj, bool bRmFrmPrnt/* = false*/)
{
	ASSERT(iInd >= fl::ioffset && iInd < nInds);
    COpener<THIS> opnr(*this);
    INDEX *pi = GetIndex();
    int32_t iBas = pi->Bas(iInd, iObj), cnt;
	thread_local static Obj o; // файловый статич.объект тут использовать нельзя т.к. оба заняты в Edit() и Find()
	GetItem(fl::ioffset, iBas, o);
	if (!o.IsValid()) // удалить можно только однажды
		return iBas;
	if (0 == o.Invalidate(iBas, bRmFrmPrnt)) {
		const int32_t *const pir = (int32_t*) pi->Index(idel, &cnt);
		ASSERT(glb::FindEqual(pir, cnt, iBas) < 0); // удалять однажды!
		pi->SetInd(idel, (cnt > 0) ? glb::FindNearest(pir, cnt, iBas) : 0, iBas);
	}
	return Edit(o, fl::ioffset, iBas);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase<Obj, nInds>::ResetIndices переиндексирует объект по всем индексам.
/// \param o: изменённый объект, размер которого не отличается от оригинала в файле.
/// \param iInd: индекс, по которому редактируется объект.
/// \param iObj: индекс объекта в индексе iInd.
/// \return ёбаный буль, догадайся какой.
///
/*template<class Obj, int nInds>
bool CvBase<Obj, nInds>::ResetIndices(const Obj &o, int iInd, int iObj)
{
	ASSERT(o.IsValid());
	ASSERT(o.GetBinData(glb::ab) == GetItem(o.m_nFlOfst).GetBinData(glb::ab));
	Obj old(GetItem(o.m_nFlOfst));// редактируемый оригинал
	int ai[nInds], i;
	const int cnt = GetIndex(ioffset)->count();
	int nfs = -1;
	// все индексы редактируемого объекта
	for (i = 0; i < nInds; i++)
		if (i == iInd)
			ai[i] = iObj;
		else if ((ai[i] = GetIndex(i)->FindEqual(old)) < 0 || ai[i] >= cnt)
			ASSERT(false);
	// изменю объект в файле...
	glb::ab.SetCountZeroND();
	nfs = o.GetBinData(glb::ab);
	// размеры объекта равны
	if (edit(o.m_nFlOfst + sizeof nfs, glb::ab, nfs) != nfs)
		ASSERT(false);
	// исправлю все индексы отредактированного эл-та
	for (i = 0; i < nInds; i++)
		GetIndex(i)->Reset(o, ai[i]);
	return true;
}*/

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Не компилируется без m_id в эл-те массива, как fl::CString. Эту ф-цию нужно делать в соответствующем массиве,
/// таком как CfxArrayInd, где наличие такого члена обязательно.
/// \brief UpdateIndexed - ф-ция для редактирования файлового массива с эл-тами, содержащими файловые иден-
/// тификаторы glb::CObjID. Редактирует эл-ты из 'a' в файле, добавляя новые, синхронизирует индексы эл-тов
/// (таких как индексы вершины в пос-ти вершин). См. CIntFile<Obj>::UpdateIndexed.
/// Эта ф-ция может длиться вечность, т.к. редактирование эл-та в файле может сопровождаться копированием
/// эл-тов в файле, если обновляемые эл-ты отличаются размером от тех, что в файле. См. this->Edit(Obj&, int, int)
/// Ограничения, накладываемые на эл-ты массива 'a':
/// 1. наличие идентификатора CObjID m_id с базовым индексом объекта.
/// Нужно избавиться от этой ф-ции, не применять её. Тем более теперь индексы в разделяемой памяти.
/// \param a: массив обновляемых эл-тов.
/// \return базовый индекс первого отредактированного эл-та
///
/*template<class Obj, int nInds>
int CvBase<Obj, nInds>::UpdateIndexed(const glb::CFxArray<Obj, false> &a)
{
	const int iBs = FindEqual(a[0], -1); // базовый индекс (отступ)
	int i, j, cnt;
*/	/*		 t
	 * f. 0123456789
	 * a.    0123456789
	 * x = 3 - 0 = 3;
	 * x1= (3 + 10) / 2 = 6;
	 * x1= 9 - 6 = 3;
	 * x = 3 + 3 = 6
	 *	  ------ +
	 * a:	 3210123456
	 * f: 654
	 */
/*	int64_t nall = iBs + a.GetCount();
	Obj o(GetItem((int) 0, iBs)); // нужны индексы эл-та
	int x = o.m_id.m_iObj - a[0].m_id.m_iObj,// decrement
		x1 = (int) (nall / 2);
	x1 = a.Last().m_id.m_iObj - x1;
	j = iBs;
	for (i = 0, cnt = a.GetCount(); i < cnt; i++, j++) {
		o = a[i];
		o.m_id.m_iObj -= x1;
		Edit(j, &o, 1);
	}
	for (x += x1, i = iBs - 1; i >= 0; i--) {
		Read(i, &o, 1);
		o.m_id.m_iObj -= x;
		Edit(i, &o, 1);
	}
	return iBs;
}*/

template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Count() const
{
    COpener<THIS> opnr(const_cast<THIS&>(*this));
    return GetIndex()->count();
}

template<class Obj, int nInds>
inline int32_t CvBase<Obj, nInds>::Count(int32_t iInd) const
{
	COpener<THIS> opnr(const_cast<THIS&>(*this));
	return GetIndex()->count(iInd);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase<Obj, nInds>::BaseIndex предъявит базовый индекс - индекс эл-та в базовом индексе, где
/// файловые отступы упорядочены по возрастанию
/// \param iInd: индекс - базовый, или пользовательский
/// \param i: индекс эл-та в индексе iInd
/// \return базовый индекс эл-та, указанного параметрами
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::BaseIndex(int32_t iInd, int32_t i) const
{
	ASSERT(i >= 0 && i < GetIndex()->count(iInd));
	return GetIndex()->Bas(iInd, i);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief func CvBase::FindEqual находит 'o'. Чтобы найти объект в не уникальном индексе, нужно
/// предоставить его базовый ид-р.
/// \param o: искомый объект. Может быть искуственным, т.е. содержать только нужные для ф-ции
/// сравнения сведения.
/// \param iInd: индекс, по которому ищу 'o'
/// \param iBas: базовый ид-р эл-та если известен, буду искать его среди равных в не уникальном индексе
/// \param iStart: индекс первого эл-та диапазона поиска, включительно
/// \param iEnd: индекс последнего эл-та диапазона поиска, включительно
/// \return индекс эл-та в индексе iInd. Если индекс не уникальный, вернёт индекс какого-то из эл-тов.
/// Если такого объекта в массиве нет, вернёт -1.
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::FindEqual(const Obj &o, int32_t iInd, int32_t iBas, // iBas = INV32_BIND
									  int32_t iStart, int32_t iEnd) const		// iStart = 0, iEnd = -1
{	// никакой объект не содержит отступ
	ASSERT(iInd > fl::ioffset && iInd < nInds);
	ASSERT(IsOpen());
	fl::CFileItemObtainer<THIS, Obj> oo(const_cast<THIS&>(*this));
	Obj &otmp = oo.obj();
	ASSERT((uint64_t) &o != (uint64_t) &otmp);
	const INDEX *pi = GetIndex();
	const int32_t cnt = pi->count(iInd);
	int32_t i, r,
		a = MAX(0, iStart),
	    z = (iEnd < 0 || iEnd >= cnt) ? cnt - 1 : iEnd;
	for (i = a + (z - a) / 2; a <= z; i = a + (z - a) / 2) {
		r = pi->cmp(iInd, o, GetItem(iInd, i, otmp));
		if (r < 0)
			z = i - 1;
		else if (r > 0)
			a = i + 1;
		else
		{	// если есть базовый ид-р, найду его среди равных объектов в не уникальном индексе
			if (iBas >= 0 && iBas != pi->Bas(iInd, i)) {
				ASSERT(iBas < pi->count(iInd));
				for (int32_t iBk = i - 1, iFd = i + 1; iBk >= a || iFd <= z; iBk--, iFd++) {
					if (iBk >= a)
						if (pi->cmp(iInd, o, GetItem(iInd, iBk, otmp)) != 0)
							iBk = a;
						else if (iBas == pi->Bas(iInd, iBk)) {
							i = iBk;
							break; // for
						}
					if (iFd <= z) {
						if (pi->cmp(iInd, o, GetItem(iInd, iFd, otmp)) != 0)
							iFd = z;
						else if (iBas == pi->Bas(iInd, iFd)) {
							i = iFd;
							break;
						}
					}
				}// индексное переебение, если iBas правильный. Это должно быть выведено из ASSERT, такое
				// возможно. Если изменяю наименование продукта на то которое уже занято, возникнет эта ошибка
				// по первым двум индексам. В этом случае нужно возвращать ошибку или швырять исключение.
				ASSERT(iBas == pi->Bas(iInd, i)); // индексное переебение
			}// assert целостность (не уникального) индекса
			ASSERT(i == cnt - 1 || pi->cmp(iInd, o, GetItem(iInd, i + 1, otmp)) <= 0);
			ASSERT(!i || pi->cmp(iInd, o, GetItem(iInd, i - 1, otmp)) >= 0);
			ASSERT(!pi->cmp(iInd, o, GetItem(iInd, i, otmp)));// только чтобы вернуть в *po (второй объект) искомый
			return i;// *po содержит объект из файла, индексируемый i
		}
	}
	return -1;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief func CvBase::FindNearest находит индекс (в индексе iInd в соответствии с ф-цией сравнения
/// m_fcmp) равного или следующего объекта. Результат может быть равен к-ву эл-ов
/// \param o: искомый объект может содержать только сведения, нужные в ф-ции сравнения m_fcmp
/// \param iInd: индекс, по которому ищу 'o'
/// \param iStart: индекс начала диапазона поиска, или -1, если искать нужно с первого эл-та
/// \param iEnd: индекс последнего эл-та включительно
/// \return индекс искомого эл-та, если такой имеется, или индекс первого большего эл-та
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::FindNearest(const Obj &o, int32_t iInd, int32_t iStart, int32_t iEnd) const
{
	const INDEX *pi = GetIndex();
	const int32_t cnt = pi->count(iInd);
	CFileItemObtainer<THIS, Obj> oo(const_cast<THIS&>(*this));
	Obj &otmp = oo.obj();// *static_obj();
	ASSERT((uint64_t) &o != (uint64_t) &otmp);
	int32_t i, r,
		a = MAX(0, iStart),
		z = !cnt ? 0 : (iEnd < 0 || iEnd >= cnt) ? cnt - 1 : iEnd;
	for (i = a + (z - a) / 2; a < z; i = a + (z - a) / 2) {
		r = pi->cmp(iInd, o, GetItem(iInd, i, otmp));
		if (r < 0)
			z = MAX(a, i - 1);
		else if (r > 0)
			a = MIN(z, i + 1);
		else
			break;
	}
	if (z == a)	{// нет идентичного эл-та
		ASSERT(i >= 0 && i <= cnt);	// i == min(a, z)
		if (i < cnt && pi->cmp(iInd, o, GetItem(iInd, i, otmp)) > 0)
			i++;
	}// assert целостность (не уникального) индекса
	ASSERT(i <= cnt && (i == cnt || pi->cmp(iInd, o, GetItem(iInd, i, otmp)) <= 0));
	ASSERT(!i || pi->cmp(iInd, o, GetItem(iInd, i - 1, otmp)) >= 0);
	return i;// *po не содержит эл-т, индексируемый i
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::FindValid находит действительный объект. В массиве могут быть недействительные объекты,
/// подлежащие удалению.
/// \param o: искомый объект, содержит только сведения, нужные для поиска
/// \param iInd: индекс, по которому нахожу объект
/// \param iBas: см. FindEqual(). По-умолчанию -1
/// \param iStart: начало диапазона поиска, включительно. По-умолчанию -1
/// \param iEnd: последний эл-т поиска в индексе iInd, включительно. По-умолчанию -1
/// \return искомый индекс, или -1
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::FindValid(const Obj &o, int32_t iInd, // iBas = -1, iStart = 0, iEnd == -1
									  int32_t iBas, int32_t iStart, int32_t iEnd) const
{	// FindEqual использует второй объект, возвращаемый static_obj()
	CFileItemObtainer<THIS, Obj> oo(const_cast<THIS&>(*this));
	Obj &otmp = oo.obj();// static_obj();
	ASSERT(&o != &otmp);
	int32_t i = FindEqual(o, iInd, iStart, iEnd), iMid;
	if (i < 0)
		return i;// нет такого эл-та
	const INDEX *pi = GetIndex();
	const int32_t cnt = pi->count(iInd);
	// otmp содержит эл-т из файла, индексируемый i
	GetItem(iInd, i, otmp);
	if (otmp.IsValid()) {
		ASSERT(!pi->cmp(iInd, o, otmp));
		return i;// найден действительный эл-т
	}// i индексирует недействительный эл-т
	if (iStart < 0)
		iStart = 0;
	if (iEnd < 0 || iEnd >= cnt)
		iEnd = cnt - 1;
	iMid = i;
	while (++i < iEnd) {
		GetItem(iInd, i, otmp);
		if (pi->cmp(iInd, o, otmp) != 0)
			break;
		if (otmp.IsValid())
			return i;// *po содержит эл-т из файла, индексируемый i
	}
	for (i = iMid - 1; i >= iStart; i--) {
		GetItem(iInd, i, otmp);
		if (pi->cmp(iInd, o, otmp) != 0)
			return -1;
		if (otmp.IsValid())
			break;
		else if (!i)
			return -1;
	}
	return i;// m_obj содержит эл-т из файла, индексируемый i
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::FindObj находит объект по значению и ид-ру (идентичный) в указанном не уникальном индексе
/// и диапазоне. Ф-ция сделана для поиска в не уникальном индексе. Нет смысла в вызове ф-ции, если iInd уникальный.
/// \param o: искомый объект
/// \param iBas: базовый индекс (ид-р) объекта 'o'
/// \param iInd: не уникальный иднекс, в котором ищу объект. Если индекс уникальный, не ошибка, но глупость.
/// \param iStart: первый индекс (в iInd) в диапазоне включительно, или -1, если не задействован
/// \param iEnd: последний индекс (в iInd) в диапазоне включительно, или -1, если не задействован
/// \return идекс в iInd
///
/*template<class Obj, int nInds> // по-умолчанию iStart = 0, iEnd = -1
int32_t CvBase<Obj, nInds>::FindObj(const Obj &o, int32_t iBas, int32_t iInd, int32_t iStart, int32_t iEnd) const
{	// FindEqual использует второй объект, возвращаемый static_obj()
	Obj *po = NULL;
	static_obj(&po);
	ASSERT(&o != po);
	int32_t i = FindEqual(o, iInd, iStart, iEnd), iMid;
	if (i < 0)
		return i;// нет такого эл-та в индексе iInd в указанном диапазоне
	// 'o' найден, нужно найти базовый инд-р среди равных (в не уникальном индексе)
	const INDEX *pi = GetIndex();
	const int32_t cnt = pi->count();
	if (iStart < 0)
		iStart = 0;
	if (iEnd < 0 || iEnd >= cnt)
		iEnd = cnt - 1;
	iMid = i;
	while (++i <= iEnd) {
		GetItem(iInd, i, *po);
		if (pi->cmp(iInd, o, *po) != 0)
			break;
		if (iBas == pi->Bas(iInd, i))
			return i;// найден искомый эл-т в индексе iInd
	}
	for (i = iMid - 1; i >= iStart; i--) {
		GetItem(iInd, i, *po);
		if (pi->cmp(iInd, o, *po) != 0)
			return -1;
		if (iBas == pi->Bas(iInd, i))
			return i;// найден искомый эл-т в индексе iInd
	}
	return ((i < iStart) ? -1 : i);// m_obj содержит эл-т из файла, индексируемый i
}*/

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::Reindex изменение пользовательского индекса (который содержит базовые ин-
/// ексы) для нового эл-та перед его записью в основной файл.
/// \param o: копия нового объекта, который уже изменён, находится по индексу iCur.
/// \param iInd: редактируемый индекс
/// \param iCur: индекс объекта в iInd
/// \return новый индекс эл-та
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::Reindex(const Obj &o, int32_t iInd, int32_t iCur)
{
	ASSERT(iCur >= 0 && iCur < GetIndex()->count(iInd));
	INDEX *pi = GetIndex();
	int32_t cnt = pi->count(iInd), i = iCur;
	CFileItemObtainer<THIS, Obj> oo(this);
	Obj &otmp = oo.obj();
//	static_obj(&po);// второй объект т.к. первым может быть 'o'
	ASSERT(&o != &otmp);
	// По завершении работы FindNearest, i указывает на элемент больший, или равный o, либо индекс равен к-ву эл-ов
	// массива. А предшествующий i эл-т меньше о. Т.е. о меньше или равен элементу i, если i < m_iCount. Ситуация
	// устроит, если элемент i сдвигается право. Двигать влево можно только предшествующий i эл-т. Поэтому под
	// первым if от возвращаемого FindNearest индекса отнимается 1.
	// Несмотря на то что от результата отнимается 1, конечный результат не может быть равен iCur т.к. o > iCur + 1
	if (cnt - iCur > 1 && pi->cmp(iInd, o, GetItem(iInd, iCur + 1, otmp)) > 0)
		i = FindNearest(o, iInd, iCur + 1, cnt - 1) - 1;
	else if (iCur > 0 && pi->cmp(iInd, o, GetItem(iInd, iCur - 1, otmp)) < 0)
		i = FindNearest(o, iInd, 0, iCur - 1);
	else
		return iCur; // 'o' не больше следующего и не меньше предыдущего эл-та, т.е. остаётся на прежнем месте
	ASSERT(i >= 0 && i < cnt && i != iCur);
	pi->Move(iInd, i, iCur);
#ifdef __DEBUG // объект должен точно стоять на своём месте, иначе нахуя всё это затевалось...
	Obj odbg;
	GetItem(iInd, i, odbg);
	ASSERT(!i || pi->cmp(iInd, odbg, GetItem(iInd, i - 1, otmp)) >= 0);
	ASSERT(cnt - i == 1 || pi->cmp(iInd, odbg, GetItem(iInd, i + 1, otmp)) <= 0);
#endif// defined __DEBUG
	return i;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::ObjSz вычисляет файловый отступ и размер объекта, указанного
/// индексами. Согласно с новым соглашением от 19.09.2022 г. файловые отступы содержит
/// только базовый индекс, другие содержат базовые индексы.
/// \param iInd: индекс
/// \param iObj: индекс объекта в индексе iInd
/// \param nOfst: файловый отступ объекта
/// \return размер объекта в байтах
///
template<class Obj, int nInds>
int32_t CvBase<Obj, nInds>::ObjSz(int32_t iInd, int32_t iObj, int64_t &nOfst) const
{
	const INDEX *pi = GetIndex();
	int32_t iBas = pi->Bas(iInd, iObj);
	nOfst = pi->Ofst(fl::ioffset, iBas);
	return (pi->count(fl::ioffset) - iBas > 1) ? pi->Ofst(fl::ioffset, iBas + 1) - nOfst : BAS::m_nsz - nOfst;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::io_buf вернёт указатель на статич.масив байт для ввода/вывода. Такой подход
/// взят тут: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66944 т.к. эта ошибка до сих такая.
/// 05.10.2022. Молодец, Олег Пронин! Я пытался объявить переменную в iTog/global.cpp
/// \return статич.массив для ввода-вывода
///
template<class Obj, int nInds>
BYTES* CvBase<Obj, nInds>::io_buf()
{
	static thread_local glb::CFxArray<BYTE, true> *pb;
	if (!pb) {
		static thread_local glb::CFxArray<BYTE, true> ab(128);
		pb = &ab;
	}
	return pb;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::static_obj в целях оптимизации файловый массив объявляет статический массив
/// указателей на файловые эл-ты
/// \return указатель на файловый объект
///
template<class Obj, int nInds>
Obj* CvBase<Obj, nInds>::static_obj()
{
	thread_local static glb::CFxArray<Obj*, true> ao;
	int32_t i, cnt;
	Obj **ppo = ao.data(cnt), *po;
	for (i = 0; i < cnt; i++, ppo++)
		if (!(*ppo)->Used())
			return *ppo;
	ao.Add(po = new Obj);
	return po;
/*	thread_local static Obj *p1, *p2;
	if (!p1 && !p2) {
		thread_local static Obj o1, o2;
		p1 = &o1;
		p2 = &o2;
	}
	if (ppo)
		*ppo = p2;
	return p1;*/
}

/*template<class Obj, int nInds>
bool CvBase<Obj, nInds>::IsStatic(const Obj *po)
{
	Obj *p1, *p2 = static_obj(&p1);
	return (po == p1 || po == p2);
}*/

} // namespace a
} // namespace fl
