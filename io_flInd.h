#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_flInd — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_flInd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <stdint.h>

namespace fl {
namespace i {
////////////////////////////////////////////////////////////////////////////////////////////
/// \brief class fl::CIndBase базовый класс - индекс для индексированного файлового массива.
/// Производные CIndILcl, CIndVLcl, CIndIShrd, CIndVshrd. Этот объект не содержит непосредственно
/// индексы, они размещаются в производных классах. Тут только обозначены переопределяемые ф-ции.
/// \param class O (Object): хранимый в файловом массиве объект.
///
template <class O>
class CIndBase
{
public:
	typedef int(*fcmp)(const O&, const O&);		// прототип ф-ции сравнения эл-тов
public:
	CIndBase();								// для массива. Обязательно присвоить зн-е m_fcmp
	CIndBase(fcmp f);
public:// открытие/закрытие
	virtual long int open(const char *pcszPN = NULL) = 0;
	virtual void close() = 0;
public:// запись
	virtual int SetOffset(int64_t noffset) = 0;	// добавит только отступ
	virtual int SetInd(int iInd, int iAt, int iBas) = 0;// вставит базовый индекс в указанный (в удалённых iAt игнорируется)
	virtual void Edit(int32_t iInd, int32_t iStart, int32_t nAdd) = 0;// изменит все значения, начиная от iStart
	virtual void Rem(int iInd, int iAt) = 0;
//	virtual void Rem(int32_t iInd, int32_t iAt, int32_t nObjSz) = 0;
	virtual int RemNC(int iInd, int iAt) = 0;
	virtual void Move(int iInd, int iTo, int iFrom) = 0;
	virtual void Empty() = 0;
	virtual void Set(fcmp *f) = 0;
public:// извлечение
	virtual int Bas(int iInd, int iObj) const = 0;		// базовый индекс по индексу
	virtual long int Ofst(int iInd, int iObj) const = 0;// файловый отступ
	virtual const void* Index(int32_t iInd, int32_t *pcnt = NULL) const = 0;// void* потому что может быть int* и long*
public:// сравнение и прочее
	virtual int cmp(int iInd, const O &a, const O &b) const = 0;	// сравнит объекты
	virtual int32_t count() const = 0;
	virtual int32_t count(int iInd) const = 0;
	virtual bool CheckCount() const = 0;
	//	virtual int64_t ObjSize(int iInd, int32_t iObj) const = 0;
};

template<class O>
CIndBase<O>::CIndBase()
{

}

template<class O>
CIndBase<O>::CIndBase(fcmp f)
{

}

}// namespace fl
}// namespace i