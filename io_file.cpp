/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_file.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_file.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_file.h"
#include "exception.h"
#include "defs.h"
#include <ctime>
#include <unistd.h> // ftruncate

#define MX_FLSZ	static_cast<int64_t>(LONG_MAX - sizeof m_nsz)
#define AVL_FLSZ (MX_FLSZ - m_nsz)
#define BUF_LEN (512 * 8)

namespace glb {
	const char* FileName(const char *pcszPath, const int cPathLen);
	double Bytes(uint64_t n, const char **pps);// global.cpp
}
namespace fl {
	extern int nBlockSz;		// размер блока чтения/записи, объявлен в global.cpp см. CPath::CreateAppPath
	extern thread_local BYTES ab;// см. iTgog/global.h; StrFileArray/global.h

#ifdef __DEBUG
bool not_ab(const void *pv, int64_t cnt)
{
	CBYTE *cpb = (CBYTE*) pv;
	return (!ab || cpb + cnt < ab || cpb > ab + ab.GetCount());
}
#endif

CBinFile::CBinFile()
{
	m_nsz = 0;
	m_pf = nullptr;
	if (!ab.GetCount()) {
		if (!nBlockSz)
			nBlockSz = BUF_LEN;
		ab.SetCount(nBlockSz);
	}
}

CBinFile::~CBinFile()
{
	if (m_pf != NULL)
		CBinFile::close();
}

////////////////////////////////////////////////////////////////////////////////
/// \brief CBinFile::open дублирует open(const char*) исключает выделение памяти для пути
/// \param sPathNm: перемещается в m_sPath
/// \return open(const char*)
///
/*int64_t CBinFile::open(glb::CFxString &sPathNm) за экстримизм закомментирована
{
	ASSERT(m_pf == nullptr);// должен быть закрыт
	m_sPath.Attach(sPathNm);
	return CBinFile::open();
}*/

///////////////////////////////////////////////////////////////////////////
/// \brief fl::CBinFile::open откроет существующий файл для чтения и записи, прочтёт к-во элементов в m_nsz.
/// Если файла нет, создаст его. В случае возникновения ошибки, швырнет исключение с errno-кодом ошибки.
/// \param pcszPathName: путь к файлу. Может быть нулём, если файл открывается повторно или перегруженной ф-цией.
/// \return Вернёт размер файла без учёта размера m_nsz.
///
int64_t CBinFile::open(const char *pcszPathNm) // pcszPathNm = NULL
{
	ASSERT(pcszPathNm || (const char*) m_sPath);
	ASSERT(m_pf == nullptr); // должен быть закрыт
	cerror = 0;
	if (!pcszPathNm)
		pcszPathNm = m_sPath;
	else if (!m_sPath.GetCount() || pcszPathNm != m_sPath && strcmp(m_sPath, pcszPathNm) != 0)
		m_sPath = pcszPathNm;
	if ((m_pf = fopen(pcszPathNm, "r+b")) != nullptr &&
			fread(&m_nsz, sizeof m_nsz, 1, m_pf) == 1) {
		return m_nsz; // success
	}
	int nerr = cerror;
	if (!nerr || nerr == ENOENT) { // новый файл, или нет такого файла.
		if ((m_pf = fopen(pcszPathNm, "ab")) != nullptr) {
			CBinFile::close();
			return CBinFile::open(pcszPathNm); // рекурсия. Без CBinFile::, пойдёт в вирт.ф-цию.
		}
		nerr = cerror;
	}
	m_sPath.SetCountZeroND();
	throw err::CCException(nerr, __FILE__, __LINE__);
}

void CBinFile::close()
{
	ASSERT(m_pf != nullptr);
	if (fseek(m_pf, 0L, SEEK_SET) ||
		fwrite(&m_nsz, sizeof m_nsz, 1U, m_pf) != 1U)
		throw err::CCException(cerror, __FILE__, __LINE__);
	fclose(m_pf);
	m_pf = nullptr;
	m_nsz = 0;
}

/*
 * определения для ф-ций insert, remove, get, getcp:
 */
#define TO_MOVE (m_nsz - at)
#define AT (at + sizeof m_nsz)
#define CP (cp + sizeof m_nsz)

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief fl::CBinFile::putcp записывает 'cnt' байт из 'pd' в текущую позицию в файле.
/// Следует вызывать после любой другой ф-ции редактирования (insert, edit, add, remove),
/// после установки тек.позиции в файле, либо сразу после открытия файла.
/// \param at: текущая позиция в файле без учёта размера m_nsz (должна точно соответствовать)
/// \param pd: что пишем?
/// \param cnt: к-во записываемых байт в 'pd'
/// \return Возвращает к-во записаных байт, швыряет исключение при попытке превысить
/// наибольший допустимый размер файла LONG_MAX.
///
int64_t CBinFile::putcp(const int64_t at, const void *pd, const int64_t cnt)
{
	ASSERT(m_pf && at >= 0 && at <= m_nsz);
	ASSERT(AT == ftell(m_pf));
	ASSERT(cnt > 0);
	if (LONG_MAX - m_nsz < at - m_nsz + cnt) {
		const char *pb;
		double f = glb::Bytes(LONG_MAX, &pb);
		throw err::CLocalException(SI_FLSZ, time(NULL), glb::FileName(m_sPath, m_sPath.GetCount()),
								   2, f, pb, (double)(cnt - AVL_FLSZ), S_BYTES, __FILE__, __LINE__);
	}
	if (fwrite(pd, 1U, (size_t) cnt, m_pf) != (size_t) cnt)
		throw err::CCException(cerror, __FILE__, __LINE__);
	if (at + cnt > m_nsz)
		m_nsz += at - m_nsz + cnt;
	return cnt;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief CBinFile::insert вставляет или добавляет 'cnt' байт из 'pd' в позицию
/// 'at' в файле 'm_pf'. Позиция 'at' не учитывает размер переменной 'm_nsz' как
/// и сама 'm_nsz'.
/// Швырнет исключение err::CCException в случае системной ошибки ввода/вывода.
/// Швырнет исключение err::CLocalException, если размер файла может превысить
/// максимальный размер.
/// \param at: отступ от начала файла - позиция, в которую будет записываться
/// \param pd: указатель на начало массива байт, записываемых в файл
/// \param cnt: к-во байт, записываемых в файл
/// \return к-во записаных байт.
///
int64_t CBinFile::insert(const int64_t at, const void *pd, const int64_t cnt)
{
	ASSERT(m_pf && cnt > 0);
	ASSERT(at >= 0 && at <= m_nsz);
	ASSERT(nBlockSz > 0 && ab.GetCount() >= nBlockSz);
	ASSERT(not_ab(pd, cnt));
	int64_t nMoved = 0, nMoveNow = 0, cp = 0; // cp - current position
	BYTE *pb = ab;
	if (cnt > AVL_FLSZ) {
		const char *pb;
		double f = glb::Bytes(LONG_MAX, &pb);
		throw err::CLocalException(SI_FLSZ, time(NULL), glb::FileName(m_sPath, m_sPath.GetCount()),
								   2, f, pb, 0, (double)(cnt - AVL_FLSZ), S_BYTES, __FILE__, __LINE__);
	}
	cerror = 0;
	nMoveNow = MIN(nBlockSz, TO_MOVE);
	for (cp = m_nsz - nMoveNow; nMoveNow > 0; cp = m_nsz - nMoveNow - nMoved) {
		ASSERT(cp >= at);
		if (fseek(m_pf, CP, SEEK_SET) ||
			fread(pb, 1U, (size_t) nMoveNow, m_pf) != (size_t) nMoveNow ||
			fseek(m_pf, CP + cnt, SEEK_SET) ||
			fwrite(pb, 1U, (size_t) nMoveNow, m_pf) != (size_t) nMoveNow)
			throw err::CCException(cerror, __FILE__, __LINE__);
		nMoved += nMoveNow;
		nMoveNow = MIN(nBlockSz, TO_MOVE - nMoved);
	}
	if (fseek(m_pf, AT, SEEK_SET) || fwrite(pd, 1, (size_t) cnt, m_pf) != (size_t) cnt)
		throw err::CCException(cerror, __FILE__, __LINE__);
	m_nsz += cnt;
	return cnt;
}

#undef TO_MOVE
#define TO_MOVE (m_nsz - at - cnt)

///			  1			2
/// 01234567890123456789012345678
/// ***********************
///      ***********************
///		 a (23)
/// at		5
/// cnt		23
/// m_nsz	23

////////////////////////////////////////////////////////////////////////////////////
/// \brief CBinFile::edit редактирует файл. Размер файла может быть увеличен.
/// \param at: файловая позиция (не учитывает m_nsz)
/// \param pd: указатель на массив байт, записываемых в поз. 'at'
/// \param cnt: к-во байт, записываемых в файл
/// \return к-во байт, записанных в файл.
///
int64_t CBinFile::edit(const int64_t at, const void *pd, const int64_t cnt)
{
	ASSERT(m_pf && cnt > 0);
	ASSERT(at >= 0 && at <= m_nsz);
	if (AVL_FLSZ < at + cnt - m_nsz) {// 5 + 23 - 23 = 5
		const char *pb;
		double f = glb::Bytes(LONG_MAX, &pb);
		int64_t nex = at + cnt - m_nsz - AVL_FLSZ;
		throw err::CLocalException(SI_FLSZ, time(NULL), glb::FileName(m_sPath, m_sPath.GetCount()),
								   2, f, pb, 0, (double) nex, S_BYTES, __FILE__, __LINE__);
	}
	if (fseek(m_pf, AT, SEEK_SET) ||
		fwrite(pd, 1U, (size_t) cnt, m_pf) != (size_t) cnt)
		throw err::CCException(cerror, __FILE__, __LINE__);
	if (at + cnt > m_nsz)
		m_nsz += at + cnt - m_nsz;// 23 + (5 + 23 - 23) = 28
	return cnt;
}

/*
 * Remove: удаляю 'cnt' байт из файла в позиции 'at'.
 * Возвращаю новый размер файла без учёта размера m_nsz.
 */
int64_t CBinFile::remove(const int64_t at, const int64_t cnt)
{
	ASSERT(m_pf && cnt >= 0 && TO_MOVE >= 0);
	ASSERT(nBlockSz > 0 && (!TO_MOVE || ab.GetCount() >= nBlockSz));
	BYTE *pb = ab;
	int64_t nMoved = 0, nMoveNow = 0, cp = 0; // cp - current position
	nMoveNow = MIN(nBlockSz, TO_MOVE);// m_nsz - at - cnt
	for (cp = at + cnt; nMoveNow > 0; cp = at + cnt + nMoved) {
		if (fseek(m_pf, CP, SEEK_SET) ||
			fread(pb, 1U, (size_t) nMoveNow, m_pf) != (size_t) nMoveNow ||
			fseek(m_pf, AT + nMoved, SEEK_SET) ||
			fwrite(pb, 1U, (size_t) nMoveNow, m_pf) != (size_t) nMoveNow)
			throw err::CCException(cerror, __FILE__, __LINE__);
		nMoved += nMoveNow;
		nMoveNow = MIN(nBlockSz, TO_MOVE - nMoved);
	}
	if (cnt > 0)
		ftruncate(fileno(m_pf), sizeof m_nsz + (m_nsz - cnt));
	return (m_nsz -= cnt);
}

#undef TO_MOVE
#undef BUF_LEN

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBinFile::get читаю 'cnt' байт в позиции 'at', записываю прочитанное по адресу 'pb'.
/// \param at: отступ от начала файла
/// \param pb: хранилище, куда читаю
/// \param cnt: к-во байт в 'pb'
/// \return к-во прочитанных байт. Всегда равно 'cnt'. В сл.ошибки швыряю исключение.
///
int64_t CBinFile::get(const int64_t at, void *pb, const int64_t cnt) const
{
	ASSERT(m_pf != NULL);
	ASSERT(at >= 0 && cnt > 0 && at + cnt <= m_nsz);
	if (m_nsz - at < cnt) {
		const char *pb;
		double f = glb::Bytes(m_nsz + sizeof m_nsz, &pb);
		throw err::CLocalException(SI_OFL, time(NULL), glb::FileName(m_sPath, m_sPath.GetCount()),
								   2, f, pb, cnt, AT, __FILE__, __LINE__);
	}
	if (fseek(m_pf, AT, SEEK_SET) ||
		fread(pb, 1U, (size_t) cnt, m_pf) != (size_t) cnt)
		throw err::CCException(cerror, __FILE__, __LINE__);
	return cnt;
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CBinFile::getcp читает из файла в текущей позиции, если она известна. Быстрее
/// 'get' т.к. не устанавливает тек.позицыю в файле.
/// \param at: текущая позиция (без учёта размера m_nsz) только для проверки правильности
/// вызова.
/// \param pb: указатель на массив, в который читаю из файла.
/// \param cnt: к-во байт, подлежащих чтению и зфайла и записи в 'pb'.
/// \return к-во прочитанных байт.
///
int64_t CBinFile::getcp(const int64_t at, void *pb, const int64_t cnt) const
{
	ASSERT(m_pf && at >= 0 && cnt > 0 && at + cnt <= m_nsz);
	if (m_nsz - at < cnt) {
		const char *pb;
		double f = glb::Bytes(LONG_MAX, &pb);
		throw err::CLocalException(SI_OFL, time(NULL), glb::FileName(m_sPath, m_sPath.GetCount()),
								   2, f, pb, cnt, AT, __FILE__, __LINE__);
	}
	if (fread(pb, 1U, (size_t) cnt, m_pf) != (size_t) cnt)
		throw err::CCException(cerror, __FILE__, __LINE__);
	return cnt;
}

#undef CP
#undef AT

} // namespace fl