#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл defs.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * defs.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <climits>
#include <cstdint>	// типы (C) int32_t и т.д.
#ifdef __DEBUG
#include <cassert>
#define ASSERT(n) assert(n)
#else
#define ASSERT(n)
#endif // defined __DEBUG
// определения Q_OS_WIN32 Q_OS_WIN64 Q_OS_WINRT WINAPI_FAMILY_APP Q_OS_LINUX Q_OS_ANDROID Q_OS_MACOS
#include <QtGlobal>

typedef uint8_t BYTE;
typedef const uint8_t CBYTE;
typedef const char CCHAR;
typedef const int32_t cint32_t;
typedef const uint32_t cuint32_t;
typedef const int16_t cint16_t;
typedef const int64_t cint64_t;
typedef const uint64_t cuint64_t;
#define INV32_ID	0xffffffff
#define INV32_IND	INT_MIN
#define INV32_BIND	-1
#define INVOBJ_ID(o) (o.m_iObj == INV32_IND || o.m_iSym == 0xff || o.m_iPrd == 0xff || o.m_nType == -1)
#define INV_OFST	-1L

// наибольшее к-во вершин в файле (Линейщикъ)
#define MX_TOPS			524288
#define MX_ELEMENTS		262144

// 21.5.2021 вынесено из liner/FxBarBuf.h, что касается рабочего массива свечей
#define MX_BARS			640
#define ASSERT_VALID_INDEX(i) ASSERT((i) >= 0 && (i) < MX_BARS)
#define ASSERT_VALID_COUNT(n) ASSERT((n) >= glb::Mx(m_iNew, MinCount()) && (n) <= MX_BARS)

//#define DOC_VERSION		3		// версия документа
#define MX_STRLEN		256
#define MX_TICKS		2048	// к-во эл-ов в массиве CTicker
#define BUFLEN32		32		// длина строки с им.инструмента
#define BUFLEN64		64		// строка, длиной 64 байт
#define MX_PATH			1024	// длина пути к файлу

// напрвление: вверх, вниз, неопределено (Линейщикъ)
#define DIR_TOP		1   // направление вершины - вверх
#define DIR_BOT		2
#define DIR_CH		3   // непостоянное направление, как в случае с AO - может быть вверх, или вниз
#define DIR_LEFT	4
#define DIR_RIGHT	5
#define DIR_ANY		0   // любое направление
#define DIR_BUY		DIR_TOP
#define DIR_SELL	DIR_BOT
#define DIR_UP		DIR_TOP
#define DIR_DOWN	DIR_BOT
#define DIR_OPST(dir)	((dir == DIR_TOP) ? DIR_BOT : DIR_TOP) // обратное заданому
#define DIR_BEGIN	DIR_TOP
#define DIR_END		DIR_BOT

#define OP_BUY		DIR_UP
#define OP_SELL		DIR_DOWN
#define OP_INV		DIR_ANY

// сравнение double
#define DCMP_MORE(a, b, e) ((a) + (e) > (b) && (b) + (e) < (a))
#define DCMP_LESS(a, b, e) ((a) + (e) < (b) && (b) + (e) > (a))
#define DCMP_EQUAL(a, b, e) ((b) - (e) <= (a) && (b) + (e) >= (a))
#define DCMP_LOE(a, b, e) !DCMP_MORE(a, b, e)
#define DCMP_MOE(a, b, e) !DCMP_LESS(a, b, e)
#define DCMP_IN(v, mn, mx, e) (DCMP_MOE(v, mn, e) && DCMP_LOE(v, mx, e))
#define DCMP_BETWEEN(v, mn, mx, e) (DCMP_MORE(v, mn, e) && DCMP_LESS(v, mx, e))
#define DCMP_IN_RANGE(v, mn, mx, e) (DCMP_MOE(v, mn, e) && DCMP_LOE(v, mx, e))

// минимальные, максимальные значения
#define MIN(v1, v2) (((v1) > (v2)) ? (v2) : (v1))
#define MAX(v1, v2) (((v1) > (v2)) ? (v1) : (v2))
//#define MID(n1, n2) (MIN(n1, n2) + (MAX(n1, n2) - MIN(n1, n2)) / 2)
#define ABS(v) (((v) < 0) ? (v) * (-1) : (v))
// для действительных
#define FMIN(v1,v2,e) (DCMP_LESS(v1,v2,e) ? (v1) : (v2))
#define FMAX(v1,v2,e) (DCMP_MORE(v1,v2,e) ? (v1) : (v2))

// file & top counter
#define FL_NOT_FOUND	1
#define FL_PRTNDR_REM	2
#define FL_OLD			3
#define FL_NEW_TOP		4
#define FL_SUCCESS		0

// список log-list (Линейщикъ)
#define LVLL_COLUMNS	2
// логотипы настроения для log-списка
#define LLIM_GO			8				// зелёная морда, спокойная
#define LLIM_LCS		7				// пересечение линий SPD с поддержкой SPS
#define LLIM_TOP		6				// образование вершины
#define LLIM_LC			5				// пересечение линий
#define LLIM_ALERT		4				// рожа белая с открытым ртом
#define LLIM_NEUTRAL	3				// рожа без цвета
#define LLIM_WARNING	2				// розовая, недовольная
#define LLIM_SMILE		1				// жёлтая, довольная
#define LLIM_ERROR		0				// восклицание

// file names
/*#define INSTR_PERIOD_LIST_FILE	"Instruments.lst"
#define TEST_OPTIONS_FILE_NAME	"TestOptions.fxt"
#define OPTIONS_FILE_NAME		"Options.fxt"*/

// угол альфа в градусах
#define PI	3.1415926535897932384626433832795
// a - прилежащий (adjacent), o - противолежащий (opposite)
#define AngleA(a, o) (180.0 / PI * atan((o) / (a)))

// цена (или значение индикатора) в пунктах (накладный макрос - вызов функции pow()
#define Points(fv, iPrec) (fv * pow(10.0, iPrec))

// индексы полей
enum FxFields
{
	fIndex,			// 0
	fTime,			// 1
	fOpen,			// 2
	fHigh,			// 3
	fLow,			// 4
	fClose,			// 5
	fVolume,		// 6
	fSpd,			// 7
	fSps,			// 8
	fAvg1,			// 9
	fAvg2			// 10
};

#define PRC_FIELD(f) (f == fHigh || f == fLow || f == fOpen || f == fClose)

// состояния периода
enum InstrState
{
	Neutral,
	TrendUp,
	LastingTrendUp,
	TrendDown,
	LastingTrendDown,
	UturnUp,
	UturnDown,
	Error
};

// идентификаторы сообщений msg_FromWorker (Линейщикъ ещё в Wind'е, сообщения от раб.потока основному)
#define PRC_SEQ_SET		1	// CStaticContainer
#define PRC_SEQ_DEL		2	// удаляется пос-ть ценовых вершин
#define LOG_MSG			3	// CLogMsg
#define OPT_REST		4	// перезапуск во время оптимизации
#define INSTR_INIT		5	// инициализация инструмента завершилась успешно
#define ANLSR_STATE		6	// состояние FxSpdDB::CAnalyser - структура CSpdDBParam::CStateMsg
#define WKR_TERM		7	// DWORD WINAPI FxTesterWorker(LPVOID pInstrInfo) завершает работу
#define ORDR_UPDATE		8	// заказ (CTradeOrder) изменился - требуется перерисовка в списке. lParam = order.m_dwID
#define LINE_NEW		9	// новая линия		аргумент - идентификатор линии
#define LINE_DEL		10	// линия удалена	аргумент - идентификатор линии
#define NEW_TICK		11	// новый тик		аргумент - идентификатор периода
#define NEW_BAR			12	// новый бар		аргумент - идентификатор периода
#define NEW_TOP			13	// новая вершина
#define UPD_TOP			14	// обновление вершины
#define OPT_CHANGE		15	// изменились настройки пользователя в рабочем потоке
#define TEST_BALANCE	16	// баланс инструмента
#define SPD_TOPLVL_CH	17	// изменилась вершина-уровень индикатора
#define SPD_LVL_STOVE	18	// индикатор пробивает уровень
#define SPD_LVL_STOVES	19	// индикатор пробивает уровень. Следует вывести строку в log
#define SPD_SEQ_5TH_TOP	20	// сформировалась 5-я вершина пос-ти SPD
#define PRC_S_ROOT_LVL	21	// сфрпмипрвался корневой уровень ценовой последовательности
#define SPDDVC_NEW_4TH	22	// появление новой 4-й вершины в пос-ти с дивером...
#define SPD_SEQ_SET		23	// добавляется/обновляется пос-ть вершин индикатора
#define SPD_SEQ_DEL		24	// удаляется пос-ть вершин индикатора
#define SPD_IN_TRGL		25	// индикатор находится в треугольнике, образованном горизонтальной линией правой вершины дивера и наклонной линией дивера
#define STTU_SET		26	// новый элемент статистики пос-ти вершин индикатора
#define STTU_DEL		27	// удалён эл-т статистики пос-ти вершин индикатора
#define STTU_CHECK_END	28	// завершилась проверка рейтинга эл-та статистики
#define HOUR_CLOSE		29	// закрылся час...
#define NEW_TOP_LVL		30	// новый автоматический уровень вершины
#define DEL_TOP_LVL		31	// удалён корневой или активный уровень вершины. Аргумент - массив, завершённый недействительным эл-том
#define TOP_STOVEN		32	// 31.10.2018 интересное совпадение: последний день десятого месяца... Очень важное будет сообщение.
#define LNS_CROSS		33	// 11.01.2019 пересечение линий - цена вблизи от уровня вершины, индикатор - от уровня эл-та стат-ки
#define NPI_VOLUME		34	// 22.02.2019 no period indicator значительный объём на иснструменте
#define FIBO_SET		35	// добавлен новый уровень фибо (на данный момент, пришёл из сети)
#define SPDS_GROW		36	// новая свеча пробивает вершину пос-ти
#define SG4THTOP_TOUCH	37	// касание 4-й вершины - вероятный "Рост индикатора" 5 вершин и формирование 6-й вершины
#define SPD_AVG_CRS		38	// пересечение индикатора и среднего выше длинного среднего
#define SPD_SYMMETRY	39	// симметричная пос-ть эл-та стат-ки
// пользовательские сообщения
#define WM_USER_MAX			0x7fff
#define WM_MYOPT_CNANGE		(WM_USER + 77)
#define WM_WRKR_TERM		(WM_USER + 78)	// завершилась работа потока
#define	WM_CHART_CLOSE		(WM_USER + 79)	// окно с графиком закрыто
#define	WM_TICKER_CLOSE		(WM_USER + 80)	// окно с индикатором (секундомером) закрыто
#define WM_LISTS_FRM_CLS	(WM_USER + 81)	// закрылось окно со списками: уровни, линии, эл-ты стат-ки.
#define WM_SEQ_CHANGE		(WM_USER + 82)	// изменилась пос-ть		хуйня кака-то происходит с ёбаными микрософтами... Блядь, компилятор не видит это определение!
#define WM_LVLSTVNFRM_CLS	(WM_USER + 83)	// окно со списком пробитых уровней закрыто нахуй
#define WM_GRPSFRM_CLS		(WM_USER + 84)	// закрылось окно со списком групп

// types
typedef FxFields FIELDS;
/*typedef const FxFields CFIELDS;
typedef const unsigned long CDWORD;
typedef unsigned __int64 UBINT;							// class wizard не знает, что такое unsigned __int64
typedef __int64 BINT;*/									// та же причина

// символы, число
#define IsDigit(n) (n >= '0' && n <= '9')
// пробел
#define IsWS(n) (n == ' ' || n == '\t' || n == '\r' || n == '\n')
// допустимый разделитель в записи даты
#define ValidDtDel(c) (c == '.')
// нуль - завершение строки
#define IsZero(c) (c == '\0')
// действительный разделитель между датой и временем
#define ValidDtTmDel(c) (IsWS(c) || IsZero(c))
// действительный разделитель в записи времени
#define ValidTmDel(c) (c == ':' || IsZero(c))
// действительный день месяца
#define ValidMonthDay(n) (n > 0 && n < 32)
// действительный год
#define ValidYear(n) (n > 69 && n < 137)
// действительный месяц
#define ValidMonth(n) (n > (-1) && n < 12)
// действительный час
#define ValidHour(n) (n > (-1) && n < 24)
// действительная минута\секунда
#define ValidMinSec(n) (n >= 0 && n < 60)
///
#define PATH_DEL

// результат сравнения функции CompareStrings
#define CSTR_GREATER		3
#define CSTR_EQUAL			2
#define CSTR_LESS			1

// языковые идентификаторы
#define LCID_EN	MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_ENGLISH_US),SORT_DEFAULT)
#define LCID_RU	MAKELCID(MAKELANGID(LANG_RUSSIAN,SUBLANG_DEFAULT),SORT_DEFAULT)

// идентификаторы событий происходящих в документе для отображения изменений в представлениях: LWORD32(lParam)
#define DH_INSTR_CHANGE		1	// изменение индексов CFxTesterDoc::m_iSymbol и m_iPeriod - отображение новых данных
#define DH_INITIALIZE		2	// документ открыт, нужно вывести содержимое
#define DH_INITIALIZE2		3	// закончилась инициализация рабочего потока
#define DH_NEW_INSTR		4	// новый грёбаный инструмент
#define DH_NEW_BAR			5	// закрылся новый бар
#define DH_NEW_TICK			6	// пришёл новый тик
#define DH_NEW_TOP			7	// сформировалась новая вершина
#define DH_UPT_TOP			8	// изменилась имеющаяся вершина
#define DH_TRD_ORDR			9	// изменился фокус в списке заказов
#define DH_DEL_INSTR		10	// инструмент удалён из документа
#define DH_DEL_CONTENT		11	// удаляется содержимое документа во всех представлениях
#define DH_NEW_LINE			12	// имеется новая линия (в аргументе-указателе UpdateAllViews)
#define DH_LN_SELCHANGE		13	// изменилось выделение в статичестком списке линий
#define DH_TOP_SELCHANGE	14	// изменилась активная вершина в списке вершин. Аргумент - указатель на время time_t
#define DH_BAR_SELCHANGE	15	// изменился индекс выделенного бара
#define DH_UNPAUSE			16	// снята пауза - обновление представлений
#define DH_REINDEX			17	// переиндексация массива баров в периоде
#define DH_LINESREALLOC		18	// изменение расположения массива линий периода
#define DH_HIGH_REALLOC		19	// перераспределение вершин в периоде
#define DH_LOW_REALLOC		20	// перераспределение оснований в периоде
#define DH_SEQ_SELCHANGE	21	// изменилась последовательность - не период и не инструмент
#define DH_PRD_CHANGE		22	// изменился выбор периода, не инструмента
#define DH_FL_SELCHANGE		23	// активный эл-т дерева "уровни фибо"
#define DH_FIBO				24	// добавлен или удалён уровень. Аргумент - идентификатор периода
#define DH_DEL_FIBO			25	// удалён
#define DH_LVLSTVN			26	// пробита вершина
#define DH_NEW_TOPLEVEL		27	// новый уровень вершины
#define DH_DEL_LINE			28
#define DH_DEL_TOPLEVEL		29
#define DH_LN_GRP_OBJ		30
//#define DN_LNS_REINDEX		15	// миссив линий изменён - идентификаторы всех элементов массива изменены (они считаются статическими)

#define HHALF32				0xffff0000
#define LHALF32				0x0000ffff
#define HWORD32(n) ((n) >> 16 & LHALF32)
#define LWORD32(n) ((n) & LHALF32)
#define MKLONG32(h, l) ((unsigned int) (h) << 16 & 0xffff0000 | (unsigned int) (l) & 0x0000ffff)
// верхняя/нижняя половина 64 бит
#define HHALF64				0xffffffff00000000
#define LHALF64				0x00000000ffffffff
// индекс инструмента - 8 бит, индекс периода - 8 бит, индекс эл-та массива третего уровня - 16 бит
// извлечение индекса из идентификатора (32 bit)... теперь вместо 32-х битного целого структура CObjID
#define SYM3(n) (n).m_iSym
#define PRD3(n) (n).m_iPrd
#define TP3(n) (n).m_nType
#define INDX3(n) (n).m_iObj
// установка индексов - определение идентификатора (32 bit) 
// первый аргумент - индекс, второй - идентификатор (не может быть выражением)
#define SSYM3(i,n) n.m_iSym = i
#define SPRD3(i,n) n.m_iPrd = i
#define SINDX3(i,n) n.m_iObj = i
// сравнение идентификаторов. Довольно кривое.
#define CMPID3(n1, n2) (\
(SYM3(n1) < SYM3(n2)) ? -1 : (SYM3(n1) > SYM3(n2)) ? 1 : \
(PRD3(n1) < PRD3(n2)) ? -1 : (PRD3(n1) > PRD3(n2)) ? 1 : \
(TP3(n1) < TP3(n2)) ? -1 : (TP3(n1) > TP3(n2)) ? 1 : \
(INDX3(n1) < INDX3(n2)) ? -1 : (INDX3(n1) > INDX3(n2)) ? 1 : 0)
// доступ к идетнифицируемым элементам g_instrs: 
// указатель на период по идентификатору линии, заказа и другому трёхзначному идентификатору
//#define GET_PERIOD(id) g_instrs[SYM2(id)]->m_prds[PRD2(id)]
#define GET_PERIOD(id) g_instrs[SYM3(id)]->m_prds[PRD3(id)]
// ссылка на заказ по идентификатору заказа
#define GET_ORDER(id) GET_PERIOD(id)->m_og.m_ords[INDX3(id)]
// ссылка на линию по идентификатору линии
#define GET_LINE(id) GET_PERIOD(id)->m_lns[INDX3(id)]
// возвращает ссылку на пос-ть
#define GET_SEQMAN(id) GET_PERIOD(id)->m_sm
#define GET_PRC_SEQUENCE(id) GET_SEQMAN(id).m_prc[INDX3(id)]
#define GET_SPD_SEQUENCE(id) GET_SEQMAN(id).m_spd[INDX3(id)]
#define GET_STAT_UNIT(id) GET_SEQMAN(id).m_aStat[INDX3(id)]
#define GET_TOPlEVEL(id) GET_PERIOD(id)->m_aLevels[INDX3(id)]
#define GET_FIBOlEVEL(id) GET_PERIOD(id)->m_aFibo[INDX3(id)]
#define GET_LNSMAN(id) GET_PERIOD(id)->m_al
//
// индексы в индексированном массиве линий периода
#define IND_FUNC_INIT	{ cmplt, cmplv, cmpseqid, cmplnf }	// инициализация массива функций сравнения для индексированного массива линий с двумя индексами - 1) по значению линии и 2) по её вершинам
#define ALI_LRT		0		// по времени и направлению вершин
#define ALI_VAL		1		// индексирование по значению линии
#define ALI_SEQ		2		// сравнение по идентификатору последовательности
#define ALI_LNF		3		// сравнение по флагам принадлежности линии к последовательности
#define ALI_CNT		4
//#define ALI_DIST	1		// удаление от текущей цены - distance

// периоды в секундах (Oanda)
#define PS5				5
#define PS10			10
#define PS15			15
#define PS30			30
#define PM1				60
#define PM2				120
#define PM4				240
#define PM5				300
#define PM10			600
#define PM15			900
#define PM30			1800
#define PH1				3600
#define PH2				7200
#define PH3				10800
#define PH4				14400
#define PH6				21600
#define PH8				28800
#define PH12			43200
#define PD1				86400
#define PW1				604800
#define PMN1			2592000

// time period up - если нужно поднять время открытия бара до большего периода
// t - время открытия свечи меньшего периода (time_t)
// prd - больший период в минутах 23.12.2019 добавляю недельный период 22.8.2021 период в секундах
// 1.1.1970 - четверг. Неделя у пендосов и в AlPari начинается в воскресенье. У кого-то может в понедельник?
#define FOUR_DAYS	345600	// 4 дня в секундах.
#define TMPRDUP(t, prd)	((t) - ((t) + (((prd) == PW1) ? FOUR_DAYS : 0)) % (prd))

//#define INVALID_INT	-1			// недействительное знаковое/беззнаковое значение

// идентификаторы состояния индикатора (SPD Sequence State)
#define SS_ABV_LVL		0x00000001		// индикатор находится выше уровня
#define SS_UNDR_LVL		0x00000002		// индикатор находится ниже уровня
#define SS_LT_ABV		0x00000004		// претендент или последняя вершина находится выше уровня
#define SS_LT_UNDR		0x00000008		// претендент или последняя вершина находится ниже уровня
#define SS_DVC_DN		0x00000010		// имеется дивер вниз - основание пос-ти выше уровня
#define SS_DVC_UP		0x00000020		// имеется дивер вверх - основание пос-ти ниже уровня
#define SS_DIR_DOWN		(SS_ABV_LVL|SS_LT_ABV|SS_DVC_DN)		// сигналит вниз
#define SS_DIR_UP		(SS_UNDR_LVL|SS_LT_UNDR|SS_DVC_UP)	// сигнилит вверх

// возвращаю уровень вершины
#define TOP_LEVEL(top) LWORD32((top).m_iLevel)
// возвращаю уровень претендента вершины
#define PRET_LEVEL(top) HWORD32((top).m_iLevel)
// достаточно ли высокий уровень претендента и вершины (top) согласно настройкам п-ля (lno)
#define VALID_LINE_TOP(opt, top) (PRET_LEVEL(top) >= opt.m_nPretLevel && TOP_LEVEL(top) >= opt.m_nTopLevel)
// требуемый уровень вершины
#define REQUIRED_LINE_TOP(opt, top) (PRET_LEVEL(top) >= opt.m_nPretLevel && TOP_LEVEL(top) == opt.m_nTopLevel)

// параметры для тестирования
/*enum OptParameter
{	// общие
	opNone = -2,			// значение не задано										-2
	opPeriod,				// период инструмента										-1
	opHistPrd,				// период просматриваемой истории							0
	opMaxOrdrPrd,			// максимальный из периодов допустимый к торговле			1
	opMinOrdrPrd,			// минимальный из периодов допустимый к торговле			2
	opPrdsInSet,			// к-во периодов в наборе									3
	opLinesInSet,			// к-во линий в наборе										4
	opSpdSeqPrd,			// к-во периодов с сигналом SPD								5
	opSpdSeqMinPrd,			// индекс минимального периода в пос-ти периодов с сигналом PSD		6
	// заказ
	opOrdrType,				// тип заказа												7
	opDynOrdr,				// динамическое изменение типа заказа						8
	opStopPace,				// шаг для изменения стопа									9
	opProfLevel,			// уровень цели	в процентах									10
	opMinProfit,			// минимальная цель в пунктах								11
	opSpdLevel1,			// new: верхний уровень индикатора							12
	opSpdLevel2,			// new: нижний уровень индикатора							13
	opSpdTopLevel,			// new: уровень вершины индикатора							14
	// линии
//	opLnThicks,				// толщина линии в пунктах
	opLnDelMin,				// допуск на пробой в пунктах								15
	opLnDelMax,				// допуск на удаление от текущей цены						16
	opLnOIUp,				// отступ вверх от линии для заказа							17
	opLnOIDown,				// отступ вниз от линии для заказа							18
	opLnSvrStop,			// стоп заказа для сервера от линии							19
	opLnLclStop,			// местный стоп												20
	opShdwKills,			// пробой линии тенью бара									21
	opReorder,				// повторный заказ от линии									22
	opPretLevel,			// уровень претендента										23
	opTopLevel,				// уровень вершины											24
	opFilter,				// тип фильтрации линий - параллель, или оба вида			25
	// к-во:
	opCount					// к-во 'не-периодов'										26
};*/

#define TIMER_SELECT_BAR	USHRT_MAX		// таймер в CChartView для мерцания выделенных баров
#define TIMER_STATUS_BAR	(USHRT_MAX + 1)	// таймер в CMainFrame для отображения соотщения в строке состояния
#define TIMER_AUTOSAVE		(USHRT_MAX + 2)	// автосохранение в гл.окне

// сообщение от рабочего потока основному
#define WTM_BARDEC		0x01	// переиндексация свечей периода
#define WTM_LNSRENX		0x02	// перераспределение линий
#define WTM_TOPS_HIGH	0x04	// перераспределение вершин CSeqMan::m_high
#define WTM_TOPS_LOW	0x08	// перераспределение вершин CSeqMan::m_low
//#define WTM_TOPS_SPD	0x10	// перераспределение вершин CSeqMan::m_spd

// аргумент dwWakeMask в ф-ции MsgWaitForMultipleObjects: весь ввод, кроме таймера (QS_TIMER) и сообщений, посланных 
// другими потоками или приложениями (QS_SENDMESSAGE).
#define QS_WAITINPUT	(QS_ALLEVENTS & ~(QS_TIMER|QS_SENDMESSAGE))	//(QS_INPUT|QS_POSTMESSAGE|QS_PAINT|QS_HOTKEY)

// окончание в слове "пункт" от к-ва пунктов
#define PNT_END(n) ((ABS(n) > 10 && ABS(n) < 20 || !(ABS(n) % 10) || ABS(n) % 10 >= 5 && ABS(n) % 10 <= 9) ? "ов" : (ABS(n) % 10 >= 2 && ABS(n) % 10 <= 4) ? "а" : "")
#define TPS_END(n) ((ABS(n) > 10 && ABS(n) < 20 || !(ABS(n) % 10) || ABS(n) % 10 >= 5 && ABS(n) % 10 <= 9) ? "" : (ABS(n) % 10 >= 2 && ABS(n) % 10 <= 4) ? "ы" : "а")

// формат времени для _tcsftime
/*#define TM_FORMAT(nPeriod) (((nPeriod) < 60) ? _T("%d.%m.%Y %H:%M") : ((nPeriod) < 1440) ? _T("%d.%m.%Y %H") : _T("%d.%m.%Y"));
#define TM_VFORMAT(tmf) (((tmf) % 86400) ? _T("%d.%m.%Y %H:%M") : _T("%d.%m.%Y"))
*/
// IsWindow
/*#define IS_WINDOW(pw) ((pw) != nullptr && (pw)->m_hWnd != INVALID_HANDLE_VALUE && ::IsWindow((pw)->m_hWnd) == true)
#define IS_VIEW(pw) (IS_WINDOW(pw) == true && (pw)->IsKindOf(RUNTIME_CLASS(CView)) == true)
#define IS_KINDOF(pw, RT_Class) (IS_WINDOW(pw) == true && (pw)->IsKindOf(RUNTIME_CLASS(RT_Class)) == true)*/

// типы отображения линий в CLinesListCtrl
#define VM_DYNAMIC	0x01	// отображать без фильтра
#define VM_STATIC	0x02	// представление фильтруется
#define VM_BYTOP	0x04	// отображаются по выделенной вершине (правая в линии)
#define VM_SEL		0x08	// отображаются только выбранные пользователем линии одного периода
#define VM_AP		0x10	// отображаются линии всех периодов одного инструмента
#define VM_SEQ		0x20	// отображаются линии определённой пос-ти
#define VM_CORRECT(n)	((n & (VM_DYNAMIC|VM_STATIC)) != (VM_DYNAMIC|VM_STATIC) && \
						(n & (VM_BYTOP|VM_SEL)) != (VM_BYTOP|VM_SEL))

// список ф-ций для инициализации массива указатей на ф-ции сравнения в индексированном файловом массиве CFxArrayFl
// 1. &sucmpseqtops		- сравнение пос-тей по времени вершин. (уникальный индекс)
// 2. &sucmpavgfar		- сравнение по удалённости правой вершины дивера от среднего в процентах
// 3. &sucmplnangl12,	- сравнение по углу наклона линии 1-2
// 4. &sucmplnangl23,	- сравнение по углу наклона линии 2-3
// 5. &sucmplnangl34,	- сравнение по углу наклона линии 3-4
// 6. &sucmplnsavg,		- сравнение по средней длине 3-х линий: 1-2, 2-3 и 3-4, где 1 - левая вершина дивера
// 7. &sucmptrngssqr,	- сравнение по соотношению площадей треугольников с вершинами 1-2-3 и 2-3-4
// 8. &sucmpstartlevel	- сравнение по уровню, от которого цена пошла в прибыль
// 9. &sucmplnsanglesandlengths - сравнение по среднему углов и длин трёх линий от лев.вершины дивера
/*#define SPD_STAT_CMP_ARR_LIST	&sucmpseqtops,		\
								&sucmpavgfar,		\
								&sucmplnangl12,		\
								&sucmplnangl23,		\
								&sucmplnangl34,		\
								&sucmplnsavg,		\
								&sucmptrngssqr,		\
								&sucmpstartlevel,	\
								&sucmplnsanglesandlengths
#define SPD_STAT_CMP_ARR_NUM	9
#define SUFA_LALA				8	*/// StatUnitFileArray - Lines Alngles and Lengths Avarage - индекс сравнения по средним значениям углов и длин трёх линий от лев.вершины дивера

#define RED(n) (n & 0x000000ffU)
#define GREEN(n) ((n & 0x0000ff00U) >> 8)
#define BLUE(n) ((n & 0x00ff0000U) >> 16)

// CKey state
#define KEYSTATE_VALID			0x55555555
#define KEYSTATE_TIME			(KEYSTATE_VALID|0x00000002)

// вертикально, в представлении (начало координат в верхнем левом углу)
#define IN_VIEW(n, u, d) (n > u && n < d)

// типы объектов
#define OT_NONE			-1	// исходное состояние - нуль
#define OT_LINE			1	// линия
#define OT_TSEQ			2	// ценовая пос-ть вершин
#define OT_SSEQ			3	// пос-ть вершин индикатора
#define OT_FIBO			4	// какой-то уровень fibo
// эл-т стат-ки
#define OT_SUNIT		5	// эл-т стат-ки
#define OT_SURT			6	// уровень правой вершины эл-та стат-ки
#define OT_SUE			7	// уровень среднего эл-та стат-ки
#define OT_SUD			8	// линия дивера эл-та стат-ки
#define OT_SUDP			9	// линия дивера на ценовых вершинах эл-та стат-ки
// инструмент и период
#define OT_PRD			13	// период инструмента
#define OT_INSTR		14	// инструмент
// уровень ценовой вершины
#define OT_TLVL			15	// уровень вершины
#define OT_TLM			16	// уровень среднего
#define OT_TLB			17	// цена открытия/закрытия свечи
//
#define OT_SLGRP		18	// группа в CSortedLevels
#define OT_SLE			19	// элемент в CSortedLevels
#define OT_FMAN			20	// CFileMan
#define OT_FM			21	// CFileMan
#define OT_PROD_INGT	22	// ингредиент продукта

/* Разделитель папок/файлов (пути) в операционке. Похоже, тольков wind'е он отличается.
 * Ошибка errno в Wind'е отличается...
 */
#if defined Q_OS_WIN32 || defined Q_OS_WIN64 || defined Q_OS_WINRT || defined WINAPI_FAMILY_APP
#	define CPSEP '\\'
#	define SPSEP "\\"
#	define CSSEP '\r'
#	define SSSEP "\r\n"
#	define cerror _doserrno
#elif defined Q_OS_LINUX || defined Q_OS_ANDROID || defined Q_OS_MACOS
#	define CPSEP '/'
#	define SPSEP "/"
#	define CSSEP '\n'
#	define SSSEP "\n"
#	define cerror errno
#else
#   error смотри чадо, какой разделитель файлов/папок в операционке
#   error проверь константы errno в своей операционке, они должны соответствовать
#define cerror -1
#endif