#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_intFileShrd.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_intFileShrd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_flIndIShrd.h"
#include "io_indexIntegrity.h"
#include "io_opener.h"
#include "global.h"
#include "io_FileItem.h"
#include "../../common/iTgDefs.h"

namespace fl {
namespace a {
extern CIndexIntegrity ii;
///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief class CiShrd индексированный файловый массив с объектами одинакового размера, с синхронизи-
/// рованным доступом между потоками и процессами с индексами, размещёнными в общей памяти.
/// Так как объект массива хоть и статич.размера, может содержать виртуальные ф-ции, здесь размер объекта
/// указывается в шаблоне, его размер не берётся оператором sizeof и базовый класс не CIntFile, а CBinFile.
/// Поэтому следует следить за тем, чтобы адрес объекта брался индивидуально, объекты этого класса нельзя
/// записывать массивом этих объектов.
/// По новому соглашению от 23.08.2024 задействуется логика не пустого эл-та и, как и прежде, - логика
/// целостности уникальных индексов. См. fl::a::SetItem()
/// Наследуют: store::CResidues, store::CIncomes, store::CGoods, store::CExpends, store::CIngredients,
///			   cafe::CJobs, cafe::CPostAdrss,
/// \param Obj: объект массива, производный от CIntFileItem.
/// \param sz: размер файлового объекта Obj в байтах
/// \param nInds: к-во индексов. См. CIndex и его производные CIndexLocal и CIndexShared.
///
template <class Obj, size_t szo, int nInds>
class CiShrd : public CBinFile
{
public:// тип индекса этого индексированного массива:
	typedef i::CIndIShrd<Obj,nInds> INDEX;
	typedef CiShrd<Obj, szo, nInds> THIS;
	typedef CBinFile BAS;
//	typedef glb::CFxArray<BYTE, true> BYTES;
private:// меж-процессорная и меж-потоковая синхронизация доступа к файловому массиву:
	CFileMan::CLocker m_lock;
	INDEX m_ind;
public:
	CiShrd();// для массивов
	CiShrd(const char *pcszKey, const char *pcszPN, typename INDEX::fcmp *ppf, bool bopen = true);
	~CiShrd();
protected:
	virtual INDEX* GetIndex();
public:// открытия/закрытия
	virtual int64_t open(const char *pcszPN) override;
	virtual int64_t open(const char *pcszKey, const char *pcszPN);
	virtual void close() override;
	virtual void drop();
	// расширенные возможности для открытия файла без параметров
	virtual int64_t open();
	virtual const char* PathName();	// путь и имя файла (производный класс обязан предъявить)
	virtual const char* Key(glb::CFxString& s);// ключ к файлу для QSharedMemory (производный класс обязан предъявить)
public:// редактирование файлового массива
	virtual int32_t Add(Obj &o);
	virtual int32_t AddUnic(Obj &o);
	virtual int32_t Edit(Obj &o, int iInd, int iObj, bool bIncrsRef = false);
	virtual int32_t EditUnic(Obj &o, int iInd, int iObj, bool bSafe = true);
	virtual int Rem(const int iInd, const int iAt); // удаляю 1 эл-т, от 'iAt' по индексу 'iInd'
	virtual int32_t Invalidate(int32_t iInd, int32_t iObj, bool bRmFrmPrnt = false);
	virtual void Empty();	// обнуляет содержимое файла
public:// извлечение
	virtual const Obj& GetItem(int iInd, int i, Obj &o) const;
	virtual Obj& GetItem(int iInd, int i, Obj &o);
	virtual const INDEX* GetIndex() const;
//	virtual int32_t UnicIndex() const;
	virtual const int32_t* UnicIndices(int32_t *pcnt) const;
	virtual int32_t FindEqual(const Obj &o, int32_t iInd, int32_t iBas = INV32_BIND,
							  int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t FindNearest(const Obj &o, int32_t iInd, int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t FindValid(const Obj &o, int32_t iInd, int32_t iStart = 0, int32_t iEnd = -1) const;
	virtual int32_t Reindex(const Obj &o, int32_t iInd, int32_t iCur);
public:// атрибуты
	int32_t Count() const;
	virtual bool IsOpen() const override;
	virtual int32_t BaseIndex(int32_t iInd, int32_t i) const;
	bool IsValid(int32_t iind, int32_t iitm) const;
//	bool IsStatic(const Obj *po) const;
public:// извлечение ранее загруженных объектов
	std::recursive_mutex& Mtx();
	Obj& StoredObj(int32_t iBas);
	static BYTES* io_buf();
	static Obj* static_obj();
};

template<class Obj, size_t szo, int nInds>
CiShrd<Obj, szo, nInds>::CiShrd()
{
	ASSERT(nInds >= 0 && nInds < 255);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd конструктор CiShrd откроет индексированный синхронизированный
/// файловый массив, если bopen не нуль. Если нуль, файл не будет открыт, будет создан пустой,
/// бездействующий объект. После его можно будет задействовать ф-цией Open.
/// \param pcszKey: уникальный идентификатор файла. Может быть нулём, если !bopen
/// \param pcszPN: путь и имя открываемого файла. Может быть нулём, если !bopen
/// \param ppf: массив ф-цйи сравнения индексированного файлового массива. Должен быть предоставлен в к-ве nInds.
/// \param bopen: открывать ли файл? Если нуль, попытка открыть файл не будет предпринята.
/// соответственно, уникальный идентификатор файла и путь могут быть недействительными.
///
template<class Obj, size_t szo, int nInds>
CiShrd<Obj,szo,nInds>::CiShrd(const char *pcszKey,		// ключ см. CFileMan::CFile::MkKey
                              const char *pcszPN,		// путь и имя файла
                              typename INDEX::fcmp *ppf,// ф-ции сравнения для каждого индекса в к-ве nInds
                              bool bopen)				// bopen = true
	: m_lock(pcszKey)
{
	ASSERT(nInds >= 0 && nInds < 255);
	ASSERT(!bopen || pcszKey && pcszPN);
	m_ind.Set(ppf);
	if (bopen)
		open(pcszPN);
	ASSERT(!(m_nsz % szo));
}

template<class Obj, size_t szo, int nInds>
CiShrd<Obj, szo, nInds>::~CiShrd()
{
	drop();
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief Open откроет индексированный файл с синхронизацией между потоками и процессами на
/// уровне операционки. Эту ф-цию следует вызывать если в объект уже был отправлен ключ. На-
/// пример, при повторном отркрытии файлового массива.
/// За 'open' следует звать 'close'.
/// \param pcszPN путь и имя файла
/// \return к-во элементов в файле
///
template<class Obj, size_t szo, int nInds>
int64_t CiShrd<Obj, szo, nInds>::open(const char *pcszPN)
{
	ASSERT(pcszPN || m_sPath.GetCount());
	ASSERT(nInds >= 0 && nInds < 255);
	ASSERT(!IsOpen());
	m_lock.Lock();
	BAS::open(pcszPN);			// швырнет исключение, если что не так...
	ASSERT(!(m_nsz % szo));
	return m_ind.open(pcszPN);	// откроет, прочтёт и закроет файл
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief fl::CiShrd::open откроет индексированный файл с синхронизацией между потоками
/// и приложениями на уровне операционки. За 'open' следует звать 'close'.
/// \param pcszKey: ключ файла см. CFileMan::CFile::MkKey
/// \param pcszPN: путь к файлу
/// \return к-во эл-тов в файле
///
template<class Obj, size_t szo, int nInds>
int64_t CiShrd<Obj, szo, nInds>::open(const char *pcszKey, const char *pcszPN)
{
	ASSERT(!IsOpen());
	m_lock.Lock(pcszKey);
	BAS::open(pcszPN);			// швырнет исключение, если что не так...
	ASSERT(!(m_nsz % szo));
	return m_ind.open(pcszPN);	// откроет, прочтёт и закроет файл
}

template<class Obj, size_t szo, int nInds>
void CiShrd<Obj, szo, nInds>::close()
{
	ASSERT(nInds >= 0 && nInds < 255);
	ASSERT(IsOpen());
	ASSERT(!(m_nsz % szo));
	m_ind.close();
	BAS::close();
	m_lock.Unlock();// индексы остаются в разделяемой памяти
}

template<class Obj, size_t szo, int nInds>
void CiShrd<Obj, szo, nInds>::drop()
{
//	ASSERT(!(m_nsz % szo)); тут нельзя т.к. зацикливает, если были изменения в структуре объекта
	if (IsOpen())
		close();
	// если объект не использовался, индексы перезаписывать не нужно
	if (!m_lock.IsValid())
		return;
	m_lock.Lock();	// ограничение доступа и к индексам тоже
	if (m_ind.IsValid())
		m_ind.drop();	// завершит использование разделяемой памяти, запишет каждый индекс в файл
	m_lock.Unlock();
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::open открываю синхронизированный, гибридный файловый массив без
/// параметров. Ключ и путь предоставят соответствующие ф-ции. В случае повторного открытия
/// ключ и путь не требуются.
/// \return к-во эл-тов массива
///
template<class Obj, size_t szo, int nInds>
int64_t CiShrd<Obj, szo, nInds>::open()
{
	ASSERT(nInds >= 0 && nInds < 255);
	ASSERT(!IsOpen());
	if (!m_sPath.GetCount()) {
		glb::CFxString sKey;
		return open(Key(sKey), PathName());
	}
	return open(m_sPath);
}

template<class Obj, size_t szo, int nInds>
const char* CiShrd<Obj, szo, nInds>::PathName()
{
	ASSERT(!(m_nsz % szo));
	ASSERT(false);// для производного класса
	return NULL;
}

template<class Obj, size_t szo, int nInds>
const char* CiShrd<Obj, szo, nInds>::Key(glb::CFxString& s)
{
	ASSERT(false);// для производного класса
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::Add добавляет эл-т в конец файла и во все индексы.
/// \param o: добавляемый объект
/// \return идентификатор - индекс объекта в файле
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::Add(Obj &o)
{
	ASSERT(o.IsValid() && !o.IsEmpty());
	// блокирую доступ к файлу и открываю файл
	COpener<THIS> opnr(*this);
	// декларацыи, иницыацыи...
	int32_t i, ai[nInds], iBas = m_ind.Del() ? 0 : -1;
	// новый эл-т, к-во ссылок 1
	o.m_nref = 1;
	// если есть не действительные эл-ты...
	if (iBas >= 0) {
#ifdef __DEBUG
		Obj o2;
		GetItem(idel, 0, o2);
		ASSERT(!o2.IsValid() && !o2.m_nref);
#endif
		return Edit(o, idel, 0);// удалит iBas из удалённых, вернёт базовый индекс нового эл-та
	}// имеющиеся индексы:
	iBas = (int32_t) (m_nsz / szo);
	for (i = 0; i < nInds; i++)
		ai[i] = m_ind.SetInd(i, FindNearest(o, i), iBas);
	// время изменения и время создания равны
	o.m_tme = o.m_tmc = time(nullptr);
	try {// в конец основного файла
		BYTES &ab = *io_buf();
		ab.SetCountZeroND();
		i = o.GetBinData(ab);
		ASSERT(i == szo);
		BAS::put(ab, szo);
	} catch (...) {
		for (i = 0; i < nInds; i++)
			m_ind.RemNC(i, ai[i]);
		throw;
	}
	return iBas;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::AddUnic ф-ция для массивов с уникальным индексом, иначе не имеет смысла.
/// Добавит объект или изменит, если он уже записан в файл. В любом случае к-во ссылок 'o'
/// изменится, несмотря на его const.
/// \param o: добавляемый объект (изменится его m_nref)
/// \return уникальный ид-р объекта (базовый индекс) или меньше нуля если объект не добавлен
/// в файл. Причиной может быть попытка нарушения целостности уник.индекса или несовпадение
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::AddUnic(Obj &o)
{
	COpener<THIS> opnr(*this);
	int32_t i = -1;
	// если объект не нарушает целостность уник.индексов в массиве и объект уникальный, добавлю новый
	if (ii.IsUnic<THIS, Obj>(*this, o))
		return Add(o);
	// если объект идентичный по всем уник.индексам имеющемуся
	if ((i = ii.IsEqual()) >= 0)
		return Edit(o, fl::ioffset, i, true);
	// 'o' нельзя добавить в массив, он нарушет целостность уник.индексов
	return -1;
/*	ASSERT(&o != static_obj(nullptr));
	COpener<THIS> opnr(*this);
	int32_t i = CheckExistence<THIS, INDEX, Obj, nInds>(*this, o);
	if (i == -1)
		return Add(o);
	if (i < 0)
		return i; // нарушение целостности уник.индекса (i * -1 - 1) или объект отличается не уник.индексом (i==INT_MIN)
	return Edit(o, ioffset, i, true);*/
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::Edit перезапишет объект в файле, переиндексирует объект по всем
/// индексам. Все эл-ты объекта, включая m_nref должны быть действительными.
/// \param o: файловый объект заменяется этим. Может быть недействительным. Меняется к-во ссылок, время.
/// \param iInd: индекс, по которому редактируется объект
/// \param iObj: индекс объекта в индексе iInd.
/// \param bIncrsResf: увеличить к-во ссылок на изменяемый объект
/// \return базовый индекс объекта в файле
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::Edit(Obj &o, int iInd, int iObj, bool bIncrsRef) // bIncrsRef = false
{
	ASSERT(iInd == idel || iInd == ioffset || iInd >= 0 && iInd < nInds);
	// блокирую доступ к массиву для аналогичных объектов на уровне ОС
	COpener<THIS> opnr(*this);
	ASSERT(!(m_nsz % szo));
	ASSERT(iObj >= 0 && (iInd == idel && iObj < m_ind.count(iInd) || iObj < m_ind.count()));
	// извлекаю редактируемый объект
	CFileItemObtainer<THIS, Obj> oa(this);
	Obj &old = oa.GetItem(iInd, iObj);
	// базовый индекс эл-та
	cint32_t iBas = (iInd == ioffset) ? iObj : m_ind.Bas(iInd, iObj);
	// файловый отступ эл-та
	cint64_t nOfst = iBas * szo;
	int32_t ai[nInds], i, nfs = -1, ndel;
//	cint32_t cnt = (int32_t) (m_nsz / szo);
	// редактируемый оригинал
	BYTES &ab = *io_buf();
/*	ab.SetCountND(szo);
	BAS::get(nOfst, ab, szo);
	old.SetBinData(ab, szo);*/
	ASSERT(!old.m_nref || old.IsValid());
	// переиндексация: все индексы изменяемого объекта
	for (i = 0; i < nInds; i++)
		ai[i] = (i == iInd) ? iObj : FindEqual(old, i, iBas);
	// если редактируется не действительный объект, извлекаю его ид-р из массива удалённых
	if (!old.IsValid()) {
		ASSERT(o.IsValid());
		cint32_t *const pi = (cint32_t*) m_ind.Index(idel, &ndel);
		i = glb::FindEqual(pi, ndel, iBas);
		ASSERT(i >= 0 && i < ndel);
		m_ind.RemNC(idel, i);
	}
#ifdef __DEBUG
	else if (o.IsValid()) {
		cint32_t *const pi = (cint32_t*) m_ind.Index(idel, &ndel);
		ASSERT(glb::FindEqual(pi, ndel, iBas) < 0);
	}
#endif
	// к-во может быть не известно, тогда заимствую его у старого, если оно есть (если старый действит.) Иначе 1.
	if (!o.m_nref && o.IsValid())
		o.m_nref = old.m_nref ? old.m_nref + bIncrsRef : 1;
	// время изменения и время создания
	o.m_tme = time(nullptr);
	o.m_tmc = (old.m_flags & ITM_DEL) ? o.m_tme : old.m_tmc;
	// изменю объект в файле...
	ab.SetCountZeroND();
	o.GetBinData(ab);
	ASSERT(szo == ab.GetCount());
	BAS::edit(nOfst, ab, szo);
	// переиндексация: исправлю все индексы изменённого эл-та
	for (i = 0; i < nInds; i++)
		Reindex(o, i, ai[i]);
	return iBas;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CvBase::EditUnic точная копия CvBase<Obj, nInds>::EditUnic()
/// заменит указанный индексами объект на новый, сохранив целостность уникальных индексов. Ф-ция
/// добавит новый объект в файл, если указано 'bSafe' и изменяемый объект отличается от имеющегося
/// и на него ссылаются больше одного раза.
/// Если объект 'o' уже есть в файле наряду с указанным индексами (например, изменяю строку "водка"
/// на строку "вискарь", но "вискарь" уже есть в файле), уменьшу к-во ссылок изменяемого объекта и
/// если оно обнуляется, то удалю его, а к-во ссылок найденного объекта увеличу и заменю его на но-
/// вый.
/// \param o: новый объект, вместо указанного индексами. Не может быть одним из статических. Если
/// этот объект уже есть в файле и он отличается от указанного индексами, уменьшу к-во ссылок на
/// объект в файле и увеличу к-во ссылок на найденный объект заменив его на 'o'.
/// \param iInd: индекс массива, по которому меняю объект (не может быть idel)
/// \param iObj: индекс объекта в 'iInd'
/// \param bSafe: если "трю" и на изменяемый в файле объект ссылаются более одного раза, он останется
/// без изменений за исключением уменьшения счётчика на 1. А новый объект 'o' будет добавлен в файл.
/// Если !bSafe, то объект в файле будет изменён несмотря на к-во ссылок.
/// \return базовый индекс изменённого эл-та
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::EditUnic(Obj &oNew, int iInd, int iObj, bool bSafe) // bSafe = true
{
	ASSERT(nInds >= 0 && nInds < 100 && iInd >= fl::ioffset && iInd < nInds);
	COpener<THIS> opnr(*this);
	int32_t i = ii.CanEdit<THIS, Obj>(*this, oNew);
	// если 'o' - это изменнёный или идентичный указанному в файле...
	if (i == BaseIndex(iInd, iObj))
		return Edit(oNew, iInd, iObj);
	// если 'o' уникальный объект, им можно заменить имеющийся или добавить новый
	if (ii.IsUnic()) {
		if (bSafe) {// только если на изменяемый объект не ссылаются другие
			CFileItemObtainer<THIS, Obj> oi(this);
			Obj &o = oi.GetItem(iInd, iObj);
			if (o.m_nref > 1) {
				i = Add(oNew); // o.m_nref = 1; возвращаю i - базовый ид-р 'o'
				o.m_nref--;
				Edit(o, iInd, iObj);
			} else // 1 или 0 если удалёный
				i = Edit(oNew, iInd, iObj);
		} else { // иначе меняю несмотря на ссылки (force edit)
			const_cast<Obj&>(oNew).m_nref = 0; // к-во ссылок будет как в файле см. Edit()
			i = Edit(oNew, iInd, iObj);
		}
		return i;
	}// <0? объект нельзя ни добавить в файл ни изменить имеющийся т.к. он нарушает целостность уник.индексов
	if (i < 0)
		return i;
	// единственный объект в файле, который возможно заменить на 'o' индексируется 'i', но этот объект не
	// указан аргументом iObj. Почему? Вероятно, объект 'o' сильно изенился и теперь похож на другой. Значит,
	// к-во ссылок указанного индексами объекта должно быть уменьшено. Если обнуляется, объект становится не
	// действительным.
	Invalidate(iInd, iObj);
	// К-во ссылок найденного объекта увеличивается на 1 и объект в файле заменяется новым. Это спорный момент
	// если индексов много. Если в массиве уник.индекс единственный, то возможно, а если нет? Спорно.
	return Edit(oNew, fl::ioffset, i, true);
/*	COpener<THIS> opnr(*this);// блокирую доступ к массиву для аналогичных объектов на уровне ОС
	int32_t i = CheckExistence<THIS, INDEX, Obj, nInds>(*this, o);
	if (i == -1) {// 'o' уникальный объект, или массив без уник.индексов. Можно менять файловый исходник.
		if (bSafe) {// только если на изменяемый объект не ссылаются другие (safe)
			Obj *po = &GetItem(iInd, iObj, *static_obj(nullptr));
			if (po->m_nref > 1) {
				i = Add(o); // o.m_nref = 1; i - базовый ид-р объекта возвращается
				po->m_nref--;
				Edit(*po, iInd, iObj);
			} else // 1 или 0, как у удалёного объекта
				i = Edit(o, iInd, iObj);
		} else { // иначе меняю несмотря на ссылки (force)
			ASSERT(!o.m_nref); // к-во ссылок остаётся прежним, как в файле
			i = Edit(o, iInd, iObj);
		}
		return i;
	} else if (i == BaseIndex(iInd, iObj)) // объект на изменяемый
		return Edit(o, iInd, iObj);
	else if (i == INT_MIN)
		return i;// это значит объект в файле похож всеми уник.индексами, но отличается не уникальным индексом
	else if (i < 0)
		return i;// объект 'o' отличается от имеющегося в файле уник.индексом (i * -1 - 1)
	// такой объект есть в файле и этот объект не указанный аргументами iInd, iObj. Значит, к-во
	// ссылок указанного индексами объекта должно быть уменьшено. Если обнуляется, объект будет удалён.
	Invalidate(iInd, iObj);
	// К-во ссылок найденного объекта увеличивается на 1 и объект в файле заменяется новым.
	Obj *po = &GetItem(fl::ioffset, i, *static_obj(nullptr));
	const_cast<Obj*>(&o)->m_nref = po->m_nref + 1;
	return Edit(o, fl::ioffset, i); */// увеличиваю счётчик ссылок нового объекта
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::Rem дорогостоящая операция. Вырывает из массива эл-т с корнем. Не
/// рекомендуемый подход, желательно использовать Invalidate().
/// \param iInd: индекс, в этом массиве
/// \param iAt: индекс в iInd
/// \return базовый индекс удалённого эл-та
///
template<class Obj, size_t szo, int nInds>
int CiShrd<Obj, szo, nInds>::Rem(const int iInd, const int iAt)
{
	COpener<THIS> opnr(*this);
	ASSERT(!(m_nsz % szo));
	ASSERT(iAt >= 0 && iAt < m_ind.count());
	// декларацыи, иницыацыи...
	int32_t i, iBas = (iInd == ioffset) ? iAt : m_ind.Bas(iInd, iAt);
	const int64_t nOffset = iBas * szo;
	CFileItemObtainer<THIS, Obj> oi(this);
	Obj &o = oi.obj();//*static_obj(nullptr);
	// удаляемый объект
	BYTES &ab = *io_buf();
	ab.SetCountND(szo);
	BAS::get(nOffset, ab, szo);
	o.SetBinData(ab, szo);
	// из всех индексов...
	for (i = ibanal; i < nInds; i++)
		m_ind.Rem(i, (i == iInd) ? iAt : FindEqual(o, i, iBas));
	if (!o.IsValid()) {
		const int32_t cnt = 0, *pi = (int32_t*) m_ind.Index(idel, const_cast<int32_t*>(&cnt));
		m_ind.Rem(idel, (iInd == idel) ? iAt : glb::FindEqual(pi, cnt, iBas));
	}// из основного файла...
	BAS::remove(nOffset, szo); // физически, из основного файла
	return iBas;
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::Invalidate отмечает эл-т как не действительный, помещает базовый индекс
/// эл-та в массив недействительных эл-тов.
/// \param iInd: индекс для доступа к эл-ту
/// \param iObj: индекс эл-та в iInd
/// \param bRmFrmPrnt: параметр для ф-ции Obj::Invalidate() передаётся объекту
/// \return базовый индекс не действительного эл-та
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::Invalidate(int32_t iInd, int32_t iObj, bool bRmFrmPrnt/* = false*/)
{
	ASSERT(iInd >= ioffset && iInd < nInds);
	COpener<THIS> opnr(*this);
	ASSERT(!(m_nsz % szo));
	ASSERT(iObj >= 0 && iObj < m_ind.count());
	int32_t iBas = (iInd == ioffset) ? iObj : m_ind.Bas(iInd, iObj), cnt;
	thread_local static Obj o;// статич.объект из массива тут не проканает т.к. используется в Edit, Find...
	GetItem(iInd, iObj, o);
	if (!o.IsValid())
		return iBas;
	if (!o.Invalidate(iObj, bRmFrmPrnt)) {// его базовый индекс добавляется в массив не действительных
		cint32_t *const pi = (int32_t*) m_ind.Index(idel, &cnt);
		ASSERT(!cnt || glb::FindEqual(pi, cnt, iBas) < 0);
		m_ind.SetInd(idel, !cnt ? 0 : glb::FindNearest(pi, cnt, iBas), iBas);
	}// перезапись объекта в основном файле. Возможна переиндексация, если есть индекс по флагам.
	return Edit(o, iInd, iObj);
}

template<class Obj, size_t szo, int nInds>
void CiShrd<Obj, szo, nInds>::Empty()
{
	COpener<THIS> opnr(*this);
	ASSERT(!(m_nsz % szo));
	m_ind.Empty();
	remove(0, m_nsz); // all
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::Count вернёт к-во эл-тов. Если файл не был открыт, или был изменён,
/// результат может быть не точным.
/// \return к-во эл-тов в файле
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::Count() const
{
	ASSERT(!(m_nsz % szo));
	COpener<THIS> opnr(const_cast<THIS&>(*this));
	ASSERT(m_nsz / szo == m_ind.count());
	return (int32_t) (m_nsz / szo);
}

template<class Obj, size_t szo, int nInds>
bool CiShrd<Obj, szo, nInds>::IsOpen() const
{
	return (m_lock.Locked() && BAS::IsOpen());
}

template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::BaseIndex(int32_t iInd, int32_t i) const
{
	return (iInd == ioffset) ? i : m_ind.Bas(iInd, i);
}

template<class Obj, size_t szo, int nInds>
inline bool CiShrd<Obj, szo, nInds>::IsValid(int32_t iind, int32_t iitm) const
{
	int32_t cnt;
	const int32_t *const pi = (int32_t*) m_ind.Index(idel, &cnt);
	if (iind != ioffset)
		iitm = BaseIndex(iind, iitm);
	return (!cnt || glb::FindEqual(pi, cnt, iitm) < 0);
}

/*template<class Obj, size_t szo, int nInds>
inline bool CiShrd<Obj, szo, nInds>::IsStatic(const Obj *po) const
{
	Obj *p1, *p2 = static_obj(&p1);
	return (po == p1 || po == p2);
}*/

template<class Obj, size_t szo, int nInds>
BYTES* CiShrd<Obj, szo, nInds>::io_buf()
{
	static thread_local BYTES *pb;
	if (!pb) {
		static thread_local BYTES ab;
		pb = &ab;
	}
	return pb;
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::static_obj в целях оптимизации массив объявляет статический массив указателй
/// на объекты
/// \return указатель на объект массива
///
template<class Obj, size_t szo, int nInds>
Obj* CiShrd<Obj, szo, nInds>::static_obj()
{
	thread_local static glb::CFxArray<Obj*, true> ao;
	int32_t i, cnt;
	Obj **ppo = ao.data(cnt), *po;
	for (i = 0; i < cnt; i++, ppo++)
		if (!(*ppo)->Used())
			return *ppo;
	ao.Add(po = new Obj);
	return po;
/*	static thread_local Obj *p1, *p2;
	if (!p1 && !p2) {
		//static thread_local Obj o1, o2;
		// в куче т.к. в CIntFileItem перегружен оператор взятия адреса '&'. Эти указатели можно
		// бросить, а можно освободить. См. glb::DeleteStaticPointers
		p1 = new Obj;// &o1;
		p2 = new Obj;// &o2;
	}
	if (ppo)
		*ppo = p2;
	return p1;*/
}

////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd<Obj, szo, nInds>::GetItem возвращает эл-т по идентификатору. Если
/// запрашиваемого эл-та нет в памяти, ф-ция получит его из файла.
/// \param iInd: индекс, по которому извлекаю объект
/// \param i: базовый индекс/идентификатор извлекаемого объекта
/// \return извлекаемый объект
///
template<class Obj, size_t szo, int nInds>
const Obj& CiShrd<Obj, szo, nInds>::GetItem(int iInd, int i, Obj &o) const
{
	ASSERT(iInd == ioffset || iInd >= 0 && iInd < nInds);
	COpener<THIS> opnr(*const_cast<THIS*>(this));
	ASSERT(!(m_nsz % szo));
	ASSERT(i >= 0 && i < m_ind.count(iInd));
	ASSERT(m_ind.count(iInd) == (int) (m_nsz / szo));
	cint32_t iBas = (iInd == ioffset) ? i : m_ind.Bas(iInd, i);
	cint64_t nOfst = iBas * szo;
	BYTES &ab = *io_buf();
	ab.SetCountND(szo);
	BAS::get(nOfst, ab, szo);
	o.SetBinData(ab, szo);
	return o;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::GetItem извлекаю 1 эл-т из массива
/// \param iInd: может быть ioffset, idel или пользовательским
/// \param iObj: индекс эл-та в индексе iInd
/// \param o:
/// \return ссылку на 'o'
///
template<class Obj, size_t szo, int nInds>
Obj& CiShrd<Obj, szo, nInds>::GetItem(int iInd, int i, Obj &o)
{
	ASSERT(iInd == ioffset || iInd == idel || iInd >= 0 && iInd < nInds);
	COpener<THIS> opnr(*this);
	ASSERT(!(m_nsz % szo));
	ASSERT(i >= 0 && i < m_ind.count((iInd == ioffset) ? ibanal : iInd));
	ASSERT(m_ind.count() == (int) (m_nsz / szo));
	cint32_t iBas = (iInd == ioffset) ? i : m_ind.Bas(iInd, i);
	cint64_t nOfst = iBas * szo;
	BYTES &ab = *io_buf();
	ab.SetCountND(szo);
	BAS::get(nOfst, ab, szo);
	o.SetBinData(ab, szo);
	return o;
}

template<class Obj, size_t szo, int nInds>
const i::CIndIShrd<Obj,nInds>* CiShrd<Obj, szo, nInds>::GetIndex() const
{
	return &m_ind;
}

template<class Obj, size_t szo, int nInds>
i::CIndIShrd<Obj,nInds>* CiShrd<Obj, szo, nInds>::GetIndex()
{
	return &m_ind;
}

/*template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::UnicIndex() const
{
	return inone; // если есть уник.индексы, производному преопределить
}*/

template<class Obj, size_t szo, int nInds>
const int32_t* CiShrd<Obj, szo, nInds>::UnicIndices(int32_t *pcnt) const
{
	if (pcnt)
		*pcnt = 0;
	return nullptr;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief func CiShrd::FindEqual находит 'o'
/// \param o: искомый объект. Может быть искуственным, т.е. содержать только нужные для ф-ции
/// сравнения сведения. Может быть удалённым (не действительным).
/// \param iInd: индекс, по которому ищу 'o'
/// \param iStart: индекс первого эл-та диапазона поиска, включительно
/// \param iEnd: индекс последнего эл-та диапазона поиска, включительно
/// \return индекс эл-та в индексе iInd. Если индекс не уникальный, вернёт индекс какого-то из эл-тов.
/// Если такого объекта в массиве нет, вернёт -1.
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::FindEqual(const Obj &o, int32_t iInd, int32_t iBas,	// iBas = INV32_BIND
										   int32_t iStart, int32_t iEnd) const			// iStart = -1, iEnd = -1
{
	CFileItemObtainer<THIS, Obj> oi(const_cast<THIS&>(*this));
	Obj &otmp = oi.obj();
//	static_obj(&po);// второй статич.объект
	ASSERT(&o != &otmp);
	COpener<THIS> opnr(const_cast<THIS&>(*this));
	int cnt = m_ind.count(iInd);
	int i, r,
		a = MAX(0, iStart),
	    z = (iEnd < 0 || iEnd >= cnt) ? cnt - 1 : iEnd;
	for (i = a + (z - a) / 2; a <= z; i = a + (z - a) / 2) {
		r = m_ind.cmp(iInd, o, GetItem(iInd, i, otmp));
		if (r < 0)
			z = i - 1;
		else if (r > 0)
			a = i + 1;
		else {	// если есть базовый ид-р, найду его среди равных объектов в не уникальном индексе
			if (iBas >= 0 && iBas != m_ind.Bas(iInd, i)) {
				ASSERT(iBas < m_ind.count(iInd));
				int32_t iBk, iFd;
				for (iBk = i - 1, iFd = i + 1; iBk >= a || iFd <= z; iBk--, iFd++) {
					if (iBk >= a)
						if (m_ind.cmp(iInd, o, GetItem(iInd, iBk, otmp)) != 0)
							iBk = a;
						else if (iBas == m_ind.Bas(iInd, iBk)) {
							i = iBk;
							break; // for
						}
					if (iFd <= z)
						if (m_ind.cmp(iInd, o, GetItem(iInd, iFd, otmp)) != 0)
							iFd = z;
						else if (iBas == m_ind.Bas(iInd, iFd)) {
							i = iFd;
							break;
						}
				}
				ASSERT(iBas == m_ind.Bas(iInd, i));// это хуёвая ошибка, если iBas правильный
			}// assert целостность (не уникального) индекса
			ASSERT(i == cnt - 1 || m_ind.cmp(iInd, o, GetItem(iInd, i + 1, otmp)) <= 0);
			ASSERT(!i || m_ind.cmp(iInd, o, GetItem(iInd, i - 1, otmp)) >= 0);
			return i;
		}
	}
	return -1;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief func CiShrd::FindNearest находит индекс (в индексе iInd в соответствии с ф-цией сравнения
/// m_fcmp) равного или следующего объекта. Результат может быть равен к-ву эл-ов
/// \param o: искомый объект может содержать только сведения, нужные в ф-ции сравнения m_fcmp. Может
/// быть недействительным (удалённым).
/// \param iInd: индекс, по которому ищу 'o'
/// \param iStart: индекс начала диапазона поиска, или -1, если искать нужно с первого эл-та
/// \param iEnd: индекс последнего эл-та включительно
/// \return индекс искомого эл-та, если такой имеется, или индекс первого большего эл-та
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::FindNearest(const Obj &o, int32_t iInd, int32_t iStart, int32_t iEnd) const
{
	CFileItemObtainer<THIS, Obj> oi(const_cast<THIS&>(*this));
	Obj &otmp = oi.obj();
//	static_obj(&po);
	ASSERT(&o != &otmp);
	COpener<THIS> opnr(const_cast<THIS&>(*this));
	int cnt = m_ind.count(iInd);
	int i, r,
		a = MAX(0, iStart),
		z = !cnt ? 0 : (iEnd < 0 || iEnd >= cnt) ? cnt - 1 : iEnd;
	for (i = a + (z - a) / 2; a < z; i = a + (z - a) / 2) {
		r = m_ind.cmp(iInd, o, GetItem(iInd, i, otmp));
		if (r < 0)
			z = MAX(a, i - 1);
		else if (r > 0)
			a = MIN(z, i + 1);
		else
			break;
	}
	if (z == a) {// нет идентичного эл-та
		ASSERT(i >= 0 && i <= cnt);	// i == min(a, z)
		if (i < cnt && m_ind.cmp(iInd, o, GetItem(iInd, i, otmp)) > 0)
			i++;
	}// assert целостность (не уникального) индекса
	ASSERT(i <= cnt && (i == cnt || m_ind.cmp(iInd, o, GetItem(iInd, i, otmp)) <= 0));
	ASSERT(!i || m_ind.cmp(iInd, o, GetItem(iInd, i - 1, otmp)) >= 0);
	return i;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CiShrd::FindValid находит действительный объект. В массиве могут быть недействительные объ-
/// екты, подлежащие удалению.
/// \param o: искомый объект, содержит только сведения, нужные для поиска
/// \param iInd: индекс, по которому нахожу объект
/// \param iStart: начало диапазона поиска, включительно
/// \param iEnd: последний эл-т поиска в индексе iInd, включительно
/// \return искомый индекс, или -1
///
template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::FindValid(const Obj &o, int32_t iInd, int32_t iStart, int32_t iEnd) const
{
	CFileItemObtainer<THIS, Obj> oi(const_cast<THIS&>(*this));
	Obj &otmp = oi.obj();
//	static_obj(&po);
	ASSERT(&o != &otmp);
	ASSERT(o.IsValid());
	COpener<THIS> opnr(const_cast<THIS&>(*this));
	int i = FindEqual(o, iInd, INV32_IND, iStart, iEnd), istart;
	const int cnt = m_ind.count(iInd);
	if (i < 0 || i >= cnt || GetItem(iInd, i, otmp).IsValid())// (*this)[i].IsValid())
		return i;// эл-т не найден, или найден действительный
	// i индексирует недействительный эл-т
	istart = i;
	while (++i < cnt) {
		const Obj &obj = GetItem(iInd, i, otmp);
		if (m_ind.cmp(iInd, o, obj) != 0)
			break;
		if (obj.IsValid())
			return i;
	}
	for (i = istart - 1; i >= 0; i--) {
		const Obj &obj = GetItem(iInd, i, otmp);
		if (m_ind.cmp(iInd, o, obj) != 0)
			return -1;
		if (obj.IsValid())
			break;
	}
	return i;
}

template<class Obj, size_t szo, int nInds>
int32_t CiShrd<Obj, szo, nInds>::Reindex(const Obj &o, int32_t iInd, int32_t iCur)
{
	ASSERT(m_lock.Locked() && BAS::IsOpen());
	ASSERT(iCur >= 0 && iCur < m_ind.count());
	const int32_t cnt = m_ind.count(iInd);
	int32_t i = iCur;
	CFileItemObtainer<THIS, Obj> oi(this);
	Obj &otmp = oi.obj();
//	static_obj(&po);
	ASSERT(&o != &otmp);
	// По завершении работы FindNearest, i указывает на элемент больший (следующий), или равный o, либо i равен
	// к-ву эл-ов массива. А предшествующий i эл-т меньше о. Т.е. о меньше или равен элементу i, если i < m_iCount.
	// Ситуация устроит, если элемент i сдвигается право. Двигать влево можно только предшествующий i эл-т. Поэтому
	// под первым if от возвращаемого FindNearest индекса отнимается 1.
	// Несмотря на то что от результата отнимается 1, конечный результат не может быть равен iCur т.к. o > iCur + 1
	if (cnt - iCur > 1 && m_ind.cmp(iInd, o, GetItem(iInd, iCur + 1, otmp)) > 0)
		i = FindNearest(o, iInd, iCur + 1, cnt - 1) - 1;
	else if (iCur > 0 && m_ind.cmp(iInd, o, GetItem(iInd, iCur - 1, otmp)) < 0)
		i = FindNearest(o, iInd, 0, iCur - 1);
	else
		return iCur; // 'o' не больше следующего и не меньше предыдущего эл-та, т.е. остаётся на прежнем месте
	ASSERT(i >= 0 && i < cnt && i != iCur);
	m_ind.Move(iInd, i, iCur);
#ifdef __DEBUG // объект должен точно стоять на своём месте, иначе нахуя всё это затевалось...
	Obj od;
	GetItem(iInd, i, od);
	ASSERT(!i || m_ind.cmp(iInd, od, GetItem(iInd, i - 1, otmp)) >= 0);
	ASSERT(cnt - i == 1 || m_ind.cmp(iInd, od, GetItem(iInd, i + 1, otmp)) <= 0);
#endif// defined __DEBUG
	return i;
}

}// namespace a
}// namespace fl
