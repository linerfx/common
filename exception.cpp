/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл exception.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * exception.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <cstdarg>
#include "exception.h"
#ifdef OACRL_LIBRARY
#include <ctime>
#endif// ifdef OACRL_LIBRARY
#include <QMessageBox>
#include "ErrLog.h"
#if defined(__ITOG) || defined(__STRFLA)
#include "io_strings.h"
namespace fl {
	extern CStrings strs;
}
#endif	// defined __ITOG | defined __STRFLA
namespace err {
	extern CLog log;

CInputException::CInputException(int id, int idExpected, int i, const char *pcszFormat, ...)
{
	m_id = id;
	m_idExpected = idExpected;
	m_ind = i;
	va_list vlist;
	va_start(vlist, pcszFormat);
	log.Add(pcszFormat, vlist);
	va_end(vlist);
}

#ifdef OACRL_LIBRARY
CCrlException::CCrlException(CURLcode code, const char *pcszFile, const int cnLine)
{
   m_code = code;
   log.Add(FRMTSTR_CURL_EXCEPTION_RISE, time(nullptr), code, pcszFile, cnLine);
}
#endif // defined OACRL_LIBRARY

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CCException::CCException
/// \param nCerrno: C errno
/// \param pcszFormat: строка формата, вероятно из fl::CStrings
///
CCException::CCException(int nCerrno, const char *pcszFormat, ...)
{	// файловый массив со строками открыт, или нихуя? Это исключение как раз и может быть
	// посвещено тому что невозможно окрыть файловый массив...
	m_errno = nCerrno;
	va_list vlist;
	va_start(vlist, pcszFormat);
	log.Add(pcszFormat, vlist);
	va_end(vlist);
}

#if defined(__ITOG) || defined(__STRFLA)
//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLocalException::CLocalException если иницыацыя прошла успешно, доступ к файлу со
/// строками имеется.
/// \param nStrErrorID: идентификатор (индекс строки из fl::CStrings) строки формата с ошибкой.
///
CLocalException::CLocalException(int iStrErrorID, ...)
{
	fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs);
	va_list vlist;
	va_start(vlist, iStrErrorID);
	log.Add(SZ(iStrErrorID, os.obj()), vlist);
	va_end(vlist);
	m_iStrID = iStrErrorID;
}

//#elif defined(__ITGCLNT)

// для iTgClnt конструктор местного ислючения в iTgClnt/global.cpp
// для ltts конструктор в ltts/global.cpp

#endif// defined(__ITOG) || defined(__STRFLA)

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLocalException::CLocalException если иницыацыя не прошла, швырни такое исключение.
/// Обозначает невозможность осуществления иницыацыи... должно долетать до main() и завершать
/// работу приложения.
/// \param pcsz: строка с текстом ошибки
///
CLocalException::CLocalException(const char *pcsz, ...)
{
	va_list vlist;
	va_start(vlist, pcsz);
	try {
		log.Add(pcsz, vlist);
	} catch (...) {
		va_end(vlist);
		throw;
	}
	va_end(vlist);
	m_iStrID = INV32_BIND;
}

} // namespace err
