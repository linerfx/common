#include "io_FileItem.h"
#include "iTgOpts.h"
/*
 * Copyright 2024 Николай Вамбольдт
 *
 * Файл io_FileItem.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2024 Nikolay Vamboldt
 *
 * io_FileItem.cpp is part of reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
namespace fl {

int CFileItem::GetBinData(BYTES &a) const
{
	a.AddND((CBYTE*) &m_tmc, SZ_VFI);
	return SZ_VFI;
}

int CFileItem::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	glb::read_raw((BYTE*) &m_tmc, SZ_VFI, cpb, cnt);
	return SZ_VFI;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::CBaseObj
///
CBaseObj::CBaseObj()
	: m_tp(ndef)
{
	m_iPrnt = m_iNm = m_iDn = m_iImg = INV32_BIND;
}

CBaseObj::CBaseObj(Type tp)
	: m_tp(tp)
{
	m_iPrnt = m_iNm = m_iDn = m_iImg = INV32_BIND;
}

CBaseObj::CBaseObj(const CBaseObj &o)
	: BAS(o)
	, m_tp(o.m_tp)
	, m_iNm(o.m_iNm)
	, m_iPrnt(o.m_iPrnt)
	, m_iDn(o.m_iDn)
	, m_iImg(o.m_iImg)
	, m_ai(o.m_ai)
{

}

CBaseObj::CBaseObj(CBaseObj &&o)
	: BAS(o)
	, m_tp(o.m_tp)
	, m_iNm(o.m_iNm)
	, m_iPrnt(o.m_iPrnt)
	, m_iDn(o.m_iDn)
	, m_iImg(o.m_iImg)
	, m_ai(o.m_ai)
{

}

int CBaseObj::GetBinData(BYTES &a) const
{
	const int cnt = a.GetCount();
	BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_tp, SZ_BSOBJ);
	m_ai.GetBinData(a);
	return (a.GetCount() - cnt);
}

int CBaseObj::SetBinData(CBYTE *cpb, int cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_tp, SZ_BSOBJ, cpb + i, cnt - i);
	return (i + m_ai.SetBinData(cpb + i, cnt - i));
}

void CBaseObj::Init()
{
	BAS::Init();
	m_iNm = m_iPrnt = m_iDn = m_iImg = INV32_BIND;
	m_ai.SetCountZeroND();
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseObj::AddSubitem добавит ид-р подэлемента в массив m_ai если его ещё там нет.
/// \param tp, id: тип объекта и его ид-р
///
void CBaseObj::AddSubitem(Type tp, int32_t id)
{
	const ID *pi = m_ai;
	for (int32_t i = 0, n = m_ai.GetCount(); i < n; i++, pi++)
		if (tp == pi->tp && id == pi->i)
			return;
		else if (tp <= pi->tp && id < pi->i) {
			m_ai.Insert(i, { tp, id });
			return;
		}
	m_ai.AddND({ tp, id });
}

}// namespace fl

#if defined __ITOG || defined __ITGCLNT

namespace cafe {

int CDept::SetBinData(CBYTE *cpb, int cnt)
{	// в CFxArrayInd нет ф-ции SetBinData(), добавлять эл-ты в индексированный массив нужно вручную
	int32_t i = fl::CBaseObj::SetBinData(cpb, cnt);
	glb::CFxArray<uint32_t, true> a;
	a.SetBinData(cpb + i, cnt - i);
	m_aCat.SetCountZeroND();
	for (int j = 0, n = a.GetCount(); j < n; j++)
		m_aCat.Add(a[j]);
	return i;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CDept::Dif
/// Этот объект записан в базу, в аргументе категории категории отдела, который изменяет этот.
/// см. CvShrdCat<O, nInds, t>::Edit()
/// Нужно найти категории,
///		отсуствующие в этом отделе, имеющиеся в новом	(добавляются)
///		имеющиеся в этом отделе, отсутствующие в новом	(удаляются)
/// \param auc
/// \return аргумент
///
void CDept::DifCat(o::CiTgOpts::AUCATS &aiuc,
				   glb::CFxArray<uint32_t, true> &aucAdd,
				   glb::CFxArray<uint32_t, true> &aucRem) const
{
	uint32_t uc;
	int i, n;
	aucAdd.SetCountZeroND();
	aucRem.SetCountZeroND();
	for (i = 0, n = aiuc.GetCount(); i < n; i++) {
		uc = aiuc.GetItem(0, i);
		// в этом объекте такой категории нет, её нужно добавить в базу
		if (m_aCat.FindEqual(uc, INV32_IND, 0) < 0)
			aucAdd.AddND(uc);
	}
	for (i = 0, n = m_aCat.GetCount(); i < n; i++) {
		uc = m_aCat.GetItem(0, i);
		// в новом объекте такой категории нет, её нужно удалить
		if (aiuc.FindEqual(uc, INV32_IND, 0) < 0)
			aucRem.AddND(uc);
	}
}

}// namespace cafe

#endif