#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgCafeObj.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgCafeObj.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "iTgStoreObj.h"
#include "io_FileItem.h"
#include "iTgOpts.h"
#include "FxVArray.h"

// флаг для заказа CBill - заказ закрыт
#define BF_CLOSED	0x00000002

namespace cafe {

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CCoworker class
///
#define SZ_CWRKR	(sizeof(int64_t) + sizeof(int32_t) * 12)

class CCoworker : public fl::CFileItem
{
public:
	// индексы массива сотрудников
	enum EIndices {	// быстрый уникальный индекс iCwrkINmPw
		iCwrkINmPw,		// ФИО и пароль по базовым индексам строк быстро - уникальный индекс
		iCwrkNmPw,		// ФИО и пароль по буквам медленно - уникальный индекс
		iCwrkNm,		// ФИО - не уникальный индекс
		iCwrkID,		// ид-р п-ля, такой как ИИН и ИНН - не уникальный индекс (не обязательный параметр)
		iCwrkTm,		// время принятия и увольнения - не уникальный индекс
		iSwrkStt,		// Флаги CCoworker::m_flags - не уникальный индекс
		iCwrkInds
	};
public:
	int64_t	m_tmBrn;	// дата рождения
	uint32_t m_nrts;	// полномочия rights IR_* из iTgDefs.h
	int32_t	m_iNm,		// ид-р строки с именем
			m_iMidNm,	// ид-р строки с отчеством
	        m_iSurNm,	// ид-р строки с фамилией.. фамилия - имя п-ля для входа с паролем
			m_iID,		// ид-р строки с ИИН/ИНН
			m_ipw,		// ид-р строки с паролем
			m_iPict,	// ид-р строки с именем файла с изображением
			m_iStrOt,	// ид-р строки с причиной увольнения сотрудника
			m_iNts,		// ид-р строки с именем текстового файла с заметками
			m_iAdrs,	// ид-р из таблицы почтовых адресов
			m_iPrnt,	// ид-р отдела из cafe::depts
			m_iPos;		// ид-р из таблицы должностей
	glb::CFxArray<int32_t, true>
			m_aiTel,	// ид-ры строк с номерами телефонов
			m_aiRef;	// ид-ры строк с э-адресами/ссылками
public:
	CCoworker();
	CCoworker(const CCoworker &o);
	CCoworker(CCoworker &&o);
	virtual ~CCoworker() {}
	virtual const CCoworker& operator =(const CCoworker &o);
	virtual CCoworker& operator =(CCoworker &&o);
public:// функцыонал:
	virtual int GetBinData(glb::CFxArray<BYTE, true> &a) const override;
	virtual int SetBinData(const BYTE *pb, int cnt)  override;
	virtual bool IsEmpty() const override;
	virtual bool IsValid() const override;
	virtual bool IsNull() const override;
#ifdef __ITOG
	virtual int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt = false) override;
#endif
	static size_t sz();
#ifdef __ITOG
public:// индексы
	typedef int(*fcmp)(const CCoworker&, const CCoworker&);
	static int cmp_nm(const CCoworker &a, const CCoworker &z);		// имя, отчество, фамилия
	static int cmp_nmpw(const CCoworker &a, const CCoworker &z);	// фамилия, пароль
	static int cmp_inmpw(const CCoworker &a, const CCoworker &z);	// фамилия, пароль - ид-ры, не строки
	static int cmp_id(const CCoworker &a, const CCoworker &z);		// ИИН/ИНН
	static int cmp_tm(const CCoworker &a, const CCoworker &z);		// время принятия и увольнения
	static int cmp_state(const CCoworker &a, const CCoworker &z);	// состояние - уволен/нет
#endif// defined __ITOG
};

inline CCoworker::CCoworker()
{
	m_nrts = 0;
	m_iNm = m_iPrnt = m_iPos = m_iMidNm = m_iSurNm = m_iID = m_ipw = m_iPict = m_iAdrs = m_iNts = m_iStrOt = INV32_BIND;
	m_tmBrn = 0;
}

inline CCoworker::CCoworker(const CCoworker &o)
	: fl::CFileItem(o)
	, m_nrts(o.m_nrts)
	, m_iNm(o.m_iNm)
	, m_iPrnt(o.m_iPrnt)
	, m_iPos(o.m_iPos)
	, m_iMidNm(o.m_iMidNm)
	, m_iSurNm(o.m_iSurNm)
	, m_iID(o.m_iID)
	, m_ipw(o.m_ipw)
	, m_iPict(o.m_iPict)
	, m_iAdrs(o.m_iAdrs)
	, m_iNts(o.m_iNm)
	, m_iStrOt(o.m_iStrOt)
	, m_aiTel(o.m_aiTel)
	, m_aiRef(o.m_aiRef)
{

}

inline CCoworker::CCoworker(CCoworker &&o)
	: fl::CFileItem(o)
	, m_nrts(o.m_nrts)
	, m_iNm(o.m_iNm)
	, m_iPrnt(o.m_iPrnt)
	, m_iPos(o.m_iPos)
	, m_iMidNm(o.m_iMidNm)
	, m_iSurNm(o.m_iSurNm)
	, m_iID(o.m_iID)
	, m_ipw(o.m_ipw)
	, m_iPict(o.m_iPict)
	, m_iAdrs(o.m_iAdrs)
	, m_iNts(o.m_iNm)
	, m_iStrOt(o.m_iStrOt)
	, m_aiTel(o.m_aiTel)
	, m_aiRef(o.m_aiRef)
{

}

inline const CCoworker& CCoworker::operator =(const CCoworker &o)
{
	fl::CFileItem::operator =(o);
	m_nrts = o.m_nrts;
	m_iNm = o.m_iNm;
	m_iPrnt = o.m_iPrnt;
	m_iPos = o.m_iPos;
	m_iMidNm = o.m_iMidNm;
	m_iSurNm = o.m_iSurNm;
	m_iID = o.m_iID;
	m_ipw = o.m_ipw;
	m_iPict = o.m_iPict;
	m_iAdrs = o.m_iAdrs;
	m_iNts = o.m_iNm;
	m_iStrOt = o.m_iStrOt;
	m_aiTel = o.m_aiTel;	// ид-ры номеров телефонов
	m_aiRef = o.m_aiRef;	// ид-ры строк с э-адресами/ссылками
	return *this;
}

inline CCoworker& CCoworker::operator =(CCoworker &&o)
{
	fl::CFileItem::operator =(o);
	m_nrts = o.m_nrts;
	m_iNm = o.m_iNm;
	m_iPrnt = o.m_iPrnt;
	m_iPos = o.m_iPos;
	m_iMidNm = o.m_iMidNm;
	m_iSurNm = o.m_iSurNm;
	m_iID = o.m_iID;
	m_ipw = o.m_ipw;
	m_iPict = o.m_iPict;
	m_iAdrs = o.m_iAdrs;
	m_iNts = o.m_iNm;
	m_iStrOt = o.m_iStrOt;
	m_aiTel.Attach(o.m_aiTel);	// ид-ры номеров телефонов
	m_aiRef.Attach(o.m_aiRef);	// ид-ры строк с э-адресами/ссылками
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CCoworker::IsEmpty сотрудник без имени подлежит удалению в файловом массиве
/// \return трю/хрю
///
inline bool CCoworker::IsEmpty() const
{	// девственно чистые 16 переменных
	return (m_iNm < 0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CCoworker::IsValid не действительный объект удалённый, а для добавления нового эл-та в фай-
/// ловый массив обязательно наличие наименования.
/// \return трю/хрю
///
inline bool CCoworker::IsValid() const
{
	return (m_iNm >= 0 && fl::CFileItem::IsValid());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CCoworker::IsNull отсутствующий объект совершенно новый
/// \return трю/хрю
///
inline bool CCoworker::IsNull() const
{
	return (!m_tmBrn && m_iPos == INV32_BIND && m_iNm == INV32_BIND && m_iMidNm == INV32_BIND &&
			m_iSurNm == INV32_BIND && m_iID == INV32_BIND && m_ipw == INV32_BIND && m_iPict == INV32_BIND &&
			m_iAdrs == INV32_BIND && m_iNts == INV32_BIND && m_iStrOt == INV32_BIND && !m_aiTel.GetCount() &&
			!m_aiRef.GetCount() && fl::CFileItem::IsNull());
}

inline size_t CCoworker::sz()
{
	return (fl::CFileItem::sz() + SZ_CWRKR);
}

#define SZ_CNTRCTR	(sizeof(int32_t) * 8 + sizeof(BYTE))
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CContractor class субъект на договоре - поставщик, клиент, сотрудник
/// Наследуют: CSupplier
///
class CContractor : public fl::CFileItem
{
public:
	typedef fl::CFileItem BAS;
	// индексы массива CContractors
	enum EIndices {
		iCntrITpNm,		// уник. индекс по ид-рам категории/типа кантрактника и наименования
		iCntrTpNm,		// уник. индекс по наименованиям категории и контрактника
		iCntrINm,		// не уник. быстрый индекс по ид-ру наименования
		iCntrNm,		// не уник. долгий индекс по наименованию
		iCntrBisID,
		iCntrBnkID,
		iCntrBnkNm,
		iCntrAcnt,
		iCntrPaddrs,
		iCntrIndices
	};
public:
	int32_t
		m_iNm,		// Ид-р строки с наименованием - обязательный параметр
		m_iDn,		// Ид-р строки с заметкой/пояснением/характеристикой
		m_iImg,		// Ид-р строки с именем файла с изображением
		m_iBisID,	// Ид-р строки БИН/ИИН
		m_iBnkID,	// Ид-р строки БИК
		m_iBnkNm,	// Ид-р строки с именем банка
		m_iAcnt,	// Ид-р строки ИИК/счёт
		m_iAggrt;	// Ид-р строки с именем договора
	BYTE m_bCntr,	// Тип контрактника (индекс в CiTgOpts::m_aiCnts) - обязательный параметр.
		m_abRes[sizeof(int64_t) - sizeof m_bCntr];// выравниваю до 64 бит только в памяти
	glb::CFxArray<int32_t, true>
		m_aiAdrs,	// массив Ид-ров почтовых адресов из cafe::adrss
		m_aiTel,	// массив Ид-ров строк- № телефона/мобилы
		m_aiRefs;	// массив Ид-ров строк со ссылками (соц.сети, сайт, эл.почта)
public:
	CContractor();
	CContractor(const CContractor &o);
	CContractor(CContractor &&o);
	virtual ~CContractor() {}
public:
	virtual void Init() override;
public:// функцыонал:
	virtual const CContractor& operator =(const CContractor &o);
	virtual CContractor& operator =(CContractor &&o);
	virtual int GetBinData(glb::CFxArray<BYTE, true> &a) const override;
	virtual int SetBinData(const BYTE *pb, int cnt)  override;
	virtual bool IsEmpty() const override;
	virtual bool IsValid() const override;
	virtual bool IsNull() const override;
#ifdef __ITOG
	virtual int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt = false) override;
#endif
	static size_t sz();
public:// индексы
	typedef int(*fcmp)(const CContractor&, const CContractor&);
	static int cmp_iTpNm(const CContractor &a, const CContractor &z);
	static int cmp_inm(const CContractor &a, const CContractor &z);		// ид-ры имён
#ifdef __ITOG
	static int cmp_tpNm(const CContractor &a, const CContractor &z);	// тип (категория) и имя
	static int cmp_nm(const CContractor &a, const CContractor &z);		// имена
	static int cmp_bis_id(const CContractor &a, const CContractor &z);	// БИН/ИИН/ИНН
	static int cmp_bnk_id(const CContractor &a, const CContractor &z);	// БИК
	static int cmp_bnk_nm(const CContractor &a, const CContractor &z);	// наим.банка
	static int cmp_bacnt(const CContractor &a, const CContractor &z);	// банк.счёт
	static int cmp_addrs(const CContractor &a, const CContractor &z);	// почтовый адрес
#endif
};

inline const CContractor& CContractor::operator =(const CContractor &o)
{
	fl::CFileItem::operator =(o);
	memcpy(&m_iNm, &o.m_iNm, SZ_CNTRCTR);
	m_aiTel = o.m_aiTel;
	m_aiRefs = o.m_aiRefs;
	m_aiAdrs = o.m_aiAdrs;
	return *this;
}

inline CContractor& CContractor::operator =(CContractor &&o)
{
	fl::CFileItem::operator =(o);
	memcpy(&m_iNm, &o.m_iNm, SZ_CNTRCTR);
	m_aiTel.Attach(o.m_aiTel);
	m_aiRefs.Attach(o.m_aiRefs);
	m_aiAdrs.Attach(o.m_aiAdrs);
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CContractor::IsEmpty пустой контрактник без наименования или типа подлежит удалению в базе
/// \return трю/хрю
///
inline bool CContractor::IsEmpty() const
{
	return (m_iNm < 0 || m_bCntr == 0xff);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CContractor::IsValid действительный объект в файле не отмеченный как удалённый, а
/// при добавлении нового э-та обязательно наличие наименования и типа контрактника.
/// \return
///
inline bool CContractor::IsValid() const
{
	return (m_iNm >= 0 && m_bCntr != 0xff && fl::CFileItem::IsValid());
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CContractor::IsNull
/// \return
///
inline bool CContractor::IsNull() const
{
	return (m_iNm == INV32_BIND && m_iBisID == INV32_BIND && m_iBnkID == INV32_BIND && m_iBnkNm == INV32_BIND &&
			m_iAcnt == INV32_BIND && m_iAggrt == INV32_BIND && m_iDn == INV32_BIND && m_iImg == INV32_BIND &&
			m_bCntr == 0xff && !m_aiRefs.GetCount() && !m_aiTel.GetCount() && fl::CFileItem::IsNull());
}

inline size_t CContractor::sz()
{
	return (fl::CFileItem::sz() + SZ_CNTRCTR);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CSupplier class поставщик наследует контрактника
///
class CSupplier : public CContractor
{
public:
	typedef CContractor BAS;
	enum ESplrInd {
		iSplrITpNm,		// уник. индекс по ид-рам категории/типа кантрактника и наименования
		iSplrTpNm,		// уник. индекс по наименованиям категории и контрактника
		iSplrINm,		// не уник. быстрый индекс по ид-ру наименования
		iSplrNm,		// не уник. долгий индекс по наименованию
		iSplrBisID,
		iSplrBnkID,
		iSplrBnkNm,
		iSplrAcnt,
		iSplrPaddrs,
		iSplrInds
	};
public:
	/* массив концевых категорий товаров поставляемых этим поставщиком в отличие от аналогичного массива
	 * категорий в настройках содержит только концевые категории */
	o::CiTgOpts::fCmpUCat m_afcc[1] = { o::CiTgOpts::CCat::cmpc };
	o::CiTgOpts::AUCATS m_aCat; // допустимые категории товаров в приходе/расходе
public:
	CSupplier() : m_aCat(16, m_afcc) {}
	virtual ~CSupplier() {}
	virtual void Init() override;
	virtual int GetBinData(BYTES &a) const override;
	virtual int SetBinData(CBYTE *cpb, cint32_t cnt) override;
public:// индексы
	typedef int(*fcmp)(const CSupplier&, const CSupplier&);
	static int cmp_iTpNm(const CSupplier &a, const CSupplier &z);
	static int cmp_inm(const CSupplier &a, const CSupplier &z);		// ид-ры имён
#ifdef __ITOG
	static int cmp_tpNm(const CSupplier &a, const CSupplier &z);	// тип (категория) и имя
	static int cmp_nm(const CSupplier &a, const CSupplier &z);		// имена
	static int cmp_bis_id(const CSupplier &a, const CSupplier &z);	// БИН/ИИН/ИНН
	static int cmp_bnk_id(const CSupplier &a, const CSupplier &z);	// БИК
	static int cmp_bnk_nm(const CSupplier &a, const CSupplier &z);	// наим.банка
	static int cmp_bacnt(const CSupplier &a, const CSupplier &z);	// банк.счёт
	static int cmp_addrs(const CSupplier &a, const CSupplier &z);	// почтовый адрес
#endif
};

inline void CSupplier::Init()
{
	BAS::Init();
	m_aCat.SetCountZeroND();
}

inline int CSupplier::GetBinData(BYTES &a) const
{
	int n = BAS::GetBinData(a);
	return n + m_aCat.GetBinData(a);
}

/*inline fl::CBaseObj::Type CSupplier::Type() const
{
	return fl::CBaseObj::splr;
}*/

inline int CSupplier::cmp_iTpNm(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_iTpNm(a, z);
}

inline int CSupplier::cmp_inm(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_inm(a, z);
}

#ifdef __ITOG

inline int CSupplier::cmp_tpNm(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_tpNm(a, z);
}

inline int CSupplier::cmp_nm(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_nm(a, z);
}

inline int CSupplier::cmp_bis_id(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_bis_id(a, z);
}

inline int CSupplier::cmp_bnk_id(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_bnk_id(a, z);
}

inline int CSupplier::cmp_bnk_nm(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_bnk_nm(a, z);
}

inline int CSupplier::cmp_bacnt(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_bacnt(a, z);
}

inline int CSupplier::cmp_addrs(const CSupplier &a, const CSupplier &z)
{
	return CContractor::cmp_addrs(a, z);
}

#endif

#define SZ_BILL	(sizeof(int32_t) * 4)

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CBill class
///
class CBill : public fl::CFileItem
{
public:
	// индексы массива CBills
	enum EIndices {
		iBillIWtrTm,	// уник.индекс по ид-рам официанта и времени
		iBillICshrTm,	// по ид-рам кассира и времени
		iBillIDeptTm,	// по ид-рам отдела и времени
		iBillTm,		// по времени
		iBillCshrTm,	// по именам кассира и времени
		iBillDeptTm,	// по имена отдела и времени
		iBillInds
	};
public:
/*	struct CPrdct {
		double m_fQtt;		// к-во
		uint32_t m_iPrdct;	// ид-р продукта
		glb::CFxArray<CPrdct, false> m_aAdds;// доп.продукты
		// ф-ции массива
		CPrdct() : m_fQtt(0.0), m_iPrdct(INV32_BIND) {}
		CPrdct(const CPrdct &o);
		CPrdct(CPrdct &&o);
		const CPrdct& operator =(const CPrdct &o);
		CPrdct& operator =(CPrdct &&o);
		int32_t GetBinData(BYTES ab);
		int32_t SetBinData(CBYTE *cpb, cint32_t cnt);
	};*/

public:
	int32_t	m_iCashier,	// ид-р сотрудника-кассира
			m_iDept,	// ид-р отдела (касса)
			m_iCwr;		// ид-р сотрудника, кто открыл/закрыл счёт (обсл.персона)
	BYTE	m_bPos,		// ид-р позиции в зале
			m_bTable,	// ид-р стола клиента
			m_bSeat,	// ид-р места клиента
			m_abr;		// выравнивание до 64 бит
	glb::CvArray<store::CPrdctQtt>
			m_aPrdcts;	// продукты в расход
public:
	CBill();
	CBill(const CBill &o);
	CBill(CBill &&o);
	virtual ~CBill() {}
public:// функцыонал:
	virtual const CBill& operator =(const CBill &o);
	virtual CBill& operator =(CBill &&o);
	virtual int32_t GetBinData(BYTES &a) const override;
	virtual int32_t SetBinData(CBYTE *cpb, cint32_t cnt)  override;
	virtual bool IsValid() const override;
	virtual bool IsEmpty() const override;
	virtual bool IsNull() const override;
	static size_t sz();
	double Sum() const;
public:// индексы
	typedef int(*fcmp)(const CBill&, const CBill&);
	static int cmp_iwtr_tm(const CBill &a, const CBill &z);		// уник.индекс быстро, по ид-рам официантов и времени
	static int cmp_icshr_tm(const CBill &a, const CBill &z);	// быстро по ид-рам кассиров и времени
	static int cmp_idept_tm(const CBill &a, const CBill &z);	// быстро, по ид-рам отделов и времени
	static int cmp_tm(const CBill &a, const CBill &z);			// быстро, только время
	// ещё 2 индекса в CBills
};

inline CBill::CBill()
{
	m_iCashier = m_iCwr = m_iDept = INV32_BIND;
	m_bPos = m_bTable = m_bSeat = 0;
}

inline CBill::CBill(const CBill &o)
	: fl::CFileItem(o)
	, m_aPrdcts(o.m_aPrdcts)
{
	m_iCashier = o.m_iCashier;
	m_iCwr = o.m_iCwr;
	m_iDept = o.m_iDept;
	m_bPos = o.m_bPos;
	m_bTable = o.m_bTable;
	m_bSeat = o.m_bSeat;
}

inline CBill::CBill(CBill &&o)
	: fl::CFileItem(o)
{
	m_aPrdcts.Attach(o.m_aPrdcts);
	m_iCashier = o.m_iCashier;
	m_iCwr = o.m_iCwr;
	m_iDept = o.m_iDept;
	m_bPos = o.m_bPos;
	m_bTable = o.m_bTable;
	m_bSeat = o.m_bSeat;
}

inline const CBill& CBill::operator =(const CBill &o)
{
	fl::CFileItem::operator =(o);
	m_iCashier = o.m_iCashier;
	m_iCwr = o.m_iCwr;
	m_iDept = o.m_iDept;
	m_bPos = o.m_bPos;
	m_bTable = o.m_bTable;
	m_bSeat = o.m_bSeat;
	m_aPrdcts = o.m_aPrdcts;
	return *this;
}

inline CBill& CBill::operator =(CBill &&o)
{
	fl::CFileItem::operator =(o);
	m_iCashier = o.m_iCashier;
	m_iCwr = o.m_iCwr;
	m_iDept = o.m_iDept;
	m_bPos = o.m_bPos;
	m_bTable = o.m_bTable;
	m_bSeat = o.m_bSeat;
	m_aPrdcts.Attach(o.m_aPrdcts);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBill::IsValid не действительный счёт удалённый. Для открытия счёта (записи его
/// в базу) достаточно наличие ид-ра сотрудника.
/// \return трю/хрю
///
inline bool CBill::IsValid() const
{
	return fl::CFileItem::IsValid();// && m_iCwr >= 0 && m_iDept >= 0 && m_iCashier >= 0);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBill::IsEmpty используется для автоматического удаления эл-тов базы. Например,
/// когда п-ль в форме ввода удаляет содержимое строки, в файле эта ранее записанная строка
/// удаляется, когда сервер получает пустую строку с действительным ид-ром.
/// Счёта автоматически не удаляется.
/// \return хрю
///
inline bool CBill::IsEmpty() const
{
	return (m_iDept < 0 || m_iCashier < 0 || m_iCwr < 0 || !m_aPrdcts.GetCount());
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBill::IsNull отсутствующий счёт совершенно новый
/// \return трю/хрю
///
inline bool CBill::IsNull() const
{
	return (m_iCashier < 0 && m_iDept < 0 && m_iCwr < 0 && !m_aPrdcts.GetCount() && fl::CFileItem::IsNull());
}

inline size_t CBill::sz()
{
	return (fl::CFileItem::sz() + SZ_BILL);
}

inline int CBill::cmp_tm(const CBill &a, const CBill &z)
{
	return ((a.m_tme < z.m_tme) ? -1 : (a.m_tme > z.m_tme) ? 1 : 0);
}

inline int CBill::cmp_icshr_tm(const CBill &a, const CBill &z)
{
	if (a.m_iCashier != z.m_iCashier)
		return a.m_iDept - z.m_iDept;
	return ((a.m_tme < z.m_tme) ? -1 : (a.m_tme > z.m_tme) ? 1 : 0);
}

inline int CBill::cmp_idept_tm(const CBill &a, const CBill &z)
{
	if (a.m_iDept != z.m_iDept)
		return a.m_iDept - z.m_iDept;
	return ((a.m_tme < z.m_tme) ? -1 : (a.m_tme > z.m_tme) ? 1 : 0);
}

inline int CBill::cmp_iwtr_tm(const CBill &a, const CBill &z)
{
	if (a.m_iCwr != z.m_iCwr)
		return a.m_iCwr - z.m_iCwr;
	return ((a.m_tme < z.m_tme) ? -1 : (a.m_tme > z.m_tme) ? 1 : 0);
}

/*inline CBill::CPrdct::CPrdct(const CPrdct &o)
	: m_fQtt(o.m_fQtt)
	, m_iPrdct(o.m_iPrdct)
	, m_aAdds(o.m_aAdds)
{

}

inline CBill::CPrdct::CPrdct(CPrdct &&o)
	: m_fQtt(o.m_fQtt)
	, m_iPrdct(o.m_iPrdct)
	, m_aAdds(o.m_aAdds)
{

}

inline const CBill::CPrdct& CBill::CPrdct::operator =(const CPrdct &o)
{
	m_fQtt = o.m_fQtt;
	m_iPrdct = o.m_iPrdct;
	m_aAdds = o.m_aAdds;
	return *this;
}

inline CBill::CPrdct& CBill::CPrdct::operator =(CPrdct &&o)
{
	m_fQtt = o.m_fQtt;
	m_iPrdct = o.m_iPrdct;
	m_aAdds.Attach(o.m_aAdds);
	return *this;
}*/

////////////////////////////////////////////////////////////////////////////////////
#define SZ_PADRS	(sizeof(int32_t) * 9)
class CPostAdrs : public fl::CFileItem
{
public:
	enum EIndices {
		iPostAdrsAll,
		iPostAdrsInds
	};
public:
	int32_t
		m_iCountry,		// ид-р строки с названием страны
		m_iRegion,		// ид-р строки с названием области
		m_iToun,		// ид-р строки с названием населённого пункта
		m_iDistr,		// ид-р строки с названием района
		m_iIndex,		// ид-р строки с индексом
		m_iStreet,		// ид-р строки с улицей
		m_iBuilding,	// ид-р строки с номером строения
		m_iBldgNm,		// ид-р строки с наименованием строения, как общяга, ЖК...
		m_iFlat,		// ид-р строки с номером квартиры/комнаты
		m_res;			// выравниваю до 64 бит
	CPostAdrs();
	CPostAdrs(const CPostAdrs &o);
	virtual ~CPostAdrs() {}
	virtual void Init() override;
	int32_t GetBinData(BYTES &a) const override;
	int SetBinData(CBYTE *cpb, cint32_t cnt) override;
	static size_t sz();		// размер объекта, записываемый в файл
	const CPostAdrs& operator =(const CPostAdrs &o);
	bool IsValid() const override;
	bool IsEmpty() const override;	// эл-т пустой, никогда не был действительным
	bool IsNull() const override;
#ifdef __ITOG
	int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt = false) override;
#endif
};

inline CPostAdrs::CPostAdrs()
{
	m_iCountry = m_iRegion = m_iToun = m_iDistr
		= m_iIndex = m_iStreet = m_iBuilding = m_iBldgNm = m_iFlat = INV32_BIND;
	m_res = 0;
}

inline CPostAdrs::CPostAdrs(const CPostAdrs &o)
	: fl::CFileItem(o)
{
	memcpy(&m_iCountry, &o.m_iCountry, SZ_PADRS);
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAdrs::operator =() копирует в этот объект содержимое массива
/// \param pb: массив байт с содержимым для этого объекта. Убедись, что в массиве не менее sz() байт
/// \return ук-ль на следующий объект в массиве
///
/*inline CBYTE* CPostAdrs::operator =(CBYTE *pb)
{
	memcpy(&m_iObj, fl::CFileItem::operator =(pb), SZ_PADRS);
	return (pb + SZ_VFI + SZ_PADRS);
}*/

inline bool CPostAdrs::IsValid() const
{
	return (fl::CFileItem::IsValid() && m_iCountry != INV32_BIND && m_iToun != INV32_BIND && m_iStreet != INV32_BIND);
}

inline const CPostAdrs& CPostAdrs::operator =(const CPostAdrs &o)
{
	fl::CFileItem::operator =(o);
	memcpy(&m_iCountry, &o.m_iCountry, SZ_PADRS);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAdrs::IsEmpty пустой объект подлежит удалению в файле, если есть его ид-р.
/// \return хрю
///
inline bool CPostAdrs::IsEmpty() const
{
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAdrs::IsNull отсутствующий эл-т совершенно новый
/// \return трю/хрю
///
inline bool CPostAdrs::IsNull() const
{
	return (fl::CFileItem::IsNull() && m_iCountry == INV32_BIND && m_iRegion == INV32_BIND &&
			m_iToun == INV32_BIND && m_iDistr == INV32_BIND && m_iIndex == INV32_BIND &&
			m_iStreet == INV32_BIND && m_iBuilding == INV32_BIND && m_iBldgNm == INV32_BIND && m_iFlat == INV32_BIND);
}

inline void CPostAdrs::Init()
{
	m_iCountry = m_iRegion = m_iToun = m_iDistr = m_iIndex = m_iStreet =
		m_iBuilding = m_iBldgNm = m_iFlat = INV32_BIND;
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAdrs::sz
/// \return сколько байт записываю в файл, читаю из файла
///
inline size_t CPostAdrs::sz()
{
	return (fl::CFileItem::sz() + SZ_PADRS);
}

#define SZ_JOB (sizeof(int32_t) * 3)
/////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CJob class должность одного сотрудника предприятия
///
class CJob : public fl::CFileItem
{
public:
	enum EIndices {
		iJbNm,
		iJbInds
	};
	int32_t m_iNm,		// наименование должности
			m_iDesc,	// описание
			m_iImg,		// ид-р имени файла рисунка
			m_nres;		// выравнивание до 64 бит
	CJob();
	CJob(const CJob &o);
	virtual ~CJob() {}
	int GetBinData(glb::CFxArray<BYTE, true> &a) const override;
	int SetBinData(CBYTE *cpb, const int32_t cnt) override;
	static size_t sz();		// размер объекта, записываемый в файл
//	CBYTE* operator =(CBYTE *pb) override;
	const CJob& operator =(const CJob &o);
	bool IsValid() const override;
	bool IsEmpty() const override;	// эл-т пустой
	bool IsNull() const override;	// эл-т пустой
#ifdef __ITOG
	int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt = false) override;
#endif
};

inline CJob::CJob()
{
	m_iNm = m_iDesc = m_iImg = INV32_BIND;
	m_nres = 0;
}

inline CJob::CJob(const CJob &o)
	: fl::CFileItem(o)
	, m_iNm(o.m_iNm)
	, m_iDesc(o.m_iDesc)
	, m_iImg(o.m_iImg)
{

}

inline size_t CJob::sz()
{
	return (fl::CFileItem::sz() + SZ_JOB);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CJob::operator = перед вызовом убедись, что размер массива 'cpb' не меньше CJob::sz()
/// \param pb
/// \return следующий байт за этим объектом
///
/*inline CBYTE* CJob::operator =(CBYTE *pb)
{
//	ASSERT((uint64_t) this == (uint64_t) &m_iNm);
	memcpy((void*) &m_iNm, fl::CFileItem::operator =(pb), SZ_JOB);
	return (pb + SZ_VFI + SZ_JOB);
}*/

inline const CJob& CJob::operator =(const CJob &o)
{
	fl::CFileItem::operator =(o);
	m_iNm = o.m_iNm;
	m_iDesc = o.m_iDesc;
	m_iImg = o.m_iImg;
	return *this;
}

inline bool CJob::IsValid() const
{
	return (m_iNm >= 0 && fl::CFileItem::IsValid());
}

inline bool CJob::IsEmpty() const
{
	return (m_iNm < 0);
}

inline bool CJob::IsNull() const
{
	return (m_iDesc == INV32_BIND && m_iNm == INV32_BIND && m_iImg == INV32_BIND && fl::CFileItem::IsNull());
}

}// namespace cafe
