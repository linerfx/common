#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_indexIntegrity.h — часть программ reader, linerfx, iTog,
 * iTogClient, StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_indexIntegrity.h is part of reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxArray.h"

namespace fl::a {

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIndexIntegrity class контролирует целостность уникальных индексов в индекси-
/// рованных (файловых) массивах. Используется если требуется полная проверка наличия или
/// отсутствия объекта в массиве при добавлении или изменении эл-та массива.
///
class CIndexIntegrity
{ /*
   * если после завершения ф-ции SearchUnic() к-во эл-тов в m_ai больше 1, значит объект вносит
   * в массив индексное переебение. Иначе, если к-во эл-тов в m_ai равно 1, а m_cnt и m_nui не равны,
   * значит объект безопасно изменит одно или более значений уник.индексов файлового объекта, ид-р
   * которого находится в m_ai. Если m_cnt и m_nui равны, значит значения уникальных индексов
   * обоих объектов идентичны. Если m_ai пустой, m_cnt нуль, а m_nui больше нуля, значит объект
   * отличается от имеющихся по всем уник.индексам - объект уникальный
   */
	glb::CFxArray<int32_t, true> m_ai;
	int32_t m_cnt, m_nui; // к-во эл-тов и к-во уник.индексов массива

public:
	explicit CIndexIntegrity();

private:
	int AddUnicID(int32_t id);

	template<class A, class O>
	void SearchUnic(const A &a, const O &o);

public:
	template<class A, class O>
	bool IsUnic(const A &a, const O &o);

	bool IsUnic() const;
	int32_t IsEqual() const;

	template<class A, class O>
	int32_t CanEdit(const A &a, const O &o);

	int32_t CanEdit() const;
};

inline CIndexIntegrity::CIndexIntegrity()
	: m_ai(8)
	, m_cnt(0)
	, m_nui(0)
{

}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief Search ищет объект в массиве по всем уник.индексам. Если по завершении ф-ции m_cnt и m_nui
/// не равны, значит в одном или более индексов нет соответствующего значения из 'o', т.е. 'o' частично
/// отличается от одного (m_ai.GetCount() == 1) или более эл-тов в массиве если к-во эл-тов m_ai больше 1,
/// значит объект 'o' вносит индексное переебение в массив - его нельзя ни добавить в 'a', ни изменить им
/// никакой из объектов в 'a' нельзя.
/// \param a, o: массив и объект
///
template<class A, class O>
inline void CIndexIntegrity::SearchUnic(const A &a, const O &o)
{
	int32_t i, iObj;
	const int32_t *pi = a.UnicIndices(&m_nui);
	m_ai.SetCountZeroND();
	for (i = m_cnt = 0; i < m_nui; i++) {
		if ((iObj = a.FindEqual(o, pi[i])) >= 0) {
			m_cnt++;
			if (AddUnicID(a.BaseIndex(pi[i], iObj)) > 1)
				break;// не приемлимое индексное перебение
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief IsUnic возможно ли добавить объект в массив без нарушения целостности уник.индексов?
/// \param a, o: массив и объект
/// \return "трю" массив не содержит уник.индексов или объект уникальный; "хрю" объект частично
/// или полностью имеется в массиве
///
template<class A, class O>
inline bool CIndexIntegrity::IsUnic(const A &a, const O &o)
{
	SearchUnic(a, o);
	return (!m_cnt && m_nui > 0 && !m_ai.GetCount());
}

inline bool CIndexIntegrity::IsUnic() const
{
	return (!m_cnt && m_nui > 0 && !m_ai.GetCount());
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief IsEqual идентичный по всем индексам указанному параметром
/// \return базовый ид-р объекта, идентичного указанному в параметре
///
inline int32_t CIndexIntegrity::IsEqual() const
{
	return ((m_cnt != m_nui || m_ai.GetCount() != 1) ? -1 : m_ai[0]);
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CanEdit какой файловый объект возможно заменить на указанный без нарушения
/// целостности уник.индексов? Если объект уникальный, его можно добавить в массив.
/// \param a, o: массив и объект
/// \return -1 если такой объект не найден; базовый ид-р объекта, который можно заменить
/// указанным без нарушения целостности индексов т.к. он одним или более значением уник.индексов
/// похож на новый
///
template<class A, class O>
inline int32_t CIndexIntegrity::CanEdit(const A &a, const O &o)
{
	SearchUnic(a, o);
	return ((m_ai.GetCount() == 1) ? m_ai[0] : -1);
}

inline int32_t CIndexIntegrity::CanEdit() const
{
	return ((m_ai.GetCount() == 1) ? m_ai[0] : -1);
}

}// namespace