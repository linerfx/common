#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgStoreObj.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgStoreObj.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_FileItem.h"
#include "iTgDefs.h"
#include <cfloat>

// clang предупреждает, что "этот класс содержит вирт.ф-ции, в него копировать (memcpy) нельзя в ф-ции
// CStaple::operator =(const CStaple &o)
#ifdef __llvm__
#pragma clang diagnostic ignored "-Wdynamic-class-memaccess"
#endif

#define SZ_STPL	(sizeof(int32_t) * 7 + sizeof(double))

namespace store {
////////////////////////////////////////////////////////////////////////////////////
/// \brief class CStaple - товар/складской элемент. Содержит вирт.ф-ции, записывать
/// только индивидуально, массивом нельзя.
///
class CStaple : public fl::CFileItem
{
public:
	typedef fl::CFileItem BAS;
	enum EIndices {
		iGdsInmTara,	// уник.индекс. Быстрое сравнение ид-ров наименования и ёмкости тары
		iGdsNmiTara,	// уник.индекс по наименованию без учёта регистра и ёмкости тары
		iGdsCat,		// не уник.индекс - быстрое сравнение категорий товаров
		iGdsNm,			// Не уник.индекс. Долгое сравнение неименований
		iGdsNmi,		// Не уник.индекс. Долгое сравнение неименований без учёта регистра
		iGdsCatNmi,		// Не уник.индекс. Долгое сравнение категорий и наименований без учёта регистра
		iGdsArt,		/* Не уник.индекс. Долгое сравнение артикулов (не уникальный индекс - наличие артикула не
						 * обязательное) */
		iGdsInds		// к-во индексов товаров
	};
public:
	double	m_fTara;	/* 8 б. ёмкость тары в указанной е.и. o::CiTgOpts::m_amu[m_iMu].
						 * Если указана ёмкость тары, товар штучный */
	int32_t	m_iNm,		// 4 б. ид-р строки из CStrings с наименованием товара (строка с флагом ITM_STPL)
			m_iArtl,	// 4 б. ид-р строки с артикулом
			m_iDn,		// 4 б. ид-р строки с описанием
			m_iImg,		// 4 б. ид-р строки с именем файла рисунка
			m_iMu,		// 4 б. ид-р е.и. из настроек
			m_nCal;		// 8 б. к-во калорий на 100 гр.
	uint32_t m_nc,		// 4 б. категория товара из common/iTgDefs.h
			m_res;		// выравнивание только в памяти, в файл не записывается см. SZ_STPL
public:
	CStaple();
	CStaple(const CStaple &o);
	virtual ~CStaple() {}
public: // ф-ции для (файлового) массива CIntFile (см. CFileItem)
//	virtual CBYTE* operator =(CBYTE *pb) override;
	virtual bool IsEmpty() const override;
	virtual bool IsValid() const override;
	virtual bool IsNull() const override;
#ifdef __ITOG
	virtual int32_t Invalidate(const int32_t iObj, bool bRmFrmPrnt = false) override;
#endif
	virtual void Init() override;
	static size_t sz();
	virtual int GetBinData(BYTES &a) const override;
	virtual int SetBinData(CBYTE *cpb, int cnt) override;
public:
	double CostOut(double fCostIn) const;

public:
	typedef int(*fcmp)(const CStaple&, const CStaple&);
	static int cmp_inmtara(const CStaple&, const CStaple&);	// быстрый уник. индекс по ид-ру имени и ёмкости тары
	static int cmp_cat(const CStaple&, const CStaple&);		// не уник.индекс - по категории товара
#ifdef __ITOG
	static int cmp_nmtarai(const CStaple&, const CStaple&);	// долгий уник. индекс - по имени и ёмкости тары
	static int cmp_art(const CStaple&, const CStaple&);		// не уник. индекс (вероятность невелика, тем не менее)
	static int cmp_catnmi(const CStaple&, const CStaple&);	// не уник. индекс (по категории и наименованию)
	static int cmp_nm(const CStaple&, const CStaple&);		// не уник. индекс
	static int cmp_nmi(const CStaple&, const CStaple&);		// не уник. индекс по наименованиям без учёта регистра
/*	static int cmp_splr(const CStaple&, const CStaple&);	/  не уник. индекс - по наименованию поставщика товара
															 * ф-ция определена в ../iTog/iTog/goods.cpp */
#endif// defined __ITOG
};

inline CStaple::CStaple()
{
	m_iNm = m_iArtl = m_iDn = m_iImg = m_iMu = INV32_BIND;
	m_nc = 0;
	m_nCal = 0;
	m_fTara = .0;
}

inline CStaple::CStaple(const CStaple &o)
	: BAS(o)
{
	memcpy(&m_fTara, &o.m_fTara, SZ_STPL);
}

/*inline CBYTE* CStaple::operator =(CBYTE *cpb) // override CFileItem::operator =(CBYTE*)
{
	memcpy(&m_fTara, fl::CFileItem::operator =(cpb), SZ_STPL);
	return (cpb + SZ_VFI + SZ_STPL);
}*/

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::sz
/// \return возвращает размер записываемых и читаемых (в/из файла) байт без учёта размера
/// указателей на вирт.ф-ции и т.п.
///
inline size_t CStaple::sz()
{
	return (BAS::sz() + SZ_STPL);
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::CostOut себестоимость товара за расходную е.и. Например, бутыль JW
/// стоит 700 у.е., ёмкость тары 0.7 л., а расходная е.и. 1 л. (1000 мл.)
/// Тогда стоимость в расходных е.и. будет 700 / 0.7 = 1000 за 1 л.
/// Или netto банки консервированных фруктов 325 гр. (е.и. 1 гр.) по 250 у.е. за банку.
/// Тогда 250 / 325 = 0,769230769 у.е. за 1 гр.
/// Тоже, если е.и. кг. (1000 гр.), netto банки 0,325, цена та же:
/// 250 / 0,325 = 769,23/кг.
/// Если товар штучный (к-во грамм товарной е.и. <=0), банка так же 325
/// \param fCostIn: цена товара последнего прихода за единицу
/// \return стоимость товара за одну расходную е.и.
///
inline double CStaple::CostOut(double fCostIn) const
{
	ASSERT(!DCMP_EQUAL(m_fTara, .0, DBL_EPSILON));
	return (fCostIn / m_fTara);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::IsValid новый не действительный эл-т нельзя записать в базу, но возможно
/// заменить действительный эл-т не действительным. Без наименования не может быть товара.
/// \return трю/хрю
///
inline bool CStaple::IsValid() const
{	// мин.требования: не удалённый, с ид-ром наименования. Надо бы добавить: е.и., тарой...
	return (m_iNm != INV32_BIND && BAS::IsValid());
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::IsNull отсутствующий эл-т совершенно новый
/// \return трю/хрю
///
inline bool CStaple::IsNull() const
{
	return (m_iNm < 0 && m_iArtl < 0 && m_iDn < 0 && m_iImg < 0 && m_iMu < 0 &&
			DCMP_LOE(m_fTara, .0, DBL_EPSILON) && BAS::IsNull());
}

inline void CStaple::Init()
{
	BAS::Init();
	m_fTara = .0;
	m_iNm = m_iArtl = m_iDn = m_iImg = m_iMu = INV32_BIND;
	m_nCal = 0;
	m_nc = 0;
	m_flags = ITM_STPL;
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::IsEmpty единственный обязательный параметр товара - наименование. Если его
/// нет, товар удаляется из файла.
/// \return трю/хрю
///
inline bool CStaple::IsEmpty() const
{
	return (m_iNm < 0);
}

// быстрое сравнение ид-ров наименования и ёмкости тары
inline int CStaple::cmp_inmtara(const CStaple &a, const CStaple &z)
{
	return ((a.m_iNm < z.m_iNm) ? -1 : (a.m_iNm > z.m_iNm) ? 1 :
				DCMP_LESS(a.m_fTara, z.m_fTara, .0001) ? -1 : DCMP_MORE(a.m_fTara, z.m_fTara, .0001) ? 1 : 0);
}

#define SZ_RESIDUE (sizeof(double) * 2 + sizeof(int32_t) * 3)
////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CResidue class остаток каждого товара вычисляется: весь приход от предыдущего остатка ми-
/// нус весь расход от предыдущего остатка, плюс предыдущий остаток.
/// Остаток каждого товара возможно получить по отделу, или безотносительно отдела. Так же, с учётом
/// внутренних перемещений, или без учёта. Т.е. только внешний приход/расход, или только внутренние
/// перемещения по отделам.
///
class CResidue : public fl::CFileItem
{
public:
	enum EIndices {
		iRsdDST,		// отдел, товар, время (уникальный индекс)
		iRsdTSD,		// время, товар, отдел (уникальный индекс)
		iRsdPrsn,		// сотрудник
		iRsdInds
	};
public:
	double	m_fRes,			// остаток
			m_fCorrect;		// корректировка остатка
	int32_t m_iStpl,		// ид-р товара
			m_iDept,		// ид-р отдела, в котором вычисляется остаток товара m_iStpl
			m_iPerson,		// ид-р сотрудника, внесшего корректировку остатка
			m_res;			// выравниваю до 64 бит
public:
	CResidue();
	CResidue(const CResidue &o);
	virtual ~CResidue() {}
	virtual bool IsValid() const override;
	virtual int GetBinData(glb::CFxArray<BYTE, true> &a) const override;
	virtual int SetBinData(CBYTE *pb, const int32_t cnt) override;
//	virtual CBYTE* operator =(CBYTE *pb) override;
	virtual bool IsEmpty() const override;
	virtual bool IsNull() const override;
	size_t sz();
	const CResidue& operator =(const CResidue &o);
#ifdef __ITOG
	// только для сервера в виду отсутствия у клиента базы. Реализовано в iTog/residue.cpp
	double set(int iStplID);
	typedef int(*fcmp)(const CResidue&, const CResidue&);
	static int cmpDST(const CResidue &a, const CResidue &z);	// отдел, товар и время (уникальный индекс)
	static int cmpTSD(const CResidue &a, const CResidue &z);	// время, товар и отдел (уникальный индекс)
	static int cmp_prsntm(const CResidue &a, const CResidue &z);// сотрудник и время (уникальный индекс)
#endif// defined __ITOG
};

inline CResidue::CResidue()
{
	m_iStpl = m_iDept = m_iPerson = INV32_BIND;
	m_fRes = m_fCorrect = 0.0;
	m_res = 0;
}

inline CResidue::CResidue(const CResidue &o)
	: fl::CFileItem(o)
{
//	ASSERT((uint64_t) this == (uint64_t) &m_fRes);
	memcpy(&m_fRes, &o, SZ_RESIDUE);
}

inline bool CResidue::IsValid() const
{
	return (fl::CFileItem::IsValid() && m_iStpl != INV32_BIND && m_iDept != INV32_BIND && m_tmc != 0
			&& (m_tme > m_tmc || DCMP_EQUAL(m_fCorrect, .0, DBL_EPSILON)));
}

/////////////////////////////////////////////////////////////////////////
/// \brief CResidue::operator =() копирую содержимое массива в этот объект
/// \param cpb: указатель на массив байт, который содержит этот объект. Размер
/// массива должен быть не меньше размера этого объекта (SZ_VFI + SZ_RESIDUE),
/// иначе результат не определён
/// \return ук-ль на следующий объект в массиве cpb
///
/*inline const BYTE* CResidue::operator =(CBYTE *cpb)
{
//	ASSERT((uint64_t) this == (uint64_t) &m_fRes);
	memcpy(&m_fRes, fl::CFileItem::operator =(cpb), SZ_RESIDUE);
	return (cpb + SZ_VFI + SZ_RESIDUE);
}*/

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CResidue::IsEmpty остаток без ид-ра товара или отдела подлежит удалению (отмечается как
/// не действительный)
/// \return трю/хрю
///
inline bool CResidue::IsEmpty() const
{
	return (m_iStpl < 0 || m_iDept < 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CResidue::IsNull отсутствующий объект совершенно новый
/// \return трю/хрю
///
inline bool CResidue::IsNull() const
{
	return (m_iStpl < 0 && m_iDept < 0 && m_iPerson < 0 &&
			DCMP_LOE(m_fRes, .0, DBL_EPSILON) && fl::CFileItem::IsNull());
}

inline size_t CResidue::sz()
{
	return (fl::CFileItem::sz() + SZ_RESIDUE);
}

// во время создания продукта ид-р ингредиента скорее всего отсутствует. Идентифицироват
// его возможно только по типу и ид-ру товара/продукта
struct Cid {
	fl::CBaseObj::Type tp;
	int32_t iSrc;
	Cid(fl::CBaseObj::Type t = fl::CBaseObj::ndef, int32_t i = INV32_BIND) { tp = t; iSrc = i; }
	bool Valid() const;
	bool operator <(const Cid &o) const;
	bool operator >(const Cid &o) const;
	bool operator ==(const Cid &o) const;
	bool operator !=(const Cid &o) const;
	bool operator <=(const Cid &o) const;
	bool operator >=(const Cid &o) const;
};

inline bool Cid::operator <(const Cid &o) const
{	// товар больше продукта, ниже в списке
	ASSERT(fl::CBaseObj::ndef < fl::CBaseObj::product && fl::CBaseObj::staple > fl::CBaseObj::product);
	return (tp < o.tp || tp == o.tp && iSrc < o.iSrc);
}

inline bool Cid::operator >(const Cid &o) const
{
	ASSERT(fl::CBaseObj::ndef < fl::CBaseObj::product && fl::CBaseObj::staple > fl::CBaseObj::product);
	return (tp > o.tp || tp == o.tp && iSrc > o.iSrc);
}

inline bool Cid::operator ==(const Cid &o) const
{
	ASSERT(fl::CBaseObj::ndef < fl::CBaseObj::product && fl::CBaseObj::staple > fl::CBaseObj::product);
	return (tp == o.tp && iSrc == o.iSrc);
}

inline bool Cid::operator !=(const Cid &o) const
{
	ASSERT(fl::CBaseObj::ndef < fl::CBaseObj::product && fl::CBaseObj::staple > fl::CBaseObj::product);
	return (tp != o.tp || iSrc != o.iSrc);
}

inline bool Cid::operator <=(const Cid &o) const
{
	return (*this < o || *this == o);
}

inline bool Cid::operator >=(const Cid &o) const
{
	return (*this > o || *this == o);
}

inline bool Cid::Valid() const
{
	return (tp != fl::CBaseObj::ndef && iSrc >= 0);
}

#define SZ_STPLEX (sizeof(double) + sizeof(float) * 2 + sizeof(int32_t) * 4 + sizeof(Cid))
///////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief class CIngredient ингредиент продукта может быть создан из товара или продукта
///
class CIngredient : public fl::CFileItem
{
public:
	typedef fl::CFileItem BAS;
	enum EIndices {
		iIngsNP,	// уникальный индекс - сравнение по ид-рам продукта и товара
		iIngsStpl,	// не уник.индекс - сравнение по товарам
		iIngsPrdct,	// не уник.индекс - сравнение по продуктам
		iIngsLoad,	// не уник.индекс - сравнение по наценке товара
		iIngsQtt,	// не уник.индекс - сравнение по к-ву товара
		iIngsInds
	};
public:
	double	m_fLoad;	// наценка на ингр-т %
	float	m_fQtt,		// к-во товара или продукта в его расходной е.и.
			m_fLoss;	// потери при обработке % влияет на выход продукта
	int32_t	m_iSource,	// ид-р товара из CGoods или продукта из store::CProducts в зависимости от флагов ITM_MNMS
			m_iPrdct,	// ид-р продукта из store::CProducts, частю которого является этот ингредиент
			m_iMu,		// индекс o::CiTgOpt::m_amu товара или продукта
			m_nRes;		// выравнивание до 64 бит
	Cid		m_idBas;	// тип и ид-р ингредиента, альтернативой которому является этот
						// ингр-т (должен быть флаг ITM_INGTALT)
public:
	CIngredient();
	CIngredient(const CIngredient &o);
	int GetBinData(BYTES &a) const override;
	int SetBinData(CBYTE *pb, int cnt) override;
	static size_t sz();
	const CIngredient& operator =(const CIngredient &o);
	bool IsValid() const override;
	bool IsEmpty() const override;
	double Out() const;
#ifdef __ITOG
	virtual int32_t Invalidate(const int32_t iObj = -1, bool bRmFrmPrnt = false) override;
#endif
public:// индексы
	typedef int (*cmp)(const CIngredient&, const CIngredient&);
	// уник.инекс: быстрое сравнение ид-ров товара и продукта
	static int32_t cmpips(const CIngredient &a, const CIngredient &z);
	// не уник.индекс: быстрое сравнение типов и ид-ров источ
	static int32_t cmpis(const CIngredient &a, const CIngredient &z);
	// не уник.индекс: быстрое сравнения ид-ров продукта
	static int32_t cmpip(const CIngredient &a, const CIngredient &z);
	// не уник.индекс: сравнение наценки товаров
	static int cmpld(const CIngredient &a, const CIngredient &z);
	// не уник.индекс: сравнение к-ва товаров
	static int cmpqtt(const CIngredient &a, const CIngredient &z);
};

inline CIngredient::CIngredient()
	: m_fLoad(.0)
	, m_fQtt(0.f)
	, m_fLoss(0.f)
	, m_iSource(0.f)
	, m_iPrdct(INV32_BIND)
	, m_iMu(INV32_BIND)
{

}

inline CIngredient::CIngredient(const CIngredient &o)
	: BAS(o)
	, m_fLoad(o.m_fLoad)
	, m_fQtt(o.m_fQtt)
	, m_fLoss(o.m_fLoss)
	, m_iSource(o.m_iSource)
	, m_iPrdct(o.m_iPrdct)
	, m_iMu(o.m_iMu)
	, m_idBas(o.m_idBas)
{

}

inline bool CIngredient::IsValid() const
{	// не удалённый и обязательный минимум - товар и продукт
	return (BAS::IsValid() && m_iSource != INV32_BIND && m_iPrdct != INV32_BIND && m_iMu != INV32_BIND
			&& ((m_flags & ITM_MNMS) == ITM_STPL || (m_flags & ITM_MNMS) == ITM_PRDCT));
}

inline bool CIngredient::IsEmpty() const
{
	return (!m_fLoad && !m_fQtt && !m_fLoss && m_iSource == INV32_BIND && fl::CFileItem::IsEmpty());
}

inline const CIngredient& CIngredient::operator =(const CIngredient &o)
{
	BAS::operator =(o);
	m_fLoad = o.m_fLoad;
	m_fQtt = o.m_fQtt;
	m_fLoss = o.m_fLoss;
	m_iSource = o.m_iSource;
	m_iPrdct = o.m_iPrdct;
	m_iMu = o.m_iMu;
	m_idBas = o.m_idBas;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::Out к-во (выход) с учётом потерь
///
inline double CIngredient::Out() const
{
	return (m_fQtt - m_fQtt * m_fLoss);
}

inline size_t CIngredient::sz()
{
	return (BAS::sz() + SZ_STPLEX);
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredients::cmpstpl сравнивает 2 ингредиента по ид-рам продукта и товара. Уникальный индекс.
/// \param a, z: сравниваемые объекты
/// \return <0|0|>0
///
inline int32_t CIngredient::cmpips(const CIngredient &a, const CIngredient &z)
{
	if (a.m_iPrdct != z.m_iPrdct)
		return (a.m_iPrdct - z.m_iPrdct);
	if ((a.m_flags & ITM_MNMS) != (z.m_flags & ITM_MNMS))
		return (a.m_flags & ITM_MNMS) < (z.m_flags & ITM_MNMS) ? -1 : 1;
	return (a.m_iSource - z.m_iSource);
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmpis не уникальный индекс по ид-рам товара
/// \param a, z
/// \return <0|0|>0
///
inline int32_t CIngredient::cmpis(const CIngredient &a, const CIngredient &z)
{
	if ((a.m_flags & ITM_MNMS) != (z.m_flags & ITM_MNMS))
		return (a.m_flags & ITM_MNMS) < (z.m_flags & ITM_MNMS) ? -1 : 1;
	return (a.m_iSource - z.m_iSource);
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmpip по ид-рам продукта
/// \param a, z
/// \return <0|0|>0
///
inline int32_t CIngredient::cmpip(const CIngredient &a, const CIngredient &z)
{
	return (a.m_iPrdct - z.m_iPrdct);
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmpld по наценке
/// \param a, z
/// \return -1|0|1
///
inline int CIngredient::cmpld(const CIngredient &a, const CIngredient &z)
{
	double fe = .0001;
	return (DCMP_MORE(a.m_fLoad, z.m_fLoad, fe) ? 1 : DCMP_LESS(a.m_fLoad, z.m_fLoad, fe) ? -1 : 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmpqtt по к-ву товара
/// \param a, z
/// \return -1|0|1
///
inline int CIngredient::cmpqtt(const CIngredient &a, const CIngredient &z)
{
	double fe = .0001;
	return (DCMP_MORE(a.m_fQtt, z.m_fQtt, fe) ? 1 : DCMP_LESS(a.m_fQtt, z.m_fQtt, fe) ? -1 : 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// размер статич. эл-тов объекта c объектом базового класса
#define SZ_PRODUCT (sizeof(double) + sizeof(float) + sizeof(int))
////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CProduct class продукт (эл-т подменю) содержит ингредиенты
///
class CProduct : public fl::CBaseObj
{
public:
	typedef fl::CBaseObj BAS;
	typedef fl::CBaseObj::ID IngID;	// ид-р ингредиента

	enum EPIndices {
		iPrdctINames,	// уник.индекс по ид-рам строк наименований продуктов
		iPrdctNames,	// уник.индекс по наименованиям
		iPrdctPrntNms,	// уник.индекс по наименованиям подменю
		iPrdctFlags,	// не уник.индекс по флагам объектов
		iPrdctLoad,		// не уник.индекс по наценке продуктов
		iPrdctCost,		// не уник.индекс по стоимости продуктов
		iPrdctPrice,	// не уник.индекс по цене продуктов
		iPrdctIds
	};

	struct CGen {		// рассчитывается (genarate) ф-цией Data()
		double fCost,	// себестоимость продукта за 1 продукт. Себестоим. за е.и. продукта будет: fCost / fOut
			fPrice,		// цена одного продукта. Цена за 1 е.и. продукта будет: fPrice / fOut
			fOut;		// выход продукта в е.и. продукта
		int32_t nCal;	// калорий в продукте. Калорий в 100 гр. будет: muProd.convert(nCal / fOut, mu100gr)
		CGen() : fCost(.0), fPrice(.0), fOut(.0), nCal(0) {}
		const CGen& Init();
		const CGen& operator +=(const CGen&);
	};

/*	struct CAddAlt {	// допольнительный или альтернативный продукт для замены или добавлени ингредиента
		IngID idObj,	// тип и ид-р дополнительного или альтернативного ингредиента
			idTarg;		// тип и ид-р ингредиента из списка m_ai, который может быть заменен на idObj
		bool bAlt;		// если "трю", значит idTarg действительный идентифицирует объект, заменяемый этим
		// тут зависает 3 или 7 байт в зависимости от следующего эл-та. Для эл-та массива CAddAlt 3
		CAddAlt() : bAlt(false) {}
		CAddAlt(IngID io, IngID it, bool ba) { idObj = io; idTarg = it; bAlt = ba; }
	};*/

public:
	// наценка продукта %
	double m_fLoad;
	// m_fRnd: округлять до... может быть 0,001; 0,01; 1; 5; 10; 50; 100; 1000 и т.д.
	float m_fRnd;
	/* индекс е.и. продукта из o::CiTgOpt::m_amu. Эта е.и. для "выхода" продукта - сумма "выхода"
	 * всех ингредиентов.
	 * эта е.и. не годится для определения к-ва продукта в качестве ингредиента другого продукта */
	int m_iMu,
		m_nRes; // выравнивание до 64 бит

public:// конструкцыи/деструкцыи...
	CProduct();
	CProduct(const CProduct &o);
	CProduct(CProduct &&o);
	virtual ~CProduct() {}

	virtual void Init() override;
	const CProduct& operator =(const CProduct &o);
	CProduct& operator =(CProduct &&o);

public:// обязательные переопределения:
	virtual int32_t GetBinData(BYTES &a) const override;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	virtual bool IsValid() const override;
	static size_t sz();

public:// сведения...
#ifdef __ITOG
	// только для сервера. Клиент лишён такой возможности ввиду отсутствия базы
	// реализовано в iTog::product.cpp
	double Cost(double *pfOut = nullptr) const;	// себестоимость продукта за 1 продукт (за штуку)
	double Out() const;		// выход продукта в е.и. продукта
	double Price() const;	// цена продукта (эл-та меню - за шт.)
	int Cal() const;
	const CGen& Data(CGen &g) const;

public:// индексы
	typedef int (*fcmp_prdct)(const CProduct&, const CProduct&);
	static int cmp_cost(const CProduct &a, const CProduct &z);
	static int cmp_price(const CProduct &a, const CProduct &z);
	static int cmp_names(const CProduct &a, const CProduct &z);
	static int cmp_prnt_nms(const CProduct &a, const CProduct &z);
#endif
	static int cmp_inames(const CProduct &a, const CProduct &z);
	static int cmp_flags(const CProduct &a, const CProduct &z);
	static int cmp_load(const CProduct &a, const CProduct &z);

#ifdef __ITOG
#endif// defined __ITOG
};

inline const CProduct::CGen& CProduct::CGen::Init()
{
	fCost = fPrice = fOut = .0;
	nCal = 0;
	return *this;
}

inline const CProduct::CGen& CProduct::CGen::operator +=(const CGen &g)
{
	fCost += g.fCost;
	fPrice += g.fPrice;
	fOut += g.fOut;
	nCal += g.nCal;
	return *this;
}

inline CProduct::CProduct(const CProduct &o)
	: BAS(o)
	, m_fLoad(o.m_fLoad)
	, m_fRnd(o.m_fRnd)
	, m_iMu(o.m_iMu)
{

}

inline CProduct::CProduct(CProduct &&o)
	: BAS(o)
	, m_fLoad(o.m_fLoad)
	, m_fRnd(o.m_fRnd)
	, m_iMu(o.m_iMu)
{

}

inline const CProduct& CProduct::operator =(const CProduct &o)
{
	BAS::operator =(o);
	m_fLoad = o.m_fLoad;
	m_fRnd = o.m_fRnd;
	m_iMu = o.m_iMu;
	return *this;
}

inline CProduct& CProduct::operator =(CProduct &&o)
{
	BAS::operator =(o);
	m_fLoad = o.m_fLoad;
	m_fRnd = o.m_fRnd;
	m_iMu = o.m_iMu;
	return *this;
}

inline void CProduct::Init()
{
	BAS::Init();
	m_flags = ITM_PRDCT;
	m_fLoad = .0;
	m_fRnd = 1.f;
	m_iMu = INV32_BIND;
}

inline bool CProduct::IsValid() const
{
	return (/*m_iMu != o::iMuPg && */BAS::IsValid());
}

inline size_t CProduct::sz()
{
	return (BAS::sz() + SZ_PRODUCT);
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::cmp_load сравнение по наценке продукта
/// \param a
/// \param z
/// \return
///
inline int CProduct::cmp_load(const CProduct &a, const CProduct &z)
{
	return (DCMP_MORE(a.m_fLoad, z.m_fLoad, FLT_EPSILON) ? 1 :
				DCMP_LESS(a.m_fLoad, z.m_fLoad, FLT_EPSILON) ? -1 : 0);
}

#ifdef __ITOG
inline int CProduct::cmp_cost(const CProduct &a, const CProduct &z)
{
	double fa = a.Cost(), fz = z.Cost();
	return (DCMP_LESS(fa, fz, DBL_EPSILON) ? -1 : DCMP_MORE(fa, fz, DBL_EPSILON) ? 1 : 0);
}

inline int CProduct::cmp_price(const CProduct &a, const CProduct &z)
{
	double fa = a.Price(), fz = z.Price();
	return (DCMP_LESS(fa, fz, DBL_EPSILON) ? -1 : DCMP_MORE(fa, fz, DBL_EPSILON) ? 1 : 0);
}

inline int CProduct::cmp_names(const CProduct &a, const CProduct &z)
{
	ASSERT((a.m_flags & ITM_MNMS) == ITM_PRDCT && (z.m_flags & ITM_MNMS) == ITM_PRDCT);
	return fl::CBaseObj::cmp_names(a, z);
}

inline int CProduct::cmp_prnt_nms(const CProduct &a, const CProduct &z)
{
	return fl::CBaseObj::cmp_prnt_nms(a, z);
}
#endif

inline int CProduct::cmp_inames(const CProduct &a, const CProduct &z)
{
	return fl::CBaseObj::cmp_inames(a, z);
}

inline int CProduct::cmp_flags(const CProduct &a, const CProduct &z)
{
	return fl::CBaseObj::cmp_flags(a, z);
}

#define SZ_PRDCTQTT sizeof(double)
/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CPrdctQtt class - продукт и его к-во для счёта cafe::CBill
///
class CPrdctQtt : public CProduct
{
public:
	typedef CProduct BAS;

	double m_fQtt;

public:// конструкцыи/деструкцыи...
	CPrdctQtt();
	CPrdctQtt(const CPrdctQtt &o);
	CPrdctQtt(CPrdctQtt &&o);
	virtual ~CPrdctQtt() {}

	virtual void Init() override;
	const CPrdctQtt& operator =(const CPrdctQtt &o);
	CPrdctQtt& operator =(CPrdctQtt &&o);

public:// обязательные переопределения:
	virtual int32_t GetBinData(BYTES &a) const override;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	virtual bool IsValid() const override;
	static size_t sz();
};

inline CPrdctQtt::CPrdctQtt()
	: BAS()
	, m_fQtt(0.0)
{

}

inline CPrdctQtt::CPrdctQtt(const CPrdctQtt &o)
	: BAS(o)
	, m_fQtt(o.m_fQtt)
{

}

inline CPrdctQtt::CPrdctQtt(CPrdctQtt &&o)
	: BAS(o)
	, m_fQtt(o.m_fQtt)
{

}

inline void CPrdctQtt::Init()
{
	BAS::Init();
	m_fQtt = 0.0;
}

inline const CPrdctQtt& CPrdctQtt::operator =(const CPrdctQtt &o)
{
	BAS::operator=(o);
	m_fQtt = o.m_fQtt;
	return *this;
}

inline int CPrdctQtt::GetBinData(BYTES &a) const
{
	int32_t n = BAS::GetBinData(a);
	a.AddND((CBYTE*) &m_fQtt, SZ_PRDCTQTT);
	return (n + SZ_PRDCTQTT);
}

inline int CPrdctQtt::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_tp(m_fQtt, cpb + i, cnt - i));
}

inline bool CPrdctQtt::IsValid() const
{
	return DCMP_MORE(m_fQtt, .0, DBL_EPSILON);
}

inline size_t CPrdctQtt::sz()
{
	return (BAS::sz() + SZ_PRDCTQTT);
}

inline CPrdctQtt& CPrdctQtt::operator =(CPrdctQtt &&o)
{
	BAS::operator =(o);
	m_fQtt = o.m_fQtt;
	return *this;
}

/*
 * Приход от внешнего поставщика не содержит ITM_INNER. Это значит, что поставщик из таблицы "Контрагенты", а не
 * "Отделы". Флагом ITM_INNER отмечены все внутренние перемещения. Это значит, что поставщик и получатель - оба
 * внутренние отделы. Когда/если осуществляется внутреннее перемещение, делаю две записи - в "Расход" и "Приход".
 * Расход без ITM_INNER значит расход на кассу за деньги, или иначе из предприятия вон. Это так же значит, что
 * товарного прихода в этом случае нет. Но наверное есть приход бабла на кассу.
 * Чтобы извлечь приход от внешнего поставщика: запускаю запрос в таблицу прихода без ITM_INNER т.к. это приход от
 * внешнего источника.
 * Приход для внутреннего: запускаю 2 запроса - с ITM_INNER и без него - оба нужны. Один приход от внутреннего
 * поставщика/отдела, другой от внешнего. Только так можно получить достоверные сведения о приходе для опреде-
 * лённого отдела. Можно конечно допустить, что приход извне для обычного отдела не допускается, но это доп.
 * ораничение, может, не нужное.
 * Так же с расходом для любого отдела: 2 запроса наверняка.
 * Расход для любого отдела без ITM_INNER значит расход на кассу, т.е. однозначно out, за деньги. Даже для склада -
 * расход в другой отдел с ITM_INNER. Поэтому для склада приход всегда без ITM_INNER, а расход - наоборот, с ITM_INNER.
 * Весь приход/расход товара, соответственно, остаток без учёта поставщика/получателя возможно получить без
 * ITM_INNER. Сумма остатков всех отделов этого товара должна будет равняться этому остатку.
 *
 * Возможно ли как-то иначе организовать приход/расход для каждого отдела с учётом внутренних перемещений?
 * Приход и расход всегда содержит отправителя и получателя. Он так же должен быть отмечен как внутренний,
 * если за него не берут бабла. Стоит ли приход-расход разделять радикально на внутренний и внешний? Если
 * не разделять, можно брать весь приход или расход для одного отдела одним запросом вместо двух. Или для
 * всех отделов вместе. А чтобы выделить внутренний/внешний, нужно фильтровать.
 * Сейчас индекс упорядочивает по ITM_INNER, ид-ру поставщика, ид-ру товара и времени. Для всего расхода/прихода
 * без учёта ITM_INNER можно сократить индекс, оставив ид-ры поставщика и товара и время. Тогда не придётся делать
 * 2 запроса. Добавил индекс без ITM_INNER.
 *
 * Флаг ITM_INNER обозначает внутреннее перемещение, точнее, внутреннего поставщика и получателя. Без ITM_INNER может
 * быть только приход от внешнего поставщика и расход вовне.
 */

#define SZ_INCOME (sizeof(int32_t) * 4 + sizeof(double) * 2)
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIncome class - эл-т файлового массива "приход" store::income
///
class CIncome : public fl::CFileItem
{
public:
	enum EIndices {
		iIncmTmInStplSplrRcvr,// cmp_tm_istpl_splr_rcvr уник.индекс по времени, типу перемещения и всем ид-рам объектов
		iIncmStplTm,		// cmp_istpl_tm упорядочиваю приход по идентификатору товара и времени (не уникальный индекс)
		iIncmStplInTm,		// cmp_istpl_in_tm по идентификатору товара и времени, разделяя внутренний и внешний приход
		iIncmSplrStplTm,	// cmp_isplr_stpl_tm по ид-рам поставщика и товара, по времени, разделяя внутр./внешний приход
		iIncmRcvrStplTm,	// cmp_ircvr_stpl_tm по ид-рам получателя и товара, по времени, разделяя внутр./внешний приход
		iIncmInds			// к-во индексов в этом массиве
	};
public:
	double	m_fQtt,		// К-во товара в е.и. товара
			m_fCost;	// цена прихода за е.и. товара
	int32_t	m_iStpl,	// Ид-р товара из store::goods
			m_iSplr,	// Если внутреннее перемещение, то ид-р отдела из cafe::depts. Если внешнее, то контрактника.
			m_iRcvr,	// Ид-р получателя (Отдел: Склад/Касса или другой. Все из cafe::depts)
			m_iCwrkr;	// Ид-р сотрудника, принявшего товар из cafe::cwrkrs
public:
	CIncome();
	CIncome(const CIncome&);
	virtual ~CIncome() {}
	static size_t sz();
	virtual void Init() override;
//	CBYTE* operator =(CBYTE *pb) override;
	const CIncome& operator =(const CIncome &o);
	bool IsValid() const override;
	bool IsEmpty() const override;
	bool IsNull() const override;
	int GetBinData(glb::CFxArray<BYTE, true> &a) const override;
	int SetBinData(CBYTE *cpb, const int32_t cnt) override;
public:
	typedef int(*fcmp)(const CIncome&, const CIncome&);
	static int cmp_istpl_tm(const CIncome &a, const CIncome &z);
	static int cmp_istpl_in_tm(const CIncome &a, const CIncome &z);
	static int cmp_isplr_stpl_tm(const CIncome &a, const CIncome &z);
	static int cmp_ircvr_stpl_tm(const CIncome &a, const CIncome &z);
	static int cmp_tm_istpl_splr_rcvr(const CIncome &a, const CIncome &z);
};

inline CIncome::CIncome()
{
	m_iStpl = m_iSplr = m_iRcvr = m_iCwrkr = INV32_BIND;
	m_fQtt = m_fCost = 0.0;
}

inline CIncome::CIncome(const CIncome &o)
	: fl::CFileItem(o)
	, m_iStpl(o.m_iStpl)
	, m_iSplr(o.m_iStpl)
	, m_iRcvr(o.m_iRcvr)
	, m_iCwrkr(o.m_iCwrkr)
	, m_fQtt(o.m_fQtt)
	, m_fCost(o.m_fCost)
{

}

inline void CIncome::Init()
{
	fl::CFileItem::Init();
	m_iStpl = m_iSplr = m_iRcvr = m_iCwrkr = INV32_BIND;
	m_fQtt = m_fCost = 0.0;
}

/*inline CBYTE* CIncome::operator =(CBYTE *cpb)
{
	memcpy(&m_fQtt, fl::CFileItem::operator =(cpb), SZ_INCOME);
	return (cpb + SZ_VFI + SZ_INCOME);
}*/

inline const CIncome& CIncome::operator =(const CIncome &o)
{
	fl::CFileItem::operator =(o);
	memcpy(&m_fQtt, &o.m_fQtt, SZ_INCOME);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::IsValid не действительный эл-т значит удалённый в файле. Проверяю перед за-
/// писью в файл. Не действительный эл-т не может быть добавлен в файл, но может быть заменен
/// на не действительный.
/// \return трю/хрю
///
inline bool CIncome::IsValid() const
{	// если приход внешний, ид-ры поставщика и получателя могут быть одинаковыми
	return ((!(m_flags & ITM_INNER) || m_iRcvr != m_iSplr) && DCMP_MORE(m_fQtt, .0, .0001) &&
			DCMP_MORE(m_fCost, .0, DBL_EPSILON) && fl::CFileItem::IsValid());
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::IsEmpty приход с отстутствующим ид-ром товара, получатели или отправителя
/// или отсутствующим к-вом или ценой, подлежит удалению в файле
/// \return трю/хрю
///
inline bool CIncome::IsEmpty() const
{
	return (m_iStpl < 0 || m_iSplr < 0 || m_iRcvr < 0 ||
			DCMP_LOE(m_fQtt, .0, DBL_EPSILON) || DCMP_LOE(m_fCost, .0, DBL_EPSILON));
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::IsNull отсутствующий приход совершенно новый
/// \return трю/хрю
///
inline bool CIncome::IsNull() const
{
	return (m_iStpl < 0 && m_iRcvr < 0 && m_iSplr < 0 && m_iCwrkr < 0 && !DCMP_MORE(m_fQtt, .0, DBL_EPSILON) &&
			!DCMP_MORE(m_fCost, .0, DBL_EPSILON) && fl::CFileItem::IsNull());
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::cmp_tm_istpl_splr_rcvr уник.индекс - сравнивает эл-ты по времени, типу (внутреннее
/// перемещение или внешнее), ид-рам товара, ид-рам поставщика, ид-рам получателя.
/// \param a, z: сравниваемые объекты
/// \return -1|0|1
///
inline int CIncome::cmp_tm_istpl_splr_rcvr(const CIncome &a, const CIncome &z)
{
	uint32_t f1 = a.m_flags & ITM_INNER, f2 = z.m_flags & ITM_INNER;
	return (a.m_tme < z.m_tme) ? -1 : (a.m_tme > z.m_tme) ? 1 :
									  (f1 < f2) ? -1 : (f1 > f2) ? 1 :
									  (a.m_iStpl < z.m_iStpl) ? -1 : (a.m_iStpl > z.m_iStpl) ? 1 :
									  (a.m_iSplr < z.m_iSplr) ? -1 : (a.m_iSplr > z.m_iSplr) ? 1 :
									  (a.m_iRcvr < z.m_iRcvr) ? -1 : (a.m_iRcvr > z.m_iRcvr) ? 1 : 0;
}

inline size_t CIncome::sz()
{
	return (fl::CFileItem::sz() + SZ_INCOME);
}

#define SZ_EXPEND  (sizeof(int32_t) * 4 + sizeof(double))
//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CExpend class расход товара
///
class CExpend : public fl::CFileItem
{
public:
	typedef fl::CFileItem BAS;
	enum EIndices {
		iStplTm,		// по ид-ру товара и времени
		iTmStpl,		// по времени и ид-ру товара
		iInSndrStplTm,	// по флагу ITM_INNER, ид-рам отправителя/поставщика и товара и даты-времени расхода
		iSndrStplTm,	// по ид-рам отправителя/поставщика и товара и даты-времени расхода
		iInResStplTm,	// по флагу ITM_INNER, ид-рам получателя и товара и даты-времени расхода
		iResStplTm,		// по ид-рам получателя и товара и даты-времени расхода
		iExpInds
	};
public:
	double	m_fQtt;		// К-во в расходной е.и. Не забудь привести если перемещаешь внутри с одного отдела в другой.
	int32_t	m_iStpl,	// Ид-р товара
			m_iSplr,	// Ид-р отправителя (внешнего, если ITM_INNER, то внутреннего)
			m_iRcvr,	// Ид-р получателя (Склад-Отдел/Касса)
			m_res;		// выравниваю до 64 бит
public:
	CExpend();
	CExpend(const CExpend &o);
	virtual ~CExpend() {}
public:
	int GetBinData(BYTES &a) const override;
	int SetBinData(CBYTE *cpb, cint32_t cnt) override;
	static size_t sz();
//	CBYTE* operator =(CBYTE *cpb) override;
	const CExpend& operator =(const CExpend &o);
	bool IsNull() const override;
	bool IsEmpty() const override;
#ifdef __ITOG
public:
	typedef int(*fcmp)(const CExpend&, const CExpend&);
	static int cmp_istpl_tm(const CExpend &a, const CExpend &z);		// товар и время
	static int cmp_tm_stpl(const CExpend &a, const CExpend &z);			// время и товар
	static int cmp_in_splr_stpl_tm(const CExpend &a, const CExpend &z);
	static int cmp_splr_stpl_tm(const CExpend &a, const CExpend &z);
	static int cmp_in_rcvr_stpl_tm(const CExpend &a, const CExpend &z);	// получатель, товар и время
	static int cmp_rcvr_stpl_tm(const CExpend &a, const CExpend &z);		// получатель, товар и время
#endif// defined __ITOG
};

inline CExpend::CExpend()
{
	m_iStpl = m_iSplr = m_iRcvr = -1;
	m_fQtt = 0.0;
}

inline CExpend::CExpend(const CExpend &o)
	: BAS(o)
	, m_iStpl(o.m_iStpl)
	, m_iSplr(o.m_iSplr)
	, m_iRcvr(o.m_iRcvr)
	, m_fQtt(o.m_fQtt)
{

}

/*inline CBYTE* CExpend::operator =(CBYTE *cpb)
{
	memcpy(this, fl::CFileItem::operator =(cpb), SZ_EXPEND);
	return (cpb + SZ_VFI + SZ_EXPEND);
}*/

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CExpend::IsNull отсутствующий эл-т совершенно новый
/// \return трю/хрю
///
inline bool CExpend::IsNull() const
{
	return (m_iStpl < 0 && m_iRcvr < 0 && m_iSplr < 0 &&
			!DCMP_MORE(m_fQtt, .0, DBL_EPSILON) && fl::CFileItem::IsNull());
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CExpend::IsEmpty расход с отсутствующим ид-ром товара, отправителя или получателя
/// или к-вом расхода подлежит удалению в файле
/// \return трю/хрю
///
inline bool CExpend::IsEmpty() const
{
	return (m_iStpl < 0 || m_iSplr < 0 || m_iRcvr < 0 || DCMP_LOE(m_fQtt, .0, DBL_EPSILON));
}

inline size_t CExpend::sz()
{
	return (BAS::sz() + SZ_EXPEND);
}

#define SZ_FLCAT	(sizeof(fl::CBaseObj::Type) + sizeof(int32_t) * 2)
/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CCat class категория товара какого-то объекта, как товар, отдел, поставщик.
///
class CCat : public fl::CFileItem {
public:
	enum EIndices {
		itic,	// уник.индекс по типу объекта, его ид-ру и категории
		iti,	// индекс по типу и ид-ру
		itc,	// индекс по типу и категории товара
		inds
	};
public:
	fl::CBaseObj::Type t;	// тип объекта
	int32_t i;				// базовый ид-р объекта в файловом массиве типа 't'
	uint32_t u;				// концевая категория товара как SC_FD см. o::CiTgOpts::CCat
	CCat();
	CCat(const CCat&);
	CCat(fl::CBaseObj::Type type, int32_t iObjID, uint32_t uCat);
	virtual ~CCat() {}
	void Set(fl::CBaseObj::Type type, int32_t iObjID, uint32_t uCat);
	virtual int GetBinData(BYTES &a) const override;
	virtual int SetBinData(CBYTE *cpb, cint32_t cnt) override;
	virtual const CCat& operator =(const CCat &o);
	virtual bool IsNull() const override;
	virtual bool IsEmpty() const override;
	typedef int(*fcmp)(const CCat&, const CCat&);
	static int cmp_tic(const CCat &a, const CCat &z);	// уник.индекс
	static int cmp_ti(const CCat &a, const CCat &z);
	static int cmp_tc(const CCat &a, const CCat &z);
//	static int cmp_ct(const CCat &a, const CCat &z);
};

inline CCat::CCat()
	: t(fl::CBaseObj::ndef)
	, i(INV32_BIND)
	, u(0)
{

}

inline CCat::CCat(const CCat &o)
	: fl::CFileItem(o)
	, t(o.t)
	, i(o.i)
	, u(o.u)
{

}

inline CCat::CCat(fl::CBaseObj::Type type, int32_t iObjID, uint32_t uCat)
	: t(type)
	, i(iObjID)
	, u(uCat)
{

}

inline void CCat::Set(fl::CBaseObj::Type type, int32_t iObjID, uint32_t uCat)
{
	Init();
	t = type;
	i = iObjID;
	u = uCat;
}

inline int CCat::GetBinData(BYTES &a) const
{
	int n = fl::CFileItem::GetBinData(a);
	a.AddND((BYTE*) &t, SZ_FLCAT);
	return (n + SZ_FLCAT);
}

inline int CCat::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int i = fl::CFileItem::SetBinData(cpb, cnt);
	return i + glb::read_raw((BYTE*) &t, SZ_FLCAT, cpb + i, cnt - i);
}

inline const CCat& CCat::operator =(const CCat &o)
{
	fl::CFileItem::operator =(o);
	memcpy(&t, &o.t, SZ_FLCAT);
	return *this;
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief CCat::IsNull отсутствующий объект совершенно новый
/// \return трю/хрю
///
inline bool CCat::IsNull() const
{
	return (t == fl::CBaseObj::ndef && i == INV32_BIND && fl::CFileItem::IsNull());
}

/////////////////////////////////////////////////////////////////////////////////
/// \brief CCat::IsEmpty категория с отстутствующим эл-том подлежит удалению в файле
/// \return трю/хрю
///
inline bool CCat::IsEmpty() const
{
	return (i < 0);
}

/////////////////////////////////////////////////////////////////////////////////
/// \brief CCat::cmp_tic тип, ид-р объекта и категория составляют уникальный индекс
/// То есть, тип и ид-р могут быть одними и теми же, отличаться объекты могут категорией
/// тип и категория могут быть одними и теми же, оличаться объекты могут ид-ром
/// ид-р объекта и категория могут быть одними и теми же, отличаться объекты могут типом
/// \param a, z
/// \return <0|0|>0
///
inline int CCat::cmp_tic(const CCat &a, const CCat &z)
{
	int na = a.t, nz = z.t;
	return ((na != nz) ? na - nz : (a.i != z.i) ? a.i - z.i : o::CiTgOpts::CCat::cmpc(a.u, z.u));
}

inline int CCat::cmp_ti(const CCat &a, const CCat &z)
{
	return ((a.t != z.t) ? a.t - z.t : a.i - z.i);
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CCat::cmp_tc сортировка по типу и категории.
/// Результат выборки: множество с идентичными типом и категорией и разными ид-рами
/// \param a, z
/// \return <0|0|>0
///
inline int CCat::cmp_tc(const CCat &a, const CCat &z)
{
	return ((a.t != z.t) ? a.t - z.t : o::CiTgOpts::CCat::cmpc(a.u, z.u));
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CCat::cmp_b сортировка по категории и типу: одна категория множество типов
/// \param a, z
/// \return <0|0|>0
///
/*inline int CCat::cmp_ct(const CCat &a, const CCat &z)
{
	int n = o::CiTgOpts::CCat::cmpc(a.u, z.u);
	if (n)
		return n;
	return (a.t != z.t) ? a.t - z.t : 0;
}*/

}// namespace store
