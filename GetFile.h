#pragma once
/*
 * Copyright 2021-2023 Николай Вамбольдт
 *
 * Файл GetFile.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021-2023 Nikolay Vamboldt
 *
 * GetFile.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "global.h"
#include "ErrLog.h"
#if defined __ITOG | defined __STRFLA
#include "io_strings.h"
#endif// defined __ITOG | defined __STRFLA
namespace err {
extern CLog log;
}// namespace err
namespace fl {
	extern int nBlockSz;	// блок диска для ф-цый чтения/записи в CBinFile см. CPath::CreateAppPath
#if defined __ITOG | defined __STRFLA
	extern CStrings strs;
#endif// defined __ITOG | defined __STRFLA
}// namespace fl
namespace glb {
#if defined __ITOG | defined __STRFLA
	const char* FileName(const char *pcszPath, int *pnLen);
/////////////////////////////////////////////////////////////////////////////////////
/// \brief GetFile читаю файл, добавляю содержимое в массив, возвращаю к-во записанных байт.
/// \param A: может быть CFxArray<BYTE, true>, CFxArray<char, true> или CFxString.
/// \param TP: может быть BYTE или char
/// \param pcszPthNm (в): путь и имя файла.
/// \param ab (из): массив, куда добавляю прочитанное из файла.
/// \return к-во записанных в 'ab' байт
///
template<class A, class TP>
int32_t GetFile(CCHAR *pcszPthNm, A &a)
{
	ASSERT(pcszPthNm);
	ASSERT(fl::nBlockSz);
	FILE *pf;
	const int32_t ca = a.GetCount();
	int32_t e, na = ca, nRead;
	fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs), os2(fl::strs);
	fl::CString &sf = os.obj(), &sd = os2.obj();// формат сообщения ошибки
	cerror = 0;
	if (!(pf = fopen(pcszPthNm, "rb"))) {
		e = cerror;
		throw err::CCException(e, SZ(SI_FLE, sf), time(NULL), SZ(SI_WOPEN, sd), pcszPthNm,
							   strerror(e), FileName(__FILE__, nullptr), __LINE__);
	}
	a.SetCountND(na + fl::nBlockSz);
	while ((nRead = (int32_t) fread((TP*) a + na, 1, fl::nBlockSz, pf)) > 0) {
		na += nRead;
		if (nRead != fl::nBlockSz)
			break;
		a.SetCountND(na + fl::nBlockSz);
	}
	if (feof(pf)) {
		fclose(pf);
		a.SetCountND(na);
		return (na - ca); // к-во байт записанных в 'ab'
	}
	e = cerror;
	fclose(pf);
	a.SetCountND(ca);
	throw err::CCException(e, SZ(SI_FLE, sf), time(NULL), SZ(SI_WREAD, sd), pcszPthNm,
						   strerror(e), FileName(__FILE__, nullptr), __LINE__);
}
#endif //defined __ITOG | defined __STRFLA
}// namespace glb