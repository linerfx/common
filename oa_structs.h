#pragma once
/*
 * Copyright 2021 Николай Вамбольдт
 *
 * Файл 'oa_structs.h' — часть программы 'reader', входящей в проект 'linerfx'
 *
 * 'reader' свободная программа: вы можете перераспространять ее и/или изменять
 * ее на условиях Стандартной общественной лицензии GNU в том виде, в каком
 * она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * 'reader' распространяется в надежде, что она будет полезной,
 * но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
 * общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с этой программой. Если это не так, см.
 * <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * 'oa_structs.h' is part of 'reader', which is a part of 'linerfx'.
 *
 * 'reader' is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 'reader' is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE
 * along with 'reader'. If not, see <https://www.gnu.org/licenses/>.
 */
#include "oa_types.h"
#include "oa_defs.h"
#include "incl_fxarray.h"
#include "defs.h"
#include "io_file.h"
/* Файл 'oa_structs.h' создан программой 'reader' наряду с 'oa_id.cpp' и 'oa_enum.h' и 'oa_terms.txt'.
 */

namespace oatp {

/*
 * A tag associated with an entity.
 */
struct Tag
{	// The type of the tag.
	string type;
	// The name of the tag.
	string name;

	Tag() : type(8), name(8) {}

	void Init() {
		type.SetCountZeroND();
		name.SetCountZeroND();
	}
	int InvalidElemetID() const {
		return (!type.GetCount() ? OA_type : !name.GetCount() ? OA_name : 0);
	}
	int putcp(long int at, fl::CBinFile &file) const;
	int getcp(long int at, fl::CBinFile &file);
	bool operator ==(const Tag &t) const;
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * A FinancingDayOfWeek message defines a day of the week when financing charges are debited or credited.
 */
struct FinancingDayOfWeek
{	/* DayOfWeek: The DayOfWeek provides a representation of the day of the week.
	 * возможные значения:
	 * case SUNDAY: // Sunday
	 * case MONDAY: // Monday
	 * case TUESDAY: // Tuesday
	 * case WEDNESDAY: // Wednesday
	 * case THURSDAY: // Thursday
	 * case FRIDAY: // Friday
	 * case SATURDAY: // Saturday */
	DayOfWeek dayOfWeek; /* The day of the week to charge the financing. */
	// The number of days worth of financing to be charged on dayOfWeek.
	integer daysCharged;
	/*
	 * функцыонал
	 */
	FinancingDayOfWeek() {
		dayOfWeek = daysCharged = 0;
	}
	void Init() {
		dayOfWeek = daysCharged = 0;
	}
	int InvalidElementID() {
		return (!dayOfWeek ? OA_day_Of_Week : !daysCharged ? OA_days_Charged : 0);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * Financing data for the instrument.
 */
struct InstrumentFinancing
{	/* The financing rate to be used for a long position for the instrument. The value is in decimal rather than
	 * percentage points, i.e. 5% is represented as 0.05.*/
	DecimalNumber longRate;
	/* The financing rate to be used for a short position for the instrument. The value is in decimal rather than
	 * percentage points, i.e. 5% is represented as 0.05.*/
	DecimalNumber shortRate;
	/* The days of the week to debit or credit financing charges; the exact time of day at which to charge the
	 * financing is set in the DivisionTradingGroup for the client’s account.*/
	glb::CFxArray<FinancingDayOfWeek, true> financingDaysOfWeek;
	//
	InstrumentFinancing() : financingDaysOfWeek(4) {
		longRate = shortRate = 0.0;
	}
	void Init() {
		longRate = shortRate = 0.0;
		financingDaysOfWeek.SetCountZeroND();
	}
	int InvalidElementID() const {
		return ((longRate == 0.0) ? OA_long_Rate : (shortRate == 0.0) ? OA_short_Rate :
									!financingDaysOfWeek.GetCount() ? OA_financing_Days_Of_Week : 0);
	}
	int putcp(long int at, fl::CBinFile &file) const;
	int getcp(long int at, fl::CBinFile &file);
	bool operator ==(const InstrumentFinancing &i) const;
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * An InstrumentCommission represents an instrument-specific commission
 */
struct InstrumentCommission
{	// The commission amount (in the Account’s home currency) charged per unitsTraded of the instrument
	DecimalNumber commission;
	// The number of units traded that the commission amount is based on.
	DecimalNumber unitsTraded;
	// The minimum commission amount (in the Account’s home currency) that is charged when an Order is filled for this instrument.
	DecimalNumber minimumCommission;
	//
	InstrumentCommission() {
		commission = unitsTraded = minimumCommission = 0.0;
	}
	void Init() {
		commission = unitsTraded = minimumCommission = 0.0;
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
	int InvalidElementID() const {
		return((commission == 0.0) ?
				   OA_commission : (unitsTraded == 0.0) ?
					   OA_units_Traded : (minimumCommission == 0.0) ? OA_minimum_Commission : 0);
	}
	bool operator ==(const InstrumentCommission &i) const;
};

/*
 * A GuaranteedStopLossOrderLevelRestriction represents the total position size that can exist within a given price window for Trades with guaranteed Stop Loss Orders attached for a specific Instrument.
 */
struct GuaranteedStopLossOrderLevelRestriction
{	// Applies to Trades with a guaranteed Stop Loss Order attached for the specified Instrument. This is the total allowed Trade volume that can exist within the priceRange based on the trigger prices of the guaranteed Stop Loss Orders.
	DecimalNumber volume;
	// The price range the volume applies to. This value is in price units.
	DecimalNumber priceRange;
	//
	GuaranteedStopLossOrderLevelRestriction() {
		priceRange = volume = 0.0;
	}
	void Init() {
		priceRange = volume = 0.0;
	}
	int InvalidElementID() const {
		return ((volume == 0.0) ? OA_volume : (priceRange == 0.0) ? OA_price_Range : 0);
	}
	bool IsValid() const {
		return (volume != 0.0 && priceRange != 0.0);
	}
	bool operator ==(const GuaranteedStopLossOrderLevelRestriction &g) const;
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * Full specification of an Instrument.
 */
struct Instrument
{	/* InstrumentType: The type of an Instrument.
	 * возможные значения:
	 * case CURRENCY: // Currency
	 * case CFD: // Contract For Difference
	 * case METAL: // Metal */
	InstrumentType type; /* The type of the Instrument */
	/* GuaranteedStopLossOrderModeForInstrument: The overall behaviour of the Account regarding Guaranteed Stop Loss Orders for a specific Instrument.
	 * возможные значения:
	 * case DISABLED: // The Account is not permitted to create Guaranteed Stop Loss Orders for this Instrument.
	 * case ALLOWED: // The Account is able, but not required to have Guaranteed Stop Loss Orders for open Trades for this Instrument.
	 * case REQUIRED: // The Account is required to have Guaranteed Stop Loss Orders for all open Trades for this Instrument. */
	GuaranteedStopLossOrderModeForInstrument guaranteedStopLossOrderMode; /* The current Guaranteed Stop Loss Order mode of the Account for this Instrument. */
	// The name of the Instrument
	InstrumentName name;
	// The display name of the Instrument
	string displayName;
	/* The location of the “pip” for this instrument. The decimal position of the pip in this Instrument’s price
	 * can be found at 10 ^ pipLocation (e.g. -4 pipLocation results in a decimal pip position of 10 ^ -4 = 0.0001). */
	integer pipLocation;
	/* The number of decimal places that should be used to display prices for this instrument. (e.g. a
	 * displayPrecision of 5 would result in a price of “1” being displayed as “1.00000”) */
	integer displayPrecision;
	// The amount of decimal places that may be provided when specifying the number of units traded for this instrument.
	integer tradeUnitsPrecision;
	// The smallest number of units allowed to be traded for this instrument.
	DecimalNumber minimumTradeSize;
	// The maximum trailing stop distance allowed for a trailing stop loss created for this instrument. Specified in price units.
	DecimalNumber maximumTrailingStopDistance;
	// The minimum distance allowed between the Trade’s fill price and the configured price for guaranteed Stop Loss Orders created for this instrument. Specified in price units.
	DecimalNumber minimumGuaranteedStopLossDistance;
	// The minimum trailing stop distance allowed for a trailing stop loss created for this instrument. Specified in price units.
	DecimalNumber minimumTrailingStopDistance;
	// The maximum position size allowed for this instrument. Specified in units.
	DecimalNumber maximumPositionSize;
	// The maximum units allowed for an Order placed for this instrument. Specified in units.
	DecimalNumber maximumOrderUnits;
	// The margin rate for this instrument.
	DecimalNumber marginRate;
	// The commission structure for this instrument.
	InstrumentCommission commission;
	// The amount that is charged to the account if a guaranteed Stop Loss Order is triggered and filled. The value is in price units and is charged for each unit of the Trade. This field will only be present if the Account’s guaranteedStopLossOrderMode for this Instrument is not ‘DISABLED’.
	DecimalNumber guaranteedStopLossOrderExecutionPremium;
	// The guaranteed Stop Loss Order level restriction for this instrument. This field will only be present if the Account’s guaranteedStopLossOrderMode for this Instrument is not ‘DISABLED’.
	GuaranteedStopLossOrderLevelRestriction guaranteedStopLossOrderLevelRestriction;
	// Financing data for this instrument.
	InstrumentFinancing financing;
	// The tags associated with this instrument.
	glb::CFxArray<Tag, false> tags;
	//
	Instrument() : tags(4) {
		Init();
	}
	void Init();
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
	int putcp(long int at, fl::CBinFile &file) const;
	int getcp(long int at, fl::CBinFile &file);
	bool operator ==(const Instrument &i) const;
};

/*
 * A ConversionFactor contains information used to convert an amount, from an Instrument’s base or quote currency, to the home currency of an Account.
 */
struct ConversionFactor
{	// The factor by which to multiply the amount in the given currency to obtain the amount in the home currency of the Account.
	DecimalNumber factor;
};

/*
 * A HomeConversionFactors message contains information used to convert amounts, from an Instrument’s base or quote currency, to the home currency of an Account.
 */
struct HomeConversionFactors
{	// The ConversionFactor in effect for the Account for converting any gains realized in Instrument quote units into units of the Account’s home currency.
	ConversionFactor gainQuoteHome;
	// The ConversionFactor in effect for the Account for converting any losses realized in Instrument quote units into units of the Account’s home currency.
	ConversionFactor lossQuoteHome;
	// The ConversionFactor in effect for the Account for converting any gains realized in Instrument base units into units of the Account’s home currency.
	ConversionFactor gainBaseHome;
	// The ConversionFactor in effect for the Account for converting any losses realized in Instrument base units into units of the Account’s home currency.
	ConversionFactor lossBaseHome;
};

/*
 * A PriceBucket represents a price available for an amount of liquidity
 */
struct PriceBucket
{	// The Price offered by the PriceBucket
	PriceValue price;
	// The amount of liquidity offered by the PriceBucket
	integer liquidity;
	//
	PriceBucket() {
		price = 0.0;
		liquidity = 0;
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * QuoteHomeConversionFactors represents the factors that can be used used to convert quantities of a Price’s Instrument’s quote currency into the Account’s home currency.
 */
struct QuoteHomeConversionFactors
{	// The factor used to convert a positive amount of the Price’s Instrument’s quote currency into a positive amount of the Account’s home currency. Conversion is performed by multiplying the quote units by the conversion factor.
	DecimalNumber positiveUnits;
	// The factor used to convert a negative amount of the Price’s Instrument’s quote currency into a negative amount of the Account’s home currency. Conversion is performed by multiplying the quote units by the conversion factor.
	DecimalNumber negativeUnits;
	//
	QuoteHomeConversionFactors() {
		positiveUnits = negativeUnits = 0.0;
	}
	void Init() {
		positiveUnits = negativeUnits = 0.0;
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * HomeConversions represents the factors to use to convert quantities of a given currency into the Account’s home currency. The conversion factor depends on the scenario the conversion is required for.
 */
struct HomeConversions
{	// The currency to be converted into the home currency.
	Currency currency;
	// The factor used to convert any gains for an Account in the specified currency into the Account’s home currency. This would include positive realized P/L and positive financing amounts. Conversion is performed by multiplying the positive P/L by the conversion factor.
	DecimalNumber accountGain;
	// The factor used to convert any losses for an Account in the specified currency into the Account’s home currency. This would include negative realized P/L and negative financing amounts. Conversion is performed by multiplying the positive P/L by the conversion factor.
	DecimalNumber accountLoss;
	// The factor used to convert a Position or Trade OA_Value in the specified currency into the Account’s home currency. Conversion is performed by multiplying the Position or Trade OA_Value by the conversion factor.
	DecimalNumber positionValue;
};

/*
 * A PricingHeartbeat object is injected into the Pricing stream to ensure that the HTTP connection remains active.
 */
struct PricingHeartbeat
{	// The string “HEARTBEAT” default=HEARTBEAT),
	string type;
	// The date/time when the Heartbeat was created.
	DateTime time;
};

/*
 * The representation of a Position for a single direction (long or short).
 */
struct PositionSide
{	// Number of units in the position (negative value indicates short position, positive indicates long position).
	DecimalNumber units;
	// Volume-weighted average of the underlying Trade open prices for the Position.
	PriceValue averagePrice;
	// List of the open Trade IDs which contribute to the open Position.
	glb::CFxArray<TradeID, true> tradeIDs;
	// Profit/loss realized by the PositionSide over the lifetime of the Account.
	AccountUnits pl;
	// The unrealized profit/loss of all open Trades that contribute to this PositionSide.
	AccountUnits unrealizedPL;
	// Profit/loss realized by the PositionSide since the Account’s resettablePL was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected for this PositionSide over the lifetime of the Account.
	AccountUnits financing;
	// The total amount of dividend adjustment paid for the PositionSide over the lifetime of the Account.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders attached to Trades for this PositionSide.
	AccountUnits guaranteedExecutionFees;
	/*
	 * функцыонал
	 */
	PositionSide() : tradeIDs(4) {
		units = averagePrice = pl = unrealizedPL = resettablePL = financing = dividendAdjustment =
				guaranteedExecutionFees = 0.0;
	}
	void Init() {
		units = averagePrice = pl = unrealizedPL = resettablePL = financing = dividendAdjustment =
				guaranteedExecutionFees = 0.0;
		tradeIDs.SetCountZeroND();
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The specification of a Position within an Account.
 */
struct Position
{	// The Position’s Instrument.
	InstrumentName instrument;
	// Profit/loss realized by the Position over the lifetime of the Account.
	AccountUnits pl;
	// The unrealized profit/loss of all open Trades that contribute to this Position.
	AccountUnits unrealizedPL;
	// Margin currently used by the Position.
	AccountUnits marginUsed;
	// Profit/loss realized by the Position since the Account’s resettablePL was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected for this instrument over the lifetime of the Account.
	AccountUnits financing;
	// The total amount of commission paid for this instrument over the lifetime of the Account.
	AccountUnits commission;
	// The total amount of dividend adjustment paid for this instrument over the lifetime of the Account.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders for this instrument.
	AccountUnits guaranteedExecutionFees;
	// The details of the long side of the Position.
	PositionSide Long;
	// The details of the short side of the Position.
	PositionSide Short;
	/*
	 * функцыонал
	 */
	Position() {
		pl = unrealizedPL = marginUsed = resettablePL = financing = commission = dividendAdjustment =
			guaranteedExecutionFees	= 0.0;
	}
	void Init() {
		pl = unrealizedPL = marginUsed = resettablePL = financing = commission = dividendAdjustment =
			guaranteedExecutionFees	= 0.0;
		Long.Init();
		Short.Init();
		instrument.SetCountZeroND();
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The dynamic (calculated) state of a Position
 */
struct CalculatedPositionState
{	// The Position’s Instrument.
	InstrumentName instrument;
	// The Position’s net unrealized profit/loss
	AccountUnits netUnrealizedPL;
	// The unrealized profit/loss of the Position’s long open Trades
	AccountUnits longUnrealizedPL;
	// The unrealized profit/loss of the Position’s short open Trades
	AccountUnits shortUnrealizedPL;
	// Margin currently used by the Position.
	AccountUnits marginUsed;
};

/*
 * The dynamic (calculated) state of an open Trade
 */
struct CalculatedTradeState
{	// The Trade’s ID.
	TradeID id;
	// The Trade’s unrealized profit/loss.
	AccountUnits unrealizedPL;
	// Margin currently used by the Trade.
	AccountUnits marginUsed;
};

/*
 * An OrderIdentifier is used to refer to an Order, and contains both the OrderID and the ClientOrderID.
 */
struct OrderIdentifier
{	// The OANDA-assigned Order ID
	OrderID orderID;
	// The client-provided client Order ID
	ClientID clientOrderID;
};

/*
 * The dynamic state of an Order. This is only relevant to TrailingStopLoss Orders, as no other Order type has dynamic state.
 */
struct DynamicOrderState
{	// The Order’s ID.
	OrderID id;
	// The Order’s calculated trailing stop value.
	PriceValue trailingStopValue;
	// The distance between the Trailing Stop Loss Order’s trailingStopValue and the current Market Price. This represents the distance (in price units) of the Order from a triggering price. If the distance could not be determined, this value will not be set.
	PriceValue triggerDistance;
	// True if an exact trigger distance could be calculated. If false, it means the provided trigger distance is a best estimate. If the distance could not be determined, this value will not be set.
	boolean isTriggerDistanceExact;
};

/*
 * Representation of many units of an Instrument are available to be traded for both long and short Orders.
 * Здесь регистр первой буквы в 'long' и 'short' задран, чтобы отличался от типа в С/С++.
 */
struct UnitsAvailableDetails
{	// The units available for long Orders.
	DecimalNumber m_fLong;
	// The units available for short Orders.
	DecimalNumber m_fShort;
	//
	UnitsAvailableDetails() {
		m_fLong = m_fShort = 0.0;
	}
	void Init() {
		m_fLong = m_fShort = 0.0;
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * Representation of how many units of an Instrument are available to be traded by an Order depending on its
 * positionFill option.
 * Здесь в имене параметра Default первая буква задрана в верхний регистр.
 */
struct UnitsAvailable
{	// The number of units that are available to be traded using an Order with a positionFill option of “DEFAULT”. For an Account with hedging enabled, this value will be the same as the “OPEN_ONLY” value. For an Account without hedging enabled, this value will be the same as the “REDUCE_FIRST” value.
	UnitsAvailableDetails Default;
	// The number of units that may are available to be traded with an Order with a positionFill option of “REDUCE_FIRST”.
	UnitsAvailableDetails reduceFirst;
	// The number of units that may are available to be traded with an Order with a positionFill option of “REDUCE_ONLY”.
	UnitsAvailableDetails reduceOnly;
	// The number of units that may are available to be traded with an Order with a positionFill option of “OPEN_ONLY”.
	UnitsAvailableDetails openOnly;
	//
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
	void Init() {
		Default.Init();
		reduceFirst.Init();
		reduceOnly.Init();
		openOnly.Init();
	}
};

/*
 * The specification of an Account-specific Price.
 */
struct ClientPrice
{	/* PriceStatus: The status of the Price.
	 * возможные значения:
	 * case tradeable: // The Instrument’s price is tradeable.
	 * case non-tradeable: // The Instrument’s price is not tradeable.
	 * case invalid: // The Instrument of the price is invalid or there is no valid Price for the Instrument. */
	PriceStatus status; /* The status of the Price. Deprecated: Will be removed in a future API update. deprecated), */
	// The string “PRICE”. Used to identify the a Price object when found in a stream. default=PRICE),
	string type;
	// The Price’s Instrument.
	InstrumentName instrument;
	// The date/time when the Price was created
	DateTime time;
	// Flag indicating if the Price is tradeable or not
	boolean btradeable;
	/* The list of prices and liquidity available on the Instrument’s bid side. It is possible
	 * for this list to be empty if there is no bid liquidity currently available for the Instrument in the Account. */
	glb::CFxArray<PriceBucket, true> bids;
	/* The list of prices and liquidity available on the Instrument’s ask side. It is possible for this list to be
	 * empty if there is no ask liquidity currently available for the Instrument in the Account. */
	glb::CFxArray<PriceBucket, true> asks;
	/* The closeout bid Price. This Price is used when a bid is required to closeout a Position (margin
	 * closeout or manual) yet there is no bid liquidity. The closeout bid is never used to open a new position. */
	PriceValue closeoutBid;
	/* The closeout ask Price. This Price is used when a ask is required to closeout a Position (margin
	 * closeout or manual) yet there is no ask liquidity. The closeout ask is never used to open a new position. */
	PriceValue closeoutAsk;
	/* The factors used to convert quantities of this price’s Instrument’s quote currency into
	 * a quantity of the Account’s home currency. When the includeHomeConversions is present in
	 * the pricing request (regardless of its value), this field will not be present.
	 * Deprecated: Will be removed in a future API update. */
	QuoteHomeConversionFactors quoteHomeConversionFactors;
	/* Representation of how many units of an Instrument are available to be traded by an Order
	 * depending on its positionFill option.
	 * Deprecated: Will be removed in a future API update. deprecated) */
	UnitsAvailable unitsAvailable;
	//
	ClientPrice();
	void Init();
	int Set(const BYTE *pb, const int cnt);
	int Read(const char *pcsz, const int cnt);
#ifdef LINER_PROJ
	void *m_pOacrl;	// указатель на CInstrShrd::COaCrl
#endif // defined OACRL_LIBRARY
};

/*
 * Details required by clients creating a Guaranteed Stop Loss Order
 */
struct GuaranteedStopLossOrderEntryData
{	// The minimum distance allowed between the Trade’s fill price and the configured price for guaranteed Stop Loss Orders created for this instrument. Specified in price units.
	DecimalNumber minimumDistance;
	// The amount that is charged to the account if a guaranteed Stop Loss Order is triggered and filled. The value is in price units and is charged for each unit of the Trade.
	DecimalNumber premium;
	// The guaranteed Stop Loss Order level restriction for this instrument.
	GuaranteedStopLossOrderLevelRestriction levelRestriction;
};

/*
 * The price data (open, high, low, close) for the Candlestick representation.
 */
struct CandlestickData
{	// The first (open) price in the time-range represented by the candlestick.
	PriceValue o;
	// The highest price in the time-range represented by the candlestick.
	PriceValue h;
	// The lowest price in the time-range represented by the candlestick.
	PriceValue l;
	// The last (closing) price in the time-range represented by the candlestick.
	PriceValue c;
	//
	CandlestickData();
	bool IsValid() const {
		return(o != 0.0 && h != 0.0 && l != 0.0 && c != 0.0);
	}
	bool IsEmpty() const {
		return(o == .0 && h == .0 && l == .0 && c == .0);
	}
	int InvalidElementID() const {
		return ((o == 0.0) ? OA_o : (h == 0.0) ? OA_h : (l == 0.0) ? OA_l : (c == 0.0) ? OA_c : 0);
	}
	void Init() {
		o = h = l = c = 0.0;
	}
	int operator ==(const CandlestickData &cd) const;
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The Candlestick representation
 */
struct Candlestick
{	// The start time of the candlestick
	DateTime time;
	// The candlestick data based on bids. Only provided if bid-based candles were requested.
	CandlestickData bid;
	// The candlestick data based on asks. Only provided if ask-based candles were requested.
	CandlestickData ask;
	// The candlestick data based on midpoints. Only provided if midpoint-based candles were requested.
	CandlestickData mid;
	// The number of prices created during the time-range represented by the candlestick.
	integer volume;
	// A flag indicating if the candlestick is complete. A complete candlestick is one whose ending time is not in the future.
	boolean complete;
	//
	Candlestick() {
		volume = 0;
		complete = false;
	}
	Candlestick(const Candlestick &c) {
		memcpy(this, &c, sizeof *this);
	}
	const Candlestick &operator =(const Candlestick &c) {
		memcpy(this, &c, sizeof *this); // integral...
		return *this;
	}
	void Init() {
		memset(this, 0, sizeof *this);// integlal...
		time.Init();
	}
	bool IsValid() const {
		return(time.IsValid() && (bid.IsValid() || ask.IsValid() || mid.IsValid()) && volume > 0);
	}
	bool IsEmpty() const {
		return (!volume && !complete && !time.IsValid() && bid.IsEmpty() && mid.IsEmpty() && ask.IsEmpty());
	}
	int InvalidElementID() const {
		return(!time.IsValid() ? OA_time : !bid.IsValid() ? OA_bid : !ask.IsValid() ? OA_ask : !mid.IsValid() ? OA_mid : !volume ? OA_volume : 0);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
	/*
	 * Fill записывает цены из массивов pba и pbb в свечу. Такое формирование свечи происходит в ф-ции
	 * чтения ценового потока с сервера CInstrShrd::COaCrl::NewPrice.
	 * - pba: массив цен предложения
	 * - pbb: массив цен спроса
	 * - cnt: к-во эл-тов массивов pba и pbb
	 * - tm: время цены
	 * - bo: если "да", то первая в pba и pbb цена записывается в цену открытия свечи
	 */
	void Fill(const oatp::PriceBucket *pba, const oatp::PriceBucket *pbb, const int cnt, const oatp::dateTime *ptm = nullptr, bool bo = false);
	const Candlestick &operator +=(const Candlestick &c);
};

/*
 * Response containing instrument, granularity, and list of candles.
 */
struct CandlestickDataResponse
{	/* CandlestickGranularity: The granularity of a candlestick
	 * возможные значения:
	 * case S5: // 5 second candlesticks, minute alignment
	 * case S10: // 10 second candlesticks, minute alignment
	 * case S15: // 15 second candlesticks, minute alignment
	 * case S30: // 30 second candlesticks, minute alignment
	 * case M1: // 1 minute candlesticks, minute alignment
	 * case M2: // 2 minute candlesticks, hour alignment
	 * case M4: // 4 minute candlesticks, hour alignment
	 * case M5: // 5 minute candlesticks, hour alignment
	 * case M10: // 10 minute candlesticks, hour alignment
	 * case M15: // 15 minute candlesticks, hour alignment
	 * case M30: // 30 minute candlesticks, hour alignment
	 * case H1: // 1 hour candlesticks, hour alignment
	 * case H2: // 2 hour candlesticks, day alignment
	 * case H3: // 3 hour candlesticks, day alignment
	 * case H4: // 4 hour candlesticks, day alignment
	 * case H6: // 6 hour candlesticks, day alignment
	 * case H8: // 8 hour candlesticks, day alignment
	 * case H12: // 12 hour candlesticks, day alignment
	 * case D: // 1 day candlesticks, day alignment
	 * case W: // 1 week candlesticks, aligned to start of week
	 * case M: // 1 month candlesticks, aligned to first day of the month */
	CandlestickGranularity granularity; /* The granularity of the candlesticks provided: идентификатор из oa_defs.h */
	// The instrument whose Prices are represented by the candlesticks.
	InstrumentName instrument;
	// The list of candlesticks that satisfy the request.
	glb::CFxArray<Candlestick, true> candles;
	//
	CandlestickDataResponse() : candles(256) {
		granularity = 0;
	}
	CandlestickDataResponse(const CandlestickDataResponse &cr) {
		granularity = cr.granularity;
		instrument = cr.instrument;
		candles = cr.candles;
	}
	const CandlestickDataResponse &operator =(const CandlestickDataResponse &cr) {
		granularity = cr.granularity;
		instrument = cr.instrument;
		candles = cr.candles;
		return *this;
	}
	bool IsValid() const {
		return (granularity > 0 && instrument.GetCount() && candles.GetCount());
	}
	int InvalidElementID() const {
		return((granularity <= 0) ? OA_granularity : !instrument.GetCount() ? OA_instrument : !candles.GetCount() ? OA_candles : -1);
	}
	void Init() {
		granularity = -1;
		instrument.SetCountZeroND();
		candles.SetCountZeroND();
	}
#ifdef OACRL_LIBRARY
	int Read(const char *, const int);
#endif // defined OACRL_LIBRARY
};

/*
 * The order book data for a partition of the instrument’s prices.
 */
struct OrderBookBucket
{	// The lowest price (inclusive) covered by the bucket. The bucket covers the price range from the price to price + the order book’s bucketWidth.
	PriceValue price;
	// The percentage of the total number of orders represented by the long orders found in this bucket.
	DecimalNumber longCountPercent;
	// The percentage of the total number of orders represented by the short orders found in this bucket.
	DecimalNumber shortCountPercent;
};

/*
 * The representation of an instrument’s order book at a point in time
 */
struct OrderBook
{	// The order book’s instrument
	InstrumentName instrument;
	// The time when the order book snapshot was created.
	DateTime time;
	// The price (midpoint) for the order book’s instrument at the time of the order book snapshot
	PriceValue price;
	// The price width for each bucket. Each bucket covers the price range from the bucket’s price to the bucket’s price + bucketWidth.
	PriceValue bucketWidth;
	// The partitioned order book, divided into buckets using a default bucket width. These buckets are only provided for price ranges which actually contain order or position data.
	glb::CFxArray<OrderBookBucket, true> buckets;
};

/*
 * The position book data for a partition of the instrument’s prices.
 */
struct PositionBookBucket
{	// The lowest price (inclusive) covered by the bucket. The bucket covers the price range from the price to price + the position book’s bucketWidth.
	PriceValue price;
	// The percentage of the total number of positions represented by the long positions found in this bucket.
	DecimalNumber longCountPercent;
	// The percentage of the total number of positions represented by the short positions found in this bucket.
	DecimalNumber shortCountPercent;
};

/*
 * The representation of an instrument’s position book at a point in time
 */
struct PositionBook
{	// The position book’s instrument
	InstrumentName instrument;
	// The time when the position book snapshot was created
	DateTime time;
	// The price (midpoint) for the position book’s instrument at the time of the position book snapshot
	PriceValue price;
	// The price width for each bucket. Each bucket covers the price range from the bucket’s price to the bucket’s price + bucketWidth.
	PriceValue bucketWidth;
	// The partitioned position book, divided into buckets using a default bucket width. These buckets are only provided for price ranges which actually contain order or position data.
	glb::CFxArray<PositionBookBucket, true> buckets;
};

/*
 * An AccountState Object is used to represent an Account’s current price-dependent state. Price-dependent Account state is dependent on OANDA’s current Prices, and includes things like unrealized PL, NAV and Trailing Stop Loss Order state. Fields will be omitted if their value has not changed since the specified transaction ID.
 */
struct AccountChangesState
{	// The total unrealized profit/loss for all Trades currently open in the Account.
	AccountUnits unrealizedPL;
	// The net asset value of the Account. Equal to Account balance + unrealizedPL.
	AccountUnits Nav;
	// Margin currently used for the Account.
	AccountUnits marginUsed;
	// Margin available for Account currency.
	AccountUnits marginAvailable;
	// The value of the Account’s open positions represented in the Account’s home currency.
	AccountUnits positionValue;
	// The Account’s margin closeout unrealized PL.
	AccountUnits marginCloseoutUnrealizedPL;
	// The Account’s margin closeout NAV.
	AccountUnits marginCloseoutNAV;
	// The Account’s margin closeout margin used.
	AccountUnits marginCloseoutMarginUsed;
	// The Account’s margin closeout percentage. When this value is 1.0 or above the Account is in a margin closeout situation.
	DecimalNumber marginCloseoutPercent;
	// The value of the Account’s open positions as used for margin closeout calculations represented in the Account’s home currency.
	DecimalNumber marginCloseoutPositionValue;
	// The current WithdrawalLimit for the account which will be zero or a positive value indicating how much can be withdrawn from the account.
	AccountUnits withdrawalLimit;
	// The Account’s margin call margin used.
	AccountUnits marginCallMarginUsed;
	// The Account’s margin call percentage. When this value is 1.0 or above the Account is in a margin call situation.
	DecimalNumber marginCallPercent;
	// The current balance of the account.
	AccountUnits balance;
	// The total profit/loss realized over the lifetime of the Account.
	AccountUnits pl;
	// The total realized profit/loss for the account since it was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected over the lifetime of the account.
	AccountUnits financing;
	// The total amount of commission paid over the lifetime of the Account.
	AccountUnits commission;
	// The total amount of dividend adjustment paid over the lifetime of the Account in the Account’s home currency.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders.
	AccountUnits guaranteedExecutionFees;
	// The date/time when the Account entered a margin call state. Only provided if the Account is in a margin call.
	DateTime marginCallEnterTime;
	// The number of times that the Account’s current margin call was extended.
	integer marginCallExtensionCount;
	// The date/time of the Account’s last margin call extension.
	DateTime lastMarginCallExtensionTime;
	// The price-dependent state of each pending Order in the Account.
	glb::CFxArray<DynamicOrderState, true> orders;
	// The price-dependent state for each open Trade in the Account.
	glb::CFxArray<CalculatedTradeState, true> trades;
	// The price-dependent state for each open Position in the Account.
	glb::CFxArray<CalculatedPositionState, false> positions;
};

/*
 * Properties related to an Account.
 */
struct AccountProperties
{	// The Account’s identifier
	AccountID id;
	// The Account’s associated MT4 Account ID. This field will not be present if the Account is not an MT4 account.
	integer mt4AccountID;
	// The Account’s tags
	glb::CFxArray<string, false> tags;
};

/*
 * The current mutability and hedging settings related to guaranteed Stop Loss orders.
 */
struct GuaranteedStopLossOrderParameters
{	/* GuaranteedStopLossOrderMutability: For Accounts that support guaranteed Stop Loss Orders, describes the actions that can be be performed on guaranteed Stop Loss Orders.
	 * возможные значения:
	 * case FIXED: // Once a guaranteed Stop Loss Order has been created it cannot be replaced or cancelled.
	 * case REPLACEABLE: // An existing guaranteed Stop Loss Order can only be replaced, not cancelled.
	 * case CANCELABLE: // Once a guaranteed Stop Loss Order has been created it can be either replaced or cancelled.
	 * case PRICE_WIDEN_ONLY: // An existing guaranteed Stop Loss Order can only be replaced to widen the gap from the current price, not cancelled. */
	GuaranteedStopLossOrderMutability mutabilityMarketOpen; /* The current guaranteed Stop Loss Order mutability setting of the Account when market is open. */
	// The current guaranteed Stop Loss Order mutability setting of the Account when market is halted.
	GuaranteedStopLossOrderMutability mutabilityMarketHalted;
	/*
	 * функцыонал
	 */
	GuaranteedStopLossOrderParameters() {
		mutabilityMarketOpen = mutabilityMarketHalted = 0;
	}
	void Init() {
		mutabilityMarketOpen = mutabilityMarketHalted = 0;
	}
	int InvalidElementID() const {
		return (!mutabilityMarketOpen ? OA_mutability_Market_Open : !mutabilityMarketHalted ? OA_mutability_Market_Halted : 0);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * A summary representation of a client’s Account. The AccountSummary does not provide to full specification of pending Orders, open Trades and Positions. Implemented by: Account
 */
struct AccountSummary
{	/* GuaranteedStopLossOrderMode: The overall behaviour of the Account regarding guaranteed Stop Loss Orders.
	 * возможные значения:
	 * case DISABLED: // The Account is not permitted to create guaranteed Stop Loss Orders.
	 * case ALLOWED: // The Account is able, but not required to have guaranteed Stop Loss Orders for open Trades.
	 * case REQUIRED: // The Account is required to have guaranteed Stop Loss Orders for all open Trades. */
	GuaranteedStopLossOrderMode guaranteedStopLossOrderMode; /* The current guaranteed Stop Loss Order mode of the Account. */
	/* GuaranteedStopLossOrderMutability: For Accounts that support guaranteed Stop Loss Orders, describes the actions that can be be performed on guaranteed Stop Loss Orders.
	 * возможные значения:
	 * case FIXED: // Once a guaranteed Stop Loss Order has been created it cannot be replaced or cancelled.
	 * case REPLACEABLE: // An existing guaranteed Stop Loss Order can only be replaced, not cancelled.
	 * case CANCELABLE: // Once a guaranteed Stop Loss Order has been created it can be either replaced or cancelled.
	 * case PRICE_WIDEN_ONLY: // An existing guaranteed Stop Loss Order can only be replaced to widen the gap from the current price, not cancelled. */
	GuaranteedStopLossOrderMutability guaranteedStopLossOrderMutability; /* The current guaranteed Stop Loss Order mutability setting of the Account. This field will only be present if the guaranteedStopLossOrderMode is not ‘DISABLED’. Deprecated: Will be removed in a future API update. deprecated), */
	// The Account’s identifier
	AccountID id;
	// Client-assigned alias for the Account. Only provided if the Account has an alias set
	string alias;
	// The home currency of the Account
	Currency currency;
	// ID of the user that created the Account.
	integer createdByUserID;
	// The date/time when the Account was created.
	DateTime createdTime;
	// The current guaranteed Stop Loss Order settings of the Account. This field will only be present if the guaranteedStopLossOrderMode is not ‘DISABLED’.
	GuaranteedStopLossOrderParameters guaranteedStopLossOrderParameters;
	// The date/time that the Account’s resettablePL was last reset.
	DateTime resettablePLTime;
	// Client-provided margin rate override for the Account. The effective margin rate of the Account is the lesser of this value and the OANDA margin rate for the Account’s division. This value is only provided if a margin rate override exists for the Account.
	DecimalNumber marginRate;
	// The number of Trades currently open in the Account.
	integer openTradeCount;
	// The number of Positions currently open in the Account.
	integer openPositionCount;
	// The number of Orders currently pending in the Account.
	integer pendingOrderCount;
	// Flag indicating that the Account has hedging enabled.
	boolean hedgingEnabled;
	// The total unrealized profit/loss for all Trades currently open in the Account.
	AccountUnits unrealizedPL;
	// The net asset value of the Account. Equal to Account balance + unrealizedPL.
	AccountUnits Nav;
	// Margin currently used for the Account.
	AccountUnits marginUsed;
	// Margin available for Account currency.
	AccountUnits marginAvailable;
	// The value of the Account’s open positions represented in the Account’s home currency.
	AccountUnits positionValue;
	// The Account’s margin closeout unrealized PL.
	AccountUnits marginCloseoutUnrealizedPL;
	// The Account’s margin closeout NAV.
	AccountUnits marginCloseoutNAV;
	// The Account’s margin closeout margin used.
	AccountUnits marginCloseoutMarginUsed;
	// The Account’s margin closeout percentage. When this value is 1.0 or above the Account is in a margin closeout situation.
	DecimalNumber marginCloseoutPercent;
	// The value of the Account’s open positions as used for margin closeout calculations represented in the Account’s home currency.
	DecimalNumber marginCloseoutPositionValue;
	// The current WithdrawalLimit for the account which will be zero or a positive value indicating how much can be withdrawn from the account.
	AccountUnits withdrawalLimit;
	// The Account’s margin call margin used.
	AccountUnits marginCallMarginUsed;
	// The Account’s margin call percentage. When this value is 1.0 or above the Account is in a margin call situation.
	DecimalNumber marginCallPercent;
	// The current balance of the account.
	AccountUnits balance;
	// The total profit/loss realized over the lifetime of the Account.
	AccountUnits pl;
	// The total realized profit/loss for the account since it was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected over the lifetime of the account.
	AccountUnits financing;
	// The total amount of commission paid over the lifetime of the Account.
	AccountUnits commission;
	// The total amount of dividend adjustment paid over the lifetime of the Account in the Account’s home currency.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders.
	AccountUnits guaranteedExecutionFees;
	// The date/time when the Account entered a margin call state. Only provided if the Account is in a margin call.
	DateTime marginCallEnterTime;
	// The number of times that the Account’s current margin call was extended.
	integer marginCallExtensionCount;
	// The date/time of the Account’s last margin call extension.
	DateTime lastMarginCallExtensionTime;
	// The ID of the last Transaction created for the Account.
	TransactionID lastTransactionID;
};

/*
 * The mutable state of a client’s Account. Implemented by: AccountSummary, AccountChangesState
 */
struct AccumulatedAccountState
{	// The current balance of the account.
	AccountUnits balance;
	// The total profit/loss realized over the lifetime of the Account.
	AccountUnits pl;
	// The total realized profit/loss for the account since it was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected over the lifetime of the account.
	AccountUnits financing;
	// The total amount of commission paid over the lifetime of the Account.
	AccountUnits commission;
	// The total amount of dividend adjustment paid over the lifetime of the Account in the Account’s home currency.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders.
	AccountUnits guaranteedExecutionFees;
	// The date/time when the Account entered a margin call state. Only provided if the Account is in a margin call.
	DateTime marginCallEnterTime;
	// The number of times that the Account’s current margin call was extended.
	integer marginCallExtensionCount;
	// The date/time of the Account’s last margin call extension.
	DateTime lastMarginCallExtensionTime;
};

/*
 * The dynamically calculated state of a client’s Account. Implemented by: AccountSummary, AccountChangesState
 */
struct CalculatedAccountState
{	// The total unrealized profit/loss for all Trades currently open in the Account.
	AccountUnits unrealizedPL;
	// The net asset value of the Account. Equal to Account balance + unrealizedPL.
	AccountUnits Nav;
	// Margin currently used for the Account.
	AccountUnits marginUsed;
	// Margin available for Account currency.
	AccountUnits marginAvailable;
	// The value of the Account’s open positions represented in the Account’s home currency.
	AccountUnits positionValue;
	// The Account’s margin closeout unrealized PL.
	AccountUnits marginCloseoutUnrealizedPL;
	// The Account’s margin closeout NAV.
	AccountUnits marginCloseoutNAV;
	// The Account’s margin closeout margin used.
	AccountUnits marginCloseoutMarginUsed;
	// The Account’s margin closeout percentage. When this value is 1.0 or above the Account is in a margin closeout situation.
	DecimalNumber marginCloseoutPercent;
	// The value of the Account’s open positions as used for margin closeout calculations represented in the Account’s home currency.
	DecimalNumber marginCloseoutPositionValue;
	// The current WithdrawalLimit for the account which will be zero or a positive value indicating how much can be withdrawn from the account.
	AccountUnits withdrawalLimit;
	// The Account’s margin call margin used.
	AccountUnits marginCallMarginUsed;
	// The Account’s margin call percentage. When this value is 1.0 or above the Account is in a margin call situation.
	DecimalNumber marginCallPercent;
};

/*
 * Contains the attributes of a user.
 */
struct UserAttributes
{	// The user’s OANDA-assigned user ID.
	integer userID;
	// The user-provided username.
	string username;
	// The user’s title.
	string title;
	// The user’s name.
	string name;
	// The user’s email address.
	string email;
	// The OANDA division the user belongs to.
	string divisionAbbreviation;
	// The user’s preferred language.
	string languageAbbreviation;
	// The home currency of the Account.
	Currency homeCurrency;
};

/*
 * The base Transaction specification. Specifies properties that are common between all Transaction.
 */
struct Transaction
{	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * A CreateTransaction represents the creation of an Account.
 */
struct CreateTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “CREATE” in a CreateTransaction. default=CREATE), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Division that the Account is in
	integer divisionID;
	// The ID of the Site that the Account was created at
	integer siteID;
	// The ID of the user that the Account was created for
	integer accountUserID;
	// The number of the Account within the site/division/user
	integer accountNumber;
	// The home currency of the Account
	Currency homeCurrency;
};

/*
 * A CloseTransaction represents the closing of an Account.
 */
struct CloseTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “CLOSE” in a CloseTransaction. default=CLOSE) */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * A ReopenTransaction represents the re-opening of a closed Account.
 */
struct ReopenTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “REOPEN” in a ReopenTransaction. default=REOPEN) */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * A ClientConfigureTransaction represents the configuration of an Account by a client.
 */
struct ClientConfigureTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “CLIENT_CONFIGURE” in a ClientConfigureTransaction. default=CLIENT_CONFIGURE), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The client-provided alias for the Account.
	string alias;
	// The margin rate override for the Account.
	DecimalNumber marginRate;
};

/*
 * A ClientConfigureRejectTransaction represents the reject of configuration of an Account by a client.
 */
struct ClientConfigureRejectTransaction
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “CLIENT_CONFIGURE_REJECT” in a ClientConfigureRejectTransaction. default=CLIENT_CONFIGURE_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The client#provided alias for the Account.
	string alias;
	// The margin rate override for the Account.
	DecimalNumber marginRate;
};

/*
 * A TransferFundsTransaction represents the transfer of funds in/out of an Account.
 */
struct TransferFundsTransaction
{	/* FundingReason: The reason that an Account is being funded.
	 * возможные значения:
	 * case OA_Value: // Description
	 * case CLIENT_FUNDING: // The client has initiated a funds transfer
	 * case ACCOUNT_TRANSFER: // Funds are being transferred between two Accounts.
	 * case DIVISION_MIGRATION: // Funds are being transferred as part of a Division migration
	 * case SITE_MIGRATION: // Funds are being transferred as part of a Site migration
	 * case ADJUSTMENT: // Funds are being transferred as part of an Account adjustment */
	FundingReason fundingReason; /* The reason that an Account is being funded. */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRANSFER_FUNDS” in a TransferFundsTransaction. default=TRANSFER_FUNDS), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The amount to deposit/withdraw from the Account in the Account’s home currency. A positive value indicates a deposit, a negative value indicates a withdrawal.
	AccountUnits amount;
	// An optional comment that may be attached to a fund transfer for audit purposes
	string comment;
	// The Account’s balance after funds are transferred.
	AccountUnits accountBalance;
};

/*
 * A TransferFundsRejectTransaction represents the rejection of the transfer of funds in/out of an Account.
 */
struct TransferFundsRejectTransaction
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* FundingReason: The reason that an Account is being funded.
	 * возможные значения:
	 * case OA_Value: // Description
	 * case CLIENT_FUNDING: // The client has initiated a funds transfer
	 * case ACCOUNT_TRANSFER: // Funds are being transferred between two Accounts.
	 * case DIVISION_MIGRATION: // Funds are being transferred as part of a Division migration
	 * case SITE_MIGRATION: // Funds are being transferred as part of a Site migration
	 * case ADJUSTMENT: // Funds are being transferred as part of an Account adjustment */
	FundingReason fundingReason; /* The reason that an Account is being funded. */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRANSFER_FUNDS_REJECT” in a TransferFundsRejectTransaction. default=TRANSFER_FUNDS_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The amount to deposit/withdraw from the Account in the Account’s home currency. A positive value indicates a deposit, a negative value indicates a withdrawal.
	AccountUnits amount;
	// An optional comment that may be attached to a fund transfer for audit purposes
	string comment;
};

/*
 * An OrderCancelTransaction represents the cancellation of an Order in the client’s Account.
 */
struct OrderCancelTransaction
{	/* OrderCancelReason: The reason that an Order was cancelled.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // The Order was cancelled because at the time of filling, an unexpected internal server error occurred.
	 * case ACCOUNT_LOCKED: // The Order was cancelled because at the time of filling the account was locked.
	 * case ACCOUNT_NEW_POSITIONS_LOCKED: // The order was to be filled, however the account is configured to not allow new positions to be created.
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // Filling the Order wasn’t possible because it required the creation of a dependent Order and the Account is locked for Order creation.
	 * case ACCOUNT_ORDER_FILL_LOCKED: // Filling the Order was not possible because the Account is locked for filling Orders.
	 * case CLIENT_REQUEST: // The Order was cancelled explicitly at the request of the client.
	 * case MIGRATION: // The Order cancelled because it is being migrated to another account.
	 * case MARKET_HALTED: // Filling the Order wasn’t possible because the Order’s instrument was halted.
	 * case LINKED_TRADE_CLOSED: // The Order is linked to an open Trade that was closed.
	 * case TIME_IN_FORCE_EXPIRED: // The time in force specified for this order has passed.
	 * case INSUFFICIENT_MARGIN: // Filling the Order wasn’t possible because the Account had insufficient margin.
	 * case FIFO_VIOLATION: // Filling the Order would have resulted in a a FIFO violation.
	 * case BOUNDS_VIOLATION: // Filling the Order would have violated the Order’s price bound.
	 * case CLIENT_REQUEST_REPLACED: // The Order was cancelled for replacement at the request of the client.
	 * case DIVIDEND_ADJUSTMENT_REPLACED: // The Order was cancelled for replacement with an adjusted fillPrice to accommodate for the price movement caused by a dividendAdjustment.
	 * case INSUFFICIENT_LIQUIDITY: // Filling the Order wasn’t possible because enough liquidity available.
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // Filling the Order would have resulted in the creation of a Take Profit Order with a GTD time in the past.
	 * case TAKE_PROFIT_ON_FILL_LOSS: // Filling the Order would result in the creation of a Take Profit Order that would have been filled immediately, closing the new Trade at a loss.
	 * case LOSING_TAKE_PROFIT: // Filling the Order would result in the creation of a Take Profit Loss Order that would close the new Trade at a loss when filled.
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // Filling the Order would have resulted in the creation of a Stop Loss Order with a GTD time in the past.
	 * case STOP_LOSS_ON_FILL_LOSS: // Filling the Order would result in the creation of a Stop Loss Order that would have been filled immediately, closing the new Trade at a loss.
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // Filling the Order would result in the creation of a Stop Loss Order whose price would be zero or negative due to the specified distance.
	 * case STOP_LOSS_ON_FILL_REQUIRED: // Filling the Order would not result in the creation of Stop Loss Order, however the Account’s configuration requires that all Trades have a Stop Loss Order attached to them.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // Filling the Order would not result in the creation of a guaranteed Stop Loss Order, however the Account’s configuration requires that all Trades have a guaranteed Stop Loss Order attached to them.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // Filling the Order would result in the creation of a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // Filling the Order would result in the creation of a guaranteed Stop Loss Order with a distance smaller than the configured minimum distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // Filling the Order would result in the creation of a guaranteed Stop Loss Order with trigger price and number of units that that violates the account’s guaranteed Stop Loss Order level restriction.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_HEDGING_NOT_ALLOWED: // Filling the Order would result in the creation of a guaranteed Stop Loss Order for a hedged Trade, however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // Filling the Order would result in the creation of a Stop Loss Order whose TimeInForce value is invalid. A likely cause would be if the Account requires guaranteed stop loss orders and the TimeInForce value were not GTC.
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // Filling the Order would result in the creation of a Stop Loss Order whose TriggerCondition value is invalid. A likely cause would be if the stop loss order is guaranteed and the TimeInForce is not TRIGGER_DEFAULT or TRIGGER_BID for a long trade, or not TRIGGER_DEFAULT or TRIGGER_ASK for a short trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // Filling the Order would have resulted in the creation of a Guaranteed Stop Loss Order with a GTD time in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LOSS: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order that would have been filled immediately, closing the new Trade at a loss.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order whose price would be zero or negative due to the specified distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // Filling the Order would not result in the creation of a Guaranteed Stop Loss Order, however the Account’s configuration requires that all Trades have a Guaranteed Stop Loss Order attached to them.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with a distance smaller than the configured minimum distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_HEDGING_NOT_ALLOWED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order for a hedged Trade, however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order whose TimeInForce value is invalid. A likely cause would be if the Account requires guaranteed stop loss orders and the TimeInForce value were not GTC.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order whose TriggerCondition value is invalid. A likely cause would be the TimeInForce is not TRIGGER_DEFAULT or TRIGGER_BID for a long trade, or not TRIGGER_DEFAULT or TRIGGER_ASK for a short trade.
	 * case TAKE_PROFIT_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // Filling the Order would result in the creation of a Take Profit Order whose price would be zero or negative due to the specified distance.
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // Filling the Order would have resulted in the creation of a Trailing Stop Loss Order with a GTD time in the past.
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // Filling the Order would result in the creation of a new Open Trade with a client Trade ID already in use.
	 * case POSITION_CLOSEOUT_FAILED: // Closing out a position wasn’t fully possible.
	 * case OPEN_TRADES_ALLOWED_EXCEEDED: // Filling the Order would cause the maximum open trades allowed for the Account to be exceeded.
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Filling the Order would have resulted in exceeding the number of pending Orders allowed for the Account.
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS: // Filling the Order would have resulted in the creation of a Take Profit Order with a client Order ID that is already in use.
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS: // Filling the Order would have resulted in the creation of a Stop Loss Order with a client Order ID that is already in use.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS: // Filling the Order would have resulted in the creation of a Guaranteed Stop Loss Order with a client Order ID that is already in use.
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS: // Filling the Order would have resulted in the creation of a Trailing Stop Loss Order with a client Order ID that is already in use.
	 * case POSITION_SIZE_EXCEEDED: // Filling the Order would have resulted in the Account’s maximum position size limit being exceeded for the Order’s instrument.
	 * case HEDGING_GSLO_VIOLATION: // Filling the Order would result in the creation of a Trade, however there already exists an opposing (hedged) Trade that has a guaranteed Stop Loss Order attached to it. Guaranteed Stop Loss Orders cannot be combined with hedged positions.
	 * case ACCOUNT_POSITION_VALUE_LIMIT_EXCEEDED: // Filling the order would cause the maximum position value allowed for the account to be exceeded. The Order has been cancelled as a result.
	 * case INSTRUMENT_BID_REDUCE_ONLY: // Filling the order would require the creation of a short trade, however the instrument is configured such that orders being filled using bid prices can only reduce existing positions. New short positions cannot be created, but existing long positions may be reduced or closed.
	 * case INSTRUMENT_ASK_REDUCE_ONLY: // Filling the order would require the creation of a long trade, however the instrument is configured such that orders being filled using ask prices can only reduce existing positions. New long positions cannot be created, but existing short positions may be reduced or closed.
	 * case INSTRUMENT_BID_HALTED: // Filling the order would require using the bid, however the instrument is configured such that the bids are halted, and so no short orders may be filled.
	 * case INSTRUMENT_ASK_HALTED: // Filling the order would require using the ask, however the instrument is configured such that the asks are halted, and so no long orders may be filled.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_BID_HALTED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order (GSLO). Since the trade is long the GSLO would be short, however the bid side is currently halted. GSLOs cannot be created in this situation.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_ASK_HALTED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order (GSLO). Since the trade is short the GSLO would be long, however the ask side is currently halted. GSLOs cannot be created in this situation.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_BID_HALTED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order (GSLO). Since the trade is long the GSLO would be short, however the bid side is currently halted. GSLOs cannot be created in this situation.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_ASK_HALTED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order (GSLO). Since the trade is short the GSLO would be long, however the ask side is currently halted. GSLOs cannot be created in this situation.
	 * case FIFO_VIOLATION_SAFEGUARD_VIOLATION: // Filling the Order would have resulted in a new Trade that violates the FIFO violation safeguard constraints.
	 * case FIFO_VIOLATION_SAFEGUARD_PARTIAL_CLOSE_VIOLATION: // Filling the Order would have reduced an existing Trade such that the reduced Trade violates the FIFO violation safeguard constraints.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade. */
	OrderCancelReason reason; /* The reason that the Order was cancelled. */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “ORDER_CANCEL” for an OrderCancelTransaction. default=ORDER_CANCEL), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Order cancelled
	OrderID orderID;
	// The client ID of the Order cancelled (only provided if the Order has a client Order ID).
	OrderID clientOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled for replacement).
	OrderID replacedByOrderID;
};

/*
 * An OrderCancelRejectTransaction represents the rejection of the cancellation of an Order in the client’s Account.
 */
struct OrderCancelRejectTransaction
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “ORDER_CANCEL_REJECT” for an OrderCancelRejectTransaction. default=ORDER_CANCEL_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Order intended to be cancelled
	OrderID orderID;
	// The client ID of the Order intended to be cancelled (only provided if the Order has a client Order ID).
	OrderID clientOrderID;
};

/*
 * A MarginCallEnterTransaction is created when an Account enters the margin call state.
 */
struct MarginCallEnterTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARGIN_CALL_ENTER” for an MarginCallEnterTransaction. default=MARGIN_CALL_ENTER) */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * A MarginCallExtendTransaction is created when the margin call state for an Account has been extended.
 */
struct MarginCallExtendTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARGIN_CALL_EXTEND” for an MarginCallExtendTransaction. default=MARGIN_CALL_EXTEND), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The number of the extensions to the Account’s current margin call that have been applied. This value will be set to 1 for the first MarginCallExtend Transaction
	integer extensionNumber;
};

/*
 * A MarginCallExitTransaction is created when an Account leaves the margin call state.
 */
struct MarginCallExitTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARGIN_CALL_EXIT” for an MarginCallExitTransaction. default=MARGIN_CALL_EXIT) */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * A DelayedTradeClosure Transaction is created administratively to indicate open trades that should have been closed but weren’t because the open trades’ instruments were untradeable at the time. Open trades listed in this transaction will be closed once their respective instruments become tradeable.
 */
struct DelayedTradeClosureTransaction
{	/* MarketOrderReason: The reason that the Market Order was created
	 * возможные значения:
	 * case CLIENT_ORDER: // The Market Order was created at the request of a client
	 * case TRADE_CLOSE: // The Market Order was created to close a Trade at the request of a client
	 * case POSITION_CLOSEOUT: // The Market Order was created to close a Position at the request of a client
	 * case MARGIN_CLOSEOUT: // The Market Order was created as part of a Margin Closeout
	 * case DELAYED_TRADE_CLOSE: // The Market Order was created to close a trade marked for delayed closure */
	MarketOrderReason reason; /* The reason for the delayed trade closure */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “DELAYED_TRADE_CLOSURE” for an DelayedTradeClosureTransaction. default=DELAYED_TRADE_CLOSURE), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// List of Trade ID’s identifying the open trades that will be closed when their respective instruments become tradeable
	TradeID tradeIDs;
};

/*
 * A ResetResettablePLTransaction represents the resetting of the Account’s resettable PL counters.
 */
struct ResetResettablePLTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “RESET_RESETTABLE_PL” for a ResetResettablePLTransaction. default=RESET_RESETTABLE_PL) */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
};

/*
 * Used to pay or collect a dividend adjustment amount for an open Trade within the Account.
 */
struct OpenTradeDividendAdjustment
{	// The ID of the Trade for which the dividend adjustment is to be paid or collected.
	TradeID tradeID;
	// The dividend adjustment amount to pay or collect for the Trade.
	AccountUnits dividendAdjustment;
	// The dividend adjustment amount to pay or collect for the Trade, in the Instrument’s quote currency.
	DecimalNumber quoteDividendAdjustment;
};

/*
 * A DividendAdjustment Transaction is created administratively to pay or collect dividend adjustment mounts to or from an Account.
 */
struct DividendAdjustmentTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “DIVIDEND_ADJUSTMENT” for a DividendAdjustmentTransaction. default=DIVIDEND_ADJUSTMENT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The name of the instrument for the dividendAdjustment transaction
	InstrumentName instrument;
	// The total dividend adjustment amount paid or collected in the Account’s home currency for the Account as a result of applying the DividendAdjustment Transaction. This is the sum of the dividend adjustments paid/collected for each OpenTradeDividendAdjustment found within the Transaction.
	AccountUnits dividendAdjustment;
	// The total dividend adjustment amount paid or collected in the Instrument’s quote currency for the Account as a result of applying the DividendAdjustment Transaction. This is the sum of the quote dividend adjustments paid/collected for each OpenTradeDividendAdjustment found within the Transaction.
	DecimalNumber quoteDividendAdjustment;
	// The HomeConversionFactors in effect at the time of the DividendAdjustment.
	HomeConversionFactors homeConversionFactors;
	// The Account balance after applying the DividendAdjustment Transaction
	AccountUnits accountBalance;
	// The dividend adjustment payment/collection details for each open Trade, within the Account, for which a dividend adjustment is to be paid or collected.
	glb::CFxArray<OpenTradeDividendAdjustment, true> openTradeDividendAdjustments;
};

/*
 * A ClientExtensions object allows a client to attach a clientID, tag and comment to Orders and Trades in their Account.
 * Do not set, modify, or delete this field if your account is associated with MT4.
 */
struct ClientExtensions
{	// The Client ID of the Order/Trade
	ClientID id;
	// A tag associated with the Order/Trade
	ClientTag tag;
	// A comment associated with the Order/Trade
	ClientComment comment;
public:
	/*
	 * функцыонал
	 */
	ClientExtensions() {

	}
	void Init() {
		id.SetCountZeroND();
		tag.SetCountZeroND();
		comment.SetCountZeroND();
	}
	int InvalidElemenetID() const {
		return (!id.GetCount() ? OA_id : !tag.GetCount() ? OA_tag : !comment.GetCount() ? OA_comment : 0);
	}
	int GetBinData(glb::CFxArray<BYTE, true> &a) const;
	int SetBinData(const BYTE *p, const int c);
	// оператор сравнения вернёт 0 если объекты равны, >0 если этот объект больше 'o' и <0 иначе.
	int operator ==(const ClientExtensions &o) const {
		int n;
		return ((n = id == o.id) ? n : (n = tag == o.tag) ? n : comment == o.comment);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The summary of a Trade within an Account. This representation does not provide the full details of the Trade’s dependent Orders.
 */
struct TradeSummary
{	/* TradeState: The current state of the Trade.
	 * возможные значения:
	 * case OPEN: // The Trade is currently open
	 * case CLOSED: // The Trade has been fully closed
	 * case CLOSE_WHEN_TRADEABLE: // The Trade will be closed as soon as the trade’s instrument becomes tradeable */
	TradeState state; /* The current state of the Trade. */
	// The Trade’s identifier, unique within the Trade’s Account.
	TradeID id;
	// The Trade’s Instrument.
	InstrumentName instrument;
	// The execution price of the Trade.
	PriceValue price;
	// The date/time when the Trade was opened.
	DateTime openTime;
	// The initial size of the Trade. Negative values indicate a short Trade, and positive values indicate a long Trade.
	DecimalNumber initialUnits;
	// The margin required at the time the Trade was created. Note, this is the ‘pure’ margin required, it is not the ‘effective’ margin used that factors in the trade risk if a GSLO is attached to the trade.
	AccountUnits initialMarginRequired;
	// The number of units currently open for the Trade. This value is reduced to 0.0 as the Trade is closed.
	DecimalNumber currentUnits;
	// The total profit/loss realized on the closed portion of the Trade.
	AccountUnits realizedPL;
	// The unrealized profit/loss on the open portion of the Trade.
	AccountUnits unrealizedPL;
	// Margin currently used by the Trade.
	AccountUnits marginUsed;
	// The average closing price of the Trade. Only present if the Trade has been closed or reduced at least once.
	PriceValue averageClosePrice;
	// The IDs of the Transactions that have closed portions of this Trade.
	glb::CFxArray<TransactionID, true> closingTransactionIDs;
	// The financing paid/collected for this Trade.
	AccountUnits financing;
	// The dividend adjustment paid for this Trade.
	AccountUnits dividendAdjustment;
	// The date/time when the Trade was fully closed. Only provided for Trades whose state is CLOSED.
	DateTime closeTime;
	// The client extensions of the Trade.
	ClientExtensions clientExtensions;
	// ID of the Trade’s Take Profit Order, only provided if such an Order exists.
	OrderID takeProfitOrderID;
	// ID of the Trade’s Stop Loss Order, only provided if such an Order exists.
	OrderID stopLossOrderID;
	// ID of the Trade’s Guaranteed Stop Loss Order, only provided if such an Order exists.
	OrderID guaranteedStopLossOrderID;
	// ID of the Trade’s Trailing Stop Loss Order, only provided if such an Order exists.
	OrderID trailingStopLossOrderID;
	/*
	 * функцыонал
	 */
	TradeSummary() {
		state = id = takeProfitOrderID = stopLossOrderID = guaranteedStopLossOrderID = trailingStopLossOrderID = 0;
		price = initialUnits = initialMarginRequired = currentUnits = realizedPL = unrealizedPL = marginUsed =
				averageClosePrice = financing = dividendAdjustment = 0.0;
	}
	void Init() {
		state = id = takeProfitOrderID = stopLossOrderID = guaranteedStopLossOrderID = trailingStopLossOrderID = 0;
		price = initialUnits = initialMarginRequired = currentUnits = realizedPL = unrealizedPL = marginUsed =
				averageClosePrice = financing = dividendAdjustment = 0.0;
		openTime.Init();
		closeTime.Init();
		clientExtensions.Init();
		instrument.SetCountZeroND();
		closingTransactionIDs.SetCountZeroND();
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The base Order definition specifies the properties that are common to all Orders.
 * Implemented by:	MarketOrder,
 *					FixedPriceOrder,
 *					LimitOrder,
 *					StopOrder,
 *					MarketIfTouchedOrder,
 * TakeProfitOrder, StopLossOrder, GuaranteedStopLossOrder, TrailingStopLossOrder
 */
struct Order
{	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	/*
	 * функцыонал
	 */
	Order() {
		state = id = 0;
	}
	void Init() {
		state = id = 0;
		createTime.Init();
		clientExtensions.Init();
	}
	int InvalidElementID() const {
		return (!state ? OA_state : !id ? OA_id : !createTime.IsValid() ? OA_create_Time :
																		  clientExtensions.InvalidElemenetID());
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * The full details of a client’s Account. This includes full open Trade, open Position and pending Order representation.
 */
struct Account
{	/* GuaranteedStopLossOrderMode: The overall behaviour of the Account regarding guaranteed Stop Loss Orders.
	 * возможные значения:
	 * case DISABLED: // The Account is not permitted to create guaranteed Stop Loss Orders.
	 * case ALLOWED: // The Account is able, but not required to have guaranteed Stop Loss Orders for open Trades.
	 * case REQUIRED: // The Account is required to have guaranteed Stop Loss Orders for all open Trades. */
	GuaranteedStopLossOrderMode guaranteedStopLossOrderMode; /* The current guaranteed Stop Loss Order mode of the Account. */
	/* GuaranteedStopLossOrderMutability: For Accounts that support guaranteed Stop Loss Orders, describes the actions that can be be performed on guaranteed Stop Loss Orders.
	 * возможные значения:
	 * case FIXED: // Once a guaranteed Stop Loss Order has been created it cannot be replaced or cancelled.
	 * case REPLACEABLE: // An existing guaranteed Stop Loss Order can only be replaced, not cancelled.
	 * case CANCELABLE: // Once a guaranteed Stop Loss Order has been created it can be either replaced or cancelled.
	 * case PRICE_WIDEN_ONLY: // An existing guaranteed Stop Loss Order can only be replaced to widen the gap from the current price, not cancelled. */
	GuaranteedStopLossOrderMutability guaranteedStopLossOrderMutability; /* The current guaranteed Stop Loss Order mutability setting of the Account. This field will only be present if the guaranteedStopLossOrderMode is not ‘DISABLED’. Deprecated: Will be removed in a future API update. deprecated), */
	// The Account’s identifier
	AccountID id;
	// Client-assigned alias for the Account. Only provided if the Account has an alias set
	string alias;
	// The home currency of the Account
	Currency currency;
	// ID of the user that created the Account.
	integer createdByUserID;
	// The date/time when the Account was created.
	DateTime createdTime;
	// The current guaranteed Stop Loss Order settings of the Account. This field will only be present if the guaranteedStopLossOrderMode is not ‘DISABLED’.
	GuaranteedStopLossOrderParameters guaranteedStopLossOrderParameters;
	// The date/time that the Account’s resettablePL was last reset.
	DateTime resettablePLTime;
	// Client-provided margin rate override for the Account. The effective margin rate of the Account is the lesser of this value and the OANDA margin rate for the Account’s division. This value is only provided if a margin rate override exists for the Account.
	DecimalNumber marginRate;
	// The number of Trades currently open in the Account.
	integer openTradeCount;
	// The number of Positions currently open in the Account.
	integer openPositionCount;
	// The number of Orders currently pending in the Account.
	integer pendingOrderCount;
	// Flag indicating that the Account has hedging enabled.
	boolean hedgingEnabled;
	// The total unrealized profit/loss for all Trades currently open in the Account.
	AccountUnits unrealizedPL;
	// The net asset value of the Account. Equal to Account balance + unrealizedPL.
	AccountUnits Nav;
	// Margin currently used for the Account.
	AccountUnits marginUsed;
	// Margin available for Account currency.
	AccountUnits marginAvailable;
	// The value of the Account’s open positions represented in the Account’s home currency.
	AccountUnits positionValue;
	// The Account’s margin closeout unrealized PL.
	AccountUnits marginCloseoutUnrealizedPL;
	// The Account’s margin closeout NAV.
	AccountUnits marginCloseoutNAV;
	// The Account’s margin closeout margin used.
	AccountUnits marginCloseoutMarginUsed;
	// The Account’s margin closeout percentage. When this value is 1.0 or above the Account is in a margin closeout situation.
	DecimalNumber marginCloseoutPercent;
	// The value of the Account’s open positions as used for margin closeout calculations represented in the Account’s home currency.
	DecimalNumber marginCloseoutPositionValue;
	// The current WithdrawalLimit for the account which will be zero or a positive value indicating how much can be withdrawn from the account.
	AccountUnits withdrawalLimit;
	// The Account’s margin call margin used.
	AccountUnits marginCallMarginUsed;
	// The Account’s margin call percentage. When this value is 1.0 or above the Account is in a margin call situation.
	DecimalNumber marginCallPercent;
	// The current balance of the account.
	AccountUnits balance;
	// The total profit/loss realized over the lifetime of the Account.
	AccountUnits pl;
	// The total realized profit/loss for the account since it was last reset by the client.
	AccountUnits resettablePL;
	// The total amount of financing paid/collected over the lifetime of the account.
	AccountUnits financing;
	// The total amount of commission paid over the lifetime of the Account.
	AccountUnits commission;
	// The total amount of dividend adjustment paid over the lifetime of the Account in the Account’s home currency.
	AccountUnits dividendAdjustment;
	// The total amount of fees charged over the lifetime of the Account for the execution of guaranteed Stop Loss Orders.
	AccountUnits guaranteedExecutionFees;
	// The date/time when the Account entered a margin call state. Only provided if the Account is in a margin call.
	DateTime marginCallEnterTime;
	// The number of times that the Account’s current margin call was extended.
	integer marginCallExtensionCount;
	// The date/time of the Account’s last margin call extension.
	DateTime lastMarginCallExtensionTime;
	// The ID of the last Transaction created for the Account.
	TransactionID lastTransactionID;
	// The details of the Trades currently open in the Account.
	glb::CFxArray<TradeSummary, false> trades;
	// The details all Account Positions.
	glb::CFxArray<Position, false> positions;
	// The details of the Orders currently pending in the Account.
	glb::CFxArray<Order, false> orders;
	/*
	 * функцыонал
	 */
	Account() : trades(4), positions(4), orders(4) {
		guaranteedStopLossOrderMode = guaranteedStopLossOrderMutability = createdByUserID = openTradeCount =
			openPositionCount = pendingOrderCount = marginCallExtensionCount = 0;
		marginRate = unrealizedPL = Nav = marginUsed = marginAvailable = positionValue = marginCloseoutUnrealizedPL =
			marginCloseoutNAV = marginCloseoutMarginUsed = marginCloseoutPercent = marginCloseoutPositionValue =
			withdrawalLimit = marginCallMarginUsed = marginCallPercent = balance = pl = resettablePL = financing =
			commission = dividendAdjustment = guaranteedExecutionFees = 0.0;
		hedgingEnabled = false;
	}
	/* Иницыацыя! Не забудь вызвать перед чтением...
	 */
	void Init() {
		guaranteedStopLossOrderMode = guaranteedStopLossOrderMutability = createdByUserID = openTradeCount =
			openPositionCount = pendingOrderCount = marginCallExtensionCount = lastTransactionID = 0;
		marginRate = unrealizedPL = Nav = marginUsed = marginAvailable = positionValue = marginCloseoutUnrealizedPL =
			marginCloseoutNAV = marginCloseoutMarginUsed = marginCloseoutPercent = marginCloseoutPositionValue =
			withdrawalLimit = marginCallMarginUsed = marginCallPercent = balance = pl = resettablePL = financing =
			commission = dividendAdjustment = guaranteedExecutionFees = 0.0;
		hedgingEnabled = false;
		id.SetCountZeroND();
		alias.SetCountZeroND();
		currency.SetCountZeroND();
		createdTime.Init();
		guaranteedStopLossOrderParameters.Init();
		resettablePLTime.Init();
		marginCallEnterTime.Init();
		lastMarginCallExtensionTime.Init();
		trades.SetCountZeroND();
		positions.SetCountZeroND();
		orders.SetCountZeroND();
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * An AccountChanges Object is used to represent the changes to an Account’s Orders, Trades and Positions since a specified Account TransactionID in the past.
 */
struct AccountChanges
{	// The Orders created. These Orders may have been filled, cancelled or triggered in the same period.
	glb::CFxArray<Order, false> ordersCreated;
	// The Orders cancelled.
	glb::CFxArray<Order, false> ordersCancelled;
	// The Orders filled.
	glb::CFxArray<Order, false> ordersFilled;
	// The Orders triggered.
	glb::CFxArray<Order, false> ordersTriggered;
	// The Trades opened.
	glb::CFxArray<TradeSummary, false> tradesOpened;
	// The Trades reduced.
	glb::CFxArray<TradeSummary, false> tradesReduced;
	// The Trades closed.
	glb::CFxArray<TradeSummary, false> tradesClosed;
	// The Positions changed.
	glb::CFxArray<Position, false> positions;
	// The Transactions that have been generated.
	glb::CFxArray<Transaction, false> transactions;
};

/*
 * A TakeProfitOrder is an order that is linked to an open Trade and created with a price threshold. The Order will be filled (closing the Trade) by the first price that is equal to or better than the threshold. A TakeProfitOrder cannot be used to open a new Position.
 */
struct TakeProfitOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “TAKE_PROFIT” for Take Profit Orders. default=TAKE_PROFIT), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TakeProfit Order. Restricted to “GTC”, “GFD” and “GTD” for TakeProfit Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the TakeProfit Order. The associated Trade will be closed by a market price that is equal to or better than this threshold. required),
	PriceValue price;
	// The date/time when the TakeProfit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A StopLossOrder is an order that is linked to an open Trade and created with a price threshold. The Order will be filled (closing the Trade) by the first price that is equal to or worse than the threshold. A StopLossOrder cannot be used to open a new Position.
 */
struct StopLossOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “STOP_LOSS” for Stop Loss Orders. default=STOP_LOSS), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the StopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for StopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The premium that will be charged if the Stop Loss Order is guaranteed and the Order is filled at the guaranteed price. It is in price units and is charged for each unit of the Trade. Deprecated: Will be removed in a future API update. deprecated),
	DecimalNumber guaranteedExecutionPremium;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Stop Loss Order. The associated Trade will be closed by a market price that is equal to or worse than this threshold. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Flag indicating that the Stop Loss Order is guaranteed. The default value depends on the GuaranteedStopLossOrderMode of the account, if it is REQUIRED, the default will be true, for DISABLED or ENABLED the default is false. Deprecated: Will be removed in a future API update. deprecated),
	boolean guaranteed;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A GuaranteedStopLossOrder is an order that is linked to an open Trade and created with a price threshold which is guaranteed against slippage that may occur as the market crosses the price set for that order. The Order will be filled (closing the Trade) by the first price that is equal to or worse than the threshold. The price level specified for the GuaranteedStopLossOrder must be at least the configured minimum distance (in price units) away from the entry price for the traded instrument. A GuaranteedStopLossOrder cannot be used to open a new Position.
 */
struct GuaranteedStopLossOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “GUARANTEED_STOP_LOSS” for Guaranteed Stop Loss Orders. default=GUARANTEED_STOP_LOSS), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the GuaranteedStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for GuaranteedStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The premium that will be charged if the Guaranteed Stop Loss Order is filled at the guaranteed price. It is in price units and is charged for each unit of the Trade.
	DecimalNumber guaranteedExecutionPremium;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Guaranteed Stop Loss Order. The associated Trade will be closed at this price. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Guaranteed Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the GuaranteedStopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A TrailingStopLossOrder is an order that is linked to an open Trade and created with a price distance. The price distance is used to calculate a trailing stop value for the order that is in the losing direction from the market price at the time of the order’s creation. The trailing stop value will follow the market price as it moves in the winning direction, and the order will filled (closing the Trade) by the first price that is equal to or worse than the trailing stop value. A TrailingStopLossOrder cannot be used to open a new Position.
 */
struct TrailingStopLossOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “TRAILING_STOP_LOSS” for Trailing Stop Loss Orders. default=TRAILING_STOP_LOSS), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TrailingStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for TrailingStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price distance (in price units) specified for the TrailingStopLoss Order. required),
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	/* The trigger price for the Trailing Stop Loss Order. The trailing stop value will trail (follow) the
	 * market price by the TSL order’s configured “distance” as the market price moves in the winning direction.
	 * If the market price moves to a level that is equal to or worse than the trailing stop value, the order
	 * will be filled and the Trade will be closed.*/
	PriceValue trailingStopValue;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * The specification of a Trade within an Account. This includes the full representation of the Trade’s
 * dependent Orders in addition to the IDs of those Orders.
 */
struct Trade
{	/* TradeState: The current state of the Trade.
	 * возможные значения:
	 * case OPEN: // The Trade is currently open
	 * case CLOSED: // The Trade has been fully closed
	 * case CLOSE_WHEN_TRADEABLE: // The Trade will be closed as soon as the trade’s instrument becomes tradeable */
	TradeState state; /* The current state of the Trade. */
	// The Trade’s identifier, unique within the Trade’s Account.
	TradeID id;
	// The Trade’s Instrument.
	InstrumentName instrument;
	// The execution price of the Trade.
	PriceValue price;
	// The date/time when the Trade was opened.
	DateTime openTime;
	// The initial size of the Trade. Negative values indicate a short Trade, and positive values indicate a long Trade.
	DecimalNumber initialUnits;
	// The margin required at the time the Trade was created. Note, this is the ‘pure’ margin required, it is not the ‘effective’ margin used that factors in the trade risk if a GSLO is attached to the trade.
	AccountUnits initialMarginRequired;
	// The number of units currently open for the Trade. This value is reduced to 0.0 as the Trade is closed.
	DecimalNumber currentUnits;
	// The total profit/loss realized on the closed portion of the Trade.
	AccountUnits realizedPL;
	// The unrealized profit/loss on the open portion of the Trade.
	AccountUnits unrealizedPL;
	// Margin currently used by the Trade.
	AccountUnits marginUsed;
	// The average closing price of the Trade. Only present if the Trade has been closed or reduced at least once.
	PriceValue averageClosePrice;
	// The IDs of the Transactions that have closed portions of this Trade.
	glb::CFxArray<TransactionID, true> closingTransactionIDs;
	// The financing paid/collected for this Trade.
	AccountUnits financing;
	// The dividend adjustment paid for this Trade.
	AccountUnits dividendAdjustment;
	// The date/time when the Trade was fully closed. Only provided for Trades whose state is CLOSED.
	DateTime closeTime;
	// The client extensions of the Trade.
	ClientExtensions clientExtensions;
	// Full representation of the Trade’s Take Profit Order, only provided if such an Order exists.
	TakeProfitOrder takeProfitOrder;
	// Full representation of the Trade’s Stop Loss Order, only provided if such an Order exists.
	StopLossOrder stopLossOrder;
	// Full representation of the Trade’s Trailing Stop Loss Order, only provided if such an Order exists.
	TrailingStopLossOrder trailingStopLossOrder;
};

/*
 * A TakeProfitOrderRequest specifies the parameters that may be set when creating a Take Profit Order.
 */
struct TakeProfitOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “TAKE_PROFIT” when creating a Take Profit Order. default=TAKE_PROFIT), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TakeProfit Order. Restricted to “GTC”, “GFD” and “GTD” for TakeProfit Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the TakeProfit Order. The associated Trade will be closed by a market price that is equal to or better than this threshold. required),
	PriceValue price;
	// The date/time when the TakeProfit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
};

/*
 * A StopLossOrderRequest specifies the parameters that may be set when creating a Stop Loss Order. Only one of the price and distance fields may be specified.
 */
struct StopLossOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “STOP_LOSS” when creating a Stop Loss Order. default=STOP_LOSS), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the StopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for StopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Stop Loss Order. The associated Trade will be closed by a market price that is equal to or worse than this threshold. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Flag indicating that the Stop Loss Order is guaranteed. The default value depends on the GuaranteedStopLossOrderMode of the account, if it is REQUIRED, the default will be true, for DISABLED or ENABLED the default is false. Deprecated: Will be removed in a future API update. deprecated),
	boolean guaranteed;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
};

/*
 * A GuaranteedStopLossOrderRequest specifies the parameters that may be set when creating a Guaranteed Stop Loss Order. Only one of the price and distance fields may be specified.
 */
struct GuaranteedStopLossOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “GUARANTEED_STOP_LOSS” when creating a Guaranteed Stop Loss Order. default=GUARANTEED_STOP_LOSS), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the GuaranteedStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for GuaranteedStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Guaranteed Stop Loss Order. The associated Trade will be closed at this price. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Guaranteed Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the GuaranteedStopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
};

/*
 * A TrailingStopLossOrderRequest specifies the parameters that may be set when creating a Trailing Stop Loss Order.
 */
struct TrailingStopLossOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “TRAILING_STOP_LOSS” when creating a Trailing Stop Loss Order. default=TRAILING_STOP_LOSS), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TrailingStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for TrailingStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price distance (in price units) specified for the TrailingStopLoss Order. required),
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
};

/*
 * A TakeProfitOrderTransaction represents the creation of a TakeProfit Order in the user’s Account.
 */
struct TakeProfitOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TakeProfit Order. Restricted to “GTC”, “GFD” and “GTD” for TakeProfit Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TakeProfitOrderReason: The reason that the Take Profit Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Take Profit Order was initiated at the request of a client
	 * case REPLACEMENT: // The Take Profit Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Take Profit Order was initiated automatically when an Order was filled that opened a new Trade requiring a Take Profit Order. */
	TakeProfitOrderReason reason; /* The reason that the Take Profit Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TAKE_PROFIT_ORDER” in a TakeProfitOrderTransaction. default=TAKE_PROFIT_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the TakeProfit Order. The associated Trade will be closed by a market price that is equal to or better than this threshold. required),
	PriceValue price;
	// The date/time when the TakeProfit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A TakeProfitOrderRejectTransaction represents the rejection of the creation of a TakeProfit Order.
 */
struct TakeProfitOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TakeProfit Order. Restricted to “GTC”, “GFD” and “GTD” for TakeProfit Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TakeProfitOrderReason: The reason that the Take Profit Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Take Profit Order was initiated at the request of a client
	 * case REPLACEMENT: // The Take Profit Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Take Profit Order was initiated automatically when an Order was filled that opened a new Trade requiring a Take Profit Order. */
	TakeProfitOrderReason reason; /* The reason that the Take Profit Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TAKE_PROFIT_ORDER_REJECT” in a TakeProfitOrderRejectTransaction. default=TAKE_PROFIT_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the TakeProfit Order. The associated Trade will be closed by a market price that is equal to or better than this threshold. required),
	PriceValue price;
	// The date/time when the TakeProfit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A StopLossOrderTransaction represents the creation of a StopLoss Order in the user’s Account.
 */
struct StopLossOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the StopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for StopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* StopLossOrderReason: The reason that the Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Stop Loss Order. */
	StopLossOrderReason reason; /* The reason that the Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “STOP_LOSS_ORDER” in a StopLossOrderTransaction. default=STOP_LOSS_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Stop Loss Order. The associated Trade will be closed by a market price that is equal to or worse than this threshold. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Flag indicating that the Stop Loss Order is guaranteed. The default value depends on the GuaranteedStopLossOrderMode of the account, if it is REQUIRED, the default will be true, for DISABLED or ENABLED the default is false. Deprecated: Will be removed in a future API update. deprecated),
	boolean guaranteed;
	// The fee that will be charged if the Stop Loss Order is guaranteed and the Order is filled at the guaranteed price. The value is determined at Order creation time. It is in price units and is charged for each unit of the Trade. Deprecated: Will be removed in a future API update. deprecated),
	DecimalNumber guaranteedExecutionPremium;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A StopLossOrderRejectTransaction represents the rejection of the creation of a StopLoss Order.
 */
struct StopLossOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the StopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for StopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* StopLossOrderReason: The reason that the Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Stop Loss Order. */
	StopLossOrderReason reason; /* The reason that the Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “STOP_LOSS_ORDER_REJECT” in a StopLossOrderRejectTransaction. default=STOP_LOSS_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Stop Loss Order. The associated Trade will be closed by a market price that is equal to or worse than this threshold. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Flag indicating that the Stop Loss Order is guaranteed. The default value depends on the GuaranteedStopLossOrderMode of the account, if it is REQUIRED, the default will be true, for DISABLED or ENABLED the default is false. Deprecated: Will be removed in a future API update. deprecated),
	boolean guaranteed;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A GuaranteedStopLossOrderTransaction represents the creation of a GuaranteedStopLoss Order in the user’s Account.
 */
struct GuaranteedStopLossOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the GuaranteedStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for GuaranteedStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* GuaranteedStopLossOrderReason: The reason that the Guaranteed Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Guaranteed Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Guaranteed Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Guaranteed Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Guaranteed Stop Loss Order. */
	GuaranteedStopLossOrderReason reason; /* The reason that the Guaranteed Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “GUARANTEED_STOP_LOSS_ORDER” in a GuaranteedStopLossOrderTransaction. default=GUARANTEED_STOP_LOSS_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Guaranteed Stop Loss Order. The associated Trade will be closed at this price. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Guaranteed Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the GuaranteedStopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The fee that will be charged if the Guaranteed Stop Loss Order is filled at the guaranteed price. The value is determined at Order creation time. It is in price units and is charged for each unit of the Trade.
	DecimalNumber guaranteedExecutionPremium;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A GuaranteedStopLossOrderRejectTransaction represents the rejection of the creation of a GuaranteedStopLoss Order.
 */
struct GuaranteedStopLossOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the GuaranteedStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for GuaranteedStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* GuaranteedStopLossOrderReason: The reason that the Guaranteed Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Guaranteed Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Guaranteed Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Guaranteed Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Guaranteed Stop Loss Order. */
	GuaranteedStopLossOrderReason reason; /* The reason that the Guaranteed Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “GUARANTEED_STOP_LOSS_ORDER_REJECT” in a GuaranteedStopLossOrderRejectTransaction. default=GUARANTEED_STOP_LOSS_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price threshold specified for the Guaranteed Stop Loss Order. The associated Trade will be closed at this price. required),
	PriceValue price;
	// Specifies the distance (in price units) from the Account’s current price to use as the Guaranteed Stop Loss Order price. If the Trade is short the Instrument’s bid price is used, and for long Trades the ask is used.
	DecimalNumber distance;
	// The date/time when the GuaranteedStopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A TrailingStopLossOrderTransaction represents the creation of a TrailingStopLoss Order in the user’s Account.
 */
struct TrailingStopLossOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TrailingStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for TrailingStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TrailingStopLossOrderReason: The reason that the Trailing Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Trailing Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Trailing Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Trailing Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Trailing Stop Loss Order. */
	TrailingStopLossOrderReason reason; /* The reason that the Trailing Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRAILING_STOP_LOSS_ORDER” in a TrailingStopLossOrderTransaction. default=TRAILING_STOP_LOSS_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price distance (in price units) specified for the TrailingStopLoss Order. required),
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A TrailingStopLossOrderRejectTransaction represents the rejection of the creation of a TrailingStopLoss Order.
 */
struct TrailingStopLossOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the TrailingStopLoss Order. Restricted to “GTC”, “GFD” and “GTD” for TrailingStopLoss Orders. required, default=GTC), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TrailingStopLossOrderReason: The reason that the Trailing Stop Loss Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Trailing Stop Loss Order was initiated at the request of a client
	 * case REPLACEMENT: // The Trailing Stop Loss Order was initiated as a replacement for an existing Order
	 * case ON_FILL: // The Trailing Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Trailing Stop Loss Order. */
	TrailingStopLossOrderReason reason; /* The reason that the Trailing Stop Loss Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRAILING_STOP_LOSS_ORDER_REJECT” in a TrailingStopLossOrderRejectTransaction. default=TRAILING_STOP_LOSS_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade to close when the price threshold is breached. required),
	TradeID tradeID;
	// The client ID of the Trade to be closed when the price threshold is breached.
	ClientID clientTradeID;
	// The price distance (in price units) specified for the TrailingStopLoss Order. required),
	DecimalNumber distance;
	// The date/time when the StopLoss Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The ID of the OrderFill Transaction that caused this Order to be created (only provided if this Order was created automatically when another Order was filled).
	TransactionID orderFillTransactionID;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A OrderClientExtensionsModifyTransaction represents the modification of an Order’s Client Extensions.
 */
struct OrderClientExtensionsModifyTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “ORDER_CLIENT_EXTENSIONS_MODIFY” for a OrderClientExtensionsModifyTransaction. default=ORDER_CLIENT_EXTENSIONS_MODIFY), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Order who’s client extensions are to be modified.
	OrderID orderID;
	// The original Client ID of the Order who’s client extensions are to be modified.
	ClientID clientOrderID;
	// The new Client Extensions for the Order.
	ClientExtensions clientExtensionsModify;
	// The new Client Extensions for the Order’s Trade on fill.
	ClientExtensions tradeClientExtensionsModify;
};

/*
 * A OrderClientExtensionsModifyRejectTransaction represents the rejection of the modification of an Order’s Client Extensions.
 */
struct OrderClientExtensionsModifyRejectTransaction
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT” for a OrderClientExtensionsModifyRejectTransaction. default=ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Order who’s client extensions are to be modified.
	OrderID orderID;
	// The original Client ID of the Order who’s client extensions are to be modified.
	ClientID clientOrderID;
	// The new Client Extensions for the Order.
	ClientExtensions clientExtensionsModify;
	// The new Client Extensions for the Order’s Trade on fill.
	ClientExtensions tradeClientExtensionsModify;
};

/*
 * A TradeClientExtensionsModifyTransaction represents the modification of a Trade’s Client Extensions.
 */
struct TradeClientExtensionsModifyTransaction
{	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRADE_CLIENT_EXTENSIONS_MODIFY” for a TradeClientExtensionsModifyTransaction. default=TRADE_CLIENT_EXTENSIONS_MODIFY), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade who’s client extensions are to be modified.
	TradeID tradeID;
	// The original Client ID of the Trade who’s client extensions are to be modified.
	ClientID clientTradeID;
	// The new Client Extensions for the Trade.
	ClientExtensions tradeClientExtensionsModify;
};

/*
 * A TradeClientExtensionsModifyRejectTransaction represents the rejection of the modification of a Trade’s Client Extensions.
 */
struct TradeClientExtensionsModifyRejectTransaction
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT” for a TradeClientExtensionsModifyRejectTransaction. default=TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Trade who’s client extensions are to be modified.
	TradeID tradeID;
	// The original Client ID of the Trade who’s client extensions are to be modified.
	ClientID clientTradeID;
	// The new Client Extensions for the Trade.
	ClientExtensions tradeClientExtensionsModify;
};

/*
 * TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client.
 * This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s
 * dependent Take Profit Order is modified directly through the Trade.
 */
struct TakeProfitDetails
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should
	 * remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time in force for the created Take Profit Order. This may only be GTC, GTD or GFD. default=GTC), */
	// The price that the Take Profit Order will be triggered at. Only one of the price and distance fields may be specified.
	PriceValue price;
	// The date when the Take Profit Order will be cancelled on if timeInForce is GTD.
	DateTime gtdTime;
	// The Client Extensions to add to the Take Profit Order when created.
	ClientExtensions clientExtensions;
public:
	TakeProfitDetails() {
		timeInForce = 0;
		price = 0.0;
	}
	void Init() {
		timeInForce = 0;
		price = 0.0;
		gtdTime.Init();
		clientExtensions.Init();
	}
	int GetBinData(glb::CFxArray<BYTE, true> &a) const;
	int SetBinData(const BYTE *p, const int cnt);
	// cmp вернёт 0 если объекты равны, -1 если этот меньше 'o' и 1 иначе.
	int cmp(const TakeProfitDetails &o, double eps) const {
		int n;
		return ((timeInForce < o.timeInForce) ? -1 : (timeInForce > o.timeInForce) ? 1 :
				DCMP_LESS(price, o.price, eps) ? -1 : DCMP_MORE(price, o.price, eps) ? 1 :
				(n = gtdTime == o.gtdTime) ? n : clientExtensions == o.clientExtensions);
	}
};

/*
 * StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may
 * happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop
 * Loss Order is modified directly through the Trade.
 */
struct StopLossDetails
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain
	 * pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time in force for the created Stop Loss Order. This may only be GTC, GTD or GFD. default=GTC), */
	// The price that the Stop Loss Order will be triggered at. Only one of the price and distance fields may be specified.
	PriceValue price;
	/* Specifies the distance (in price units) from the Trade’s open price to use as the Stop Loss Order price.
	 * Only one of the distance and price fields may be specified. */
	DecimalNumber distance;
	// The date when the Stop Loss Order will be cancelled on if timeInForce is GTD.
	DateTime gtdTime;
	// The Client Extensions to add to the Stop Loss Order when created.
	ClientExtensions clientExtensions;
	// Flag indicating that the price for the Stop Loss Order is guaranteed. The default value depends on the GuaranteedStopLossOrderMode of the account, if it is REQUIRED, the default will be true, for DISABLED or ENABLED the default is false. Deprecated: Will be removed in a future API update. deprecated)
	boolean guaranteed;
public:
	StopLossDetails() {
		timeInForce = 0;
		price = distance = 0.0;
		guaranteed = false;
	}
	void Init() {
		timeInForce = 0;
		price = distance = 0.0;
		gtdTime.Init();
		clientExtensions.Init();
		guaranteed = false;
	}
	int GetBinData(glb::CFxArray<BYTE, true> &a) const;
	int SetBinData(const BYTE *p, const int cnt);
	// cmp вернёт 0 если объекты равны, -1 если этот меньше 'o' и 1 иначе.
	int cmp(const StopLossDetails &o, double eps) const {
		int n;
		return ((timeInForce < o.timeInForce) ? -1 : (timeInForce > o.timeInForce) ? 1 :
				(guaranteed < o.guaranteed) ? -1 : (guaranteed > o.guaranteed) ? 1 :
				DCMP_LESS(price, o.price, eps) ? -1 : DCMP_MORE(price, o.price, eps) ? 1 :
				DCMP_LESS(distance, o.distance, eps) ? -1 : DCMP_MORE(distance, o.distance, eps) ? 1 :
				(n = gtdTime == o.gtdTime) ? n : clientExtensions == o.clientExtensions);
	}
};

/*
 * GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of
 * a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or
 * when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
 */
struct GuaranteedStopLossDetails
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time in force for the created Guaranteed Stop Loss Order. This may only be GTC, GTD or GFD. default=GTC), */
	// The price that the Guaranteed Stop Loss Order will be triggered at. Only one of the price and distance fields may be specified.
	PriceValue price;
	// Specifies the distance (in price units) from the Trade’s open price to use as the Guaranteed Stop Loss Order price. Only one of the distance and price fields may be specified.
	DecimalNumber distance;
	// The date when the Guaranteed Stop Loss Order will be cancelled on if timeInForce is GTD.
	DateTime gtdTime;
	// The Client Extensions to add to the Guaranteed Stop Loss Order when created.
	ClientExtensions clientExtensions;
public:
	GuaranteedStopLossDetails() {
		timeInForce = 0;
		price = distance = 0.0;
	}
	void Init() {
		timeInForce = 0;
		price = distance = 0.0;
		gtdTime.Init();
		clientExtensions.Init();
	}
	int GetBinData(glb::CFxArray<BYTE, true> &a) const;
	int SetBinData(const BYTE *p, const int cnt);
	// cmp вернёт 0 если объекты равны, -1 если этот меньше 'o' и 1 иначе.
	int cmp(const GuaranteedStopLossDetails &o, double eps) const {
		int n;
		return ((timeInForce < o.timeInForce) ? -1 : (timeInForce > o.timeInForce) ? 1 :
				DCMP_LESS(price, o.price, eps) ? -1 : DCMP_MORE(price, o.price, eps) ? 1 :
				DCMP_LESS(distance, o.distance, eps) ? -1 : DCMP_MORE(distance, o.distance, eps) ? 1 :
				(n = gtdTime == o.gtdTime) ? n : clientExtensions == o.clientExtensions);
	}
};

/*
 * TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client.
 * This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s
 * dependent Trailing Stop Loss Order is modified directly through the Trade.
 * Структура идентичная TakeProfitDetails, поэтому просто псевдоним объявлю.
 */
typedef TakeProfitDetails TrailingStopLossDetails;
/*struct TrailingStopLossDetails : public TakeProfitDetails
{	* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
//	TimeInForce timeInForce; /* The time in force for the created Trailing Stop Loss Order. This may only be GTC, GTD or GFD. default=GTC), */
	// The distance (in price units) from the Trade’s fill price that the Trailing Stop Loss Order will be triggered at.
//	DecimalNumber distance;
	// The date when the Trailing Stop Loss Order will be cancelled on if timeInForce is GTD.
//	DateTime gtdTime;
	// The Client Extensions to add to the Trailing Stop Loss Order when created.
//	ClientExtensions clientExtensions;
/*public:
	TrailingStopLossDetails() {
		timeInForce = 0;
		distance = 0.0;
	}
	void Init() {
		timeInForce = 0;
		distance = 0.0;
		gtdTime.Init();
		clientExtensions.Init();
	}
};*/

/*
 * A FixedPriceOrder is an order that is filled immediately upon creation using a fixed price.
 */
struct FixedPriceOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “FIXED_PRICE” for Fixed Price Orders. default=FIXED_PRICE), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The Fixed Price Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Fixed Price Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price specified for the Fixed Price Order. This price is the exact price that the Fixed Price Order will be filled at. required),
	PriceValue price;
	// The state that the trade resulting from the Fixed Price Order should be set to. required),
	string tradeState;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
};

/*
 * A LimitOrder is an order that is created with a price threshold, and will only be filled by a price that is equal to or better than the threshold.
 */
struct LimitOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “LIMIT” for Limit Orders. default=LIMIT), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Limit Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The Limit Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Limit Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Limit Order. The Limit Order will only be filled by a market price that is equal to or better than this price. required),
	PriceValue price;
	// The date/time when the Limit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A StopOrder is an order that is created with a price threshold, and will only be filled by a price that is equal to or worse than the threshold.
 */
struct StopOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “STOP” for Stop Orders. default=STOP), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Stop Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The Stop Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Stop Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Stop Order. The Stop Order will only be filled by a market price that is equal to or worse than this price. required),
	PriceValue price;
	// The worst market price that may be used to fill this Stop Order. If the market gaps and crosses through both the price and the priceBound, the Stop Order will be cancelled instead of being filled.
	PriceValue priceBound;
	// The date/time when the Stop Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A MarketIfTouchedOrder is an order that is created with a price threshold, and will only be filled by a market
 * price that is touches or crosses the threshold.
 */
struct MarketIfTouchedOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “MARKET_IF_TOUCHED” for Market If Touched Orders. default=MARKET_IF_TOUCHED), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the MarketIfTouched Order. Restricted to “GTC”, “GFD” and “GTD” for MarketIfTouched Orders. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	// The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// The MarketIfTouched Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the MarketIfTouched Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the MarketIfTouched Order. The MarketIfTouched Order will only be filled by a market price that crosses this price from the direction of the market price at the time when the Order was created (the initialMarketPrice). Depending on the value of the Order’s price and initialMarketPrice, the MarketIfTouchedOrder will behave like a Limit or a Stop Order. required),
	PriceValue price;
	// The worst market price that may be used to fill this MarketIfTouched Order.
	PriceValue priceBound;
	// The date/time when the MarketIfTouched Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The Market price at the time when the MarketIfTouched Order was created.
	PriceValue initialMarketPrice;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	// Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a Trade was opened as a result of the fill)
	TradeID tradeOpenedID;
	// Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a Trade was reduced as a result of the fill)
	TradeID tradeReducedID;
	// Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and one or more Trades were closed as a result of the fill)
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	// ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
	// The ID of the Order that was replaced by this Order (only provided if this Order was created as part of a cancel/replace).
	OrderID replacesOrderID;
	// The ID of the Order that replaced this Order (only provided if this Order was cancelled as part of a cancel/replace).
	OrderID replacedByOrderID;
};

/*
 * A MarketOrderRequest specifies the parameters that may be set when creating a Market Order.
 */
struct MarketOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “MARKET” when creating a Market Order. default=MARKET), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Market Order. Restricted to FOK or IOC for a MarketOrder. required, default=FOK), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	// The Market Order’s Instrument. required),
	InstrumentName instrument;
	/* The quantity requested to be filled by the Market Order. A positive number of units results in a
	 * long Order, and a negative number of units results in a short Order. required),*/
	DecimalNumber units;
	// The worst price that the client is willing to have the Market Order filled at.
	PriceValue priceBound;
	/* The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account
	 * is associated with MT4. */
	ClientExtensions clientExtensions;
	/* TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This
	 * may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent
	 * Take Profit Order is modified directly through the Trade. */
	TakeProfitDetails takeProfitOnFill;
	/* StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may
	 * happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss
	 * Order is modified directly through the Trade. */
	StopLossDetails stopLossOnFill;
	/* GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of
	 * a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or
	 * when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade. */
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	/* TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client.
	 * This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s
	 * dependent Trailing Stop Loss Order is modified directly through the Trade. */
	TrailingStopLossDetails trailingStopLossOnFill;
	/* Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).
	 * Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4. */
	ClientExtensions tradeClientExtensions;
};

/*
 * A LimitOrderRequest specifies the parameters that may be set when creating a Limit Order.
 */
struct LimitOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “LIMIT” when creating a Market Order. default=LIMIT), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Limit Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Limit Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Limit Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Limit Order. The Limit Order will only be filled by a market price that is equal to or better than this price. required),
	PriceValue price;
	// The date/time when the Limit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A StopOrderRequest specifies the parameters that may be set when creating a Stop Order.
 */
struct StopOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “STOP” when creating a Stop Order. default=STOP), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Stop Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The Stop Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Stop Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Stop Order. The Stop Order will only be filled by a market price that is equal to or worse than this price. required),
	PriceValue price;
	// The worst market price that may be used to fill this Stop Order. If the market gaps and crosses through both the price and the priceBound, the Stop Order will be cancelled instead of being filled.
	PriceValue priceBound;
	// The date/time when the Stop Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A MarketIfTouchedOrderRequest specifies the parameters that may be set when creating a Market-if-Touched Order.
 */
struct MarketIfTouchedOrderRequest
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order to Create. Must be set to “MARKET_IF_TOUCHED” when creating a Market If Touched Order. default=MARKET_IF_TOUCHED), */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the MarketIfTouched Order. Restricted to “GTC”, “GFD” and “GTD” for MarketIfTouched Orders. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	// The MarketIfTouched Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the MarketIfTouched Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the MarketIfTouched Order. The MarketIfTouched Order will only be filled by a market price that crosses this price from the direction of the market price at the time when the Order was created (the initialMarketPrice). Depending on the value of the Order’s price and initialMarketPrice, the MarketIfTouchedOrder will behave like a Limit or a Stop Order. required),
	PriceValue price;
	// The worst market price that may be used to fill this MarketIfTouched Order.
	PriceValue priceBound;
	// The date/time when the MarketIfTouched Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// The client extensions to add to the Order. Do not set, modify, or delete clientExtensions if your account is associated with MT4.
	ClientExtensions clientExtensions;
	// TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s dependent Take Profit Order is modified directly through the Trade.
	TakeProfitDetails takeProfitOnFill;
	// StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s dependent Stop Loss Order is modified directly through the Trade.
	StopLossDetails stopLossOnFill;
	// GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s dependent Guaranteed Stop Loss Order is modified directly through the Trade.
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade.
	TrailingStopLossDetails trailingStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created). Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A FixedPriceOrderTransaction represents the creation of a Fixed Price Order in the user’s account. A Fixed Price Order is an Order that is filled immediately at a specified price.
 */
struct FixedPriceOrderTransaction
{	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* FixedPriceOrderReason: The reason that the Fixed Price Order was created
	 * возможные значения:
	 * case PLATFORM_ACCOUNT_MIGRATION: // The Fixed Price Order was created as part of a platform account migration
	 * case TRADE_CLOSE_DIVISION_ACCOUNT_MIGRATION: // The Fixed Price Order was created to close a Trade as part of division account migration
	 * case TRADE_CLOSE_ADMINISTRATIVE_ACTION: // The Fixed Price Order was created to close a Trade administratively */
	FixedPriceOrderReason reason; /* The reason that the Fixed Price Order was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “FIXED_PRICE_ORDER” in a FixedPriceOrderTransaction. default=FIXED_PRICE_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Fixed Price Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Fixed Price Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price specified for the Fixed Price Order. This price is the exact price that the Fixed Price Order will be filled at. required),
	PriceValue price;
	// The state that the trade resulting from the Fixed Price Order should be set to. required),
	string tradeState;
	// The client extensions for the Fixed Price Order.
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A LimitOrderTransaction represents the creation of a Limit Order in the user’s Account.
 */
struct LimitOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Limit Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* LimitOrderReason: The reason that the Limit Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Limit Order was initiated at the request of a client
	 * case REPLACEMENT: // The Limit Order was initiated as a replacement for an existing Order */
	LimitOrderReason reason; /* The reason that the Limit Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “LIMIT_ORDER” in a LimitOrderTransaction. default=LIMIT_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Limit Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Limit Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Limit Order. The Limit Order will only be filled by a market price that is equal to or better than this price. required),
	PriceValue price;
	// The date/time when the Limit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A LimitOrderRejectTransaction represents the rejection of the creation of a Limit Order.
 */
struct LimitOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Limit Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* LimitOrderReason: The reason that the Limit Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Limit Order was initiated at the request of a client
	 * case REPLACEMENT: // The Limit Order was initiated as a replacement for an existing Order */
	LimitOrderReason reason; /* The reason that the Limit Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “LIMIT_ORDER_REJECT” in a LimitOrderRejectTransaction. default=LIMIT_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Limit Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Limit Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Limit Order. The Limit Order will only be filled by a market price that is equal to or better than this price. required),
	PriceValue price;
	// The date/time when the Limit Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A StopOrderTransaction represents the creation of a Stop Order in the user’s Account.
 */
struct StopOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Stop Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* StopOrderReason: The reason that the Stop Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Stop Order was initiated at the request of a client
	 * case REPLACEMENT: // The Stop Order was initiated as a replacement for an existing Order */
	StopOrderReason reason; /* The reason that the Stop Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “STOP_ORDER” in a StopOrderTransaction. default=STOP_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Stop Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Stop Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Stop Order. The Stop Order will only be filled by a market price that is equal to or worse than this price. required),
	PriceValue price;
	// The worst market price that may be used to fill this Stop Order. If the market gaps and crosses through both the price and the priceBound, the Stop Order will be cancelled instead of being filled.
	PriceValue priceBound;
	// The date/time when the Stop Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A StopOrderRejectTransaction represents the rejection of the creation of a Stop Order.
 */
struct StopOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Stop Order. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* StopOrderReason: The reason that the Stop Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Stop Order was initiated at the request of a client
	 * case REPLACEMENT: // The Stop Order was initiated as a replacement for an existing Order */
	StopOrderReason reason; /* The reason that the Stop Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “STOP_ORDER_REJECT” in a StopOrderRejectTransaction. default=STOP_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Stop Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Stop Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the Stop Order. The Stop Order will only be filled by a market price that is equal to or worse than this price. required),
	PriceValue price;
	// The worst market price that may be used to fill this Stop Order. If the market gaps and crosses through both the price and the priceBound, the Stop Order will be cancelled instead of being filled.
	PriceValue priceBound;
	// The date/time when the Stop Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A MarketIfTouchedOrderTransaction represents the creation of a MarketIfTouched Order in the user’s Account.
 */
struct MarketIfTouchedOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the MarketIfTouched Order. Restricted to “GTC”, “GFD” and “GTD” for MarketIfTouched Orders. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* MarketIfTouchedOrderReason: The reason that the Market-if-touched Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Market-if-touched Order was initiated at the request of a client
	 * case REPLACEMENT: // The Market-if-touched Order was initiated as a replacement for an existing Order */
	MarketIfTouchedOrderReason reason; /* The reason that the Market-if-touched Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARKET_IF_TOUCHED_ORDER” in a MarketIfTouchedOrderTransaction. default=MARKET_IF_TOUCHED_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The MarketIfTouched Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the MarketIfTouched Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the MarketIfTouched Order. The MarketIfTouched Order will only be filled by a market price that crosses this price from the direction of the market price at the time when the Order was created (the initialMarketPrice). Depending on the value of the Order’s price and initialMarketPrice, the MarketIfTouchedOrder will behave like a Limit or a Stop Order. required),
	PriceValue price;
	// The worst market price that may be used to fill this MarketIfTouched Order.
	PriceValue priceBound;
	// The date/time when the MarketIfTouched Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order replaces (only provided if this Order replaces an existing Order).
	OrderID replacesOrderID;
	// The ID of the Transaction that cancels the replaced Order (only provided if this Order replaces an existing Order).
	TransactionID cancellingTransactionID;
};

/*
 * A MarketIfTouchedOrderRejectTransaction represents the rejection of the creation of a MarketIfTouched Order.
 */
struct MarketIfTouchedOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the MarketIfTouched Order. Restricted to “GTC”, “GFD” and “GTD” for MarketIfTouched Orders. required, default=GTC), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* OrderTriggerCondition: Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid.
	 * возможные значения:
	 * case DEFAULT: // Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders.
	 * case INVERSE: // Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders.
	 * case BID: // Trigger an Order by comparing its price to the bid regardless of whether it is long or short.
	 * case ASK: // Trigger an Order by comparing its price to the ask regardless of whether it is long or short.
	 * case MID: // Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. */
	OrderTriggerCondition triggerCondition; /* Specification of which price component should be used when determining if an Order should be triggered and filled. This allows Orders to be triggered based on the bid, ask, mid, default (ask for buy, bid for sell) or inverse (ask for sell, bid for buy) price depending on the desired behaviour. Orders are always filled using their default price component. This feature is only provided through the REST API. Clients who choose to specify a non-default trigger condition will not see it reflected in any of OANDA’s proprietary or partner trading platforms, their transaction history or their account statements. OANDA platforms always assume that an Order’s trigger condition is set to the default value when indicating the distance from an Order’s trigger price, and will always provide the default trigger condition when creating or modifying an Order. A special restriction applies when creating a Guaranteed Stop Loss Order. In this case the TriggerCondition value must either be “DEFAULT”, or the “natural” trigger side “DEFAULT” results in. So for a Guaranteed Stop Loss Order for a long trade valid values are “DEFAULT” and “BID”, and for short trades “DEFAULT” and “ASK” are valid. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* MarketIfTouchedOrderReason: The reason that the Market-if-touched Order was initiated
	 * возможные значения:
	 * case CLIENT_ORDER: // The Market-if-touched Order was initiated at the request of a client
	 * case REPLACEMENT: // The Market-if-touched Order was initiated as a replacement for an existing Order */
	MarketIfTouchedOrderReason reason; /* The reason that the Market-if-touched Order was initiated */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARKET_IF_TOUCHED_ORDER_REJECT” in a MarketIfTouchedOrderRejectTransaction. default=MARKET_IF_TOUCHED_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The MarketIfTouched Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the MarketIfTouched Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The price threshold specified for the MarketIfTouched Order. The MarketIfTouched Order will only be filled by a market price that crosses this price from the direction of the market price at the time when the Order was created (the initialMarketPrice). Depending on the value of the Order’s price and initialMarketPrice, the MarketIfTouchedOrder will behave like a Limit or a Stop Order. required),
	PriceValue price;
	// The worst market price that may be used to fill this MarketIfTouched Order.
	PriceValue priceBound;
	// The date/time when the MarketIfTouched Order will be cancelled if its timeInForce is “GTD”.
	DateTime gtdTime;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
	// The ID of the Order that this Order was intended to replace (only provided if this Order was intended to replace an existing Order).
	OrderID intendedReplacesOrderID;
};

/*
 * A TradeOpen object represents a Trade for an instrument that was opened in an Account. It is found
 * embedded in Transactions that affect the position of an instrument in the Account, specifically the
 * OrderFill Transaction.
 */
struct TradeOpen
{	// The ID of the Trade that was opened
	TradeID tradeID;
	// The number of units opened by the Trade
	DecimalNumber units;
	// The average price that the units were opened at.
	PriceValue price;
	// This is the fee charged for opening the trade if it has a guaranteed Stop Loss Order attached to it.
	AccountUnits guaranteedExecutionFee;
	/* This is the fee charged for opening the trade if it has a guaranteed Stop Loss Order attached to it,
	 * expressed in the Instrument’s quote currency.*/
	DecimalNumber quoteGuaranteedExecutionFee;
	// The client extensions for the newly opened Trade
	ClientExtensions clientExtensions;
	/* The half spread cost for the trade open. This can be a positive or negative value and is represented
	 * in the home currency of the Account.*/
	AccountUnits halfSpreadCost;
	/* The margin required at the time the Trade was created. Note, this is the ‘pure’ margin required, it
	 * is not the ‘effective’ margin used that factors in the trade risk if a GSLO is attached to the trade.*/
	AccountUnits initialMarginRequired;
};

/*
 * A TradeReduce object represents a Trade for an instrument that was reduced (either partially or fully) in an Account. It is found embedded in Transactions that affect the position of an instrument in the account, specifically the OrderFill Transaction.
 */
struct TradeReduce
{	// The ID of the Trade that was reduced or closed
	TradeID tradeID;
	// The number of units that the Trade was reduced by
	DecimalNumber units;
	// The average price that the units were closed at. This price may be clamped for guaranteed Stop Loss Orders.
	PriceValue price;
	// The PL realized when reducing the Trade
	AccountUnits realizedPL;
	// The financing paid/collected when reducing the Trade
	AccountUnits financing;
	// The base financing paid/collected when reducing the Trade
	DecimalNumber baseFinancing;
	// The quote financing paid/collected when reducing the Trade
	DecimalNumber quoteFinancing;
	// The financing rate in effect for the instrument used to calculate the amount of financing paid/collected when reducing the Trade. This field will only be set if the AccountFinancingMode at the time of the order fill is SECOND_BY_SECOND_INSTRUMENT. The value is in decimal rather than percentage points, e.g. 5% is represented as 0.05.
	DecimalNumber financingRate;
	// This is the fee that is charged for closing the Trade if it has a guaranteed Stop Loss Order attached to it.
	AccountUnits guaranteedExecutionFee;
	// This is the fee that is charged for closing the Trade if it has a guaranteed Stop Loss Order attached to it, expressed in the Instrument’s quote currency.
	DecimalNumber quoteGuaranteedExecutionFee;
	// The half spread cost for the trade reduce/close. This can be a positive or negative value and is represented in the home currency of the Account.
	AccountUnits halfSpreadCost;
};

/*
 * An OrderFillTransaction represents the filling of an Order in the client’s Account.
 */
struct OrderFillTransaction
{	/* OrderFillReason: The reason that an Order was filled
	 * возможные значения:
	 * case LIMIT_ORDER: // The Order filled was a Limit Order
	 * case STOP_ORDER: // The Order filled was a Stop Order
	 * case MARKET_IF_TOUCHED_ORDER: // The Order filled was a Market-if-touched Order
	 * case TAKE_PROFIT_ORDER: // The Order filled was a Take Profit Order
	 * case STOP_LOSS_ORDER: // The Order filled was a Stop Loss Order
	 * case GUARANTEED_STOP_LOSS_ORDER: // The Order filled was a Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS_ORDER: // The Order filled was a Trailing Stop Loss Order
	 * case MARKET_ORDER: // The Order filled was a Market Order
	 * case MARKET_ORDER_TRADE_CLOSE: // The Order filled was a Market Order used to explicitly close a Trade
	 * case MARKET_ORDER_POSITION_CLOSEOUT: // The Order filled was a Market Order used to explicitly close a Position
	 * case MARKET_ORDER_MARGIN_CLOSEOUT: // The Order filled was a Market Order used for a Margin Closeout
	 * case MARKET_ORDER_DELAYED_TRADE_CLOSE: // The Order filled was a Market Order used for a delayed Trade close
	 * case FIXED_PRICE_ORDER: // The Order filled was a Fixed Price Order
	 * case FIXED_PRICE_ORDER_PLATFORM_ACCOUNT_MIGRATION: // The Order filled was a Fixed Price Order created as part of a platform account migration
	 * case FIXED_PRICE_ORDER_DIVISION_ACCOUNT_MIGRATION: // The Order filled was a Fixed Price Order created to close a Trade as part of division account migration
	 * case FIXED_PRICE_ORDER_ADMINISTRATIVE_ACTION: // The Order filled was a Fixed Price Order created to close a Trade administratively */
	OrderFillReason reason; /* The reason that an Order was filled */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “ORDER_FILL” for an OrderFillTransaction. default=ORDER_FILL), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	/* The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied
	 * to the Account simultaneously.*/
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The ID of the Order filled.
	OrderID orderID;
	// The client Order ID of the Order filled (only provided if the client has assigned one).
	ClientID clientOrderID;
	// The name of the filled Order’s instrument.
	InstrumentName instrument;
	// The number of units filled by the OrderFill.
	DecimalNumber units;
	/* This is the conversion factor in effect for the Account at the time of the OrderFill for
	 * converting any gains realized in Instrument quote units into units of the Account’s home currency.
	 * Deprecated: Will be removed in a future API update. deprecated),*/
	DecimalNumber gainQuoteHomeConversionFactor;
	/* This is the conversion factor in effect for the Account at the time of the OrderFill for converting
	 * any losses realized in Instrument quote units into units of the Account’s home currency.
	 * Deprecated: Will be removed in a future API update. deprecated),*/
	DecimalNumber lossQuoteHomeConversionFactor;
	// The HomeConversionFactors in effect at the time of the OrderFill.
	HomeConversionFactors homeConversionFactors;
	/* This field is now deprecated and should no longer be used. The individual tradesClosed,
	 * tradeReduced and tradeOpened fields contain the exact/official price each unit was filled at.
	 * Deprecated: Will be removed in a future API update. deprecated),*/
	PriceValue price;
	/* The price that all of the units of the OrderFill should have been filled at, in the absence of
	 * guaranteed price execution. This factors in the Account’s current ClientPrice, used liquidity
	 * and the units of the OrderFill only. If no Trades were closed with their price clamped for guaranteed
	 * stop loss enforcement, then this value will match the price fields of each Trade opened, closed, and
	 * reduced, and they will all be the exact same.*/
	PriceValue fullVWAP;
	// The price in effect for the account at the time of the Order fill.
	ClientPrice fullPrice;
	// The profit or loss incurred when the Order was filled.
	AccountUnits pl;
	// The profit or loss incurred when the Order was filled, in the Instrument’s quote currency.
	DecimalNumber quotePL;
	// The financing paid or collected when the Order was filled.
	AccountUnits financing;
	// The financing paid or collected when the Order was filled, in the Instrument’s base currency.
	DecimalNumber baseFinancing;
	// The financing paid or collected when the Order was filled, in the Instrument’s quote currency.
	DecimalNumber quoteFinancing;
	/* The commission charged in the Account’s home currency as a result of filling the Order.
	 * The commission is always represented as a positive quantity of the Account’s home currency,
	 * however it reduces the balance in the Account.*/
	AccountUnits commission;
	// The total guaranteed execution fees charged for all Trades opened, closed or reduced with guaranteed Stop Loss Orders.
	AccountUnits guaranteedExecutionFee;
	/* The total guaranteed execution fees charged for all Trades opened, closed or reduced with guaranteed Stop
	 * Loss Orders, expressed in the Instrument’s quote currency.*/
	DecimalNumber quoteGuaranteedExecutionFee;
	// The Account’s balance after the Order was filled.
	AccountUnits accountBalance;
	// The Trade that was opened when the Order was filled (only provided if filling the Order resulted in a new Trade).
	TradeOpen tradeOpened;
	/* The Trades that were closed when the Order was filled (only provided if filling the Order resulted in a
	 * closing open Trades).*/
	glb::CFxArray<TradeReduce, true> tradesClosed;
	/* The Trade that was reduced when the Order was filled (only provided if filling the Order resulted in reducing
	 * an open Trade).*/
	TradeReduce tradeReduced;
	/* The half spread cost for the OrderFill, which is the sum of the halfSpreadCost values in the tradeOpened,
	 * tradesClosed and tradeReduced fields. This can be a positive or negative value and is represented in the
	 * home currency of the Account.*/
	AccountUnits halfSpreadCost;
};

/*
 * A MarketOrderTradeClose specifies the extensions to a Market Order that has been created specifically to close a Trade.
 */
struct MarketOrderTradeClose
{	// The ID of the Trade requested to be closed
	TradeID tradeID;
	// The client ID of the Trade requested to be closed
	string clientTradeID;
	// Indication of how much of the Trade to close. Either “ALL”, or a DecimalNumber reflection a partial close of the Trade.
	string units;
public:
	MarketOrderTradeClose();
	void Init();
	int GetBinData(glb::CFxArray<BYTE, true> &a) const;
	int SetBinData(const BYTE *p, const int cnt);
	// оператор сравнения вернёт 0 если объекты равны, -1 если этот меньше 'o', 1 иначе.
	int operator ==(const MarketOrderTradeClose &o) const {
		int n;
		return ((tradeID < o.tradeID) ? -1 : (tradeID > o.tradeID) ? 1 :
				(n = clientTradeID == o.clientTradeID) ? n : units == o.units);
	}
};

/*
 * Details for the Market Order extensions specific to a Market Order placed that is part of a Market Order
 * Margin Closeout in a client’s account
 */
struct MarketOrderMarginCloseout
{	/* MarketOrderMarginCloseoutReason: The reason that the Market Order was created to perform a margin closeout
	 * возможные значения:
	 * case MARGIN_CHECK_VIOLATION: // Trade closures resulted from violating OANDA’s margin policy
	 * case REGULATORY_MARGIN_CALL_VIOLATION: // Trade closures came from a margin closeout event resulting from regulatory conditions placed on the Account’s margin call
	 * case REGULATORY_MARGIN_CHECK_VIOLATION: // Trade closures resulted from violating the margin policy imposed by regulatory requirements */
	MarketOrderMarginCloseoutReason reason; /* The reason the Market Order was created to perform a margin closeout */
public:
	MarketOrderMarginCloseout() {
		reason = 0;
	}
};

/*
 * Details for the Market Order extensions specific to a Market Order placed with the intent of fully closing
 * a specific open trade that should have already been closed but wasn’t due to halted market conditions
 */
struct MarketOrderDelayedTradeClose
{	// The ID of the Trade being closed
	TradeID tradeID;
	// The Client ID of the Trade being closed
	TradeID clientTradeID;
	// The Transaction ID of the DelayedTradeClosure transaction to which this Delayed Trade Close belongs to
	TransactionID sourceTransactionID;
public:
	MarketOrderDelayedTradeClose() {
		tradeID = clientTradeID = sourceTransactionID = 0;
	}
	void Init() {
		tradeID = clientTradeID = sourceTransactionID = 0;
	}
};

/*
 * A MarketOrderPositionCloseout specifies the extensions to a Market Order when it has been created to closeout
 * a specific Position.
 */
struct MarketOrderPositionCloseout
{	// The instrument of the Position being closed out.
	InstrumentName instrument;
	/* Indication of how much of the Position to close. Either “ALL”, or a DecimalNumber reflection a partial close
	 * of the Trade. The DecimalNumber must always be positive, and represent a number that doesn’t exceed the
	 * absolute size of the Position.*/
	string units;
public:
	MarketOrderPositionCloseout() {
	}
	void Init() {
		instrument.SetCountZeroND();
		units.SetCountZeroND();
	}
	// оператор вернёт 0 если объекты равны, -1 если этот меньше 'o' и 1 если этот больше 'o'
	int operator ==(const MarketOrderPositionCloseout &o) const {
		int n;
		return ((n = instrument == o.instrument) ? n : units == o.units);
	}
};

/*
 * A MarketOrder is an order that is filled immediately upon creation using the current market price.
 */
struct MarketOrder
{	/* OrderType: The type of the Order.
	 * возможные значения:
	 * case MARKET: // A Market Order
	 * case LIMIT: // A Limit Order
	 * case STOP: // A Stop Order
	 * case MARKET_IF_TOUCHED: // A Market-if-touched Order
	 * case TAKE_PROFIT: // A Take Profit Order
	 * case STOP_LOSS: // A Stop Loss Order
	 * case GUARANTEED_STOP_LOSS: // A Guaranteed Stop Loss Order
	 * case TRAILING_STOP_LOSS: // A Trailing Stop Loss Order
	 * case FIXED_PRICE: // A Fixed Price Order */
	OrderType type; /* The type of the Order. Always set to “MARKET” for Market Orders. default=MARKET), */
	/* OrderState: The current state of the Order.
	 * возможные значения:
	 * case PENDING: // The Order is currently pending execution
	 * case FILLED: // The Order has been filled
	 * case TRIGGERED: // The Order has been triggered
	 * case CANCELLED: // The Order has been cancelled */
	OrderState state; /* The current state of the Order. */
	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before
	 * being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Market Order. Restricted to FOK or IOC for a MarketOrder. required, default=FOK), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	// The Order’s identifier, unique within the Order’s Account.
	OrderID id;
	// The time when the Order was created.
	DateTime createTime;
	/* The client extensions of the Order. Do not set, modify, or delete clientExtensions if your account is
	 * associated with MT4. */
	ClientExtensions clientExtensions;
	// The Market Order’s Instrument. required),
	InstrumentName instrument;
	/* The quantity requested to be filled by the Market Order. A positive number of units results in a
	 * long Order, and a negative number of units results in a short Order. required), */
	DecimalNumber units;
	// The worst price that the client is willing to have the Market Order filled at.
	PriceValue priceBound;
	/* Specification of how Positions in the Account are modified when the Order is partially filled. required,
	 * default=DEFAULT), */
	OrderPositionFill partialFill;
	/* Details of the Trade requested to be closed, only provided when the Market Order is being used to
	 * explicitly close a Trade. */
	MarketOrderTradeClose tradeClose;
	/* Details of the long Position requested to be closed out, only provided when a Market Order is being used
	 * to explicitly closeout a long Position. */
	MarketOrderPositionCloseout longPositionCloseout;
	/* Details of the short Position requested to be closed out, only provided when a Market Order is
	 * being used to explicitly closeout a short Position. */
	MarketOrderPositionCloseout shortPositionCloseout;
	// Details of the Margin Closeout that this Market Order was created for
	MarketOrderMarginCloseout marginCloseout;
	// Details of the delayed Trade close that this Market Order was created for
	MarketOrderDelayedTradeClose delayedTradeClose;
	/* TakeProfitDetails specifies the details of a Take Profit Order to be created on behalf of a client.
	 * This may happen when an Order is filled that opens a Trade requiring a Take Profit, or when a Trade’s
	 * dependent Take Profit Order is modified directly through the Trade. */
	TakeProfitDetails takeProfitOnFill;
	/* StopLossDetails specifies the details of a Stop Loss Order to be created on behalf of a client.
	 * This may happen when an Order is filled that opens a Trade requiring a Stop Loss, or when a Trade’s
	 * dependent Stop Loss Order is modified directly through the Trade. */
	StopLossDetails stopLossOnFill;
	/* GuaranteedStopLossDetails specifies the details of a Guaranteed Stop Loss Order to be created on behalf of a client.
	 * This may happen when an Order is filled that opens a Trade requiring a Guaranteed Stop Loss, or when a Trade’s
	 * dependent Guaranteed Stop Loss Order is modified directly through the Trade. */
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	/* TrailingStopLossDetails specifies the details of a Trailing Stop Loss Order to be created on behalf of a
	 * client. This may happen when an Order is filled that opens a Trade requiring a Trailing Stop Loss, or when
	 * a Trade’s dependent Trailing Stop Loss Order is modified directly through the Trade. */
	TrailingStopLossDetails trailingStopLossOnFill;
	/* Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).
	 * Do not set, modify, or delete tradeClientExtensions if your account is associated with MT4. */
	ClientExtensions tradeClientExtensions;
	// ID of the Transaction that filled this Order (only provided when the Order’s state is FILLED)
	TransactionID fillingTransactionID;
	// Date/time when the Order was filled (only provided when the Order’s state is FILLED)
	DateTime filledTime;
	/* Trade ID of Trade opened when the Order was filled (only provided when the Order’s state is FILLED and a
	 * Trade was opened as a result of the fill) */
	TradeID tradeOpenedID;
	/* Trade ID of Trade reduced when the Order was filled (only provided when the Order’s state is FILLED and a
	 * Trade was reduced as a result of the fill) */
	TradeID tradeReducedID;
	/* Trade IDs of Trades closed when the Order was filled (only provided when the Order’s state is FILLED and
	 * one or more Trades were closed as a result of the fill)
	 * Здесь, б-дь не ясно что значит filled, сука ебаная. Допускаю, что открыт, а тут идентификаторы команд
	 * частичного закрытия. Чтобы определить цену, по которой заказ закрыт наглухо к ебеням? Стопом, целью, п-лем,
	 * какимнибудь пидором или норм.человеком или женщиной? Так и должны записывать: закрыт пидором. */
	glb::CFxArray<TradeID, true> tradeClosedIDs;
	/* ID of the Transaction that cancelled the Order (only provided when the Order’s state is CANCELLED)
	 * Допускаю, что это идентификатор команды, которой заказ наглухо закрыт (cancel: аннулировать, погашать)
	 * к ебеням. В таком случае, состояние на cancelled ещё проверяется. Так и порешим. Но если это не верно,
	 * тогда, что-б вам, деятелям Oanda... сука... */
	TransactionID cancellingTransactionID;
	// Date/time when the Order was cancelled (only provided when the state of the Order is CANCELLED)
	DateTime cancelledTime;
public:
	// ф-ции для индексированного файлового массива fl::CVarFile
	MarketOrder();
	int GetBinData(glb::CFxArray<BYTE, true> &ab) const;
	int SetBinData(const BYTE *p, const int cnt);
	bool IsValid() const;
	void Invalidate();
	bool cmp(const MarketOrder &o, const Instrument &i) const;
};

/*
 * A MarketOrderTransaction represents the creation of a Market Order in the user’s account. A Market Order is an Order that is filled immediately at the current market price. Market Orders can be specialized when they are created to accomplish a specific task: to close a Trade, to closeout a Position or to participate in in a Margin closeout.
 */
struct MarketOrderTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending
	 * before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Market Order. Restricted to FOK or IOC for a MarketOrder. required, default=FOK), */
	/* OrderPartialFill: Specification of how Positions in the Account are modified when the Order is paerially filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPartialFill partialFill; /* Specification of how Positions in the Account are modified when the Order is partially filled. Тип предположительный, но имя параметра встречается в запросах со значением DEFAULT или POSITION_DEFAULT */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* MarketOrderReason: The reason that the Market Order was created
	 * возможные значения:
	 * case CLIENT_ORDER: // The Market Order was created at the request of a client
	 * case TRADE_CLOSE: // The Market Order was created to close a Trade at the request of a client
	 * case POSITION_CLOSEOUT: // The Market Order was created to close a Position at the request of a client
	 * case MARGIN_CLOSEOUT: // The Market Order was created as part of a Margin Closeout
	 * case DELAYED_TRADE_CLOSE: // The Market Order was created to close a trade marked for delayed closure */
	MarketOrderReason reason; /* The reason that the Market Order was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARKET_ORDER” in a MarketOrderTransaction. default=MARKET_ORDER), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Market Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Market Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The worst price that the client is willing to have the Market Order filled at.
	PriceValue priceBound;
	// Details of the Trade requested to be closed, only provided when the Market Order is being used to explicitly close a Trade.
	MarketOrderTradeClose tradeClose;
	// Details of the long Position requested to be closed out, only provided when a Market Order is being used to explicitly closeout a long Position.
	MarketOrderPositionCloseout longPositionCloseout;
	// Details of the short Position requested to be closed out, only provided when a Market Order is being used to explicitly closeout a short Position.
	MarketOrderPositionCloseout shortPositionCloseout;
	// Details of the Margin Closeout that this Market Order was created for
	MarketOrderMarginCloseout marginCloseout;
	// Details of the delayed Trade close that this Market Order was created for
	MarketOrderDelayedTradeClose delayedTradeClose;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A MarketOrderRejectTransaction represents the rejection of the creation of a Market Order.
 */
struct MarketOrderRejectTransaction
{	/* TimeInForce: The time-in-force of an Order. TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
	 * возможные значения:
	 * case GTC: // The Order is “Good unTil Cancelled”
	 * case GTD: // The Order is “Good unTil Date” and will be cancelled at the provided time
	 * case GFD: // The Order is “Good For Day” and will be cancelled at 5pm New York time
	 * case FOK: // The Order must be immediately “Filled Or Killed”
	 * case IOC: // The Order must be “Immediately partially filled Or Cancelled” */
	TimeInForce timeInForce; /* The time-in-force requested for the Market Order. Restricted to FOK or IOC for a MarketOrder. required, default=FOK), */
	/* OrderPositionFill: Specification of how Positions in the Account are modified when the Order is filled.
	 * возможные значения:
	 * case OPEN_ONLY: // When the Order is filled, only allow Positions to be opened or extended.
	 * case REDUCE_FIRST: // When the Order is filled, always fully reduce an existing Position before opening a new Position.
	 * case REDUCE_ONLY: // When the Order is filled, only reduce an existing Position.
	 * case DEFAULT: // When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts.
	 * case POSITION_DEFAULT: // Same as DEFAULT. Added manually. */
	OrderPositionFill positionFill; /* Specification of how Positions in the Account are modified when the Order is filled. required, default=DEFAULT), */
	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	/* MarketOrderReason: The reason that the Market Order was created
	 * возможные значения:
	 * case CLIENT_ORDER: // The Market Order was created at the request of a client
	 * case TRADE_CLOSE: // The Market Order was created to close a Trade at the request of a client
	 * case POSITION_CLOSEOUT: // The Market Order was created to close a Position at the request of a client
	 * case MARGIN_CLOSEOUT: // The Market Order was created as part of a Margin Closeout
	 * case DELAYED_TRADE_CLOSE: // The Market Order was created to close a trade marked for delayed closure */
	MarketOrderReason reason; /* The reason that the Market Order was created */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “MARKET_ORDER_REJECT” in a MarketOrderRejectTransaction. default=MARKET_ORDER_REJECT), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The Market Order’s Instrument. required),
	InstrumentName instrument;
	// The quantity requested to be filled by the Market Order. A positive number of units results in a long Order, and a negative number of units results in a short Order. required),
	DecimalNumber units;
	// The worst price that the client is willing to have the Market Order filled at.
	PriceValue priceBound;
	// Details of the Trade requested to be closed, only provided when the Market Order is being used to explicitly close a Trade.
	MarketOrderTradeClose tradeClose;
	// Details of the long Position requested to be closed out, only provided when a Market Order is being used to explicitly closeout a long Position.
	MarketOrderPositionCloseout longPositionCloseout;
	// Details of the short Position requested to be closed out, only provided when a Market Order is being used to explicitly closeout a short Position.
	MarketOrderPositionCloseout shortPositionCloseout;
	// Details of the Margin Closeout that this Market Order was created for
	MarketOrderMarginCloseout marginCloseout;
	// Details of the delayed Trade close that this Market Order was created for
	MarketOrderDelayedTradeClose delayedTradeClose;
	// Client Extensions to add to the Order (only provided if the Order is being created with client extensions).
	ClientExtensions clientExtensions;
	// The specification of the Take Profit Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	TakeProfitDetails takeProfitOnFill;
	// The specification of the Stop Loss Order that should be created for a Trade opened when the Order is filled (if such a Trade is created).
	StopLossDetails stopLossOnFill;
	// The specification of the Trailing Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	TrailingStopLossDetails trailingStopLossOnFill;
	// The specification of the Guaranteed Stop Loss Order that should be created for a Trade that is opened when the Order is filled (if such a Trade is created).
	GuaranteedStopLossDetails guaranteedStopLossOnFill;
	// Client Extensions to add to the Trade created when the Order is filled (if such a Trade is created).  Do not set, modify, delete tradeClientExtensions if your account is associated with MT4.
	ClientExtensions tradeClientExtensions;
};

/*
 * A liquidity regeneration schedule Step indicates the amount of bid and ask liquidity that is used by the Account at a certain time. These amounts will only change at the timestamp of the following step.
 */
struct LiquidityRegenerationScheduleStep
{	// The timestamp of the schedule step.
	DateTime timestamp;
	// The amount of bid liquidity used at this step in the schedule.
	DecimalNumber bidLiquidityUsed;
	// The amount of ask liquidity used at this step in the schedule.
	DecimalNumber askLiquidityUsed;
};

/*
 * A LiquidityRegenerationSchedule indicates how liquidity that is used when filling an Order for an instrument is regenerated following the fill. A liquidity regeneration schedule will be in effect until the timestamp of its final step, but may be replaced by a schedule created for an Order of the same instrument that is filled while it is still in effect.
 */
struct LiquidityRegenerationSchedule
{	// The steps in the Liquidity Regeneration Schedule
	glb::CFxArray<LiquidityRegenerationScheduleStep, true> steps;
};

/*
 * OpenTradeFinancing is used to pay/collect daily financing charge for an open Trade within an Account
 */
struct OpenTradeFinancing
{	// The ID of the Trade that financing is being paid/collected for.
	TradeID tradeID;
	// The amount of financing paid/collected for the Trade.
	AccountUnits financing;
	// The amount of financing paid/collected in the Instrument’s base currency for the Trade.
	DecimalNumber baseFinancing;
	// The amount of financing paid/collected in the Instrument’s quote currency for the Trade.
	DecimalNumber quoteFinancing;
	// The financing rate in effect for the instrument used to calculate the the amount of financing paid/collected for the Trade. This field will only be set if the AccountFinancingMode at the time of the daily financing is DAILY_INSTRUMENT or SECOND_BY_SECOND_INSTRUMENT. The value is in decimal rather than percentage points, e.g. 5% is represented as 0.05.
	DecimalNumber financingRate;
};

/*
 * OpenTradeFinancing is used to pay/collect daily financing charge for a Position within an Account
 */
struct PositionFinancing
{	/* AccountFinancingMode: The financing mode of an Account
	 * возможные значения:
	 * case NO_FINANCING: // No financing is paid/charged for open Trades in the Account
	 * case SECOND_BY_SECOND: // Second-by-second financing is paid/charged for open Trades in the Account, both daily and when the the Trade is closed
	 * case DAILY: // A full day’s worth of financing is paid/charged for open Trades in the Account daily at 5pm New York time */
	AccountFinancingMode accountFinancingMode; /* The account financing mode at the time of the daily financing. */
	// The instrument of the Position that financing is being paid/collected for.
	InstrumentName instrument;
	// The amount of financing paid/collected for the Position.
	AccountUnits financing;
	// The amount of base financing paid/collected for the Position.
	DecimalNumber baseFinancing;
	// The amount of quote financing paid/collected for the Position.
	DecimalNumber quoteFinancing;
	// The HomeConversionFactors in effect for the Position’s Instrument at the time of the DailyFinancing.
	HomeConversionFactors homeConversionFactors;
	// The financing paid/collected for each open Trade within the Position.
	glb::CFxArray<OpenTradeFinancing, true> openTradeFinancings;
};

/*
 * A DailyFinancingTransaction represents the daily payment/collection of financing for an Account.
 */
struct DailyFinancingTransaction
{	/* AccountFinancingMode: The financing mode of an Account
	 * возможные значения:
	 * case NO_FINANCING: // No financing is paid/charged for open Trades in the Account
	 * case SECOND_BY_SECOND: // Second-by-second financing is paid/charged for open Trades in the Account, both daily and when the the Trade is closed
	 * case DAILY: // A full day’s worth of financing is paid/charged for open Trades in the Account daily at 5pm New York time */
	AccountFinancingMode accountFinancingMode; /* The account financing mode at the time of the daily financing. This field is no longer in use moving forward and was replaced by accountFinancingMode in individual positionFinancings since the financing mode could differ between instruments. Deprecated: Will be removed in a future API update. deprecated), */
	/* TransactionType: The possible types of a Transaction
	 * возможные значения:
	 * case CREATE: // Account Create Transaction
	 * case CLOSE: // Account Close Transaction
	 * case REOPEN: // Account Reopen Transaction
	 * case CLIENT_CONFIGURE: // Client Configuration Transaction
	 * case CLIENT_CONFIGURE_REJECT: // Client Configuration Reject Transaction
	 * case TRANSFER_FUNDS: // Transfer Funds Transaction
	 * case TRANSFER_FUNDS_REJECT: // Transfer Funds Reject Transaction
	 * case MARKET_ORDER: // Market Order Transaction
	 * case MARKET_ORDER_REJECT: // Market Order Reject Transaction
	 * case FIXED_PRICE_ORDER: // Fixed Price Order Transaction
	 * case LIMIT_ORDER: // Limit Order Transaction
	 * case LIMIT_ORDER_REJECT: // Limit Order Reject Transaction
	 * case STOP_ORDER: // Stop Order Transaction
	 * case STOP_ORDER_REJECT: // Stop Order Reject Transaction
	 * case MARKET_IF_TOUCHED_ORDER: // Market if Touched Order Transaction
	 * case MARKET_IF_TOUCHED_ORDER_REJECT: // Market if Touched Order Reject Transaction
	 * case TAKE_PROFIT_ORDER: // Take Profit Order Transaction
	 * case TAKE_PROFIT_ORDER_REJECT: // Take Profit Order Reject Transaction
	 * case STOP_LOSS_ORDER: // Stop Loss Order Transaction
	 * case STOP_LOSS_ORDER_REJECT: // Stop Loss Order Reject Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER: // Guaranteed Stop Loss Order Transaction
	 * case GUARANTEED_STOP_LOSS_ORDER_REJECT: // Guaranteed Stop Loss Order Reject Transaction
	 * case TRAILING_STOP_LOSS_ORDER: // Trailing Stop Loss Order Transaction
	 * case TRAILING_STOP_LOSS_ORDER_REJECT: // Trailing Stop Loss Order Reject Transaction
	 * case ORDER_FILL: // Order Fill Transaction
	 * case ORDER_CANCEL: // Order Cancel Transaction
	 * case ORDER_CANCEL_REJECT: // Order Cancel Reject Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY: // Order Client Extensions Modify Transaction
	 * case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT: // Order Client Extensions Modify Reject Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY: // Trade Client Extensions Modify Transaction
	 * case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT: // Trade Client Extensions Modify Reject Transaction
	 * case MARGIN_CALL_ENTER: // Margin Call Enter Transaction
	 * case MARGIN_CALL_EXTEND: // Margin Call Extend Transaction
	 * case MARGIN_CALL_EXIT: // Margin Call Exit Transaction
	 * case DELAYED_TRADE_CLOSURE: // Delayed Trade Closure Transaction
	 * case DAILY_FINANCING: // Daily Financing Transaction
	 * case DIVIDEND_ADJUSTMENT: // Dividend Adjustment Transaction
	 * case RESET_RESETTABLE_PL: // Reset Resettable PL Transaction */
	TransactionType type; /* The Type of the Transaction. Always set to “DAILY_FINANCING” for a DailyFinancingTransaction. default=DAILY_FINANCING), */
	// The Transaction’s Identifier.
	TransactionID id;
	// The date/time when the Transaction was created.
	DateTime time;
	// The ID of the user that initiated the creation of the Transaction.
	integer userID;
	// The ID of the Account the Transaction was created for.
	AccountID accountID;
	// The ID of the “batch” that the Transaction belongs to. Transactions in the same batch are applied to the Account simultaneously.
	TransactionID batchID;
	// The Request ID of the request which generated the transaction.
	RequestID requestID;
	// The amount of financing paid/collected for the Account.
	AccountUnits financing;
	// The Account’s balance after daily financing.
	AccountUnits accountBalance;
	// The financing paid/collected for each Position in the Account.
	glb::CFxArray<PositionFinancing, false> positionFinancings;
};

/*
 * A TransactionHeartbeat object is injected into the Transaction stream to ensure that the HTTP connection remains active.
 */
struct TransactionHeartbeat
{	// The string “HEARTBEAT” default=HEARTBEAT),
	string type;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
	// The date/time when the TransactionHeartbeat was created.
	DateTime time;
};

/*
 * Добавлено вручную в reader/source/oa_terms. Не видел пока как Oanda возвращает ошибку. Подозреваю, что в этой структуре, где один из эл-тов может отсутствовать.
 */
struct ErrorResponse
{	/* TransactionRejectReason: The reason that a Transaction was rejected.
	 * возможные значения:
	 * case INTERNAL_SERVER_ERROR: // An unexpected internal server error has occurred
	 * case INSTRUMENT_PRICE_UNKNOWN: // The system was unable to determine the current price for the Order’s instrument
	 * case ACCOUNT_NOT_ACTIVE: // The Account is not active
	 * case ACCOUNT_LOCKED: // The Account is locked
	 * case ACCOUNT_ORDER_CREATION_LOCKED: // The Account is locked for Order creation
	 * case ACCOUNT_CONFIGURATION_LOCKED: // The Account is locked for configuration
	 * case ACCOUNT_DEPOSIT_LOCKED: // The Account is locked for deposits
	 * case ACCOUNT_WITHDRAWAL_LOCKED: // The Account is locked for withdrawals
	 * case ACCOUNT_ORDER_CANCEL_LOCKED: // The Account is locked for Order cancellation
	 * case INSTRUMENT_NOT_TRADEABLE: // The instrument specified is not tradeable by the Account
	 * case PENDING_ORDERS_ALLOWED_EXCEEDED: // Creating the Order would result in the maximum number of allowed pending Orders being exceeded
	 * case ORDER_ID_UNSPECIFIED: // Neither the Order ID nor client Order ID are specified
	 * case ORDER_DOESNT_EXIST: // The Order specified does not exist
	 * case ORDER_IDENTIFIER_INCONSISTENCY: // The Order ID and client Order ID specified do not identify the same Order
	 * case TRADE_ID_UNSPECIFIED: // Neither the Trade ID nor client Trade ID are specified
	 * case TRADE_DOESNT_EXIST: // The Trade specified does not exist
	 * case TRADE_IDENTIFIER_INCONSISTENCY: // The Trade ID and client Trade ID specified do not identify the same Trade
	 * case INSUFFICIENT_MARGIN: // The Account had insufficient margin to perform the action specified. One possible reason for this is due to the creation or modification of a guaranteed StopLoss Order.
	 * case INSTRUMENT_MISSING: // Order instrument has not been specified
	 * case INSTRUMENT_UNKNOWN: // The instrument specified is unknown
	 * case UNITS_MISSING: // Order units have not been not specified
	 * case UNITS_INVALID: // Order units specified are invalid
	 * case UNITS_PRECISION_EXCEEDED: // The units specified contain more precision than is allowed for the Order’s instrument
	 * case UNITS_LIMIT_EXCEEDED: // The units specified exceeds the maximum number of units allowed
	 * case UNITS_MINIMUM_NOT_MET: // The units specified is less than the minimum number of units required
	 * case PRICE_MISSING: // The price has not been specified
	 * case PRICE_INVALID: // The price specified is invalid
	 * case PRICE_PRECISION_EXCEEDED: // The price specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MISSING: // The price distance has not been specified
	 * case PRICE_DISTANCE_INVALID: // The price distance specified is invalid
	 * case PRICE_DISTANCE_PRECISION_EXCEEDED: // The price distance specified contains more precision than is allowed for the instrument
	 * case PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The price distance exceeds that maximum allowed amount
	 * case PRICE_DISTANCE_MINIMUM_NOT_MET: // The price distance does not meet the minimum allowed amount
	 * case TIME_IN_FORCE_MISSING: // The TimeInForce field has not been specified
	 * case TIME_IN_FORCE_INVALID: // The TimeInForce specified is invalid
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_MISSING: // The TimeInForce is GTD but no GTD timestamp is provided
	 * case TIME_IN_FORCE_GTD_TIMESTAMP_IN_PAST: // The TimeInForce is GTD but the GTD timestamp is in the past
	 * case PRICE_BOUND_INVALID: // The price bound specified is invalid
	 * case PRICE_BOUND_PRECISION_EXCEEDED: // The price bound specified contains more precision than is allowed for the Order’s instrument
	 * case ORDERS_ON_FILL_DUPLICATE_CLIENT_ORDER_IDS: // Multiple Orders on fill share the same client Order ID
	 * case TRADE_ON_FILL_CLIENT_EXTENSIONS_NOT_SUPPORTED: // The Order does not support Trade on fill client extensions because it cannot create a new Trade
	 * case CLIENT_ORDER_ID_INVALID: // The client Order ID specified is invalid
	 * case CLIENT_ORDER_ID_ALREADY_EXISTS: // The client Order ID specified is already assigned to another pending Order
	 * case CLIENT_ORDER_TAG_INVALID: // The client Order tag specified is invalid
	 * case CLIENT_ORDER_COMMENT_INVALID: // The client Order comment specified is invalid
	 * case CLIENT_TRADE_ID_INVALID: // The client Trade ID specified is invalid
	 * case CLIENT_TRADE_ID_ALREADY_EXISTS: // The client Trade ID specified is already assigned to another open Trade
	 * case CLIENT_TRADE_TAG_INVALID: // The client Trade tag specified is invalid
	 * case CLIENT_TRADE_COMMENT_INVALID: // The client Trade comment is invalid
	 * case ORDER_FILL_POSITION_ACTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_FILL_POSITION_ACTION_INVALID: // The OrderFillPositionAction specified is invalid
	 * case TRIGGER_CONDITION_MISSING: // The TriggerCondition field has not been specified
	 * case TRIGGER_CONDITION_INVALID: // The TriggerCondition specified is invalid
	 * case ORDER_PARTIAL_FILL_OPTION_MISSING: // The OrderFillPositionAction field has not been specified
	 * case ORDER_PARTIAL_FILL_OPTION_INVALID: // The OrderFillPositionAction specified is invalid.
	 * case INVALID_REISSUE_IMMEDIATE_PARTIAL_FILL: // When attempting to reissue an order (currently only a MarketIfTouched) that was immediately partially filled, it is not possible to create a correct pending Order.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management Order can be attached to a Trade.
	 * case ORDERS_ON_FILL_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Orders on fill would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to a Trade.
	 * case TAKE_PROFIT_ORDER_ALREADY_EXISTS: // A Take Profit Order for the specified Trade already exists
	 * case TAKE_PROFIT_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Take Profit Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints.
	 * case TAKE_PROFIT_ON_FILL_PRICE_MISSING: // The Take Profit on fill specified does not provide a price
	 * case TAKE_PROFIT_ON_FILL_PRICE_INVALID: // The Take Profit on fill specified contains an invalid price
	 * case TAKE_PROFIT_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Take Profit on fill specified contains a price with more precision than is allowed by the Order’s instrument
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_MISSING: // The Take Profit on fill specified does not provide a TimeInForce
	 * case TAKE_PROFIT_ON_FILL_TIME_IN_FORCE_INVALID: // The Take Profit on fill specifies an invalid TimeInForce
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_MISSING: // The Take Profit on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Take Profit on fill specifies a GTD timestamp that is in the past
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Take Profit on fill client Order ID specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Take Profit on fill client Order tag specified is invalid
	 * case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Take Profit on fill client Order comment specified is invalid
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_MISSING: // The Take Profit on fill specified does not provide a TriggerCondition
	 * case TAKE_PROFIT_ON_FILL_TRIGGER_CONDITION_INVALID: // The Take Profit on fill specifies an invalid TriggerCondition
	 * case STOP_LOSS_ORDER_ALREADY_EXISTS: // A Stop Loss Order for the specified Trade already exists
	 * case STOP_LOSS_ORDER_GUARANTEED_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case STOP_LOSS_ORDER_GUARANTEED_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case STOP_LOSS_ORDER_GUARANTEED_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_CREATE_VIOLATION: // An attempt was made to create a guaranteed Stop Loss Order when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case STOP_LOSS_ORDER_GUARANTEED_HEDGING_NOT_ALLOWED: // An attempt was made to create a guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case STOP_LOSS_ORDER_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Stop Loss Order, however the Account’s configuration requires every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Stop Loss Order, however the Account’s configuration prevents the modification of Stop Loss Orders.
	 * case STOP_LOSS_ORDER_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt was made to create a guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss Order request contains both the price and distance fields.
	 * case STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss Order request contains neither the price nor distance fields.
	 * case STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Stop Loss Order.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_NOT_ALLOWED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_REQUIRED: // An attempt to create a pending Order was made with a Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires guaranteed Stop Loss Orders.
	 * case STOP_LOSS_ON_FILL_PRICE_MISSING: // The Stop Loss on fill specified does not provide a price
	 * case STOP_LOSS_ON_FILL_PRICE_INVALID: // The Stop Loss on fill specifies an invalid price
	 * case STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case STOP_LOSS_ON_FILL_GUARANTEED_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case STOP_LOSS_ON_FILL_GUARANTEED_LEVEL_RESTRICTION_EXCEEDED: // An attempt to create a pending Order was made with a guaranteed Stop Loss Order on fill configured, and the Order’s units exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Stop Loss on fill distance is invalid
	 * case STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Stop Loss on fill contains both the price and distance fields.
	 * case STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Stop Loss on fill contains neither the price nor distance fields.
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Stop Loss on fill specified does not provide a TimeInForce
	 * case STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Stop Loss on fill specifies an invalid TimeInForce
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Stop Loss on fill specifies a GTD timestamp that is in the past
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Stop Loss on fill client Order ID specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Stop Loss on fill client Order tag specified is invalid
	 * case STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Stop Loss on fill client Order comment specified is invalid
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Stop Loss on fill specified does not provide a TriggerCondition
	 * case STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Stop Loss on fill specifies an invalid TriggerCondition
	 * case GUARANTEED_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Guaranteed Stop Loss Order for the specified Trade already exists
	 * case GUARANTEED_STOP_LOSS_ORDER_REQUIRED: // An attempt was made to to create a non-guaranteed stop loss order in an account that requires all stop loss orders to be guaranteed.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_WITHIN_SPREAD: // An attempt to create a guaranteed stop loss order with a price that is within the current tradeable spread.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order, however the Account’s configuration does not allow Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_CREATE_VIOLATION: // An attempt was made to create a Guaranteed Stop Loss Order when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was halted.
	 * case GUARANTEED_STOP_LOSS_ORDER_TIGHTEN_VIOLATION: // An attempt was made to re-create a Guaranteed Stop Loss Order with a tighter fill price when the market was open.
	 * case GUARANTEED_STOP_LOSS_ORDER_HEDGING_NOT_ALLOWED: // An attempt was made to create a Guaranteed Stop Loss Order on a hedged Trade (ie there is an existing open Trade in the opposing direction), however the Account’s configuration does not allow Guaranteed Stop Loss Orders for hedged Trades/Positions.
	 * case GUARANTEED_STOP_LOSS_ORDER_MINIMUM_DISTANCE_NOT_MET: // An attempt was made to create a Guaranteed Stop Loss Order, however the distance between the current price and the trigger price does not meet the Account’s configured minimum Guaranteed Stop Loss distance.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_CANCELABLE: // An attempt was made to cancel a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration requires every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ORDER_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is open, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_HALTED_NOT_REPLACEABLE: // An attempt was made to cancel and replace a Guaranteed Stop Loss Order when the market is halted, however the Account’s configuration prevents the modification of Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ORDER_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // An attempt was made to create a Guaranteed Stop Loss Order, however doing so would exceed the Account’s configured guaranteed StopLoss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss Order request contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss Order request contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Guaranteed Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case GUARANTEED_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Guaranteed Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED_FOR_PENDING_ORDER: // An attempt to create a pending Order was made with no Guaranteed Stop Loss Order on fill specified and the Account’s configuration requires that every Trade have an associated Guaranteed Stop Loss Order.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_NOT_ALLOWED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be guaranteed, however the Account’s configuration does not allow guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_REQUIRED: // An attempt to create a pending Order was made with a Guaranteed Stop Loss Order on fill that was explicitly configured to be not guaranteed, however the Account’s configuration requires Guaranteed Stop Loss Orders.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid price
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill specifies a price with more precision than is allowed by the Order’s instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_MINIMUM_DISTANCE_NOT_MET: // An attempt to create a pending Order was made with the distance between the Guaranteed Stop Loss Order on fill’s price and the pending Order’s price is less than the Account’s configured minimum guaranteed stop loss distance.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_VOLUME_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger number of units that violates the account’s Guaranteed Stop Loss Order level restriction volume.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_LEVEL_RESTRICTION_PRICE_RANGE_EXCEEDED: // Filling the Order would result in the creation of a Guaranteed Stop Loss Order with trigger price that violates the account’s Guaranteed Stop Loss Order level restriction price range.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_INVALID: // The Guaranteed Stop Loss on fill distance is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Guaranteed Stop Loss on fill price distance exceeds the maximum allowed amount.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_DISTANCE_PRECISION_EXCEEDED: // The Guaranteed Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_SPECIFIED: // The Guaranteed Stop Loss on fill contains both the price and distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_PRICE_AND_DISTANCE_BOTH_MISSING: // The Guaranteed Stop Loss on fill contains neither the price nor distance fields.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TimeInForce
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Guaranteed Stop Loss on fill specifies a GTD TimeInForce but does not provide a GTD timestamp
	 * case GUARANTEED_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Guaranteed Stop Loss on fill specifies a GTD timestamp that is in the past.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Guaranteed Stop Loss on fill client Order ID specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Guaranteed Stop Loss on fill client Order tag specified is invalid
	 * case GUARANTEED_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Guaranteed Stop Loss on fill client Order comment specified is invalid.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Guaranteed Stop Loss on fill specified does not provide a TriggerCondition.
	 * case GUARANTEED_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Guaranteed Stop Loss on fill specifies an invalid TriggerCondition.
	 * case TRAILING_STOP_LOSS_ORDER_ALREADY_EXISTS: // A Trailing Stop Loss Order for the specified Trade already exists
	 * case TRAILING_STOP_LOSS_ORDER_WOULD_VIOLATE_FIFO_VIOLATION_SAFEGUARD: // The Trailing Stop Loss Order would cause the associated Trade to be in violation of the FIFO violation safeguard constraints
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_MUTUALLY_EXCLUSIVE_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that only one risk management order can be attached to a Trade.
	 * case TRAILING_STOP_LOSS_ORDER_RMO_MUTUAL_EXCLUSIVITY_GSLO_EXCLUDES_OTHERS_VIOLATION: // The Trailing Stop Loss Order would be in violation of the risk management Order mutual exclusivity configuration specifying that if a GSLO is already attached to a Trade, no other risk management Order can be attached to the same Trade.
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a distance
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_INVALID: // The Trailing Stop Loss on fill distance is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_PRECISION_EXCEEDED: // The Trailing Stop Loss on fill distance contains more precision than is allowed by the instrument
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MAXIMUM_EXCEEDED: // The Trailing Stop Loss on fill price distance exceeds the maximum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_PRICE_DISTANCE_MINIMUM_NOT_MET: // The Trailing Stop Loss on fill price distance does not meet the minimum allowed amount
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_MISSING: // The Trailing Stop Loss on fill specified does not provide a TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_TIME_IN_FORCE_INVALID: // The Trailing Stop Loss on fill specifies an invalid TimeInForce
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_MISSING: // The Trailing Stop Loss on fill TimeInForce is specified as GTD but no GTD timestamp is provided
	 * case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST: // The Trailing Stop Loss on fill GTD timestamp is in the past
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_INVALID: // The Trailing Stop Loss on fill client Order ID specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_TAG_INVALID: // The Trailing Stop Loss on fill client Order tag specified is invalid
	 * case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_COMMENT_INVALID: // The Trailing Stop Loss on fill client Order comment specified is invalid
	 * case TRAILING_STOP_LOSS_ORDERS_NOT_SUPPORTED: // A client attempted to create either a Trailing Stop Loss order or an order with a Trailing Stop Loss On Fill specified, which may not yet be supported.
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_MISSING: // The Trailing Stop Loss on fill specified does not provide a TriggerCondition
	 * case TRAILING_STOP_LOSS_ON_FILL_TRIGGER_CONDITION_INVALID: // The Tailing Stop Loss on fill specifies an invalid TriggerCondition
	 * case CLOSE_TRADE_TYPE_MISSING: // The request to close a Trade does not specify a full or partial close
	 * case CLOSE_TRADE_PARTIAL_UNITS_MISSING: // The request to close a Trade partially did not specify the number of units to close
	 * case CLOSE_TRADE_UNITS_EXCEED_TRADE_SIZE: // The request to partially close a Trade specifies a number of units that exceeds the current size of the given Trade
	 * case CLOSEOUT_POSITION_DOESNT_EXIST: // The Position requested to be closed out does not exist
	 * case CLOSEOUT_POSITION_INCOMPLETE_SPECIFICATION: // The request to closeout a Position was specified incompletely
	 * case CLOSEOUT_POSITION_UNITS_EXCEED_POSITION_SIZE: // A partial Position closeout request specifies a number of units that exceeds the current Position
	 * case CLOSEOUT_POSITION_REJECT: // The request to closeout a Position could not be fully satisfied
	 * case CLOSEOUT_POSITION_PARTIAL_UNITS_MISSING: // The request to partially closeout a Position did not specify the number of units to close.
	 * case MARKUP_GROUP_ID_INVALID: // The markup group ID provided is invalid
	 * case POSITION_AGGREGATION_MODE_INVALID: // The PositionAggregationMode provided is not supported/valid.
	 * case ADMIN_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_INVALID: // The margin rate provided is invalid
	 * case MARGIN_RATE_WOULD_TRIGGER_CLOSEOUT: // The margin rate provided would cause an immediate margin closeout
	 * case ALIAS_INVALID: // The account alias string provided is invalid
	 * case CLIENT_CONFIGURE_DATA_MISSING: // No configuration parameters provided
	 * case MARGIN_RATE_WOULD_TRIGGER_MARGIN_CALL: // The margin rate provided would cause the Account to enter a margin call state.
	 * case AMOUNT_INVALID: // Funding is not possible because the requested transfer amount is invalid
	 * case INSUFFICIENT_FUNDS: // The Account does not have sufficient balance to complete the funding request
	 * case AMOUNT_MISSING: // Funding amount has not been specified
	 * case FUNDING_REASON_MISSING: // Funding reason has not been specified
	 * case OCA_ORDER_IDS_STOP_LOSS_NOT_ALLOWED: // The list of Order Identifiers provided for a One Cancels All Order contains an Order Identifier that refers to a Stop Loss Order. OCA groups cannot contain Stop Loss Orders.
	 * case CLIENT_EXTENSIONS_DATA_MISSING: // Neither Order nor Trade on Fill client extensions were provided for modification
	 * case REPLACING_ORDER_INVALID: // The Order to be replaced has a different type than the replacing Order.
	 * case REPLACING_TRADE_ID_INVALID: // The replacing Order refers to a different Trade than the Order that is being replaced.
	 * case ORDER_CANCEL_WOULD_TRIGGER_CLOSEOUT: // Canceling the order would cause an immediate margin closeout. */
	TransactionRejectReason rejectReason; /* The reason that the Reject Transaction was created */
	// Строка с описанием ошибки (не очень красноречивым)
	string errorMessage;
};

/*
 * Добавлено вручную в reader/source/oa_terms. Ответ на запрос /v3/accounts содержит массив accounts.
 */
struct AccountsResponse
{	// The list of Accounts the client is authorized to access and their associated properties.
	glb::CFxArray<AccountProperties, false> accounts;
};

/*
 * Добавлено вручную в reader/source/oa_terms. Ответ на запрос /v3/accounts/{accountID} содержит сведения об учётной записи.
 */
struct AccountResponse
{	// The full details of the requested Account.
	Account account;
	// The ID of the most recent Transaction created for the Account.
	TransactionID lastTransactionID;
	/*
	 * функцыонал
	 */
	AccountResponse() {
		account.Init();
		lastTransactionID = -1;
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * Добавлено вручную в reader/source/oa_terms. Ответ на запрос /v3/accounts/{accountID}/instruments
 */
struct InstrumentsResponse
{	// List of tradeable instruments for the given Account. The list of tradeable instruments is dependent on the regulatory division that the Account is located in, thus should be the same for all Accounts owned by a single user.
	glb::CFxArray<Instrument, false> instruments;
	// The ID of the last Transaction created for the Account.
	TransactionID lastTransactionID;
	/*
	 * функцыонал
	 */
	InstrumentsResponse() : instruments(16) {
		lastTransactionID = -1;
	}
	int InvalidElementID() const {
		return((lastTransactionID < 0) ? OA_last_Transaction_ID : !instruments.GetCount() ? OA_instruments : 0);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
};

/*
 * Set the client-configurable portions of an Account. PATCH: /v3/accounts/{accountID}/configuration
 */
struct AccountConfigurationResponse
{	// The transaction that configures the Account.
	ClientConfigureTransaction clientConfigureTransaction;
	// The ID of the last Transaction created for the Account.
	TransactionID lastTransactionID;
};

/*
 * 400: The configuration specification was invalid. 403: The configuration operation was forbidden on the Account
 */
struct AccountConfigurationErrorResponse
{	// The transaction that rejects the configuration of the Account.
	ClientConfigureRejectTransaction clientConfigureRejectTransaction;
	// The ID of the last Transaction created for the Account.
	TransactionID lastTransactionID;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * Ответ на запрос /v3/instruments/{instrument}/orderBook
 */
struct OrderBookResponse
{	// The instrument’s order book
	OrderBook orderBook;
};

/*
 * Ответ на запрос /v3/instruments/{instrument}/positionBook
 */
struct PositionBookResponse
{	// The instrument’s position book
	PositionBook positionBook;
};

/*
 * 201 (The Order was created as specified): Положительный ответ на запрос POST: /v3/accounts/{accountID}/orders
 */
struct CreateOrderPositiveResponse
{	// The Transaction that created the Order specified by the request.
	Transaction orderCreateTransaction;
	// The Transaction that filled the newly created Order. Only provided when the Order was immediately filled.
	OrderFillTransaction orderFillTransaction;
	// The Transaction that cancelled the newly created Order. Only provided when the Order was immediately cancelled.
	OrderCancelTransaction orderCancelTransaction;
	// The Transaction that reissues the Order. Only provided when the Order is configured to be reissued for its remaining units after a partial fill and the reissue was successful.
	Transaction orderReissueTransaction;
	// The Transaction that rejects the reissue of the Order. Only provided when the Order is configured to be reissued for its remaining units after a partial fill and the reissue was rejected.
	Transaction orderReissueRejectTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 (The Order specification was invalid), или  (The Order or Account specified does not exist): Отрицательный ответ на запрос POST: /v3/accounts/{accountID}/orders
 */
struct CreateOrderNegativeResponse
{	// The Transaction that rejected the creation of the Order as requested
	Transaction orderRejectTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * Get a list of Orders for an Account. Запрос GET: /v3/accounts/{accountID}/orders
 */
struct GetOrdersResponse
{	// The list of Order detail objects
	glb::CFxArray<Order, false> orders;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get details for a single Order in an Account. Запрос GET: /v3/accounts/{accountID}/orders/{orderSpecifier}
 */
struct GetOrderDetailsResponse
{	// The details of the Order requested
	Order order;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Replace an Order in an Account by simultaneously cancelling it and creating a replacement Order. Запрос: PUT: /v3/accounts/{accountID}/orders/{orderSpecifier}
 */
struct ReplaceOrderPositiveResponse
{	// The Transaction that cancelled the Order to be replaced.
	OrderCancelTransaction orderCancelTransaction;
	// The Transaction that created the replacing Order as requested.
	Transaction orderCreateTransaction;
	// The Transaction that filled the replacing Order. This is only provided when the replacing Order was immediately filled.
	OrderFillTransaction orderFillTransaction;
	// The Transaction that reissues the replacing Order. Only provided when the replacing Order was partially filled immediately and is configured to be reissued for its remaining units.
	Transaction orderReissueTransaction;
	// The Transaction that rejects the reissue of the Order. Only provided when the replacing Order was partially filled immediately and was configured to be reissued, however the reissue was rejected.
	Transaction orderReissueRejectTransaction;
	// The Transaction that cancelled the replacing Order. Only provided when the replacing Order was immediately cancelled.
	OrderCancelTransaction replacingOrderCancelTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 - The Order specification was invalid.
 */
struct ReplaceOrderNegativeResponse400
{	// The Transaction that rejected the creation of the replacing Order
	Transaction orderRejectTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account.
	TransactionID lastTransactionID;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * 404 - The Account or Order specified does not exist.
 */
struct ReplaceOrderNegativeResponse404
{	// The Transaction that rejected the cancellation of the Order to be replaced. Only present if the Account exists.
	Transaction orderCancelRejectTransaction;
	// The IDs of all Transactions that were created while satisfying the request. Only present if the Account exists.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account. Only present if the Account exists.
	TransactionID lastTransactionID;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * 200: Cancel a pending Order in an Account. Запрос: PUT /v3/accounts/{accountID}/orders/{orderSpecifier}/cancel
 */
struct CancelPendingOrderPositiveResponse
{	// The Transaction that cancelled the Order
	OrderCancelTransaction orderCancelTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 200: Update the Client Extensions for an Order in an Account. Do not set, modify, or delete clientExtensions if your account is associated with MT4. Запрос: PUT /v3/accounts/{accountID}/orders/{orderSpecifier}/clientExtensions
 */
struct UpdateClientExtensionsPositiveResponse
{	// The Transaction that modified the Client Extensions for the Order
	OrderClientExtensionsModifyTransaction orderClientExtensionsModifyTransaction;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
};

/*
 * 400 (The Order Client Extensions specification was invalid) и 404 (The Account or Order specified does not exist)
 */
struct UpdateClientExtensionsNegativeResponse
{	// The Transaction that rejected the modification of the Client Extensions for the Order
	OrderClientExtensionsModifyRejectTransaction orderClientExtensionsModifyRejectTransaction;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * Get a list of Trades for an Account. Запрос: GET /v3/accounts/{accountID}/trades|openTrades
 */
struct GetTradesResponse
{	// The list of Trade detail objects
	glb::CFxArray<Trade, false> trades;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get the details of a specific Trade in an Account. Запрос: /v3/accounts/{accountID}/trades/{tradeSpecifier}
 */
struct GetTradeDetailsResponse
{	// The details of the requested trade
	Trade trade;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Close (partially or fully) a specific open Trade in an Account. Запрос: /v3/accounts/{accountID}/trades/{tradeSpecifier}/close
 */
struct CloseTradePositiveResponse
{	// The MarketOrder Transaction created to close the Trade.
	MarketOrderTransaction orderCreateTransaction;
	// The OrderFill Transaction that fills the Trade-closing MarketOrder and closes the Trade.
	OrderFillTransaction orderFillTransaction;
	// The OrderCancel Transaction that immediately cancelled the Trade-closing MarketOrder.
	OrderCancelTransaction orderCancelTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 - The Trade cannot be closed as requested.
 */
struct CloseTradeNegativeResponse400
{	// The MarketOrderReject Transaction that rejects the creation of the Trade- closing MarketOrder.
	MarketOrderRejectTransaction orderRejectTransaction;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * 404 - The Account or Trade specified does not exist.
 */
struct CloseTradeNegativeResponse404
{	// The MarketOrderReject Transaction that rejects the creation of the Trade- closing MarketOrder. Only present if the Account exists.
	MarketOrderRejectTransaction orderRejectTransaction;
	// The ID of the most recent Transaction created for the Account. Only present if the Account exists.
	TransactionID lastTransactionID;
	// The IDs of all Transactions that were created while satisfying the request. Only present if the Account exists.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * Update the Client Extensions for a Trade. Do not add, update, or delete the Client Extensions if your account is associated with MT4. Запрос: PUT /v3/accounts/{accountID}/trades/{tradeSpecifier}/clientExtensions
 */
struct UpdateTradeClientExtensionsPositiveResponse
{	// The Transaction that updates the Trade’s Client Extensions.
	TradeClientExtensionsModifyTransaction tradeClientExtensionsModifyTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 (The Trade’s Client Extensions cannot be modified as requested) и 404 (The Account or Trade specified does not exist).
 */
struct UpdateTradeClientExtensionsNegativeResponse
{	// The Transaction that rejects the modification of the Trade’s Client Extensions.
	TradeClientExtensionsModifyRejectTransaction tradeClientExtensionsModifyRejectTransaction;
	// The ID of the most recent Transaction created for the Account.
	TransactionID lastTransactionID;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * 200 - The Trade’s dependent Orders have been modified as requested. Запрос: PUT /v3/accounts/{accountID}/trades/{tradeSpecifier}/orders
 */
struct TradesDependentOrderPositiveResponse
{	// The Transaction created that cancels the Trade’s existing Take Profit Order.
	OrderCancelTransaction takeProfitOrderCancelTransaction;
	// The Transaction created that creates a new Take Profit Order for the Trade.
	TakeProfitOrderTransaction takeProfitOrderTransaction;
	// The Transaction created that immediately fills the Trade’s new Take Profit Order. Only provided if the new Take Profit Order was immediately filled.
	OrderFillTransaction takeProfitOrderFillTransaction;
	// The Transaction created that immediately cancels the Trade’s new Take Profit Order. Only provided if the new Take Profit Order was immediately cancelled.
	OrderCancelTransaction takeProfitOrderCreatedCancelTransaction;
	// The Transaction created that cancels the Trade’s existing Stop Loss Order.
	OrderCancelTransaction stopLossOrderCancelTransaction;
	// The Transaction created that creates a new Stop Loss Order for the Trade.
	StopLossOrderTransaction stopLossOrderTransaction;
	// The Transaction created that immediately fills the Trade’s new Stop Order. Only provided if the new Stop Loss Order was immediately filled.
	OrderFillTransaction stopLossOrderFillTransaction;
	// The Transaction created that immediately cancels the Trade’s new Stop Loss Order. Only provided if the new Stop Loss Order was immediately cancelled.
	OrderCancelTransaction stopLossOrderCreatedCancelTransaction;
	// The Transaction created that cancels the Trade’s existing Trailing Stop Loss Order.
	OrderCancelTransaction trailingStopLossOrderCancelTransaction;
	// The Transaction created that creates a new Trailing Stop Loss Order for the Trade.
	TrailingStopLossOrderTransaction trailingStopLossOrderTransaction;
	// The Transaction created that cancels the Trade’s existing Guaranteed Stop Loss Order.
	OrderCancelTransaction guaranteedStopLossOrderCancelTransaction;
	// The Transaction created that creates a new Guaranteed Stop Loss Order for the Trade.
	GuaranteedStopLossOrderTransaction guaranteedStopLossOrderTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 - The Trade’s dependent Orders cannot be modified as requested.
 */
struct TradesDependentOrderNegativeResponse
{	// An OrderCancelRejectTransaction represents the rejection of the cancellation of an Order in the client’s Account.
	OrderCancelRejectTransaction takeProfitOrderCancelRejectTransaction;
	// A TakeProfitOrderRejectTransaction represents the rejection of the creation of a TakeProfit Order.
	TakeProfitOrderRejectTransaction takeProfitOrderRejectTransaction;
	// An OrderCancelRejectTransaction represents the rejection of the cancellation of an Order in the client’s Account.
	OrderCancelRejectTransaction stopLossOrderCancelRejectTransaction;
	// A StopLossOrderRejectTransaction represents the rejection of the creation of a StopLoss Order.
	StopLossOrderRejectTransaction stopLossOrderRejectTransaction;
	// An OrderCancelRejectTransaction represents the rejection of the cancellation of an Order in the client’s Account.
	OrderCancelRejectTransaction trailingStopLossOrderCancelRejectTransaction;
	// A TrailingStopLossOrderRejectTransaction represents the rejection of the creation of a TrailingStopLoss Order.
	TrailingStopLossOrderRejectTransaction trailingStopLossOrderRejectTransaction;
	// An OrderCancelRejectTransaction represents the rejection of the cancellation of an Order in the client’s Account.
	OrderCancelRejectTransaction guaranteedStopLossOrderCancelRejectTransaction;
	// A GuaranteedStopLossOrderRejectTransaction represents the rejection of the creation of a GuaranteedStopLoss Order.
	GuaranteedStopLossOrderRejectTransaction guaranteedStopLossOrderRejectTransaction;
	// The ID of the most recent Transaction created for the Account.
	TransactionID lastTransactionID;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * List all Positions for an Account. The Positions returned are for every instrument that has had a position during the lifetime of an the Account. Запрос: GET /v3/accounts/{accountID}/positions
 */
struct GetPositionsResponse
{	// The list of Account Positions.
	glb::CFxArray<Position, false> positions;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get the details of a single Instrument’s Position in an Account. The Position may by open or not. Запрос: GET /v3/accounts/{accountID}/positions/{instrument}
 */
struct GetPositionResponse
{	// The requested Position.
	Position position;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Closeout the open Position for a specific instrument in an Account. Запрос: PUT /v3/accounts/{accountID}/positions/{instrument}/close
 */
struct ClosePositionPositiveResponse
{	// The MarketOrderTransaction created to close the long Position.
	MarketOrderTransaction longOrderCreateTransaction;
	// OrderFill Transaction that closes the long Position
	OrderFillTransaction longOrderFillTransaction;
	// OrderCancel Transaction that cancels the MarketOrder created to close the long Position
	OrderCancelTransaction longOrderCancelTransaction;
	// The MarketOrderTransaction created to close the short Position.
	MarketOrderTransaction shortOrderCreateTransaction;
	// OrderFill Transaction that closes the short Position
	OrderFillTransaction shortOrderFillTransaction;
	// OrderCancel Transaction that cancels the MarketOrder created to close the short Position
	OrderCancelTransaction shortOrderCancelTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * 400 (The Parameters provided that describe the Position closeout are invalid) и 404 (The Account or one or more of the Positions specified does not exist).
 */
struct ClosePositionNegativeResponse
{	// The Transaction created that rejects the creation of a MarketOrder to close the long Position.
	MarketOrderRejectTransaction longOrderRejectTransaction;
	// The Transaction created that rejects the creation of a MarketOrder to close the short Position.
	MarketOrderRejectTransaction shortOrderRejectTransaction;
	// The IDs of all Transactions that were created while satisfying the request.
	glb::CFxArray<TransactionID, true> relatedTransactionIDs;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
	// The code of the error that has occurred. This field may not be returned for some errors.
	string errorCode;
	// The human-readable description of the error that has occurred. required)
	string errorMessage;
};

/*
 * Get a list of Transactions pages that satisfy a time-based Transaction query. Запрос: /v3/accounts/{accountID}/transactions
 */
struct GetTransactionsResponse
{	// The starting time provided in the request.
	DateTime from;
	// The ending time provided in the request.
	DateTime to;
	// The pageSize provided in the request
	integer pageSize;
	// The Transaction-type filter provided in the request
	glb::CFxArray<TransactionFilter, true> type;
	// The number of Transactions that are contained in the pages returned
	integer count;
	// The list of URLs that represent idrange queries providing the data for each page in the query results
	glb::CFxArray<string, false> pages;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get the details of a single Account Transaction. Запрос: /v3/accounts/{accountID}/transactions/{transactionID}
 */
struct GetAccountResponse
{	// The details of the Transaction requested
	Transaction transaction;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get a range of Transactions for an Account based on the Transaction IDs. Запросы: /v3/accounts/{accountID}/transactions/idrange, /v3/accounts/{accountID}/transactions/sinceid
 */
struct GetTransactionsByIDsResponse
{	// The list of Transactions that satisfy the request.
	glb::CFxArray<Transaction, false> transactions;
	// The ID of the most recent Transaction created for the Account
	TransactionID lastTransactionID;
};

/*
 * Get dancing bears and most recently completed candles within an Account for specified combinations of instrument, granularity, and price component. Запрос: /v3/accounts/{accountID}/candles/latest
 */
struct GetRecentCandlesResponse
{	// The latest candle sticks.
	glb::CFxArray<CandlestickDataResponse, false> latestCandles;
};

/*
 * Get pricing information for a specified list of Instruments within an Account. Запрос: /v3/accounts/{accountID}/pricing
 */
struct GetPricingResponse
{	// The list of Price objects requested. required),
	glb::CFxArray<ClientPrice, false> prices;
	// The list of home currency conversion factors requested. This field will only be present if includeHomeConversions was set to true in the request.
	glb::CFxArray<HomeConversions, false> homeConversions;
	// The DateTime value to use for the “since” parameter in the next poll request.
	DateTime time;
};

} // namespace oatp