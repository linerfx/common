#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FxArray.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FxArray.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#ifdef __llvm__
#pragma clang diagnostic ignored "-Wdangling-else"
#pragma clang diagnostic ignored "-Wlogical-op-parentheses"
#endif
// FxArray.h: interface for the CFxArray class.
//
//////////////////////////////////////////////////////////////////////

#include "defs.h"
#include <limits.h>
#include <memory.h>
//#if defined __ITOG || defined __ITOGCLNT
#include "global.h"
#include "exception.h"
#include "strdef.h"
//#endif

#if defined __DEBUG && defined __LINER
extern thread_local uint32_t g_nDbgNoLnsCheck;	// debug, batch lines delete/reindex
#endif

namespace glb {
const char* FileName(const char *pcszPath, int32_t *pnLen);

/* массив эл-тов TP, каждый из которых либо базового (integral) типа (bIntType) или нет (!bIntType)
 * Для файловых операций на эл-т массива накладываются следующие ограничения:
 * 1. конструкто по-умолчанию (без аргументов)
 * 2. оператор присваивания '='
 * ф-ции для файловых операций, если такие есть:
 * 3. int32_t putcp(long int32_t at, fl::CBinFile &file) const;
 * 4. int32_t getcp(long int32_t at, fl::CBinFile &file);
 * эл-т этого массива переменной длины должен иметь ф-ции ввода/вывода
 * 5. int32_t GetBinData(CFxArray<BYTE, true> &a) const;
 * 6. int32_t SetBinData(const BYTE *p, const int32_t i);
 */
template <class TP, bool bIntType>
class CFxArray
{
public:
	typedef CFxArray<TP, bIntType> THIS;
	typedef const CFxArray<TP, bIntType> CTHIS;
protected:
	TP *m_pData;			// 64 бит
	int32_t m_iCount,		// 32 бит
		m_iMem,				// 32 бит
		m_iHeir;			// выравнивание до 64 бит, наследник может пользоваться
	const int32_t m_ciGrow;	// 32 бит
public:// создание/удаление
	CFxArray();				// по-умолчанию - для массивов
	CFxArray(CTHIS &a);		// копирования - тоже д.массивов
	CFxArray(THIS &&a);		// перемещение (move constuctor) C++11
	CFxArray(int32_t iGrow);// дежурный, индивидуальнй подход
	CFxArray(const TP *pt, int32_t cnt = 1);
	~CFxArray();
public:// управление
	virtual int32_t Add(const TP *pitem, int32_t nCount = 1);
	virtual int32_t Add(const TP &obj);
	virtual int32_t AddND(const TP *pitem, int32_t nCount = 1);
	virtual int32_t AddND(const TP &obj);
	virtual int32_t SetND(CTHIS &obj);
	virtual void RemAt(const int32_t iInd, const int32_t iCnt = 1);
	virtual void RemND(const int32_t iInd, const int32_t iCnt = 1);
	virtual void Insert(int32_t iAt, const TP &o);
	virtual void Insert(int32_t iAt, const TP *pa, int32_t cnt);
	virtual void Edit(int32_t iAt, const TP *pi, int32_t cnt);
	virtual void Move(int32_t iTo, int32_t iFrom);
    virtual void FreeRedundant();
	virtual void SetCountZeroND();
	virtual void SetCountND(int32_t nCount);
	virtual void SetCount(int32_t iNewCount);
	virtual TP& Last();
	virtual const TP& Last() const;
	virtual void Attach(CFxArray<TP, bIntType> & a);
	virtual TP* Detach();
	virtual void AllocND(int32_t n);
	// файловые операции только для объектов статич.размера. Для объектов динамич.размера см. glb::CvArray
	virtual int32_t GetBinData(CFxArray<BYTE, true> &a) const;
	virtual int32_t SetBinData(CBYTE *p, int32_t n);
	virtual int32_t SetBinData(const CFxArray<BYTE, true> &a);
	// атрибуты
	virtual int32_t GetCount() const;
	int32_t GetObjectsAlloc() const;
	TP& operator [](cint32_t i);
	const TP& operator [](cint32_t i) const;
	virtual operator const TP*() const;
	virtual operator TP*();
	virtual const TP* data(int32_t &cnt) const;
	virtual TP* data(int32_t &cnt);
	virtual CTHIS& operator =(CTHIS &arr);
	virtual THIS& operator =(THIS &&a);
};

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::CFxArray() : 
	m_ciGrow(8)
{
    m_iMem = m_iCount = 0;
    m_pData = nullptr;
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::CFxArray(int32_t iGrow) :
    m_ciGrow(iGrow)
{
    ASSERT(m_ciGrow > 0);
    m_iMem = m_iCount = 0;
	m_pData = nullptr;
}

template<class TP, bool bIntType>
CFxArray<TP, bIntType>::CFxArray(const TP *pt, int32_t cnt)
	: m_ciGrow(8)
	, m_pData(nullptr)
	, m_iMem(0)
	, m_iCount(0)
{
	ASSERT(pt);
	Add(pt, cnt);
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::CFxArray(CTHIS &a) :
	m_ciGrow(a.m_ciGrow)
	, m_pData(nullptr)
	, m_iMem(0)
	, m_iCount(0)
{
	*this = a;
}

template<class TP, bool bIntType>
CFxArray<TP, bIntType>::CFxArray(THIS &&a)
	: m_ciGrow(a.m_ciGrow)
{
	Attach(a);
}

template <class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::GetCount() const
{
    return m_iCount;
}

template <class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::GetObjectsAlloc() const
{
    return m_iMem;
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::~CFxArray()
{
    if(m_pData)
        delete [] m_pData;
	m_pData = nullptr;
	m_iCount = m_iMem = 0;
}

template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::Attach(CFxArray<TP, bIntType> &a)
{
	if (m_pData && a.m_pData != m_pData)
        delete [] m_pData;
    m_pData = a.m_pData;
    m_iCount = a.m_iCount;
    m_iMem = a.m_iMem;
    a.m_pData = nullptr;
    a.m_iCount = 0;
	a.m_iMem = 0;
}

template<class TP, bool bIntType>
TP *CFxArray<TP, bIntType>::Detach()
{
	TP *p = m_pData;
	m_pData = nullptr;
	m_iMem = m_iCount = 0;
	return p;
}

//////////////////////////////////////////////////////////////
/// \brief выделяет память, если необходимо, ставит к-во в нуль
///
template<class TP, bool bIntType>
void CFxArray<TP, bIntType>::AllocND(int32_t n)
{
	ASSERT(n >= 0);
	if (m_iMem < n)
		CFxArray<TP, bIntType>::SetCount(n); // ф-ция этого, то бишь базового класса (см. CFxString)
	m_iCount = 0;
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArray<TP, bIntType>::GetBinData записывает содержимое объекта в 'a' в таком формате:
/// 1. к-во (m_iCount) элементов
/// 2. последовательно, все эл-ты. Эл-т должен описывать ф-цию 'int32_t GetBinData(CFxArray<BYTE, true>&)'
/// \param a - ИЗ: в этот массив добавляется содержимое этого массива
/// \return к-во записанных байт.
///
template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::GetBinData(CFxArray<BYTE, true> &a) const
{
	ASSERT(bIntType);		// см. CvArray<TP>
	int32_t n = GetCount();	// может отличаться от m_iCount (см. CFxString)
	a.AddND((CBYTE*) &n, sizeof n);
	if (n)
		a.AddND((CBYTE*) m_pData, n * sizeof(TP));
	return (sizeof n + n * sizeof(TP));
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArray<TP, bIntType>::SetBinData возвращает обратно содержимое объекта,
/// ранее извлечённое ф-цией GetBinData.
/// Только для целых объектов, для массива объектов динамич.длины объявляй CvArray из
/// CFxVArray.h там переопределены файловые ф-ции.
/// \param p - В: массив, содержащий исчерпывающие сведения
/// \param cnt - В: к-во BYTE в 'p'.
/// \return к-во байт прочитанных в 'a', или -1, если в 'p' их недостаточно.
///
template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::SetBinData(CBYTE *cpb, int32_t cnt)
{
	ASSERT(bIntType);// для объектов динамич.размера см. CvArray::SetBinData
	int32_t n, i = read_tp(n, cpb, cnt);
	if (n < 0) // мусор?
		throw err::CLocalException(SI_NED, time(NULL), FileName(__FILE__, nullptr), __LINE__);
	SetCountND(n); // m_iCount может отличаться от 'n' см. CFxString
	return (!n ? i : i + read_raw((BYTE*) m_pData, n * sizeof(TP), cpb + i, cnt - i));
}

template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::SetBinData(const CFxArray<BYTE, true> &a)
{
   return SetBinData(a, a.GetCount());
}

template <class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::Add(const TP *pitem, int32_t nCount)	// nCount == 1
{
	ASSERT(pitem);
	int32_t i, j, iIndex = m_iCount;
    SetCount(m_iCount + nCount);
	if (!bIntType)  {
		for (i = iIndex, j = 0; i < m_iCount; i++, j++)
            m_pData[i] = pitem[j];
        ASSERT(j == nCount);
	} else
        memcpy(m_pData + iIndex, pitem, sizeof(TP) * nCount);
	return (m_iCount - nCount);	// индекс первого добавленного эл-та
}

template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::Add(const TP &obj)
{
	SetCount(m_iCount + 1);
	m_pData[m_iCount - 1] = obj;
	return (m_iCount - 1);	// индекс добавленного эл-та
}

template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::AddND(const TP *pitem, int32_t cnt) // nCount = 1
{
	int32_t i, j, nOldCnt = m_iCount;
	SetCountND(nOldCnt + cnt);
	if (!bIntType) {
		for(i = nOldCnt, j = 0; i < m_iCount; i++, j++)
			m_pData[i] = pitem[j];
		ASSERT(j == cnt);
	} else
		memcpy(m_pData + nOldCnt, pitem, sizeof(TP) * cnt);
	return nOldCnt;	// индекс первого добавленного эл-та
}

template<class TP, bool bIntType>
int32_t CFxArray<TP, bIntType>::AddND(const TP &obj)
{
	SetCountND(m_iCount + 1);
	m_pData[m_iCount - 1] = obj;
	return (m_iCount - 1);	// индекс добавленного эл-та
}

template<class TP, bool bIntType>
inline int32_t CFxArray<TP, bIntType>::SetND(CTHIS &obj)
{
	SetCountND(obj.m_iCount);
	if (bIntType)
		memcpy(m_pData, obj.m_pData, sizeof(TP) * m_iCount);
	else for(int32_t i = 0; i < m_iCount; i++)
		m_pData[i] = obj.m_pData[i];
	return m_iCount;	// к-во эл-тов
}

template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::RemAt(const int32_t iInd, const int32_t iCnt) // iCnt = 1
{
	ASSERT(iInd >= 0 && iCnt > 0 && iInd + iCnt <= m_iCount);
	if (m_iCount - iInd > iCnt) {
		if (!bIntType) {
			for (int32_t i = iInd; i < m_iCount - iCnt; i++)
                m_pData[i] = m_pData[i + iCnt];
		} else
            memcpy(m_pData + iInd, m_pData + iInd + iCnt, (m_iCount - iInd - iCnt) * sizeof(TP));
	}// здесь нужно вызывать GetCount() т.к. в CFxString её значение на 1 меньше чем m_iCount.
	CFxArray<TP, bIntType>::SetCount(Mx<int32_t>(GetCount() - iCnt, 0));
}

/* RemND (no delete) то же что RemAt, только не перераспределяет
 * массив в памяти. Добавлено 13.05.2021, т.е. лет через 5 после RemAt,
 * а то и больше.
 */
template<class TP, bool bIntType>
void CFxArray<TP, bIntType>::RemND(const int32_t iAt, const int32_t iCnt) // iCnt = 1
{
	ASSERT(iAt >= 0 && iAt < m_iCount);
	ASSERT(iCnt > 0);
	if (m_iCount - iAt > iCnt)
		if (!bIntType)
			for (int32_t i = iAt; i < m_iCount - iCnt; i++)
				m_pData[i] = m_pData[i + iCnt];
		else
			memcpy(m_pData + iAt, m_pData + iAt + iCnt, (m_iCount - iAt - iCnt) * sizeof(TP));
	// вынужден вызывать GetCount() т.к. в CFxString её значение на 1 меньше чем m_iCount.
	CFxArray<TP, bIntType>::SetCountND(Mx(GetCount() - iCnt, 0));
}

template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::SetCount(int32_t iNewCount)
{
    ASSERT(m_iCount >= 0 && m_iCount <= m_iMem);
    ASSERT(iNewCount >= 0);
	int32_t iNewMem = (iNewCount % m_ciGrow) ? iNewCount + m_ciGrow - iNewCount % m_ciGrow : iNewCount;
    if(!iNewMem && m_iMem)
    {
        delete [] m_pData;
        m_pData = nullptr;
    }
    else if(iNewMem != m_iMem)
    {
        TP *pItem = new TP [iNewMem]; // швырнет bad_alloc в случае ошибки. Лови!
        if(m_iMem)
        {
            if(m_iCount)
            {
                if(!bIntType)
                {
					for(int32_t i = MIN(m_iCount, iNewMem) - 1; i >= 0; i--)
                        pItem[i] = m_pData[i];
                }
                else
					memcpy(pItem, m_pData, sizeof(TP) * MIN(m_iCount, iNewMem));
            }
            delete [] m_pData;
        }
        m_pData = pItem;
    }
    m_iCount = iNewCount;
    m_iMem = iNewMem;
}

// FreeRedundant освободит неиспользуемую память
template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::FreeRedundant()
{
	int32_t cnt = (m_iCount / m_ciGrow + (!m_iCount || m_iCount % m_ciGrow != 0)) * m_ciGrow;
    ASSERT(cnt > 0 && cnt >= m_iCount && m_iCount <= m_iMem && cnt <= m_iMem);
    ASSERT((cnt < m_iMem) ? (m_iMem % cnt == 0) : (cnt % m_iMem == 0));
    if(cnt < m_iMem)
    {
        TP *p = new TP [cnt];
        if(!bIntType)
        {
			for(int32_t i = 0; i < m_iCount; i++)
                p[i] = m_pData[i];
        }
        else if(m_iCount)
            memcpy(p, m_pData, m_iCount * sizeof(TP));
        delete m_pData;
        m_pData = p;
        m_iMem = cnt;
    }
}

template <class TP, bool bIntType>
const CFxArray<TP, bIntType>& CFxArray<TP, bIntType>::operator =(CTHIS& arr)
{
    m_iCount = 0;
    SetCount(arr.m_iCount);
    if(!bIntType)
    {
		for(int32_t i = 0; i < m_iCount; i++)
            m_pData[i] = arr.m_pData[i];
    }
    else if(m_iCount)
        memcpy(m_pData, arr.m_pData, sizeof(TP) * m_iCount);
	return *this;
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>& CFxArray<TP, bIntType>::operator =(THIS &&a)
{
	Attach(a);
	return *this;
}

template <class TP, bool bIntType>
TP & CFxArray<TP, bIntType>::operator [](cint32_t i)
{
    ASSERT(i >= 0 && i < m_iCount);
    return m_pData[i];
}

template <class TP, bool bIntType>
const TP & CFxArray<TP, bIntType>::operator [](cint32_t i) const
{
    ASSERT(i >= 0 && i < m_iCount);
    return m_pData[i];
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArray::Insert вставит 1 эл-т
/// \param i: куда вставлять новый эл-т
/// \param o: вставляемый эл-т
/// \param bnd: если "трю", не буду перераспределять память, если её к-во достаточное
///
template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::Insert(int32_t iAt, const TP &o)
{
	ASSERT(iAt >= 0 && iAt <= m_iCount);
	// вирт.ф-ции Get/SetCount, в CFxString подход отличается
	SetCountND(m_iCount + 1);
	if (!bIntType) {
		for(int32_t j = m_iCount - 1; j > iAt; j--)
			m_pData[j] = m_pData[j - 1];
	} else if (m_iCount - iAt > 1)
		memmove(m_pData + iAt + 1, m_pData + iAt, (m_iCount - 1 - iAt) * sizeof(TP));
	m_pData[iAt] = o;
}

/*		  2
		012
		0123(4)
*/
template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::Insert(int32_t iAt, const TP *pa, int32_t cnt)
{
	ASSERT(iAt >= 0 && iAt <= m_iCount);
    ASSERT(cnt > 0 && pa != nullptr);
	SetCountND(m_iCount + cnt);
	if (!bIntType) {
		int32_t i;
		for (i = m_iCount - 1; i >= iAt + cnt; i--)
            m_pData[i] = m_pData[i - cnt];
		for (i = 0; i < cnt; iAt++, i++)
			m_pData[iAt] = pa[i];
	} else {
		if (m_iCount - cnt > iAt)
			memmove(m_pData + iAt + cnt, m_pData + iAt, (m_iCount - cnt - iAt) * sizeof(TP));
		memcpy(m_pData + iAt, pa, cnt * sizeof(TP));
	}
}

template<class TP, bool bIntType>
void CFxArray<TP, bIntType>::Edit(int32_t iAt, const TP *pi, int32_t cnt)
{
	ASSERT(iAt + cnt <= m_iCount);
	if (bIntType)
		memcpy(m_pData + iAt, pi, cnt * sizeof(TP));
	else for (int32_t i = 0; i < cnt; i++)
		m_pData[iAt + i] = pi[i];
}

// перемещает объект, смещая промежуточные
/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArray::Move перемещает объект из одной позиции в другую.
/// \param iTo: позиция в которую перемещается объект
/// \param iFrom: позиция, из которой перемещается объект
///
template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::Move(int32_t iTo, int32_t iFrom)
{
    ASSERT(iTo >= 0 && iTo < m_iCount && iFrom >= 0 && iFrom < m_iCount && iTo != iFrom);
	TP o = m_pData[iFrom];
	if (!bIntType) {
		for (; iTo < iFrom; iFrom--)
			m_pData[iFrom] = m_pData[iFrom - 1];
		for (; iTo > iFrom; iTo++)
			m_pData[iFrom] = m_pData[iFrom + 1];
	} else if (iTo < iFrom)
		memmove(m_pData + iTo + 1, m_pData + iTo, (iFrom - iTo) * sizeof *m_pData);
	else
		memcpy(m_pData + iFrom, m_pData + iFrom + 1, (iTo - iFrom) * sizeof *m_pData);
	m_pData[iTo] = o;
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::operator const TP* () const
{
    return m_pData;
}

template <class TP, bool bIntType>
CFxArray<TP, bIntType>::operator TP* ()
{
	return m_pData;
}

template<class TP, bool bIntType>
const TP* CFxArray<TP, bIntType>::data(int32_t &cnt) const
{
	cnt = m_iCount;
	return m_pData;
}

template<class TP, bool bIntType>
TP* CFxArray<TP, bIntType>::data(int32_t &cnt)
{
	cnt = m_iCount;
	return m_pData;
}

template <class TP, bool bIntType>
const TP & CFxArray<TP, bIntType>::Last() const
{
    ASSERT(m_iCount > 0);
    return m_pData[m_iCount - 1];
}

template <class TP, bool bIntType>
TP & CFxArray<TP, bIntType>::Last()
{
    ASSERT(m_iCount > 0);
    return m_pData[m_iCount - 1];
}

template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::SetCountZeroND()
{
    m_iCount = 0;
}

// ислючаю 1 или несколько элементов массива с конца без освобождения памяти
template <class TP, bool bIntType>
void CFxArray<TP, bIntType>::SetCountND(int32_t nCount)
{
	ASSERT(nCount >= 0);
	if (m_iMem < nCount)
		CFxArray<TP, bIntType>::SetCount(nCount); // ф-ция этого, то бишь базового класса (см. CFxString)
	m_iCount = nCount;
}

}// namespace glb

typedef glb::CFxArray<BYTE, true> BYTES;
typedef const glb::CFxArray<BYTE, true> CBYTES;

namespace glb {
/* Массив - объект массива CFxArrayInd
 */
/*template <class TP, bool bIntType>
class CFxArrayIO : public CFxArray<TP, bIntType>
{
public:
	CObjID m_id;
public:
	virtual const CFxArrayIO<TP, bIntType>& operator =(const CFxArrayIO<TP, bIntType>& arr) {
		CFxArray<TP, bIntType>::operator =(arr);
		m_id = arr.m_id;
		return *this;
	}
public:// создание/удаление
	CFxArrayIO(int32_t iGrow) : CFxArray<TP, bIntType>(iGrow) { }
	CFxArrayIO(const CFxArrayIO<TP, bIntType> &arr) : CFxArray<TP, bIntType>(arr) {
		*this = arr;
	}
	bool IsValid() const {
		return (CFxArray<TP, bIntType>::m_pData && CFxArray<TP, bIntType>::m_iCount > 0);
	}
	void Invalidate() {
		CFxArray<TP, bIntType>::SetCountZeroND();
		m_id.Init();
	}
};*/

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd - базовый класс для индексированных массивов. Для быстрого доступа используются
/// ф-ции поиска в упорядоченном массиве по индексу.
/// Ограничения для объектов, CFxArrayInd указаны в описании CFxArrayIndItem
/// Удаление элемента:
/// Ид-р удалённого эл-та помещается в m_del, оставаясь на своём месте во всех индексах. Если
/// эл-т удаляется со сменой индексированных значений, то нужно выполнять его переиндексацию с
/// новыми значениями. Т.е. удалённые эл-ты тоже индексируется.
/// \param TP: может быть эл-том базового типа или соответствовать CFxArrayIndItem
/// \param nIndices: к-во индексов
/// \param bIntTyte:
///
template <class TP, int32_t nIndices, bool bIntType>
class CFxArrayInd : public CFxArray<TP, bIntType>
{
public:// типы этого класса
	typedef CFxArrayInd<TP, nIndices, bIntType> THIS;
	typedef CFxArray<TP, bIntType> BAS;
	typedef int32_t (*pfcmp)(const TP&, const TP&);
	typedef int32_t (*pfcmpp)(const TP*, const TP*);

    class CIndex
    {
    public:
		int32_t *m_pi;	// массив физ.индексов элементов, упорядоченный в соответствии с m_fcmp
		pfcmp m_fcmp;	// ф-ция сравнения эл-тов массива
    public:
		// изменение к-ва индексов (к-ва эл-тов массива)
		void Realloc(int32_t nValid, int32_t nNewCnt) {
			int32_t *p = nullptr;
			if (nNewCnt > 0)
				p = new int32_t [nNewCnt];
			if (m_pi) {
				if (nValid > 0)
					memcpy(p, m_pi, MIN(nValid, nNewCnt) * sizeof *m_pi);
                delete [] m_pi;
            }
            m_pi = p;
        }
	public:
		CIndex() : m_pi(nullptr), m_fcmp(nullptr) { }
        ~CIndex() {
            if(m_pi)
                delete [] m_pi;
        }
	};
protected:
	CIndex m_aInd[nIndices];		// индексы
	CFxArray<int32_t, true> m_del;	// индексы (в m_pData) удалённых эл-тов

public:// конструкцыя/деструкцыя, копирование
	CFxArrayInd();					//  может быть эл-том массива...
	CFxArrayInd(const THIS &a);
	CFxArrayInd(THIS &&a);
	CFxArrayInd(pfcmp *ppf);
	CFxArrayInd(int32_t iGrow, pfcmp *ppf);

	virtual void Attach(THIS &a);

	const THIS& operator =(const THIS& a);
	THIS& operator =(THIS &&a);
	~CFxArrayInd();

public:// наследование
	virtual int32_t Add(const TP *pitem, int32_t nCount = 1) override;
	virtual int32_t Add(const TP &o) override;
	virtual void Edit(int32_t iBas, const TP *pi, int32_t cnt = 1) override;
	virtual void Edit(int32_t iBas, const TP &on, int32_t *paio, int32_t *pain);
	virtual void SetCountZeroND() override;

protected:
	virtual void SetCount(int32_t iNewCount) override;
	// ниже ф-ции, котоые в этом массиве отсутствуют
	virtual void SetCountND(int32_t nCount) override;
	virtual int32_t AddND(const TP *pitem, int32_t nCount = 1) override;
	virtual int32_t AddND(const TP &obj) override;
	virtual void RemAt(cint32_t iInd, cint32_t iCnt = 1) override;
	virtual void RemND(cint32_t iInd, cint32_t iCnt = 1) override;
	virtual void Insert(int32_t iAt, const TP &o) override;
	virtual void Insert(int32_t iAt, const TP *pa, int32_t cnt) override;
	virtual void Move(int32_t iTo, int32_t iFrom) override;
	virtual void FreeRedundant() override;
	virtual TP &Last() override;
	virtual const TP& Last() const override;
	virtual int32_t SetBinData(CBYTE *p, int32_t n) override;
	virtual int32_t SetBinData(const CFxArray<BYTE, true> &a) override;

public:
	int32_t ResetIndex(const TP &o, cint32_t id, cint32_t current, cint32_t index);
	int32_t SetIndex(const TP &o, cint32_t index, cint32_t x);

public:
	void Remove(int32_t iInd, int32_t iItm);
	void Remove(const TP&);
	void Empty();
	int32_t DelCount() const;
	void Invalidate(const TP &o, int32_t iIndex = 0);
	void Invalidate(cint32_t id);
	void UndeleteItem(int32_t id);
	const TP& GetItem(int32_t index, int32_t iObj) const;
	TP& GetItem(int32_t index, int32_t iObj);
	int32_t GetValidCount() const;
	virtual int32_t FindEqual(const TP &o, cint32_t id, cint32_t index) const;
	virtual int32_t FindNearest(const TP &o, int32_t index) const;
	virtual int32_t FindNearest(const TP &o, int32_t index, int32_t iBegin, int32_t iEnd) const;
	int32_t FindValid(const TP &o, cint32_t iIndex, bool bNearest = false) const;
	int32_t Cmp(const int32_t iObj, const TP &o, const int32_t iIndex) const;
	int32_t ValidCount() const;
	int32_t BaseInd(int32_t iInd, int32_t iItm) const;
	virtual const CIndex* Index(int i) const;
/*#ifdef _DEBUG
		bool CheckLinesOrder(const int32_t cIndex, const int32_t cCurElt, const int32_t cPrevElt = -1);
#endif */// defined _DEBUG
#if defined __DEBUG && defined __LINER
	bool CheckLinesOrder(const int32_t cIndex, const int32_t cCurrElt, const int32_t cPrevElt);
#endif
};

template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::CFxArrayInd() :
    m_del(128)
{

}

// fcmp - указатель на массив функций сравнения элементов индекса. К-во элементов - nIndices
template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::CFxArrayInd(int32_t iGrow, pfcmp *ppf) :
	CFxArray<TP, bIntType>(iGrow),
    m_del(128)
{
	for (int32_t i = 0; i < nIndices; i++)
		m_aInd[i].m_fcmp = ppf[i];
}

template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::CFxArrayInd(pfcmp *ppf) :
	CFxArray<TP, bIntType>(64),
    m_del(128)
{
	for (int32_t i = 0; i < nIndices; i++)
		m_aInd[i].m_fcmp = ppf[i];
}

template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::CFxArrayInd(const THIS &a) :
	CFxArray<TP, bIntType>(dynamic_cast<const CFxArray<TP, bIntType>&>(a)),
    m_del(128)
{
	for (int32_t i = 0; i < nIndices; i++) {
		m_aInd[i].m_fcmp = a.m_aInd[i].m_fcmp;
		m_aInd[i].m_pi = new int32_t [BAS::m_iMem];
		memcpy(m_aInd[i].m_pi, a.m_aInd[i].m_pi, BAS::m_iCount * sizeof(int32_t));
	}
}

template<class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::CFxArrayInd(THIS &&a)
{
	Attach(a);
}

template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>::~CFxArrayInd()
{

}

template <class TP, int32_t nIndices, bool bIntType>
const CFxArrayInd<TP, nIndices, bIntType>& CFxArrayInd<TP, nIndices, bIntType>::operator =(const THIS &a)
{
	int32_t i, n = BAS::m_iMem;
	BAS::operator =(a);
	for (i = 0; i < nIndices; i++) {
		if (n != BAS::m_iMem) {
			if(m_aInd[i].m_pi)
				delete [] m_aInd[i].m_pi;
			m_aInd[i].m_pi = new int32_t[BAS::m_iMem];
        }
		memcpy(m_aInd[i].m_pi, a.m_aInd[i].m_pi, BAS::m_iCount * sizeof(int32_t));
		m_aInd[i].m_fcmp = a.m_aInd[i].m_fcmp;
	}
	m_del = a.m_del;
	return *this;
}

template <class TP, int32_t nIndices, bool bIntType>
CFxArrayInd<TP, nIndices, bIntType>& CFxArrayInd<TP, nIndices, bIntType>::operator =(THIS &&a)
{
	Attach(a);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::FindValid находит действительный объект приблизительно или точно. Объект должен
/// иметь соответствующую ф-цию IsValid()
/// \param o: искомый объект
/// \param iIndex
/// \param bNearest
/// \return индекс найденного объекта в индексе 'iIndex'
///
template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::FindValid(const TP &o, cint32_t iIndex, bool bNearest) const // bNearest = false
{
    ASSERT(iIndex >= 0 && iIndex < nIndices);
	int32_t i = bNearest ? FindNearest(o, iIndex) : FindEqual(o, INV32_IND, iIndex), j = -1;
	const TP *pu = nullptr, *pd = nullptr, *po = nullptr;
	if (i < 0 || i >= BAS::m_iCount)
        return -1;
	const CIndex &ind = m_aInd[iIndex];
	ASSERT(ind.m_fcmp && (!BAS::m_iCount || ind.m_pi));
	if (BAS::m_pData[ind.m_pi[i]].IsValid())
		return i;
	for (j = i++ - 1; (i < BAS::m_iCount || j >= 0) && !pu && !pd; i++, j--) {
		if (i < BAS::m_iCount && (po = &BAS::m_pData[ind.m_pi[i]])->IsValid())
			pu = po;
		if (j >= 0 && (po = &BAS::m_pData[ind.m_pi[j]])->IsValid())
            pd = po;
    }
    if (pu && (bNearest || ind.m_fcmp(o, *pu) == 0))
		return (i - 1);
    if (pd && (bNearest || ind.m_fcmp(o, *pd) == 0))
		return (j + 1);
    return -1;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd<TP, nIndices, bIntType>::FindEqual находит элемент 'o' по значению и
/// идентификатору. Последнее не обязательно, но в таком случае идентификатор, по крайней мере
/// индекс объекта должен быть недействительным.
/// Возвращаемый индекс может быть использован в ф-ции GetItem например:
/// CLineEx ln;
/// ln.m_fValue = 3.3;
/// ln = ali.GetItem(ALI_VAL, aln.FindEqual(ln, ALI_VAL)); // какая-то линия со значением 3,3
/// \param o - искомый объект
/// \param id: базовый идентификатор (физический индекс) объекта 'o'. Может быть недействтительным INV32_IND
/// \param index - индекс объекта в m_aInd
/// \return возвращает индекс эл-та в индексе m_aInd или -1.
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::FindEqual(const TP &o, cint32_t id, cint32_t index) const
{
	ASSERT(nIndices > 0);
    ASSERT(index >= 0 && index < nIndices);
	const CIndex &ind = m_aInd[index];
	ASSERT(ind.m_fcmp && (ind.m_pi || !BAS::m_iCount));
	int32_t i, j, a = 0, z = BAS::m_iCount - 1, r;
	for (i = z / 2; a <= z; i = a + (z - a) / 2) {
		r = ind.m_fcmp(o, BAS::m_pData[ ind.m_pi[i] ]);
		if (r < 0)
            z = i - 1;
		else if (r > 0)
            a = i + 1;
		else if (ind.m_pi[i] == id)// o.m_id.m_iObj)
            return i;
		else if (id != INV32_IND) {
			// имеется объект с идентичным значением, значит 'id' должен быть рядом
			ASSERT(id < (BAS::m_iCount));
			for (j = i + 1; j <= z && ind.m_pi[j] != id && !ind.m_fcmp(o, BAS::m_pData[ind.m_pi[j]]); j++)
				;
			if (j > z || ind.m_pi[j] != id) {
				for (j = i - 1; j >= a && ind.m_pi[j] != id && !ind.m_fcmp(o, BAS::m_pData[ind.m_pi[j]]); j--)
					;
            }
			if (j < a || ind.m_pi[j] != id)	{
                ASSERT(false);
				return -1; // ошибка! указан неправильный идентификатор. Нужен INV32_IND или в пределах обл.знач.
            }
            return j;
		}
		else
			return i; // найдено значение без учёта идентификатора
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd<TP, nIndices, bIntType>::FindNearest находит индекс следующего
/// или равного по значению объекта
/// \param o - искомый объект
/// \param index - индекс объекта m_aInd[index]
/// \return возвращает индекс равного, или следующего объекта. Может равняться к-ву
/// эл-тов массива, если 'o' больше последнего.
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::FindNearest(const TP &o, int32_t index) const
{
	return ((BAS::m_iCount > 0) ? FindNearest(o, index, 0, BAS::m_iCount - 1) : -1);
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::FindNearest ищу индекс большего или равного по значению объекта.
/// \param o: искомый объект
/// \param index: в каком индексе искать
/// \param iBegin: индекс начала сегмента поиска (включительно)
/// \param iEnd: индекс окончания сегмента поиска (включительно)
/// \return индекс в индексе 'index'
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::FindNearest(const TP &o, int32_t index, int32_t iBegin, int32_t iEnd) const
{
	ASSERT(index >= 0 && index < nIndices);
	ASSERT(iBegin <= iEnd && BAS::m_iCount >= 0);
	const CIndex &ind = m_aInd[index];
	ASSERT(ind.m_fcmp && (!BAS::m_iCount || ind.m_pi));
	int32_t i, r,
		a = (iBegin <= 0) ? 0 : (iBegin < BAS::m_iCount) ? iBegin : !BAS::m_iCount ? 0 : BAS::m_iCount - 1,
		z = !BAS::m_iCount ? 0 : (iEnd < 0 || iEnd >= BAS::m_iCount) ? BAS::m_iCount - 1 : iEnd;
	for (i = (a + z) / 2; a < z; i = (a + z) / 2) {
		ASSERT(ind.m_pi[i] >= 0 && ind.m_pi[i] < (BAS::m_iCount));
		r = ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i]]);
        if (r < 0)
			z = MAX(a, i - 1);
        else if (r > 0)
			a = MIN(z, i + 1);
        else
            break;
    }
	if (a == z) {
        ASSERT(i == a);
		if (i < BAS::m_iCount && ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i]]) > 0)
            i += 1;
	}
	ASSERT(i <= BAS::m_iCount);
	ASSERT(i == BAS::m_iCount || ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i]]) <= 0);
	// возвращаю индекс индекса...
    return i;
}

// сравнивает этот объект с 'о': Cmp(*this, o)
template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::Cmp(const int32_t iObj, const TP &o, const int32_t iIndex) const
{
    ASSERT(iIndex >= 0 && iIndex < nIndices);
	ASSERT(iObj >= 0 && iObj < (BAS::m_iCount));
	const CIndex &ind = m_aInd[iIndex];
	ASSERT(ind.m_fcmp && (!BAS::m_iCount || ind.m_pi));
	return ind.m_fcmp(BAS::m_pData[ind.m_pi[iObj]], o);
}

template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::ValidCount() const
{
	return (THIS::m_iCount - m_del.GetCount());
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::BaseInd вернёт базовый (физический) индекс эл-та массива, который можно
/// использовать, скажем, в операторе [] базового класса.
/// \param iInd: индекс
/// \param iItm: индекс эл-та в индексе iInd
/// \return базовый идентификатор эл-та
///
template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::BaseInd(int32_t iInd, int32_t iItm) const
{
	ASSERT(iInd >= 0 && iInd < nIndices && iItm >= 0 && iItm < BAS::m_iCount);
	return m_aInd[iInd].m_pi[iItm];
}

template<class TP, int32_t nIndices, bool bIntType>
const typename CFxArrayInd<TP, nIndices, bIntType>::CIndex* CFxArrayInd<TP, nIndices, bIntType>::Index(int i) const
{
	ASSERT(i >= 0 && i < nIndices);
	return (m_aInd + i);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd<TP, nIndices, bIntType>::Add добавит новый эл-т 'o' в массив, поместит
/// физический индекс эл-та в o.m_id.m_iObj.
/// \param o - добавляемый эл-т
/// \return вернёт физический индекс нового эл-та, или -1 в случае переполнения массива (к-во эл-тов INT_MAX).
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::Add(const TP &o)
{
//    ASSERT(o.IsValid()); объект может быть базового типа
	int32_t iBas = -1, i, aitmp[nIndices], bDel = false;
	if (m_del.GetCount()) {
		// 04.06.2017 теперь беру не с начала m_del, а с конца т.к. теперь удлалённые эл-ты упорядочиваются
		// по первому индексу, чтобы тут выбирать параллели перед их основными линиями, сохраняя целостность
		// индекса ALI_LRT (см. так же Invalidate)
		// 03.09.2023 эл-ты в m_del упорядочены не по первому индексу, а по базовому ид-ру. Если нужна преем-
		// ственность, следует наследовать этот массив, скажем массивом CLinesArray
		iBas = m_del.Last();
		m_del.SetCountND(m_del.GetCount() - 1);
		ASSERT(iBas >= 0 && iBas < BAS::m_iCount);// && !BAS::m_pData[iBas].IsValid());
//		ASSERT(INDX3(BAS::m_pData[iBas].m_id) == iBas);
        // найду нынешние индексы...
		for(i = 0; i < nIndices; i++) {
			aitmp[i] = FindEqual(BAS::m_pData[iBas], iBas, i);
			ASSERT(aitmp[i] >= 0 && aitmp[i] < BAS::m_iCount);
		}
		bDel = true;	// используется имеющийся (удалённый) объект
    }
	else if (BAS::m_iCount < INT_MAX) {
		iBas = BAS::m_iCount;
		THIS::SetCount(iBas + 1);	// SetCount - виртуальная ф-ция
    }
    else
		return -1;//throw (err::CLocalException(LE_IND_LIMIT));
	BAS::m_pData[iBas] = o;
    // найти место объекта во всех индексах
	for (i = 0; i < nIndices; i++)
		if (bDel)
			ResetIndex(BAS::m_pData[iBas], iBas, aitmp[i], i);
		else
			SetIndex(BAS::m_pData[iBas], i, iBas);
	return iBas;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::SetIndex добавляю новый эл-т в индексированный массив. Индекса эл-та 'o' в массиве
/// ind.m_pi не должно быть - объект 'o' должен быть либо новым, либо предварительно изъятым (CFxArray::RemAt) из
/// этого массива. Память в ind уже выделена для нового эл-та и последний эл-т массива ind.m_pi недействительный.
/// Поэтому FindNearest здесь не используется.
/// \param o: добавляемый объект
/// \param index: по которому добавляю
/// \param x: базовый идентификатор (физический индекс) объекта в этом массиве
/// \return x
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::SetIndex(const TP &o, cint32_t index, cint32_t x)
{
	ASSERT(x != INV32_IND);
    ASSERT(index >= 0 && index < nIndices);
	ASSERT(BAS::m_iCount > 0);
	CIndex &ind = m_aInd[index];
	if (!ind.m_fcmp)
		return -1;
	ASSERT(ind.m_pi || !BAS::m_iCount);
	int32_t i, r, a = 0,
		z = BAS::m_iCount - 2;	// -2 потому что последний элемент массива может быть ещё не добавлен, но память
								// для него уже выделена. Он пустой или недействительный, тут не рассматривается
	for (i = z / 2; a < z; i = a + (z - a) / 2) {
		ASSERT(ind.m_pi[i] >= 0 && ind.m_pi[i] < BAS::m_iCount - 1);
		r = ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i]]);
        if (r < 0)
			z = MAX(a, i - 1);
        else if (r > 0)
			a = MIN(z, i + 1);
		else {// предыд.элемент равен или меньше 'o' и  следующий эл-т больше или равен 'o'
			ASSERT((i == 0 || ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i - 1]]) >= 0) &&
				   (i == BAS::m_iCount - 2 || ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i + 1]]) <= 0));
            break;
        }
    }
	if (z == a) {// нет идентичного эл-та
		ASSERT(i >= 0 && i < BAS::m_iCount);	// 'i' может быть равной к-ву действительных эл-ов
		if (i < BAS::m_iCount - 1 && ind.m_fcmp(o, BAS::m_pData[ind.m_pi[i]]) > 0)
			i++;
	} // добавлен элемент, нужно смещать все эл-ты справа от 'i' включительно - вправо. Последний эл-т не имеет значения (добавлен только что, может быть перезаписан)
	if (i < BAS::m_iCount - 1)
		memmove(ind.m_pi + i + 1, ind.m_pi + i, (BAS::m_iCount - i - 1) * sizeof(int32_t));
	ind.m_pi[i] = x;//INDX3(o.m_id);
	ASSERT(!i || ind.m_fcmp(BAS::m_pData[ind.m_pi[i]], BAS::m_pData[ind.m_pi[i - 1]]) >= 0);
	ASSERT(BAS::m_iCount - i == 1 || ind.m_fcmp(BAS::m_pData[ind.m_pi[i]], BAS::m_pData[ind.m_pi[i + 1]]) <= 0);
	return x;
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::Invalidate отмечает объект как недействительный без переиндексации
/// \param o: удаляемый объект
/// \param iIndex: индекс, по которому удаляется объект. Это должен быть уникальный индекс,
/// иначе результат не определён.
///
template<class TP, int32_t nIndices, bool bIntType>
inline void CFxArrayInd<TP, nIndices, bIntType>::Invalidate(const TP &o, int32_t iIndex) // iIndex = 0
{
	ASSERT(o.IsValid());
	ASSERT(iIndex >= 0 && iIndex < nIndices);
	Invalidate(FindEqual(o, INV32_IND, 0));
}

// элемент массива просто отмечается как "удалённый", но остаётся в массиве и всех индексах. Также добавляется новый эл-т
// в массив удалённых элементов.
/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::Invalidate отмечает объект как "удалённый" поместив его ид-р в массив уда-
/// лённых объектов.
/// \param id: базовый ид-р удаляемого эл-та массива
///
template <class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Invalidate(cint32_t id)
{
	const int32_t cnt = m_del.GetCount(), *p = m_del,
		i = FindNearest(p, cnt, id);			// global.h
	ASSERT(i >= 0 && (i == cnt || id < p[i]));	// повторное удаление?
	m_del.Insert(i, id);
	ASSERT(!BAS::m_pData[id].IsValid() && m_del[i] == id);
}

// вернуть удалённый эл-т, значит удалить индекс из массива m_del
// безопасно использовать только в том случае, если индексы не нужно переиндексировать (см. Add)
// вернуть эл-т к жизни (по меньшей мере снять флаг удаления) ответственна вызывающая ф-ция, UndeleteItem 
// только удаляет индекс из массива удалённых эл-тов
/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::UndeleteItem только удалит ид-р id из массива удалённых
/// \param id: ид-р удалённого эл-та
///
template<class TP, int32_t nIndices, bool bIntType>
inline void CFxArrayInd<TP, nIndices, bIntType>::UndeleteItem(int32_t id)
{
	m_del.RemAt(glb::FindEqual((int32_t*) m_del, m_del.GetCount(), id)); // RemAt() asserts если id не найден
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::ResetIndex значение объекта изменилось и нужно указать ему новое место в индексе
/// m_aInd[index].m_id
/// \param o: новое значение объекта, уже отредактированное в массиве
/// \param id: базовый идентификатор (физический индекс) объекта 'o'
/// \param curr: настоящий индекс эл-та 'o' в массиве индексов m_aInd[index].m_pi который нужно пересмотреть
/// \param index: изменяемый индекс
/// \return индекс объекта 'o' в индексе m_aInd[index].m_id
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::ResetIndex(const TP &o, cint32_t id, cint32_t curr, cint32_t index)
{   // здесь я являюсь представителем следующего действа: массив девственно отсортирован
    // по возрастанию, но значение объекта 'o' изменилось только что и требует изменения индекса
	ASSERT(nIndices);
//	ASSERT(id == BAS::m_pData[id].m_id);
	ASSERT(index >= 0 && index < nIndices);
	ASSERT(curr >= 0 && curr < BAS::m_iCount);
	ASSERT(id == m_aInd[index].m_pi[curr]);
	int32_t *pi = m_aInd[index].m_pi, i = curr;
	pfcmp pf = m_aInd[index].m_fcmp;
	if (!pf)
		return -1;
    // По завершении работы FindNearest, i указывает на элемент больший, или равный o, либо индекс равен к-ву эл-ов массива.
    // А предшествующий i эл-т меньше о. Т.е. о меньше или равен элементу i, если i < m_iCount. Ситуация устроит, если элемент i
    // сдвигается право. Двигать влево можно только предшествующий i эл-т. Поэтому под первым if от возвращаемого FindNearest
    // индекса отнимается 1.
    // Несмотря на то что от результата отнимается 1, конечный результат не может быть равен curr т.к. o > curr + 1
	if (curr < BAS::m_iCount - 1 && pf(o, BAS::m_pData[pi[curr + 1]]) > 0)
		i = FindNearest(o, index, curr + 1, BAS::m_iCount - 1) - 1;
	else if (curr > 0 && pf(o, BAS::m_pData[pi[curr - 1]]) < 0)
		i = FindNearest(o, index, 0, curr - 1);
    else
		goto _return; // 'o' не больше следующего и не меньше предыдущего эл-та, т.е. остаётся на прежнем месте
	ASSERT(i >= 0 && i < BAS::m_iCount && i != curr);
    if(i < curr)
		memmove(pi + i + 1, pi + i, (curr - i) * sizeof(int32_t));
    else if(i > curr)
		memcpy(pi + curr, pi + curr + 1, (i - curr) * sizeof(int32_t));
	pi[i] = id;// INDX3(o.m_id);
    // теперь объект должен стоять на своём месте уж точно...
	ASSERT(!i || m_aInd[index].m_fcmp(BAS::m_pData[pi[i]], BAS::m_pData[pi[i - 1]]) >= 0);
	ASSERT(BAS::m_iCount - i == 1 || m_aInd[index].m_fcmp(BAS::m_pData[pi[i]], BAS::m_pData[pi[i + 1]]) <= 0);
_return:
    return i;
/*#ifdef _DEBUG
	CheckLinesOrder(index, i, curr);
#else
	;
#endif */// #ifdef _DEBUG
}

// Dump2: сбрасывает в конец или в начало индекса. Применяется в случае множества с одинаковыми
// значениями, как в случае со значениями по умолчанию 0U и -1U, которые соответственно гарантиро-
// ванно безопасно размещаются в начале или конце индекса.
/*template <class TP, int32_t nIndices>
void CFxArrayInd<TP, nIndices, bIntType>::Dump2(const int32_t cDir, const TP &o, const int32_t curr, const int32_t index)
{	// здесь я являюсь представителем следующего действа: массив девственно отсортирован 
	// по возрастанию, но значение объекта 'o' изменилось только что и требует изменения индекса
		ASSERT(m_aInd && nIndices);
        ASSERT(o.m_id == m_pData[INDX3(o.m_id)].m_id);
        ASSERT(index >= 0 && index < nIndices);
        ASSERT(curr >= 0 && curr < m_iCount);
		ASSERT(INDX3(o.m_id) == m_aInd[index].m_pi[curr]);
	int32_t *pi = m_aInd[index].m_pi,
		i = (cDir == DIR_BEGIN) ? 0 : m_iCount - 1;
	if (i == curr)
		return;
	// элементы находящиеся между индексами i и curr смещаются на один, перезаписывая эл-т индексируемый curr
	if (i > curr)
		memcpy(pi + curr, pi + curr + 1, (i - curr) * sizeof(int32_t));
	else if (i < curr)
		memmove(pi + i + 1, pi + i, (curr - i) * sizeof(int32_t));
	pi[i] = INDX3(o.m_id);
	// теперь объект должен стоять на своём месте уж точно...
		ASSERT(!i || m_aInd[index].m_fcmp(m_pData[pi[i]], m_pData[pi[i - 1]]) >= 0);
		ASSERT(m_iCount - i == 1 || m_aInd[index].m_fcmp(m_pData[pi[i]], m_pData[pi[i + 1]]) <= 0);
}*/

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::GetItem извлекаю эл-т из индексированного списка
/// \param index: индекс массива индексов
/// \param iObj: индекс объекта
/// \return ссылку на эл-т массива
///
template <class TP, int32_t nIndices, bool bIntType>
const TP& CFxArrayInd<TP, nIndices, bIntType>::GetItem(int32_t index, int32_t iObj) const
{
	ASSERT(nIndices && index >= 0 && index < nIndices);
	ASSERT(iObj >= 0 && iObj < BAS::m_iCount);
	ASSERT(m_aInd[index].m_pi[iObj] >= 0 && m_aInd[index].m_pi[iObj] < BAS::m_iCount);
	return BAS::m_pData[m_aInd[index].m_pi[iObj]];
}

template <class TP, int32_t nIndices, bool bIntType>
TP& CFxArrayInd<TP, nIndices, bIntType>::GetItem(int32_t index, int32_t iObj)
{
	ASSERT(nIndices && index < nIndices);
	ASSERT(iObj >= 0 && iObj < BAS::m_iCount);
	ASSERT(m_aInd[index].m_pi[iObj] >= 0 && m_aInd[index].m_pi[iObj] < BAS::m_iCount);
	return BAS::m_pData[m_aInd[index].m_pi[iObj]];
}

template<class TP, int32_t nIndices, bool bIntType>
inline int32_t CFxArrayInd<TP, nIndices, bIntType>::GetValidCount() const
{
	return (BAS::GetCount() - DelCount());
}

template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::DelCount() const
{
    return m_del.GetCount();
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd<TP, nIndices, bIntType>::Add добавит все эл-ты 'pItem' в массив, поместит
/// физический индекс каждого эл-та в pItem->m_id.m_iObj.
/// \param p - указатель на добавляемые эл-ты
/// \param cnt - к-во добавляемых эл-тов
/// \return вернёт физический индекс первого эл-та pItem[0].m_id.m_iObj; Швырнет исключение
/// CLinerException при попытке переполнения см. int32_t Add(TP&);
///
template <class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::Add(const TP *p, int32_t cnt) // cnt = 1
{
	ASSERT(p && cnt > 0);
	const TP *pi = p + cnt - 1;
	int32_t i = -1;
	while (pi >= p) {
		const TP &o = *pi--;
		i = Add(o);
	}
	return i;
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Remove(int32_t iInd, int32_t iItm)
{
	ASSERT(iInd >= 0 && iInd < nIndices);
	ASSERT(iItm >= 0 && iItm < BAS::m_iCount);
	Remove(BAS::m_pData[m_aInd[iInd].m_pi[iItm]]);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Remove(const TP &o)
{
	int32_t ii, i, j, *pi, iBas = INV32_IND;
	for (i = 0; i < nIndices; i++) {
		ii = FindEqual(o, iBas, i);
		ASSERT(ii >= 0 && ii < BAS::m_iCount);
		if (iBas == INV32_IND)
			iBas = m_aInd[i].m_pi[ii];
		ASSERT(iBas == m_aInd[i].m_pi[ii]);
		if (BAS::m_iCount - ii > 1) {
			pi = m_aInd[i].m_pi + ii;
			memcpy(pi, pi + 1, (BAS::m_iCount - ii - 1) * sizeof *pi);
		}// все индексы большие iBas сокращаются на 1
		for (pi = m_aInd[i].m_pi, j = 0; j < BAS::m_iCount - 1; j++, pi++)
			if (*pi > iBas)
				(*pi)--;
	}// удалённые эл-ты
	ii = m_del.GetCount();
	if (ii > 0) {
		i = glb::FindEqual((cint32_t*) m_del, ii, iBas);
		if (i >= 0 && i < ii)
			m_del.RemND(i);
		for (pi = m_del, i = 0; i < ii; i++, pi++)
			if (*pi > iBas)
				(*pi)--;
	}
	BAS::RemND(iBas);
}

template<class TP, int32_t nIndices, bool bIntType>
inline void CFxArrayInd<TP, nIndices, bIntType>::Empty()
{
	BAS::SetCount(0);
	for (int i = 0; i < nIndices; i++)
		m_aInd[i].Realloc(0, 0);
	m_del.SetCount(0);
}

template <class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Attach(THIS &a)
{
	for (int32_t i = i; i < nIndices; i++) {
		m_aInd[i].m_fcmp = a.m_aInd[i].m_fcmp;
		m_aInd[i].m_pi = a.m_aInd[i].m_pi;
		a.m_aInd[i].m_fcmp = nullptr;
		a.m_aInd[i].m_pi = nullptr;
	}
	m_del.Attach(a.m_del);
	BAS::Attach(a);
}

template <class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::SetCountZeroND()
{
	BAS::SetCountZeroND();
	m_del.SetCountZeroND();
}

template <class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::SetCountND(int32_t)
{
    ASSERT(false);	// сокращая массив m_pData нужно позаботиться о том что в m_del не будет удаляемых элементов
}

template <class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::SetCount(int32_t iNewCount)
{
	int32_t nCnt = BAS::m_iCount, i, nMem = BAS::m_iMem;
    // base class..
	BAS::SetCount(iNewCount);
	// если только к-во эл-ов изменилось...
	if (nMem != BAS::m_iMem) {
		for (i = 0; i < nIndices; i++)
			m_aInd[i].Realloc(nCnt, BAS::m_iMem);
	}
}

/*template<class TP, int32_t nIndices, bool bIntType>
inline int32_t CFxArrayInd<TP, nIndices, bIntType>::SetBinData(CBYTE *cpb, int32_t cnt)
{
	int32_t nCnt = read_tp<int32_t>(cpb, cnt), i = sizeof nCnt, j;
	for (j = 0; j < nCnt; j++)
		Add()
}*/

template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::AddND(const TP *pitem, int32_t nCount)
{
	ASSERT(false);
	return -1;
}

template<class TP, int32_t nIndices, bool bIntType>
int32_t CFxArrayInd<TP, nIndices, bIntType>::AddND(const TP &obj)
{
	ASSERT(false);
	return -1;
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::RemAt(cint32_t iInd, cint32_t iCnt)
{
	ASSERT(false);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::RemND(cint32_t iInd, cint32_t iCnt)
{
	ASSERT(false);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Insert(int32_t iAt, const TP &o)
{
	ASSERT(false);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Insert(int32_t iAt, const TP *pa, int32_t cnt)
{
	ASSERT(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::Edit
/// \param iBas: базовый идентификатор
/// \param pi: указатель на новый эл-т вместо указанного
/// \param cnt: не используется, только для совместимости с ф-цией базового класса
///
template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Edit(int32_t iBas, const TP *pi, int32_t cnt)
{
	ASSERT(nIndices && cnt <= 1);
	TP &old = BAS::m_pData[iBas];
	int32_t i, ai[nIndices];
	// беру все индесы для переиндексации
	for (i = 0; i < nIndices; i++)
		ai[i] = FindEqual(old, iBas, i);
	// изменяю эл-т индексированного массива
	old = *pi;
	// переиндексация индексов изменённого эл-та индесированного массива
	for (i = 0; i < nIndices; i++)
		ai[i] = ResetIndex(old, iBas, ai[i], i);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CFxArrayInd::Edit заменяю эл-т массива на новый, переиндексирую массив
/// \param iBas: базовый инденификатор изменяемого эл-та массива
/// \param on: новый эл-т массива
/// \param paio [ИЗ]: массив индексов старого объекта в к-ве nIndices
/// \param pain [ИЗ]: массив индексов нового объекта в к-ве nIndices
/// \return базовый ид-р изменённого эл-та
///
template<class TP, int32_t nIndices, bool bIntType>
inline void CFxArrayInd<TP, nIndices, bIntType>::Edit(int32_t iBas, const TP &on, int32_t *paio, int32_t *pain)
{
	TP &o = BAS::m_pData[iBas];
	// собираю индексы старого объекта
	for (int i = 0; i < nIndices; i++)
		paio[i] = FindEqual(o, iBas, i);
	// меняю старый объект на новый
	o = on;
	// переиндексация
	for (int i = 0; i < nIndices; i++)
		pain[i] = ResetIndex(o, iBas, paio[i], i);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::Move(int32_t iTo, int32_t iFrom)
{
	ASSERT(false);
}

template<class TP, int32_t nIndices, bool bIntType>
void CFxArrayInd<TP, nIndices, bIntType>::FreeRedundant()
{
	ASSERT(false);
}


template<class TP, int32_t nIndices, bool bIntType>
TP& CFxArrayInd<TP, nIndices, bIntType>::Last()
{
	ASSERT(false);
	return BAS::Last();
}

template<class TP, int32_t nIndices, bool bIntType>
const TP& CFxArrayInd<TP, nIndices, bIntType>::Last() const
{
	ASSERT(false);
	return BAS::Last();
}

template<class TP, int32_t nIndices, bool bIntType>
inline int32_t CFxArrayInd<TP, nIndices, bIntType>::SetBinData(CBYTE *p, int32_t n)
{
	ASSERT(false);
	return 0;
}

template<class TP, int32_t nIndices, bool bIntType>
inline int32_t CFxArrayInd<TP, nIndices, bIntType>::SetBinData(const CFxArray<BYTE, true> &a)
{
	ASSERT(false);
	return 0;
}

#if defined __DEBUG && defined __LINER
//
// аргументы:
// 1. cIndex	индекс индекса в массиве
// 2. cCurrElt	текущий индекс элемента (линии) в массиве
// 3. cPrevElt	предыдущий индекс элемента как в случае переиндексации
//
// в проекте две точки, где меняется индекс: 
// 1. CFxArray<TP, bIntType>SetIndex, где объект впервые индексируется и
// 2. CFxArray<TP, bIntType>ResetIndex, где он переиндексируется, если его значение меняется.
// Здесь я проверяю правильность индексации/переиндексации
template <class TP, int32_t nIndices, bool bIntType>
bool CFxArrayInd<TP, nIndices, bIntType>::CheckLinesOrder(const int32_t cIndex, const int32_t cCurrElt, const int32_t cPrevElt)
{
	ASSERT(cCurrElt >= 0 && cCurrElt < BAS::m_iCount);
	ASSERT(cPrevElt < BAS::m_iCount);
	ASSERT(cIndex >= 0 && cIndex < nIndices);
	const TP *plc = &GetItem(cIndex, cCurrElt),
                *plp = (cCurrElt > 0) ? &GetItem(cIndex, cCurrElt - 1) : nullptr,
				*pln = (cCurrElt < BAS::m_iCount - 1) ? &GetItem(cIndex, cCurrElt + 1) : nullptr;
	if (!plp)
        goto returntrue;
	if (m_aInd[cIndex].m_fcmp(*plp, *plc) > 0 || pln && m_aInd[cIndex].m_fcmp(*pln, *plc) < 0)
	{
                ASSERT(false);
        return false;
	}
	// порядок элементов в индексе ALI_SEQ:
	// линии может предшествовать
	// если эта линия - основная:
	// 1. любая параллель, или основная линия, отличная по вершинам.
	// если эта - параллель:
	// 3. основная линия этой параллели с тем же идентификатором пос-ти
	// 4. аналогичная параллель с тем же идентификатором пос-ти
	if (cIndex == ALI_SEQ && plc->m_nSeqID != INV32_ID)
	{
		if (plc->m_flags & 0x00000040)	// LN_PRL тут не объявлен
		{	// параллели может предшествовать линия с тем же идентификатором
			if (plp->m_nSeqID == plc->m_nSeqID &&
				// и теми же вершинами (основная линия, или параллель)
				plp->m_right.m_iIndex == plc->m_right.m_iIndex && plp->m_left.m_iIndex == plc->m_left.m_iIndex &&
				plp->m_right.m_tm == plc->m_right.m_tm && plp->m_left.m_tm == plc->m_left.m_tm && 
				// направления вершин (основных) линий должно быть одинаковым
				plp->TopsDir() == plc->TopsDir())
				goto returntrue;
			// debug: в массив добавлена параллель, которая не относится к той же пос-ти, что её основная линия. Т.е. у параллели 
			// идентификатор пос-ти (-1), а у основной - действительный. Поэтому параллель находится в конце массива, среди подобных
			// по идентификатору пос-ти. Таким образом, на линии без идентификатора пос-ти эти правила не распространяются.
			ASSERT(g_nDbgNoLnsCheck != 0);
			return false;
		}
		// Основной линии может предшествовать линия с другим идентификатором пос-ти или другими вершинами. Причём, вершины прове-
		// ряю не только по времени и/или индексу, а и по направлению тоже - линии могут располагаться на разнонаправленных 
		// вершинах.
		else if (plp->m_nSeqID == plc->m_nSeqID && plp->m_right.m_tm == plc->m_right.m_tm && plp->m_left.m_tm == plc->m_left.m_tm &&
			plp->TopsDir() == plc->TopsDir())
		{	// Но имеется исключение g_nDbgNoLnsCheck: при переиндексации линий во время удаления пос-ти основной линии
			// может предшествовать даже её параллель.
			ASSERT(g_nDbgNoLnsCheck != 0 && !cPrevElt);// исключение из правила: предыдущая позиция перемещённой линии в индексе ALI_SEQ (здесь рекурсия)
			return false;
		}
	}
	// порядок линий в индексе ALI_LRT:
	// линии может предшествовать если она - параллель:
	// 1. параллель или основная линия с теми-же вершинами
	// если линия основная:
	// 1. параллель или основная линия с другими вершинами
	else if (cIndex == ALI_LRT)
	{
		if (plc->m_flags & 0x00000040 && (// LN_PRL тут не объявлен
			// параллели может предшествовать только линия с аналогичными вершинами
			plp->m_right.m_tm != plc->m_right.m_tm || plp->m_left.m_tm != plc->m_left.m_tm ||
			// вершины должны быть направлены аналогично
			plp->TopsDir() != plc->TopsDir())
			||
			!(plc->m_flags & 0x00000040) &&
			// основной линии может предшествовать только линия с другими вершинами
			plp->m_right.m_tm == plc->m_right.m_tm && plp->m_left.m_tm == plc->m_left.m_tm &&
			// либо направленные обратное
			plp->TopsDir() == plc->TopsDir())
		{
                        ASSERT(false);
            return false;
		}
	}
	// если имеется индекс предыдущего положения объекта в массиве, можно запустить рекурсию - к тому элементу,
	// который сейчас на месте cPrevElt предъявляются те же требования.
returntrue:
	if (cPrevElt >= 0)
		return CheckLinesOrder(cIndex, cPrevElt, -1);
    return true;
}

/* если есть недействительный идентификатор пос-ти, сравниваются только идентификаторы
	if (lnPrev.m_nSeqID == INV32_ID || lnCurr.m_nSeqID == INV32_ID)
                ASSERT(lnPrev.m_nSeqID <= lnCurr.m_nSeqID);
	else if (lnCurr.m_flags & 0x00000040)	// LN_PRL
                ASSERT(lnPrev.m_nSeqID == lnCurr.m_nSeqID &&
			lnPrev.m_right.m_tm == lnCurr.m_right.m_tm && lnPrev.m_left.m_tm == lnCurr.m_left.m_tm &&
			(	// если этой паралели предшествует паралель, то только аналогичная - вершины равны направлением и временем
				lnPrev.m_flags & 0x00000040 && lnPrev.m_right.m_iDir == lnCurr.m_right.m_iDir &&
				lnPrev.m_left.m_iDir == lnCurr.m_left.m_iDir ||
				// если основная линия - то этой параллели
				!(lnPrev.m_flags & 0x00000040) && lnPrev.m_right.m_iDir == DIR_OPST(lnCurr.m_right.m_iDir) &&
				lnPrev.m_left.m_iDir == DIR_OPST(lnCurr.m_left.m_iDir)
				)
			);
	else	// перед основнй линией может быть другая линия - основная или параллель
                ASSERT(//lnPrev.m_nSeqID != lnCurr.m_nSeqID && (
			lnPrev.m_right.m_tm != lnCurr.m_right.m_tm || lnPrev.m_left.m_tm != lnCurr.m_left.m_tm);*/

#endif	// #if defined __DEBUG && defined __LINER

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CFxArayIndItem class - это базовый виртуальный класс для элемента массива CFxArrayInd,
/// который накладывает ограничения на элементы:
/// 1. Наличие конструктора по-умолчанию.
/// 2. оператор присваивания '='
/// 3. логический оператор равенства '=='
/// 5. Наличие ф-ции IsValid, которая вернёт true если объект не удалён и false в прот.случае
/// 6. Наличие ф-ции Invalidate, которая сделает объект недействительным (удалённым)
/// 7. Удалять объект из массива можно только посредством ф-ции CFxArrayInd::Invalidate(TP&)
/// Для хранения в файле (если хранится):
/// 8. файловая ф-ция int32_t GetBinData(CFxArray<BYTE, true> &a) const; скопирует объект для записи в
/// индексированный файловый массив. Аналогично,
/// 9. Обратная int32_t SetBinData(const BYTE *p, const int32_t i); восстановит объек из файла.


/// Удалённый элемент отмечается как удалённый, оставаясь на своём месте во всех индексах. Если
/// эл-т удаляется со сменой индексированных значений, то нужно выполнять его переиндексацию с
/// новыми значениями. Иными словами, удалённые эл-ты тоже индексируется.
/// Ф-ция CFxArrayInd::Add устанавливает индекс нового эл-та в CFxArrayIndItem.m_id, по которому
/// к нему можно получить доступ, используя оператор [] базового класса CFxArray. Нужно сделить за
/// тем, чтобы этот индекс не менялся при копировании в операторе присваивания класса, наследующего
/// CFxArrayIndItem.
///
class CFxArrayIndItem {
public:
//	CObjID m_id;
	CFxArrayIndItem();
	CFxArrayIndItem(const CFxArrayIndItem &o);
	CFxArrayIndItem(CFxArrayIndItem &&o);
	virtual ~CFxArrayIndItem();
	virtual bool IsValid() const = 0;
	virtual void Invalidate() = 0;
	virtual const CFxArrayIndItem& operator =(const CFxArrayIndItem &o);
	virtual CFxArrayIndItem& operator =(CFxArrayIndItem &&o);
};

inline glb::CFxArrayIndItem::CFxArrayIndItem()
{

}

inline CFxArrayIndItem::CFxArrayIndItem(const CFxArrayIndItem &o)
{

}

inline CFxArrayIndItem::CFxArrayIndItem(CFxArrayIndItem &&o)
{

}

inline glb::CFxArrayIndItem::~CFxArrayIndItem()
{

}

inline const CFxArrayIndItem& CFxArrayIndItem::operator =(const CFxArrayIndItem &o)
{
	return *this;
}

inline CFxArrayIndItem& CFxArrayIndItem::operator =(CFxArrayIndItem &&)
{
	return *this;
}

} // namespace glb

