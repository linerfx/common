#pragma once

/*
 * Copyright 2021 Николай Вамбольдт
 *
 * Файл oa_types.h — часть программы oacrl, входящей в проект linerfx
 *
 * oacrl свободная программа: вы можете перераспространять ее и/или изменять
 * ее на условиях Стандартной общественной лицензии GNU в том виде, в каком
 * она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * oacrl распространяется в надежде, что она будет полезной,
 * но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
 * общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с этой программой. Если это не так, см.
 * <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * oa_types.h is part of 'oacrl', which is a part of 'linerfx'.
 *
 * 'oacrl' is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 'oacrl' is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE
 * along with 'oacrl'. If not, see <https://www.gnu.org/licenses/>.
 *
 * oa_types.h содержит синонимы типов oanda
 */

/* time_t
 */
#include <ctime>
#include "incl_fxarray.h"

namespace oatp {

typedef glb::CFxString string;
typedef int integer;
typedef bool boolean;

/* The string representation of a decimal number.
 * A decimal number encoded as a string. The amount of precision provided depends on what the number represents.
 */
typedef double DecimalNumber;

/* The string representation of a quantity of an Account’s home currency.
 * A decimal number encoded as a string. The amount of precision provided depends on the Account’s home currency.
 */
typedef double AccountUnits;

/* Currency name identifier. Used by clients to refer to currencies.
 * A string containing an ISO 4217 currency (http://en.wikipedia.org/wiki/ISO_4217)
 */
typedef glb::CFxString Currency;

/* Instrument name identifier. Used by clients to refer to an Instrument.
 * A string containing the base currency and quote currency delimited by a “_”.
 */
typedef glb::CFxString InstrumentName;

/* A date and time value using either RFC3339 or UNIX time representation.
 * The RFC 3339 representation is a string conforming to https://tools.ietf.org/rfc/rfc3339.txt. The Unix
 * representation is a string representing the number of seconds since the Unix Epoch (January 1st, 1970 at UTC).
 * The value is a fractional number, where the fractional part represents a fraction of a second (up to nine decimal places).
 */
typedef struct dateTime {
	time_t m_nsec;	/* к-во секунд с "начала эпохи". (long int) 64 бит */
	time_t m_nns;	/* к-во наночастей секунды m_nsec. Можно разбить если понадобится на 2 int'а */
	dateTime() {
		m_nsec = m_nns = 0;// так, чтобы содержащие dateTime объекты можно было memset(0)
	}
	dateTime(time_t ts, time_t tn) {
		m_nsec = ts;
		m_nns = tn;
	}
	bool IsValid() const {
		return(m_nsec || m_nns);
	}
	void Init() {
		m_nsec = m_nns = 0;
	}
	/* Periods: к-во временных периодов в этом объекте
	 * nMinPeriod: период в минутах
	 */
	long int Periods(int nMinPeriod) const {
		return (m_nsec / (nMinPeriod * 60));
	}
	// оператор сравнения вернёт 0 если объекты равны, -1 если этот меньше 'o' и 1 иначе.
	int operator ==(const dateTime &o) const {
		return ((m_nsec > o.m_nsec) ? 1 : (m_nsec < o.m_nsec) ? -1 :
				(m_nns > o.m_nns) ? 1 : (m_nns < o.m_nns) ? -1 : 0);
	}
	bool operator >(const dateTime &o) const {
		return ((m_nsec > o.m_nsec) ? true : (m_nsec < o.m_nsec) ? false :
									(m_nns > o.m_nns) ? true : false);
	}
	bool operator <(const dateTime &o) const {
		return ((m_nsec < o.m_nsec) ? true : (m_nsec > o.m_nsec) ? false :
									(m_nns < o.m_nns) ? true : false);
	}
#ifdef OACRL_LIBRARY
	int Read(const char *pcsz, const int cnt);
#endif // defined OACRL_LIBRARY
} DateTime;

/* The Price component(s) to get candlestick data for.
 * Can contain any combination of the characters “M” (midpoint candles) “B” (bid candles) and “A” (ask candles).
 */
typedef glb::CFxString PricingComponent;

/* The string representation of a Price for a Bucket.
 * A decimal number encodes as a string. The amount of precision provided depends on the Instrument.
 */
typedef double PriceValue;

/* An instrument name, a granularity, and a price component to get candlestick data for.
 * A string containing the following, all delimited by “:” characters: 1) InstrumentName 2) CandlestickGranularity
 * 3) PricingComponent e.g. EUR_USD:S10:BM
 */
typedef glb::CFxString CandleSpecification;

/* The string representation of the OANDA-assigned TradeID. OANDA-assigned TradeIDs are positive integers,
 * and are derived from the TransactionID of the Transaction that opened the Trade.
 * Example 1523
 */
typedef integer TradeID;

/* The identification of a Trade as referred to by clients
 * Either the Trade’s OANDA-assigned TradeID or the Trade’s client-provided ClientID prefixed by the “@” symbol
 * Example @my_trade_id
 * Таким образом, TradeSpecifier строка, но может быть целым, если не начинается с 'at'
 */
typedef glb::CFxString TradeSpecifier;

/* The Order’s identifier, unique within the Order’s Account.
 * The string representation of the OANDA-assigned OrderID. OANDA-assigned OrderIDs are positive integers, and are
 * derived from the TransactionID of the Transaction that created the Order.
 * Example 1523
 */
typedef integer OrderID;

/* The specification of an Order as referred to by clients
 * Either the Order’s OANDA-assigned OrderID or the Order’s client-provided ClientID prefixed by the “@” symbol
 * Example 1523
 */
typedef glb::CFxString OrderSpecifier;

/* The string representation of an Account Identifier.
 * “-“-delimited string with format “{siteID}-{divisionID}-{userID}-{accountNumber}”
 * Example 001-011-5838423-001
 */
typedef glb::CFxString AccountID;

/* A client provided request identifier.
 * Example: my_request_id
 */
typedef glb::CFxString ClientRequestID;

/* The request identifier.
 */
typedef glb::CFxString RequestID;

/* A client-provided comment that can contain any data and may be assigned to their Orders or Trades. Comments
 * are typically used to provide extra context or meaning to an Order or Trade.
 * Example This is a client comment
 */
typedef glb::CFxString ClientComment;

/* A client-provided tag that can contain any data and may be assigned to their Orders or Trades. Tags are
 * typically used to associate groups of Trades and/or Orders together.
 * Example: client_tag_1
 */
typedef glb::CFxString ClientTag;

/* A client-provided identifier, used by clients to refer to their Orders or Trades with an identifier that they have provided.
 * Example: my_order_id
 */
typedef glb::CFxString ClientID;

/* The unique Transaction identifier within each Account.
 * String representation of the numerical OANDA-assigned TransactionID
 * Example: 1523... в oacrl пересчитываю в целое
 */
typedef integer TransactionID;

} // namespace oatp
