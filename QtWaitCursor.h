#pragma once
#include <QWidget>

class CWaitCursor
{
	QWidget *m_pw;
public:
	CWaitCursor(QWidget *pw, Qt::CursorShape s = Qt::WaitCursor) {
		pw->setCursor(s);
		m_pw = pw;
	}
	~CWaitCursor() {
		m_pw->unsetCursor();
	}
	void Reset() {
		m_pw->unsetCursor();
	}
	void SetWait() {
		m_pw->setCursor(Qt::WaitCursor);
	}
};