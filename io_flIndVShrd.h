#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_flIndVShrd.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_flIndVShrd.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_flIndIShrd.h"
#include "exception.h"
#include <QSharedMemory>

namespace glb {
	const char* FileName(const char *pcszPath, const int cPathLen);// common/global.cpp
	const CFxString& GetAppPath();// common/global.cpp
	double Bytes(uint64_t n, const char **pps);
}
namespace fl {
namespace i {
//////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIndVShrd class
///
template <class O, int nInds>
class CIndVShrd : public CIndIShrd<O, nInds>
{
	typedef CIndIShrd<O, nInds> BAS;
	typename BAS::LNDEX m_iOfsts; // файловые отступы (базовый индекс)
public:
	CIndVShrd();	// для массива. Обязательно присвоить зн-е m_fcmp всем эл-там массива
	CIndVShrd(typename CIndBase<O>::fcmp *f);
public:// открытие/закрытие
	virtual int64_t open(const char *pcszPN = NULL) override;
	virtual void close() override;
	void drop();
public:// запись/удаление
	virtual int SetOffset(int64_t noffset) override;// добавит только отступ
	virtual void Edit(int32_t iInd, int32_t iStart, int32_t nAdd) override;// изменит все значения, начиная от iStart
	virtual void Rem(int32_t iInd, int32_t iAt) override;
	virtual int RemNC(int iInd, int iAt) override;
	virtual void Move(int iInd, int iTo, int iFrom) override;
	virtual void Empty() override;
public:// извлечение
	virtual int64_t Ofst(int iInd, int iObj) const override;// файловый отступ объекта по индексу
	virtual int32_t Bas(int iInd, int iObj) const override;	// базовый индекс по индексу
	virtual const void* Index(int32_t iInd, int32_t *pcnt = NULL) const override;
public:// сравнение и прочее
	virtual int32_t count() const override;
	virtual int32_t count(int iInd) const override;
	virtual bool CheckCount() const override;
	virtual bool IsValid() const override;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::CIndVShrd конструктор для массивов. Перед использованием требуется иницыацыя.
///
template<class O, int nInds>
CIndVShrd<O,nInds>::CIndVShrd()
{
	// Требуется иницыацыя ф-цыей CIndBase<O>::Set...
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::CIndVShrd
/// \param *f: указатель на первую ф-цию сравнения в пос-ти ф-ций, количество которых равно nInds для
/// каждого индекса m_ai.
///
template<class O, int nInds>
CIndVShrd<O, nInds>::CIndVShrd(typename CIndBase<O>::fcmp *f)
	: BAS(f)
{

}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::open откроет все файлы индексов, прочтёт их содержимое и закроет каждый файл.
/// \param pcszPN: путь и имя к основному файлу, чьим индексом является этот объект.
/// \return
///
template<class O, int nInds>
int64_t CIndVShrd<O, nInds>::open(const char *pcszPN)
{
	ASSERT(nInds >= 0 && nInds < 100);
	if (m_iOfsts.IsValid())
		return m_iOfsts.count();
	char *pszPN = NULL;
	int n, x, npath = (int) strlen(pcszPN);
	glb::CFxString sPath(pcszPN, npath);
	sPath.SetCount(npath + 3); // +3: ".01" - ".99"
	pszPN = sPath;
	strncpy(pszPN + npath, ".01", 4);
	x = m_iOfsts.open(pszPN);
	n = BAS::open(pcszPN);
	if (x != n)
		throw err::CLocalException(SI_INT_DISPARITY, time(NULL), x, n, glb::FileName(pcszPN, npath + 3));
	return n;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::close только для совместимости. Для индекса в разделяемой памяти такое закрытие
/// не требуется т.к. файл, в котором размещаются индексы закрывается в ф-ции open сразу после чте-
/// ния. А индексы продолжают существовать в разделяемой памяти. Для завершения работы с индексами
/// нужно вызывать drop.
///
template<class O, int nInds>
void CIndVShrd<O, nInds>::close()
{
	m_iOfsts.close();
	BAS::close();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::drop отсоединяет объект от разделяемой памяти. Если этот индекс последний в
/// числе пользователей разделяемой памяти, то запишет индекс в файл.
///
template<class O, int nInds>
void CIndVShrd<O, nInds>::drop()
{
	m_iOfsts.drop();
	BAS::drop();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::SetOffset добавляет файловый оступ в индекс.
/// \param noffset: файловый отступ
/// \return базовый индекс эл-та
///
template<class O, int nInds>
int CIndVShrd<O, nInds>::SetOffset(int64_t noffset)
{
	return m_iOfsts.Append(noffset);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::Edit изменит все значения от указанного в базовом индексе, где отступы упорядочены
/// по возрастанию.
/// \param iInd: файловый отступ, либо индекс недействительных эл-тов
/// \param iStart: индекс первого изменяемого эл-та
/// \param nAdd: добавит/отнимет это значение
///
template<class O, int nInds>
void CIndVShrd<O, nInds>::Edit(int32_t iInd, int32_t iStart, int32_t nAdd)
{
	ASSERT(iInd == ioffset || iInd == idel);// только последовательно расположенные эл-ты
	if (iInd == ioffset) {
		int32_t cnt;
		int64_t *pl = const_cast<int64_t*>(m_iOfsts.Index(&cnt));
		ASSERT(pl != NULL);
		for (pl += iStart; iStart < cnt; iStart++, pl++)
			*pl += nAdd;
	} else
		BAS::Edit(iInd, iStart, nAdd);
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::Rem удаляет указанное к-во эл-тов из индекса, правит оставшиеся эл-ты.
/// \param iInd: индекс, по которому удаляется эл-та
/// \param iAt: индекс эл-та в iInd
///
template<class O, int nInds>
void CIndVShrd<O, nInds>::Rem(int32_t iInd, int32_t iAt)
{
	ASSERT(nInds >= 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	// упорядоченные по возрастанию эл-ты удаляются и редактируются быстрее и проще
	if (iInd == ioffset) {
		int64_t nsz = m_iOfsts.RemNC(iAt);		// удалённый отступ
		if (m_iOfsts.count() - iAt > 0) {
			nsz = m_iOfsts.Value(iAt) - nsz;	// размер удалённого объекта
			Edit(ioffset, iAt, (int32_t) nsz * -1);
		}
	} else if (iInd == idel) {
		RemNC(idel, iAt);
		Edit(idel, iAt, -1);
	} else
		BAS::Rem(iInd, iAt);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::RemNC удаляет эл-т из индекса без изменений оставшихся, в отличие
/// от Rem. NC - do Not Correct remains
/// \param iInd: индекс, из которого удаляется эл-т
/// \param iAt: индекс эл-та в iInd
/// \return базовый индекс удалённого эл-та
///
template<class O, int nInds>
int CIndVShrd<O, nInds>::RemNC(int iInd, int iAt)
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.RemNC(iAt) : BAS::RemNC(iInd, iAt);
}

template<class O, int nInds>
void CIndVShrd<O, nInds>::Move(int iInd, int iTo, int iFrom)
{
	ASSERT(iInd >= idel && iInd < nInds);
	if (iInd == ioffset)
		m_iOfsts.Move(iTo, iFrom);
	else
		BAS::Move(iInd, iTo, iFrom);
}

template<class O, int nInds>
void CIndVShrd<O, nInds>::Empty()
{
	m_iOfsts.Empty();
	BAS::Empty();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::Ofst извлекает из индекса файловый отступ по указанному индексу
/// \param iInd: индекс
/// \param iObj: индекс эл-та в iInd
/// \return
///
template<class O, int nInds>
int64_t CIndVShrd<O, nInds>::Ofst(int iInd, int iObj) const
{
	ASSERT(iInd >= idel && iInd < nInds);
	return m_iOfsts.Value((iInd == ioffset) ? iObj : BAS::Bas(iInd, iObj));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::Bas вернёт базовый индекс объекта
/// \param iInd: индекс, по которому извлекается базовый
/// \param iObj: индекс объекта в iInd
/// \return базовый индекс объекта
///
template<class O, int nInds>
int32_t CIndVShrd<O, nInds>::Bas(int iInd, int iObj) const
{
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? iObj : BAS::Bas(iInd, iObj);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::Index переопределяет ф-цию базового класса. Возвращает указатель на первый эл-т индекса,
/// здесь тип любого индекса int32_t, а в производном классе может быть CIndVShrd int64_t.
/// \param iInd: индекс, обл.значений от idel до nInds - 1
/// \param pcnt: к-во эл-тов в индексе.
/// \return указатель на первый эл-т индекса. Если запрашивается ioffset из производного класса, тип указателя
/// будет int64_t, иначе int32_t
///
template<class O, int nInds>
const void* CIndVShrd<O, nInds>::Index(int32_t iInd, int32_t *pcnt) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.Index(pcnt) : BAS::Index(iInd, pcnt);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIndVShrd::count вычисляет к-во эл-тов в индексе, берёт наименьшее из всех потому что во время добав-
/// ления нового эл-та к-во в индексах может быть разным, а достоверным в этом случае является наименьшее.
/// \return к-во эл-тов в индексе
///
template<class O, int nInds>
int32_t CIndVShrd<O, nInds>::count() const
{
	ASSERT(nInds > 0 && nInds < 100);
	return glb::Mn(m_iOfsts.count(), BAS::count());
}

template<class O, int nInds>
int32_t CIndVShrd<O, nInds>::count(int iInd) const
{
	ASSERT(nInds > 0 && nInds < 100);
	ASSERT(iInd >= idel && iInd < nInds);
	return (iInd == ioffset) ? m_iOfsts.count() : BAS::count(iInd);
}

template<class O, int nInds>
bool CIndVShrd<O, nInds>::CheckCount() const
{
	return (BAS::CheckCount() && m_iOfsts.count() == BAS::count(0));
}

template<class O, int nInds>
bool CIndVShrd<O, nInds>::IsValid() const
{
	if (!m_iOfsts.IsValid())
		return false;
	return BAS::IsValid();
}

}// namespace fl
}// namespace i