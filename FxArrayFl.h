﻿#pragma once

#include "StdAfx.h"
#include "FxArray.h"
#include "FileReader.h"

HANDLE OpenFile(LPCTSTR pcszPath,
				unsigned int uAccess,							// доступ (GENERIC_READ, GENERIC_WRITE)
				unsigned int uShare,	// = 0,					// никто не может читать/писать, если 0
				unsigned int uWait,		// = INFINITE,			// время ожидания в миллисекундах. попробую так, чёто глючит сука, не открывается
				unsigned int uMethod,	// = OPEN_ALWAYS,		// создаю новый файл, если не найден, или открываю имеющийся
				unsigned int uFileAttributes); //= FILE_ATTRIBUTE_NORMAL);

// индексированный массив, хранящихся в файле объектов не одинакового размера...
// отступ от начала файла является уникальным идентификатором объекта. Постоянность идентификатора не гарантируется за
// пределами класса до тех пор, пока небыло удаления из файла объектов.
// в файл объекты добавляются в порядке поступления - в конец файла, а индекс хранит идентификатор объекта
//
// Требования:
// 1. объект должен содержать переменную __int64 m_nOfset; (offset - отступ) ...
// 2. объект должен иметь ф-ции чтения и записи в файл size_t ReadFile(HANDLE) и константную size_t WriteFile(HANDLE) const
// 3. объект должен иметь ф-цию BOOL IsValid()
// 4. первый индекс массива должен быть уникальным
//
// удаление и редактирование пока не предусмотрено

template <class TP, int nIndices> class CFxArrayFl
{
public:
	typedef int(*fcmp)(const TP&, const TP&);

	template <class TP1> class CFile
	{
	protected:
		TCHAR m_pszFile[MAX_PATH];
		HANDLE m_hFile;
		TP1 m_obj;			// объект TP должен содержать ф-цию "size_t ReadFile(HFILE)" и такую же для записи
	public:
		CFile();
		~CFile();
	public:
		const TP1 & GetLastRead() { return m_obj; }
		LPCTSTR FilePath() { return m_pszFile; }
		void Add(TP1 & o);					// ф-ция добавит новый эл-т, вернёт позицию от начала файла в кот.записан 'o'
		__int64 Edit(const TP1 & o);		// редактирует эл-т в файле, вернёт позицию от начала файла в кот.записан 'o'
		void Del(const TP1 & o);			// удаляет из файла объект
		TP1 & operator[](__int64 nOfset);	// оператор вернёт объет из файла m_file (прочтёт файл)
		void OpenFile(LPCTSTR pcszPath, DWORD dwAccess = GENERIC_READ|GENERIC_WRITE, DWORD dwShareMode = 0);
		void CloseFile();
	};
	
	class CIndFile
	{
	public:
		HANDLE m_hFile;
		TCHAR m_pszPath[MAX_PATH];
	public:
		CIndFile(LPCTSTR pcszPath, unsigned int nAccess = GENERIC_READ|GENERIC_WRITE, unsigned int nShrMode = 0);
		~CIndFile();
		void Read(CFxArray<__int64, TRUE> & aind);
		void Write(CFxArray<__int64, TRUE> & aind);
	};

	template <class TP2> class CIndex
	{
	public:
		CFile<TP2> & m_file;					// ссылка на CFxArrayFl<TP>. Используется в CompareObjects
		CFxArray<__int64, TRUE> m_ao;		// упорядоченный массив индексов (отступов) объектов от начала файла
		fcmp m_fcmp;	// int(*m_fcmp)(const TP1&, const TP1&);	// указатель на ф-цию сравнения объектов
	public:
		CIndex(CFile<TP2> & file, fcmp af);
	public:
		int Add(const TP2 & o);
		int FindEqual(const TP2 & o);
		int FindNearest(const TP2 & o);
		void Del(const TP2 & o);
		TP2 & operator[](int);
	};
protected:
	CFile<TP> m_file;						// файл с объектами
	CIndex<TP> *m_ppIndices[nIndices];		// массив указателей на CIndex<TP> - заданные индексы
	int m_iActiveIndex;
public:
	CFxArrayFl();
	CFxArrayFl(LPCTSTR pcszPath, fcmp *ppf, DWORD dwFileAccess = GENERIC_READ|GENERIC_WRITE, DWORD dwShareFile = 0);
	~CFxArrayFl();
	void Create(LPCTSTR pcszPath, fcmp *ppf, DWORD dwFileAccess = GENERIC_READ|GENERIC_WRITE, DWORD dwShareFile = 0);
	void Close();
	void PutIndices();
public:
	void Add(TP & o);
	void Del(const TP & o, __int64 nOfset = -1);
	void Edit(const TP & o, __int64 nOfset = -1);
public:
	int FindEqual(const TP & o, int iIndex = -1);
	int FindNearest(const TP & o, int iIndex = -1);
	BOOL IsOpen() { return m_file.m_hFile != INVALID_HANDLE_VALUE && m_ppIndices[sizeof m_ppIndices / sizeof *m_ppIndices - 1]; }
	unsigned int GetCount() const;
	TP & GetItem(int iIndex, int iItem) { return (*m_ppIndices[iIndex])[iItem]; }
	TP & operator[](int i) { return (*m_ppIndices[m_iActiveIndex])[i]; }
public:
	int SetActiveIndex(int i) { return((i >= 0 && i < nIndices) ? (m_iActiveIndex = i) : -1); }		// для оператора []
};

template <class TP, int nIndices>
CFxArrayFl<TP, nIndices>::CFxArrayFl()
{
	memset(m_ppIndices, 0, sizeof m_ppIndices);
	m_iActiveIndex = 0;
}

template <class TP, int nIndices>
CFxArrayFl<TP, nIndices>::CFxArrayFl(LPCTSTR pcszPath, fcmp *ppf, DWORD dwFileAccess, DWORD dwShareFile) // dwFileAccess = GENERIC_READ|GENERIC_WRITE, dwShareFile = 0) :
{
	memset(m_ppIndices, 0, sizeof m_ppIndices);
	Create(pcszPath, ppf, dwFileAccess, dwShareFile);
	m_iActiveIndex = 0;
}

template <class TP, int nIndices>
CFxArrayFl<TP, nIndices>::~CFxArrayFl()
{
	for (int i = 0; i < nIndices; i++)
		if(m_ppIndices[i])
			delete m_ppIndices[i];
}

// Create открывает файл с элементами и читает индексы
template<class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::Create(LPCTSTR pcszPath, fcmp *ppf, DWORD dwFileAccess, DWORD dwShareFile) // dwFileAccess = GENERIC_READ|GENERIC_WRITE, dwShareFile = 0
{
	TCHAR pszPath[MAX_PATH], *pszExt;
	int nMxLen;
	
	lstrcpy(pszPath, pcszPath);
	pszExt = CFileReader::GetFileExt(pszPath);
	nMxLen = MAX_PATH - lstrlen(pcszPath) + lstrlen(pszExt) - 1;
	
	m_file.OpenFile(pcszPath, dwFileAccess, dwShareFile);

	for (int i = 0; i < nIndices; i++)
	{
		m_ppIndices[i] = new CIndex<TP>(m_file, ppf[i]);
		_sntprintf_s(pszExt, nMxLen, _TRUNCATE, _T("i%d.bin"), i + 1);
		CIndFile indf(pszPath);
		indf.Read(m_ppIndices[i]->m_ao);
	}
#ifdef _DEBUG
	// Коррекция размера к-ва индексов. Возникает ошибка, когда один из индексов содержит больше эл-тов
	// чем другие. Вероятно, в случае неправильного завершения потока или программы. Оставлю пока в отладке.
	int i, nMin = INT_MAX;
	for (i = 0; i < nIndices; i++)
		if (m_ppIndices[i]->m_ao.GetCount() < nMin)
			nMin = m_ppIndices[i]->m_ao.GetCount();
	for(i = 0; i < nIndices; i++)
		if (m_ppIndices[i]->m_ao.GetCount() > nMin)
		{
			_sntprintf_s(pszExt, nMxLen, _TRUNCATE, _T("i%d.bin"), i + 1);
			CIndFile indf(pszPath);
			m_ppIndices[i]->m_ao.SetCount(nMin);
			indf.Write(m_ppIndices[i]->m_ao);
		}
#endif
}

// закрывает файл, удаляет индексы. Не забудь их записать перед закрытием массива, открытого
// для записи.
template<class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::Close()
{
	for (int i = 0; i < nIndices; i++)
	{
		delete m_ppIndices[i];
		m_ppIndices[i] = NULL;
	}
	m_iActiveIndex = 0;
	m_file.CloseFile();
}

template<class TP, int nIndices>
inline void CFxArrayFl<TP, nIndices>::PutIndices()
{
	int nMxLen;
	LPTSTR pszExt;
	TCHAR pszPath[MAX_PATH];
	lstrcpy(pszPath, m_file.FilePath());

	pszExt = CFileReader::GetFileExt(pszPath);
	nMxLen = MAX_PATH - lstrlen(pszPath) + lstrlen(pszExt) - 1;

	for (int i = 0; i < nIndices; i++)
	{
		_sntprintf_s(pszExt, nMxLen, _TRUNCATE, _T("i%d.bin"), i + 1);
		CIndFile indf(pszPath);
		indf.Write(m_ppIndices[i]->m_ao);
	}
}

// перед использованием, поищи эл-т в файле... FinEqual
template <class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::Add(TP & o)
{
	// если в файле есть такой объект... (использую уникальный индекс)
	// действо выводится из ф-ции для гибкости: используй FinEqual
	m_file.Add(o);
	for (int i = 0; i < nIndices; i++)
		m_ppIndices[i]->Add(o);
	// добавляя нов.эл-т в массив, запишу индексы... выведено из ф-ции
}

template <class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::Del(const TP & o, __int64 nOfset)	// nOfset = -1
{
	ASSERT(FALSE);	// оставлено "на потом" - нужно удалить объект из файла и из всех индексо (линейный поиск в индексах)
					// либо, удаляя объект в файле обнулять его, оставив размер и отступ неизменными.
}

template <class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::Edit(const TP & o, __int64 nOfset)	// nOfset = -1
{
	ASSERT(FALSE);
}

// FindEqual ищет 'o' по первому индексу, который должен быть уникальным
/*template <class TP, int nIndices>
__int64 CFxArrayFl<TP, nIndices>::FindEqual(const TP & o)
{
	int 
	return 0;
}

template <class TP, int nIndices>
__int64 CFxArrayFl<TP, nIndices>::FindNearest(const TP & o)
{
	return 0;
}*/

template<class TP, int nIndices>
inline int CFxArrayFl<TP, nIndices>::FindEqual(const TP & o, int iIndex) // iIndex = -1
{
	if (iIndex < 0)
		iIndex = m_iActiveIndex;
	ASSERT(iIndex >= 0 && iIndex < nIndices);
	return m_ppIndices[iIndex]->FindEqual(o);
}

// FindNearest вернёт индекс эл-та в индексе iIndex (или m_iActiveIndex если iIndex = -1)
template<class TP, int nIndices>
inline int CFxArrayFl<TP, nIndices>::FindNearest(const TP & o, int iIndex) // iIndex = -1
{
	if (iIndex < 0)
		iIndex = m_iActiveIndex;
	ASSERT(iIndex >= 0 && iIndex < nIndices);
	return m_ppIndices[iIndex]->FindNearest(o);
}

template<class TP, int nIndices>
inline unsigned int CFxArrayFl<TP, nIndices>::GetCount() const
{
#ifdef _DEBUG
	for (int i = 0; i < nIndices; i++)
		ASSERT(i == nIndices - 1 || m_ppIndices[i]->m_ao.GetCount() == m_ppIndices[i + 1]->m_ao.GetCount());
#endif
	return m_ppIndices[0]->m_ao.GetCount();
}

template<class TP, int nIndices>
template<class TP1>
CFxArrayFl<TP, nIndices>::CFile<TP1>::CFile()
{
	*m_pszFile = _T('\0');
	m_hFile = INVALID_HANDLE_VALUE;
}

/*template <class TP, int nIndices>
template <class TP2>
CFxArrayFl<TP, nIndices>::CFile<TP2>::CFile(LPCTSTR pszPath)
{
	OpenFile(pszPath);
}*/

template <class TP, int nIndices>
template <class TP2>
CFxArrayFl<TP, nIndices>::CFile<TP2>::~CFile()
{
	if(m_hFile && m_hFile != INVALID_HANDLE_VALUE)
		CloseFile();
}

template <class TP, int nIndices>
template <class TP2>
inline void CFxArrayFl<TP, nIndices>::CFile<TP2>::OpenFile(LPCTSTR pcszPath, DWORD dwAccess, DWORD dwShareMode) // dwAccess = GENERIC_READ|GENERIC_WRITE, dwShareMode = 0
{
	ASSERT(m_hFile == INVALID_HANDLE_VALUE);
	lstrcpy(m_pszFile, pcszPath);
	m_hFile = ::OpenFile(pcszPath, dwAccess, dwShareMode, INFINITE, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL);
}

template <class TP, int nIndices>
template <class TP2>
inline void CFxArrayFl<TP, nIndices>::CFile<TP2>::CloseFile()
{
	ASSERT(m_hFile != INVALID_HANDLE_VALUE);
	::CloseHandle(m_hFile);
	m_hFile = INVALID_HANDLE_VALUE;
}

// оператор читает файл по указанному в аргументе адресу и возвращает ссылку на прочтённый объект
// если возникает ошибка, оператор швыряет исключение
template <class TP, int nIndices>
template <class TP2>
TP2 & CFxArrayFl<TP, nIndices>::CFile<TP2>::operator[](__int64 nOfset)
{
	ASSERT(m_hFile != INVALID_HANDLE_VALUE);
	ASSERT(sizeof(long int) == sizeof(__int64) / 2);
	SetFilePointer(m_hFile, &nOfset, FILE_BEGIN, m_pszFile);
	m_obj.ReadFile(m_hFile);
	return m_obj;
}

// ф-ция добавляет 'o' в конец файала
template <class TP, int nIndices>
template <class TP1>
void CFxArrayFl<TP, nIndices>::CFile<TP1>::Add(TP1 & o)
{
	ASSERT(sizeof(long int) == sizeof(__int64) / 2);
	o.m_nOfset = 0;
	SetFilePointer(m_hFile, &o.m_nOfset, FILE_END, m_pszFile);
	o.WriteFile(m_hFile);
}

template <class TP, int nIndices>
template <class TP1>
__int64 CFxArrayFl<TP, nIndices>::CFile<TP1>::Edit(const TP1 & o)
{
	ASSERT(FALSE);
	return 0;
}

template <class TP, int nIndices>
template <class TP1>
void CFxArrayFl<TP, nIndices>::CFile<TP1>::Del(const TP1 & o)
{
	ASSERT(FALSE);
}

template <class TP, int nIndices>
CFxArrayFl<TP, nIndices>::CIndFile::CIndFile(LPCTSTR pcszPath, unsigned int nAccess, unsigned int nShrMode)
{
	_tcscpy_s(m_pszPath, MAX_PATH, pcszPath);
	m_hFile = ::OpenFile(m_pszPath, nAccess, nShrMode, INFINITE, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL);
}

template <class TP, int nIndices>
CFxArrayFl<TP, nIndices>::CIndFile::~CIndFile()
{
	if(m_hFile)
		::CloseHandle(m_hFile);
}

// заменяю содержимое файла содержимым массива 'aind'
template<class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::CIndFile::Write(CFxArray<__int64, TRUE>& aind)
{
	ASSERT(sizeof(__int64) == sizeof(DWORD) * 2);
	ASSERT(m_hFile);
	DWORD dwWrite, dwWritten, dwError;
	__int64 nSz = 0, nWritten;
	SetFilePointer(m_hFile, &nSz, FILE_BEGIN, m_pszPath);
	nSz = aind.GetCount() * sizeof(__int64);
	for (nWritten = 0; nWritten < nSz; nWritten += dwWritten)
	{
		dwWrite = (DWORD) min(ULONG_MAX, nSz - nWritten);
		if (!::WriteFile(m_hFile, (LPBYTE)(__int64*)aind + nWritten, dwWrite, &dwWritten, NULL) || dwWrite != dwWritten)
		{
			dwError = GetLastError();
			AfxThrowFileException(CFileException::OsErrorToException((long int)dwError), (long int)dwError, _T("CFxArrayFl<TP, nIndices>::CIndFile::Write"));
		}
	}
}

template <class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::CIndFile::Read(CFxArray<__int64, TRUE> & aind)
{
	ASSERT(sizeof(LARGE_INTEGER) == sizeof(__int64));
	ASSERT(m_hFile);
	__int64 nFlSz,		// размер файла байт
			nRead;		// всего прочёл байт
	DWORD	dwRead,		// прочёл байт за раз
			dwToRead,	// нужно прочесть байт
			dwError;
	LPBYTE	pbWriteTo;	// адрес в памяти, куда помещать содержимое файла
	if (!::GetFileSizeEx(m_hFile, (PLARGE_INTEGER) &nFlSz))
	{
		dwError = GetLastError();
		AfxThrowFileException(CFileException::OsErrorToException((long int)dwError), (long int)dwError, _T("CFxArrayFl::CIndFile::ReadData - GetFileSizeEx fail"));
	}
	ASSERT(!(nFlSz % sizeof(__int64)));
	aind.SetCount((int) (nFlSz / sizeof(__int64)));
	for (nRead = 0; nRead < nFlSz; nRead += dwRead)
	{
		dwToRead = min(UINT_MAX, (unsigned int) (nFlSz - nRead));
		pbWriteTo = (LPBYTE)(__int64*) aind + nRead;
		if (!::ReadFile(m_hFile, pbWriteTo, dwToRead, &dwRead, NULL) || dwRead != dwToRead)
		{
			dwError = GetLastError();
			AfxThrowFileException(CFileException::OsErrorToException((long int)dwError), (long int)dwError, _T("CFxArrayFl::CIndFile::ReadData - ReadFile fail"));
		}
	}
}

/*template <class TP, int nIndices>
void CFxArrayFl<TP, nIndices>::CIndFile::ReadData(CFxArray<__int64> & aind)
{
	ASSERT(m_hFile);
	__int64 nFlSz,		// размер файла байт
			nRead = 0,	// всего прочёл байт
			nElements,	// к-во элементов массива в файле
			nTmp;
	DWORD	dwRead,		// прочитано байт
			dwToRead;	// нужно прочесть байт
	LPBYTE	pbWriteTo;	// адрес в памяти, куда помещать содержимое файла
	if (!::GetFileSizeEx(m_hFile, &nFlSz))
	{
		nOfset = GetLastError();
		AfxThrowFileException(CFileException::OsErrorToException((long int)nOfset), (long int)nOfset, _T("CFxArrayFl::CIndFile::ReadData - GetFileSizeEx fail"));
	}
	ASSERT(nFlz % 3 == 0);	// в файле должны быть 2 массива одинаковой длины: массив 64-х битных целых и массив 32-х битных целых
	nElements = nFlSz / 3 / sizeof(int);
	aind.SetCount((int) nElements);	// к-во элементов массива контролируется во время добавления нового эл-та
	for (nRead = 0; nRead < nFlSz; nRead += dwRead)
	{
		if (nRead < nElements * sizeof(__int64))	// ещё не прочёл массив отступов
		{
			nTmp = nElements * sizeof(__int64) - nRead;	// остаток массива отступов в файле
			dwToRead = (DWORD) min(nTmp, ULONG_MAX);
			if (!::ReadFile(m_hFile, (LPBYTE)(__int64*)aind + nRead, dwToRead, &dwRead, NULL) || dwRead != dwToRead)
			{
				nOfset = GetLastError();
				AfxThrowFileException(CFileException::OsErrorToException((long int)nOfset), (long int)nOfset, _T("CFxArrayFl::CIndFile::ReadData - ReadFile fail"));
			}
		}
		else	// прочёл массив __int64, теперь int'ы
		{
			ASSERT(nFlSz / 3 == nElements * sizeof(int));
			nTmp = nFlSz / 3;	// треть файла - это массив int'ов - индекс CFxTesterInd
			dwToRead = (DWORD) min(nTmp, ULONG_MAX);
			pbWriteTo = (LPBYTE) aind.m_pInd->m_pi + (nRead - nElements * sizeof(__int64));
			if (!::ReadFile(m_hFile, pbWriteTo, dwToRead, &dwRead, NULL) || dwRead != dwToRead)
			{
				nOfset = GetLastError();
				AfxThrowFileException(CFileException::OsErrorToException((long int)nOfset), (long int)nOfset, _T("CFxArrayFl::CIndFile::ReadData - ReadFile fail (#2)"));
			}
		}
	}
}*/

/*template <class TP, int nIndices>
template <class TP2>
CFxArrayFl<TP, nIndices>::CIndex<TP2>::CIndex() :
	m_ao(32, FALSE)
{
	m_fcmp = NULL;
}*/

// file - файл с объектами
// fcmp - ф-ция сравнения объектов
// nIndices
template <class TP, int nIndices>
template <class TP2>
CFxArrayFl<TP, nIndices>::CIndex<TP2>::CIndex(CFile<TP2> & file, fcmp pf) :
	m_ao(32),
	m_file(file)
{
	m_fcmp = pf;
}

/*template <class TP, int nIndices>
template <class TP2>
int CFxArrayFl<TP, nIndices>::CIndex<TP2>::CompareObjects(const __int64 & n1, const __int64 & n2)
{
	return m_fcmp(m_file[n1], m_file[n2]);
}*/

// объект 'o' должен иметь отступ от начала файла в o.m_nOfset т.е. должен быть уже в конце файла
template <class TP, int nIndices>
template <class TP2>
int CFxArrayFl<TP, nIndices>::CIndex<TP2>::Add(const TP2 & o)
{
	ASSERT(o.m_nOfset >= 0);
	const __int64 *po = m_ao;
	int a = 0, z = m_ao.GetCount() - 1, i, n;
	for (i = z / 2; a <= z; i = a + (z - a) / 2)
	{
		n = m_fcmp(o, m_file[po[i]]);
		if(n < 0)
			z = i - 1;
		else if(n > 0)
			a = i + 1;
		else
			break;
	}
	ASSERT(i <= m_ao.GetCount());
	if (i < m_ao.GetCount())
	{
		ASSERT(m_fcmp(o, m_file[po[i]]) <= 0); // объект должен быть меньше или равен сдвигаемому вправо
		m_ao.Insert(i, o.m_nOfset);
	}
	else
	{
		ASSERT(!i || m_fcmp(o, m_file[po[i - 1]]) >= 0); // объект должен быть больше или равен последнему
		m_ao.Add(&o.m_nOfset);
	}
	return i;
	// теперь обращение к индексу ind[i] вернёт объект
}

// FindEqual ищет 'o' в файле. Индекс *this должен быть уникальным.
template<class TP, int nIndices>
template<class TP2>
int CFxArrayFl<TP, nIndices>::CIndex<TP2>::FindEqual(const TP2 & o)
{
	ASSERT(o.IsValid());
	__int64 *po = m_ao;
	int a = 0, z = m_ao.GetCount() - 1, i, n;
	for (i = z / 2; a <= z; i = a + (z - a) / 2)
	{
		n = m_fcmp(o, m_file[po[i]]);
		if (n < 0)
			z = i - 1;
		else if (n > 0)
			a = i + 1;
		else
		{
			ASSERT(!i || m_fcmp(o, m_file[po[i - 1]]) > 0);
			ASSERT(m_ao.GetCount() - i <= 1 || m_fcmp(o, m_file[po[i + 1]]) < 0);
			return i;
		}
	}
	return -1;
}

// FindNearest находит индекс (в этом индексе) равного или следующего объекта. Результат может быть равен к-ву эл-ов.
//		**		больше 5-го и меньше 6-го
// 0123456789
// a = 0, z = 10,	i = 5		= 1
// a = 6, z = 10,	i = 8		= -1
// a = 6, z = 7,	i = 6		= -1
// a = 6, z = 5 в этой ситуации неясно, проверялся ли 5-й
template<class TP, int nIndices>
template<class TP2>
int CFxArrayFl<TP, nIndices>::CIndex<TP2>::FindNearest(const TP2 & o)
{
	ASSERT(o.IsValid());
	int64_t *po = m_ao;
	int i,
		a = 0,
		z = m_ao.GetCount(),
		r;
	for (i = z / 2; a < z; i = (a + z) / 2)
	{
		r = m_fcmp(o, m_file[po[i]]);
		if (r < 0)
			z = i - 1;
		else if (r > 0)
			a = i + 1;
		else
			break;
	}// если 'о' меньше первого эл-та, 'z' может равняться -1. Еесли 'o' больше последнего, 'i' будет равно к-ву
	if (z <= a)	// нет идентичного эл-та
	{
		ASSERT(i >= 0 && i <= m_ao.GetCount());	// i == min(a, z)
		if (i < m_ao.GetCount() && m_fcmp(o, m_file[po[i]]) > 0)
			i++;
	}
	return i;
}

template <class TP, int nIndices>
template <class TP2>
void CFxArrayFl<TP, nIndices>::CIndex<TP2>::Del(const TP2 & o)
{
	ASSERT(FALSE);
}

template <class TP, int nIndices>
template <class TP2>
inline TP2 & CFxArrayFl<TP, nIndices>::CIndex<TP2>::operator[](int i)
{
	return m_file[m_ao[i]];
}