/*
 * Copyright 2021 Николай Вамбольдт
 *
 * Файл 'oa_structs.h' — часть программ 'liner' и 'oacrl', входящих в проект
 * 'linerfx'
 *
 * 'liner' и 'oacrl' свободные программы: вы можете перераспространять их и/или
 * изменять их на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * 'liner' и 'oacrl' распространяются в надежде, что они будут полезными,
 * но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
 * общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с этой программой. Если это не так, см.
 * <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * 'oa_structs.h' is part of 'liner' and 'oacrl', which are a part of
 * 'linerfx'.
 *
 * 'liner' and 'oacrl' are free software: you can redistribute them and/or
 * modify them under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 'liner' and 'oacrl' are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE
 * along with 'liner' and 'oacrl'. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "oa_structs.h"

extern thread_local double g_eps;
extern thread_local double g_pnt;
thread_local double g_epsTU;		// trade unit epsilon

namespace oatp {
static thread_local glb::CFxArray<BYTE, true> g_atmp(256);

CandlestickData::CandlestickData()
{
	o = h = l = c = 0.0;
}

int CandlestickData::operator ==(const CandlestickData &cd) const
{
   return(DCMP_EQUAL(o, cd.o, g_eps) &&
		  DCMP_EQUAL(h, cd.h, g_eps) &&
		  DCMP_EQUAL(l, cd.l, g_eps) &&
		  DCMP_EQUAL(c, cd.c, g_eps));
}

void Instrument::Init()
{	// целые числа
	type = guaranteedStopLossOrderMode = pipLocation = displayPrecision = tradeUnitsPrecision = 0;
	// действительные
	minimumTradeSize = maximumTrailingStopDistance = minimumGuaranteedStopLossDistance =
	minimumTrailingStopDistance = maximumPositionSize = maximumOrderUnits = marginRate =
	   guaranteedStopLossOrderExecutionPremium = 0.0;
	// массивы и строки
	tags.SetCountZeroND();
	name.SetCountZeroND();
	displayName.SetCountZeroND();
	// структуры
	financing.Init();
	commission.Init();
	guaranteedStopLossOrderLevelRestriction.Init();
}

/* putcp: записываю сведения об инструменте в файл в текущую позицию 'at' (должна точно
 * соответствовать)
 */
int Instrument::putcp(long int at, fl::CBinFile &file) const
{	// целые числа
	const long int ns = at;
	at += file.putcp(at, &type, sizeof type);
	at += file.putcp(at, &guaranteedStopLossOrderMode, sizeof guaranteedStopLossOrderMode);
	at += file.putcp(at, &pipLocation, sizeof pipLocation);
	at += file.putcp(at, &displayPrecision, sizeof displayPrecision);
	at += file.putcp(at, &tradeUnitsPrecision, sizeof tradeUnitsPrecision);
	// действительные
	at += file.putcp(at, &minimumTradeSize, sizeof minimumTradeSize);
	at += file.putcp(at, &maximumTrailingStopDistance, sizeof maximumTrailingStopDistance);
	at += file.putcp(at, &minimumGuaranteedStopLossDistance, sizeof minimumGuaranteedStopLossDistance);
	at += file.putcp(at, &minimumTrailingStopDistance, sizeof minimumTrailingStopDistance);
	at += file.putcp(at, &maximumPositionSize, sizeof maximumPositionSize);
	at += file.putcp(at, &maximumOrderUnits, sizeof maximumOrderUnits);
	at += file.putcp(at, &marginRate, sizeof marginRate);
	at += file.putcp(at, &guaranteedStopLossOrderExecutionPremium, sizeof guaranteedStopLossOrderExecutionPremium);
	// массивы и строки
	at += tags.putcp(at, file);
	at += name.putcp(at, file);
	at += displayName.putcp(at, file);
	// структуры
	at += financing.putcp(at, file);
	at += file.putcp(at, &commission, sizeof commission);
	at += file.putcp(at, &guaranteedStopLossOrderLevelRestriction, sizeof guaranteedStopLossOrderLevelRestriction);
	return (int) (at - ns);
}

int Instrument::getcp(long int at, fl::CBinFile &file)
{	// целые числа
	const long int ns = at;
	at += file.getcp(at, &type, sizeof type);
	at += file.getcp(at, &guaranteedStopLossOrderMode, sizeof guaranteedStopLossOrderMode);
	at += file.getcp(at, &pipLocation, sizeof pipLocation);
	at += file.getcp(at, &displayPrecision, sizeof displayPrecision);
	at += file.getcp(at, &tradeUnitsPrecision, sizeof tradeUnitsPrecision);
	// действительные
	at += file.getcp(at, &minimumTradeSize, sizeof minimumTradeSize);
	at += file.getcp(at, &maximumTrailingStopDistance, sizeof maximumTrailingStopDistance);
	at += file.getcp(at, &minimumGuaranteedStopLossDistance, sizeof minimumGuaranteedStopLossDistance);
	at += file.getcp(at, &minimumTrailingStopDistance, sizeof minimumTrailingStopDistance);
	at += file.getcp(at, &maximumPositionSize, sizeof maximumPositionSize);
	at += file.getcp(at, &maximumOrderUnits, sizeof maximumOrderUnits);
	at += file.getcp(at, &marginRate, sizeof marginRate);
	at += file.getcp(at, &guaranteedStopLossOrderExecutionPremium, sizeof guaranteedStopLossOrderExecutionPremium);
	// массивы и строки
	at += tags.getcp(at, file);
	at += name.getcp(at, file);
	at += displayName.getcp(at, file);
	// структуры
	at += financing.getcp(at, file);
	at += file.getcp(at, &commission, sizeof commission);
	at += file.getcp(at, &guaranteedStopLossOrderLevelRestriction, sizeof guaranteedStopLossOrderLevelRestriction);
	// alles...
	return (int) (at - ns);
}

bool Instrument::operator ==(const Instrument &i) const
{
	ASSERT(g_eps != 0.0 && g_epsTU != 0.0);
	return (type == i.type && guaranteedStopLossOrderMode == i.guaranteedStopLossOrderMode &&
			pipLocation == i.pipLocation && displayPrecision == i.displayPrecision &&
			tradeUnitsPrecision == i.tradeUnitsPrecision &&
			// действительные
			DCMP_EQUAL(minimumTradeSize, i.minimumTradeSize, g_epsTU) &&
			DCMP_EQUAL(maximumTrailingStopDistance, i.maximumTrailingStopDistance, g_eps) &&
			DCMP_EQUAL(minimumGuaranteedStopLossDistance, i.minimumGuaranteedStopLossDistance, g_eps) &&
			DCMP_EQUAL(minimumTrailingStopDistance, i.minimumTrailingStopDistance, g_eps) &&
			DCMP_EQUAL(maximumPositionSize, i.maximumPositionSize, g_epsTU) &&
			DCMP_EQUAL(maximumOrderUnits, i.maximumOrderUnits, g_epsTU) && DCMP_EQUAL(marginRate, i.marginRate, g_eps) &&
			DCMP_EQUAL(guaranteedStopLossOrderExecutionPremium, i.guaranteedStopLossOrderExecutionPremium, g_eps) &&
			// массивы и строки
			tags == i.tags && name == i.name && displayName == i.displayName &&
			// структуры
			financing == i.financing && commission == i.commission &&
			guaranteedStopLossOrderLevelRestriction == i.guaranteedStopLossOrderLevelRestriction);
}

int Tag::putcp(long int at, fl::CBinFile &file) const
{
	const long int ns = at;
	at += type.putcp(at, file);
	at += name.putcp(at, file);
	return (int) (at - ns);
}

// читаю pb, возвращаю к-во прочитанных байт
int Tag::getcp(long int at, fl::CBinFile &file)
{
	const long int ns = at;
	at += type.getcp(at, file);
	at += name.getcp(at, file);
	return (int) (at - ns);
}

bool Tag::operator ==(const Tag &t) const
{
	return (type == t.type && name == t.name);
}

int InstrumentFinancing::putcp(long int at, fl::CBinFile &file) const
{
	const long int ns = at;
	at += file.putcp(at, &longRate, sizeof longRate);
	at += file.putcp(at, &shortRate, sizeof shortRate);
	return ((int) (at - ns) + financingDaysOfWeek.putcp(at, file));
}

int InstrumentFinancing::getcp(long int at, fl::CBinFile &file)
{
	const long int ns = at;
	at += file.getcp(at, &longRate, sizeof longRate);
	at += file.getcp(at, &shortRate, sizeof shortRate);
	return ((int) (at - ns) + financingDaysOfWeek.getcp(at, file));
}

bool InstrumentFinancing::operator ==(const InstrumentFinancing &i) const
{
	return (DCMP_EQUAL(longRate, i.longRate, .001) && DCMP_EQUAL(shortRate, i.shortRate, .001) &&
			financingDaysOfWeek == i.financingDaysOfWeek);
}

bool InstrumentCommission::operator ==(const InstrumentCommission &i) const
{
	return (DCMP_EQUAL(commission, i.commission, g_eps) &&
			DCMP_EQUAL(unitsTraded, i.unitsTraded, g_eps) &&
			DCMP_EQUAL(minimumCommission, i.minimumCommission, g_eps));
}

bool GuaranteedStopLossOrderLevelRestriction::operator ==(const GuaranteedStopLossOrderLevelRestriction &g) const
{
	return (DCMP_EQUAL(volume, g.volume, g_eps) && DCMP_EQUAL(priceRange, g.priceRange, g_eps));
}

MarketOrder::MarketOrder()
{	// целые:
	type = state = timeInForce = positionFill = id = partialFill = fillingTransactionID =
			tradeOpenedID = tradeReducedID = cancellingTransactionID = 0;
	// действительные
	units = priceBound = 0.0;
}

/* GetBinData добавляет в 'ab' всё своё содержимое.
 */
int MarketOrder::GetBinData(glb::CFxArray<BYTE, true> &ab) const
{
	const int cnt = ab.GetCount();
	// целые
	ab.Add((const BYTE*) &type, sizeof type);
	ab.Add((const BYTE*) &state, sizeof state);
	ab.Add((const BYTE*) &timeInForce, sizeof timeInForce);
	ab.Add((const BYTE*) &positionFill, sizeof positionFill);
	ab.Add((const BYTE*) &id, sizeof id);
	ab.Add((const BYTE*) &partialFill, sizeof partialFill);
	ab.Add((const BYTE*) &fillingTransactionID, sizeof fillingTransactionID);
	ab.Add((const BYTE*) &tradeOpenedID, sizeof tradeOpenedID);
	ab.Add((const BYTE*) &tradeReducedID, sizeof tradeReducedID);
	ab.Add((const BYTE*) &cancellingTransactionID, sizeof cancellingTransactionID);
	// действительные
	ab.Add((const BYTE*) &units, sizeof units);
	ab.Add((const BYTE*) &priceBound, sizeof priceBound);
	// структуры DateTime
	ab.Add((const BYTE*) &createTime, sizeof createTime);
	ab.Add((const BYTE*) &cancelledTime, sizeof cancelledTime);
	ab.Add((const BYTE*) &filledTime, sizeof filledTime);
	// структура ClientExtensions
	clientExtensions.GetBinData(ab);
	tradeClientExtensions.GetBinData(ab);
	// структура (строка) InstrumentName
	instrument.GetBinData(ab);
	// структура-массив tradeReducedID
	tradeClosedIDs.GetBinData(ab);
	// структура MarketOrderTradeClose
	tradeClose.GetBinData(ab);
	// структура MarketOrderPositionCloseout
	longPositionCloseout.instrument.GetBinData(ab);
	longPositionCloseout.units.GetBinData(ab);
	// структура MarketOrderPositionCloseout
	shortPositionCloseout.instrument.GetBinData(ab);
	shortPositionCloseout.units.GetBinData(ab);
	// структура MarketOrderMarginCloseout
	ab.Add((const BYTE*) &marginCloseout.reason, sizeof marginCloseout.reason);
	// структура MarketOrderDelayedTradeClose
	ab.Add((const BYTE*) &delayedTradeClose, sizeof delayedTradeClose);
	// структура TakeProfitDetails
	takeProfitOnFill.GetBinData(ab);
	// структура StopLossDetails
	stopLossOnFill.GetBinData(ab);
	// структура GuaranteedStopLossDetails
	guaranteedStopLossOnFill.GetBinData(ab);
	// структура TrailingStopLossDetails
	trailingStopLossOnFill.GetBinData(ab);
	return ((unsigned short) (ab.GetCount() - cnt));
}

int MarketOrder::SetBinData(const BYTE *p, const int cnt)
{
	int i = 0;
	if (cnt < (int) (sizeof type + sizeof state + sizeof timeInForce + sizeof positionFill +
			sizeof id + sizeof partialFill + sizeof fillingTransactionID + sizeof tradeOpenedID +
			sizeof tradeReducedID + sizeof cancellingTransactionID + sizeof units + sizeof priceBound))
		throw err::CLocalException(SI_NED);
	memcpy(&type, p, sizeof type);
	i = sizeof type;
	memcpy(&state, p + i, sizeof state);
	i += sizeof state;
	memcpy(&timeInForce, p + i, sizeof timeInForce);
	i += sizeof timeInForce;
	memcpy(&positionFill, p + i, sizeof positionFill);
	i += sizeof positionFill;
	memcpy(&id, p + i, sizeof id);
	i += sizeof id;
	memcpy(&partialFill, p + i, sizeof partialFill);
	i += sizeof partialFill;
	memcpy(&fillingTransactionID, p + i, sizeof fillingTransactionID);
	i += sizeof fillingTransactionID;
	memcpy(&tradeOpenedID, p + i, sizeof tradeOpenedID);
	i += sizeof tradeOpenedID;
	memcpy(&tradeReducedID, p + i, sizeof tradeReducedID);
	i += sizeof tradeReducedID;
	memcpy(&cancellingTransactionID, p + i, sizeof cancellingTransactionID);
	i += sizeof cancellingTransactionID;
	// действительные
	memcpy(&units, p + i, sizeof units);
	i += sizeof units;
	memcpy(&priceBound, p + i, sizeof priceBound);
	i += sizeof priceBound;
	// структуры DateTime
	if (i + (int) (sizeof createTime + sizeof cancelledTime + sizeof filledTime) >= cnt)
		throw err::CLocalException(SI_NED);
	memcpy(&createTime, p + i, sizeof createTime);
	i += sizeof createTime;
	memcpy(&cancelledTime, p + i, sizeof cancelledTime);
	i += sizeof cancelledTime;
	memcpy(&filledTime, p + i, sizeof filledTime);
	i += sizeof filledTime;
	// структура ClientExtensions
	i += clientExtensions.SetBinData(p + i, cnt - i);
	i += tradeClientExtensions.SetBinData(p + i, cnt - i);
	// структура (строка) InstrumentName
	i += instrument.SetBinData(p + i, cnt - i);
	// структура-массив tradeReducedID
	i += tradeClosedIDs.SetBinData(p + i, cnt - i);
	// структура MarketOrderTradeClose
	i += tradeClose.SetBinData(p + i, cnt - i);
	// структура MarketOrderPositionCloseout
	i += longPositionCloseout.instrument.SetBinData(p + i, cnt - i);
	i += longPositionCloseout.units.SetBinData(p + i, cnt - i);
	// структура MarketOrderPositionCloseout
	i += shortPositionCloseout.instrument.SetBinData(p + i, cnt - i);
	i += shortPositionCloseout.units.SetBinData(p + i, cnt - i);
	// структура MarketOrderMarginCloseout
	if (cnt < i + (int) (sizeof marginCloseout.reason + sizeof delayedTradeClose))
		throw err::CLocalException(SI_NED);
	memcpy(&marginCloseout.reason, p + i, sizeof marginCloseout.reason);
	i += sizeof marginCloseout.reason;
	// структура MarketOrderDelayedTradeClose
	memcpy(&delayedTradeClose, p + i, sizeof delayedTradeClose);
	i += sizeof delayedTradeClose;
	// структура TakeProfitDetails
	i += takeProfitOnFill.SetBinData(p + i, cnt - i);
	// структура StopLossDetails
	i += stopLossOnFill.SetBinData(p + i, cnt - i);
	// структура GuaranteedStopLossDetails
	i += guaranteedStopLossOnFill.SetBinData(p + i, cnt - i);
	// структура TrailingStopLossDetails
	return (i + trailingStopLossOnFill.SetBinData(p + i, cnt - i));
}

void MarketOrder::Invalidate()
{	// целые:
	type = state = timeInForce = positionFill = id = partialFill = fillingTransactionID =
			tradeOpenedID = tradeReducedID = cancellingTransactionID = 0;
	// действительные
	units = priceBound = 0.0;
	// структура DateTime
	createTime.Init();
	// структура ClientExtensions
	clientExtensions.Init();
	// структура (строка) InstrumentName
	instrument.SetCountZeroND();
	// структура MarketOrderTradeClose
	tradeClose.Init();
	// структура MarketOrderPositionCloseout
	longPositionCloseout.Init();
	shortPositionCloseout.Init();
	// структура MarketOrderMarginCloseout
	marginCloseout.reason = 0;
	// структура MarketOrderDelayedTradeClose
	delayedTradeClose.Init();
	// структура TakeProfitDetails
	takeProfitOnFill.Init();
	// структура StopLossDetails
	stopLossOnFill.Init();
	// структура GuaranteedStopLossDetails
	guaranteedStopLossOnFill.Init();
	// структура TrailingStopLossDetails
	trailingStopLossOnFill.Init();
	// структура ClientExtensions
	tradeClientExtensions.Init();
	// структура DateTime
	filledTime.Init();
	// структура-массив tradeReducedID
	tradeClosedIDs.SetCountZeroND();
	// структура DateTime
	cancelledTime.Init();
}

// cmp вернёт 1 если заказы равны, 0 иначе
bool MarketOrder::cmp(const MarketOrder &o, const Instrument &i) const
{	// целые
	if (type != o.type || state != o.state || timeInForce != o.timeInForce || positionFill != o.positionFill ||
			id != o.id || partialFill != o.partialFill || fillingTransactionID != o.fillingTransactionID ||
			tradeOpenedID != o.tradeOpenedID || tradeReducedID != o.tradeReducedID ||
			cancellingTransactionID != o.cancellingTransactionID)
		return false;
	// действительные
	double ueps = pow(10, i.tradeUnitsPrecision + 1),
			eps = pow(10, i.displayPrecision + 1);
	if (!DCMP_EQUAL(units, o.units, ueps) || !DCMP_EQUAL(priceBound, o.priceBound, eps))
		return false;
	// структура DateTime
	if (createTime.m_nns != o.createTime.m_nns || createTime.m_nsec != o.createTime.m_nsec)
		return false;
	// структура ClientExtensions
	if (clientExtensions == o.clientExtensions != 0)
		return false;
	// структура (строка) InstrumentName
	if (instrument == o.instrument != 0)
		return false;
	// структура MarketOrderTradeClose
	if (tradeClose == o.tradeClose != 0)
		return false;
	// структура MarketOrderPositionCloseout
	if (longPositionCloseout == o.longPositionCloseout != 0 ||
		shortPositionCloseout == o.shortPositionCloseout != 0)
		return false;
	// структура MarketOrderMarginCloseout
	if (marginCloseout.reason != o.marginCloseout.reason)
		return false;
	// структура MarketOrderDelayedTradeClose
	if (delayedTradeClose.clientTradeID != o.delayedTradeClose.clientTradeID ||
		delayedTradeClose.sourceTransactionID != o.delayedTradeClose.sourceTransactionID ||
		delayedTradeClose.tradeID != o.delayedTradeClose.tradeID)
		return false;
	// структура TakeProfitDetails
	if (takeProfitOnFill.cmp(o.takeProfitOnFill, eps) != 0)
		return false;
	// структура StopLossDetails
	if (stopLossOnFill.cmp(o.stopLossOnFill, eps) != 0)
		return false;
	// структура GuaranteedStopLossDetails
	if (guaranteedStopLossOnFill.cmp(o.guaranteedStopLossOnFill, eps) != 0 ||
			trailingStopLossOnFill.cmp(o.trailingStopLossOnFill, eps) != 0)
		return false;
	// структура ClientExtensions
	if (tradeClientExtensions == o.clientExtensions != 0)
		return false;
	// структура DateTime
	if (filledTime == o.filledTime != 0)
		return false;
	// структура-массив tradeReducedID
	return (tradeClosedIDs == o.tradeClosedIDs && cancelledTime == o.cancelledTime == 0);
}

MarketOrderTradeClose::MarketOrderTradeClose()
{
	tradeID = 0;
}

void MarketOrderTradeClose::Init()
{
	tradeID = 0;
	clientTradeID.SetCountZeroND();
	units.SetCountZeroND();
}

int MarketOrderTradeClose::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	a.Add((const BYTE*) &tradeID, sizeof tradeID);
	clientTradeID.GetBinData(a);
	units.GetBinData(a);
	return (a.GetCount() - cnt);
}

int MarketOrderTradeClose::SetBinData(const BYTE *p, const int cnt)
{
	if (cnt < (int) sizeof tradeID)
		throw err::CLocalException(SI_NED);
	memcpy(&tradeID, p, sizeof tradeID);
	int i = sizeof tradeID;
	i += clientTradeID.SetBinData(p + i, cnt - i);
	return (i + units.SetBinData(p + i, cnt - i));
}

int ClientExtensions::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	int cnt = id.GetBinData(a);
	cnt += tag.GetBinData(a);
	return (cnt + comment.GetBinData(a));
}

int ClientExtensions::SetBinData(const BYTE *p, const int cnt)
{
	int i = id.SetBinData(p, cnt);
	i += tag.SetBinData(p + i, cnt - i);
	return (i + comment.SetBinData(p + i, cnt - i));
}

int StopLossDetails::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	a.Add((const BYTE*) &timeInForce, sizeof timeInForce);
	a.Add((const BYTE*) &price, sizeof price);
	a.Add((const BYTE*) &distance, sizeof distance);
	a.Add((const BYTE*) &gtdTime, sizeof gtdTime);
	a.Add((const BYTE*) &guaranteed, sizeof guaranteed);
	clientExtensions.GetBinData(a);
	return (a.GetCount() - cnt);
}

int StopLossDetails::SetBinData(const BYTE *p, const int cnt)
{
	int i = 0;
	if (cnt < (int) (sizeof timeInForce + sizeof price + sizeof distance +
					 sizeof gtdTime + sizeof guaranteed))
		throw err::CLocalException(SI_NED);
	memcpy(&timeInForce, p, sizeof timeInForce);
	i = sizeof timeInForce;
	memcpy(&price, p + i, sizeof price);
	i += sizeof price;
	memcpy(&distance, p + i, sizeof distance);
	i += sizeof distance;
	memcpy(&gtdTime, p + i, sizeof gtdTime);
	i += sizeof gtdTime;
	memcpy(&guaranteed, p + i, sizeof guaranteed);
	i += sizeof guaranteed;
	return (i + clientExtensions.SetBinData(p + i, cnt - i));
}

int TakeProfitDetails::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	a.Add((const BYTE*) &timeInForce, sizeof timeInForce);
	a.Add((const BYTE*) &price, sizeof price);
	a.Add((const BYTE*) &gtdTime, sizeof gtdTime);
	clientExtensions.GetBinData(a);
	return (a.GetCount() - cnt);
}

int TakeProfitDetails::SetBinData(const BYTE *p, const int cnt)
{
	int i = 0;
	if (cnt < (int) (sizeof timeInForce + sizeof price + sizeof gtdTime))
		throw err::CLocalException(SI_NED);
	memcpy(&timeInForce, p, sizeof timeInForce);
	i += sizeof timeInForce;
	memcpy(&price, p + i, sizeof price);
	i += sizeof price;
	memcpy(&gtdTime, p + i, sizeof gtdTime);
	i += sizeof gtdTime;
	return (i + clientExtensions.SetBinData(p + i, cnt - i));
}

int GuaranteedStopLossDetails::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	a.Add((const BYTE*) &timeInForce, sizeof timeInForce);
	a.Add((const BYTE*) &price, sizeof price);
	a.Add((const BYTE*) &distance, sizeof distance);
	a.Add((const BYTE*) &gtdTime, sizeof gtdTime);
	clientExtensions.GetBinData(a);
	return (a.GetCount() - cnt);
}

int GuaranteedStopLossDetails::SetBinData(const BYTE *p, const int cnt)
{
	int i = 0;
	if (cnt < (int) (sizeof timeInForce + sizeof price + sizeof distance + sizeof gtdTime))
		throw err::CLocalException(SI_NED);
	memcpy(&timeInForce, p, sizeof timeInForce);
	i += sizeof timeInForce;
	memcpy(&price, p + i, sizeof price);
	i += sizeof price;
	memcpy(&distance, p + i, sizeof distance);
	i += sizeof distance;
	memcpy(&gtdTime, p + i, sizeof gtdTime);
	i += sizeof gtdTime;
	return (i + clientExtensions.SetBinData(p + i, cnt - i));
}

ClientPrice::ClientPrice() :
	bids(8), asks(8)
{
	status = 0;
	btradeable = false;
	closeoutBid = closeoutAsk = 0.0; // цены, по которым можно закрываться
#ifdef LINER_PROJ
	m_pInstrShrd = nullptr;
#endif
}

void ClientPrice::Init()
{	/* The status of the Price.
	 * Deprecated: Will be removed in a future API update. deprecated), */
	status = 0;
	// The string “PRICE”. Used to identify the a Price object when found in a stream. default=PRICE),
	type.SetCountZeroND();
	// The Price’s Instrument.
	instrument.SetCountZeroND();
	// The date/time when the Price was created
	time.Init();
	// Flag indicating if the Price is tradeable or not
	btradeable = false;
	/* The list of prices and liquidity available on the Instrument’s bid side. It is possible
	 * for this list to be empty if there is no bid liquidity currently available for the Instrument in the Account. */
	bids.SetCountZeroND();
	/* The list of prices and liquidity available on the Instrument’s ask side. It is possible for this list to be
	 * empty if there is no ask liquidity currently available for the Instrument in the Account. */
	asks.SetCountZeroND();
	/* The closeout bid Price. This Price is used when a bid is required to closeout a Position (margin
	 * closeout or manual) yet there is no bid liquidity. The closeout bid is never used to open a new position. */
	closeoutBid = 0.0;
	/* The closeout ask Price. This Price is used when a ask is required to closeout a Position (margin
	 * closeout or manual) yet there is no ask liquidity. The closeout ask is never used to open a new position. */
	closeoutAsk = 0.0;
	/* The factors used to convert quantities of this price’s Instrument’s quote currency into
	 * a quantity of the Account’s home currency. When the includeHomeConversions is present in
	 * the pricing request (regardless of its value), this field will not be present.
	 * Deprecated: Will be removed in a future API update. */
	quoteHomeConversionFactors.Init();
	/* Representation of how many units of an Instrument are available to be traded by an Order
	 * depending on its positionFill option.
	 * Deprecated: Will be removed in a future API update. deprecated) */
	unitsAvailable.Init();
}

} // namespace oatp
