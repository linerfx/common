#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgIsc.h — часть программы iTogClient и iTog. iTogClient и iTog
 * свободные программы, вы можете перераспространять их и/или изменять на
 * условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient и iTog распространяются в надежде, что будут полезными, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgIsc.h is part of iTogClient and iTog. These programms are free software:
 * you can redistribute them and/or modify them under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * iTogClient and iTog are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
//#include <cfloat>
#include <cstdint>
//#include <ctime>
#include "defs.h"
#include "FxString.h"
#include "io_string.h"	// fl::CString
#include "iTgCafeObj.h"
#include "iTgOpts.h"
#include "iTgStoreObj.h"
#ifdef __ITOG
#include "FxArray.h"
#elif defined __ITGCLNT
#include <QImage>
#endif// defined __ITOG/__ITGCLNT
//#include "MeasureUnit.h"
#include "FxVArray.h"
#include "iTgDefs.h"

namespace isc {// inter-server-client objects' data exchange

/*struct CMu {
	store::CMu mu;
	int32_t id;						// ид-р е.и. в CiTgOpts::m_amu
	glb::CFxString snm, ssnm;
	CMu() : id(INV32_BIND) { }
	CMu(const CMu &o) : mu(o.mu), id(o.id), snm(o.snm), ssnm(o.ssnm) { }
	CMu(CMu &&o) : mu(o.mu), id(o.id), snm(o.snm), ssnm(o.ssnm) { }
	int32_t SetBinData(CBYTE *cpb, int32_t cnt);
	int32_t GetBinData(BYTES &ab) const;
	const CMu& operator =(const CMu &o);
	CMu& operator =(CMu &&o);
	bool IsValid() const;
#ifdef __ITOG
	CMu& Set(int32_t iMu);
#endif
};

inline const CMu& CMu::operator =(const CMu &o)
{
	mu = o.mu;
	id = o.id;
	snm = o.snm;
	ssnm = o.ssnm;
	return *this;
}

inline CMu& CMu::operator =(CMu &&o)
{
	mu = o.mu;
	id = o.id;
	snm.Attach(o.snm);
	ssnm.Attach(o.ssnm);
	return *this;
}

inline bool CMu::IsValid() const
{
	return (id != INV32_BIND && mu.IsValid());
}*/


#define SZ_BASITM (sizeof(int64_t) * 2 + sizeof(int32_t) * 6 + sizeof(fl::CBaseObj::Type))
//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CBaseItem class базовый сласс для isc::CDept, isc::CIngredient, isc::CProduct,
/// isc::CStaple, isc::CContractor, isc::CSupplier, isc::CMenue isc::CBaseItemComplex.
///
class CBaseItem
{
public:
	typedef int(*fcmpp)(const isc::CBaseItem *const &a, const isc::CBaseItem *const &z);
	typedef int(*fcmp)(const CBaseItem&, const CBaseItem&);

public: // tmEdit должен быть первым в классе т.к. его адрес использую при копировании в объект байт-массива
		// не только в этом классе, см. CBaseItemComplex::SetBinData()
	int64_t tmEdit,	// время изменения
		tmCrtd;		// время создания
	uint32_t flgs;	// флаги ITM_* как ITM_EDIT или ITM_DEL
	int32_t iObj,	// базовый ид-р объекта
		iPrnt,		// базовый ид-р патриарха, такого как меню для продукта или отдел для подотдела, продукт для ингр-та
		iNm,		// базовый ид-р строки с наименованием эл-та
		iDn,		// базовый ид-р строки - имени файла с описанием
		iImg;		// базовый ид-р строки с именем файла рисунка
	fl::CBaseObj::Type
		tp;			// тип файлового объекта
	fl::CString sNm;// наименование эл-та (ид-р iNm)
	glb::CFxString
		sDn;		// описание эл-та (ид-р имени файла в дирректории SI_TXT iDn)
	BYTES img;		// рисунок эл-та для передачи клиенту по сети (ид-р имени файла в папке SI_PICTD iImg)
	CBaseItem();
	CBaseItem(fl::CBaseObj::Type t);
	CBaseItem(const CBaseItem &o);
	CBaseItem(CBaseItem &&o);
	virtual ~CBaseItem() {}
	// для перемещения
	virtual int32_t GetBinData(BYTES &ab) const;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt);
	virtual bool IsValidToSave() const;
	virtual uint32_t NmFlags() const;
	virtual const glb::CFxArray<CBaseItem*, true>& GetSubitems() const;
	virtual glb::CFxArray<CBaseItem*, true>& GetSubitems();
	virtual void SetType(fl::CBaseObj::Type t);
	// полиморфизьм
	static CBaseItem* Duplicate(const CBaseItem *pbi);
	static CBaseItem* Create(fl::CBaseObj::Type tp);
	bool TypeEqual(const CBaseItem &itm) const;
	bool TypeValid() const;
#ifdef __ITOG
	// только для сервера: читаю базу
	virtual bool SetToSend(uint32_t szImg, uint32_t uSbimg = 0, bool bVisible = true);
	// запись эл-та в базу
	virtual int32_t SaveToBase(int16_t iind, fl::CFileItem &fi);
#elif defined __ITGCLNT
	void Strings(QString &qsnm, QString &qsdn);
	bool Image(QImage &qimg);
#endif
	virtual const CBaseItem& operator =(const CBaseItem &o);
	virtual CBaseItem& operator =(CBaseItem &&o);
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const;
	virtual isc::CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl);
	void Set(int64_t tmc, int64_t tme, uint32_t flags, int32_t iItm, int32_t iParent, int32_t iNm,
			 int32_t iDn, int32_t iImage, fl::CBaseObj::Type tp);
	void Get(int64_t &tmc, int64_t &tme, uint32_t &flags, int32_t &iItm, int32_t &iParent, int32_t &iName,
			 int32_t &iDn, int32_t &iImage, fl::CBaseObj::Type &tp);
	bool IsNull() const;
	// ф-ции сравнения (типа fcmpp см. выше) эл-тов индексированного массива указателей isc::CBaseItem
	static int cmp_tpnm(const CBaseItem *const &a, const CBaseItem *const &z);
	static int cmp_nmi(const CBaseItem *const &a, const CBaseItem *const &z);
	static int cmp_tpid(const CBaseItem *const &a, const CBaseItem *const &z);
};

inline CBaseItem::CBaseItem()
	: tmEdit(0)
	, tmCrtd(0)
	, flgs(0)
	, tp(fl::CBaseObj::ndef)
{
	iObj = iPrnt = iNm = iDn = iImg = INV32_BIND;
}

inline CBaseItem::CBaseItem(fl::CBaseObj::Type t)
	: tp(t)
{
	tmEdit = tmCrtd = 0;
	flgs = 0;
	iObj = iPrnt = iNm = iDn = iImg = INV32_BIND;
	SetType(t);
}

inline CBaseItem::CBaseItem(const CBaseItem &o)
	: sNm(o.sNm)
	, sDn(o.sDn)
	, img(o.img)
{
	memcpy((void*) &tmEdit, &o.tmEdit, SZ_BASITM); // debug: в GCC this != &tmEdit
}

inline CBaseItem::CBaseItem(CBaseItem &&o)
	: sNm(o.sNm)
	, sDn(o.sDn)
	, img(o.img)
{
	memcpy((void*) &tmEdit, &o.tmEdit, SZ_BASITM);
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::IsValidToSave годится ли объект для сохранения его в базе?
/// \return трю/хрю
///
inline bool CBaseItem::IsValidToSave() const
{	// достаточно одного наименования
	return (sNm.m_str.GetCount() > 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::NmFlags флаги для наименования, такие как ITM_STPL, ITM_DEPT, ITM_MENUE и т.д.
/// \return флаги файлового объекта-строки с наименованием этого объекта
///
inline uint32_t CBaseItem::NmFlags() const
{
	switch (tp) {
	case fl::CBaseObj::menue:
	case fl::CBaseObj::submenue:
		return ITM_MENUE;
	case fl::CBaseObj::dept:
	case fl::CBaseObj::subdept:
		return ITM_DEPT;
	case fl::CBaseObj::staple:
		return ITM_STPL;
	case fl::CBaseObj::product:
		return ITM_PRDCT;
	default:
		return 0;
	}
}

inline const glb::CFxArray<CBaseItem*, true>& CBaseItem::GetSubitems() const
{
	ASSERT(false); // только для производного
}

inline glb::CFxArray<CBaseItem*, true>& CBaseItem::GetSubitems()
{
	ASSERT(false); // только для производного
}

inline bool CBaseItem::TypeEqual(const CBaseItem &bi) const
{
	return (tp == bi.tp && (tp != fl::CBaseObj::ingrdt || (flgs & ITM_MNMS) == (bi.flgs & ITM_MNMS)));
}

inline bool CBaseItem::TypeValid() const
{
	return (tp >= 0 && tp < fl::CBaseObj::types);
}

#ifdef __ITGCLNT

inline void CBaseItem::Strings(QString &qsnm, QString &qsdn)
{
	qsnm = QByteArray::fromRawData((CCHAR*) (CBYTE*) sNm.m_str, sNm.m_str.GetCount());
	qsdn = QByteArray::fromRawData((CCHAR*) sDn, sDn.GetCount());
}

inline bool CBaseItem::Image(QImage &qimg)
{
	return qimg.loadFromData(QByteArray::fromRawData((CCHAR*) (CBYTE*) img, img.GetCount()));
}

#endif

inline const CBaseItem& CBaseItem::operator =(const CBaseItem &o)
{
	memcpy((void*) &tmEdit, &o.tmEdit, SZ_BASITM);
	sNm = o.sNm;
	sDn = o.sDn;
	img = o.img;
	return *this;
}

inline CBaseItem& CBaseItem::operator =(CBaseItem &&o)
{
	memcpy((void*) &tmEdit, &o.tmEdit, SZ_BASITM);
	sNm = o.sNm;
	sDn = o.sDn;
	img = o.img;
	return *this;
}

inline bool CBaseItem::IsNull() const
{
	return (!tmEdit && !tmCrtd && !flgs &&  tp == fl::CBaseObj::ndef &&
			iObj == INV32_BIND && iPrnt == INV32_BIND && iNm == INV32_BIND &&
			iDn == INV32_BIND && iImg == INV32_BIND && !sNm.m_str.GetCount() && !sDn.GetCount() && !img.GetCount());
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CBaseItemComplex class базовый класс для объектов с подэлементами
/// Наследуют: isc::CDept, isc::CMenue, isc::CProduct
///
class CBaseItemComplex : public CBaseItem
{
public:/* это медленно, но альтернативы массиву указателей нет, т.к. клиенту могут потребоваться сведения
		* от подэлемента как CProduct::Cost(). Для меню это подменю (isc::CMenue), для подменю - продукты
		* (isc::CProduct), для отдела - подотделы или меню, для продукта - ингредиенты isc::CIngredient */
	glb::CFxArray<CBaseItem*, true> apSbitms;
public:
	CBaseItemComplex();
	CBaseItemComplex(fl::CBaseObj::Type t, int32_t iID = INV32_BIND);
	CBaseItemComplex(const CBaseItemComplex &o);
	// если оставить move constructor, не будет работать оператор присваивания. Такая ошибка в CExpend::SetLayout():
	// Object of type 'CBaseItemComplex' cannot be assigned because its copy assignment operator is implicitly deleted:
	// copy assignment operator is implicitly deleted because 'CBaseItemComplex' has a user-declared move constructor
//	CBaseItemComplex(CBaseItemComplex &&o);
	virtual ~CBaseItemComplex();
	const CBaseItemComplex& operator =(const CBaseItemComplex &d);
	CBaseItemComplex& operator =(CBaseItemComplex &&d);
	virtual int32_t GetBinData(BYTES &ab) const override;
	virtual int32_t SetBinData(CBYTE *cpb, cint32_t cnt) override;
	virtual const glb::CFxArray<CBaseItem*, true>& GetSubitems() const override;
	virtual glb::CFxArray<CBaseItem*, true>& GetSubitems() override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	void SetObjID(int32_t iID);
#ifdef __ITOG
	virtual void SetToSend(fl::CBaseObj &bo, int32_t iID, uint32_t uImg, uint32_t uSbimg = 0);
	virtual bool SetToSend(uint32_t uImg, uint32_t uSbimg = 0, bool bVisible = true) override;
	virtual int32_t SaveToBase(int16_t iind, fl::CFileItem &fi) override;
#endif
};

inline CBaseItemComplex::CBaseItemComplex()
	: CBaseItem(fl::CBaseObj::ndef)
{

}

inline CBaseItemComplex::CBaseItemComplex(fl::CBaseObj::Type t, int32_t iID)
	: CBaseItem(t)
{	// возможные объекты с подэлементами
	ASSERT(t == fl::CBaseObj::menue || t == fl::CBaseObj::submenue ||
		   t == fl::CBaseObj::dept || t == fl::CBaseObj::subdept || fl::CBaseObj::product);
	iObj = iID;
}

inline CBaseItemComplex::CBaseItemComplex(const CBaseItemComplex &o)
	: CBaseItem(o)
{
	for (int i = 0, n = o.apSbitms.GetCount(); i < n; i++)
		apSbitms.Add(Duplicate(o.apSbitms[i]));
}

/*inline CBaseItemComplex::CBaseItemComplex(CBaseItemComplex &&o)
	: CBaseItem(o)
{
	apSbitms.Attach(o.apSbitms);
}*/

inline CBaseItemComplex::~CBaseItemComplex()
{
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++)
		delete apSbitms[i];
	apSbitms.SetCountZeroND();
}

inline const glb::CFxArray<CBaseItem*, true> &CBaseItemComplex::GetSubitems() const
{
	return apSbitms;
}

inline glb::CFxArray<CBaseItem*, true> &CBaseItemComplex::GetSubitems()
{
	return apSbitms;
}

inline void CBaseItemComplex::SetObjID(int32_t iID)
{
	iObj = iID;
}

inline CBaseItemComplex& CBaseItemComplex::operator =(CBaseItemComplex &&o)
{
	CBaseItem::operator =(o);
	apSbitms.Attach(o.apSbitms);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CMenue class - меню с подменю или подменю с продуктами
///
class CMenue : public CBaseItemComplex
{
public:
	typedef CBaseItemComplex BAS;
	CMenue();
	CMenue(fl::CBaseObj::Type t, int32_t iMenue = INV32_BIND);
	CMenue(const CMenue &o);
	CMenue(CMenue &&o);
	virtual ~CMenue() {}
	virtual const CMenue& operator =(const CMenue &d);
	CMenue& operator =(CMenue &&d);
};

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CMenue::CMenue
/// \param t
/// \param iMenue: базовый ид-р по индексу fl::ioffset
///
inline CMenue::CMenue()
	: BAS(fl::CBaseObj::menue)
{

}

inline CMenue::CMenue(fl::CBaseObj::Type t, int32_t iObjID) // iObjID = INV32_BIND
	: CBaseItemComplex(t, iObjID)
{
	ASSERT(t == fl::CBaseObj::menue || t == fl::CBaseObj::submenue);
}

inline CMenue::CMenue(const CMenue &o)
	: CBaseItemComplex(o)
{

}

inline CMenue::CMenue(CMenue &&o)
	: CBaseItemComplex(o)
{

}

inline const CMenue& CMenue::operator =(const CMenue &d)
{
	CBaseItemComplex::operator =(d);
	return *this;
}

inline CMenue& CMenue::operator =(CMenue &&d)
{
	CBaseItemComplex::operator =(d);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CDept class отдел или подотдел c подотделами или меню
///
class CDept : public CBaseItemComplex
{
public:
	typedef CBaseItemComplex BAS;
	o::CiTgOpts::fCmpUCat afcmp_cats[1] = { o::CiTgOpts::CCat::cmpc };
	o::CiTgOpts::AUCATS cats;
	////////////////////////
	CDept();
	CDept(fl::CBaseObj::Type t, int32_t iDept = INV32_BIND);
	CDept(const CDept &o);
	CDept(CDept &&o);
	virtual ~CDept() {}
	const CDept& operator =(const CDept &d);
	CDept& operator =(CDept &&d);
	virtual int32_t GetBinData(BYTES &ab) const override;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
#ifdef __ITOG
	virtual void SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg = 0) override;
#endif
};

inline CDept::CDept()
	: BAS(fl::CBaseObj::dept)
	, cats(16, afcmp_cats)
{

}

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CDept::CDept
/// \param t: может быть fl::CBaseObj::dept или fl::CBaseObj::subdept
/// \param iDeptID
///
inline CDept::CDept(fl::CBaseObj::Type t, int32_t iDeptID) // iDept = INV32_BIND
	: CBaseItemComplex(t, iDeptID)
	, cats(16, afcmp_cats)
{
	ASSERT(t == fl::CBaseObj::dept || t == fl::CBaseObj::subdept);
}

inline CDept::CDept(const CDept &o)
	: CBaseItemComplex(o)
	, cats(o.cats)
{

}

inline CDept::CDept(CDept &&o)
	: CBaseItemComplex(o)
	, cats(o.cats)
{

}

inline int32_t CDept::GetBinData(BYTES &ab) const
{
	ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
	int32_t n = CBaseItemComplex::GetBinData(ab);	// 1. объект базового класса
	return n + cats.GetBinData(ab);					// 2. категории товаров
}

class CStaple;	// для ингредиента
class CProduct;	// для ингредиента

#define SZ_ISCINGT (sizeof(double) + sizeof(float) * 4 + sizeof(int32_t) * 4 + sizeof(store::Cid))

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIngredient class ингредиент продукта isc::CProduct на основе файлового store::CIngredient
///
class CIngredient : public CBaseItem
{
public:
	double fLd;		// % наценки
	float fQtt,		// к-во в е.и. 'mu' или штук, если товар/продукт штучный
		fPrc,		// цена товара/прод. за е.и. см. Стоимость ингредиента будет произведением fPrc * fQtt
		fLs,		// % потерь
		fTara;		// объём тары штучного товара или "выход" продукта store::CProduct::Out()
	int32_t nCal,	// калорий на 100 гр.
		iMu,		// е.и. товара или продукта см. store::CIngredient::m_iMu
		iSource,	// ид-р товара или продукта
		nRes;		// выравнивание до 64 б.
	store::Cid
		idBas;		// ид-р основного ингр-та, если этот альтернативный (есть фалаг ITM_INGTALT)
	CIngredient(int32_t iIngID = INV32_BIND);
	CIngredient(const CIngredient &o);
	CIngredient(CIngredient &&o);
	virtual ~CIngredient() {}
	int32_t GetBinData(BYTES &ab) const override;
	int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	double Weight() const;
	double Cost() const; // себестоимость
	double Price() const;// цена с наценкой
	uint32_t Type() const;
	const CIngredient& operator =(const CIngredient &o);
	CIngredient& operator =(CIngredient &&o);
	const CIngredient& operator =(const CStaple &s);
	const CIngredient& operator =(const CProduct &s);
	// индексы
	typedef int (*fcmp)(const CIngredient&, const CIngredient&);
	static int cmp_nmi(const CIngredient&, const CIngredient&);
	static int cmp_flgs(const isc::CIngredient&, const isc::CIngredient&);
	static int cmp_flgsnmi(const isc::CIngredient&, const isc::CIngredient&);
	static int cmp_tpnmi(const isc::CIngredient&, const isc::CIngredient&);
	static int cmp_tpsrc(const isc::CIngredient&, const isc::CIngredient&);
	// вспомогательные ф-ции сравнения типов ингр-тов. Используются в индексах
	fl::CBaseObj::Type SrcType() const;
	bool IsValidType() const;
	bool IsStaple() const;
	bool IsProduct() const;
	bool IsAlt() const;
	bool IsAdd() const;
	bool IsSel() const;
	bool IsBas() const;
	bool IsRoot() const;
	bool IsValid() const;
#ifdef __ITOG
	virtual bool SetToSend(uint32_t uImg, uint32_t nSbimg = 0, bool bVisible = true) override;
	virtual int32_t SaveToBase(int16_t iind, fl::CFileItem &fi) override;
#endif
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &o) const override;
};

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::CIngredient
/// \param iIngID: базовый ид-р по индексу fl::ioffset
///
inline CIngredient::CIngredient(int32_t iIngID) // iIngID = INV32_BIND
	: CBaseItem(fl::CBaseObj::ingrdt)
	, fLd(.0)		// % наценки
	, fQtt(0.f)		// к-во в е.и. 'mu' или штук, если товар/продукт штучный
	, fPrc(0.f)		// цена товара/прод. за е.и. см. Стоимость ингредиента будет произведением fPrc * fQtt
	, fLs(0.f)		// % потерь
	, fTara(0.f)	// объём тары штучного товара или "выход" продукта store::CProduct::Out()
	, nCal(0)		// калорий на 100 гр.
	, iMu(INV32_BIND)		// е.и. товара или продукта см. store::CIngredient::m_iMu
	, iSource(INV32_BIND)// ид-р товара или продукта
	, nRes(0)			// выравнивание до 64 б.
{
	fQtt = fPrc = fLs = fTara = 0.f;
	iObj = iIngID;
}

inline CIngredient::CIngredient(const CIngredient &o)
	: CBaseItem(o)
	, fLd(o.fLd)		// % наценки
	, fQtt(o.fQtt)		// к-во в е.и. 'mu' или штук, если товар/продукт штучный
	, fPrc(o.fPrc)		// цена товара/прод. за е.и. см. Стоимость ингредиента будет произведением fPrc * fQtt
	, fLs(o.fLs)		// % потерь
	, fTara(o.fTara)	// объём тары штучного товара или "выход" продукта store::CProduct::Out()
	, nCal(o.nCal)		// калорий на 100 гр.
	, iMu(o.iMu)		// е.и. товара или продукта см. store::CIngredient::m_iMu
	, iSource(o.iSource)// ид-р товара или продукта
	, nRes(0)			// выравнивание до 64 б.
	, idBas(o.idBas)	// ид-р основного ингр-та, если этот альтернативный - фалаг ITM_INGTALT
{

}

inline CIngredient::CIngredient(CIngredient &&o)
	: CBaseItem(o)
	, fLd(o.fLd)		// % наценки
	, fQtt(o.fQtt)		// к-во в е.и. 'mu' или штук, если товар/продукт штучный
	, fPrc(o.fPrc)		// цена товара/прод. за е.и. см. Стоимость ингредиента будет произведением fPrc * fQtt
	, fLs(o.fLs)		// % потерь
	, fTara(o.fTara)	// объём тары штучного товара или "выход" продукта store::CProduct::Out()
	, nCal(o.nCal)		// калорий на 100 гр.
	, iMu(o.iMu)		// е.и. товара или продукта см. store::CIngredient::m_iMu
	, iSource(o.iSource)// ид-р товара или продукта
	, nRes(0)			// выравнивание до 64 б.
	, idBas(o.idBas)	// ид-р основного ингр-та, если этот альтернативный - фалаг ITM_INGTALT
{

}

inline double CIngredient::Cost() const
{
	return ((double) fQtt * fPrc);
}

inline double CIngredient::Price() const
{
	double f = (double) fQtt * fPrc;
	return (f + f * fLd);
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::Type ингредиенты двух типов: продукт и товар
/// \return тип ингредиента (может быть нуль, если не определено)
///
inline uint32_t CIngredient::Type() const
{
	ASSERT((flgs & ITM_MNMS) == (sNm.m_flags & ITM_MNMS));
	return (sNm.m_flags & ITM_MNMS);
}

inline const CIngredient& CIngredient::operator =(const CIngredient &o)
{
	CBaseItem::operator =(o);
	memcpy(&fLd, &o.fLd, SZ_ISCINGT);
	return *this;
}

inline CIngredient& CIngredient::operator =(CIngredient &&o)
{
	CBaseItem::operator =(o);
	memcpy(&fLd, &o.fLd, SZ_ISCINGT);
	return *this;
}

inline int CIngredient::cmp_nmi(const CIngredient &a, const CIngredient &z)
{
	int32_t na, nz;
	CBYTE *psza = a.sNm.m_str.data(na), *pszz = z.sNm.m_str.data(nz);
	return glb::cmp_iUtf8s((CCHAR*) psza, na, (CCHAR*) pszz, nz);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmp_tpnm сортирую ингредиенты по типу и наименованию
/// \return -1|0|1
///
inline int CIngredient::cmp_flgsnmi(const isc::CIngredient &a, const isc::CIngredient &z)
{
	int i = cmp_flgs(a, z);
	return ((i != 0) ? i : cmp_nmi(a, z));
}

inline int CIngredient::cmp_tpnmi(const CIngredient &a, const CIngredient &z)
{
	ASSERT(a.IsValidType() && z.IsValidType());
	return (a.flgs & ITM_STPL && z.flgs & ITM_PRDCT) ? 1 :	// a товар, z продукт
		   (a.flgs & ITM_PRDCT && z.flgs & ITM_STPL) ? -1 :	// a продукт, z товар
											cmp_nmi(a, z);	// a товар, z товар или а продукт, z продукт

}

inline int CIngredient::cmp_tpsrc(const CIngredient &a, const CIngredient &z)
{
	ASSERT(a.IsValidType() && z.IsValidType());
	return (a.flgs & ITM_STPL && z.flgs & ITM_PRDCT) ? 1 :		// a товар, z продукт (товар "больше" продукта)
		   (a.flgs & ITM_PRDCT && z.flgs & ITM_STPL) ? -1 :		// a продукт, z товар
		   (a.iSource > z.iSource) ? 1 : (a.iSource < z.iSource) ? -1 : 0;	// a товар, z товар или а продукт, z продукт
}

inline fl::CBaseObj::Type CIngredient::SrcType() const
{
	ASSERT(IsValidType());
	return ((flgs & ITM_STPL) ? fl::CBaseObj::staple : fl::CBaseObj::product);
}

inline bool CIngredient::IsValidType() const
{
	return ((flgs & (ITM_STPL|ITM_PRDCT)) && (flgs & (ITM_STPL|ITM_PRDCT)) != (ITM_STPL|ITM_PRDCT));
}

inline bool CIngredient::IsStaple() const
{
	ASSERT(IsValidType());
	return ((flgs & ITM_STPL) != 0);
}

inline bool CIngredient::IsProduct() const
{
	ASSERT(IsValidType());
	return ((flgs & ITM_PRDCT) != 0);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::IsAlt "альтернативный" назначается во время создания/изменения продукта
/// уполномоченным лицом вне основного состава ингредиентов. Если покупатель включил его в ос-
/// новной состав ингредиентов, эта ф-ция всё равно вернёт "трю" (альтернативный). Кроме того,
/// альтернативным ингр-т становится и базовый, если п-ль выбирает альт-ингр-т вместо базового,
/// последний становится альтернативным, но с флагом ITM_INGTREP.
/// \return трю/хрю
///
inline bool CIngredient::IsAlt() const
{
	ASSERT((flgs & (ITM_INGTALT|ITM_INGTADD)) != (ITM_INGTALT|ITM_INGTADD));
	ASSERT(!(flgs & ITM_INGTALT) || idBas.Valid());
	return ((flgs & ITM_INGTALT) != 0);// && (flgs & ITM_INGTSEL) == 0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::IsAdd "дополнительный" ингр-т указывается во время создания/изменения продукта
/// уполномоченным лицом вне списка основных ингр-тов. Может быть добавлен покупателем в основной
/// состав. В таком случае всё равно будет дополнительным, эта ф-ция вернёт "трю".
/// \return трю/хрю
///
inline bool CIngredient::IsAdd() const
{
	ASSERT((flgs & (ITM_INGTALT|ITM_INGTADD)) != (ITM_INGTALT|ITM_INGTADD));
	return ((flgs & ITM_INGTADD) != 0);// && (flgs & ITM_INGTSEL) == 0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::IsSel "выбранный" ингр-т дополнительный или альтернативный, добавленный п-лем
/// в основной состав продукта.
/// \return трю/хрю
///
inline bool CIngredient::IsSel() const
{
	ASSERT(!(flgs & ITM_INGTSEL) || flgs & (ITM_INGTADD|ITM_INGTALT));
	return ((flgs & ITM_INGTSEL) != 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::IsBas "базовый" ингр-т в основном составе. Не может быть дополнительным или
/// альтернативным. Доп/альт ингр-т в основном составе имеет флаг ITM_INGTSEL.
/// \return трю/хрю
///
inline bool CIngredient::IsBas() const
{
	ASSERT((flgs & (ITM_INGTALT|ITM_INGTADD)) != (ITM_INGTALT|ITM_INGTADD));
	return !(flgs & (ITM_INGTALT|ITM_INGTADD));
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::IsRoot - root значит основной ингредиент, замещённый альтернативным.
/// У такого инр-та тоже статус ITM_INGTALT + ITM_INGTREP
///
inline bool CIngredient::IsRoot() const
{
	ASSERT(!(flgs & ITM_INGTREP) || IsBas());
	return ((flgs & ITM_INGTREP) != 0);
}

inline bool CIngredient::IsValid() const
{
	return (IsBas() || IsAlt() || IsAdd() || IsSel() || IsRoot());
}

#define SZ_ISCPRDCT	(sizeof(double) + sizeof(float) + sizeof(int))
/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CProduct class продукт с ингредиентами isc::CIngredient в apSbitms для передачи
/// между клиентом и сервером
///
class CProduct : public CBaseItemComplex
{
public:
	typedef CBaseItemComplex BAS;
	double fLd;	// % наценки store::CProduct::m_fLd
	float fRnd;	// округлить до...
	int iMu;	// индекс е.и. o::CiTgOpts::m_amu из/для store::CProduct::m_iMu
	CProduct(int32_t iPrdct = INV32_BIND);
	CProduct(const CProduct &o);
	CProduct(CProduct &&o);
	virtual ~CProduct() {}
	int32_t GetBinData(BYTES &ab) const override;
	int32_t SetBinData(CBYTE *cpb, const int32_t cnt) override;
	const CProduct& operator =(const CProduct &o);
	CProduct& operator =(CProduct &&o);
#ifdef __ITOG
	virtual void SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg = 0) override;
#endif
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;

	void SetProductID(int32_t iPrdctID);

	double Cost() const; // себестоимость
	double Price() const;// с наценкой
	int Cal() const;	 // калорий в продукте
	int Cal100() const;	 // калорий на 100 грамм продукта
	double Out() const;	 // выход в е.и. продукта
	double Load() const; // наценка

	CIngredient* FindIngredient(uint32_t uf, CBYTES &sNm);
//	int AddIngredient(const CBaseItem *ping);

	// ф-ции сравнения (типа CBaseItem::fcmpp) эл-тов индексированного массива указателей isc::CBaseItem
	static int cmp_prc(const CBaseItem *const&, const CBaseItem *const&);
};

inline CProduct::CProduct(const CProduct &o)
	: CBaseItemComplex(o)
	, fLd(o.fLd)
	, fRnd(o.fRnd)
	, iMu(o.iMu)
{

}

inline CProduct::CProduct(CProduct &&o)
	: CBaseItemComplex(o)
	, fLd(o.fLd)
	, fRnd(o.fRnd)
	, iMu(o.iMu)
{

}

/*inline CProduct::CAddAlt::CAddAlt(const CAddAlt &o)
	: idAddAlt(o.idAddAlt)
	, pp(nullptr)
{

}

inline CProduct::CAddAlt::CAddAlt(const store::CProduct::CAddAlt &id, const isc::CProduct &p)
	: idAddAlt(id)
	, pp(nullptr)
{
	pp = CAST(CBaseItem, CProduct, CBaseItem::Duplicate(&p));
	ASSERT(pp);
}

inline const CProduct::CAddAlt& CProduct::CAddAlt::operator =(const CAddAlt &o)
{
	idAddAlt = o.idAddAlt;
	if (pp && pp->tp == o.pp->tp)
		*pp = reinterpret_cast<const CBaseItem&>(*o.pp); // down_cast
	else
		pp = o.pp ? CAST(CBaseItem, CProduct, CBaseItem::Duplicate(o.pp)) : nullptr;
	return *this;
}

inline int32_t CProduct::CAddAlt::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	ASSERT(pp);// иначе нет смысла, массив доп.эл-тов продукта должен быть пустым
	int32_t i = glb::read_raw((BYTE*) &idAddAlt, sizeof idAddAlt, cpb, cnt);
	return (i + pp->SetBinData(cpb + i, cnt - i));
}

inline int32_t CProduct::CAddAlt::GetBinData(BYTES &ab)
{
	ASSERT(pp);// иначе нет смысла, массив доп.эл-тов продукта должен быть пустым
	ab.AddND((CBYTE*) &idAddAlt, sizeof idAddAlt);
	return (sizeof idAddAlt + pp->GetBinData(ab));
}*/

///////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::SetProductID
/// \param iPrdctID: базовый ид-р по индексу fl::ioffset
///
inline void CProduct::SetProductID(int32_t iPrdctID)
{
	ASSERT(iPrdctID >= 0 && tp == fl::CBaseObj::product);
	iObj = iPrdctID;
}

#define SZ_ISCPRDCTQTT	sizeof(double)
/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CPrdctQtt class продукт с к-вом для счёта CBill. Используется только в
/// представлении iTogClnt
///
class CPrdctQtt : public CProduct
{
public:
	typedef CProduct BAS;

	double m_fQtt;

	CPrdctQtt(int32_t iPrdct = INV32_BIND);
	CPrdctQtt(const CPrdctQtt &o);
	CPrdctQtt(CPrdctQtt &&o);
	virtual ~CPrdctQtt() {}

	int32_t GetBinData(BYTES &ab) const override;
	int32_t SetBinData(CBYTE *cpb, cint32_t cnt) override;

	const CPrdctQtt& operator =(const CPrdctQtt &o);
	CPrdctQtt& operator =(CPrdctQtt &&o);

#ifdef __ITOG
	virtual void SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg = 0) override;
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
#endif
};

inline CPrdctQtt::CPrdctQtt(int32_t iPrdct)
	: CProduct(iPrdct)
	, m_fQtt(.0)
{

}

inline CPrdctQtt::CPrdctQtt(const CPrdctQtt &o)
	: CProduct(o)
	, m_fQtt(o.m_fQtt)
{

}

inline CPrdctQtt::CPrdctQtt(CPrdctQtt &&o)
	: CProduct()
	, m_fQtt(o.m_fQtt)
{

}

inline int32_t CPrdctQtt::GetBinData(BYTES &ab) const
{
	int32_t n = BAS::GetBinData(ab);
	ab.AddND((CBYTE*) &m_fQtt, SZ_ISCPRDCTQTT);
	return (n + SZ_ISCPRDCTQTT);
}

inline int32_t CPrdctQtt::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_tp(m_fQtt, cpb + i, cnt - i));
}

inline const CPrdctQtt& CPrdctQtt::operator =(const CPrdctQtt &o)
{
	BAS::operator =(o);
	m_fQtt = o.m_fQtt;
	return *this;
}

inline CPrdctQtt& CPrdctQtt::operator =(CPrdctQtt &&o)
{
	BAS::operator =(o);
	m_fQtt = o.m_fQtt;
	return *this;
}

#ifdef __ITOG

inline void CPrdctQtt::SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg)
{
	BAS::SetToSend(fi, iID, uImg, uSbimg);
	store::CPrdctQtt *pp = CAST(fl::CBaseObj, store::CPrdctQtt, &fi);
	ASSERT(nullptr != pp);
	m_fQtt = pp->m_fQtt;
}

inline CBaseItem& CPrdctQtt::Set(int32_t iItm, const fl::CFileItem &fl)
{
	BAS::Set(iItm, fl);
	const store::CPrdctQtt *pp = CAST(const fl::CFileItem, const store::CPrdctQtt, &fl);
	ASSERT(nullptr != pp);
	m_fQtt = pp->m_fQtt;
	return *this;
}

inline fl::CFileItem& CPrdctQtt::Get(int32_t &iItm, fl::CFileItem &fl) const
{
	BAS::Get(iItm, fl);
	store::CPrdctQtt *pp = CAST(fl::CFileItem, store::CPrdctQtt, &fl);
	ASSERT(nullptr != pp);
	pp->m_fQtt = m_fQtt;
	return fl;
}

#endif// defined __ITOG

#define SZ_ISCSTPL (sizeof(double) * 2 + sizeof(int32_t) * 4)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CStaple class - товар любой категории
///
class CStaple : public CBaseItem
{
public:// патриарх содержит время изменения, ид-ры объекта, патриарха, имени файла-описания и имени файла-рисунка
	double fTara,	// объём тары в е.и. 'mu'
		fPrc;		// цена за одну е.и. (mu). Если в граммах, за грамм; в штуках, за шт.
	int32_t iArt,	// ид-р строки с артикулом
		nCal,		// к-во калорий
		iMu;		// индекс o::CiTgOpts::m_amu
	uint32_t uCat;	// концевая категория товара из CiTgOpts::m_aCat
	fl::CString
		sArtl;		// артикул
	glb::CFxString
		sCat;		// наименование категории товара
//	CMu mu;			// расходная е.и. товара и тары
	// конструкцыя
	CStaple(int32_t iStplID = INV32_BIND);
	CStaple(const CStaple &o);
	CStaple(const CStaple &&o);
	const CStaple& operator =(const CStaple &o);
	CStaple& operator =(CStaple &&o);
	virtual int32_t GetBinData(BYTES &ab) const override;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	// ф-цыи индексов для массива CFxArrayInd<>
	typedef int(*fcmp)(const CStaple&, const CStaple&);
	static int cmp_nms(const CStaple &a, const CStaple &z);
	static int cmp_nmi(const CStaple &a, const CStaple &z);
	void SetStapleID(int32_t iStpl);
#ifdef __ITOG
	bool SetToSend(uint32_t uImg, uint32_t uSbimg = 0, bool bVisible = true) override;
	virtual int32_t SaveToBase(int16_t iind, fl::CFileItem &fi) override;
#endif
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
#ifdef __ITGCLNT
	glb::CFxString Name() const;
	glb::CFxString& Name(glb::CFxString &s) const;
#endif // defined __ITGCLNT
};

inline CStaple::CStaple(int32_t iStplID) // iStplID = INV32_BIND
	: CBaseItem(fl::CBaseObj::staple)
	, uCat(0U)
	, iMu(INV32_BIND)
	, nCal(0)
	, iArt(INV32_BIND)
{
	fTara = fPrc = 0.0;
	iObj = iStplID;
}

inline CStaple::CStaple(const CStaple &o)
	: CBaseItem(o)
	, fTara(o.fTara)
	, fPrc(o.fPrc)
	, iArt(o.iArt)
	, uCat(o.uCat)
	, nCal(o.nCal)
	, iMu(o.iMu)
	, sArtl(o.sArtl)
	, sCat(o.sCat)
{

}

inline CStaple::CStaple(const CStaple &&o)
	: CBaseItem(o)
	, fTara(o.fTara)
	, fPrc(o.fPrc)
	, iArt(o.iArt)
	, uCat(o.uCat)
	, nCal(o.nCal)
	, iMu(o.iMu)
	, sArtl(o.sArtl)
	, sCat(o.sCat)
{

}

inline void CStaple::SetStapleID(int32_t iStpl)
{
	ASSERT(iStpl >= 0 && tp == fl::CBaseObj::staple);
	iObj = iStpl;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::cmp_nms для сортировки в индексе массива CFxArrayInd<> для упорядочи-
/// вания товаров по наименованиям
/// \param a, z: товары
/// \return -1|0|1
///
inline int CStaple::cmp_nms(const CStaple &a, const CStaple &z)
{
	return a.sNm.cmp(z.sNm);
/*	if (!n)
		n = a.sSplr.cmp(z.sSplr);
	return n;*/
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::cmp_nmi сравнение наименований без учёта регистра
/// \param a, z: сравниваемые эл-ты
/// \return -1|0|1
///
inline int CStaple::cmp_nmi(const CStaple &a, const CStaple &z)
{
	return a.sNm.cmpi(z.sNm);
}

////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The isc::CPostAddress class передаю адрес клиенту для отображения/изменения
///
class CPostAddress : public cafe::CPostAdrs
{
public:
	int32_t m_iObj;
	fl::CString			// файловая строка, чтобы не грузить сервер копированием строк при сохранении в SaveToBase()
		m_sCountry,		// название страны
		m_sRegion,		// название области
		m_sToun,		// название населённого пункта
		m_sDistr,		// название района
		m_sIndex,		// почтовый индексом
		m_sStreet,		// улица
		m_sBuilding,	// номер строения
		m_sBldgNm,		// наименование строения, как общяга, ЖК...
		m_sFlat;		// номер квартиры/комнаты
	CPostAddress();
	CPostAddress(int32_t iPa, cafe::CPostAdrs &pa);
	CPostAddress(const CPostAddress&);
	CPostAddress(CPostAddress&&);
	const CPostAddress& operator =(const CPostAddress&);
	CPostAddress& operator =(CPostAddress&&);
	void Init();
	int32_t SetBinData(CBYTE *cpb, cint32_t cnt);
	int32_t GetBinData(BYTES &ab);
#ifdef __ITOG
	void SetToSend(int32_t iObj, const cafe::CPostAdrs &pa);
	int32_t SaveToBase(int16_t iind);
#endif
};

inline CPostAddress::CPostAddress()
	: m_iObj(INV32_BIND)
{

}

////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAddress::CPostAddress
/// \param iObj: базовый ид-р по индексу fl::ioffset
/// \param pa
///
inline CPostAddress::CPostAddress(int32_t iObj, cafe::CPostAdrs &pa)
	: cafe::CPostAdrs(pa)
	, m_iObj(iObj)
{

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CContractor class субъект на контракте для передачи клиенту.
/// Наследует isc::CSupplier
///
#define SZ_ISC_CONTRR (sizeof(int32_t) * 5 + sizeof(BYTE))
class CContractor : public CBaseItem
{
public:
	int32_t
		iBisID,	// Ид-р строки БИН/ИИН
		iBnkID,	// Ид-р строки БИК
		iBnkNm,	// Ид-р строки Банк
		iAcnt,	// Ид-р строки ИИК/счёт
		iAggrt;	// Ид-р имени файла с договором
	BYTE iCntr;	// тип/категория контрактника - индекс в массиве ид-ров CiTgOpts::m_aiCnts
	fl::CString	// файловая строка тяжелее на 4 б. зато не придётся грузить сервер при сохранении в SaveToBase()
		sBisID,	// БИН/ИИН
		sBnkID,	// БИК
		sBnkNm,	// Банк
		sAcnt,	// ИИК/счёт
		sAggrt;	// договор из таблицы с договорами
	glb::CFxArray<int32_t, true>
		aiAdrs,	// массив Ид-ров почтовых адресов из cafe::adrss
		aiTel,	// массив Ид-ров № телефона/мобилы
		aiRefs;	// массив Ид-ров строк со ссылками (соц.сети, сайт, эл.почта)
	glb::CvArray<fl::CString>
		asTel,	// все №телефонов (в строках)
		asRefs;	// все ссылки
	glb::CvArray<CPostAddress>
		aAdrs;	// все адреса
	CContractor(int32_t iCntrctr = INV32_BIND);
	CContractor(fl::CBaseObj::Type tp, int32_t iCntrctr = INV32_BIND);
	CContractor(const CContractor&);
	CContractor(CContractor&&);
	virtual ~CContractor() {}
	const CContractor& operator =(const CContractor&);
	CContractor& operator =(CContractor&&);
	virtual int32_t GetBinData(BYTES &ab) const override;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	void SetContractor(int32_t iCntrctr);
#ifdef __ITOG
	bool SetToSend(uint32_t szImg, uint32_t uSbimg = 0, bool bVisible = true) override;
	int32_t SaveToBase(int16_t iind, fl::CFileItem &fi) override;
//	virtual fl::CFileItem& Object();
	virtual int32_t SaveObject(int32_t iInd, int32_t iObj, fl::CFileItem &fi);
#endif
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
};

#ifdef __ITOG

/*inline fl::CFileItem& CContractor::Object()
{
	thread_local static cafe::CContractor c;
	return c;
}*/
#endif
////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CSupplier class поставщик - это контрактник с массивом категорий постав-
/// ляемых товаров и/или услуг
///
class CSupplier : public CContractor
{
public:
	glb::CFxArray<uint32_t, true> cats;
public:
	CSupplier(int32_t iSplr = INV32_BIND);
	CSupplier(const CSupplier&);
	CSupplier(CSupplier&&);
	const CSupplier& operator =(const CSupplier&);
	CSupplier& operator =(CSupplier&&);
public:
	int32_t GetBinData(BYTES &ab) const override;
	int32_t SetBinData(CBYTE *pb, cint32_t cnt) override;
	virtual CBaseItem& Set(int32_t iItm, const fl::CFileItem &fl) override;
	virtual fl::CFileItem& Get(int32_t &iItm, fl::CFileItem &fl) const override;
#ifdef __ITOG
//	virtual fl::CFileItem& Object() override;
	virtual int32_t SaveObject(int32_t iInd, int32_t iObj, fl::CFileItem &fi) override;
#endif
};

inline CSupplier::CSupplier(int32_t iSplr) // iSplr = INV32_BIND
	: CContractor(fl::CBaseObj::splr, iSplr)
	, cats(16)
{
	ASSERT(tp == fl::CBaseObj::splr);
	iObj = iSplr;
	iCntr = 0;// SI_SPLRS
}

inline CSupplier::CSupplier(const CSupplier &s)
	: CContractor(s)
	, cats(s.cats)
{

}

inline CSupplier::CSupplier(CSupplier &&s)
	: CContractor(s)
	, cats(s.cats)
{

}

inline const CSupplier& CSupplier::operator =(const CSupplier &o)
{
	CContractor::operator =(o);
/*	const CSupplier &s = CAST(const CBaseItem, const CSupplier, o);
	ASSERT(&s != (CSupplier*) nullptr);*/
	cats = o.cats;
	return *this;
}

inline CSupplier& CSupplier::operator =(CSupplier &&o)
{
	CBaseItem::operator=(o);
/*	CSupplier &s = CAST(CBaseItem, CSupplier, o);
	ASSERT(&s != (CSupplier*) nullptr);*/
	cats.Attach(o.cats);
	return *this;
}

inline int32_t CSupplier::GetBinData(BYTES &ab) const
{
	int32_t n = CContractor::GetBinData(ab);
	return (n + cats.GetBinData(ab));
}

inline int32_t CSupplier::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t n = CContractor::SetBinData(cpb, cnt);
	return (n + cats.SetBinData(cpb + n, cnt - n));
}

/*#ifdef __ITOG

inline fl::CFileItem& CSupplier::Object()
{
	thread_local static cafe::CSupplier s;
	return s;
}
#endif*/

#define SZ_ISC_INCOME (sizeof(double) + sizeof(int32_t) * 2)
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CIncome class всё необходимое для отображения прихода в представлении, состоит из store::CIncome
/// полностью и части store::CStaple
///
class CIncome : public store::CIncome
{
public:
	double fTara;	// ёмкость тары товара в е.и. товара
	int32_t iIncm,	// ид-р объекта store::CIncome
			iMu;	// индекс o::opt.m_amu
	glb::CFxString
		sCwrkr,		// имя сотрдуника, который оприходовал товар
		sSplr,		// наименование поставщика
		sRcvr,		// наименование получателя
		sStpl;		// наименование товара
//	CMu mu;			// е.и. товара дополненная строками
public:
	CIncome(int32_t iIncID = INV32_BIND);
	CIncome(const CIncome &o);
	CIncome(CIncome &&o);
	CIncome& operator =(CIncome &&o);
	const CIncome& operator =(const CIncome &o);
	virtual int32_t GetBinData(BYTES &ab) const;
	virtual int32_t SetBinData(CBYTE *pb, cint32_t cnt);
#ifdef __ITOG
	void SetToSend();
	int32_t SaveToBase(int16_t iind);
#elif defined __ITGCLNT
	glb::CFxString Name() const;
	glb::CFxString& Name(glb::CFxString &s) const;
#endif
};

#define SZ_OBJNM (sizeof(fl::CBaseObj::Type) + 2 * sizeof(int32_t))
//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CObjNm class для передачи клиенту ид-ра объекта, а так же ид-ра наименования
/// объекта для быстрого его изменения в файловом массиве строк.
///
class CObjNm {
public:
	int32_t m_iStrID, m_iObjID;	// базовые ид-ры объекта и строки с наименованием
	fl::CBaseObj::Type m_tp;	// тип объекта
	glb::CFxString m_snm;		// наименование объекта (сравнивается в индексах)
	//
	CObjNm();
	CObjNm(fl::CBaseObj::Type tp, int32_t iObj, int32_t iStr, const glb::CFxString &s);
	CObjNm(const CObjNm&);
	CObjNm(CObjNm&&);
	CObjNm& operator =(CObjNm &&o);
	const CObjNm& operator =(const CObjNm &o);
	int32_t GetBinData(BYTES &ab) const;
	int32_t SetBinData(CBYTE *pb, cint32_t cnt);
};

inline CObjNm::CObjNm()
	: m_tp(fl::CBaseObj::ndef)
{
	m_iStrID = m_iObjID = INV32_BIND;
}

inline CObjNm::CObjNm(fl::CBaseObj::Type tp, int32_t iObj, int32_t iStr, const glb::CFxString &s)
	: m_tp(tp)
	, m_iStrID(iStr)
	, m_iObjID(iObj)
	, m_snm(s)
{

}

inline CObjNm::CObjNm(const CObjNm &o)
	: m_iObjID(o.m_iObjID)
	, m_iStrID(o.m_iStrID)
	, m_tp(o.m_tp)
	, m_snm(o.m_snm)
{

}

inline CObjNm::CObjNm(CObjNm &&o)
	: m_iObjID(o.m_iObjID)
	, m_iStrID(o.m_iStrID)
	, m_tp(o.m_tp)
{
	m_snm.Attach(o.m_snm);
}

inline CObjNm& CObjNm::operator =(CObjNm &&o)
{
	m_iStrID = o.m_iStrID;
	m_iObjID = o.m_iObjID;
	m_tp = o.m_tp;
	m_snm.Attach(o.m_snm);
	return *this;
}

inline const CObjNm& CObjNm::operator =(const CObjNm &o)
{
	m_iStrID = o.m_iStrID;
	m_iObjID = o.m_iObjID;
	m_tp = o.m_tp;
	m_snm = o.m_snm;
	return *this;
}

}// namespace isc
