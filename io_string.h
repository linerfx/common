#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_string.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_string.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_FileItem.h"
#include <QString>

namespace fl {

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CString class строки в массиве не уникальные, но отличаются флагами.
///
class CString : public CFileItem {
public:
	// индексы строк... тут нужен всё-таки индекс по флагам наименований, как ITM_STPL, ITM_DEPT
	enum EStrIndices {
		iStrsNms,	// уник.индекс сравнение по флагам наименований объектов и строкам без учёта регистра
		iStrsLeti,	// уник.индекс сравнение по строкам без учёта регистра
		iStrsInds	// к-во индексов файла со строками
	};
public:
	BYTES m_str;// Utf-8
	CString();
	CString(int32_t nGrow);
	CString(CString &&s);
	CString(const CString &s);
	CString(const BYTE *pb, int32_t cnt);
	virtual ~CString() {}
	virtual const CString& operator =(const CString &s);
	virtual CString& operator =(CString &&s);
	virtual CString& Attach(CString &s);
	virtual int GetBinData(BYTES &ab) const override;
	virtual int SetBinData(CBYTE *pb, int32_t cnt) override;
	virtual void Init() override;
	virtual bool IsEmpty() const override;
	virtual bool IsNull() const override;
	virtual int cmp(const CString &s) const;
	virtual int cmpi(const CString &s) const;
	static int cmp_objNms(const CString &a, const CString &z);		// уникальный индекс iStrsNms, он же ibanal
//	static int cmp_stpl_nmi(const CString &a, const CString &z);// уник.индекс отделяет наименования товаров
//	static int cmps(const CString &a, const CString &z);		// не уникальный индекс iStrsLet
	static int cmp_si(const CString &a, const CString &z);		// сравнение без учёта регистра
	QString Qstr() const;
	CCHAR* str() const;
	typedef int(*fcmp)(const CString&, const CString&);
};

inline CString::CString()
{

}

inline CString::CString(int32_t nGrow)
	: m_str(nGrow)
{

}

inline CString::CString(CString &&s)
	: CFileItem(s)
	, m_str(s.m_str)
{
	m_flags = s.m_flags;
	m_str.Attach(s.m_str);
}

inline CString::CString(const CString &s)
	: CFileItem(s)
	, m_str(s.m_str)
{

}

inline CString::CString(const BYTE *pb, int32_t cnt)
    : m_str(pb, cnt)
{

}

inline const CString& CString::operator =(const CString &s)
{
	CFileItem::operator =(s);
	m_str = s.m_str;
	return *this;
}

inline CString &CString::Attach(CString &s)
{
	CFileItem::operator =(s);
	m_str.Attach(s.m_str);
	return *this;
}

inline void CString::Init()
{
	fl::CFileItem::Init();
	m_str.SetCountZeroND();
}

inline CString& CString::operator =(CString &&s)
{
	CFileItem::operator =(s);
	m_str.Attach(s.m_str);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::IsEmpty пустая строка подлежит удалению в файле, если её ид-р действительный.
/// \return трю/хрю
///
inline bool CString::IsEmpty() const
{
	return (!m_str.GetCount());
}

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::IsNull отстутствующий эл-т совершенно новый
/// \return
///
inline bool CString::IsNull() const
{
	return (!m_str.GetCount() && CFileItem::IsNull());
}

inline QString CString::Qstr() const
{
	return QString::fromUtf8((CCHAR*) (CBYTE*) m_str, m_str.GetCount());
}

}// namespace fl