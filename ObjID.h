#pragma once
#include "defs.h"

namespace glb {
////////////////////////////////////////////////////////////////////////
/// \brief The CObjID struct - идентификатор эл-та (располагается тут, а не
/// в ../liner/ т.к. упоминается в FxArray.h)
///
struct	CObjID
{                   // 64 бит
	BYTE m_iSym,    // индекс (0 - 255) инструмента в Линейщикъ
		m_iPrd;     // индекс периода
	int16_t m_nType;// тип объекта OT_...
	int32_t m_iObj; // индекс объекта -2 147 483 648 - 2 147 483 647
	CObjID() {
		Init();
	}
	CObjID(int32_t iObj, BYTE iSym, BYTE iPrd, int16_t nType) {
		m_iObj = iObj;
		m_iSym = iSym;
		m_iPrd = iPrd;
		m_nType = nType;
	}
	void Init() {
		m_iObj = INV32_IND;
		m_iSym = 0xff;
		m_iPrd = 0xff;
		m_nType = OT_NONE;
	}
	void Set(int32_t iObj, BYTE iSym, BYTE iPrd, int16_t nType) {
		m_iObj = iObj;
		m_iSym = iSym;
		m_iPrd = iPrd;
		m_nType = nType;
	}
	bool IsValid() const {
		return (m_iObj != INV32_IND &&
				m_iSym != 0xff &&
				m_iPrd != 0xff &&
				m_nType != OT_NONE);
	}
	int cmp(const CObjID &o) const {
		return((m_iSym < o.m_iSym) ? -1 : (m_iSym > o.m_iSym) ? 1 :
				(m_iPrd < o.m_iPrd) ? -1 : (m_iPrd > o.m_iPrd) ? 1 :
				(m_iObj < o.m_iPrd) ? -1 : (m_iObj > o.m_iObj) ? 1 : 0);
	}
	bool operator ==(const CObjID &o) const {
		return (m_iObj == o.m_iObj && m_nType == o.m_nType && m_iPrd == o.m_iPrd && m_iSym == o.m_iSym);
	}
	bool operator !=(const CObjID &o) const {
		return (m_iObj != o.m_iObj || m_nType != o.m_nType || m_iPrd != o.m_iPrd || m_iSym != o.m_iSym);
	}
	bool operator <(const CObjID &o) const {
		return ((m_iObj < o.m_iObj) ? true : (m_iObj > o.m_iObj) ? false :
				(m_nType < o.m_nType) ? true : (m_nType > o.m_nType) ? false :
				(m_iPrd < o.m_iPrd) ? true : (m_iPrd > o.m_iPrd) ? false : (m_iSym < o.m_iSym) ? true : false);
	}
	bool operator >(const CObjID &o) const {
		return ((m_iObj > o.m_iObj) ? true : (m_iObj < o.m_iObj) ? false :
				(m_nType > o.m_nType) ? true : (m_nType < o.m_nType) ? false :
				(m_iSym > o.m_iSym) ? true : (m_iSym < o.m_iSym) ? false :
				(m_iPrd > o.m_iPrd) ? true : false);
	}
};

}// namespace glb