/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл gcolour.cpp — часть программы iTogClient. iTogClient свободная
 * программа, вы можете перераспространять её и/или изменять на условиях
 * Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient распространяется в надежде, что она будут полезной, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2022 Nikolay Vamboldt
 *
 * gcolour.cpp is part of iTogClient. iTogClient programm is free software: you
 * can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version
 * 3 of the License, or (at your option) any later version.
 *
 * iTogClient is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "gcolour.h"
#include "defs.h"
#include <QColor>
#include <QPalette>
#include <QApplication>
#include "ErrLog.h"

namespace err {
extern CLog log;
}

namespace glb {
namespace c {

static CColour::fcmp afcmp[] = { CColour::cmpd, CColour::cmp };
static thread_local CFxArrayInd<CColour, sizeof afcmp / sizeof *afcmp, true> ac(afcmp);

static CColour::fcmp afcmpg[] { CColour::cmp };
static thread_local CFxArrayInd<CColour, sizeof afcmpg / sizeof *afcmpg, true> acg(afcmpg);

// находит наибольший компонент цвета в 'c' если 'rem', то этот компонент изымается из 'c'
CClr CColour::Mx(uint32_t &ic, bool rem)
{
	uint32_t r = (ic & 0x00ff0000) >> 16 & 0x000000ff,
	        g = (ic & 0x0000ff00) >> 8 & 0x000000ff,
	        b = ic & 0x000000ff;
	CClr c; // { none, 0 }
	if (r > g) {
		c.c = r;
		c.clr = CClr::rd;
	} else if (r < g) {
		c.c = g;
		c.clr = CClr::gn;
	} else// если цвета равны, c.clr == CClr::nn
		c.c = r;
	if (c.c < b) {
		if (rem)
			ic &= 0xffffff00;
		c.c = b;
		c.clr = CClr::be;
	} else if (rem)
		if (c.clr == CClr::rd)
			ic &= 0xff00ffff;
		else if (c.clr == CClr::gn)
			ic &= 0xffff00ff;
	return c;
}

// сортировка компонентов цвета 'с' по убыванию: от большего к меньшему
CClr* CColour::Srt(CClr ac[], uint32_t c)
{
	ac[0] = Mx(c, true);
	ac[1] = Mx(c, true);
	ac[2] = Mx(c, true);
	return ac;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CColour::Invert инвертирую цвет по составляющим
/// \param c: цвет
/// \return инвертированный	цвет: (255 - цвет) отдельно красный, зелёный и синий
///
uint32_t CColour::Invert(uint32_t c)
{
	c = c & 0xff00ffff | (0x000000ff - (((c & 0x00ff0000) >> 16) & 0x000000ff)) << 16 & 0x00ff0000;
	c = c & 0xffff00ff | (0x000000ff - (((c & 0x0000ff00) >> 8) & 0x000000ff)) << 8 & 0x0000ff00;
	return (c & 0xffffff00 | (0x000000ff - (c & 0x000000ff)) & 0x000000ff);
}

/*#define A1 aa[0]
#define A2 aa[1]
#define A3 aa[2]
#define Z1 az[0]
#define Z2 az[1]
#define Z3 az[2]*/
/////////////////////////////////////////////////////////////////////////
/// \brief CColour::cmp сравниваю наибольшие компоненты цветов. Допустим, наибольший компонет
/// красный, его 128, тогда другие, скажем, 100 и 15, или 127 и 122. Другой тоже с приоритетным
/// красным, там его 19 и других 7 и 8. Второй явно темнее. Достаточно ли будет сравнение только 
/// доминирующих цветов, или понадобится сравнение и прочих? Можно брать следующий в сортировке.
/// Например, следующий синий, его 127 и во втором тоже синий, его 8. Сравниваю все 3 компонента
/// если первые равны. Но в большинстве случаев, я так полагаю, до третьего дело не дойдет.
/// \param ca
/// \param cz
/// \return
///
int CColour::cmp(const CColour &cA, const CColour &cZ)// static
{
	CClr aa[3], az[3];
	// сортирую компоненты каждого светлого цвета по убыванию
	Srt(aa, cA.Lightest());
	Srt(az, cZ.Lightest());
	// если преобладающие компоненты цвета равны, смотрю их к-ва
	auto cmpc =[](const CClr &a, const CClr &z) {
		if (a.clr == z.clr)
			return ((a.c > z.c) ? 1 : (a.c < z.c) ? -1 : 0);
		return ((a.clr > z.clr) ? 1 : -1);
	};
	for (int n, i = 0; i < 3; i++)
		if (0 != (n = cmpc(aa[i], az[i])))
			return n;
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CColour::cmpd cmp dif: чем более компоненты цвета разнятся, тем они менее нейтральные (серые),
/// более цветные. В этом индексе наименьшие будут наиболее серыми, тёмными или светми, не важно: серыми
/// \param cA, cZ сравниваемые цвета
/// \return -1; 0; 1;
///
int CColour::cmpd(const CColour &cA, const CColour &cZ)// static
{	// здесь беру светлый из двух, но можно брать среднее от обоих dif, это будет показателем "серости"
	// градиента, а не его светлого цвета. Градиенты могут иметь серый светлый и цветной тёмный.
	float	a = dif(cA.Lightest()),// (dif(cA.c1) + dif(cA.c2)) / 2.f,
	        z = dif(cZ.Lightest());// c1) + dif(cZ.c2)) / 2.f;
	return (DCMP_MORE(a, z, FLT_EPSILON) ? 1 : DCMP_LESS(a, z, FLT_EPSILON) ? -1 : 0);
}

/*int CColour::cmp(const CColour &cA, const CColour &cZ)// static
{
	uint32_t na = cA.Lightest(), nz = cZ.Lightest();
	float ca, cz, f;
	// к-во красного относительно к-ва зелёного и синего в диапазоне 0 - 100:
	f = Red(na) - Red(nz);
	fc = 8.f;// в пределах 8% цвет одинаковый
	if (DCMP_LESS(f, -fc, .0001))
		return -1;
	else if (DCMP_MORE(f, fc, .0001))
		return 1;// разные по к-ву красного
	// к-во зелёного
	ca = Grn(na);
	cz = Grn(nz);
	f = ca - cz;
	if (DCMP_LESS(f, -fc, .0001))
		return -1;
	else if (DCMP_MORE(f, fc, .0001))
		return 1;// разные по к-ву зелёного
	// к-во синего
	ca = Ble(na);
	cz = Ble(nz);
	f = ca - cz;
	if (DCMP_LESS(f, -fc, .0001))
		return -1;
	else if (DCMP_MORE(f, fc, .0001))
		return 1;// разные по к-ву синего
	// яркость (к-во белого) в диапазоне 0 - 255
	ca = Light(na);
	cz = Light(nz);
	f = ca - cz;
	// в пределах 8,16 части (3,2%) яркость одинаковая
	fc = 255.f * .032f;
	if (DCMP_LESS(f, -fc, .0001))
		return -1;
	else if (DCMP_MORE(f, fc, .0001))
		return 1;// разные по яркости
	// равны... Здесь можно ещё добавить сравнение по к-ву сдвоенных цветов:
	// кр-зел, кр-син, зел-син
	return 0;
}*/

/* нихуя не зафацало потому что формула не точно воспроизводится. А если точно, то нужно
 * присваивать каждому эл-ту "дистанцыю", а после их уже сравнивать. Это возможно.
	QColor ca(MAX(cA.c1, cA.c2)),
	        cz(MAX(cZ.c1, cZ.c2));
	ca = ca.toHsv();
	cz = cz.toHsv();
	int ha = ca.hsvHue(), sa = ca.saturation(), va = ca.value(),
			hz = cz.hsvHue(), sz = cz.saturation(), vz = cz.value();*/
	/* https://stackoverflow.com/questions/35113979/calculate-distance-between-colors-in-hsv-space
	 * такая формула указана по ссылке
	 * *************
	float f = (int) (sin(ha) * sa * va - sin(hz) * sz * vz) ^ 2 +
	        (int) (cos(ha) * sa * va - cos(hz) * sz * vz) ^ 2 +
			(int) (va - vz) ^ 2;
	************************
	int ia = (int) (sin(ha) * sa * va) ^ 2 +
	        abs((int) (cos(ha) * sa * va)) ^ 2 + (int) va ^ 2;
	int iz = (int) (sin(hz) * sz * vz) ^ 2 +
	        abs((int) (cos(hz) * sz * vz)) ^ 2 + (int) vz ^ 2;
			там есть и другая:
	dh = min(abs(ha - hz), 360 - abs(ha - hz)) / 180.0
	ds = abs(s1 - s0)
	dv = abs(va - vz) / 255.0
	dist = sqrt(dh * dh + ds * ds + dv * dv)
	я её попытался подогнать */
/* double fha = MIN(ha, 360 - hz) / 180.0,
	        fhz = MIN(hz, 360 - hz) / 180.0,
	        fva = va / 255.0,
	        fvz = vz / 255.0;
	double fda = sqrt(fha * fha + sa * sa + fva * fva),
	        fdz = sqrt(fhz * fhz + sz * sz + fvz * fvz);
	return (DCMP_MORE(fda, fdz, .00001) ? 1 : DCMP_LESS(fda, fdz, .00001) ? -1 : 0);
}*/

///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CColour::Nearest находит ближайший из светлых в ac
/// \param colour
/// \return
///
const CColour& CColour::Nearest(uint32_t colour)// static
{
	if (!ac.GetCount()) {
		CColour colors[] = GCOLORS;
		ac.Add(colors, sizeof colors / sizeof *colors);
	}// rgb без прозрачности
	CColour c{ colour & 0x00ffffff, 0 };
	int i = ac.FindNearest(c, Clr);
	if (i == ac.GetCount())
		i--;
	ASSERT(i >= 0);
	return ac.GetItem(Clr, i);
}

CGrayColours::CGrayColours()
{
	if (!ac.GetCount()) {
		CColour colors[] = GCOLORS;
		ac.Add(colors, sizeof colors / sizeof *colors);
	}
	CColour c;
	float f = 0.f;
	for (int i = 0, n = ac.GetCount(); i < n; i++) {
		c = ac.GetItem(CColour::Dif, i);
		f = CColour::dif(c.Lightest());
		if (DCMP_LESS(f, 8.f, FLT_EPSILON))
			acg.Add(&c);
		else
			break;
	}
}

const CColour& CGrayColours::Nearest(uint32_t ic)// static
{
	CColour c{ ic & 0x00ffffff, 0 };
	int32_t i = acg.FindNearest(c, 0);
	if (i == acg.GetCount())
		i--;
	ASSERT(i >= 0);
	return acg.GetItem(0, i);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief TxtBG выбираю цвет из серых для фона текста не активного эл-та и из обычных цветов - для активного
/// Кнопки получились ништяк и фон эл-тов списка, но рамки списков в не выделенном состоянии довольно хуёво.
/// Для них можно поробовать другие цвета из палитры, такие как QPalette::Link и QPalette::LinkVisited
/// \return цвета градиента фона текста эл-та, например, списка
///
const CColour TxtBG(bool bActive)
{
/*	QColor c = QApplication::palette().color(bActive ? QPalette::Active : QPalette::Inactive, QPalette::Highlight);
	return (bActive ? CColour::Nearest(c.rgb()) : CGrayColours::Nearest(c.rgb()));*/
/*	return CColour::Nearest(
		QApplication::palette().color(bActive ? QPalette::Active : QPalette::Inactive, QPalette::Highlight).rgb());*/
	if (bActive) {
		QColor c = QApplication::palette().color(QPalette::Active, QPalette::Highlight);
		return CColour::Nearest(c.rgb());
	} else {
		QColor c(0xff404040);//Qt::darkGray);
		return CGrayColours::Nearest(c.rgb());
	}
}

const CColour Border(bool bActive)
{
	return CColour::Nearest(QApplication::palette().color(bActive ? QPalette::LinkVisited : QPalette::Link).rgb());
}

}// namespace c
}// namespace glb
