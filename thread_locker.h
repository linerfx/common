#pragma once

#include <mutex>
#include <chrono>
#include <climits>

using namespace std;

class CThrdLock {
private:
	recursive_timed_mutex &m_mtx; // "рекурсивный" для основного потока, там это возможно. Чтобы не фатально.
	bool m_bLocked;
public:
	CThrdLock(recursive_timed_mutex & mtx, bool bLock = true, unsigned int nMsWait = UINT_MAX) :
		m_mtx(mtx),
		m_bLocked(bLock) {
		if (bLock)
			if (nMsWait == UINT_MAX) // блокировка без ограничения времени
				m_mtx.lock();
			else
				Lock(nMsWait); // 'Lock' присвоит положит.значение m_bLocked или швырнет исключение
	}
	~CThrdLock() {
		if (m_bLocked)
			m_mtx.unlock();
	}
	bool Lock(unsigned int nMsWait) {
		return (m_bLocked = m_mtx.try_lock_for(chrono::milliseconds(nMsWait)));
	}
	void Unlock() {
		if (m_bLocked) {
			m_mtx.unlock();
			m_bLocked = false;
		}
	}
	bool Locked() const {
		return m_bLocked;
	}
};
