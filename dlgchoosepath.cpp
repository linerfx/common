/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл dlgchoosepath.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * dlgchoosepath.cpp is part of reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "dlgchoosepath.h"
#include "./ui_dlgchoosepath.h"

namespace ui {

CDlgChoosePath::CDlgChoosePath(QWidget *parent,
							   const glb::CvArray<glb::CFxString> &as) :
	QDialog(parent),
	ui(new Ui::CDlgChoosePath)
{
	ui->setupUi(this);
	for (int i = 0, cnt = as.GetCount(); i < cnt; i++)
		ui->cboxPaths->addItem(QString(as[i]));
	m_iSelected = 0;
	ui->cboxPaths->setCurrentIndex(0);
}

CDlgChoosePath::~CDlgChoosePath()
{
	delete ui;
}

void CDlgChoosePath::OnIndexChanged(int index)
{
	m_iSelected = index;
}

}// namespace ui