﻿#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл TimeDiff.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * TimeDiff.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
/*#if !defined(AFX_TIMEDIFF_H__E45AF2EA_BF43_48FD_975F_E7A78076D462__INCLUDED_)
#define AFX_TIMEDIFF_H__E45AF2EA_BF43_48FD_975F_E7A78076D462__INCLUDED_*/
#include <chrono>

/*#if _MSC_VER >= 1000
#pragma once
#endif */// _MSC_VER >= 1000

/*
	0,000000001	наносекунда (pow(10.0, -9.0))
	0,0000001	интервал значения Win.FileTime (100 наносекунд)
	0,001		миллисекунда (pow(10.0, -3.0)) - интервал, возвращаемый CTimeDiff
*/
/*class CTimeDiff
{
//	__time64_t m_nStart;
	std::chrono::system_clock::time_point m_tpStart;
protected:
	std::chrono::system_clock::time_point Now() const;
public:
	time_t GetElapsed() const {
		std::chrono::system_clock::time_point dif(Now() - m_tpStart);
		return std::chrono::system_clock::to_time_t(dif);
	}
	void Initialize() {
		m_tpStart = Now();
	}
public:// конструкцыя/деструкцыя
	CTimeDiff() {
		Initialize();
	}
	~CTimeDiff();
};*/

namespace glb {

class CTimeDiff
{	// точка отсчёта
	std::chrono::high_resolution_clock::time_point m_tpStart;
public: // операцыи
	// истёкшее от последнего вызова Initialize время в миллисекундах
	time_t GetElapsed() const {
		using namespace std::chrono;
		return duration_cast<milliseconds>(high_resolution_clock::now() - m_tpStart).count();
	}
	// устанавливает точку отсчёта от текущего времени
	void Initialize() {
		m_tpStart = std::chrono::high_resolution_clock::now();
	}
public:// конструкцыя/деструкцыя
	CTimeDiff() {
		Initialize();
	}
	~CTimeDiff();
};
}// namespace glb
//#endif // !defined(AFX_TIMEDIFF_H__E45AF2EA_BF43_48FD_975F_E7A78076D462__INCLUDED_)
