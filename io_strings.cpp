/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_strings.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_strings.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "io_strings.h"
#include "io_path.h"
#include "strdef.h"
#include "FileMan.h"

namespace glb {
	extern thread_local const CFxString sAppPath; // ::global.cpp
	const char* FileName(const char *pcszPath, int *pnLen);
	const char* Key(CFxString &s, int32_t iFlNmID);
	const char* PathName(CFxString &sPath, int32_t iDirID);
}

namespace fl {

// 2 индекса в массиве строк: по флагам и строкам и только по строкам. Оба уникальные, строки
// сравниваются без учёта регистра
CString::fcmp af_cmp_strs[] = {
	CString::cmp_objNms,		// уникальный индекс, по флагам и utf-8 строкам без учёта регистра
	CString::cmp_si			// уникальный индекс, сравнение по utf-8 строкам без учёта регистра
};

////////////////////////////////////////////////////////////////////////////////
/// \brief CStrings::CStrings - конструктор
///
CStrings::CStrings()
	: BAS(NULL, NULL, af_cmp_strs, false)
{
	ASSERT(CString::iStrsInds == sizeof af_cmp_strs / sizeof *af_cmp_strs);
	ASSERT(m_aiu[0] == ibanal);
}

CStrings::~CStrings()
{

}

//////////////////////////////////////////////////////////////////////////////
/// \brief CStrings::PathName предоставит базовому классу путь и имя файла.
/// \return путь и имя файла
///
CCHAR* CStrings::PathName()
{
	ASSERT(glb::sAppPath.GetCount());	// provide global init
	m_sPath = glb::sAppPath;
	return CPath::AddName(CPath::AddName(m_sPath, S_STRAF), S_STRA);
}

CCHAR* CStrings::Key(glb::CFxString &s)
{	// ключ файла в общем не зависит от п-ля, это предопределённые имена приложения и папки со строками.
	ASSERT(glb::sAppPath.GetCount());	// provide global init
	int nLen = -1;
	const char *pcszAppNm = glb::FileName(glb::sAppPath, &nLen);
	const int cKey = 128, cAppNm = 32;
	char pszAppNm[cAppNm] = "";
	ASSERT(nLen < cAppNm);
	memcpy(pszAppNm, pcszAppNm, nLen);
	pszAppNm[nLen] = '\0';
	s.SetCountND(cKey - 1);
	s.SetCountND(CFileMan::CFile::MkKey(s, cKey, 2, (char*) pszAppNm, S_STRAF));
	return s;
}

cint32_t* CStrings::UnicIndices(int32_t *pcnt) const // override
{
	ASSERT(m_aiu[0] == ibanal);
	if (pcnt)
		*pcnt = sizeof m_aiu / sizeof *m_aiu;
	return m_aiu; // обязательно в возрастающем порядке
}

}// namespace fl