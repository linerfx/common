#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл FileMan.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * FileMan.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "FxArray.h"
#include <QSharedMemory> // синхронизация потоков и процессов
#include <mutex>

namespace fl {

#ifdef __LINER
enum EFile {
	NONE,
	TICKER,
	IINFO,
	OPTIONS,
	TOPS,
	BOTTOMS,
	PRC_SEQ,
	SPD_SEQ,
	SU,
	SPD_GROW
};
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief The CFileMan class - меж-потоковая с std::recursive_mutex и меж-процессорная с помощью
/// QT-объекта QSharedMemory, который создаст разделяемую потоками и приложениями синхронизируемую
/// память во всех поддерживаемых операционках (кроме android'а).
/// Каждый процесс имеет массив "файлов" CFileMan::CFile, каждый из которых содержит собственный
/// объект QSharedMemory, содержащий ключ, собираемый ф-цией MkKey из имён папки и файла. Ключ
/// для каждого файла должен быть уникальным.
/// Таким образом осуществляется синхронизация доступа к этому файлу между процессами и потоками в
/// Linux'е, Wind'е и Mack'е-хуяке. Т.е. в тех операционках, в которых POSIX реализован достаточно.
/// Не в Android'е.
///
class CFileMan
{
public:
    // CFile размещает и хранит именованный участок памяти, доступный для любого приложения. Содержит
    // уникальный идентификатор файла, доступ к которому синхронизирован между приложениями. Иной
    // способ организовать меж-процессорную синхронизацию, такой как меж-процессорный mutex в Wind'е,
    // в QT и стандартной библиотеке отсутствует https://doc.qt.io/qt-5/ipc.html.
	// "Кроссплатформенность" Qt строится на <POSIX-разделяемая> память в андройде не реализована.
	class CFile : public glb::CFxArrayIndItem
	{
		friend class CFileMan;
	public:// следует с осторожностью... Сначала укажи адрес (CShared *ps = m_psm->data())
		struct CShared {
			int64_t	m_tmUpdated,	// время последнего изменения
					m_szKey;		// размер "ключа", включая завершающий нуль
			char m_cKey;			// первый символ ключа - строки, завершённой нулём
			static size_t size(const char *pcszKey);
			void init(const char *pcszKey);
		};
	private:
		// указатель на объект-шару, в недействительном объекте CFile нуль.
		QSharedMemory *m_psm;
		// Ид-р файла. Указатель на разделяемую память. Строка размещается в шаре, по ней сортируется
		// массив CfileMan::m_af, объектом коего является этот CFile.
		const char *m_pcszKey;
		bool m_blocked;
	public:
		CFile();
		CFile(const CFile &f);
		CFile(CFile &f);
		~CFile();
	public:
		virtual CFile& operator =(CFile &&o);
		virtual const CFile& operator =(const CFile &o);
		bool Create(const char *pcszKey, QObject *parent = Q_NULLPTR);
		void Destroy();
		void Lock();
		void Unlock();
        bool Locked() const {
            return m_blocked;
        }
		bool Changed(clock_t since) const;
		// Ф-ции для индексированного массива CFxArrayInd
        virtual bool IsValid() const override;
        virtual void Invalidate() override;
		//
		QString ErrorString() const {
			return m_psm->errorString();
		}
		clock_t GetTmUpdate() const {
			ASSERT(m_blocked && m_psm && m_psm->isAttached());
			CShared *ps = (CShared*) m_psm->data();
			return ps->m_tmUpdated;
		}
        static int MkKey(char *pszKey, const int nSzKey, int n, ...);
	};
    /////////////////////////////////////////////////////////////////////////////
	/// \brief The CLocker class блокирует файл в конструкторе если задан ключ, или ф-цией Lock()
	/// разблокирует в деструкторе или Unlock(). Возможно многоразовое использование как с одним и тем
	/// же ключом, так и с другим т.к. однажды полученный индекс в CFileMan::m_af, а именно fl::man.m_af
	/// действительный всю жизнь программы.
    ///
	class CLocker {
	protected:
		int32_t m_if;	// базовый (физический) индекс "файла" CFileMan::CFile в CFileMan::m_af
	public:
		CLocker(const char *pcszKey = nullptr);
		~CLocker();
	public:
		int32_t Lock(const char *pcszKey);// если конструктор был без параметров или с другим ключом
		void Lock();    // для повторного использования с тем же ключом
		void Unlock();
		void Destroy();
		bool Locked() const;
		bool IsValid() const;
		clock_t TmUpdated() const;
	};

    typedef int (*cmpf)(const CFile &f1, const CFile &f2);
    static int cmp_keys(const CFile &f1, const CFile &f2);
private:
	cmpf m_afc[1];
    std::recursive_mutex m_mtx;// меж-потоковая синхронизация доступа к m_af
    // массив файлов, каждый из которых, однажды созданный существует на протяжении жизни объекта CFileMan.
	glb::CFxArrayInd<CFile, 1, false> m_af;
public:
	CFileMan();
	~CFileMan();
	int32_t Lock(const char *pcszKey);//, CFile **ppf = nullptr);
	void Lock(int32_t i);
	void Unlock(int32_t i);
	void Destroy(int32_t i);
	void Destroy();
	bool Locked(int32_t _if);
	const CFile* GetFile(const char *pcszKey);
	const CFile* GetFile(int32_t _if);
};

inline size_t CFileMan::CFile::CShared::size(const char *pcszKey)
{
   size_t sz = MAX(sizeof(CShared), sizeof m_tmUpdated + sizeof m_szKey + strlen(pcszKey) + 1);
   return ((sz % 8) ? sz + (8 - sz % 8) : sz);
}

inline void CFileMan::CFile::CShared::init(const char *pcszKey)
{ // с этой ф-цией осторожно! this = m_psm->data()
	m_szKey = strlen(pcszKey) + 1;
//	ASSERT(m_szKey >= sizeof(int64_t));// остаток от выравнивания 7 байт, вроде
	m_tmUpdated = 0;
	memcpy(&m_cKey, pcszKey, m_szKey);
}

////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::~CFile
///
inline CFileMan::CFile::~CFile()
{
	Destroy();
}

////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::Lock
///
inline void CFileMan::CFile::Lock()
{
	ASSERT(m_psm && !m_blocked);
	m_blocked = m_psm->lock();
	ASSERT(m_blocked);
}

/////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::Unlock предоставляет доступ другим потоками и
/// приложениям к разделяемой памяти.
/// \param bChanged: не нуль, если файл был изменён
///
inline void CFileMan::CFile::Unlock()
{
	ASSERT(m_psm && m_blocked);
	m_blocked = false;
	m_psm->unlock();
}

/////////////////////////////////////////////////////////////////
/// \brief CFileMan::CFile::Empty освобождает системные ресурсы
///
/*inline void CFileMan::CFile::Empty()
{
	ASSERT(m_psm);
	delete m_psm;// деструктор отсоединит объект от разделяемой памяти
	m_psm = nullptr;
    m_pcszKey = nullptr;
	m_id.Init();
}*/

inline bool CFileMan::CFile::IsValid() const
{
	return (m_psm && m_pcszKey && m_psm->isAttached() && m_psm->size() >= sizeof(CShared));
}

inline bool CFileMan::CLocker::IsValid() const
{
	return (m_if >= 0);
}

inline const CFileMan::CFile* CFileMan::GetFile(int32_t _if)
{
	return &m_af[_if];
}

}// namespace fl