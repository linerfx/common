/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgIsc.cpp — часть программы iTogClient и iTog. iTogClient и iTog
 * свободные программы, вы можете перераспространять их и/или изменять на
 * условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения версии 3 лицензии,
 * или (по вашему выбору) любой более поздней версии.
 *
 * iTogClient и iTog распространяются в надежде, что будут полезными, но БЕЗО
 * ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ
 * ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее смотрите в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgIsc.cpp is part of iTogClient and iTog. These programms are free
 * software: you can redistribute them and/or modify them under the terms of
 * the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * iTogClient and iTog are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "iTgIsc.h"
#include "ErrLog.h"
#include "global.h"
#include "iTgDefs.h"
#include "iTgOpts.h"
#ifdef __ITOG
#include "postadrss.h"
#include "io_strings.h"
#include "io_fileArray.h"
#include "../iTog/iTog/postadrss.h"
#include "../iTog/iTog/product.h"
#include "../iTog/iTog/goods.h"
#include "../iTog/iTog/ingredients.h"
#include "../iTog/iTog/income.h"
#include "../iTog/iTog/global.h"// шаблон ф-ции GetFile(A &a, int32_t iDir, int32_t iNm, bool bAddSize, const char*)
#include "../iTog/iTog/contractor.h"
#include "../iTog/iTog/coworker.h"
#include "../iTog/iTog/dept.h"
#include "../iTog/iTog/server.h"
#include "../iTog/iTog/menue.h"
#elif defined __ITGCLNT
#include "../iTogClnt/iTogClnt/Options.h"
#endif// defined __ITOG

namespace glb {

CCHAR* FileName(CCHAR *pcszPath, int *pnLen);
int32_t GetImage(int32_t iImgID, int32_t nw, int32_t nh, CFxArray<BYTE, true> &ab, bool bAddSz);

#ifdef __ITOG

size_t SetFile(CCHAR *pcszPthNm, CBYTE *cpb, const size_t len);
bool RightTo(int32_t nuid, uint32_t nUsr, uint32_t nAsk, int32_t iupw);

#endif// defined __ITOG

}

#ifdef __ITGCLNT

namespace o {
extern thread_local COptions opt;
}// namespace o

namespace fl {
const glb::CFxString& String(int32_t iStrID);
}//namespaace fl

#endif// #ifdef __ITGCLNT

#ifdef __ITOG

namespace tcp {

extern CServer srv;

}//namespace tcp

namespace fl {

extern CStrings strs;

}// namespace fl

namespace o {

extern CiTgOpts opt;

}

namespace store {

extern CProducts prdcts;
extern CIngredients ings;
extern CGoods goods;
extern CIncomes income;

}

namespace cafe {

extern CContractors cntrctrs;
extern CSuppliers splrs;
extern CPostAdrss adrss;		// почтовые адреса
extern CCoworkers cwrkrs;
extern CDepts depts;
extern CMenues menues, sbmns;

}

// только для местного использования в iTog
glb::CFxArray<BYTE, true> g_ab;

#endif// defined __ITOG

namespace err {
    extern CLog log;
}

namespace isc {// inter-server-client objects' data exchange

#ifdef __ITOG

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::SetToSend собираю в 'ab' минимум сведений, необходимых для отображения
/// эл-та в представлении клиента
/// \param szImg: если указан размер и есть действит.ид-р имени файла рисунка, читаю файл
/// рисунка с диска
/// \param szSbimg: размер рисунка подэлемента здесь не используется
///
bool CBaseItem::SetToSend(uint32_t szImg, uint32_t/*uSbimg*/, bool/*bVisible*/) // uSbimg = 0, bVisible = true
{	// наименование эл-та
	fl::strs.GetItem(fl::ioffset, iNm, sNm);
	// описание эл-та из файла: текст с описанием, размер текста добавлять в 'sDn' не нужно
	sDn.SetCountZeroND();
	if (iDn >= 0)
		glb::GetFile<glb::CFxString, char>(sDn, SI_TXT, iDn, false);
	// рисунок из файла: рисунок обрезается и масштабируется, размер рисунка добавлять в массив 'img' не нужно
	img.SetCountZeroND();
	if (iImg >= 0 && HWORD32(szImg) && LWORD32(szImg))
		glb::GetImage(iImg, szImg >> 16 & LHALF32, szImg & LHALF32, img, false);
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::SaveToBase записываю в базу наименование и описание, получаю или обнов-
/// ляю их ид-ры. Переопределённая ф-ция обязана соблюдать правила целостности индексирован-
/// ного массива и правила пустого объекта.
/// См. fl::CFileItem::IsEmpty(), fl::a::CvBase/CiShrd::Add/EditUnic()
/// Если есть наименование, оно будет записано как и описание по правилам целостности массива.
/// Если наименования нет но есть ид-р строки с наименованием, строка будет отмечена как не
/// действительная. Если наименования нет, то и описание будет удалено, если оно было для это-
/// го объекта.
/// \param iind: для производного т.к. этот объект содержит общие сведения для всех производных
/// \return базовый ид-р этого объекта
///
int32_t CBaseItem::SaveToBase(int16_t/* iind*/, fl::CFileItem &/*fi*/)
{
	ASSERT((sNm.m_flags & ITM_MNMS) == NmFlags()); // см. конструктор
	// сначала строки
	fl::a::COpener<fl::CStrings> os(fl::strs);
	// НАИМЕНОВАНИЕ если есть и ОПИСАНИЕ если есть
	if ((iNm = fl::a::SetItem(fl::strs, sNm, fl::ioffset, iNm)) >= 0 && sDn.GetCount()) {
		fl::CFileItemObtainer<fl::CStrings, fl::CString> osd(fl::strs), osf(fl::strs);
		fl::CString &sd(osd.obj()), &sf(osf.obj());
		// если есть имя файла с описанием
		if (iDn >= 0)
			fl::strs.GetItem(fl::ioffset, iDn, sf);
		else { // если нет имени файла для описания, делаю его из наименования
			sf.m_str.SetND(sNm.m_str);
			sf.m_flags = 0; // флаги строки с именем файла для описания объекта (0)
			iDn = (fl::CPath::MakeFileName(sf.m_str, (CBYTE*) ".txt", 4) > 0) ?
					  fl::strs.AddUnic(sf) : INV32_BIND;
			ASSERT(iDn >= 0); // отказ не принимается
		}// открываю или создаю файл для описания с синхронизацией между потоками и процессами и (пере-)записываю файл
		os.Close();
		glb::CFxString spath(glb::sAppPath), sKey;
		const int cpl = 256;
		sKey.SetCount(cpl); // байт. По-русски будет в 2 раза меньше
		sKey.SetCountND(fl::CFileMan::CFile::MkKey(sKey, cpl, 2, SZ(SI_TXT, sd), sf.str()));
		fl::CPath::AddName(fl::CPath::AddName(spath, sd.str()), sf.str());
		fl::CFileMan::CLocker locker(sKey);
		glb::SetFile(spath, (CBYTE*) (CCHAR*) sDn, sDn.GetCount());
		locker.Unlock();// после уже деструкторы spath и sKey
	}// если нет наименования или описания, но есть ид-р имени файла с описанием, описание удаляется
	else if (iDn >= 0) {
		fl::CPath::RmFile(SI_TXT, iDn);
		fl::strs.Invalidate(fl::ioffset, iDn);
		iDn = INV32_BIND;
	}
	return iObj;
}

#endif // defined __ITOG

/*void CBaseItem::Set(int32_t iItm, const fl::CFileItem &fl)
{
	const fl::CBaseObj *po = CAST(const fl::CFileItem, const fl::CBaseObj, &fl);
	ASSERT(nullptr != po);
	tmEdit = fl.m_tme;
	tmCrtd = fl.m_tmc;
	flgs = fl.m_flags;
	iObj = iItm;
	iPrnt = po->m_iPrnt;
	iNm = po->m_iNm;
	iDn = po->m_iDn;
	iImg = po->m_iImg;
	SetType(po->m_tp);
}*/

fl::CFileItem& CBaseItem::Get(int32_t &iItm, fl::CFileItem &fi) const
{
	try {// если объект fl::CBaseObj или производный
		fl::CBaseObj *pbo = CAST(fl::CFileItem, fl::CBaseObj, &fi);
		if (pbo != nullptr) {
			pbo->m_iImg = iImg;
			pbo->m_iNm = iNm;
			pbo->m_iDn = iDn;
			pbo->m_iPrnt = iPrnt;
			pbo->m_tp = tp;
		}
	} catch (err::CLocalException e) { // CAST
#ifdef __ITGCLNT
		err::log.Add(SZ(SI_LE), time(NULL), e.m_iStrID, __FILE__, __LINE__);
#else
		fl::CFileItemObtainer<fl::CStrings, fl::CString> osf(fl::strs);
		err::log.Add(SZ(SI_LE, osf.obj()), time(NULL), e.m_iStrID, glb::FileName(__FILE__), __LINE__);
#endif
	}
	fi.m_flags = flgs & ITM_SRVM; // без флагов от представления
	fi.m_nref = 0;
	fi.m_nusr = 0;
	fi.m_tmc = tmCrtd;
	fi.m_tme = tmEdit;
	iItm = iObj;
	return fi;
}

isc::CBaseItem& CBaseItem::Set(int32_t iItm, const fl::CFileItem &fi)
{
	try {// если объект fl::CBaseObj или производный
		const fl::CBaseObj *pbo = CAST(const fl::CFileItem, const fl::CBaseObj, &fi);
		if (pbo != nullptr) {
			iImg = pbo->m_iImg;
			iNm = pbo->m_iNm;
			iDn = pbo->m_iDn;
			iPrnt = pbo->m_iPrnt;
			SetType(pbo->m_tp);
		} else {
			iImg = INV32_BIND;
			iNm = INV32_BIND;
			iDn = INV32_BIND;
			iPrnt = INV32_BIND;
		}
	} catch (err::CLocalException e) { // CAST
#ifdef __ITGCLNT
		err::log.Add(SZ(SI_LE), time(NULL), e.m_iStrID, __FILE__, __LINE__);
#else
		fl::CFileItemObtainer<fl::CStrings, fl::CString> osf(fl::strs);
		err::log.Add(SZ(SI_LE, osf.obj()), time(NULL), e.m_iStrID, glb::FileName(__FILE__), __LINE__);
#endif
	}
	flgs = fi.m_flags;
	tmCrtd = fi.m_tmc;
	tmEdit = fi.m_tme;
	iObj = iItm;
	return *this;
}

void CBaseItem::Set(int64_t tmc, int64_t tme, uint32_t flags, int32_t iItm, int32_t iParent,
					int32_t iName, int32_t iDsn, int32_t iImage, fl::CBaseObj::Type t)
{
	tmEdit = tme;
	tmCrtd = tmc;
	flgs = flags;
	iObj = iItm;
	iPrnt = iParent;
	iNm = iName;
	iDn = iDsn;
	iImg = iImage;
	SetType(t);
}

void CBaseItem::Get(int64_t &tmc, int64_t &tme, uint32_t &flags, int32_t &iItm, int32_t &iParent,
					   int32_t &iName, int32_t &iDsn, int32_t &iImage, fl::CBaseObj::Type &a_tp)
{
	tme = tmEdit;
	tmc = tmCrtd;
	flags = flgs;
	iItm = iObj;
	iParent = iPrnt;
	iName = iNm;
	iDsn = iDn;
	iImage = iImg;
	a_tp = tp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::cmp_tpnm уник.индекс по типу и наименованию с учётом регистра
/// \param a, z
/// \return <0|0|>0
///
int CBaseItem::cmp_tpnm(const CBaseItem *const &a, const CBaseItem *const &z)
{
	if (a->tp != z->tp)
		return (a->tp < z->tp) ? -1 : 1;
	int32_t n1, n2;
	CBYTE *cpba = a->sNm.m_str.data(n1), *cpbz = z->sNm.m_str.data(n2);
	return glb::cmp_utf8s((CCHAR*) cpba, n1, (CCHAR*) cpbz, n2);
}

int CBaseItem::cmp_nmi(const isc::CBaseItem *const &a, const isc::CBaseItem *const &z)
{
	int32_t n1, n2;
	CBYTE *cpba = a->sNm.m_str.data(n1), *cpbz = z->sNm.m_str.data(n2);
	return glb::cmp_iUtf8s((CCHAR*) cpba, n1, (CCHAR*) cpbz, n2);
}

int CBaseItem::cmp_tpid(const isc::CBaseItem *const &a, const isc::CBaseItem *const &z)
{
	if (a->tp != z->tp)
		return (a->tp < z->tp) ? -1 : 1;
	return a->iObj - z->iObj;
}

int32_t CBaseItem::GetBinData(BYTES &ab) const
{
	cint32_t c = ab.GetCount();
	ab.AddND((CBYTE*) &tmEdit, SZ_BASITM);
	sNm.GetBinData(ab);
	sDn.GetBinData(ab);
	img.GetBinData(ab);
	return (ab.GetCount() - c);
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItem::SetBinData читаю из ввода полученное с сервера об эл-те
/// \return к-во прочитанных байт
///
int32_t CBaseItem::SetBinData(CBYTE *cpb, cint32_t cnt)
{
//	uint32_t nf = flgs & ~ITM_SRVM;// в представлении в эл-т спускаются флаги, как ITM_EDIT их нужно уважать...
	int32_t i = glb::read_raw((BYTE*) &tmEdit, SZ_BASITM, cpb, cnt);
//	flgs |= nf;
	i += sNm.SetBinData(cpb + i, cnt - i);
	i += sDn.SetBinData(cpb + i, cnt - i);
	return (i + img.SetBinData(cpb + i, cnt - i));
}

void CBaseItem::SetType(fl::CBaseObj::Type t)
{
	switch (t) {
	case fl::CBaseObj::dept:
	case fl::CBaseObj::subdept:
		flgs = sNm.m_flags = ITM_DEPT;
		break;
	case fl::CBaseObj::menue:
	case fl::CBaseObj::submenue:
		flgs = sNm.m_flags = ITM_MENUE;
		break;
	case fl::CBaseObj::staple:
		flgs = sNm.m_flags = ITM_STPL;
		break;
	case fl::CBaseObj::product:
		flgs = sNm.m_flags = ITM_PRDCT;
		break;
	default:
		break;
	}
	tp = t;
}

CBaseItem* CBaseItem::Duplicate(const CBaseItem *pbi)
{
	switch (pbi->tp) {
	case fl::CBaseObj::dept:		// отдел CDept, в m_ai ид-ры подотделов или меню
	case fl::CBaseObj::subdept:		// подотдел CDept, в m_ai ид-ры подотделов или меню
		return new isc::CDept(*CAST(const CBaseItem, const CDept, pbi));
	case fl::CBaseObj::menue:		// меню, в m_ai ид-ры подменю
	case fl::CBaseObj::submenue:	// подменю, в m_ai ид-ры продуктов
		return new isc::CMenue(*CAST(const CBaseItem, const CMenue, pbi));
	case fl::CBaseObj::product:		// продукт, в m_ai ид-ры ингредиентов
		return new isc::CProduct(*CAST(const CBaseItem, const CProduct, pbi));
	case fl::CBaseObj::staple:		// товар, m_ai пустой
		return new isc::CStaple(*CAST(const CBaseItem, const CStaple, pbi));
	case fl::CBaseObj::ingrdt:		// ингредиент: товар m_ai пустой; или продукт, тогда с ингредиентами
		return new isc::CIngredient(*CAST(const CBaseItem, const CIngredient, pbi));
	case fl::CBaseObj::cntrctr:		// контрактник, m_ai пустой
	case fl::CBaseObj::cwrkr:		// сотрудник
		return new isc::CContractor(*CAST(const CBaseItem, const CContractor, pbi));
	case fl::CBaseObj::splr:		// поставщик (наследует контрактника)
		return new isc::CSupplier(*CAST(const CBaseItem, const CSupplier, pbi));
	case fl::CBaseObj::bill:		// счёт
	case fl::CBaseObj::income:		// приход
	case fl::CBaseObj::expend:		// расход
	default:
		ASSERT(false);
	}
	return nullptr;
}

CBaseItem* CBaseItem::Create(fl::CBaseObj::Type tp)
{
	switch (tp) {
	case fl::CBaseObj::dept:		// отдел CDept, в m_ai ид-ры подотделов или меню
	case fl::CBaseObj::subdept:		// подотдел CDept, в m_ai ид-ры подотделов или меню
		return new isc::CDept(tp);
	case fl::CBaseObj::menue:		// меню, в m_ai ид-ры подменю
	case fl::CBaseObj::submenue:	// подменю, в m_ai ид-ры продуктов
		return new isc::CMenue(tp);
	case fl::CBaseObj::product:		// продукт, в m_ai ид-ры ингредиентов
		return new isc::CProduct;
	case fl::CBaseObj::staple:		// товар, m_ai пустой
		return new isc::CStaple;
	case fl::CBaseObj::ingrdt:		// ингредиент: товар m_ai пустой; или продукт, тогда с ингредиентами
		return new isc::CIngredient;
	case fl::CBaseObj::cntrctr:		// контрактник, m_ai пустой
	case fl::CBaseObj::cwrkr:		// сотрудник
		return new isc::CContractor(tp);
	case fl::CBaseObj::splr:		// поставщик (наследует контрактника)
		return new isc::CSupplier;
	case fl::CBaseObj::bill:		// счёт
	case fl::CBaseObj::income:		// приход
	case fl::CBaseObj::expend:		// расход
		ASSERT(false);
		break;// здесь надо швырять исключение InvalidEnum
	default:
		return new CBaseItem;
	}
	return nullptr;
}


double CIngredient::Weight() const
{	// выход в е.и.
	ASSERT(iMu != INV32_BIND);
#ifdef __ITOG
	const store::CMu &mu = o::opt.m_amu[iMu];
#else
	const store::CMu &mu = o::opt.m_oiTg.m_amu[iMu];
#endif
	// если товар штучный, беру ёмкость тары
	double fw = mu.ByPiece() ? fQtt * fTara : fQtt;
	return (fw - fw * fLs);
}

#ifdef __ITOG

bool CIngredient::SetToSend(uint32_t uImg, uint32_t/*nSbimg*/, bool bVisible)// bVisible = true
{
	ASSERT(iObj >= 0 && tp == fl::CBaseObj::ingrdt);
	// товар и ингредиент
	fl::CFileItemObtainer<store::CIngredients, store::CIngredient> oing(store::ings);
	const store::CIngredient &ing = oing.GetItem(fl::ioffset, iObj);
	ASSERT(ing.IsValid());
	if (bVisible && ing.m_flags & (ITM_EDIT|ITM_HDN|ITM_ARC))
		return false;
	// флаги ингредиента подчистую
	flgs = ing.m_flags;
	// ид-р товара или продукта
	iSource = ing.m_iSource;
	// ид-р базового ингр-та, если этот альтернативный
	idBas = ing.m_idBas;
	// если ингр-т альтернативный, наличие основного эл-та обязательно
	ASSERT(!(flgs & ITM_INGTALT) || idBas.Valid());
	// ингредиент может быть товаром или продуктом...
	if (ing.m_flags & ITM_STPL) {
		fl::CFileItemObtainer<store::CGoods, store::CStaple> ostpl(store::goods);
		const store::CStaple &stpl = ostpl.GetItem(fl::ioffset, iSource);// т.к. BaseItem() читает 2 файла: описание и рисунок
		// если ингр-т видимый, товар может быть скрытым, изменяемым или архивированным
		ASSERT(stpl.IsValid());
		// базовые идентификаторы
		iNm = stpl.m_iNm;
		iDn = stpl.m_iDn;
		iImg = stpl.m_iImg;
		fTara = stpl.m_fTara;	// объём тары товара на случай если товар штучный
		nCal = stpl.m_nCal;		// калорий
		iMu = stpl.m_iMu;		// е.и. ингредиента
		// цена товара в расходной е.и. за одну е.и. Открываю и закрываю файл с приходом
		fPrc = store::income.GetLast(ing.m_iSource).m_fCost;
	} else {
		ASSERT(ing.m_flags & ITM_PRDCT);
		fl::CFileItemObtainer<store::CProducts, store::CProduct> op(store::prdcts);
		const store::CProduct &p = op.GetItem(fl::ioffset, iSource);
		// если ингредиент действительный, продукт может быть скрытым или изменяемым, но действительным
		ASSERT(p.IsValid());
		iNm = p.m_iNm;
		iDn = p.m_iDn;
		iImg = p.m_iImg;
		iMu = p.m_iMu; // е.и. ингредиента
		store::CProduct::CGen data;
		fTara = p.Data(data).fOut;	// выход в е.и. продукта
		fPrc = data.fCost / fTara;	// цена ингредиента за е.и. продукта
		// калорий за fTara = data.nCal
		// калорий за 100 гр. = 100 * data.nCal / fTara; но 100 гр. нужно привести к е.и. продукта
		nCal = 0;
		if (!DCMP_EQUAL(fTara, .0, .0001)) {
			const store::CMu &mu1gr = o::opt.m_amu[o::iMuGr];
			nCal = (int) (mu1gr.ConvWeight(100.0, o::opt.m_amu[iMu]) * data.nCal / fTara);
		}
	}// базовые сведения об ингредиенте
	CBaseItem::Set(ing.m_tmc, ing.m_tme, ing.m_flags, iObj, ing.m_iPrdct, iNm, iDn, iImg, tp);
	CBaseItem::SetToSend(uImg);
	ASSERT(flgs & ITM_STPL || iPrnt >= 0);
	ASSERT((sNm.m_flags & ITM_MNMS) == ITM_STPL || (sNm.m_flags	& ITM_MNMS) == ITM_PRDCT);
	fQtt = ing.m_fQtt;// к-во товара или продукта в е.и. ингр-та
	fLd = ing.m_fLoad;// % наценки
	fLs = ing.m_fLoss;// % потерь
	ASSERT((flgs & ITM_MNMS) == (sNm.m_flags & ITM_MNMS));
	return true;
}

int32_t CIngredient::SaveToBase(int16_t iind, fl::CFileItem &fi)
{//	наименование и описание товара тут не сохраняю, только ингредиент
	int32_t i;
	Get(i, fi);
	ASSERT(fi.IsValid());
	i = fl::a::SetItem(store::ings, CAST(fl::CFileItem, store::CIngredient, fi), iind, iObj);
	if (i < 0) {
		ASSERT(iPrnt >= 0);
		fl::a::RmSubitem<store::CProducts, store::CProduct>(store::prdcts, iPrnt, tp, iObj);
	}
	return (iObj = i);
}

#endif // defined __ITOG

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::cmp_nm_tp сортирую ингредиенты по типу и наименованию
/// ингр-т может быть
/// ITM_INGTADD дополнительным
/// ITM_INGTALT альтернативным
/// ITM_INGTSEL выбранным
/// ITM_INGTREP основным
/// \return -1|0|1
///
int CIngredient::cmp_flgs(const CIngredient &a, const CIngredient &z)
{	// если оба ингр-та базовые
	if ((a.IsBas() || a.IsSel()) && (z.IsBas() || z.IsSel()))
		return 0;
	// если оба ингр-та альтернативные
	if (a.IsAlt() && z.IsAlt()) {
		if (a.idBas != z.idBas)
			return ((a.idBas < z.idBas) ? -1 : 1);
		return ((a.IsRoot() == z.IsRoot()) ? 0 : a.IsRoot() ? -1 : 1);
	} // если оба ингр-та добавочные
	if (a.IsAdd() && z.IsAdd())
		return 0;
	// если один из ингр-тов базовый
	if (a.IsBas())
		return -1; // а "меньше" z, т.е. а выше в списке
	if (z.IsBas())
		return 1; // a "больше" z, т.е. а ниже в списке
	// один из ингр-тов альт. другой - доп.
	if (a.IsAlt())
		return -1; // а "меньше" z - выше в списке
	ASSERT(z.IsAlt());
	return 1; // a "больше" z - ниже в списке
}

CBaseItem& CIngredient::Set(int32_t iItm, const fl::CFileItem &fl)
{
	ASSERT((fl.m_flags & ITM_MNMS) == ITM_STPL || (fl.m_flags & ITM_MNMS) == ITM_PRDCT);
	const store::CIngredient *ps = CAST(const fl::CFileItem, const store::CIngredient, &fl);
	ASSERT(ps != nullptr && tp == fl::CBaseObj::ingrdt);
	CBaseItem::Set(iItm, fl);
	fLd = ps->m_fLoad;
	fLs = ps->m_fLoss;
	fQtt = ps->m_fQtt;
	iSource = ps->m_iSource;
	iMu = ps->m_iMu;
	idBas = ps->m_idBas;
	iPrnt = ps->m_iPrdct;
	ASSERT(flgs & ITM_STPL || iPrnt >= 0);
	return *this;
}

const CIngredient& CIngredient::operator =(const CStaple &s)
{
	if (!(flgs & ITM_STPL) || iSource != s.iObj) {
		tmEdit = tmCrtd = 0;
		flgs = ITM_STPL;
		iObj = INV32_BIND;
//		iPrnt - продукт остаётся прежним
	}
	iNm = s.iNm;
	iDn = s.iDn;
	iImg = s.iImg;
	tp = fl::CBaseObj::ingrdt;
	sNm = s.sNm;
	ASSERT((sNm.m_flags & ITM_MNMS) == ITM_STPL);
	sDn = s.sDn;
	img = s.img;
	// CIngredient
	fLd = .0;
	fQtt = fLs = 0.f;
	fPrc = s.fPrc;
	fTara = s.fTara;
	nCal = s.nCal;
	iSource = s.iObj;
	iMu = s.iMu;
	idBas.tp = fl::CBaseObj::ndef;
	idBas.iSrc = INV32_BIND;
	return *this;
}

const CIngredient& CIngredient::operator =(const CProduct &p)
{
	if (!(flgs & ITM_PRDCT) || iSource != p.iObj) {
		tmEdit = tmCrtd = 0;
		flgs = ITM_PRDCT;
		iObj = INV32_BIND;
		// iPrnt... продукт остаётся прежним
	}
	iNm = p.iNm;
	iDn = p.iDn;
	iImg = p.iImg;
	tp = fl::CBaseObj::ingrdt;
	sNm = p.sNm;
	ASSERT((sNm.m_flags & ITM_MNMS) == ITM_PRDCT);
	sDn = p.sDn;
	img = p.img;
	// CIngredient
	fLd = .0;
	fLd = fLs = 0.f;
	fTara = p.Out();		 // выход в е.и. продукта
	fPrc = p.Cost() / fTara; // цена ингредиента за е.и. продукта
	nCal = p.Cal100();		 // калорий на 100 грамм продукта
	iSource = p.iObj;
	iMu = p.iMu;
	idBas.tp = fl::CBaseObj::ndef;
	idBas.iSrc = INV32_BIND;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIngredient::GetFlItem возвращает файловый объект
/// \param o: ингредиент для записи в таблицу store::ings
/// \return
///
fl::CFileItem& CIngredient::Get(int32_t &iItm, fl::CFileItem &fi) const
{
	ASSERT((flgs & ITM_MNMS) == ITM_STPL || (flgs & ITM_MNMS) == ITM_PRDCT);// см. оператор =
	CBaseItem::Get(iItm, fi);
	store::CIngredient *ps = CAST(fl::CFileItem, store::CIngredient, &fi);
	ASSERT(ps != nullptr && tp == fl::CBaseObj::ingrdt);
	ps->m_fLoad = fLd;
	ps->m_fQtt = fQtt;
	ps->m_fLoss = fLs;
	ps->m_iSource = iSource;
	ps->m_iPrdct = iPrnt;
	ps->m_iMu = iMu;
	ps->m_idBas = idBas;
	return fi;
}

int32_t CIngredient::GetBinData(BYTES &ab) const
{
	const int32_t c = ab.GetCount();
	CBaseItem::GetBinData(ab);
	ab.AddND((CBYTE*) &fLd, SZ_ISCINGT);
	return (ab.GetCount() - c);
}

int32_t CIngredient::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = CBaseItem::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &fLd, SZ_ISCINGT, cpb + i, cnt - i));
}

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::CProduct
/// \param iPrdct: базовый ид-р по индексу fl::ioffset
///
inline CProduct::CProduct(int32_t iPrdct) // iPrdct = INV32_BIND
	: CBaseItemComplex(fl::CBaseObj::product, iPrdct)
	, fLd(.0)
	, fRnd(1.f)
	, iMu(INV32_BIND)
{
#if defined(__ITOG) || defined(__STRFLA)
	fRnd = o::opt.m_fRndTo;
#else
	fRnd = o::opt.m_oiTg.m_fRndTo;
#endif
}

#ifdef __ITOG

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::SetToSend иницыирую продукт для отправки клиенту
/// \param fi: файловый продукт store::CProduct
/// \param iID: базовый ид-р продукта (ввод)
/// \param uImg: размер рисунка продукта
/// \param uSbimg: размер рисунка ингредиента
///
void CProduct::SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg)
{
	BAS::SetToSend(fi, iID, uImg, uSbimg);
	store::CProduct *pp = CAST(fl::CBaseObj, store::CProduct, &fi);
	ASSERT(nullptr != pp);
	fLd = pp->m_fLoad;
	fRnd = pp->m_fRnd;
	iMu = pp->m_iMu;
}

#endif // defined __ITOG

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::Set присваиваю этому объекту значения файлового объекта store::CProduct
/// \param iItm: базовый ид-р продукта
/// \param fl: файловый объект
/// \return *this
///
CBaseItem& CProduct::Set(int32_t iItm, const fl::CFileItem &fl)
{
	const store::CProduct *pp = CAST(const fl::CFileItem, const store::CProduct, &fl);
	ASSERT(pp != nullptr && tp == fl::CBaseObj::product);
	BAS::Set(iItm, fl);
	fLd = pp->m_fLoad;
	fRnd = pp->m_fRnd;
	iMu = pp->m_iMu;
	ASSERT(!DCMP_EQUAL(fRnd, 0.f, FLT_EPSILON));
	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::Get извлекаю из этого продукта файловый объект для записи в базу
/// \param iItm: базовый ид-р продукта может быть не известен
/// \param p: файловый продукт
/// \return *this
///
fl::CFileItem& CProduct::Get(int32_t &iItm, fl::CFileItem &p) const
{
	store::CProduct *pp = CAST(fl::CFileItem, store::CProduct, &p);
	ASSERT(pp != nullptr && tp == fl::CBaseObj::product);
	BAS::Get(iItm, p);
	pp->m_fLoad = fLd;
	pp->m_fRnd = fRnd;
	pp->m_iMu = iMu;
	ASSERT(!DCMP_EQUAL(pp->m_fRnd, 0.f, FLT_EPSILON));
	return p;
}

int32_t CProduct::GetBinData(BYTES &ab) const
{
	cint32_t c = ab.GetCount();
	BAS::GetBinData(ab);
	ab.AddND((CBYTE*) &fLd, SZ_ISCPRDCT);
	return (ab.GetCount() - c);
}

int32_t CProduct::SetBinData(CBYTE *cpb, const int32_t cnt)
{
	int32_t i = BAS::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &fLd, SZ_ISCPRDCT, cpb + i, cnt - i));
}

const CProduct& CProduct::operator =(const CProduct &o)
{
	BAS::operator =(o);
	fLd = o.fLd;
	fRnd = o.fRnd;
	iMu = o.iMu;
	return *this;
}

CProduct& CProduct::operator =(CProduct &&o)
{
	BAS::operator =(o);
	fLd = o.fLd;
	fRnd = o.fRnd;
	iMu = o.iMu;
	return *this;
}

double CProduct::Cost() const
{
	double f = 0.0;
	const CIngredient *pi = nullptr;
	const CBaseItem *pbi = nullptr;
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++) {
		pbi = apSbitms[i];
		pi = CAST(const isc::CBaseItem, const isc::CIngredient, pbi);
		if (pi->flgs & (ITM_INGTALT|ITM_INGTADD))
			continue;
		ASSERT(pi);
		f += pi->Cost();
	}
	return f;
}

double CProduct::Price() const
{
	ASSERT(!DCMP_EQUAL(fRnd, 0.F, FLT_EPSILON));
	double f = Cost();
	return glb::Round(f + f * fLd, (double) fRnd);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::Cal калорий в продукте с учётом веса (выхода) и к-ва калорий каждого ингредиента
/// \return калорий в продукте
///
int CProduct::Cal() const
{
	double f = 0.0;
	const CIngredient *ping = nullptr;
	const CBaseItem *pbi = nullptr;
#ifdef __ITOG
	const glb::CFxArray<store::CMu, true> &amu = o::opt.m_amu;
	store::CMu &muHg = o::opt.m_amu[o::iMuHg];
#elif defined __ITGCLNT
	const glb::CFxArray<store::CMu, true> &amu = o::opt.m_oiTg.m_amu;
	store::CMu &muHg = o::opt.m_oiTg.m_amu[o::iMuHg];
#endif
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++) {
		// калорий в ингр-те на 100 грамм 216, выход ингр-та с учётом потерь 0,8 кг.
		// 1000 * 0.8 / 100 = 8 выход ингр-та к е.и. калорий (их на 100 гр.)
		// 8 * 216 = 1728
		pbi = apSbitms[i];
		ping = CAST(const isc::CBaseItem, const isc::CIngredient, pbi);
		if (ping->flgs & (ITM_INGTALT|ITM_INGTADD))
			continue;
		if (ping->nCal > 0)
			f += amu[ping->iMu].ConvWeight(ping->Weight(), muHg) * ping->nCal;
	}
	return f;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::Cal100
/// \return калорий на 100 грамм продукта
///
int CProduct::Cal100() const
{
	int c = 0;
	const CIngredient *ping = nullptr;
	const CBaseItem *pbi = nullptr;
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++) {
		pbi = apSbitms[i];
		ping = CAST(const isc::CBaseItem, const isc::CIngredient, pbi);
		if (ping->flgs & (ITM_INGTALT|ITM_INGTADD))
			continue;
		if (ping->nCal > 0)
			c += ping->nCal;
	}
	return c;
}

double CProduct::Out() const
{
	double f = 0.0;
	const CIngredient *ping = nullptr;
	const CBaseItem *pbi = nullptr;
#if defined __ITOG || defined __STRFLARRAY
	// е.и. должна быть определена
	ASSERT(iMu >= 0);
	const glb::CFxArray<store::CMu, true> &amu = o::opt.m_amu;
	const store::CMu *pMuPrdct = &amu[(iMu < 0) ? o::opt.m_iPrdctMU : iMu];
#elif defined __ITGCLNT
	const glb::CFxArray<store::CMu, true> &amu = o::opt.m_oiTg.m_amu;
	const store::CMu *pMuPrdct = &amu[(iMu < 0) ? o::opt.m_oiTg.m_iPrdctMU : iMu];
#endif
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++) {
		pbi = apSbitms[i];
		ping = CAST(const isc::CBaseItem, const isc::CIngredient, pbi);
		if (ping->flgs & (ITM_INGTALT|ITM_INGTADD))
			continue;
		ASSERT(pMuPrdct);
		f += amu[ping->iMu].ConvWeight(ping->Weight(), *pMuPrdct);
	}
	return f;
}

double CProduct::Load() const
{
	const CIngredient *ping = nullptr;
	const CBaseItem *pbi = nullptr;
	double fSum = .0;
	int i, n;
	for (i = 0, n = apSbitms.GetCount(); i < n; i++) {
		pbi = apSbitms[i];
		ping = CAST(const isc::CBaseItem, const isc::CIngredient, pbi);
		if (ping->flgs & (ITM_INGTALT|ITM_INGTADD))
			continue;
		// стоимость за е.и. продукта умноженная на к-во
		fSum += ping->fLd;
	}
	return ((n > 0) ? fSum / n : .0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::FindIngredient ищу указанный ингредиент в массиве
/// \param uf: флаг наименования ITM_STLP или ITM_PRDCT
/// \param sNm: наименование ингредиента
/// \return указатель на найденный ингредиент
///
CIngredient* CProduct::FindIngredient(uint32_t uf, CBYTES &sNm)
{
	CBaseItem *pi = nullptr;
	int n1, n2;
	CCHAR *ps1, *ps2 = (CCHAR*) sNm.data(n2);
	for (int i = 0, n = apSbitms.GetCount(); i < n; i++) {
		pi = apSbitms[i];
		ASSERT(pi->tp == fl::CBaseObj::ingrdt);
		if (pi->sNm.m_flags & uf) {
			ps1 = (CCHAR*) pi->sNm.m_str.data(n1);
			if (0 == glb::cmp_iUtf8s(ps1, n1, ps2, n2))
				return CAST(isc::CBaseItem, isc::CIngredient, pi);
		}
	}
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CProduct::AddIngredient клиентские делишки: добавляю новый ингредиет, такой как товар
/// или продукт, или обновляю имеющийся
/// \param ping: указатель на isc::CStaple или isc::CProduct
/// \return индекс ингредиента в массиве подэлементов
///
/// ф-ция устарела, ею нельзя пользоваться с тех пор, как появились альтернативные и дополнительные
/// ингредиенты. Они должы быть отсортированы по индексу isc::CIngredient::cmp_flgsnmi().
/// см. ui::c::CScrlPrdctIngs::m_aings, ui::c::CScrlPrdctIngs::Ingredients(glb::CFxArray<isc::CBaseItem*, true>&)
/// и ui::v::CProduct::SetIngredients()
///
/*int CProduct::AddIngredient(const CBaseItem *pbi)
{
	const CProduct *pp = nullptr;
	const CStaple *ps = nullptr;
	CIngredient *ping = new CIngredient(pbi->iObj);
	switch (pbi->tp) {
	case fl::CBaseObj::staple:
		ps = CAST(const isc::CBaseItem, const isc::CStaple, pbi);
		ASSERT(ps);
		*ping = *ps;
		break;
	case fl::CBaseObj::product:
		pp = CAST(const isc::CBaseItem, const isc::CProduct, pbi);
		ASSERT(pp);
		*ping = *pp;
		break;
	default:
		ASSERT(false);
	}
	int i, n = apSbitms.GetCount();
	for (i = 0; i < n; i++)
		if (apSbitms[i]->sNm.cmpi(ping->sNm) > 0)
			break;
	apSbitms.Insert(i, ping);
	return i;
}*/

int CProduct::cmp_prc(const CBaseItem *const &a, const CBaseItem *const &z)
{
	const CBaseItem *PA = a, *PZ = z;
	const CProduct *pa = CAST(const isc::CBaseItem, const isc::CProduct, PA),
		*pz = CAST(const CBaseItem, const CProduct, PZ);
	ASSERT(pa && pz);
	double fa = pa->Price(), fz = pz->Price(),
#ifdef __ITGCLNT
		   fe = pow(10, -((int) o::opt.m_oiTg.m_cPrcn + 1));
#elif defined __ITOG
		   fe = pow(10, -((int) o::opt.m_cPrcn + 1));
#endif
	return DCMP_LESS(fa, fz, fe) ? -1 : DCMP_MORE(fa, fz, fe) ? 1 : 0;
}

#ifdef __ITOG

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::SetToSend готовлю к отправке клиенту товар для представления. Сейчас
/// извлекаю эл-ты из файла товаров по индексу iGdsCatNmi, поэтому и тут не базовый.
/// \param ps: товар из файла с товарами
/// \param uImg: размер изображения товара для представления, если требуется. Может быть нуль.
/// \param uSbimg: размер рисунка подэлемента здесь не используется
///
bool CStaple::SetToSend(uint32_t uImg, uint32_t/*uSbimg*/, bool bVisible) // uSbimg = 0, bVisible = true
{
	ASSERT(iObj >= 0 && tp == fl::CBaseObj::staple);
	fl::CFileItemObtainer<store::CGoods, store::CStaple> ostpl(store::goods);
	const store::CStaple &stpl = ostpl.GetItem(fl::ioffset, iObj);
	if (stpl.m_flags & ITM_DEL || (bVisible && stpl.m_flags & (ITM_EDIT|ITM_HDN|ITM_ARC)))
		return false;
	CBaseItem::Set(stpl.m_tmc, stpl.m_tme, stpl.m_flags, iObj, INV32_BIND, stpl.m_iNm,
				   stpl.m_iDn, stpl.m_iImg, tp);
	CBaseItem::SetToSend(uImg);// наименование, описание, рисунок
	// тара артикул калорий и категория
	fTara = stpl.m_fTara;
	iArt = stpl.m_iArtl;
	nCal = stpl.m_nCal;
	uCat = stpl.m_nc;
	// стоимость последнего прихода за е.и. товара
	fPrc = store::income.GetLast(iObj).m_fCost;
	// строки для артикула и категорий
	fl::a::COpener<fl::CStrings> os(fl::strs);
	// е.и.
	iMu = stpl.m_iMu;
	// артикул
	if (iArt >= 0)
		fl::strs.GetItem(fl::ioffset, iArt, sArtl);
	// категории
	o::opt.CatStr(stpl.m_nc, sCat);
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStaple::SaveToBase сохраняет товар в базе по правилам целостности индексированного
/// массива и пустого эл-та.
/// \return базовый ид-р товара в store::goods
///
int32_t CStaple::SaveToBase(int16_t iind, fl::CFileItem &fi)
{
	CBaseItem::SaveToBase(fl::ioffset, fi);
	iArt = fl::a::SetItem(fl::strs, sArtl, iind, iArt);
	Get(iObj, fi);
	// патриарха у товара нет. В приходе есть получатель и отправитель.
	ASSERT(iPrnt < 0);
	return (iObj = fl::a::SetItem(store::goods, CAST(fl::CFileItem, store::CStaple, fi), iind, iObj));
}

#endif // defined __ITOG

CBaseItem& CStaple::Set(int32_t iItm, const fl::CFileItem &fl)
{
	const store::CStaple *ps = CAST(const fl::CFileItem, const store::CStaple, &fl);
	ASSERT(ps != nullptr && tp == fl::CBaseObj::staple);
	CBaseItem::Set(iItm, fl);
	fTara = ps->m_fTara;
	iArt = ps->m_iArtl;
	iMu = ps->m_iMu;
	uCat = ps->m_nc;
	nCal = ps->m_nCal;
	// CBaseItem не может это получить от fl::CFileItem т.к. store::CStaple не наследует fl::CBaseObj
	iObj = iItm;
	iNm = ps->m_iNm;
	iDn = ps->m_iDn;
	iImg = ps->m_iImg;
	return *this;
}

fl::CFileItem& CStaple::Get(int32_t &iItm, fl::CFileItem &s) const
{
	store::CStaple *ps = CAST(fl::CFileItem, store::CStaple, &s);
	ASSERT(ps && tp == fl::CBaseObj::staple);
	s.m_flags = flgs & ITM_SRVM; // флаги с сервера не для представлений
	s.m_nref = 0;
	s.m_nusr = 0;
	s.m_tmc = tmCrtd;
	s.m_tme = tmEdit;
	ps->m_fTara = fTara;
	ps->m_iArtl = iArt;
	ps->m_iDn = iDn;
	ps->m_iImg = iImg;
	ps->m_iMu = iMu;
	ps->m_iNm = iNm;
	ps->m_nc = uCat;
	ps->m_nCal = nCal;
	iItm = iObj;
	return s;
}

#ifdef __ITGCLNT

glb::CFxString CStaple::Name() const
{
	ASSERT(iMu != INV32_BIND);
	glb::CFxString s(sNm.m_str);
	if (fTara)
		s.AddFrmt(64, " (%g %s)", fTara, (CCHAR*) fl::String(o::opt.m_oiTg.m_amu[iMu].m_iShrtNm));
	return s;
}

glb::CFxString& CStaple::Name(glb::CFxString &s) const
{
	ASSERT(iMu != INV32_BIND);
	if (fTara)
		s.SetFrmt(sNm.m_str.GetCount() + 64, "%s (%g %s)", sNm.str(), fTara,
					(CCHAR*) fl::String(o::opt.m_oiTg.m_amu[iMu].m_iShrtNm));
	else
		s = sNm.m_str;
	return s;
}
#endif // defined __ITGCLNT

const CStaple& CStaple::operator =(const CStaple &o)
{
	CBaseItem::operator =(o);
	ASSERT(&o != (CStaple*) nullptr);
	fTara = o.fTara;
	fPrc = o.fPrc;
	iArt = o.iArt;
	uCat = o.uCat;
	nCal = o.nCal;
	sArtl = o.sArtl;
	sCat = o.sCat;
	iMu = o.iMu;
	return *this;
}

int32_t CStaple::GetBinData(BYTES &ab) const
{
	cint32_t c = ab.GetCount();
	CBaseItem::GetBinData(ab);
	ab.AddND((CBYTE*) &fTara, SZ_ISCSTPL);
	sArtl.GetBinData(ab);
	sCat.GetBinData(ab);
	return (ab.GetCount() - c);
}

int32_t CStaple::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = CBaseItem::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &fTara, SZ_ISCSTPL, cpb + i, cnt - i);
	i += sArtl.SetBinData(cpb + i, cnt - i);
	return (i + sCat.SetBinData(cpb + i, cnt - i));
}

CStaple& CStaple::operator =(CStaple &&o)
{
	CBaseItem::operator =(o);
	fTara = o.fTara;
	fPrc = o.fPrc;
	iArt = o.iArt;
	uCat = o.uCat;
	nCal = o.nCal;
	fl::CFileItem &fi = CAST(fl::CString, fl::CFileItem, o.sArtl);
	ASSERT(&fi != (fl::CFileItem*) nullptr);
	CAST(fl::CString, fl::CFileItem, sArtl) = fi;
	sArtl.m_str.Attach(o.sArtl.m_str);
	sCat.Attach(o.sCat);
	iMu = o.iMu;
	return *this;
}

CPostAddress::CPostAddress(const CPostAddress &o)
	: cafe::CPostAdrs(o)
	, m_iObj(o.m_iObj)
	, m_sCountry(o.m_sCountry)	// название страны
	, m_sRegion(o.m_sRegion)	// название области
	, m_sToun(o.m_sToun)		// название населённого пункта
	, m_sDistr(o.m_sDistr)		// название района
	, m_sIndex(o.m_sIndex)		// почтовый индексом
	, m_sStreet(o.m_sStreet)	// улица
	, m_sBuilding(o.m_sBuilding)// номер строения
	, m_sBldgNm(o.m_sBldgNm)	// наименование строения, как общяга, ЖК...
	, m_sFlat(o.m_sFlat)		// номер квартиры/комнаты
{

}

CPostAddress::CPostAddress(CPostAddress &&o)
	: cafe::CPostAdrs(o)
	, m_iObj(o.m_iObj)
	, m_sCountry(o.m_sCountry)	// название страны
	, m_sRegion(o.m_sRegion)	// название области
	, m_sToun(o.m_sToun)		// название населённого пункта
	, m_sDistr(o.m_sDistr)		// название района
	, m_sIndex(o.m_sIndex)		// почтовый индексом
	, m_sStreet(o.m_sStreet)	// улица
	, m_sBuilding(o.m_sBuilding)// номер строения
	, m_sBldgNm(o.m_sBldgNm)	// наименование строения, как общяга, ЖК...
	, m_sFlat(o.m_sFlat)		// номер квартиры/комнаты
{

}

const CPostAddress& CPostAddress::operator =(const CPostAddress &o)
{
	reinterpret_cast<cafe::CPostAdrs&>(*this) = o;
	m_iObj = o.m_iObj;
	m_sCountry = o.m_sCountry;	// название страны
	m_sRegion = o.m_sRegion;	// название области
	m_sToun = o.m_sToun;		// название населённого пункта
	m_sDistr = o.m_sDistr;		// название района
	m_sIndex = o.m_sIndex;		// почтовый индексом
	m_sStreet = o.m_sStreet;	// улица
	m_sBuilding = o.m_sBuilding;// номер строения
	m_sBldgNm = o.m_sBldgNm;	// наименование строения, как общяга, ЖК...
	m_sFlat = o.m_sFlat;		// номер квартиры/комнаты
	return *this;
}

void CPostAddress::Init()
{
	cafe::CPostAdrs::Init();
	m_iObj = INV32_BIND;
	m_sCountry.Init();
	m_sRegion.Init();
	m_sToun.Init();
	m_sDistr.Init();
	m_sIndex.Init();
	m_sStreet.Init();
	m_sBuilding.Init();
	m_sBldgNm.Init();
	m_sFlat.Init();
}

#ifdef __ITOG

void CPostAddress::SetToSend(int32_t iObj, const cafe::CPostAdrs &pa)
{
	Init();
	m_iObj = iObj;
	cafe::CPostAdrs::operator =(pa);
	fl::a::COpener<fl::CStrings> os(fl::strs);
	if (m_iCountry >= 0)
		SZ(m_iCountry, m_sCountry);
	if (m_iRegion >= 0)
		SZ(m_iRegion, m_sRegion);
	if (m_iToun >= 0)
		SZ(m_iToun, m_sToun);
	if (m_iDistr >= 0)
		SZ(m_iDistr, m_sDistr);
	if (m_iIndex >= 0)
		SZ(m_iIndex, m_sIndex);
	if (m_iStreet >= 0)
		SZ(m_iStreet, m_sStreet);
	if (m_iBuilding >= 0)
		SZ(m_iBuilding, m_sBuilding);
	if (m_iBldgNm >= 0)
		SZ(m_iBldgNm, m_sBldgNm);
	if (m_iFlat >= 0)
		SZ(m_iFlat, m_sFlat);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CPostAddress::SaveToBase записывает почтовый адрес в файловый массив с учётом правил
/// целостности уник.индексов файлового массива и пустого объекта. См. fl::CFileItem::IsEmpty()
/// и файловые массивы fl::a::CvBase и fl::a::CiShrd
/// \return базовый ид-р этого объекта
///
int32_t CPostAddress::SaveToBase(int16_t iind)
{
	fl::a::COpener<fl::CStrings> os(fl::strs);
	m_iCountry = fl::a::SetItem(fl::strs, m_sCountry, fl::ioffset, m_iCountry);
	m_iRegion = fl::a::SetItem(fl::strs, m_sRegion, fl::ioffset, m_iRegion);
	m_iToun = fl::a::SetItem(fl::strs, m_sToun, fl::ioffset, m_iToun);
	m_iDistr = fl::a::SetItem(fl::strs, m_sDistr, fl::ioffset, m_iDistr);
	m_iIndex = fl::a::SetItem(fl::strs, m_sIndex, fl::ioffset, m_iIndex);
	m_iStreet = fl::a::SetItem(fl::strs, m_sStreet, fl::ioffset, m_iStreet);
	m_iBuilding = fl::a::SetItem(fl::strs, m_sBuilding, fl::ioffset, m_iBuilding);
	m_iBldgNm = fl::a::SetItem(fl::strs, m_sBldgNm, fl::ioffset, m_iBldgNm);
	m_iFlat = fl::a::SetItem(fl::strs, m_sFlat, fl::ioffset, m_iFlat);
	os.Close();
	return (m_iObj = fl::a::SetItem(cafe::adrss, *this, iind, m_iObj));
}

#endif // defined __ITOG

int32_t CPostAddress::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int i = cafe::CPostAdrs::SetBinData(cpb, cnt);
	i += m_sCountry.SetBinData(cpb + i, cnt - i);
	i += m_sRegion.SetBinData(cpb + i, cnt - i);
	i += m_sToun.SetBinData(cpb + i, cnt - i);
	i += m_sDistr.SetBinData(cpb + i, cnt - i);
	i += m_sIndex.SetBinData(cpb + i, cnt - i);
	i += m_sStreet.SetBinData(cpb + i, cnt - i);
	i += m_sBuilding.SetBinData(cpb + i, cnt - i);
	i += m_sBldgNm.SetBinData(cpb + i, cnt - i);
	return (i + m_sFlat.SetBinData(cpb + i, cnt - i));
}

int32_t CPostAddress::GetBinData(BYTES &ab)
{
	const int n = ab.GetCount();
	cafe::CPostAdrs::GetBinData(ab);
	m_sCountry.GetBinData(ab);
	m_sRegion.GetBinData(ab);
	m_sToun.GetBinData(ab);
	m_sDistr.GetBinData(ab);
	m_sIndex.GetBinData(ab);
	m_sStreet.GetBinData(ab);
	m_sBuilding.GetBinData(ab);
	m_sBldgNm.GetBinData(ab);
	m_sFlat.GetBinData(ab);
	return (ab.GetCount() - n);
}

CPostAddress& CPostAddress::operator =(CPostAddress &&o)
{
	cafe::CPostAdrs::operator=(o);
	m_sCountry.Attach(o.m_sCountry);
	m_sRegion.Attach(o.m_sRegion);
	m_sToun.Attach(o.m_sToun);
	m_sDistr.Attach(o.m_sDistr);
	m_sIndex.Attach(o.m_sIndex);
	m_sStreet.Attach(o.m_sStreet);
	m_sBuilding.Attach(o.m_sBuilding);
	m_sBldgNm.Attach(o.m_sBldgNm);
	m_sFlat.Attach(o.m_sFlat);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CContractor::CContractor
/// \param iCntrctr базовый ид-р по индексу fl::ioffset
///
CContractor::CContractor(int32_t iCntrctr) // iCntrctr = INV32_BIND
	: CBaseItem(fl::CBaseObj::cntrctr)
{
	iBisID = iBnkID = iBnkNm = iAcnt = iAggrt = INV32_BIND;
	iCntr = 0xff;
	iObj = iCntrctr;
}

CContractor::CContractor(fl::CBaseObj::Type tp, int32_t iCntrctr)
	: CBaseItem(tp)
{
	iBisID = iBnkID = iBnkNm = iAcnt = iAggrt = INV32_BIND;
	iCntr = 0xff;
	iObj = iCntrctr;
}

CContractor::CContractor(const CContractor &o)
	: CBaseItem(o)
	, iBisID(o.iBisID)
	, iBnkID(o.iBnkID)
	, iBnkNm(o.iBnkNm)
	, iAcnt(o.iAcnt)
	, iAggrt(o.iAggrt)
	, iCntr(o.iCntr)
	, sBisID(o.sBisID)
	, sBnkID(o.sBnkID)
	, sBnkNm(o.sBnkNm)
	, sAcnt(o.sAcnt)
	, aiAdrs(o.aiAdrs)
	, aiTel(o.aiTel)
	, aiRefs(o.aiRefs)
	, asTel(o.asTel)
	, asRefs(o.asRefs)
	, aAdrs(o.aAdrs)
	, sAggrt(o.sAggrt)
{

}

CContractor::CContractor(CContractor &&o)
	: CBaseItem(o)
	, iBisID(o.iBisID)
	, iBnkID(o.iBnkID)
	, iBnkNm(o.iBnkNm)
	, iAcnt(o.iAcnt)
	, iAggrt(o.iAggrt)
	, iCntr(o.iCntr)
	, sBisID(o.sBisID)
	, sBnkID(o.sBnkID)
	, sBnkNm(o.sBnkNm)
	, sAcnt(o.sAcnt)
	, aiAdrs(o.aiAdrs)
	, aiTel(o.aiTel)
	, aiRefs(o.aiRefs)
	, asTel(o.asTel)
	, asRefs(o.asRefs)
	, aAdrs(o.aAdrs)
	, sAggrt(o.sAggrt)
{

}

const CContractor& CContractor::operator =(const CContractor &o)
{
	CBaseItem::operator =(o);
/*	const CContractor &c = CAST(const CBaseItem, const CContractor, o);
	ASSERT(&c != (CContractor*) nullptr);*/
	memcpy(&iBisID, &o.iBisID, SZ_ISC_CONTRR);
	sBisID = o.sBisID;
	sBnkID = o.sBnkID;
	sBnkNm = o.sBnkNm;
	sAcnt = o.sAcnt;
	aiAdrs = o.aiAdrs;
	aiTel = o.aiTel;
	aiRefs = o.aiRefs;
	asTel = o.asTel;
	asRefs = o.asRefs;
	aAdrs = o.aAdrs;
	sAggrt = o.sAggrt;
	return *this;
}

CContractor& CContractor::operator =(CContractor &&o)
{
	CBaseItem::operator =(o);
/*	CContractor &c = CAST(CBaseItem, CContractor, o);
	ASSERT(&c != (CContractor*) nullptr);*/
	memcpy(&iBisID, &o.iBisID, SZ_ISC_CONTRR);
	sBisID.Attach(o.sBnkID);
	sBnkID.Attach(o.sBnkID);
	sBnkNm.Attach(o.sBnkNm);
	sAcnt.Attach(o.sAcnt);
	aiAdrs.Attach(o.aiAdrs);
	aiTel.Attach(o.aiTel);
	aiRefs.Attach(o.aiRefs);
	asTel.Attach(o.asTel);
	asRefs.Attach(o.asRefs);
	aAdrs.Attach(o.aAdrs);
	sAggrt.Attach(o.sAggrt);
	return *this;
}

int32_t CContractor::GetBinData(BYTES &ab) const
{
	cint32_t c = ab.GetCount();
	CBaseItem::GetBinData(ab);
	ab.AddND((BYTE*) &iBisID, SZ_ISC_CONTRR);
	sBisID.GetBinData(ab);
	sBnkID.GetBinData(ab);
	sBnkNm.GetBinData(ab);
	sAcnt.GetBinData(ab);
	sAggrt.GetBinData(ab);
	aiAdrs.GetBinData(ab);
	aiTel.GetBinData(ab);
	aiRefs.GetBinData(ab);
	asTel.GetBinData(ab);
	asRefs.GetBinData(ab);
	aAdrs.GetBinData(ab);
	return (ab.GetCount() - c);
}

int32_t CContractor::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = CBaseItem::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &iBisID, SZ_ISC_CONTRR, cpb + i, cnt - i);
	i += sBisID.SetBinData(cpb + i, cnt - i);
	i += sBnkID.SetBinData(cpb + i, cnt - i);
	i += sBnkNm.SetBinData(cpb + i, cnt - i);
	i += sAcnt.SetBinData(cpb + i, cnt - i);
	i += sAggrt.SetBinData(cpb + i, cnt - i);
	i += aiAdrs.SetBinData(cpb + i, cnt - i);
	i += aiTel.SetBinData(cpb + i, cnt - i);
	i += aiRefs.SetBinData(cpb + i, cnt - i);
	i += asTel.SetBinData(cpb + i, cnt - i);
	i += asRefs.SetBinData(cpb + i, cnt - i);
	return (i + aAdrs.SetBinData(cpb + i, cnt - 1));
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::CIncome
/// \param iIncID: базовый ид-р по индексу fl::ioffset
/// \param o
///
CIncome::CIncome(int32_t iIncID)// iIncID = INV32_BIND
	: fTara(.0)
	, iIncm(iIncID)
	, iMu(INV32_BIND)
{

}

CIncome::CIncome(const CIncome &o)
	: store::CIncome(o)
	, sCwrkr(o.sCwrkr)
	, sSplr(o.sSplr)
	, sRcvr(o.sRcvr)
	, iMu(o.iMu)
	, fTara(o.fTara)
	, iIncm(o.iIncm)
{

}

CIncome::CIncome(CIncome &&o)
	: store::CIncome(o)
	, sCwrkr(o.sCwrkr)
	, sSplr(o.sSplr)
	, sRcvr(o.sRcvr)
	, iMu(o.iMu)
	, fTara(o.fTara)
	, iIncm(o.iIncm)
{

}

CIncome& CIncome::operator =(CIncome &&o)
{
	store::CIncome::operator=(o);
	sCwrkr = o.sCwrkr;
	sSplr = o.sSplr;
	sRcvr = o.sRcvr;
	iMu = o.iMu;
	fTara = o.fTara;
	iIncm = o.iIncm;
	return *this;
}

const CIncome& CIncome::operator =(const CIncome &o)
{
	store::CIncome::operator=(o);
	sCwrkr = o.sCwrkr;
	sSplr = o.sSplr;
	sRcvr = o.sRcvr;
	iMu = o.iMu;
	fTara = o.fTara;
	iIncm = o.iIncm;
	return *this;
}

int32_t CIncome::GetBinData(BYTES &ab) const
{
	int32_t n = ab.GetCount();
	store::CIncome::GetBinData(ab);
	sCwrkr.GetBinData(ab);
	sSplr.GetBinData(ab);
	sRcvr.GetBinData(ab);
	ab.AddND((CBYTE*) &fTara, SZ_ISC_INCOME);
	return (ab.GetCount() - n);
}

int32_t CIncome::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = store::CIncome::SetBinData(cpb, cnt);
	i += sCwrkr.SetBinData(cpb + i, cnt - i);
	i += sSplr.SetBinData(cpb + i, cnt - i);
	i += sRcvr.SetBinData(cpb + i, cnt - i);
	return (i + glb::read_raw((BYTE*) &fTara, SZ_ISC_INCOME, cpb + i, cnt - i));
}

#ifdef __ITGCLNT

glb::CFxString CIncome::Name() const
{
	glb::CFxString s(sStpl);
	if (fTara)
		s.AddFrmt(64, " (%g %s)", fTara, (CCHAR*) fl::String(o::opt.m_oiTg.m_amu[iMu].m_iShrtNm));
	return s;
}

glb::CFxString& CIncome::Name(glb::CFxString &s) const
{
	if (fTara)
		s.SetFrmt(sStpl.GetCount() + 64, "%s (%g %s)", (CCHAR*) sStpl, fTara,
				  (CCHAR*) fl::String(o::opt.m_oiTg.m_amu[iMu].m_iShrtNm));
	else
		s = sStpl;
	return s;
}

#elif defined __ITOG

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CContractor::Set присваивае значение базового ид-ра контрактника перед тем,
/// как читать базу, перед SetToSend() вызывай SetContractor(), или иначе присваивай
/// ид-р переменной CBaseItem::iObj
/// \param iCntrctr: базовый ид-р по индексу fl::ioffset
///
void CContractor::SetContractor(int32_t iCntrctr)
{
	ASSERT(iCntrctr >= 0 && tp == fl::CBaseObj::cntrctr);
	iObj = iCntrctr;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CIncome::SetToSend отправляю приход и наименования: товара, поставщика, получателя и имя
/// сотрудника который принял товар.
/// \param szImg
///
void CIncome::SetToSend()
{	// ид-ры строк
	int32_t iSnm,	// ид-р строки с фамилией сотрудника
			iNm,	// ид-р строки с именем сотрудника
			iSplr, iRcvr, iStpl;// поставщик, получатель, товар
	iSnm = iNm = iSplr = iRcvr = iStpl = INV32_BIND;
	// сотрудник
	if (m_iCwrkr >= 0) {
		fl::CFileItemObtainer<cafe::CCoworkers, cafe::CCoworker> oc(cafe::cwrkrs);
		const cafe::CCoworker &c = oc.GetItem(fl::ioffset, m_iCwrkr);
		iSnm = c.m_iSurNm;
		iNm = c.m_iNm;	// допускается отсутствие имени, а отчество тут не берётся
	}// отдел
	fl::CFileItemObtainer<cafe::CDepts, cafe::CDept> od(cafe::depts);
	// внутренний отправитель - отдел этого предприятия
	if (m_flags & ITM_INNER) {
		iRcvr = od.GetItem(fl::ioffset, m_iRcvr).m_iNm;
		iSplr = od.GetItem(fl::ioffset, m_iSplr).m_iNm;
	} else {// внешний отправитель/поставщик - контрактник
		fl::CFileItemObtainer<cafe::CContractors, cafe::CContractor> oc(cafe::cntrctrs);
		iSplr = oc.GetItem(fl::ioffset, m_iSplr).m_iNm;
		iRcvr = od.GetItem(fl::ioffset, m_iRcvr).m_iNm;
	} // товар
	fl::CFileItemObtainer<store::CGoods, store::CStaple> ostpl(store::goods);
	const store::CStaple &stpl = ostpl.GetItem(fl::ioffset, m_iStpl);
	iStpl = stpl.m_iNm;
	fTara = stpl.m_fTara;
	// расходная е.и.
	iMu = stpl.m_iMu;
	// все строки
	fl::CFileItemObtainer<fl::CStrings, fl::CString> ostrs(fl::strs);
	fl::CString &str = ostrs.obj();
	fl::a::COpener<fl::CStrings> os(fl::strs);
	sStpl = AB(iStpl, str);
	sSplr = AB(iSplr, str);
	sRcvr = AB(iRcvr, str);
	if (iSnm >= 0)
		sCwrkr = AB(iSnm, str);
	if (iNm >= 0) {
		if (iSnm >= 0)
			sCwrkr.AddND(" ", 1);
		sCwrkr += AB(iNm, str);
	}// е.и.
}

int32_t CIncome::SaveToBase(int16_t iind)
{	// отсюда нельзя менять строки, они не принадлежат этому объекту
	// поэтому по сути эта ф-ция не имеет смысла как и отправка isc-объекта клиентом серверу
	// нужно отправлять store-объект без строк
	return (iIncm = fl::a::SetItem(store::income, *this, iind, iIncm));
}

bool CContractor::SetToSend(uint32_t szImg, uint32_t/*uSbimg*/, bool bVisible) // uSbimg = 0, bVisible = true
{
	ASSERT(iObj >= 0 && (tp == fl::CBaseObj::cntrctr || tp == fl::CBaseObj::splr));
	fl::CFileItemObtainer<cafe::CContractors, cafe::CContractor> oContractors(cafe::cntrctrs);
	const cafe::CContractor &o = oContractors.GetItem(fl::ioffset, iObj);
	CBaseItem::Set(o.m_tmc, o.m_tme, o.m_flags, iObj, INV32_BIND, o.m_iNm, o.m_iDn, o.m_iImg, tp);
	if (o.m_flags & ITM_DEL || (bVisible && o.m_flags & (ITM_EDIT|ITM_HDN|ITM_ARC)))
		return false;
	CBaseItem::SetToSend(szImg);
	tmCrtd = o.m_tmc;
	iBisID = o.m_iBisID;
	iBnkID = o.m_iBnkID;
	iBnkNm = o.m_iBnkNm;
	iAcnt = o.m_iAcnt;
	iAggrt = o.m_iAggrt;
	iCntr = iObj;
	aiAdrs = o.m_aiAdrs;
	aiTel = o.m_aiTel;
	aiRefs = o.m_aiRefs;
	asTel.SetCountZeroND();
	asRefs.SetCountZeroND();
	aAdrs.SetCountZeroND();
	fl::a::COpener<fl::CStrings> opnrStrs(fl::strs);
	if (iBisID >= 0)
		AB(iBisID, sBisID);
	if (iBnkID >= 0)
		AB(iBnkID, sBnkID);
	if (iBnkNm >= 0)
		AB(iBnkNm, sBnkNm);
	if (iAcnt >= 0)
		AB(iAcnt, sAcnt);
	if (iAggrt >= 0)
		glb::GetFile<BYTES, BYTE>(sAggrt.m_str, SI_TXT, iAggrt, false);
	fl::CFileItemObtainer<cafe::CPostAdrss, cafe::CPostAdrs> opa(cafe::adrss);
	cafe::CPostAdrs &pa = opa.obj();
	fl::a::COpener<cafe::CPostAdrss> opnrPa(cafe::adrss);
	isc::CPostAddress iscPa;
	int i, n;
	aAdrs.SetCountZeroND();
	for (i = 0, n = aiAdrs.GetCount(); i < n; i++) {
		cafe::adrss.GetItem(fl::ioffset, aiAdrs[i], pa);
		iscPa.SetToSend(aiAdrs[i], pa);
		aAdrs.AddND(iscPa);
	}
	opnrPa.Close();
	fl::CFileItemObtainer<fl::CStrings, fl::CString> ostrs(fl::strs);
	fl::CString &str = ostrs.obj();
	asTel.SetCountZeroND();
	for (i = 0, n = aiTel.GetCount(); i < n; i++) {
		fl::strs.GetItem(fl::ioffset, aiTel[i], str);
		asTel.AddND(str);
	}
	asRefs.SetCountZeroND();
	for (i = 0, n = aiRefs.GetCount(); i < n; i++) {
		fl::strs.GetItem(fl::ioffset, aiRefs[i], str);
		asRefs.AddND(str);
	}
	return true;
}

int32_t CContractor::SaveToBase(int16_t iind, fl::CFileItem &fi)
{
	CBaseItem::SaveToBase(fl::ioffset, fi);
	int32_t i, n = aiAdrs.GetCount();
	ASSERT(n == aAdrs.GetCount());
	fl::a::COpener<cafe::CPostAdrss> oa(cafe::adrss);
	for (i = 0; i < n; i++)
		aiAdrs[i] = aAdrs[i].SaveToBase(fl::ioffset);
	oa.Close();
	fl::a::COpener<fl::CStrings> os(fl::strs);
	ASSERT(aiRefs.GetCount() == asRefs.GetCount());
	for (i = 0, n = aiRefs.GetCount(); i < n; i++)
		aiRefs[i] = fl::a::SetItem(fl::strs, asRefs[i], fl::ioffset, aiRefs[i]);
	ASSERT(aiTel.GetCount() == asTel.GetCount());
	for (i = 0, n = aiTel.GetCount(); i < n; i++)
		aiTel[i] = fl::a::SetItem(fl::strs, asTel[i], fl::ioffset, aiTel[i]);
	iAggrt = fl::a::SetItem(fl::strs, sAggrt, fl::ioffset, iAggrt);
	iAcnt = fl::a::SetItem(fl::strs, sAcnt, fl::ioffset, iAcnt);
	iBnkNm = fl::a::SetItem(fl::strs, sBnkNm, fl::ioffset, iBnkNm);
	iBnkID = fl::a::SetItem(fl::strs, sBnkID, fl::ioffset, iBnkID);
	iBisID = fl::a::SetItem(fl::strs, sBisID, fl::ioffset, iBisID);
	os.Close();
	Get(i, fi);
	return (iObj = SaveObject(iind, iObj, fi));// ид-р может измениться
}

int32_t CContractor::SaveObject(int32_t iInd, int32_t iObj, fl::CFileItem &fi)
{
	return fl::a::SetItem(cafe::cntrctrs, CAST(fl::CFileItem, cafe::CContractor, fi), iInd, iObj);
}
#endif // #ifdef __ITOG

CBaseItem& CContractor::Set(int32_t iItm, const fl::CFileItem &fl)
{
	const cafe::CContractor *pc = CAST(const fl::CFileItem, const cafe::CContractor, &fl);
	ASSERT(pc != nullptr && (tp == fl::CBaseObj::cntrctr || tp == fl::CBaseObj::splr));
	CBaseItem::Set(iItm, fl);
	aiAdrs = pc->m_aiAdrs;
	aiRefs = pc->m_aiRefs;
	aiTel = pc->m_aiTel;
	iCntr = pc->m_bCntr;
	iAcnt = pc->m_iAcnt;
	iAggrt = pc->m_iAggrt;
	iBisID = pc->m_iBisID;
	iBnkID = pc->m_iBnkID;
	iBnkNm = pc->m_iBnkNm;
	// CBaseItem не может это получить из fl::CFileItem
	iImg = pc->m_iImg;
	iNm = pc->m_iNm;
	iDn = pc->m_iDn;
	return *this;
}

fl::CFileItem& CContractor::Get(int32_t &iItm, fl::CFileItem &c) const
{
	cafe::CContractor *pc = CAST(fl::CFileItem, cafe::CContractor, &c);
	ASSERT(pc != nullptr && (tp == fl::CBaseObj::cntrctr || tp == fl::CBaseObj::splr));
	CBaseItem::Get(iItm, c);
	pc->m_iNm = iNm;
	pc->m_iDn = iDn;
	pc->m_iImg = iImg;
	pc->m_aiAdrs = aiAdrs;
	pc->m_aiRefs = aiRefs;
	pc->m_aiTel = aiTel;
	pc->m_bCntr = iCntr;
	pc->m_iAcnt = iAcnt;
	pc->m_iAggrt = iAggrt;
	pc->m_iBisID = iBisID;
	pc->m_iBnkID = iBnkID;
	pc->m_iBnkNm = iBnkNm;
	return c;
}

int32_t CObjNm::GetBinData(BYTES &ab) const
{
	ab.AddND((CBYTE*) &m_iStrID, SZ_OBJNM);
	return SZ_OBJNM + m_snm.GetBinData(ab);
}

int32_t CObjNm::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = glb::read_raw((BYTE*) &m_iStrID, SZ_OBJNM, cpb, cnt);
	return i + m_snm.SetBinData(cpb + i, cnt - i);
}

/*#ifdef __ITOG

void CMenue::SetToSend(uint32_t uImg, uint32_t uSbimg) // uSbimg = 0
{
	ASSERT((tp == fl::CBaseObj::menue || tp == fl::CBaseObj::submenue) && iObj >= 0);
	int i, n;
	fl::CBaseObj *po = nullptr;
	switch (tp) {
	case fl::CBaseObj::menue:
		po = cafe::menues.static_obj(nullptr);
		cafe::menues.GetItem(fl::ioffset, iObj, *po);
		break;
	case fl::CBaseObj::submenue:
		po = cafe::sbmns.static_obj(nullptr);
		cafe::sbmns.GetItem(fl::ioffset, iObj, *po);
	default:
		ASSERT(false);
	}
	CBaseItem::Set(po->m_tmc, po->m_tme, po->m_flags, iObj, po->m_iPrnt, po->m_iNm, po->m_iDn, po->m_iImg, tp);
	CBaseItem::SetToSend(uImg, uSbimg);
	for (i = 0, n = sbitms.GetCount(); i < n; i++)
		delete sbitms[i];
	sbitms.SetCountZeroND();
	if (!uSbimg)
		return; // подэлементы не нужны
	CProduct *pp = nullptr;
	CMenue *pm = nullptr;
	const glb::CFxArray<fl::CBaseObj::ID, true> &caSbitms = po->Subitems();
	fl::CBaseObj::ID sbitmID;
	for (i = 0, n = caSbitms.GetCount(); i < n; i++) {
		sbitmID = caSbitms[i];
		switch (sbitmID.tp) {
		case fl::CBaseObj::submenue:
			ASSERT(tp == fl::CBaseObj::menue);
			sbitms.AddND(pm = new CMenue(sbitmID.tp, sbitmID.i));
			pm->SetToSend(uSbimg); // подменю без продуктов
			break;
		case fl::CBaseObj::product:
			ASSERT(tp == fl::CBaseObj::submenue);
			sbitms.AddND(pp = new CProduct(sbitmID.i));
			pp->SetToSend(uSbimg);// продукт без ингредиентов
			break;
		default:
			ASSERT(false);
		}
	}
}

int32_t CMenue::SaveToBase(int16_t iind)
{
	ASSERT(iPrnt >= 0 && (tp == fl::CBaseObj::menue || tp == fl::CBaseObj::submenue));
	ASSERT(iNm >= 0);
	// записываю наименование и описание
	CBaseItem::SaveToBase(fl::ioffset);
	// беру файловый объект меню если есть наименование
	fl::CBaseObj m;
	GetFlItem(m);
	// записываю его в файловый массив
	int32_t i = fl::a::SetItem((tp == fl::CBaseObj::menue) ? cafe::menues : cafe::sbmns, m, iind, iObj);
	if (i < 0 && iObj >= 0)
		if (tp == fl::CBaseObj::submenue)
			fl::a::RmSubitem<cafe::CMenues, fl::CBaseObj>(cafe::menues, iPrnt, iObj);
		else // патриарх меню - отдел или подотдел, они оба в одном файле
			fl::a::RmSubitem<cafe::CDepts, cafe::CDept>(cafe::depts, iPrnt, iObj);
	return (iObj = i);
}

#endif */// defined __ITOG

const CDept& CDept::operator =(const CDept &o)
{	// базовый класс
	BAS::operator =(o);
	// этот класс
//	const CDept &d = CAST(const CBaseItem, const CDept, o);
	cats.SetCountZeroND();
	cats = o.cats;
	apSbitms = o.apSbitms;
	return *this;
}

CDept& CDept::operator =(CDept &&o)
{
	BAS::operator =(o);
//	CDept &d = CAST(CBaseItem, CDept, o);
	cats.Attach(o.cats);
	apSbitms.Attach(o.apSbitms);
	return *this;
}

int32_t CDept::SetBinData(CBYTE *cpb, cint32_t cnt)
{	// сначала базовый класс...
	int32_t n, j, i = BAS::SetBinData(cpb, cnt);
	ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
	// к-во категорий
	i += glb::read_raw((BYTE*) &n, sizeof n, cpb + i, cnt - i);
	// категории товаров по одной в индексированный массив
	cats.SetCountZeroND();
	for (j = 0; j < n; j++, i += sizeof(uint32_t))
		cats.Add(glb::read_tp<uint32_t>(cpb + i, cnt - i));		// 2. категории товаров
	return i;
}

fl::CFileItem& CDept::Get(int32_t &iItm, fl::CFileItem &fl) const
{
	ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
	cafe::CDept *pd = CAST(fl::CFileItem, cafe::CDept, &fl);
	ASSERT(pd != nullptr);
	CBaseItemComplex::Get(iItm, fl);
	pd->m_aCat = cats;
	return fl;
}

#ifdef __ITOG

void CDept::SetToSend(fl::CBaseObj &fi, int32_t iID, uint32_t uImg, uint32_t uSbimg)
{
	CBaseItemComplex::SetToSend(fi, iID, uImg, uSbimg);
	cafe::CDept *pd = CAST(fl::CBaseObj, cafe::CDept, &fi);
	ASSERT(nullptr != pd);
	cats = pd->m_aCat;
}

CBaseItem& CDept::Set(int32_t iItm, const fl::CFileItem &fl)
{
	ASSERT(false);// зови сразу SetToSend()
}

#else

CBaseItem& CDept::Set(int32_t iItm, const fl::CFileItem &fl)
{
	ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
	const cafe::CDept *pd = CAST(const fl::CFileItem, const cafe::CDept, &fl);
	ASSERT(pd != nullptr);
	CBaseItemComplex::Set(iItm, fl);
	cats = pd->m_aCat;
	return *this;
}

#endif// defined __ITOG
/*#ifdef __ITOG

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CDept::SetToSend собираю из базы всё необходимое для отображения отдела/подотдела
/// \param uImg: размер рисунка эл-та ширина в нижн.слове, высота в верхнем
/// \param uSbimg: аналогично для подэлемента. Если отсутствует, подэлементы не выбираю из базы
///
void CDept::SetToSend(uint32_t uImg, uint32_t uSbimg) // uSbimg = 0
{
//	CBaseItemComplex::SetToSend(uImg, uSbimg);
	// iObj присваивается в конструкторе, или SetDeptID()
	ASSERT(iObj >= 0 && (tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept));
	int i, n;
	cafe::CDept &d = *cafe::depts.static_obj(nullptr);
	cafe::depts.GetItem(fl::ioffset, iObj, d);
	ASSERT(tp == d.m_tp);
	CBaseItem::Set(d.m_tmc, d.m_tme, d.m_flags, iObj, d.m_iPrnt, d.m_iNm, d.m_iDn, d.m_iImg, d.m_tp);
	CBaseItem::SetToSend(uImg);
	cats = d.m_aCat;
	// удалю все подэлементы, если есть чтобы не разбираться с их типами
	for (i = 0, n = sbitms.GetCount(); i < n; i++)
		delete sbitms[i];
	sbitms.SetCountZeroND();
	if (!uSbimg)
		return; // подэлементы не нужны
	// извлекаю подэлементы
	fl::CBaseObj::ID sid; // subitem id
	const glb::CFxArray<fl::CBaseObj::ID, true> &asid = d.Subitems(); // subitems' ids
	// подэлементы из adid
	CDept *pd = nullptr;
	CMenue *pm = nullptr;
	for (i = 0, n = asid.GetCount(); i < n; i++, pm = nullptr, pd = nullptr) {
		sid = asid[i];
		// в отделе может быть подотдел или меню
		switch (sid.tp) {
		case fl::CBaseObj::menue:
			sbitms.AddND(pm = new CMenue(sid.tp, sid.i));
			pm->SetToSend(uSbimg); // только меню с уменьшенным изображением без подменю
			break;
		case fl::CBaseObj::subdept:
			sbitms.AddND(pd = new CDept(sid.tp, sid.i));
			pd->SetToSend(uSbimg); // так же только подменю с уменьшенным изображением без подэлементов
			break;
		default:
			ASSERT(false);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief isc::CDept::SaveToBase записываю отдел в файл cafe::depts. Если объект не пустой, он будет
/// добавлен в файловый массив или изменён по правилам целостности индексированного массива. Если объ-
/// ект пустой и есть действительный ид-р объекта, он будет отмечен в файле как не действительный.
/// \return базовый ид-р отдела в массиве отделов и подотделов
///
int32_t CDept::SaveToBase(int16_t iind)
{
	ASSERT((tp == fl::CBaseObj::dept && iPrnt < 0) ||
		   (tp == fl::CBaseObj::subdept && iPrnt >= 0));
	ASSERT(iNm >= 0);
	// записываю наименование и описание через колено предка CMenue, чтобы не смешивать меню с отделом
	CBaseItem::SaveToBase(fl::ioffset);
	// беру файловый объект
	cafe::CDept d;
	GetFlItem(d);
	// записываю его в файловый массив
	int32_t i = fl::a::SetItem(cafe::depts, d, iind, iObj);
	// если запись не состоялась, безпощыдно удалю ид-р этого подотдела из отдела-патриарха
	if (i < 0 && iObj >= 0 && tp == fl::CBaseObj::subdept)
		fl::a::RmSubitem<cafe::CDepts, cafe::CDept>(cafe::depts, iPrnt, iObj);
	return (iObj = i);
}

#endif */// defined __ITOG

const CBaseItemComplex& CBaseItemComplex::operator =(const CBaseItemComplex &o)
{	// первое действо - базовый класс
	CBaseItem::operator =(o);
	// этот класс
	int i, ns = o.apSbitms.GetCount(), nd = apSbitms.GetCount();
	CBaseItem *pid = nullptr;		// d - destination
	const CBaseItem *pis = nullptr;	// s - source
	// по всем имеющимся в 'o' подэлементам
	for (i = 0; i < ns; i++) {
		if (i >= nd) {// тут нет эл-та
			apSbitms.AddND(CBaseItem::Duplicate(o.apSbitms[i]));
			continue;
		}
		pid = apSbitms[i];
		pis = o.apSbitms[i];
		if (pis->tp == pid->tp &&
			(pis->tp != fl::CBaseObj::ingrdt || (pis->flgs & ITM_MNMS) == (pid->flgs & ITM_MNMS))) {
			*pid = *pis;// подэл-ты одного типа
			continue;
		}
		delete pid;
		apSbitms[i] = CBaseItem::Duplicate(pis);
	}
	while (i < nd)
		delete apSbitms[i++];
	apSbitms.SetCountND(ns);
	return *this;
}

int32_t CBaseItemComplex::GetBinData(BYTES &ab) const
{
	cint32_t cn = ab.GetCount();
	int32_t i, n = apSbitms.GetCount();
	// базовый класс
	CBaseItem::GetBinData(ab);
	// подэл-ты
	ab.Add((CBYTE*) &n, sizeof n);
	for (i = 0; i < n; i++)
		apSbitms[i]->GetBinData(ab);
	// возвращаю к-во записанный байт
	return (ab.GetCount() - cn);
}

int32_t CBaseItemComplex::SetBinData(CBYTE *cpb, cint32_t cnt)
{	// объект базового класса
	int32_t ns = 0, nd = apSbitms.GetCount(), j, i;
	CBaseItem sbitm, *ps = nullptr;
	// объект базового класса
	i = CBaseItem::SetBinData(cpb, cnt);
	// к-во подэлементов
	i += glb::read_raw((BYTE*) &ns, sizeof ns, cpb + i, cnt - i);
	for (j = 0; j < ns; j++) {
		// чтобы определить тип подэлемента на вводе, нужно прочесть целую часть его CBaseItem
		glb::read_raw((BYTE*) &sbitm.tmEdit, SZ_BASITM, cpb + i, cnt - i);
		ASSERT(sbitm.TypeValid());
		if (j >= nd)
			apSbitms.AddND(ps = CBaseItem::Create(sbitm.tp));
		else {
			ps = apSbitms[j];
			if (!ps->TypeEqual(sbitm)) {
				delete ps;
				apSbitms[j] = ps = CBaseItem::Create(sbitm.tp);
			}
		}// с++ рулит
		i += ps->SetBinData(cpb + i, cnt - i);
	}
	while (j < nd)
		delete apSbitms[j++];
	apSbitms.SetCountND(ns);
	return i;
}

fl::CFileItem& CBaseItemComplex::Get(int32_t &iItm, fl::CFileItem &fl) const
{
	fl::CBaseObj *pb = CAST(fl::CFileItem, fl::CBaseObj, &fl);
	ASSERT(pb != nullptr);
	CBaseItem::Get(iItm, fl);
	int i, n;
	const CBaseItem *const *ppID = apSbitms.data(n);
	glb::CFxArray<fl::CBaseObj::ID, true> &aID = pb->Subitems();
	aID.SetCountZeroND();
	for (i = 0; i < n; i++)
		aID.AddND({ ppID[i]->tp, ppID[i]->iObj });
	return fl;
}

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItemComplex::Set вызывается клиентом после сохранения объекта на сервере
/// и получения ответа сервера с объетом fi
/// \param iItm: ид-р сохранённого на серве объекта
/// \param fl
/// \return
///
CBaseItem& CBaseItemComplex::Set(int32_t iItm, const fl::CFileItem &fl)
{
	CBaseItem::Set(iItm, fl);
	const fl::CBaseObj *pb = CAST(const fl::CFileItem, const fl::CBaseObj, &fl);
	ASSERT(pb != nullptr);
	const glb::CFxArray<fl::CBaseObj::ID, true> &aID = pb->Subitems();
	ASSERT(apSbitms.GetCount() == aID.GetCount());
	for (int i = 0, n = aID.GetCount(); i < n; i++) {
		ASSERT(apSbitms[i]->tp == aID[i].tp);
		// каждый ингредиент сохраняется в своём файле вместе с продуктом
		if (aID[i].tp == fl::CBaseObj::ingrdt)
			apSbitms[i]->iObj = aID[i].i;
		else// объекты, как "модменю", "подотдел", "продукт" получают ид-ры в своих формах
			ASSERT(apSbitms[i]->iObj == aID[i].i);
	}
	return *this;
}

#ifdef __ITOG

void CBaseItemComplex::SetToSend(fl::CBaseObj &bo, int32_t iID, uint32_t uImg, uint32_t uSbimg)
{
	CBaseItem::Set(iID, bo);
	CBaseItem::SetToSend(uImg);
	int i, n;
	if (!uSbimg && tp != fl::CBaseObj::product)
		return; // подэлементы не нужны
	// извлекаю подэлементы по их идентификаторам и типами
	const glb::CFxArray<fl::CBaseObj::ID, true> &asid = bo.Subitems();
	fl::CBaseObj::ID sid;
	CBaseItem *pi = nullptr, *ps = nullptr;
	// по всем эл-там asid
	for (i = 0, n = asid.GetCount(); i < n; i++) {
		sid = asid[i];
		if (apSbitms.GetCount() > i)
			if ((ps = apSbitms[i])->tp == sid.tp) {
				ps->iObj = sid.i;
				ps->SetToSend(uSbimg);
				continue;
			} else {
				delete ps;
				apSbitms.RemND(i);
			}
		switch (sid.tp) {
		case fl::CBaseObj::subdept:	// подэл-т отдела или подотдела
			ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
			pi = new CDept(sid.tp, sid.i);
			break;
		case fl::CBaseObj::menue:	// подэл-т отдела или подотдела
			ASSERT(tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept);
			pi = new CMenue(sid.tp, sid.i);
			break;
		case fl::CBaseObj::submenue:// подэлемент меню
			ASSERT(tp == fl::CBaseObj::menue);
			pi = new CMenue(sid.tp, sid.i);
			break;
		case fl::CBaseObj::product:	// подэлемент подменю
			ASSERT(tp == fl::CBaseObj::submenue);
			pi = new CProduct(sid.i);
			break;
		case fl::CBaseObj::ingrdt:
			ASSERT(tp == fl::CBaseObj::product);
			pi = new CIngredient(sid.i);
			break;
		default:
			ASSERT(false);
		}// с++ рулит
		if (pi->SetToSend(uSbimg))
			apSbitms.Insert(i, pi);
		else {
			ASSERT(false);
			delete pi;
		}
	}// если в apSbitms остались эл-ты
	for (n = apSbitms.GetCount(); i < n; i++)
		delete apSbitms[i];
	ASSERT(asid.GetCount() <= apSbitms.GetCount());
	apSbitms.SetCountND(asid.GetCount());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CBaseItemComplex::SetToSend подготовка эл-та к отправке клиенту. Ид-р эл-та должен быть
/// известен либо из конструктора, или непосредственно присвоен в случае повторного использования.
/// \param uImg: размеры рисунка основного эл-та
/// \param uSbimg: размеры рисунка подэлемента
/// \param bVisible: только отображаемые эл-ты (по-умолчанию "трю")
/// \return "трю" если эл-т подготовлен к отправке клиенту, "хрю" в сл.ошибки или эл-т не отображаемый
/// если bVisible "трю"
///
bool CBaseItemComplex::SetToSend(uint32_t uImg, uint32_t uSbimg, bool bVisible)// uSbimg = 0, bVisible = true
{
	ASSERT(iObj >= 0);
	cafe::CDept *pd = nullptr;
	fl::CBaseObj *pfo = nullptr;
	auto sts =[this, uImg, uSbimg, bVisible] (fl::CBaseObj &o) {
		if (o.m_flags & ITM_DEL || (bVisible && o.m_flags & (ITM_EDIT|ITM_HDN|ITM_ARC)))
			return false;
		SetToSend(o, iObj, uImg, uSbimg); // возможно рекурсия...
		return true;
	};
	if (tp == fl::CBaseObj::dept || tp == fl::CBaseObj::subdept) {
		fl::CFileItemObtainer<cafe::CDepts, cafe::CDept> od(cafe::depts);
		return sts(od.GetItem(fl::ioffset, iObj));
	}
	else if (tp == fl::CBaseObj::menue) {
		fl::CFileItemObtainer<cafe::CMenues, fl::CBaseObj> om(cafe::menues);
		return sts(om.GetItem(fl::ioffset, iObj));
	}
	else if (tp == fl::CBaseObj::submenue) {
		fl::CFileItemObtainer<cafe::CMenues, fl::CBaseObj> osm(cafe::sbmns);
		return sts(osm.GetItem(fl::ioffset, iObj));
	}
	else if (tp == fl::CBaseObj::product) {
		fl::CFileItemObtainer<store::CProducts, store::CProduct> op(store::prdcts);
		return sts(op.GetItem(fl::ioffset, iObj));
	}
	return false;
}

int32_t CBaseItemComplex::SaveToBase(int16_t iind, fl::CFileItem &fi)
{	// у отдела нет патриарха
	ASSERT(tp == fl::CBaseObj::dept && iPrnt < 0 || iPrnt >= 0);
	int32_t iNew = INV32_BIND;
	// базовый класс в первую очередь
	CBaseItem::SaveToBase(fl::ioffset, fi);
	// попытаюсь записать эл-т в базу... если не получится, безпощадно удалю его ид-р из патриарха
	switch (tp) {
	case fl::CBaseObj::dept:
	case fl::CBaseObj::subdept:
		Get(iNew, fi);
		if ((iNew = fl::a::SetItem(cafe::depts, CAST(fl::CFileItem, cafe::CDept, fi), iind, iObj)) < 0
			&& iObj >= 0 && tp == fl::CBaseObj::subdept)
			fl::a::RmSubitem<cafe::CDepts, cafe::CDept>(cafe::depts, iPrnt, tp, iObj);
		break;
	case fl::CBaseObj::menue:
		Get(iNew, fi);
		if ((iNew = fl::a::SetItem(cafe::menues, CAST(fl::CFileItem, fl::CBaseObj, fi), iind, iObj)) < 0
			&& iObj >= 0)
			fl::a::RmSubitem<cafe::CDepts, cafe::CDept>(cafe::depts, iPrnt, tp, iObj);
		break;
	case fl::CBaseObj::submenue:
		Get(iNew, fi);
		if ((iNew = fl::a::SetItem(cafe::sbmns, CAST(fl::CFileItem, fl::CBaseObj, fi), iind, iObj)) < 0
			&& iObj >= 0)
			fl::a::RmSubitem<cafe::CMenues, fl::CBaseObj>(cafe::menues, iPrnt, tp, iObj);
		break;
	case fl::CBaseObj::product:
		{	// у ингредиента нет своей отдельной формы, каждый редактируется в списке. Сохраняются здесь
			store::CIngredient ing;
			for (int i = 0, n = apSbitms.GetCount(); i < n; i++)
				apSbitms[i]->SaveToBase(fl::ioffset, ing);
			Get(iNew, fi);
			if ((iNew = fl::a::SetItem(store::prdcts, CAST(fl::CFileItem, store::CProduct, fi), iind, iObj)) < 0
				&& iObj >= 0)
				fl::a::RmSubitem<cafe::CMenues, fl::CBaseObj>(cafe::sbmns, iPrnt, tp, iObj);
		}
		break;
	default:
		ASSERT(false);
	}
	return (iObj = iNew);
}

#endif

CBaseItem& CSupplier::Set(int32_t iItm, const fl::CFileItem &fl)
{
	const cafe::CSupplier *ps = CAST(const fl::CFileItem, const cafe::CSupplier, &fl);
	ASSERT(nullptr != ps && tp == fl::CBaseObj::splr);
	CContractor::Set(iItm, fl);
	cats = ps->m_aCat;
	return *this;
}

fl::CFileItem& CSupplier::Get(int32_t &iItm, fl::CFileItem &fl) const
{
	cafe::CSupplier *ps = CAST(fl::CFileItem, cafe::CSupplier, &fl);
	ASSERT(nullptr != ps && tp == fl::CBaseObj::splr);
	CContractor::Get(iItm, fl);
	ps->m_aCat.SetCountZeroND();
	for (int i = 0, n = cats.GetCount(); i < n; i++)
		ps->m_aCat.Add(cats[i]);
	return fl;
}

#ifdef __ITOG

int32_t CSupplier::SaveObject(int32_t iInd, int32_t iObj, fl::CFileItem &fi)
{
	return fl::a::SetItem(cafe::splrs, CAST(fl::CFileItem, cafe::CSupplier, fi), iInd, iObj);
}

#endif

}// namespace isc
