#pragma once
/*
 * Copyright 2021-2024 Николай Вамбольдт
 *
 * Файл io_file.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021-2024 Nikolay Vamboldt
 *
 * io_file.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 *******
 *
 * Файл с синхронизированным доступом не откроется, если занят другим
 * потоком. Не важно, открыт ли он для чтения или для записи.
 * Файл с синхронизированным доступом нужно закрывать сразу после
 * чтения или записи.
 * Использую ф-ции стандартной С-библиотеки для чтения и записи всех
 * файлов.
 * Использую классы стандартной С++ библиотеки для синхронизации.
 *
 * Файл может содержать объекты определённого размера и переменного. Файл с
 * объектами динамического размера является индексированным и содержит по
 * меньшей мере один индекс с файловыми указателями каждого объекта.
 *
 * В Линейщикъ'е есть файл с действительными объектами, такими как пользо-
 * вательские линии, открытые заказы, эл-ты стат-ки с заказами, уровни
 * фибо, уровни вершин, настройки п-ля. В связи с добавлением прокрутки
 * графика, которой нет в рабочем Линейщикъ'е, и которая нуждается в быст-
 * ром доступе к изтории, возникает необходимость в хранении длинных мас-
 * сивов со свечами и вершинами.
 * Как с ними быть? Читать целый файл и действовать в памяти? Или искать
 * прямо в файле нужный эл-т, читать нужное к-во? Можно сделать простой
 * файловый массив (индексированный уже есть) и действовать с ним как
 * с массивом '[]'. Так и сделаю.
 * Кроме того, хотелось бы систематизировать всю файловую работу. Есть
 * файлы:
 * 1. документ
 * 2. индексированный массив (который в перспективе переезжает в память
 *    из-за низкой производительности).
 * 3. не индексированный файловый массив.
 * Всё это разрознено, но как то объединить пока не получится. Можно на-
 * чать с простого класса "файл", который открывает, читает, записывает,
 * удаляет, вставляет.
 * На его основе сделать шаблон, который делает всё это с объектом и мас-
 * сивом объектов.
 */
#include <cstdio>
//#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include "FxString.h"

namespace fl {
/*************************************************************************
 * Класс CBinFile открывает файл, читает, записывает, вставляет, удаляет,
 * используя стандартную С-библиотеку. Сразу после открытия читает размер
 * файла, перед закрытием записывает размер файла обратно. При исполнении
 * всех процедур сокращает или увеличивает размер.
 * Стандартная библиотека не предлагает уменьшать размер файла поэтому
 * физ.размер файла CBinFile может только расти, а логический может сокра-
 * щаться. Логический размер контроллирует CBinFile::m_nsz. Эта переменная
 * не содержит собственный размер.
 * Недостатки: 1) при возникновении ошибки ввода/вывода файл следует удалить.
 * Вероятно, можно делать временную копию файла, её изменять, а перед завер-
 * шением работы приложения, или во время простоя заменять исходный файл
 * правильно изменённой копией.
 * 2) Физический размер файла остаётся неизвестным. Если понадобится, можно
 * будет добавить другую переменную, которая будет хранить максимальный раз-
 * мер файла:
 * struct CHeader {
 *		int64_t m_nsz, m_nmx;
 * } m_sz;
 */
class CBinFile {
protected:
	int64_t m_nsz;		// размер файла (bytes) без учёта размера m_nsz.
protected:				//  Делится чисто на размер (int-)объекта в файле
	FILE *m_pf;			// результат ф-ции 'fopen'
	glb::CFxString m_sPath;// путь и имя файла
public:// конструкцыи/деструкцыи...
	CBinFile();
	virtual ~CBinFile();// деструктор обязательно закрывает открытый файл...
public:// открытие/закрытие
//	virtual int64_t open(glb::CFxString &sPathNm);
	virtual int64_t open(const char *pcszPathNm = NULL);
	virtual void close();
public:// грубое вмешательство - добавление/вставка, редактирование, удаление
	virtual int64_t put(const void *pd, const int64_t cnt);
	virtual int64_t putcp(const int64_t at, const void *pd, const int64_t cnt);
	virtual int64_t insert(const int64_t at, const void *pd, const int64_t cnt);
	virtual int64_t edit(const int64_t at, const void *pd, const int64_t cnt);
	virtual int64_t remove(const int64_t at, const int64_t cnt);
public:// извлечение
	virtual int64_t get(const int64_t at, void *pb, const int64_t cnt) const;
	virtual int64_t getcp(const int64_t at, void *pb, const int64_t cnt) const;
public:// атрибуты
	virtual int64_t sz() const;
	const char* path() const;
	virtual bool IsOpen() const;
};

/////////////////////////////////////////////////////////////////////
/// \brief CBinFile::put добавляет указанное к-во байт в конец файла.
/// \param pd: указатель на массив добавляемых байт
/// \param cnt: к-во добавляемых байт.
/// \return к-во добавленных байт.
///
inline int64_t CBinFile::put(const void *pd, const int64_t cnt)
{
	return insert(m_nsz, pd, cnt);
}

inline int64_t CBinFile::sz() const
{
	return m_nsz;
}

inline const char *CBinFile::path() const
{
	return m_sPath;
}

inline bool CBinFile::IsOpen() const
{
	return (m_pf != nullptr);
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief putcp - добавляю 'file' (в тек.позицию) содержимое массива 'a' в таком формате:
/// - к-во эл-тов (sizeof m_iCount)
/// - последовательность эл-тов.
/// Если объект содержит указатели или массивы (!bIntType), он должен содержать аналогичную
/// этой ф-цию 'int putcp(int64_t at, fl::CBinFile &file)'
/// \param at: тек.позиция в файле (должна точно соответствовать файловой)
/// \param file: открытый файл, где файловая позиция установлена верно и она соответствует 'at'
/// \return к-во записанных байт.
///
template<class TP, bool bIntType>
int32_t putcp(const glb::CFxArray<TP, bIntType> &a, int64_t at, CBinFile &file)
{	// сначала к-во, независимо от содержимого массива
	const int64_t ns = at;
	int32_t cnt = a.GetCount();
	at += file.putcp(at, &cnt, sizeof cnt);
	if (bIntType)
		at += file.putcp(at, a, cnt * sizeof(TP));
	else for (int32_t i = 0; i < cnt; i++)
		at += a[i].putcp(at, file);
	return (int32_t) (at - ns);
}

////////////////////////////////////////////////////////////////////////////////////
/// \brief getcp: обратная ф-ция 'putcp' читает:
/// - к-во эл-тов (sizeof m_iCount)
/// - последовательность эл-тов.
/// Если объект содержит указатели или массивы (!bIntType), он должен содержать аналогичную
/// этой ф-цию 'int getcp(int64_t at, fl::CBinFile &file)'
/// \param at: тек.позиция в файле (должна точно соответствовать файловой)
/// \param file: открытый файл, где файловая позиция установлена верно и она соответствует 'at'
/// \return к-во прочитанных байт.
///
template<class TP, bool bIntType>
int32_t getcp(glb::CFxArray<TP, bIntType> &a, int64_t at, CBinFile &file)
{
	const int64_t ns = at;
	int32_t cnt = 0;
	at += file.getcp(at, &cnt, sizeof cnt);
	a.SetCountNoDelete(cnt);
	if (bIntType)
		at += file.getcp(at, a, cnt * sizeof(TP));
	else for (int32_t i = 0; i < cnt; i++)
		at += a[i].getcp(at, file);
	return (int32_t) (ns - at);
}

} // namespace fl