#pragma once
/*
 * Copyright 2021 Николай Вамбольдт
 *
 * Файл 'exceptdef.h' — часть программ 'liner' и 'oacrl', входящих в проект
 * 'linerfx'
 *
 * 'liner' и 'oacrl' свободные программы: вы можете перераспространять их и/или
 * изменять их на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения; либо
 * версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * 'liner' и 'oacrl' распространяются в надежде, что они будут полезными,
 * но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
 * общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с этой программой. Если это не так, см.
 * <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * 'exceptdef.h' is part of 'liner' and 'oacrl', which are a part of
 * 'linerfx'.
 *
 * 'liner' and 'oacrl' are free software: you can redistribute them and/or
 * modify them under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * 'liner' and 'oacrl' are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE
 * along with 'liner' and 'oacrl'. If not, see
 * <https://www.gnu.org/licenses/>.
 */
/*#ifndef EXCEPTDEF_H
#define EXCEPTDEF_H*/
/*
#define FRMTSTR_INPUT_EXCEPTION_RISE			"%t швыряю исключение \"Ошибка ввода\" из файла \'%s\', стр.%d.\n"
#define FRMTSTR_OA_EXCEPTION_RISE				"%t швыряю исключение %d \"ошибка Oanda\" из файла \'%s\', стр.%d.\n"
#define FRMTSTR_CURL_EXCEPTION_RISE				"%t исключение CURL(%d) в файле \'%s\', стр.%d.\n"
#define FRMTSTR_C_EXCEPTION_RISE				"%t \'C\'-исключение(%d) в файле \'%s\', стр.%d.\n"
#define	FRMTSTR_LINER_EXCEPTION_RISE			"%t исключение \'%s\' в файле \'%s\', стр.%d.\n"
#define FRMTSTR_LINER_EXCEPTION_CATCH			"%t исключение \'%s\' при использовании файла \'%s\'\n"
#define FRMTSTR_LINER_UNKNNEXCEP_CATCH			"%t не ожидаемое исключение при использовании файла \'%s\'\n"
#define FRMTSTR_UT_EXCEPTION					"%t исключение в библиотеке \'uturn\': \'%s\' в файле \'%s\', стр.%d.\n"
#define UNEXPECTED_EXCEPTION					"не ожидаемое исключение"
#define	FRMTSTR_WRONG_NPFILE_STRUCT				"Неверная структура файла безпериодного индикатора %s"

// строки в liner
#define FRMT_STR_BB_TIME_OFFSET					"%t %s: несоответствие времени последней свечи в массиве" // (%t) и новой (%t)!"
#define FRMT_STR_BB_INCONSISTENCE				"%t %s: нарушена последовательность свечей!"// Время %*t предшествует времени %*t."
#define FRMT_STR_IND_CREATION					"%t %s: невозможно создать индикатор(ы)."
*/
#define OA_HTTP_OK								200
#define OA_HTTP_BAD_REQUEST						400
#define OA_HTTP_UNAUTHORISED					401
#define OA_HTTP_FORBIDDEN						403
#define OA_HTTP_NOT_FOUND						404
#define OA_HTTP_METHOD_NOT_ALLOWED				405
#define OA_HTTP_HUI_EGO_ZNAET					416
