#include <QString>
#include <QImage>
#include "defs.h"
#include "global.h"
#include <QGradient>
#ifdef __ITOG
#include "FxArray.h"
#include "../iTog/iTog/global.h"
#endif// defines __ITOG
#include <QBuffer>

namespace glb {

// последнее слово из строки 's'
QString LastWord(const QString &s)
{
	int n = s.length(),
		i = s.lastIndexOf(QChar(' '));
	return ((i < 0) ? s : s.last(n - i - 1));
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CutAndScale возвращает обрезанное в нужное соотношение сторон изображение
/// \param img: данное изображение
/// \param fAspRatio: соотношение сторон: ширина / высоту
/// \return обрезанное изображение
///
QImage CutImage(const QImage &img, float fAspRatio)
{
	QImage imgot;// out
	float fari = (float) img.width() / img.height(), fe = .001;// image's asp.ratio
	int n, ns;
	QRect rci;// image's rect
	// высота экрана 1280, ширина 1920, соотношение fAspRatio 1,5
	// высота изображения 600, ширина 800, соотношение fari 1,3333
	//
	// высота экрана 1920, ширина 1280, соотношение fars 0,6666666666666667
	// высота изображения 800, ширина 600, соотношение fari 0,75
	if (DCMP_MORE(fAspRatio, fari, fe)) {// сокращаю высоту изображения
		n = img.height();
		if (n - (ns = glb::Round(img.width() / fAspRatio, 1.f)) > 3) {
			rci.setRect(0, 0, img.width(), ns);
			rci.moveTop((n - ns) / 2);
			imgot = img.copy(rci);
		}
	} else if (DCMP_LESS(fAspRatio, fari, fe)) {// сокращаю ширину изображения
		n = img.width();
		if (n - (ns = glb::Round(img.height() * fAspRatio, 1.f)) > 3) {
			rci.setRect(0, 0, ns, img.height());
			rci.moveLeft((n - ns) / 2);
			imgot = img.copy(rci);
		}
	} else
		imgot = img;
	return imgot;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief ScaleImage вычисляет прямоугольник для отображения вписанного в окно изображения
/// с сохранением пропорций. Для последующего масштабирования изображения.
/// \param img: изображение
/// \param r: прямоугольник окна, в которое выводится img (!top и !left)
/// \return прямоугольник для изображения, вписанный в r
///
QRect ScaleImage(const QImage &img, const QRect &r)
{
	QRect ri(img.rect());
	float farw = (float) r.height() / r.width(), // asp.ratio of window
	        fari = (float) ri.height() / ri.width(), fe = .001;// image's asp.ratio
	/*
	 * окно 1080 / 1920 = ,5625
	 * изо 400 / 500 = ,8
	 */
	int nih, niw, nindt;
	/* чем меньше соотношение сторон, тем меньше высота
	 * чем больше соот-е, тем больше высота
	 * если высота больше и соотношение не меньше, тогда впишется
	 */
	if (ri.height() > r.height() && DCMP_MOE(fari, farw, fe)) {// рисунок выше окна (масштабируется по высоте)
		nih = r.height(); // высота изображения
		niw = glb::Round(r.height() / fari, 1.f);// ширина изображения
		nindt = (r.width() - niw) / 2;// отступ справа и слева
		return QRect(r.left() + nindt, r.top(), niw, nih);
	} else if (ri.width() > r.width() && DCMP_LOE(fari, farw, fe)) {// изображение шире окна (на всю ширину окна с отступами снизу сверху)
		niw = r.width();// ширина изо будет
		nih = glb::Round(r.width() * fari, 1.f);// высота изображения
		nindt = (r.height() - nih) / 2;// отступ снизу/сверху
		return QRect(r.left(), r.top() + nindt, niw, nih);
	}// центрировать изображение в 'r'
	ri.moveCenter(r.center());
	return ri;
}

QRectF ScaleImage(const QImage &img, const QRectF &r)
{
	QRectF ri(img.rect());
	float farw = r.height() / r.width(), // asp.ratio of window
	        fari = ri.height() / ri.width(), fe = .001f;// image's asp.ratio
	/* окно 1080 / 1920 = ,5625
	 * изо 400 / 500 = ,8
	 */
	float nih, niw, nindt;
	/* чем меньше соотношение сторон, тем меньше высота
	 * чем больше соот-е, тем больше высота
	 * если высота больше и соотношение не меньше, тогда впишется
	 */
	if (ri.height() > r.height() && DCMP_MOE(fari, farw, fe)) {// рисунок выше окна (масштабируется по высоте)
		nih = r.height(); // высота изображения
		niw = glb::Round(r.height() / fari, 1.f);// ширина изображения
		nindt = (r.width() - niw) / 2;// отступ справа и слева
		return QRect(r.left() + nindt, r.top(), niw, nih);
	} else if (ri.width() > r.width() && DCMP_LOE(fari, farw, fe)) {// изображение шире окна (на всю ширину окна с отступами снизу сверху)
		niw = r.width();// ширина изо будет
		nih = glb::Round(r.width() * fari, 1.f);// высота изображения
		nindt = (r.height() - nih) / 2;// отступ снизу/сверху
		return QRect(r.left(), r.top() + nindt, niw, nih);
	}// центрировать изображение в 'r'
	ri.moveCenter(r.center());
	return ri;
}

#if defined __ITOG// | defined __STRFLA
//////////////////////////////////////////////////////////////////////////////////////////////
/// \brief GetImage читаю рисунок с диска с синхронизацией доступа к файлу между потоками и процессами,
/// (поэтому клиенту эта ф-ция недоступна) обрезаю и масштабирую рисунок в соответствии с аргументами,
/// добавляю рисунок в массив для отправки клиенту в формате:
/// 1. размер рисунка 32 бит, если bAddSz. Может быть 0, если рисунка нет
/// 2. рисунок. Может отсутствовать, если размер 0.
/// \param iImgID: ид-р строки с именем рисунка
/// \param nw: требуемая ширина рисунка
/// \param nh: требуемая высота рисунка
/// \param ab: массив, в который добавляется рисунок
/// \param bAddSz: вставлять ли размер рисунка в байтах в 'ab' перед содержимым?
/// \return к-во записанных в ab байт: минимум 32 бит с размером рисунка + рисунок (может отсутствовать)
///
int32_t GetImage(int32_t iImgID, int32_t nw, int32_t nh, BYTES &ab, bool bAddSz)
{
	const int32_t cn = ab.GetCount();
	// читаю рисунок с диска в 'ab'... шаблон ф-ции из ../iTog/iTog/global.h
	if (!GetFile<BYTES, BYTE>(ab, SI_PICTD, iImgID, true))
		return (ab.GetCount() - cn);// нет файла, допускается. В 'ab' записан размер 0 (32 бит)
	int32_t i = cn + sizeof cn,
			n = read_tp<int32_t>(ab + cn, ab.GetCount() - cn);
	ASSERT(n > 0 && n == ab.GetCount() - i);
	QImage img;
	img.loadFromData(QByteArray::fromRawData((CCHAR*) (CBYTE*) ab + i, n));
	if (!img.isNull() && img.format() != QImage::Format_Invalid) {
		float f1 = (float) nh / nw,
			f2 = (float) img.height() / img.width();
		f2 = fabs(f1 - f2);
		if (DCMP_MORE(f2, .01, .001))// > 1%
			img = CutImage(img, (float) nw / nh);
		img = img.scaled(QSize(nw, nh), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	} else {// рисунок есть, но не распознаётся Qt. Возвращаю 0
		ab.SetCountND(cn);
		if (bAddSz) {
			n = 0;
			ab.AddND((CBYTE*) &n, sizeof n);
		}
		return (ab.GetCount() - cn);
	}// записываю рисунок в массив как рекомендовано в описании QImage::save(), т.е. через пень - колода
	QByteArray qab;
	QBuffer qbuf(&qab);
	qbuf.open(QIODevice::WriteOnly);
	img.save(&qbuf, "jpg"); // writes image into qbuf in JPG format
	// собираю ответ
	ab.SetCountND(cn);
	n = qab.length();
	if (bAddSz) 
		ab.AddND((CBYTE*) &n, sizeof n);	// размер рисунка, если требуется
	ab.AddND((CBYTE*) qab.constData(), n);	// рисунок
	return (ab.GetCount() - cn);			// к-во записанных байт
}

#endif // if defined __ITOG

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief UnderlineSymbol вставит амперсанд перед символом, найденым в алфавите
/// \param s: строка, в которой подчёркивается символ
/// \param sAlbt: алфавит (упорядоченные по возрастанию буквы алфавита)
/// \return строку с подчёркнутым символом
///
QString& UnderlineSymbol(QString &s, QString &sAlbt)
{
	int i = 0, j = -1, nal = sAlbt.length(), nsl = s.length();
	const QChar *ps = sAlbt.constData();
	while (i < nsl && nal > 0 && (j = FindEqual(ps, nal, s[i].toLower())) < 0)
		i++;
	if (i < nsl && j >= 0) {
		sAlbt.remove(j, 1);
		s.insert(i, QChar('&'));
	}
	return s;
}

}// namespace glb