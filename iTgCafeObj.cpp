/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл iTgCafeObj.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * iTgCafeObj.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "iTgCafeObj.h"
#include "io_string.h"
#ifdef __ITOG
#include "io_strings.h"
#include "postadrss.h"
#include "iTgOpts.h"
#include "fl_cats.h"
namespace fl {
	extern CStrings strs;
}
namespace store {
	extern CCats cats;
}
namespace cafe {
	extern CPostAdrss adrss;
}
namespace o {
	extern CiTgOpts opt;
}

#endif // difined __ITOG

namespace glb {
	const char* FileName(const char *pcszPath, int *pnLen);
}
namespace cafe {

/////////////////////////////////////////////////////////////////////////////
/// \brief CDept::GetBinData
/// \param a
/// \return
///
/*int CDept::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.Add((CBYTE*) &m_iNm, SZ_DEPT);
	m_ai.GetBinData(a);
	return (a.GetCount() - cnt);
}

int CDept::SetBinData(CBYTE *cpb, int cnt)
{
	int i = fl::CFileItem::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_iNm, SZ_DEPT, cpb + i, cnt - i);
	return (i + m_ai.SetBinData(cpb + i, cnt - i));
}*/

int CCoworker::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.AddND((BYTE*) &m_tmBrn, SZ_CWRKR);
	m_aiRef.GetBinData(a);
	m_aiTel.GetBinData(a);
	return (a.GetCount() - cnt);
}

int CCoworker::SetBinData(CBYTE *cpb, int cnt)
{	// отступ из базового класса в файл не записываю
	int i = fl::CFileItem::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_tmBrn, SZ_CWRKR, cpb + i, cnt - i);
	i += m_aiRef.SetBinData(cpb + i, cnt - i);
	return (i + m_aiTel.SetBinData(cpb + i, cnt - i));
}

CContractor::CContractor()
{
	m_iNm = m_iBisID = m_iBnkID = m_iBnkNm = m_iAcnt = m_iAggrt =
		m_iDn = m_iImg = INV32_BIND;
	m_bCntr = 0xff;
}

CContractor::CContractor(const CContractor &o)
	: BAS(o)
	, m_aiTel(o.m_aiTel)
	, m_aiRefs(o.m_aiRefs)
	, m_iNm(o.m_iNm)
	, m_iBisID(o.m_iBisID)
	, m_iBnkID(o.m_iBnkID)
	, m_iBnkNm(o.m_iBnkNm)
	, m_iAcnt(o.m_iAcnt)
	, m_iAggrt(o.m_iAggrt)
	, m_iDn(o.m_iDn)
	, m_iImg(o.m_iImg)
	, m_bCntr(o.m_bCntr)
{

}

CContractor::CContractor(CContractor &&o)
	: BAS(o)
	, m_aiTel(o.m_aiTel)
	, m_aiRefs(o.m_aiRefs)
	, m_iNm(o.m_iNm)
	, m_iBisID(o.m_iBisID)
	, m_iBnkID(o.m_iBnkID)
	, m_iBnkNm(o.m_iBnkNm)
	, m_iAcnt(o.m_iAcnt)
	, m_iAggrt(o.m_iAggrt)
	, m_iDn(o.m_iDn)
	, m_iImg(o.m_iImg)
	, m_bCntr(o.m_bCntr)
{

}

void CContractor::Init()
{
	BAS::Init();
	m_iNm = m_iBisID = m_iBnkID = m_iBnkNm = m_iAcnt = m_iAggrt = m_iDn = m_iImg = INV32_BIND;
	m_bCntr = 0xff;
}

int CContractor::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	const int cnt = a.GetCount();
	BAS::GetBinData(a);
	a.AddND((BYTE*) &m_iNm, SZ_CNTRCTR);
	m_aiRefs.GetBinData(a);
	m_aiTel.GetBinData(a);
	m_aiAdrs.GetBinData(a);
	return (a.GetCount() - cnt);
}

int CContractor::SetBinData(CBYTE *cpb, int cnt)
{
	int i = BAS::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_iNm, SZ_CNTRCTR, cpb + i, cnt - i);
	i += m_aiRefs.SetBinData(cpb + i, cnt - i);
	i += m_aiTel.SetBinData(cpb + i, cnt - i);
	return (i + m_aiAdrs.SetBinData(cpb + i, cnt - i));
}

int CContractor::cmp_iTpNm(const CContractor &a, const CContractor &z)
{
	return (a.m_bCntr - z.m_bCntr) ? a.m_bCntr - z.m_bCntr :
				(a.m_iNm - z.m_iNm) ? a.m_iNm - z.m_iNm : 0;
}

int CContractor::cmp_inm(const CContractor &a, const CContractor &z)
{	// ид-р имени - обязательный параметр
	return (a.m_iNm > z.m_iNm) ? 1 : (a.m_iNm < z.m_iNm) ? -1 : 0;
}

#ifdef __ITOG

int CContractor::cmp_tpNm(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_bCntr < 0 || z.m_bCntr < 0) {
		ASSERT(a.m_bCntr != z.m_bCntr);
		return (a.m_bCntr - z.m_bCntr);
	}
	if (a.m_iNm < 0 || z.m_iNm < 0) {
		ASSERT(a.m_iNm != z.m_iNm);
		return (a.m_iNm - z.m_iNm);
	}
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	if (a.m_bCntr != z.m_bCntr) {
		fl::CString &sz = oz.GetItem(fl::ioffset, o::opt.m_aiCnts[z.m_bCntr]);
		return oa.GetItem(fl::ioffset, o::opt.m_aiCnts[a.m_bCntr]).cmp(sz);
	}
	return oa.GetItem(fl::ioffset, a.m_iNm).cmp(oz.GetItem(fl::ioffset, z.m_iNm));
}

int CContractor::cmp_nm(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iNm == z.m_iNm)
		return 0;
	if (a.m_iNm < 0)
		return -1;
	if (z.m_iNm < 0)
		return 1;
	ASSERT(fl::strs.IsOpen());
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iNm).cmp(oz.GetItem(fl::ioffset, z.m_iNm));
}

int CContractor::cmp_bis_id(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iBisID == z.m_iBisID)
		return 0;
	if (a.m_iBisID < 0)
		return -1;
	if (z.m_iBisID < 0)
		return 1;
	ASSERT(fl::strs.IsOpen());
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iBisID).cmp(oz.GetItem(fl::ioffset, z.m_iBisID));
}

int CContractor::cmp_bnk_id(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iBnkID == z.m_iBnkID)
		return 0;
	if (a.m_iBnkID < 0)
		return -1;
	if (z.m_iBnkID < 0)
		return 1;
	ASSERT(fl::strs.IsOpen());
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iBnkID).cmp(oz.GetItem(fl::ioffset, z.m_iBnkID));
}

int CContractor::cmp_bnk_nm(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iBnkNm == z.m_iBnkNm)
		return 0;
	if (a.m_iBnkNm < 0)
		return -1;
	if (z.m_iBnkNm < 0)
		return 1;
	ASSERT(fl::strs.IsOpen());
	fl::CFileItemObtainer<fl::CStrings, fl::CString> oa(fl::strs), oz(fl::strs);
	return oa.GetItem(fl::ioffset, a.m_iBnkNm).cmp(oz.GetItem(fl::ioffset, z.m_iBnkNm));
}

int CContractor::cmp_bacnt(const CContractor &a, const CContractor &z)
{
	ASSERT(fl::strs.IsOpen());
	if (a.m_iAcnt == z.m_iAcnt)
		return 0;
	if (a.m_iAcnt < 0)
		return -1;
	if (z.m_iAcnt < 0)
		return 1;
	ASSERT(fl::strs.IsOpen());
	fl::CFileItemObtainer<fl::CStrings, fl::CString> os(fl::strs), os2(fl::strs);
	return os.GetItem(fl::ioffset, a.m_iAcnt).cmp(os2.GetItem(fl::ioffset, z.m_iAcnt));
}

int CContractor::cmp_addrs(const CContractor &a, const CContractor &z)
{
	ASSERT(adrss.IsOpen());
	ASSERT(fl::strs.IsOpen());
	int na = a.m_aiAdrs.GetCount(), nz = z.m_aiAdrs.GetCount();
	if (!na || !nz)
		return na - nz;
	fl::CFileItemObtainer<CPostAdrss, CPostAdrs> oa(adrss), oz(adrss);
	return CPostAdrss::cmp_cntr_rg_tn_str_bf(oa.GetItem(fl::ioffset, a.m_aiAdrs[0]),
											 oz.GetItem(fl::ioffset, z.m_aiAdrs[0]));
}

#endif // def __ITOG

int CSupplier::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	uint32_t c;
	int32_t n, i = BAS::SetBinData(cpb, cnt);
	m_aCat.SetCountZeroND();
	i += glb::read_tp(n, cpb + i, cnt - i);
	while (n-- > 0) {
		i += glb::read_tp(c, cpb + i, cnt - i);
		m_aCat.Add(c);
	}
	return i;
}

int32_t CBill::GetBinData(BYTES &a) const
{
	cint32_t cnt = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.AddND((CBYTE*) &m_iCashier, SZ_BILL);
	m_aPrdcts.GetBinData(a);
	return (a.GetCount() - cnt);
}

int32_t CBill::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int32_t i = fl::CFileItem::SetBinData(cpb, cnt);
	i += glb::read_raw((BYTE*) &m_iCashier, SZ_BILL, cpb + i, cnt - 1);
	return (i + m_aPrdcts.SetBinData(cpb + i, cnt - i));
}

double CBill::Sum() const
{
	return .0;
}

/*int32_t CBill::CPrdct::GetBinData(BYTES ab)
{
	cint32_t c = sizeof m_fQtt + sizeof m_iPrdct;
	ab.AddND((BYTE*) &m_fQtt, c);
	return (c + m_aAdds.GetBinData(ab));
}

int32_t CBill::CPrdct::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	cint32_t c = glb::read_raw((BYTE*) &m_fQtt, sizeof m_fQtt + sizeof m_iPrdct, cpb, cnt);
	return (c + m_aAdds.SetBinData(cpb + c, cnt - c));
}*/

int32_t CPostAdrs::GetBinData(BYTES &a) const
{
	const int32_t c = a.GetCount();
	fl::CFileItem::GetBinData(a);
	a.Add((CBYTE*) &m_iCountry, SZ_PADRS);
	return (a.GetCount() - c);
}

int32_t CPostAdrs::SetBinData(CBYTE *cpb, cint32_t cnt)
{
	int i = fl::CFileItem::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_iCountry, SZ_PADRS, cpb + i, cnt - i));
}

int CJob::GetBinData(glb::CFxArray<BYTE, true> &a) const
{
	int n = fl::CFileItem::GetBinData(a);
	a.Add((CBYTE*) &m_iNm, SZ_JOB);
	return (n + SZ_JOB);
}

int CJob::SetBinData(CBYTE *cpb, const int32_t cnt)
{
	int32_t i = fl::CFileItem::SetBinData(cpb, cnt);
	return (i + glb::read_raw((BYTE*) &m_iNm, SZ_JOB, cpb + i, cnt - i));
}

}// namespace cafe