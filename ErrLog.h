#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл ErrLog.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * ErrLog.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#if defined __ITGCLNT || defined __LTTS
#include <mutex>
#endif
#include "FxString.h"
#include <QObject>

namespace err {
/*
 * CLog - журнал событий, включая ошибки и всё прочее.
 */
class CLog : public QObject
{
	Q_OBJECT

public:
	CLog(QObject *parent = NULL);
	~CLog();

#if defined __ITGCLNT || defined __LTTS

private:
	// для клиента iTog синхронизация доступа к файлу между потоками потому что у клиента нет разделяемой памяти
	std::recursive_mutex m_mtx;

public:
	// имя папки журнала...
	glb::CFxString m_sDir;

#endif // defined __ITGCLNT || defined __LTTS

protected:
	int putl(glb::CFxString &s);

public:
	const char* PathName(glb::CFxString &s) const;// для сервера и клиента определена в их global.cpp
	void Add(const char *pcszFrmt, ...); // добавляю новую строку
	void Add(const char *pcszFormat, va_list &vl);

signals:// для отображения в списке или строке состояния
	void NewRecord(glb::CFxString &s);
};

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLog::CLog конструктор журнала
/// \param pPrnt:
/// \param pfPath: ф-ция, которая возвращает путь к файлу журнала
///
//inline CLog::CLog(const char*(*pfPath)(glb::CFxString &s), QObject *pPrnt)// pfPath = NULL, pPrnt = NULL
inline CLog::CLog(QObject *pPrnt)// pfPath = NULL, pPrnt = NULL
	: QObject(pPrnt)
{
//	m_pfp = pfPath;
}

} // namespace err