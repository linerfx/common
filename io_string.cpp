#include "io_string.h"
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл io_string.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или
 * изменять на условиях Стандартной общественной лицензии GNU в том виде, в
 * каком она была опубликована Фондом свободного программного обеспечения;
 * либо версии 3 лицензии, либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в
 * надежде, что они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной
 * гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее
 * смотрите в Стандартной общественной лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * io_string.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with
 * each of reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
namespace glb {

// сравнение строк:
int cmp_iUtf8s(CCHAR *pcsza, cint32_t n1, CCHAR *pcszb, cint32_t n2); // без учёта регистра
int cmp_utf8s(CCHAR *pcsz1, cint32_t c1, CCHAR *pcsz2, cint32_t c2);

}

namespace fl {

/////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CStrings::CString::GetBinData помещает объект в байтовый массив
/// \param a: результирующий массив
/// \return к-во записанных в 'a' байт.
///
int CString::GetBinData(BYTES &a) const
{
	const int32_t c = a.GetCount();
	CFileItem::GetBinData(a);
	m_str.GetBinData(a);
	return (a.GetCount() - c);
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::SetBinData возвращаю в массив ранее взятой ф-цией GetBinData
/// \param cpb: указатель на первый символ
/// \param cnt: к-во байт в 'cpb'
/// \return к-во прочитанных байт
///
int CString::SetBinData(CBYTE *cpb, int32_t cnt)
{
	int32_t i = CFileItem::SetBinData(cpb, cnt);
	return (i + m_str.SetBinData(cpb + i, cnt - i));
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmp посимвольное сравнение двух utf-8 строк
/// \param s: сравниваемый эл-т
/// \return <0, 0 или >0
///
int CString::cmp(const CString &s) const
{
	int32_t na, nz;
	CBYTE *pba = m_str.data(na), *pbz = s.m_str.data(nz);
	return glb::cmp_utf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmpci сравниваю 2 строки без учёта регистра (case insensitive)
/// \param s:
/// \return <0|0|>0
///
int CString::cmpi(const CString &s) const
{
	int32_t na, nz;
	CBYTE *pba = m_str.data(na), *pbz = s.m_str.data(nz);
	return glb::cmp_iUtf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmp_objNms (уникальный индекс) сравнивает флаги и utf-8 строки, группирует
/// строки по флагам, таким как пароли, наименования товаров
/// \param a, z - сравниваемые объекты
/// \return <0 или 0 или >0
///
int CString::cmp_objNms(const CString &a, const CString &z)// static
{
	int32_t na = a.m_flags & ITM_MNMS,	// обнуляю все флаги кроме флагов наименований
			nz = z.m_flags & ITM_MNMS;
	if (na < nz)
		return -1;
	if (na > nz)
		return 1;
	CBYTE *pba = a.m_str.data(na), *pbz = z.m_str.data(nz);
	return glb::cmp_iUtf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}

/*int CString::cmp_stpl_nm(const CString &a, const CString &z)
{
	if ((a.m_flags & ITM_STPL) < (z.m_flags & ITM_STPL))
		return -1;
	if ((a.m_flags & ITM_STPL) > (z.m_flags & ITM_STPL))
		return 1;
	return cmps(a, z);
}*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmp_stpl_nmi сравнение двух файловый строк по флагу ITM_STPL и наименованию
/// без учёта регистра.
/// \param a, z: сравниваемые объекты (строки)
/// \return <0|0|>0
///
/*int CString::cmp_stpl_nmi(const CString &a, const CString &z)
{	// строки без флагов
	if (!(a.m_flags & ITM_STPL) && !(z.m_flags & ITM_STPL))
		return 0;
	if ((a.m_flags & ITM_STPL) != (z.m_flags & ITM_STPL))
		return (a.m_flags & ITM_STPL) - (z.m_flags & ITM_STPL);
	int32_t na, nz;
	CBYTE *pba = a.m_str.data(na), *pbz = z.m_str.data(nz);
	return glb::cmp_iUtf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}*/

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmps сравнение двух не завершённых нулём строк
/// \param a, z: сравниваемые эл-ты
/// \return <0, 0 или >0
///
/*int CString::cmps(const CString &a, const CString &z)
{
	int32_t na, nz;
	const BYTE *pba = a.m_str.data(na), *pbz = z.m_str.data(nz);
	return glb::cmp_utf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}*/

//////////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::cmpsi уник.индекс - сравнение строк без учёта регистра
/// \param a, z: сравниваемые строки
/// \return <0|0|>0
///
int CString::cmp_si(const CString &a, const CString &z)
{
	int32_t na, nz;
	CBYTE *pba = a.m_str.data(na), *pbz = z.m_str.data(nz);
	return glb::cmp_iUtf8s((CCHAR*) pba, na, (CCHAR*) pbz, nz);
}

///////////////////////////////////////////////////////////////////////////////////////
/// \brief CString::str вернёт завершённую нулём строку. В файле строки хранятся без него.
/// \return
///
CCHAR* CString::str() const
{
	int32_t n = m_str.GetCount();
	if (!n || m_str.Last() != '\0') {
		BYTES &ab = const_cast<BYTES&>(m_str);
		ab.AddND('\0'); // увеличит к-во на 1, но это строка, а не массив байт, поэтому:
		ab.SetCountND(n);
	}
	return (CCHAR*) (CBYTE*) m_str;
}

}// namespace fl